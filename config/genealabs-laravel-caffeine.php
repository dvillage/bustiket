<?php

return [
    'dripIntervalInMilliSeconds' => 600000,
//    'dripIntervalInMilliSeconds' => 60000,
    'domain' => url('/'),
    'route' => 'genealabs/laravel-caffeine/drip',
];
