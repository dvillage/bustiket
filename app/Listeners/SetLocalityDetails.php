<?php

namespace App\Listeners;

use App\Events\SetLocality;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Models\Locality as Locality;

class SetLocalityDetails {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(SetLocality $event) {
//        dd($event);
        
        $id = $event->id;
        $opr = $event->opr;
        $type = $event->type;
        $name = $event->name;
        
//        dd($id , $opr ,$type ,$name );
        $loc = [
            'Terminal' => 'T',
            'District' => 'D',
            'City'     => 'C',
            'Province' => 'P',
        ];
        
        if($opr == 'Insert'){
            $addLoc = new Locality();
            $addLoc->name = trim($name);
            $addLoc->lid = $id;
            $addLoc->type = $loc[$type];
            $addLoc->save();
        }
        elseif($opr == 'Update'){
            $upLoc = Locality::where(['lid'=>$id,'type'=>$loc[$type]])->first();
            if($upLoc != null){
                $upLoc->name = trim($name);
                $upLoc->save();
            }else{
                $addLoc = new Locality();
                $addLoc->name = trim($name);
                $addLoc->lid = $id;
                $addLoc->type = $loc[$type];
                $addLoc->save();
            }
        }
        elseif($opr == 'Delete'){
            $delLoc = Locality::where('lid',$id)->delete();
        }
        
        
    }

}
