<?php

namespace App\Listeners;

use App\Events\Signup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterSignupHistory
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Signup  $event
     * @return void
     */
    public function handle(Signup $event)
    {
        if(env("APP_ENV") == "local")
            return true;
        
        $ip = \Request::getClientIp();
        $ua = [\Request::server("HTTP_USER_AGENT")];
        $sh = \App\Models\SignupHistory::where("ip",$ip)->first();
        if(is_null($sh))
        {
            $sh = new \App\Models\SignupHistory();
            $sh->ip = $ip;
        }
        else
        {
            $ua = $sh->ua;
            $ua[] = \Request::server("HTTP_USER_AGENT");
        }
        \Log::info('Register signup history.');
        $sh->ua = $ua;
        $sh->save();
    }
}
