<?php

namespace App\Listeners;

use App\Events\TicketBookSuccess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\mpdf\MPDF57;
class SendPushNotification {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(TicketBookSuccess $event) {
        $ticket_detail = $event->ticket_detail;
        $bb = $ticket_detail['book_by'];
        $user_id = $ticket_detail['user_id'];
        $booking_id = $ticket_detail['booking_id'];
        $userDetail = [];
        if($bb != 1){
            if($user_id != ''){
                if(in_array($bb, [2,3,4])){
                    $userDetail = \App\Models\Serviceprovider\ServiceProvider::where('id',$user_id)->first();
                }elseif(in_array($bb, [5,6,7])){
                    $userDetail = \App\Models\Admin\SeatSeller::where('id',$user_id)->first();
                }else{
                    $userDetail = \App\Models\Users::where('id',$user_id)->first();
                }
            }
        }
        if($userDetail){
            if($userDetail->device_type == 1){
                if($userDetail->device_token != ''){
                    $meta = array(array("booking_id" => $booking_id));
                    $device_tokens = array($userDetail->device_token);
                    $msg = array("Tiket anda sudah diterima");
                    $title = array("Bustiket");
                    $badge = array(1);
                    $screen = array("thc");
                    $res = \App\Lib\Push::android_push($device_tokens, $msg, $title, $badge, $screen, $meta,array(),array(array("")));
                }
            }
        }
        
    }

}
