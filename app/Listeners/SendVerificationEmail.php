<?php

namespace App\Handlers\Events;

use App\Events\NewUserSignup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendVerificationEmail implements ShouldBeQueued {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(NewUserSignup $event) {
        $user_detail = $event->user_detail;


        $settings = \App\Models\Setting::get_config('user_account_mail_verification');

        $user_detail = $event->user_detail;
        $user_detail['mail_subject'] = \Lang::get("success.verify_email_mail_subject");
        $html = \Mail::send('emails.user.account_confirm', $user_detail, function ($message) use ($user_detail) {
                    $message->to($user_detail['user_email'])->subject($user_detail['mail_subject']);
                });

//            $html = \View::make("emails.user.account_confirm",$user_detail);
//            echo $html;
    }

}
