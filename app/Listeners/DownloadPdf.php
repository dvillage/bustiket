<?php

namespace App\Listeners;

use App\Events\TicketBookSuccess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\mpdf\MPDF57;
class DownloadPdf {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(TicketBookSuccess $event) {
        $ticket_detail = $event->ticket_detail;
        $bb = $ticket_detail['book_by'];
        $user_id = $ticket_detail['user_id'];
        $sp_id = $ticket_detail['sp_id'];
        $booking_id = $ticket_detail['booking_id'];
        $total_amount = $ticket_detail['total_amount'];
        $total_seats = $ticket_detail['nos'];
        $hf = 0;
        if($bb == 0 || $bb == 1){
            $hf = 1;
        }
//        echo $bb;exit;
        \App\Models\AdminCommission::admin_commission($sp_id, $booking_id, $total_amount, $total_seats, $bb,$user_id,$hf);
        
        if($bb == 5 || $bb == 6 || $bb == 7){
//            dd($bb);
            if ($bb != 5) {
                $fetchagent = \App\Models\SsCommission::ss_commission($user_id, $sp_id, $booking_id, $total_amount, $total_seats,1);
                \App\Models\SsCommission::ss_commission($fetchagent['parent_id'], $sp_id, $booking_id, $total_amount, $total_seats);
            }else{
                \App\Models\SsCommission::ss_commission($user_id, $sp_id, $booking_id, $total_amount, $total_seats,1);
            }
        }else if($bb == 3 || $bb == 4){
            \App\Models\SpCommission::sp_commission($user_id, $booking_id, $total_amount, $total_seats);
        }
        
        if(preg_match("/[a-z]/i", $ticket_detail['bus_id']) && $ticket_detail['booking_system'] > 0){
            if($ticket_detail['booking_system'] == 2){
                $bus = \App\Models\Lorena::get_bus_info($booking_id);
                $bus = json_decode($bus['bus_detail'],true);
            }else{
                $bus = \App\Models\ApiBusInfo::where('booking_id',$booking_id)->first();
                $bus = json_decode($bus->bus_detail,true);
            }
            
            $bs = $ticket_detail['booking_seats'][0];
            $param = [];
            $param['from_term_id'] = $bs['from_terminal'];
            $param['to_term_id'] = $bs['to_terminal'];
            $term = \App\Models\Roda::get_terminals($bus, $param);
            
            $ticket_detail['from_terminal'] = [
                'name'  => $term['data']['board_point'] ,
                'loc_citi' => ['name' => $bus['from']],
                'loc_district' => ['name' => ''],
            ];
            $ticket_detail['to_terminal'] = [
                'name'  => $term['data']['drop_point'] ,
                'loc_citi' => ['name' => $bus['to']],
                'loc_district' => ['name' => ''],
            ];
            $ticket_detail['route_code'] = $bus['route_code'];
            $ticket_detail['bus'] = ['name' => $bus['bus'],'layout'=>1];
        }
        
        $newToPdf = [];
        $ticket_detail['mail_subject'] = "E-tiket Detail Perjalanan Anda dari www.bustiket.com";
       
        $file_path = config('constant.TICKET_PDF_PATH').'bustiket_'.$ticket_detail['booking_id'].'.pdf';
        
        
//        $pdf = \PDF::loadView("site.e_ticket_mail",array('data'=>$ticket_detail))->save($file_path);
        $pdf = \PDF::loadView("site.e_ticket_mail",array('data'=>$ticket_detail), [], [
                                                    'margin_top' => 0,
                                                    'margin_left' => 0,
                                                    'margin_bottom' => 0,
                                                    'margin_right' => 0,
                                                  ])->save($file_path);

        $html = \Mail::send('site.e_ticket', array('data'=>$ticket_detail), function ($message) use ($ticket_detail,$file_path) {
            $message->to($ticket_detail['booker_email'])->subject($ticket_detail['mail_subject']);
            $message->to("info@bustiket.com")->subject($ticket_detail['mail_subject']);
            $message->attach(($file_path));
        });
    }

}
