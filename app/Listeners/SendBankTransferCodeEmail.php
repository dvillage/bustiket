<?php

namespace App\Listeners;

use App\Events\TicketBookByBank;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendBankTransferCodeEmail {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(TicketBookByBank $event) {
        $ticket_detail = $event->ticket_detail;
//        dd($ticket_detail);
        $s2 = time() + (50 * 60);
        $expire_time = date("H:i", $s2);
        $bank_detail = \App\Models\PaymentRecord::where("booking_id",$ticket_detail['booking_id'])->pluck("bank_details")->first();
        $bank_detail = json_decode($bank_detail,true);
        
        $passengerinfo = \App\Models\BookingSeats::where("booking_id",$ticket_detail['booking_id'])->get()->toArray();
//        dd($passengerinfo);
        $external = $ticket_detail['booking_system'];
        if($external > 0){
            if($external == 2){
                $bus = \App\Models\Lorena::get_bus_info($ticket_detail['booking_id']);
                $bus = json_decode($bus['bus_detail'],true);
            }else{
                $bus = \App\Models\ApiBusInfo::where('booking_id',$ticket_detail['booking_id'])->first();
                $bus = json_decode($bus->bus_detail,true);
            }
            
            $bs = $passengerinfo[0];
            $param = [];
            $param['from_term_id'] = $bs['from_terminal'];
            $param['to_term_id'] = $bs['to_terminal'];
            $term = \App\Models\Roda::get_terminals($bus, $param);
            
            $ticket_detail['from_terminal'] = [
                'name'  => $term['data']['board_point'] ,
                'loc_citi' => ['name' => $bus['from']],
                'loc_district' => ['name' => ''],
            ];
            $ticket_detail['to_terminal'] = [
                'name'  => $term['data']['drop_point'] ,
                'loc_citi' => ['name' => $bus['to']],
                'loc_district' => ['name' => ''],
            ];
            $ticket_detail['route_code'] = $bus['route_code'];
            $ticket_detail['bus'] = ['name' => $bus['bus'],'layout'=>1];
            $route_str = $ticket_detail['from_terminal']['name']." - ".$ticket_detail['to_terminal']['name'];
        }else{
            $route = \App\Models\Admin\BusRoutes::where("bus_id",$ticket_detail['bus_id'])
                                            ->where("from_terminal_id",$ticket_detail['from_terminal_id'])
                                            ->where("to_terminal_id",$ticket_detail['to_terminal_id'])
                                            ->first();
            $route_str = $ticket_detail['from_terminal']['name']." - ".$ticket_detail['to_terminal']['name'];
            if(!is_null($route))
            {
                $route_str = $route->route;
            }
            $layout = $ticket_detail['bus']['layout'];
        }
        
        $site_logo = app("settings")['logo'];
        $site_logo = url("assets/uploads/logo")."/$site_logo";
        $date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($ticket_detail['pickup_date'])),date("l", strtotime($ticket_detail['pickup_date'])));
        $depdate = \App\Models\General::getIndoMonths(date("j F Y", strtotime($ticket_detail['dropping_date'])),date("l", strtotime($ticket_detail['dropping_date'])));
        $user_detail = [
            "email"                 => $ticket_detail['booker_email'],
            "mail_subject"          => "Pemesanan Tiket ".$ticket_detail['bt_id'],
            "uniqid"                => $ticket_detail['bt_id'],
            "expire_time"           => $expire_time,
            "bank_account_name"     => $bank_detail['account_name'],
            "bank_account_no"       => $bank_detail['account_no'],
            "bank_address"          => $bank_detail['bank_address'],
            "bank_name"             => $bank_detail['bank_name'],
            "final_payable_amount"  => number_format(($ticket_detail['total_amount'])/ 1000, 3),
            "total_amt"             => number_format($ticket_detail['base_amount'] / 1000, 3),
            "discount_price"        => number_format($ticket_detail['coupon_discount'] / 1000, 3),
            "handling_fee"          => number_format($ticket_detail['handling_fee'] / 1000, 3),
            "date"                  => $date,
            "bording_time"          => $ticket_detail['from_terminal']['name']."(".date("H:i",  strtotime($ticket_detail['pickup_date'])).")",
            "departure_time"        => $ticket_detail['to_terminal']['name']."(".$depdate.' '.date("H:i",  strtotime($ticket_detail['dropping_date'])).")",
            "user_name"             => $ticket_detail['booker_name'],
            "user_email"            => $ticket_detail['booker_email'],
            "user_mobile"           => $ticket_detail['booker_mo'],
            "passenger"             => $passengerinfo,
            "route"                 => $route_str,
            "layout"                => $ticket_detail['bus']['layout'],
            "parent_busname"        => $ticket_detail['bus']['name'],
            'service_provider_name' => $ticket_detail['sp']['first_name'],
            "imglogo"               => $site_logo,
        ];
//        dd($user_detail);
//        echo \View::make('emails.user.ticket_pay_by_bank_transfer_code', $user_detail);
//        exit;
        \Mail::send('emails.user.ticket_pay_by_bank_transfer_code', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject("Pemesanan Tiket :".$user_detail['uniqid']);
            $message->to("info@bustiket.com")->subject("Pemesanan Tiket :".$user_detail['uniqid']);
        });
        
    }

}
