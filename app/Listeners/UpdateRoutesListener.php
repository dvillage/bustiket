<?php

namespace App\Listeners;

use App\Events\UpdateRoutes;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Models\Locality as Locality;

class UpdateRoutesListener {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(UpdateRoutes $event) {
//        dd($event);
        
        
        $type = $event->type;
        $data = $event->data;
//        dd($data);
        if($type == 'C'){
            $this->updateFromCity($data);
        }elseif($type == 'D'){
            $this->updateFromDistrict($data);
        }elseif($type == 'T'){
            $this->updateFromTerminal($data);
        }
        
    }
    
    public function updateFromCity($data){
        $dist = Locality\District::where('city_id',$data->id)->update(['province_id'=>$data->province_id]);

        $term = Locality\Terminal::where('city_id',$data->id)->update(['province_id'=>$data->province_id]);

        
        $fRoute = \App\Models\Admin\BusRoutes::where('from_city_id',$data->id)->update(['from_province'=>$data->province_id]);

        $tRoute = \App\Models\Admin\BusRoutes::where('to_city_id',$data->id)->update(['to_province'=>$data->province_id]);

//        dd($data);
    }
    
    public function updateFromDistrict($data){
        
        $term = Locality\Terminal::where('district_id',$data->id)->update(
                [
                    'city_id'=>$data->city_id,
                    'province_id'=>$data->province_id,
                ]);

        
        $fRoute = \App\Models\Admin\BusRoutes::where('from_district_id',$data->id)->update(
                [
                    'from_city_id'=>$data->city_id,
                    'from_province'=>$data->province_id,
                ]);

        $tRoute = \App\Models\Admin\BusRoutes::where('to_district_id',$data->id)->update(
                [
                    'to_city_id'=>$data->city_id,
                    'to_province'=>$data->province_id,
                ]);

    }
    public function updateFromTerminal($data){
        
        $fRoute = \App\Models\Admin\BusRoutes::where('from_terminal_id',$data->id)->update(
                [
                    'from_city_id'=>$data->city_id,
                    'from_district_id'=>$data->district_id,
                    'from_province'=>$data->province_id,
                ]);
        

        $tRoute = \App\Models\Admin\BusRoutes::where('to_terminal_id',$data->id)->update(
                [
                    'to_city_id'=>$data->city_id,
                    'to_district_id'=>$data->district_id,
                    'to_province'=>$data->province_id,
                ]);

    }

}
