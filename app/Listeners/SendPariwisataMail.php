<?php

namespace App\Listeners;

use App\Events\PariwisataBookSuccess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\mpdf\MPDF57;
class SendPariwisataMail {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(PariwisataBookSuccess $event) {
        $user_detail = $event->user_detail;
//        dd($user_detail);
        $user_detail['mail_subject'] = "pemesanan bus pariwisata(".$user_detail['subscriber_name'].")";
        $html = \Mail::send('site.e_pariwisata', array('data'=>$user_detail), function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject($user_detail['mail_subject']);
            $message->to("info@bustiket.com")->subject($user_detail['mail_subject']);
        });
    }

}
