<?php

namespace App\Listeners;

use App\Events\ForgotPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePasswordResetMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Signup  $event
     * @return void
     */
    public function handle(ForgotPassword $event)
    {
        
        $token = $event->user->generate_new_token(\Config::get("constant.ACCOUNT_FORGETPASS_TOKEN_TYPE"));
        $tokens = $event->user->tokens;
        $tokens[] = $token;
        $event->user->tokens = $tokens;
        $event->user->save();
        $user_detail = $event->user->toArray();
        $user_detail['activation_token'] = $user_detail['_id']."_".$token['token'];
        $user_detail['mail_subject'] = \Lang::get("success.forgetpassword_mail_subject");
        
        \Mail::send('emails.user.forget_password', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject(\Lang::get("success.forgetpassword_mail_subject"));
        });
    }
}
