<?php

namespace App\Listeners;

use App\Events\Signup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenrateAutoLoginToken
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Signup  $event
     * @return void
     */
    public function handle(Signup $event)
    {
//        $activation_token = [
//            'status' => 1, 
//            'type' => \Config::get("constant.ACCOUNT_ACTIVATION_TOKEN_TYPE"), 
//            'platform' => app("platform"), 
//            'token' => str_random(35),
//            "ip" => \Request::getClientIp(), 
//            "ua" => \Request::server("HTTP_USER_AGENT")
//        ];
//        
//        $event->user->token[] = $activation_token;
        
        $auth_token = $event->user->generate_new_token(\Config::get("constant.ACCOUNT_AUTH_TOKEN_TYPE"));
        $new_token = $event->user->tokens;
        $new_token[] = $auth_token;
        $event->user->tokens = $new_token;
        $event->user->save();
        \Log::info('Autogenerating login token for '.$event->user->_id);
        return $auth_token['token'];
    }
}
