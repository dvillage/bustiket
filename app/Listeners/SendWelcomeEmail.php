<?php

namespace App\Listeners;

use App\Events\Signup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Signup  $event
     * @return void
     */
    public function handle(Signup $event)
    {
        $user_detail = $event->user->toArray();
        $user_detail['mail_subject'] = \Lang::get("success.welcome_mail_subject", ['platform' => \Config::get("constant.PLATFORM_NAME")]);
        
        \Log::info("Sending welcome email.");
        $html = \Mail::send('emails.user.welcome', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject($user_detail['mail_subject']);
        });
        \Log::info("Welcome Email sent.");
    }
}
