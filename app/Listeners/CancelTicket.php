<?php

namespace App\Listeners;

use App\Events\TicketCancel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\mpdf\MPDF57;
class CancelTicket {

    use InteractsWithQueue;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(TicketCancel $event) {
        $ticket_detail = $event->ticket_detail;
//        dd($ticket_detail);
        $bb = $ticket_detail['book_by'];
        $user_id = $ticket_detail['user_id'];
        $sp_id = $ticket_detail['sp_id'];
        $booking_id = $ticket_detail['booking_id'];
        $total_amount = $ticket_detail['total_amount'];
        $total_seats = $ticket_detail['nos'];
        $status = $ticket_detail['status'];
        $hf = 0;
        if($bb == 0 || $bb == 1){
            $hf = 1;
        }
        
        $total_cancelled = isset($ticket_detail['total_cancelled']) ? $ticket_detail['total_cancelled'] : 0;
        if($status != 3){
            if(($total_cancelled != 0 && $total_cancelled == $total_seats) || $total_cancelled == 0){
            \App\Models\AdminCommission::delete_commission($booking_id);
            }else{
                \App\Models\AdminCommission::update_commission($booking_id,$total_cancelled,$total_seats);
            }
        }
        
        
        
        
        if(($bb == 5 || $bb == 6 || $bb == 7) && $status != 3){
            if($bb != 5){
                if(($total_cancelled != 0 && $total_cancelled == $total_seats) || $total_cancelled == 0){
                    $ss = \App\Models\SsCommission::delete_commission($booking_id, $user_id);
//                    dd($booking_id,$ss);
                    \App\Models\SsCommission::delete_commission($booking_id, $ss->parent_id);
                }else{
                    $ss = \App\Models\SsCommission::update_commission($booking_id,$total_cancelled,$total_seats,$user_id);
//                    dd($ss);
                    \App\Models\SsCommission::update_commission($booking_id,$total_cancelled,$total_seats,$ss->parent_id);
                }
                
            }
        }else if(($bb == 3 || $bb == 4) && $status != 3){
            if(($total_cancelled != 0 && $total_cancelled == $total_seats) || $total_cancelled == 0){
                \App\Models\SpCommission::delete_commission($booking_id, $user_id);
            }else{
                \App\Models\SpCommission::update_commission($booking_id,$total_cancelled,$total_seats,$user_id);
            }
            
        }
        
//        echo $bb;exit;
//        \App\Models\AdminCommission::admin_commission($sp_id, $booking_id, $total_amount, $total_seats, $bb,$user_id,$hf);
//        
//        if($bb == 5 || $bb == 6 || $bb == 7){
////            dd($bb);
//            if ($bb != 5) {
//                $fetchagent = \App\Models\SsCommission::ss_commission($user_id, $sp_id, $booking_id, $total_amount, $total_seats,1);
//                \App\Models\SsCommission::ss_commission($fetchagent['parent_id'], $sp_id, $booking_id, $total_amount, $total_seats);
//            }else{
//                \App\Models\SsCommission::ss_commission($user_id, $sp_id, $booking_id, $total_amount, $total_seats,1);
//            }
//        }else if($bb == 3 || $bb == 4){
//            \App\Models\SpCommission::sp_commission($user_id, $booking_id, $total_amount, $total_seats);
//        }
        
    }

}
