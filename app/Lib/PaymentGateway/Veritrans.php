<?php

namespace App\Lib\PaymentGateway;

class Veritrans {

    
    
    public static function get_redirection_url($param) {
        require_once(__DIR__ . '/../Veritrans.php');
        \Veritrans_Config::$serverKey = env("VERITRANS_SERVER_KEY");
        \Veritrans_Config::$isProduction = env("VERITRANS_PRODUCTION");
        try {
            $url = \Veritrans_VtWeb::getRedirectionUrl($param);
        } catch (\Exception $ex) {
            dd($ex);
            $payment_error_url = \URL::to("payment/error-view");
            return \Redirect::away($payment_error_url)->with("msg", "Something might wrong !!. Please contact customer care");
        }
        return \Redirect::away($url);
    }

    public static function verify_transaction($order_id) {
        require_once(__DIR__ . '/../Veritrans.php');
        $serverKey = base64_encode(env("VERITRANS_SERVER_KEY"). ':');
        \Veritrans_Config::$isProduction = env("VERITRANS_PRODUCTION");
        $url = \Veritrans_Config::getBaseUrl(). '/' . $order_id . '/status';

        $headers = array(
            'Accept:application/json',
            'Authorization: Basic ' . $serverKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        $res = \General::error_res("Invalid Transaction");
        $res['data'] = $result;
        if (\General::is_json($result)) {
            $arr = json_decode($result, true);
            if(isset($arr['status_code']) && isset($arr['transaction_status']) && $arr['status_code'] == 200 && $arr['transaction_status'] == "settlement")
            {
                $res = \General::success_res();
                $res['data'] = json_encode($arr);
            }
            return $res;
        }
        \Log::info('Veritranse Payment '.$result);
        return $res;
    }

}
