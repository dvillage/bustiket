<?php

namespace App\Lib\PaymentGateway;

class Doku {

    public static function verify_transaction($gateway_res,$req_type) {
        
        $res = \General::error_res("Invalid Transaction");
        $res['data'] = $gateway_res;
        

//        if($gateway_res['RESULTMSG'] != 'SUCCESS'){
        if($gateway_res['RESULTMSG'] != 'SUCCESS'){
            $res = \General::error_res("Trasaction Failed.");
            $res['data'] = $gateway_res;
            return $res;
        }
            
        if($req_type == 'notify'){
            if(!isset($gateway_res['RESULTMSG']) || $gateway_res['RESULTMSG'] != 'SUCCESS'){
                $res = \General::error_res("Trasaction Failed.");
                $res['data'] = $gateway_res;
                return $res;
            }
            $word_gen=  sha1($gateway_res['AMOUNT'].env('MALLID').env('SHAREDKEY').$gateway_res['TRANSIDMERCHANT'].$gateway_res['RESULTMSG'].$gateway_res['VERIFYSTATUS']);
            if($word_gen == $gateway_res['WORDS'])
            {
                $res = \General::success_res();
                $res['data'] = $gateway_res;
                return $res;
            }
        }else if($req_type == 'redirect'){
            
            $word_gen=  sha1($gateway_res['AMOUNT'].env('SHAREDKEY').$gateway_res['TRANSIDMERCHANT'].$gateway_res['STATUSCODE']);
            if($word_gen == $gateway_res['WORDS'])
            {
                $res = \General::success_res();
                $res['data'] = $gateway_res;
                return $res;
            }
        }
        
        
        
        \Log::info('Doku Payment '.json_encode($gateway_res));
        return $res;
        
    }

}
