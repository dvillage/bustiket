<?php

namespace App\lib;

class Validation {

    private static $rules = array(
        
        "user" => [
            "login" => [
                'u_email' => 'required|min:2|max:50',
                'u_pass' => 'required|min:2|max:25',
//                'type' => 'required|in:normal,facebook,google'
            ],
            "fblogin" => [
                'email' => 'required',
            ],
            "APIlogin" => [
                'user_email' => 'required|min:2|max:50',
                'user_password' => 'required|min:2|max:25',
                'type' => 'required|in:normal,facebook,google',
                'device_token' => 'required',
            ],
            "get_from_city_list" => [
                'key' => 'required',
            ],
            "get_to_city_list" => [
                'key' => 'required',
                'from_city' => 'required',
            ],
            "search_bus" => [
                'from_city' => 'required',
                'to_city' => 'required',
                'date' => 'required',
            ],
            "signup" => [
                'user_email' => 'required|email|unique:users,email|min:2|max:50',
                'user_password' => 'required|min:6|max:25',
                'device_token' => 'required',
            ],
            "dashboard" => [
                
            ],
            "update" => [
//                'user_id' => 'required',
            ],
            "change_password" => [
                'old_password' => 'required|min:2|max:25',
                'new_password' => 'required|min:2|max:25',
            ],
            "forget_pass" => [
                'user_email' => 'required|email',
            ],
            "reset_pass" => [
                'new_pass' => 'required',
                'cnew_pass' => 'required',
            ],
            "ticket_history" => [
                'type' => "required",
            ],
			'bus_search'=>[
                'from'=>'required',
                'to'=>'required',
                'date'=>'required',
                'nop'=>'required',
            ],
            'pariwisata_search'=>[
                'from_city'=>'required',
                'bus_type'=>'required',
                'date'=>'required',
            ],
        ],
        "service" => [
            "bus_map" => [
                'bus_id' => 'required',
                'date' => 'required',
                'from_id' => 'required|numeric',
                'to_id' => 'required|numeric',
                'sess_id' => 'required',
            ],
            "reserve_seat" => [
                'sess_id' => 'required',
                'bus_id' => 'required',
                'selected_seats' => 'required',
                'date' => 'required',
                'from_term_id' => 'required',
                'to_term_id' => 'required',
                'boarding_point' => 'required',
                'dropping_point' => 'required',
            ],
            "apply_coupon_code" => [
                'coupon_code' => 'required',
                'total_seat_count' => 'required',
                'total_seat_payment' => 'required',
                'date' => 'required',
                'bus_id' => 'required',
                
            ],
            "ticket_cancel" => [
                'ticket_id' => 'required',
            ],
            "book_ticket" => [
                'sess_id' => 'required',
                'bus_id' => 'required',
                'selected_seats' => 'required',
                'date' => 'required',
                'from_term_id' => 'required',
                'to_term_id' => 'required',
                'user_firstname' => 'required',
                'user_email' => 'required|email',
                'user_mobileno' => 'required',
                'user_age' => 'required',
//                'user_gender' => 'required|in:M,F',
                'user_gender' => 'required|in:L,P',
                'total_seats' => 'required',
//                'passenger' => 'required',
//                'coupon_code' => 'required',
                'boarding_time' => 'required',
                'departure_time' => 'required',
            ],
            "direct_book_ticket" => [
                'bus_id' => 'required',
                'seat_count' => 'required',
                'date' => 'required',
                'from_id' => 'required',
                'to_id' => 'required',
                'user_firstname' => 'required',
                'user_email' => 'required|email',
                'user_mobileno' => 'required',
                'user_age' => 'required',
//                'user_gender' => 'required|in:M,F',
                'user_gender' => 'required|in:L,P',
                'passenger' => 'required',
                'boarding_time' => 'required',
                'departure_time' => 'required',
            ],
            "book_reserve" => [
                'bus_id' => 'required',
                'total_seats' => 'required|numeric',
                'date' => 'required',
//                'from_term_id' => 'required',
//                'to_term_id' => 'required',
                'from_id' => 'required',
                'to_id' => 'required',
                'user_firstname' => 'required',
                'user_email' => 'required|email',
                'user_mobileno' => 'required',
                'user_age' => 'required|integer',
//                'user_gender' => 'required|in:M,F',
                'user_gender' => 'required|in:L,P',
                'passenger' => 'required',
                'boarding_time' => 'required',
                'departure_time' => 'required',
            ],
            "pay_by_bank" => [
               'bank_id' => 'required',
               'ticket_id' => 'required',
           ],
            
            "charge_cc" => [
               'price' => 'required',
               'token-id' => 'required',
               'ticket_id' => 'required',
           ],
           "ticket_detail" => [
               'ticket_id' => 'required',
           ],
           "ticket_by_unique_id" => [
               'unique_id' => 'required',
           ],
        ],
        
        "seat_seller" => [
            "login" => [
                'agent_email' => 'required|email|min:2|max:50',
                'agent_password' => 'required|min:2|max:25',
            ],
            "forget_pass" => [
                'agent_email' => 'required|email',
            ],
            "search_bus" => [
                'from_city' => 'required',
                'to_city' => 'required',
                'date' => 'required',
            ],
            "datewise_report" => [
                'from_date' => 'required',
            ],
            "update" => [
                'agent_id' => 'required',
                'agent_name' => 'required',
                'agent_mobile' => 'required',
            ],
        ],
        
        "admin" => [
            "login" => [
                'uname' => 'required|min:2|max:50',
                'password' => 'required|min:2|max:25',
            ],
            "update_bus_name" => [
                'id' => 'required',
                'name' => 'required'
            ],
            "get_terminals" => [
                'from_id' => 'required|integer',
                'to_id' => 'required|integer'
            ],
            "ss-deposit" => [
                'id' => 'required|integer',
                'type' => 'required|in:A,R,D'
            ],
            "add_bus" => [
                'bus_name' => 'required',
                'sp_id' => 'required',
                'bus_type' => 'required',
            ],
            "edit_bus" => [
                'bus_name' => 'required',
                'sp_id' => 'required',
            ],
            "driver" => [
                'name' => 'required',
                'email' => 'email',
                'mobile' => 'required',
                'proof_type' => 'required',
                'proof_number' => 'required',
            ],
            "pariwisata" => [
                'sp_id' => 'required',
                'city_id' => 'required',
                'bus_type' => 'required',
                'price' => 'required',
                'bus_img' => 'sometimes:required',
            ],
            "card" => [
                'name' => 'required',
                'price' => 'required',
                'charge' => 'required',
                'bank_name' => 'required',
                'card_image' => 'sometimes:required',
            ],
            "fav_route" => [
                'from_city_id' => 'required',
                'to_city_id' => 'required',
                'bus_id' => 'required',
            ],
            "testimonial"=>[
                'uname'=>'required',
                'email'=>'required',
                'message'=>'required',
                'image'=>'required',
            ],
            "policy" => [
                'sp_id' => 'required',
                'duration' => 'required',
                'time' => 'required',
                'amount' => 'required',
            ],
        ]
    );

    public static function get_rules($type, $rules_name) {
        if (isset(self::$rules[$type][$rules_name]))
            return self::$rules[$type][$rules_name];
        return array();
    }

}
