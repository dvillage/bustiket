<?php  namespace App\Lib;
use App\Lib\General;

class Image {
    /* sample image url : http://localhost/openroost/images/300x300/crop/center/valign/middle/align/center/avatar/consumer/28/1426047603.jpg
     * width : in ineger
     * height : in ineger
     * crop : center,stretch,none
     * valign : top,bottom,middle
     * align : left,right,center
     */
    
    public static $image_quality = 80;
//    public static $cache_dir_path = 8;
    public static $memory_allocation = "100M";
    public static function index($uri) {
        
        $uri_array = explode("/", $uri);
//        General::_e($uri_array);
        $file_name = $uri_array[count($uri_array)-1];
        $file_size = explode("x", $uri_array[0]);
        $file_size = isset($file_size[1]) ? $file_size : "original";
        $param = [];
        $param['width'] = $file_size == "original" ? 0 : $file_size[0];
        $param['height'] = $file_size == "original" ? 0 : $file_size[1];
        $param['crop'] = isset($uri_array[2]) ? $uri_array[2] : "center";
        $param['valign'] = isset($uri_array[4]) ? $uri_array[4] : "middle";
        $param['align'] = isset($uri_array[6]) ? $uri_array[6] : "center";
        
        
        $actual_path = substr($uri,strrpos($uri, "center/")+7);
        $dir_path = substr($actual_path,0,strrpos($actual_path, "/"));
        $param['image_path'] = public_path()."/assets/uploads/".$actual_path;
        $param['dir_path'] = public_path()."/assets/uploads/".$dir_path."/";
        $param['cache_path'] = $param['dir_path']."cache/";
//        General::_e($param,1);
        
        
        if (!self::validate_image_url($param)) {
            header('HTTP/1.1 400 Bad Request');
            echo 'Error: no image was specified';
            exit();
        }
//        dd($param);
        $width = $param['width'];
        $height = $param['height'];
        $crop = $param['crop'];
        $valign = $param['valign'];
        $align = $param['align'];
//        $image_path = implode("/",  array_slice($arg, 8)).".".$this->config->item("response_format");
        $image_path = $param['image_path'];
        $dir_path = $param['dir_path'];
        $cache_path = $param['cache_path'];
//        dd($param);
        return self::generate_image($width, $height, $crop, $image_path,$cache_path, $valign, $align);
    }

    public static function validate_image_url($param) {
        
        if (!isset($param['width']) || !is_numeric($param['width']) || !isset($param['height']) || !is_numeric($param['height'])) {
//            General::_e("0");
            return false;
        }
        if (!isset($param['crop']) || $param['crop'] == "") {
//            _e("2");
            return false;
        }


        if (!isset($param['image_path']) || $param['image_path'] == "") {
//            _e("7");
            return false;
        }

        if (!isset($param['dir_path']) || $param['dir_path'] == "") {
//            _e("8");
            return false;
        }

        if (!isset($param['cache_path']) || $param['cache_path'] == "") {
//            _e("9");
            return false;
        }
        return TRUE;
    }

    public static function GCD($a, $b) {
        while ($b != 0) {
            $remainder = $a % $b;
            $a = $b;
            $b = $remainder;
        }
        return abs($a);
    }

    public static function generate_image($width, $height, $crop, $image_path,$cache_path, $valign = "middle", $align = "center") {
        ob_start();
//        $image_path = IMAGE_DIR_PATH . "/" . $image_path;
        $image_name = substr($image_path, strrpos($image_path, "/") + 1);

        $original_extension = substr($image_name, strrpos($image_name, '.'));
        $apply_watermark = 0;
        $force_download = 0;
        $requested_width = $width;
        $requested_height = $height;
        if ($crop != 'crop') {
            $w = $width;
            $h = $height;
            $gcd = self::GCD($w, $h);
            if($gcd == 0)
            {
                $ratio = "1:1";
            }
            else
            {
                $a = $w / $gcd;
                $b = $h / $gcd;
                $ratio = $a . ":" . $b;
            }
            
        }

// Images must be local files, so for convenience we strip the domain if it's there
        $image = preg_replace('/^(s?f|ht)tps?:\/\/[^\/]+/i', '', (string) $image_path);

// For security, directories cannot contain ':', images cannot contain '..' or '<', and
// images must start with '/'
// If the image doesn't exist, or we haven't been told what it is, there's nothing
// that we can do
//        dd($image);
        if (!$image) {
            header('HTTP/1.1 404 Not Found');
            echo 'Error: image does not exist: ' . $image_name;
            exit();
        }
//        dd($image);
        if (!file_exists($image)) {
            header('HTTP/1.1 404 Not Found');
            echo 'Error: image does not exist!: ' . $image_name;
            exit();
        }

// Get the size and MIME type of the requested image
        $size = GetImageSize($image);
        $mime = $size['mime'];

// Make sure that the requested file is actually an image
        if (substr($mime, 0, 6) != 'image/') {
            header('HTTP/1.1 400 Bad Request');
            echo 'Error: requested file is not an accepted type: ' . $image_name;
            exit();
        }

        $width = $size[0];
        $height = $size[1];
        
        $originalwidth = $size[0];
        $originalheight = $size[1];
        $maxWidth = (isset($requested_width)) ? (int) $requested_width : 0;
        $maxHeight = (isset($requested_height)) ? (int) $requested_height : 0;

        $color = FALSE;

// If either a max width or max height are not specified, we default to something
// large so the unspecified dimension isn't a constraint on our resized image.
// If neither are specified but the color is, we aren't going to be resizing at
// all, just coloring.
        if ($maxWidth == 0 && $maxHeight) {
            $maxWidth = 99999999999999;
        } elseif ($maxWidth && $maxHeight == 0) {
            $maxHeight = 99999999999999;
        } elseif ($maxWidth == 0 && $maxHeight == 0) {
            $maxWidth = $width;
            $maxHeight = $height;
        }
// If we don't have a max width or max height, OR the image is smaller than both
// we do not want to resize it, so we simply output the original image and exit
        if (($maxWidth == 99999999999999 && $maxHeight == 99999999999999)) {
            $data = file_get_contents($image);
            $lastModifiedString = gmdate('D, d M Y H:i:s', filemtime($image)) . ' GMT';
            $etag = md5($data);

            self::doConditionalGet($etag, $lastModifiedString);

            header("Content-type: $mime");
            header('Content-Length: ' . strlen($data));
            echo $data;
            exit();
        }

// Ratio cropping
        $offsetX = 0;
        $offsetY = 0;
        
        if ($crop != 'none') {
            $cropRatio = explode(':', (string) $ratio);
            
            if (count($cropRatio) == 2) {
                $ratioComputed = $width / $height;
                $cropRatioComputed = (float) $cropRatio[0] / (float) $cropRatio[1];
                
                if ($ratioComputed < $cropRatioComputed) { // Image is too tall so we will crop the top and bottom
                    $origHeight = $height;
                    $height = $width / $cropRatioComputed;

                    if ($valign == 'top') {
                        $offsetY = 0;
                        $offsetwmY = ($origHeight - $height); // watermark
                    } else if ($valign == 'bottom') {
                        $offsetY = ($origHeight - $height);
                        $offsetwmY = 0; // watermark
                    } else {
                        $offsetY = ($origHeight - $height) / 2;
                        $offsetwmY = ($origHeight - $height) / 2; // watermark
                    }

                    $offsetwmX = 0;
                } else if ($ratioComputed > $cropRatioComputed) { // Image is too wide so we will crop off the left and right sides
                    $origWidth = $width;
                    $width = $height * $cropRatioComputed;

                    // Get the crop position and apply accordingly
                    if ($align == 'left') {
                        $offsetX = 0;
                        $offsetwmX = ($origWidth - $width); // watermark
                    } else if ($align == 'right') {
                        $offsetX = ($origWidth - $width);
                        $offsetwmX = 0; // watermark
                    } else {
                        $offsetX = ($origWidth - $width) / 2;
                        $offsetwmX = ($origWidth - $width) / 2; //watermark
                    }
                    $offsetwmY = 0;
                }
            }
        } else {
            $offsetwmX = 0;
            $offsetwmY = 0;
        }

// Setting up the ratios needed for resizing. We will compare these below to determine how to
// resize the image (based on height or based on width)
        $xRatio = $maxWidth / $width;
        $yRatio = $maxHeight / $height;

        if ($crop != 'stretch') {
            if ($xRatio * $height < $maxHeight) { // Resize the image based on width
                $tnHeight = ceil($xRatio * $height);
                $tnWidth = $maxWidth;
            } else { // Resize the image based on height
                $tnWidth = ceil($yRatio * $width);
                $tnHeight = $maxHeight;
            }
        } else {
            $tnWidth = $maxWidth;
            $tnHeight = $maxHeight;
        }
// Determine the quality of the output image
        $quality = self::$image_quality;
// Before we actually do any crazy resizing of the image, we want to make sure that we
// haven't already done this one at these dimensions. To the cache!
// Note, cache must be world-readable
// We store our cached image filenames as a hash of the dimensions and the original filename
        $resizedImageSource = $tnWidth . 'x' . $tnHeight . 'x' . $quality;

        $resizedImageSource .= 'x' . (string) $crop;

        $resizedImageSource .= 'x' . (string) $align . 'x' . (string) $valign;
        $resizedImageSource .= '-' . $image;

        $resizedImage = md5($resizedImageSource);

//        if (!file_exists($this->cache_dir_path . '/')) {
//            mkdir($this->cache_dir_path . '/');
//        }
        $resized = $cache_path . $resizedImage;

// Check the modified times of the cached file and the original file.
// If the original file is older than the cached file, then we simply serve up the cached file
        
        if (file_exists($resized)) {
     
            $imageModified = filemtime($image);
            $thumbModified = filemtime($resized);
            
            if ($imageModified < $thumbModified) {
                
                $data = file_get_contents($resized);

                $lastModifiedString = gmdate('D, d M Y H:i:s', $thumbModified) . ' GMT';
                
                $etag = md5($data);

                self::doConditionalGet($etag, $lastModifiedString);
                header("Content-type: $mime");
                header('Content-Length: ' . strlen($data));
                echo $data;
                exit();
            }
        }
        

// We don't want to run out of memory
        ini_set('memory_limit', self::$memory_allocation);
        
// Set up a blank canvas for our resized image (destination)
        $dst = imagecreatetruecolor($tnWidth, $tnHeight);

// Set up the appropriate image handling functions based on the original image's mime type
        switch ($size['mime']) {
            case 'image/gif':
                // We will be converting GIFs to PNGs to avoid transparency issues when resizing GIFs
                // This is maybe not the ideal solution, but IE6 can suck it
                $creationFunction = 'ImageCreateFromGif';
                $outputFunction = 'ImagePng';
                $mime = 'image/png'; // We need to convert GIFs to PNGs
                $doSharpen = FALSE;
                $quality = round(10 - ($quality / 10)); // We are converting the GIF to a PNG and PNG needs a compression level of 0 (no compression) through 9
                break;

            case 'image/x-png':
            case 'image/png':
                $creationFunction = 'ImageCreateFromPng';
                $outputFunction = 'ImagePng';
                $doSharpen = FALSE;
                $quality = round(10 - ($quality / 10)); // PNG needs a compression level of 0 (no compression) through 9
                break;

            default:
                $creationFunction = 'ImageCreateFromJpeg';
                $outputFunction = 'ImageJpeg';
                $doSharpen = FALSE;
                break;
        }

// Read in the original image
        $src = $creationFunction($image);


        if (in_array($size['mime'], array('image/gif', 'image/png'))) {
            if (!$color) {
                // If this is a GIF or a PNG, we need to set up transparency
                imagealphablending($dst, false);
                imagesavealpha($dst, true);
            } else {
                // Fill the background with the specified color for matting purposes
                if ($color[0] == '#')
                    $color = substr($color, 1);

                $background = FALSE;

                if (strlen($color) == 6)
                    $background = imagecolorallocate($dst, hexdec($color[0] . $color[1]), hexdec($color[2] . $color[3]), hexdec($color[4] . $color[5]));
                else if (strlen($color) == 3)
                    $background = imagecolorallocate($dst, hexdec($color[0] . $color[0]), hexdec($color[1] . $color[1]), hexdec($color[2] . $color[2]));
                if ($background)
                    imagefill($dst, 0, 0, $background);
            }
        }


// Resample the original image into the resized canvas we set up earlier
        ImageCopyResampled($dst, $src, 0, 0, $offsetX, $offsetY, $tnWidth, $tnHeight, $width, $height);

// Make sure the cache exists. If it doesn't, then create it
        if (!file_exists($cache_path))
            //mkdir($cache_path, 0777);
            \File::makeDirectory($cache_path, 0777, true);
 

// Make sure we can read and write the cache directory
        if (!is_readable($cache_path)) {
            header('HTTP/1.1 500 Internal Server Error');
            echo 'Error: the cache directory is not readable';
            exit();
        } else if (!is_writable($cache_path)) {
            header('HTTP/1.1 500 Internal Server Error');
            echo 'Error: the cache directory is not writable';
            exit();
        }

// Write the resized image to the cache
        $outputFunction($dst, $resized, $quality);

// Put the data of the resized image into a variable
//ob_start();
        $outputFunction($dst, null, $quality);
        $data = ob_get_contents();
        ob_end_clean();

// Clean up the memory
        ImageDestroy($src);
        ImageDestroy($dst);

// See if the browser already has the image
        $lastModifiedString = gmdate('D, d M Y H:i:s', filemtime($resized)) . ' GMT';
        $etag = md5($data);

        self::doConditionalGet($etag, $lastModifiedString);

// Send the image to the browser with some delicious headers
//        return Response::make($data, 200, array('content-type' => 'image/png'));
        return \Response::make($data, 200, array('content-type' => $mime));   
    }

    public static function doConditionalGet($etag, $lastModified) {
        header("Last-Modified: $lastModified");
        $expiry = gmdate('D, d M Y H:i:s', strtotime(date("Y-m-d H:i:s", time()) . " + 365 day")) . ' GMT';
        header("Expires: $expiry");
        header("Cache-Control: 	max-age=31536000, public");
        header("ETag: \"{$etag}\"");

        $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ?
                stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) :
                false;

        $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ?
                stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) :
                false;

        if (!$if_modified_since && !$if_none_match)
        {
           return;
        }

        if ($if_none_match && $if_none_match != $etag && $if_none_match != '"' . $etag . '"')
        {
            return; // etag is there but doesn't match
        }

        if ($if_modified_since && $if_modified_since != $lastModified)
        {
            return; // if-modified-since is there but doesn't match
        }
            
// Nothing has changed since their last request - serve a 304 and exit
        header('HTTP/1.1 304 Not Modified');
//        exit();
    }

}
