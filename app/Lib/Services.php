<?php namespace App\lib;

class Services{
 
    public static function get_auth_token($type)
    {
        
        $toeken = "";
        if($type == "admin")
            $toeken = \App\Models\Admin\Token::get_active_token("auth");
        else if($type == "staff")
            $toeken = \App\Models\Staff\Token::get_active_token("auth");
        else if($type == "customer")
            $toeken = \App\Models\Customer\Token::get_active_token("auth");
        
        return $toeken;
    }
    
    public static function validate_auth_token($auth_token)
    {
        if ($user_type == "admin")
        {
            $login = App\Models\Admin\User::validate_auth_token($auth_token);
            if (!$login)
                return \Response::json(\General::session_expire_res("unauthorise"));
        }
        else if ($user_type == "church_admin")
        {
            $login = App\Models\ChurchAdmin\User::validate_auth_token($auth_token);
            if (!$login)
                return \Response::json(\General::session_expire_res("unauthorise"));
        }
        else if ($user_type == "limited_admin")
        {
            $login = App\Models\LimitedAdmin\User::validate_auth_token($auth_token);
            if (!$login)
                return \Response::json(\General::session_expire_res("unauthorise"));
        }
        else if ($user_type == "f_admin")
        {
            $login = App\Models\FAdmin\User::validate_auth_token($auth_token);
            if (!$login)
                return \Response::json(\General::session_expire_res("unauthorise"));
        }
        else if ($user_type == "user")
        {
            $login = App\Models\Users\User::validate_auth_token($auth_token);
            if (!$login)
                return \Response::json(\General::session_expire_res("unauthorise"));
        }
    }
    public static function check_login()
    {
        if (\Auth::admin()->check())
            return "admin";
        else if (\Auth::merchant()->check())
            return "merchant";
        return FALSE;
    }
    
    public static function isAdminLogin() {
        if (\Auth::admin()->check())
            return TRUE;
        return FALSE;
    }
    
    public static function allowed_login($login_res_data)
    {
        
        $json = \General::error_res("invalid_email_password");
        $allowed_user_ids = config("constant.ALLOWED_USER_IDS");
//        \General::_e($login_res_data['userType']['id']);
//        dd(array_keys($allowed_user_ids));
        if(isset($login_res_data['userType']) && in_array($login_res_data['userType']['id'], array_keys($allowed_user_ids)))
        {
            $json = \General::success_res();
            $json['data']['user_type'] = $allowed_user_ids[$login_res_data['userType']['id']];
        }
        else
        {
            $json = \General::error_res("invalid_email_password");
        }
        return $json;
    }
    public static function send_sms($ticket_id){
        $booking = \App\Models\Bookings::whereHas('bookingSeats',function($q)use($ticket_id){
            $q->where('ticket_id',$ticket_id);
        })->with(['bookingSeats','bus'])->first();
//        dd($booking);
        $msg = "Your Ticket has been booked for ".$booking->from_city." to ".$booking->to_city." on Date ".$booking->journy_date.". Your seat(s) are ".$booking['bookingSeats'][0]['seat_lbl']." and Bus name ".$booking['bus']['name'].".";
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => array(
                'user' => 'footprint_api',
                ///'password' => 'footprints2015',
                'password' => 'footprint_api',
                'SMSText' => $msg,
                'GSM' => '62'.$booking->booker_mo
            )
        ));
        
        $res = \General::success_res('Message sent successfully.');
        
        $resp = curl_exec($curl);
        if (!$resp) {
        //die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));

            $msgAdmin = "There was an error in sending SMS to user.<br/> Error: ".curl_error($curl)." - Code: ".curl_errno($curl);

            $to      = 'info@bustiket.com';

            $subject = "SMS Not Sent" ;

//            $headers  = 'MIME-Version: 1.0' . "\r\n";
//
//            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//
//            $headers .= 'From:'.$website_name.'<'.$mail_url.'>' . "\r\n";
//
//            mail($to, $subject, $msgAdmin, $headers);
            \Mail::raw($msgAdmin, function ($message) use ($to,$subject) {
                $message->to($to)->subject($subject);
            });
            
            $res = \General::error_res('Message sending failed.');
        } 

        curl_close($curl);
        
        return $res;
    }
    
    public static function test_sms_send(){
        
        $msg = 'this is test message';
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => array(
                'user' => 'footprint_api',
                ///'password' => 'footprints2015',
                'password' => 'footprint_api',
                'SMSText' => $msg,
                'GSM' => '629876543210'
            )
        ));
        $resp = curl_exec($curl);
       $info = curl_getinfo($curl);
         //dd($info,$resp);
        
        curl_close($curl);
    }
    
}
