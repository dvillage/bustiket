<?php

namespace App\Lib\BNI;
use App\Lib\BNI\BniEnc;

class BNI {
    
    public static function creatVA_No($client_id,$mo){
        $mo_no = $mo;
        
//        $client_id  = env('BNI_CLIENT_ID');
        
        if(\Auth::guard('user')->check()){
            $user_data  = \Auth::guard('user')->user();
            
            if(strlen($user_data->mobile) >= 10){
                $mo_no  = $user_data->mobile;
            }
        }
        
        $mo_no = str_pad($mo_no,10,0, STR_PAD_LEFT);
//        $client_id = str_pad($client_id,5,0, STR_PAD_LEFT);
//        $rnd = str_pad(mt_rand(1, 99),2,0, STR_PAD_LEFT);

//        $va_no = '988'.$client_id.substr($mo_no,-8);    // Last 8 digit of Mobile no.
//        dd(strlen($client_id));
        if(strlen($client_id) == 5){
            $va_no = '988'.$client_id.substr($mo_no,0,8);       // First 8 digit of Mobile no.
        }
        else if(strlen($client_id) == 3){
            $va_no = '8'.$client_id.substr($mo_no,0,10).'62';       // First 10 digit of Mobile no.
        }
//        $va_no = '988'.$client_id.substr($mo_no,0,10);       // First 10 digit of Mobile no.
        
        
        
        $bank = \App\Models\BankDetails::where('bank_short_name', 'BNI')->first()->toArray();
        $d1 = date('Y-m-d H:i', strtotime(date('H:i').' -'.config('constant.BANK_TIME_LIMIT').' minutes'));
        
        $bni_tkt = \App\Models\Bookings::with(['paymentRecord'])
                                            ->whereHas('paymentRecord',function($q) use($bank,$va_no){
                                                $q->where(['payment_by'=>config("constant.PAYMENT_BY_BANK"),'bank_id'=>$bank['id'],'va_no'=>$va_no]);
                                            })
                                            ->where('status','!=',config("constant.STATUS_BOOKED"))
                                            ->where('payment_by',config("constant.PAYMENT_BY_BANK"))
                                            ->where('created_at','>=',$d1)
                                            ->get()->toArray();

        \Log::info($d1,$bni_tkt);

        if(count($bni_tkt) > 0){
            foreach($bni_tkt as $tkt){
                $bt = \App\Models\Bookings::where('booking_id',$tkt['booking_id'])->update(['status'=>config("constant.STATUS_CANCELLED")]);
                $r = self::updateInvoice($tkt,$va_no);
            }
        }

        return $va_no;
    }
    
    public static function createInvoice($param){
//        dd($param);
        $url        = env('BNI_URL');
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        
//        $client_id = str_pad($client_id,5,0, STR_PAD_LEFT);
        
        $dt = explode(':', $param['time_limit']);
        $exp_dt = $dt[0].'-'.$dt[1].'-'.$dt[2].' '.$dt[3].':'.$dt[4];
        
        
        
        $tkt_data['type']               = 'createbilling';
        $tkt_data['client_id']          = $client_id;
        $tkt_data['trx_id']             = $param['booking_id'];
        $tkt_data['trx_amount']         = $param['total_amount'];
        $tkt_data['billing_type']       = 'c';
        $tkt_data['customer_name']      = $param['booker_name'];
        $tkt_data['customer_email']     = $param['booker_email'];
        $tkt_data['customer_phone']     = $param['booker_mo'];
        $tkt_data['datetime_expired']   = date('c', strtotime($exp_dt));
        $tkt_data['description']        = 'Payment of Trx '.$param['booking_id'];
        
        $response_json = [];
        $response_json['status'] = '';
        $counter = 0;
        
        while($response_json['status'] !== '000' && $counter < 4){
            \Log::info('Counter : '.$counter);
            $counter++;
            $tkt_data['virtual_account']     = self::creatVA_No($client_id,$param['booker_mo']);
            
//            dd($tkt_data['virtual_account']);
            
            \Log::info('BNI Create Invoice tkt Data : '.json_encode($tkt_data));
            $hashed_string = BniEnc::encrypt( $tkt_data, $client_id, $secret_key );
    //        dd($hashed_string);

            $data = [
                'client_id' => $client_id,
                'data' => $hashed_string,
            ];



            \Log::info('BNI Create Invoice URL : '.$url);
            \Log::info('BNI Create Invoice Request : '.json_encode($data));

            //response from server : start
            $response = self::get_content($url, json_encode($data));
            //response from server : over

            // dummy response for testing : start
                //success res : start
    //                $data = [
    //                    'virtual_account'   => '8001000000000001',
    //                    'trx_id'            => $param['booking_id'],
    //                ];
    //                $res_string = BniEnc::encrypt( $data, $client_id, $secret_key );
    //                $response = [
    //                    'status'    => '000',
    //                    'data'      => $res_string,
    //                ];
    //                $response = json_encode($response);
                //success res : start

                // failed res : start
    //                $response = [
    //                    'status'    => '001',
    //                    'message'   => 'Incomplete / Invalid Parameter(s).',
    //                ];
    //                $response = json_encode($response);
                // failed res : over

            // dummy response for testing : over
    //        dd($response);        
            $response_json = json_decode($response, true);
            \Log::info('BNI Create Invoice Response : '.json_encode($response_json));
        }
//        dd($tkt_data);
        

        if ($response_json['status'] !== '000') {
            
            $res = \General::error_res();
            $res['data'] = $response_json;
            return $res;
        }

        else {
            $data_response = BniEnc::decrypt($response_json['data'], $client_id, $secret_key);
//            dd($data_response);
            \Log::info('BNI Create Invoice Decrypted Response : '.json_encode($data_response));
            $res = \General::success_res();
            $res['data'] = $data_response;
            return $res;
        }
    }
    
    public static function updateInvoice($param,$va_no){
        $url        = env('BNI_URL');
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        
        $tkt_data['type']               = 'updatebilling';
        $tkt_data['client_id']          = $client_id;
        $tkt_data['trx_id']             = $param['booking_id'];
        $tkt_data['trx_amount']         = $param['total_amount'];
        $tkt_data['customer_name']      = $param['booker_name'];
        $tkt_data['datetime_expired']   = date('c');
        
        $response_json = [];
        $response_json['status'] = '';
        
            
        \Log::info('BNI Update Invoice tkt Data : '.json_encode($tkt_data));
        $hashed_string = BniEnc::encrypt( $tkt_data, $client_id, $secret_key );
//        dd($hashed_string);

        $data = [
            'client_id' => $client_id,
            'data' => $hashed_string,
        ];

        \Log::info('BNI Update Invoice URL : '.$url);
        \Log::info('BNI Update Invoice Request : '.json_encode($data));

        //response from server : start
        $response = self::get_content($url, json_encode($data));
        //response from server : over

        // dummy response for testing : start
            //success res : start
//                $data = [
//                    'virtual_account'   => '8001000000000001',
//                    'trx_id'            => $param['booking_id'],
//                ];
//                $res_string = BniEnc::encrypt( $data, $client_id, $secret_key );
//                $response = [
//                    'status'    => '000',
//                    'data'      => $res_string,
//                ];
//                $response = json_encode($response);
            //success res : start

            // failed res : start
//                $response = [
//                    'status'    => '001',
//                    'message'   => 'Incomplete / Invalid Parameter(s).',
//                ];
//                $response = json_encode($response);
            // failed res : over

        // dummy response for testing : over
//        dd($response);        
        $response_json = json_decode($response, true);
        \Log::info('BNI Update Invoice Response : '.json_encode($response_json));

        if ($response_json['status'] !== '000') {
            
            $res = \General::error_res();
            $res['data'] = $response_json;
            return $res;
        }

        else {
            $data_response = BniEnc::decrypt($response_json['data'], $client_id, $secret_key);
//            dd($data_response);
            \Log::info('BNI Update Invoice Decrypted Response : '.json_encode($data_response));
            $res = \General::success_res();
            $res['data'] = $data_response;
            return $res;
        }
    }
    
    public static function inquireInvoice($param){
//        dd($param);
        $url        = env('BNI_URL');
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        
        $tkt_data['client_id']  = $client_id;
        $tkt_data['type']       = 'inquirybilling';
        $tkt_data['trx_id']     = $param['booking_id'];
        
        $hashed_string = BniEnc::encrypt( $tkt_data, $client_id, $secret_key );
//        dd($hashed_string);
    
        $data = [
            'client_id' => $client_id,
            'data' => $hashed_string,
        ];
        

        \Log::info('BNI Inquiry Invoice URL : '.$url);
        \Log::info('BNI Inquiry Invoice Request : '.json_encode($data));
        
        //response from server : start
        $response = self::get_content($url, json_encode($data));
        //response from server : over
        // dummy response for testing : start
            //success res : start
//                $data = [
//                    'client_id'             => $client_id,
//                    'trx_id'                => $param['booking_id'],
//                    'trx_amount'            => '10000',
//                    'virtual_account'       => '8001000000000001',
//                    'customer_name'         => 'Sagar',
//                    'customer_phone'        => '0258741369',
//                    'customer_email'        => 'sagar@mail.com',
//                    'datetime_created'      => '2016-02-0116:00:00',
//                    'datetime_expired'      => '2016-02-0116:00:00',
//                    'datetime_last_updated' => '2016-02-0116:00:00',
//                    'datetime_payment'      => '2016-02-0116:00:00',
//                    'payment_ntb'           => '023589',
//                    'payment_amount'        => '10000',
//                    'va_status'             => '2',
//                    'billing_type'          => 'c',
//                    'description'           => 'Payment of Trx '.$param['booking_id'],
//                    'datetime_created_iso8601'      => '2016-02-01T16:00:00+07:00',
//                    'datetime_expired_iso8601'      => '2016-02-01T16:00:00+07:00',
//                    'datetime_last_updated_iso8601' => '2016-02-01T16:00:00+07:00',
//                    'datetime_payment_iso8601'      => '2016-02-01T16:00:00+07:00',
//                ];
//                $res_string = BniEnc::encrypt( $data, $client_id, $secret_key );
//                $response = [
//                    'status'    => '000',
//                    'data'      => $res_string,
//                ];
//                $response = json_encode($response);
            //success res : start
            
            // failed res : start
//                $data = [
//                    'client_id'             => $client_id,
//                    'trx_id'                => $param['booking_id'],
//                    'trx_amount'            => '10000',
//                    'virtual_account'       => '8001000000000001',
//                    'customer_name'         => 'Sagar',
//                    'customer_phone'        => '0258741369',
//                    'customer_email'        => 'sagar@mail.com',
//                    'datetime_created'      => '2016-02-0116:00:00',
//                    'datetime_expired'      => '2016-02-0116:00:00',
//                    'datetime_last_updated' => '2016-02-0116:00:00',
//                    'datetime_payment'      => null,
//                    'payment_ntb'           => null,
//                    'payment_amount'        => '0',
//                    'va_status'             => '1',
//                    'billing_type'          => 'c',
//                    'description'           => 'Payment of Trx '.$param['booking_id'],
//                    'datetime_created_iso8601'      => '2016-02-01T16:00:00+07:00',
//                    'datetime_expired_iso8601'      => '2016-02-01T16:00:00+07:00',
//                    'datetime_last_updated_iso8601' => '2016-02-01T16:00:00+07:00',
//                    'datetime_payment_iso8601'      => null,
//                ];
                
//                $res_string = BniEnc::encrypt( $data, $client_id, $secret_key );
                
//                $response = [
//                    'status'    => '000',
//                    'data'      => $res_string,
//                ];
//                $response = json_encode($response);
            // failed res : over
            
        // dummy response for testing : over
        
        $response_json = json_decode($response, true);
//        dd($response_json);
        
        if ($response_json['status'] !== '000') {
            \Log::info('BNI Inquiry Invoice Response : '.json_encode($response_json));
            $res = \General::error_res();
            $res['data'] = $response_json;
            return $res;
        }

        else {
            $data_response = BniEnc::decrypt($response_json['data'], $client_id, $secret_key);
            
            \Log::info('BNI Inquiry Invoice Response : '.json_encode($response_json));
            
            if($data_response['payment_ntb'] == null){
                $res = \General::error_res();
                $res['data'] = json_encode($data_response);
                return $res;
            }
            
            $res = \General::success_res();
            $res['data'] = json_encode($data_response);
            return $res;
        }
    }
    
    public static function get_content($url, $post = '') {
	$usecookie = __DIR__ . "/cookie.txt";
	$header[] = 'Content-Type: application/json';
	$header[] = "Accept-Encoding: gzip, deflate";
	$header[] = "Cache-Control: max-age=0";
	$header[] = "Connection: keep-alive";
	$header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	// curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_ENCODING, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

	if ($post)
	{
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$rs = curl_exec($ch);

	if(empty($rs)){
//            var_dump($rs, curl_error($ch));
            curl_close($ch);
            return false;
	}
	curl_close($ch);
	return $rs;
    }
// URL for payment simulation: http://dev.bni-ecollection.com/

    public static function notification($param){
//        $data = file_get_contents('php://input');
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        
        $data = $param;
        $data_json = json_decode($data, true);
//        dd($data_json);
        if (!$data_json) {
                // handling a blank  response
            
            $res = \General::error_res('jangan iseng');
            $res['status'] = 999;
            
            return $res;
                
        }
        else {
            if ($data_json['client_id'] === $client_id) {
                $tkt_data = BniEnc::decrypt( $data_json['data'], $client_id, $secret_key );
//                dd($tkt_data);
                if (!$tkt_data) {
                        // handling if the server time is incorrect / incorrect or secret key is wrong
//                        echo '{"status":"999","message":"waktu server tidak sesuai NTP atau secret key salah."}';
                    $res = \General::error_res('waktu server tidak sesuai NTP atau secret key salah.');
                    $res['status'] = 999;

                    return $res;
                }
                else {
                        // insert original data into db
                        /* $tkt_data = [
                                'trx_id' => '', // please use the following parameters as reference number billing
                                'virtual_account' => '',
                                'customer_name' => '',
                                'trx_amount' => '',
                                'payment_amount' => '',
                                'cumulative_payment_amount' => '',
                                'payment_ntb' => '',
                                'datetime_payment' => '',
                                'datetime_payment_iso8601' => '',
                        ]; */
                        
                    $res = \General::success_res();
                    $res['status']  = 000;
                    $res['data']    = $tkt_data;
//                        $res['data'] = $tkt_data;

                    return $res;
                }
            }
        }
    }
    
    public static function BNIEncrypt($param){
        
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        if(isset($param['_token'])){
            unset($param['_token']);
        }
        
        $hashed_string = BniEnc::encrypt( $param, $client_id, $secret_key );
        $res = \General::success_res();
        $res['data'] = $hashed_string;
        
        return $res;
    }
    
    public static function BNIDecrypt($param){
        
        $client_id  = env('BNI_CLIENT_ID');
        $secret_key = env('BNI_SECRET_KEY');
        if(isset($param['_token'])){
            unset($param['_token']);
        }
        
        $tkt_data = BniEnc::decrypt( $param['string'], $client_id, $secret_key );;
        $res = \General::success_res();
        $res['data'] = $tkt_data;
        
        return $res;
    }
    
}
