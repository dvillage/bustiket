<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Signup' => [
            'App\Listeners\GenrateAutoLoginToken',
            'App\Listeners\RegisterSignupHistory',
            'App\Listeners\SendWelcomeEmail',
            'App\Listeners\SendVerificationEmail',
        ],
        'App\Events\ForgotPassword' => [
            'App\Listeners\GeneratePasswordResetMail',
        ],
        'App\Events\TicketBookByBank' => [
            'App\Listeners\SendBankTransferCodeEmail',
        ],
        'App\Events\TicketBookSuccess' => [
//            'App\Handlers\Events\SendETicketEmail',
            'App\Listeners\DownloadPdf',
            'App\Listeners\SendPushNotification',
        ],
        'App\Events\TicketCancel' => [
            'App\Listeners\CancelTicket',
        ],
        'App\Events\PariwisataBookSuccess' => [
            'App\Listeners\SendPariwisataMail',
        ],
        'App\Events\SetLocality' => [
            'App\Listeners\SetLocalityDetails',
        ],
        'App\Events\UpdateRoutes' => [
            'App\Listeners\UpdateRoutesListener',
        ],
        'App\Events\NewUserSignup' => [
//          'App\Handlers\Events\SendWelcomeEmail',
            'App\Listeners\SendVerificationEmail',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
