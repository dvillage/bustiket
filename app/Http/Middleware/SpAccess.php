<?php

namespace App\Http\Middleware;

use Closure;

class SpAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(config('constant.SP_TYPE') != 'Main'){
            return \Response::view('errors.404', array(), 404);
        }
        return $next($request);
    }

}
