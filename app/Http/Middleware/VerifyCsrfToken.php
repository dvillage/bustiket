<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "payment/doku/notify",
        "payment/doku/redirect",
        "payment/doku/review",
        "payment/doku/identify",
        'payment/midtrans/notify',
        'payment/midtrans/redirect',
        'payment/midtrans/cancel',
        'payment/midtrans/error',
        'bni/callback',
    ];

    public function handle($request, \Closure $next) {
//        dd(01);
//        dd($this->tokensMatch($request));
        if ($this->isReading($request) || $this->tokensMatch($request) || in_array($request->path(), $this->except)) {
            return $this->addCookieToResponse($request, $next($request));
        }
        if (\Request::wantsJson()) {
            return \Response::json(\General::request_token_expire_res(), 401);
        }


        throw new \Illuminate\Session\TokenMismatchException;
    }

}
