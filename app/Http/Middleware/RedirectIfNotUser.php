<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'user') {
        
        if (\Request::wantsJson()) {
            $token = \Request::header('AuthToken');
            if ($token == "") {
                return \Response::json(\General::session_expire_res(), 401);
            }
            $token = explode("_", $token);
            $user_id = $token[0];
            $token = isset($token[1]) ? $token[1] : "";
            
            $user = \App\Models\User::where("_id",$user_id)->where("tokens.token",$token)->first();
            
            if (is_null($user))
                return \Response::json(\General::session_expire_res("unauthorise"), 401);
            else
            {
                $user_data = $user->toArray();
                $user_data['avatar'] = \App\Models\User::get_image_url($user_data['_id'], $user_data['avatar']);
                $user_data['auth_token'] = $user_id."_".$token;
                app()->instance('logged_in_user', $user);
            }
        }
        else {
            if (!Auth::guard($guard)->check()) {
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("user/login")->withErrors($validator, 'login');
            }
            else
            {
                $user_data = \Auth::guard($guard)->user()->toArray();
                $ua = \Request::server("HTTP_USER_AGENT");
                $ip = \Request::server("REMOTE_ADDR");

                $user = \App\Models\User::find($user_data['_id']);
                $active_index = \General::array_indexof_assoc_search(["ua" => $ua,"ip" => $ip,"type" => config("constant.ACCOUNT_AUTH_TOKEN_TYPE")],$user->tokens);
                $user_data = $user->toArray();
                $user['avatar'] = \App\Models\User::get_image_url($user['_id'], $user['avatar']);
                $user_data['auth_token'] = $user->_id."_".$user->tokens['token'][$active_index]['token'];
                app()->instance('logged_in_user', $user_data);
            }
        }

        return $next($request);
    }

}
