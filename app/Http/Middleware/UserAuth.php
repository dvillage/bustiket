<?php

namespace App\Http\Middleware;

use Closure;

class UserAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
//        dd('In Middleware');
//        if(\Auth::guard('user')->check()){
//            
//        }
//        dd(\Request::header());
        
        if (\Request::wantsJson()) {
            $token = \Request::header('AuthToken');
            if ($token == "") {
                return \Response::json(\General::session_expire_res(),401);
            }
            $already_login = \App\Models\SeatSeller\Token::is_active(\Config::get("constant.AUTH_TOKEN_STATUS"),$token);
            if (!$already_login)
                return \Response::json(\General::session_expire_res("unauthorise"),401);
        }
        else {
            if (!\Auth::guard('user')->check()) {
//                dd(\Auth::guard('user')->check());
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("user")->withErrors($validator, 'login');
            }
        }
        return $next($request);
    }

}
