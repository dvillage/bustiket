<?php

namespace App\Http\Middleware;

use Closure;

class SpAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        \Config::set('constant.LOGGER','SP');
        if(\Auth::guard('sp')->check()){
            \Config::set('constant.CURRENT_LOGIN_ID',\Auth::guard('sp')->user()->id);
            \Config::set('constant.SP_TYPE',\Auth::guard('sp')->user()->type);
            \Config::set('constant.SP_PARENT',\Auth::guard('sp')->user()->parent_id);
            \Config::set('constant.SP_BALANCE',\Auth::guard('sp')->user()->balance);
            \Config::set('constant.SP_NAME',\Auth::guard('sp')->user()->first_name.' '.\Auth::guard('sp')->user()->last_name);
        }
        
        if (\Request::wantsJson()) {
            $token = \Request::header('AuthToken');
            if ($token == "") {
                return \Response::json(\General::session_expire_res(),401);
            }
            $already_login = \App\Models\SeatSeller\Token::is_active("auth",$token);
            if (!$already_login)
                return \Response::json(\General::session_expire_res("unauthorise"),401);
        }
        else {
            if (!\Auth::guard('sp')->check()) {
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("sp")->withErrors($validator, 'login');
            }
        }
        return $next($request);
    }

}
