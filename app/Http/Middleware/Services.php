<?php

namespace App\Http\Middleware;

use Closure;

class Services {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if(\Auth::guard('sp')->check()){
            \Config::set('constant.LOGGER','SP');
            \Config::set('constant.CURRENT_LOGIN_ID',\Auth::guard('sp')->user()->id);
            \Config::set('constant.SP_TYPE',\Auth::guard('sp')->user()->type);
            \Config::set('constant.SP_PARENT',\Auth::guard('sp')->user()->parent_id);
            \Config::set('constant.SP_BALANCE',\Auth::guard('sp')->user()->balance);
            \Config::set('constant.SP_NAME',\Auth::guard('sp')->user()->first_name.' '.\Auth::guard('sp')->user()->last_name);
        }elseif(\Auth::guard('ss')->check()){
            \Config::set('constant.LOGGER','SS');
            \Config::set('constant.CURRENT_LOGIN_ID',\Auth::guard('ss')->user()->id);
            \Config::set('constant.SS_TYPE',\Auth::guard('ss')->user()->type);
            \Config::set('constant.SS_PARENT',\Auth::guard('ss')->user()->parent_id);
            \Config::set('constant.SS_NAME',\Auth::guard('ss')->user()->name);
            \Config::set('constant.SS_BALANCE',\Auth::guard('ss')->user()->balance);
        }
        
        return $next($request);
    }

}
