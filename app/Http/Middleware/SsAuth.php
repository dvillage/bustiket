<?php

namespace App\Http\Middleware;

use Closure;

class SsAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        \Config::set('constant.LOGGER','SS');
        if(\Auth::guard('ss')->check()){
            \Config::set('constant.CURRENT_LOGIN_ID',\Auth::guard('ss')->user()->id);
            \Config::set('constant.SS_TYPE',\Auth::guard('ss')->user()->type);
            \Config::set('constant.SS_PARENT',\Auth::guard('ss')->user()->parent_id);
            \Config::set('constant.SS_NAME',\Auth::guard('ss')->user()->name);
            \Config::set('constant.SS_BALANCE',\Auth::guard('ss')->user()->balance);
        }
        if (\Request::wantsJson()) {
            $token = \Request::header('AuthToken');
            if ($token == "") {
                return \Response::json(\General::session_expire_res(),401);
            }
            $already_login = \App\Models\SeatSeller\Token::is_active("auth",$token);
            if (!$already_login)
                return \Response::json(\General::session_expire_res("unauthorise"),401);
        }
        else {
            if (!\Auth::guard('ss')->check()) {
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("ss")->withErrors($validator, 'login');
            }
        }
        return $next($request);
    }

}
