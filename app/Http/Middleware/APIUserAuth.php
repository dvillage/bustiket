<?php

namespace App\Http\Middleware;

use Closure;

class APIUserAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        $token = \Request::header('AuthToken');
        $res = \App\Models\Users::is_logged_in($token);
        if($res['flag'] != 1)
        {
            if (\Request::wantsJson()) {
                return \Response::json($res);
            }
            else
            {
                \Auth::guard('user')->logout();
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("user")->withErrors($validator, 'login');
            }
        }
 
//        if (\Request::wantsJson()) {
//            
//            $token = \Request::header('AuthToken');
//            
//            if ($token == "") {
//                return \Response::json(\General::session_expire_res());
//            }
//            $already_login = \App\Models\User\Token::is_active("auth",$token);
//            
//            if (!$already_login)
//                return \Response::json(\General::session_expire_res("unauthorise"));
//            else
//            {
//                $user = \App\Models\User\User::where("user_id",$already_login)->first()->toArray();
//                unset($user['user_password']);
//                $user['auth_token'] = $token;
//                app()->instance('logged_in_user', $user);
//            }
//        }
//        else {
//            if (!\Auth::user()->check()) {
//                \Auth::user()->logout();
//                $validator = \Validator::make([], []);
//                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
//                return redirect()->to("user")->withErrors($validator, 'login');
//            }
//            else
//            {
//                $user_data = \Auth::user()->user();
//                $user = [
//                    'user_id' => $user_data->user_id,
//                    'user_email' => $user_data->user_email,
//                    'user_firstname' => $user_data->user_firstname,
//                    'user_lastname' => $user_data->user_lastname,
//                    'user_gender' => $user_data->user_gender,
//                    'user_dob' => $user_data->user_dob,
//                    'user_landno' => $user_data->user_landno,
//                    'user_mobileno' => $user_data->user_mobileno,
//                    'user_maritalstatus' => $user_data->user_maritalstatus,
//                    'user_occupation' => $user_data->user_occupation,
//                    'user_address1_1' => $user_data->user_address1_1,
//                    'user_address1_2' => $user_data->user_address1_2,
//                    'user_address1_city' => $user_data->user_address1_city,
//                    'user_address1_state' => $user_data->user_address1_state,
//                    'user_address1_pin' => $user_data->user_address1_pin,
//                    'user_address1_country' => $user_data->user_address1_country,
//                    'user_address2_1' => $user_data->user_address2_1,
//                    'user_address2_2' => $user_data->user_address2_2,
//                    'user_address2_city' => $user_data->user_address2_city,
//                    'user_address2_state' => $user_data->user_address2_state,
//                    'user_address2_country' => $user_data->user_address2_country,
//                    'user_address2_pin' => $user_data->user_address2_pin,
//                    'user_typeID' => $user_data->user_typeID,
//                    'ip_addr' => $user_data->ip_addr,
//                    'date' => $user_data->date,
//                    'user_status' => $user_data->user_status,
//                    'fb_status' => $user_data->fb_status,
//                    'g_status' => $user_data->g_status,
//                    'updated_at' => $user_data->updated_at,
//                ];
//
//                $ua = \Request::server("HTTP_USER_AGENT");
//                $ip = \Request::server("REMOTE_ADDR");
//
//                $session = \App\Models\User\Token::active()->where("type",  config("constant.AUTH_TOKEN_STATUS"))->where("ua",$ua)->where("ip",$ip)->where("user_id",$user['id'])->first();
//                if(is_null($session))
//                {
//                    \Auth::user()->logout();
//                    $user['auth_token'] = "";
//                }
//                else
//                {
//                    $user['auth_token'] = $session['token'];
//                }
//                $user['auth_token'] = $session['token'];
//                app()->instance('logged_in_user', $user);
//            }
//        }
        return $next($request);
    }

}
