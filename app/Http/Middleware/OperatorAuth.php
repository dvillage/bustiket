<?php

namespace App\Http\Middleware;

use Closure;

class OperatorAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        
        $token = \Request::header('AuthToken');
//        dd($token);
        $res = \App\Models\Serviceprovider\ServiceProvider::is_logged_in($token);
//        dd($res);
        if($res['flag'] != 1)
        {
            if (\Request::wantsJson()) {
                return \Response::json($res);
            }
            else
            {
                \Auth::guard('sp')->logout();
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return redirect()->to("operator")->withErrors($validator, 'login');
            }
        }
        return $next($request);
    }

}
