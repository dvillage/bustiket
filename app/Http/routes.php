<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
if (!env('APP_DEBUG')) {
//    \Debugbar::disable();
}
\DB::setFetchMode(\PDO::FETCH_ASSOC);


/*Route::get('/', function () {
    return view('welcome');
});*/

Route::any('/bus-search',function(){
	return view('site.bus_search');
});
Route::any('/booker-detail',function(){
	return view('site.booker_detail');
});
Route::any('/order-review',function(){
	return view('site.order_review');
});
Route::any('/booked-by-bank',function(){
	return view('site.book_bybank');
});
Route::any('about_us.php',function(){
	return view('m.about');
});


Route::get('login','UserController@getLogin');
Route::post('login','UserController@postLogin');
Route::get('signup','UserController@getSignup');
Route::get('auth/google','UserController@getAuthGoogle');
Route::get('auth/google/redirect','UserController@redirectGoogle');
Route::get('auth/facebook','UserController@getAuthFacebook');
Route::get('auth/facebook/redirect','UserController@redirectFacebook');
Route::post('signup','UserController@postSignup');
Route::get('logout','UserController@getLogout');
Route::get('forgotpass','UserController@getForgotPass');
Route::post('forgotpass','UserController@postForgotPass');

Route::any('/','welcomeController@index');

//Route::any('search-bus','SearchController@searchBus');
//Route::any('api-payment/{id}','ServiceController@apiPayment');
Route::any('search-bus/{id}','SearchController@searchBus');
Route::any('search-pariwisata/{id}','SearchController@searchPariwisata');
Route::any('ticket-checkout','PaymentController@ticket_checkout');
Route::any('checkout-pariwisata','PaymentController@pariwisataCheckout');
Route::any('ticket-processing','PaymentController@ticket_process');
Route::any('pariwisata-processing','PaymentController@pariwisataProcess');
Route::any('pariwisata-success/{id}','PaymentController@pariwisataSuccess');
Route::any('ticket-confirmed','PaymentController@ticket_confirm');
Route::any('ticket-failed','PaymentController@ticket_failed');
Route::any('ticket-confirmed/{id}','PaymentController@ticket_confirm');

Route::any('payment/doku/notify','PaymentController@postDokuNotify');
Route::any('payment/doku/redirect','PaymentController@postDokuRedirect');
Route::any('payment/doku/review','PaymentController@postDokuReview');
Route::any('payment/doku/identify','PaymentController@postDokuIdentify');

Route::any('payment/midtrans/notify','PaymentController@postMidtransNotify');
Route::any('payment/midtrans/redirect','PaymentController@postMidtransredirect');
Route::any('payment/midtrans/cancel','PaymentController@postMidtransCancel');
Route::any('payment/midtrans/error','PaymentController@postMidtransError');

Route::any('bni/callback','PaymentController@postBniNotify');

Route::any('remove-blocked-seat','WelcomeController@removeBlockSeat');

Route::any('about','WelcomeController@aboutUs');
Route::any('from','WelcomeController@from');
Route::any('term-condition','WelcomeController@tnc');
Route::any('privacy-policy','WelcomeController@privacyPolicy');
Route::any('cancel-ticket','WelcomeController@cancelTicket');
Route::any('career','WelcomeController@career');
Route::any('request-operator','WelcomeController@requestOperator');
Route::any('company-bus','WelcomeController@companyBus');
Route::any('partner','WelcomeController@partnerBusticket');
Route::any('mail-send','WelcomeController@mailSend');
Route::any('send-mail-new-subscriber','WelcomeController@sendMailNewSubscriber');
Route::any('how-to-order','WelcomeController@howToOrder');
Route::any('check-booking','WelcomeController@checkBooking');
Route::any('payment-confirmation','WelcomeController@paymentConfirmation');
Route::any('cancel-ticket','WelcomeController@cancelTicket');
Route::any('faq','WelcomeController@faq');
Route::any('contact-us','WelcomeController@contactUs');
Route::any('api/v1/seatseller/doc','WelcomeController@doc');
Route::any('send-ticket/{id?}/{email?}','WelcomeController@sendTicketMail');
//Route::any('user/profile','userController@userProfile');
Route::any('admin/commission-management/{type}/{id}','AdminController@getCommissionManagement');
Route::any('city-list','WelcomeController@cityList');
Route::any('city-list-from/{cityname}','WelcomeController@cityListFrom');
Route::any('all-route','WelcomeController@allRoute');
Route::any('admin/ticket-list/{id?}/{type?}','AdminController@getTicketList');
Route::any('ss/ticket-list/{id?}/{type?}','SeatSellerController@getTicketList');
Route::any('sp/ticket-list/{id?}/{type?}','ServiceProviderController@getTicketList');

Route::controller('welcome','WelcomeController');
Route::controller('user','UserController');
Route::controller('services','ServiceController');
Route::controller('search','SearchController');
Route::controller('admin','AdminController');
Route::controller('ss','SeatSellerController');
Route::controller('sp','ServiceProviderController');
Route::controller('payment','PaymentController');
Route::controller('test','TestController');
Route::controller('migrate','MigrateController');

//Route::prefix('api')->group(function () {
//    Route::controller('services','APIServicesController');
//});
Route::group(["prefix" => "api","middleware" => "api"],function(){
    Route::controller('services','API\ServicesController');
    Route::controller('operator','API\OperatorController');
    Route::controller('seatseller','API\SeatSellerController');
    Route::controller('user','API\UserController');
    Route::controller('test','API\TestController');
    
    Route::controller('getHello','API\ServicesController');
});

