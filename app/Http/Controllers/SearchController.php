<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\User;

class SearchController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        
        $banners = \App\Models\Admin\Banners::active()->where('group','header')->get();
        
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [""],
                "css" => ["assets/css/app.min.css"],
            ],
            'body' => [
                'banners' => $banners
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("site.index",$view_data);
    }
    
    public function SearchBus($id = ''){
        $param = \Input::all();
        
        $type = [
            'P' => 'province',
            'C' => 'city',
            'D' => 'district',
            'T' => 'terminal',
        ];
        
//        dd($id);
        $arr = explode(".", $id);
        if( count($arr) < 6 ){
            return redirect('/');
        }
        $from = $arr[0];
        $to = $arr[2];
        $date = date("l, d F Y",  strtotime($arr[4]));
        $nop = $arr[5];
        $param = [
            'from'=>$from,
            'to'=>$to,
            'date'=>$arr[4],
            'nop'=>$nop,
        ];
        $header = \Request::header('accept');
//        $validator = \Validator::make($param, \Validation::get_rules("user", "bus_search"));
//        if ($validator->fails()) {
//            $messages = $validator->messages();
//            $error = $messages->all();
//            $json = \General::validation_error_res();
//            $json['data'] = $error;
//            $json['msg'] = $error[0];
//            return \Response::json($json, 200);
////            return view("errors.404");
//        }
        //http://localhost/bustiket_new/public/search-bus?from=jakarta-25-district&to=solo-26-city&date=2017-02-30&nop=2
//        $districts= \App\Models\Locality\District::get_locDist();
//        $city     = \App\Models\Locality\Citi::get_locCity();
//        $province = \App\Models\Locality\Province::get_locPro();
//        $terminal = \App\Models\Locality\Terminal::get_locTerm();
//        dd($districts,$city);
//        $r=[];
        
//        $locality = \App\Models\Locality::where('type','C')->orWhere('type','T')->get()->toArray();
        $locality = \App\Models\Locality::orderBy('type','ASC')->orderBy('name','ASC')->get()->toArray();
        $r = [];
        foreach($locality as $l){
//            $type = 'city';
//            if($l['type'] == 'T'){
//                $type = 'terminal';
//            }
            $d = [
                'id' => $l['lid'],
//                'type' => $type,
                'type' => $type[$l['type']],
                'name' => trim($l['name']),
            ];
            $r[] = $d;
        }
        
        $sesson_id = session()->getId();
        session(['session_id' => $sesson_id]);
        
//        foreach($province['data'] as $x){
//            $d=[
//                'id'=> $x['id'],
//                'type'=> 'province',
//                'name'=> trim($x['name'],' '),
//               ];
//            $r[]=  $d;
//            foreach($city['data'] as $y){
//                if($y['province_id'] == $x['id']){
//                    $d1=[
//                        'id'=> $y['id'],
//                        'type'=> 'city',
//                        'name'=> trim($y['name'],' '),
//                       ];
//                    $r[]=  $d1;
//                
//                    foreach($districts['data'] as $i){
//                        if($i['city_id'] == $y['id']){
//                            $d2=[
//                                'id'=> $i['id'],
//                                'type'=> 'district',
//                                'name'=> trim($i['name'],' '),
//                               ];
//                            $r[]=  $d2;
//                            foreach($terminal['data'] as $j){
//                                if($j['district_id'] == $i['id']){
//                                    $d3=[
//                                        'id'=> $j['id'],
//                                        'type'=> 'terminal',
//                                        'name'=> trim($j['name'],' '),
//                                       ];
//                                    $r[]=  $d3;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }

        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'param' =>  $id,
                'locations' => json_encode($r),
            ],
            'footer' => [
                "js" => ["assets/js/search.min.js","assets/js/re.search.min.js","assets/js/common.min.js","assets/js/parsley.min.js",'assets/js/jquery.inputmask.bundle.min.js'],
                "css" => []
            ],
        ];
        $device = app('device');
        if($device == 'mobile'){
            return view("m.search_page",$view_data);
        }
        return view("site.search_page",$view_data);
    }
    public function searchPariwisata($id = ''){
        $param = \Input::all();
        
//        dd($param);
        
        $arr = explode(".", $id);
        if( count($arr) < 4 ){
            return redirect('/');
        }
        $from = $arr[0];
        $type = $arr[2];
        $date = date("l, d F Y",  strtotime($arr[3]));
        $param = [
            'from_city'=>$from,
            'bus_type'=>$type,
            'date'=>$arr[3],
            'type'=>$arr[1],
        ];
        
        $header = \Request::header('accept');
        $validator = \Validator::make($param, \Validation::get_rules("user", "pariwisata_search"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return view("errors.404");
        }
        $districts=  \App\Models\Locality\District::get_locDist();
        $city= \App\Models\Locality\Citi::get_locCity();
//        dd($districts,$city);
        $r=[];
//        foreach($districts['data'] as $x){
//            $d=[
//                'id'=> $x['id'],
//                'type'=> 'district',
//                'name'=> $x['name'],
//               ];
//            $r[]=  $d;
//            $r[$x['name']]=$x['id'].'-district';
            foreach($city['data'] as $y){
//                if($y['district_id'] == $x['id']){
                    $d1=[
                        'id'=> $y['id'],
                        'type'=> 'city',
                        'name'=> $y['name'],
                       ];
                    $r[]=  $d1;
//                }
            }
//        }
        //http://localhost/bustiket_new/public/search-bus?from=jakarta-25-district&to=solo-26-city&date=2017-02-30&nop=2
        
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'param' =>  json_encode($param,true),
                'locations' => json_encode($r),
            ],
            'footer' => [
                "js" => ["assets/js/pariwisata.search.min.js","assets/js/re.search.min.js","assets/js/common.min.js","assets/js/parsley.min.js",'assets/js/jquery.inputmask.bundle.min.js'],//"assets/js/search.min.js",
                "css" => []
            ],
        ];
        $device = app('device');
//        if($device == 'mobile'){
//            return view("m.search_page",$view_data);
//        }
        return view("site.search_pariwisata",$view_data);
    }
    
    public function postGetSeatMap(){
        $param = \Input::all();
        $l_id = 1;
        $layout = \App\Models\BusLayout::find($l_id)->toArray();
        $data = \App\Models\SeatMap::where("layout_id", $layout['id'])->orderBy("row", "ASC")->orderBy("col", "ASC")->get()->toArray();
        $seats = \App\Models\SeatMap::prepare_seatmap_array($data,$layout['rows'],$layout['columns']);
       // dd($seats);
        $view_data = [
            'seats' =>  $seats,
        ];
        return view("site.seat_map",$view_data);
    }
    

    public function postBusSearch(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "bus_search"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $header = \Request::header('accept');
//        echo $header;
//        print_r($header);
        $res = \General::success_res();
        $view_data = [
                  'body'=>[ 'message' => 'No Bus Found.Try Again.' ],
              ];
        return $res;
    }
}
