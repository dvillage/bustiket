<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class MigrateController extends Controller {

    public function __construct() {
        \Config::set('database.default', 'old');
    }
    
    public function getCouponcode($page = 0){
        $old_coupon =  \DB::table('bus_coupons')->skip($page*1000)->take(1000)->get();
//        dd($old_coupon);
        if(count($old_coupon) == 0){
            $res = \General::error_res('No Data Found.');
            return $res;
        }
        \Config::set('database.default', 'mysql');

        $migrate_coupon = \App\Models\Migrate::migrate_coupon($old_coupon);
        $res = \General::success_res();
        $res['coupon_msg'] = $migrate_coupon['data'];
        return $res;

    }
    public function getPromocode($page = 0){
        $old_promo =  \DB::table('bus_promo')->skip($page*1000)->take(1000)->get();
        
        if(count($old_promo) == 0){
            $res = \General::error_res('No Data Found.');
            return $res;
        }
        
        \Config::set('database.default', 'mysql');

        $migrate_promo = \App\Models\Migrate::migrate_promo($old_promo);
        $res = \General::success_res();
        $res['promo_msg'] = $migrate_promo['data'];
        return $res;

    }
    public function getSp(){
        $old_sp =  \DB::table('serviceprovider_info as a')->orderBy('SP_id','asc')->leftJoin('serviceprovider_info as b', function($join) {
                                $join->on('a.Bus_Admin_Parent', '=', 'b.SP_id');
                              })->select('a.*','b.SP_email as parent_email')->get();
        \Config::set('database.default', 'mysql');
        
        $migrate = \App\Models\Migrate::migrate_sp($old_sp);
        return $migrate;
    }
    public function getDistrict(){
        \Config::set('database.default', 'mysql');
        $migrate = \App\Models\Migrate::migrate_district();
        return $migrate;
    }
    public function getCity(){
        
        $old_city =\DB::table('cities')->orderBy('id','asc')->get();
        \Config::set('database.default', 'mysql');
        
        $migrate = \App\Models\Migrate::migrate_city($old_city);
        return $migrate;
    }
    public function getTerminal(){
        
        $old_terminal =  \DB::table('board_points as a')->orderBy('board_id','asc')->leftJoin('cities as b', function($join) {
                                $join->on('a.board_city', '=', 'b.id');
                              })->select('a.*','b.city_name as city_name')->get();
        \Config::set('database.default', 'mysql');
        
        $migrate = \App\Models\Migrate::migrate_terminal($old_terminal);
        return $migrate;
    }
    public function getAmenity(){
        $old_amenity =  \DB::table('bus_luxitem')->orderBy('lux_id','asc')->get();
        
        \Config::set('database.default', 'mysql');
        $migrate = \App\Models\Migrate::migrate_amenity($old_amenity);
        return $migrate;
    }

    public function getBus(){
        $old_bus =  \DB::table('businfo as a')->orderBy('Bus_id','asc')->leftJoin('parentbus as b', function($join) {
                                $join->on('a.parbus', '=', 'b.pid');
                              })
                              ->leftJoin('serviceprovider_info as c', function($join) {
                                $join->on('a.SP_id', '=', 'c.SP_id');
                              })
                              ->leftJoin('busstructuretypes as d', function($join) {
                                $join->on('a.Bus_structure', '=', 'd.structureID');
                              })
                              ->leftJoin('bustypes as e', function($join) {
                                $join->on('a.Bus_Type', '=', 'e.typeID');
                              })
                             ->select('a.*','b.par_name as par_name','c.sp_email','d.position','e.typeName')->get();
                              
//                      dd($old_bus);
        $migrate = \App\Models\Migrate::migrate_bus($old_bus);
        return $migrate;
    }
    public function getLocality(){
        \Config::set('database.default', 'mysql');
        $migrate = \App\Models\Migrate::migrate_locality();
        return $migrate;
    }
    
    public function getAmenitiesId(){
        \Config::set('database.default', 'mysql');
        $migrate = \App\Models\Migrate::migrate_amenity_id();
        return $migrate;
    }
    
    public function getAmenitiesImg(){
        \Config::set('database.default', 'mysql');
        $migrate = \App\Models\Migrate::migrate_amenity_img();
        return $migrate;
    }
    
    public function getUser($pageNo){
        $len = 100;
        $start = ($pageNo-1) * $len;
        $old_user=\DB::table('users')->skip($start)->take($len)->get();
        if(count($old_user)<1){
            return "No More User Found";
        }
        $migrate=\App\Models\Migrate::migrate_user($old_user);
        return $migrate;
    }
    
    public function getSsinfo($pageNo){
        if($pageNo<1){
            echo "Enter paramater should greater or equal 1";
            exit();
        }
        else if(is_numeric($pageNo)){
            $len=1000;
            $offRow=($pageNo-1)*$len;
        }else{
            echo "Enter paramater should numeric";
            exit();
        }
        
        \Config::set('database.default','mysql');
        $newEmail=\DB::table('ss')->select('email')->get();
        \Config::set('database.default', 'old');
        $newarray=array();
        $ssType = [0,4,5];
        $oldSS=\DB::table('seatseller_info as a')->orderBy('agent_id','asc')->whereIn('a.agent_type',$ssType)->leftJoin('seatseller_info as b', function($join) {
                                $join->on('a.bus_admin_parent', '=', 'b.agent_id');
                              })->select('a.*','b.agent_email as parent_email')->offset($offRow)->limit($len)->get();
        if(count($oldSS)<1){
            return "No Seat Seller data  Found";
        }
//        dd($oldSS);
        $migrate=\App\Models\Migrate::migrate_ssinfo($oldSS,$newEmail);
        return $migrate;
    }
    
    public function getUpdateRoute($pageNo){
        $len = 500;
//        $len = 5;
        $start = ($pageNo-1) * $len;
        \Config::set('database.default','mysql');
        $all_term=\DB::table('terminals')->skip($start)->take($len)->get();
        if(count($all_term)<1){
            return "No More Terminal Found";
        }
        $migrate=\App\Models\Migrate::update_route($all_term);
        return $migrate;
    }
    
    public function getCityList($pageNo = 0){
        if($pageNo<1){
            echo "Enter paramater should greater or equal 1";
            exit();
        }
        else if(is_numeric($pageNo)){
            $len = 500;
    //        $len = 5;
            $start = ($pageNo-1) * $len;
        }else{
            echo "Enter paramater should numeric";
            exit();
        }
        
        \Config::set('database.default','mysql');
//        $all_term=\DB::table('terminals')->skip($start)->take($len)->get();
        $all_term=  \App\Models\Locality\Terminal::with(['locDistrict','locProvince'])->with(array('locCiti' => function($query) {
                    $query->orderBy('name', 'asc');
                }))->orderBy('city_id','asc')->skip($start)->take($len)->get();
        if(count($all_term)<1){
            return "No More Terminal Found";
        }
//        dd($all_term);
        $migrate=\App\Models\Migrate::all_city($all_term);
        $filename = 'city list_'.$pageNo.'.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $f = fopen('php://output', 'w');
        foreach($migrate as $m){
            fputcsv($f, $m);
        }
        
//        return $migrate;
    }
    
    public function getRodaCityList(){
        \Config::set('database.default','mysql');
        $cities = \App\Models\Migrate::roda_cities();
        $filename = 'roda_city list.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $f = fopen('php://output', 'w');
        foreach($cities as $m){
            fputcsv($f, $m);
        }
    }
    public function getRodaTerminal(){
        \Config::set('database.default','mysql');
        $path = config('constant.UPLOAD_DIR_PATH').'roda_terminals.csv';
        if(!file_exists($path)){
            exit('no csv file exist with roda terminals');
        }
        
        $migrate = \App\Models\Migrate::roda_terminals($path);
        return $migrate;
    }
    public function getLorenaCityList(){
        \Config::set('database.default','mysql');
        $cities = \App\Models\Migrate::lorena_cities();
        $filename = 'lorena_city_list.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $f = fopen('php://output', 'w');
        foreach($cities as $m){
            fputcsv($f, $m);
        }
    }
}
