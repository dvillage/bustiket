<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\SeatSeller;
use App\Models\Serviceprovider\ServiceProvider;

class ServiceProviderController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin'];
    private static $bypass_url_for_sp = ['getIndex', 'getLogin', 'postLogin','getDashboard','getLogout','getProfile','postChangeAdminPassword','getTicketList','getTicketBookNew'
        ,'getTicketDetails','postDelTicket','postApproveTicket','postTlistFilter','postSendEmail','postSendSms','postCancelTicket','postTicketCheckout','getPaymentRequest','postPaymentRequestFilter'
        ,'postNewPaymentRequest','getCouponCode','postCouponFilter','postEditServiceProvider','getCommissionManagement','postCommlistFilter'];
    private static $logger = '';

    public function __construct() {
        $this->middleware('sp', ['except' => self::$bypass_url]);
        
        $this->middleware('spAccess', ['except' => self::$bypass_url_for_sp]);
        self::$logger = config('constant.LOGGER');
    }

    public function getIndex() {
        if (!\Auth::guard('sp')->check()) {
//            return \Response::view('errors.401', array(), 401);
            return \Redirect::to('/');
        }

        return \Redirect::to('sp/dashboard');
    }

    public function getDashboard() {
//        exit;
        
        if (!\Auth::guard('sp')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Dashboard | Service Provider Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'Dashboard',
                'logger'=> 'SP'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.sp_dashboard", $view_data);
    }

    
    public function getLogin($sec_token = "") {

        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        if ($sec_token != $s['login_url_token']) {
            return \Response::view('errors.404', array(), 404);
        }

        if (\Auth::guard("sp")->check()) {
            return \Redirect::to("sp/dashboard");
        }
        $view_data = [
            'body'=> [
                'logger' => 'Service Provider',
                'type' => 'SP'
            ]
        ];
        return view('admin.login',$view_data);
    }

    public function postLogin(Request $req) {
        $view_data = [
            'body'=> [
                'logger' => 'Service Provider',
                'type' => 'SP'
            ]
        ];
        
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "login"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            
            return view('admin.login',$view_data)->withErrors($validator);
        }
        $param = $req->input();
        $res = ServiceProvider::doLogin($param);
        if ($res['flag'] == 0) {
            return view('admin.login',$view_data)->withErrors('Wrong User Id or Password !!');
//            return \Redirect::to('/');
        }
//        dd($res);
        return \Redirect::to("sp/dashboard");
    }

    public function getLogout() {

        \App\Models\Serviceprovider\Token::delete_token();
        \Auth::guard('sp')->logout();
        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        return redirect("/sp/login/" . $s['login_url_token']);
    }

    public function getProfile($msg = "") {
        $sp_id = config('constant.CURRENT_LOGIN_ID');
        $type = config('constant.SP_TYPE');
        $param['id']=$sp_id;
        $param['type']=$type;
        $details= \App\Models\Serviceprovider\ServiceProvider::providers_details($param);
        
        $data=self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Profile | Service Provider Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'data' => $details['data'][0],
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.sp_profile", $view_data);
    }

    public function postChangeAdminPassword() {
        $param = \Input::all();
//        dd($param);
        $res = ServiceProvider::change_admin_password($param);
//        dd($res);
        if (isset($res['flag'])) {
            if ($res['flag'] == 0) {
                return \Redirect::to('sp/profile/' . $res['msg']);
            } else if ($res['flag'] == 1) {
//                return \Redirect::to("admin/dashboard");
                return \Redirect::to("sp/logout");
            }
        }
    }

    public function getServiceProvider($type = '',$id = 0) {
        if($type != 'A' && $type != 'B' && $type != 'D' && $type != 'buses'){
            return \Response::view('errors.404', array(), 404);
        }
        $param['type'] = $type;
        $param['start'] = 0;
        $param['len'] = 5;

        $view_data = [
            'header' => [
                "title" => 'Service Provider | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' || $type == 'buses' ? 'main' : $type),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        if($type == 'buses'){
            if($id > 0){
                
                $view_data['header']['js'] = [
                    \URL::to('assets/ckeditor/ckeditor.js'),
                    
                ];
                
                $allSp = ServiceProvider::active()->where('type','Main')->get();
                $buses = ServiceProvider::with('spBuses')->where('id',$id)->first();
                $view_data['body']['buses'] = $buses;
                $view_data['body']['all_sp'] = $allSp;
                return view("admin.buses_list", $view_data);
            }else{
                return \Response::view('errors.404', array(), 404);
            }
        }elseif($type=='D'){
            return view("admin.service_provider_deposit", $view_data);
        }else{
            return view("admin.sp_main", $view_data);
        }
    }

    public function postSpFilter() {
        $param = \Input::all();
        $data = ServiceProvider::get_sp_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $data['type'] = $param['type'];
            return view("admin.sp_filter", $data);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function getAddProvider($type = '') {
        $data=self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Add Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        $t = $type != '' ? '_t' : '';
        return view("admin.add_provider" . $t, $view_data);
    }

    public function getUpdateProvider($type,$id) {
        $param['id']=$id;
        $param['type']=$type;
        
        $checkParent= ServiceProvider::check_parent($id,config('constant.CURRENT_LOGIN_ID'));
        if(!$checkParent){
            return \Response::view('errors.404', array(), 404);
        }
        $details= ServiceProvider::providers_details($param);
        
        $data=self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Update Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'data' => $details['data'][0],
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        
        return view("admin.providers_detail", $view_data);
    }
    public function postEditServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'email'=>'unique:sp,email,'.$param['spid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
//            return \Response::json($json, 200);
//            dd($validator);
//            return \Redirect::back()->withErrors($validator);
//            session()->flash('message', "The email has already been taken.");
//            dd(session());
            return redirect()->back()->with('message', "The email has already been taken.");
        }
//        dd($param);
        $res = ServiceProvider::edit_sp($param);
        if ($res['flag'] == 1) {
            if(isset($param['profile'])){
                return \Redirect::to('sp/profile');
            }
            if ($param['type'] == 'Main') {
                return \Redirect::to('sp/service-provider');
            }
            return \Redirect::to('sp/service-provider/'.$param['type']);
        }
    }
    
    public function postAddServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'email'=>'unique:sp,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return \Redirect::back()->withErrors($validator)->withInput();
        }
//        dd($param);
        $res = ServiceProvider::save_sp($param);
//        return $res;
        if ($res['flag'] == 1) {
            if ($param['type'] == 'Main') {
                return \Redirect::to('sp/service-provider');
            }
            return \Redirect::to('sp/service-provider/'.$param['type']);
        }
    }

    public function postGetMainProvider() {
        $res = \General::success_res();
        $data = ServiceProvider::get_sp();
        $res['data'] = $data;
        return $res;
    }

    public function postDelServiceProvider() {
        $param = \Input::all();
        $data = ServiceProvider::delete_ServiceProvider($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postUpdateBusName(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "update_bus_name"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\Serviceprovider\Buses::update_bus_name(\Input::all());
        return \Response::json($res, 200);
    }
    public function postAddBus(){
        $param = \Input::all();
//        print_r($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "add_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $addBus = \App\Models\Serviceprovider\Buses::add_new_bus($param);
//        $res = \General::success_res();
//        $res['data'] = $param;
        return \Response::json($addBus, 200);
    }
    public function getBuses(){
        $param['type'] = '';
        $param['start'] = 0;
        $param['len'] = 5;

        $view_data = [
            'header' => [
                "title" => 'Service Provider | Service Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => '',
                'id' => 'bus',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        $id = config('constant.CURRENT_LOGIN_ID');
            if($id > 0){
                
                $view_data['header']['js'] = [
                    \URL::to('assets/ckeditor/ckeditor.js'),
                    
                ];
                
                $allSp = ServiceProvider::active()->where('type','Main')->get();
                $buses = ServiceProvider::with('spBuses')->where('id',$id)->first();
                $view_data['body']['buses'] = $buses;
                $view_data['body']['all_sp'] = $allSp;
                return view("admin.buses_list", $view_data);
            }else{
                return \Response::view('errors.404', array(), 404);
            }
        
    }
    public function postGetBus(){
        $param = \Input::all();
//        $bus = \App\Models\Serviceprovider\Buses::where('id',$param['id'])->with('sp')->first();
        $bus = \App\Models\Serviceprovider\Buses::where('id',$param['id'])->with(['sp','amenities','busLayout.seats','busPrice','busRoutes','drivers.driver','couponCode'])->first()->toArray();
        $routes = \App\Models\Admin\BusRoutes::where('bus_id',$param['id'])->with(['fromCity','toCity','fromTerminal','toTerminal'])->orderBy('from_city_id','asc')->orderBy('to_city_id','asc')->get();
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'sp-main',
                'bus'=>$bus,
                'routes'=>$routes,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.bus_detail", $view_data);
    }
    public function getBusStructure( $bus_id = 0,$sp_id = 0){
        if(!(int)$bus_id || !(int)$sp_id){
            return \Response::view('errors.404', array(), 404);
        }
        
        if(config('constant.LOGGER') == 'SP' && config('constant.CURRENT_LOGIN_ID') != $sp_id){
            return \Response::view('errors.404', array(), 404);
        }
        
        $bus = \App\Models\Serviceprovider\Buses::where('id',$bus_id)->with('amenities')->with('busPrice')->with('blockDates')->with('busRoutes')->with('busLayout')->with('couponCode')->first();
        $buses = ServiceProvider::with('spBuses')->where('id',$sp_id)->first();
        $cities = \App\Models\Locality\Citi::orderBy('name','asc')->lists('name','id');
        $dist = \App\Models\Locality\District::orderBy('name','asc')->lists('name','id');
        $routes = \App\Models\Admin\BusRoutes::where('bus_id',$bus_id)->with(['fromCity','toCity','fromTerminal','fromDistrict','toDistrict','toTerminal','prices'])->orderBy('id','asc')->get();
        $lay = \App\Models\BusLayout::where('bus_id',$bus_id)->first();
        $drivers = \App\Models\BusDrivers::where('bus_id',$bus_id)->with('driver')->get();
        
        if(!is_null($lay) && $bus['layout']){
            $layout = \App\Models\SeatMap::where('layout_id',$lay->id)->get()->toArray();
            $layout = json_encode($layout,true);
        }else{
            $layout = '';
        }
        
//        print_r($layout);
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => ['jquery.tagsinput.js'
//                    ,'jquery-ui.js'
                    ,'parsley.min.js'
                    ,'jquery.inputmask.bundle.min.js'
                    ,'ThrowPropsPlugin.min.js','TweenLite.min.js','CSSPlugin.min.js','Draggable.min.js'
                    ,\URL::to('assets/ckeditor/ckeditor.js')
                    ],
                "css" => ['jquery.tagsinput.css'
//                    ,'jquery-ui.css'
                    ],
            ],
            'body' => [
                'id' => 'bus',
                'bus'=>$bus,
                'all_bus'=>$buses,
                'city'=>$cities,
                'dist'=>$dist,
                'routes'=>$routes,
                'drivers'=>$drivers,
                'layout'=>$layout
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.bus_structure", $view_data);
    }
    
    public function postGetTerminals(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "get_terminals"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $param = \Input::all();
        $ter_from = \App\Models\Locality\Terminal::get_terminal_from_city($param['from_id']);
        $ter_f = $ter_from->toArray();
        if(count($ter_f) == 0){
            return \General::error_res('no terminal found for from city');
        }
        $ter_to = \App\Models\Locality\Terminal::get_terminal_from_city($param['to_id']);
        $ter_t = $ter_to->toArray();
        if(count($ter_t) == 0){
            return \General::error_res('no terminal found for to city');
        }
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'from'=>$ter_from,
                'to'=>$ter_to,
                'param'=>$param
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.bus_terminals", $view_data);
    }
    
    public function postGetAmenites(){
        $param = \Input::all();
        $am = \App\Models\Admin\Amenities::get_amenities($param);
        if(is_null($am)){
            return \General::error_res('no eminities found');
        }
        $r = [];
        foreach($am as $a){
            $r[$a['name']] = $a['id'];
        }
        $res = \General::success_res();
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function postEditBus(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "edit_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        if(isset($param['is_layout'])){
            if(isset($param['seat_array'])){
                $sa = json_decode($param['seat_array'],true);
//                print_r($sa);
                if(count($sa) == 0){
                    return \General::error_res('seat map is required.');
                }
            }else{
                return \General::error_res('seat map is required.');
            }
        }
        $editBus = \App\Models\Serviceprovider\Buses::edit_bus($param);
//        print_r($param);
        return $editBus;
    }
    public function postServiceProviderDeposit() {
        $param = \Input::all();
        
        $data = \App\Models\Serviceprovider\ServiceProviderDeposit::get_deposite_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.service_provider_deposite_request", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postApproveSpDeposite() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\Serviceprovider\ServiceProviderDeposit::approve_deposit($param);
        
        return \Response::json($res, 200);
    }
    
    public function postDrivers(){
        $param = \Input::all();
        $drivers = \App\Models\Drivers::get_driver($param);
        if(is_null($drivers)){
            return \General::error_res('no eminities found');
        }
        $res = \General::success_res();
        if(isset($param['all']) && $param['all'] == 1){
            $res['data'] = $drivers;
            return \Response::json($res, 200);
        }
        $r = [];
        foreach($drivers as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function postAddDriverToBus(){
        $param = \Input::all();
        $driver = \App\Models\Drivers::get_driver($param);
        if(!is_null($driver)){
            $view_data = [
                'driver' => $driver[0]
            ];

            return view("admin.add_driver_to_bus", $view_data);
        }
    }
    
    public function postAddCouponCode(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
        'ccode'=>'unique:couponcode,code'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::add_new_coupon_code($param);
    }
    public function postEditCouponCode(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
        'ccode'=>'unique:couponcode,code,'.$param['ccid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::edit_coupon_code($param);
    }
    public function postDeleteCouponCode(){
        $param = \Input::all();
        
        return \App\Models\CouponCode::delete_coupon_code($param);
    }
    
    
    public function getTicketBookNew() {
        $city = \App\Models\Locality\Citi::get_allciti();
        if(is_null($city)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($city as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $r = json_encode($r);
        
        $view_data = [
            'header' => [
                "title" => 'Book New Ticket | Service Provider Panel BusTiket',
                "js" => ['parsley.min.js','admin.search.min.js','jquery.inputmask.bundle.min.js'],
                "css" => ['sprite-general.css','admin.search.min.css'],
            ],
            'body' => [
                'id' => 'tkt-book',
                'city_list' => $r,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_book", $view_data);
    }
    
    public function postDelTicket() {
        $param = \Input::all();
//        $data = \App\Models\Bookings::delete_ticket($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or already cancelled or not found.');
        }
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    public function getTicketList() {
        $cityList = \App\Models\Locality\Citi::get_allciti();
        $city = [];
        foreach($cityList as $c){
            $city[$c['name']] = null;
        }
//        dd(json_encode($city));
//        dd(config());
        $view_data = [
            'header' => [
                "title" => 'Ticket List | Service Provider Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'tkt-book',
                'city' => json_encode($city),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_list", $view_data);
    }
    public function postApproveTicket(){
        $param = \Input::all();
//        dd($param['id']);
        $apr = \App\Models\Bookings::approve_ticket($param['id']);
        //dd($apr);
    }
    public function getTicketDetails($id = '') {
//        $id = '09PM06UR18L2U';
        if($id == ''){
            return \Response::view('errors.404', array(), 404);
        }
        $lgr = config('constant.SP_TYPE');
        $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
        $data = \App\Models\Bookings::get_ticket_detail($id);
//        dd($data);
        if($sp_id != $data['sp_id']){
            return redirect('sp/ticket-list');
        }
        $user_id = config('constant.CURRENT_LOGIN_ID');
        if($lgr != 'Main'){
            if($user_id != $data['user_id']){
                return redirect('sp/ticket-list');
            }
        }
        if($data != 0){
            $view_data = [
                'header' => [
                    "title" => 'Ticket List | Service Provider Panel BusTiket',
                    "js" => [\URL::to('assets/ckeditor/ckeditor.js')],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-list',
                    'data' => $data,
                    
                ],
                'flag' =>1,
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("admin.ticket_detail", $view_data);
        }
        else{
            return \Response::view('errors.404', array(), 404);
        }
    }
    public function postTlistFilter() {
        $param = \Input::all();
        $lgr = config('constant.SP_TYPE');
        $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
        $param['sp_id'] = $sp_id;
        if($lgr != 'Main'){
            $param['user_id'] = config('constant.CURRENT_LOGIN_ID');
            $param['book_by'] = [2,3,4];
        }
        
        $data = \App\Models\Bookings::filter_ticketlist($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view("admin.ticketlist_filter", $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postTicketCheckout(){
        $param = \Input::all();
//        dd($param);
        $lgr = config('constant.SP_TYPE');
        
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee(config('constant.CURRENT_LOGIN_ID'));

        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( ($param['total_amount'] * $handlefee[0]['handling_fee']) / 100 );
        }
        if($lgr == 'Main'){
            $hfval = 0;
        }
        $param['handle_fee'] = $hfval;
        
        $board = json_decode($param['board_point'],true);
        $drop = json_decode($param['drop_point'],true);
//        dd($board);
        $pickdt =  date('Y-m-d H:i:s', strtotime($board['boarding_datetime'])) ;
        $dropdt =  date('Y-m-d H:i:s', strtotime($drop['droping_datetime'])) ;
        
        $fromTer = [
            'name' => $board['b_name'],
            'loc_citi' =>[
                'name'=>$board['b_city_name']
            ],
        ];
        $toTer = [
            'name' => $drop['d_name'],
            'loc_citi' =>[
                'name'=>$drop['d_city_name']
            ],
        ];
        $booking_seats = [];
        
        for($i = 0;$i < $param['total_seat'];$i++){
            $dob = date('Y-m-d',  strtotime($param['pass_dob'][$i]));
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dob), date_create($today));

            $age = isset($param['pass_dob'][$i]) ? $diff->format('%y'):'';
            $booking_seats[] = [
                'passenger_name' => $param['pass_name'][$i],
                'passenger_gender' => $param['pass_gender'][$i],
                'passenger_age' => $age,
                'ticket_id' => 'not generated yet',
                'id' => '',
                'seat_index' => isset($param['seat_index']) ? $param['seat_index'][$i] : '',
                'seat_lbl' => isset($param['seat_label']) ? $param['seat_label'][$i] : '',
            ];
        }
        $total = ($param['total_amount'] + $hfval);
        $data = [
            'booking_id'=>'not generated yet',
            'booker_name'=>$param['booker_name'],
            'booker_email'=>$param['booker_email'],
            'booker_mo'=>$param['booker_mo'],
            'created_at'=>date('Y-m-d'),
            'journey_date'=>$param['journey_date'],
            'handle_fee'=>$param['handle_fee'],
            'sp_name'=>$param['sp_name'].' & Main',
            'from_terminal'=>$fromTer,
            'to_terminal'=>$toTer,
            'pickup_date'=>$pickdt,
            'dropping_date'=>$dropdt,
            'nos'=>$param['total_seat'],
            'base_amount'=>$param['total_amount'],
            'total_amount'=> $total,
            'booking_seats'=> $booking_seats,
        ];
        
        $view_data = [
                'header' => [
                    "title" => 'Ticket List | Service Provider Panel BusTiket',
                    "js" => [],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-book',
                    'data' => $data,
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
                'flag' =>0,
                'param'=>$param,
            ];
            return view("admin.ticket_detail", $view_data);
    }
    public function postSendEmail(){
        $param = \Input::all();

        \Mail::raw($param["data"], function ($message) use ($param) {
            $message->from('us@example.com', 'Bustiket');

            $message->to($param['useremail']);
        });
        
        $res = \General::success_res('Mail Send Successfully.');
        return $res;
    }
    
    public function postSendSms(){
        $param = \Input::all();
//        dd($param);
        $res = \General::success_res('SMS Send Successfully.');
        return $res;
    }
    
    public function postCancelTicket(){
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or not found.');
        }
        $res = \General::success_res('Ticket Cancelled Successfully.');
        $res['data'] = $data;
        return $res;
    }
    public function getPaymentRequest(){
        
        $view_data = [
            'header' => [
                "title" => 'Service Provider Payment Request | Service Provider Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'pay_req',
            ],
            'footer' => [
                "js" => ['parsley.min.js'],
                "css" => []
            ],
        ];

        
        return view("admin.payment_request_list", $view_data);
    }
    public function postPaymentRequestFilter() {
        $param = \Input::all();
        
        $data = \App\Models\Serviceprovider\ServiceProviderDeposit::get_payment_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['logger'] = 'SS';
            return view("admin.payment_request_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
     
    public function postNewPaymentRequest(){
        $param = \Input::all();
        $param['pay_bank'] = isset($param['pay_bank']) ? 1 : 0;
        $rules = [
            'id'=>'required',
            'amount'=>'required',
        ];
        if($param['pay_bank']){
            $rules['acc_name'] = 'required';
            $rules['acc_number'] = 'required';
        }
        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $add = \App\Models\Serviceprovider\ServiceProviderDeposit::add_new_payment_request($param);
        return $add;
//        print_r($param);
    }
    public function getReports(){
        
        $ctype = config('constant.SP_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        $parent = config('constant.SP_PARENT') ;
        $param = [];
        $param['id'] = $cid;
        $smdata = json_encode([]);
        $sadata = json_encode([]);
        $sbdata = json_encode([]);
        $tsale = json_encode([]);
        $smcomm = json_encode([]);
        $sabcomm = json_encode([]);
        $smonth = json_encode([]);
        $sdaily = json_encode([]);
        $ssasale = json_encode([]);
        $ssbsale = json_encode([]);
        
        if($ctype == 'Main'){
            $param['book_by'] = 2;
            $smdata = SeatSeller::get_ticket_booked_by_ss($param);
            
            $ids = \App\Models\Serviceprovider\ServiceProvider::where('parent_id',$cid)->pluck('id');
//            dd($ids);
            $param['child'] = $ids;
            $param['book_by'] = 3;
            $sadata = SeatSeller::get_ticket_booked_by_ss($param);
            $param['book_by'] = 4;
            $sbdata = SeatSeller::get_ticket_booked_by_ss($param);
            $param['book_by'] = [2,3,4];
            $tsale = SeatSeller::get_total_sale_by_ss($param);
            unset($param['child']);
            $smcomm = \App\Models\Serviceprovider\ServiceProvider::get_total_commission_of_sp($param);
            $param['child'] = $ids;
            $sabcomm = \App\Models\Serviceprovider\ServiceProvider::get_total_commission_of_sp($param);
            
            $smonth = SeatSeller::get_total_sale_by_month($param);
            
            $sdaily = SeatSeller::get_total_daily_sale($param);
            $param['book_by'] = 3;
            $ssasale = \App\Models\Serviceprovider\ServiceProvider::get_child_by_type($param);
            $param['book_by'] = 4;
            $ssbsale = \App\Models\Serviceprovider\ServiceProvider::get_child_by_type($param);
        }
        
        $view_data = [
            'header' => [
                "title" => 'Reports | Service Provider Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'reporting',
                'logger'=> 'SP',
                'smdata'=>$smdata,
                'sadata'=>$sadata,
                'sbdata'=>$sbdata,
                'tsale'=>$tsale,
                'smcomm'=>$smcomm,
                'sabcomm'=>$sabcomm,
                'smonth'=>$smonth,
                'sdaily'=>$sdaily,
                'ssasale'=>$ssasale,
                'ssbsale'=>$ssbsale,
            ],
            'footer' => [
                "js" => ['d3.min.js','nv.d3.js','stream_layers.js','jchart.js'
//                    ,'charts.min.js'
                    ,'charts_temp.js'
                    ],
                "css" => ['nv.d3.min.css']
            ],
        ];
        
        return view("admin.reports", $view_data);
    } 
    public function postCsvRoute(){
        $param = \Input::all();
        $csv = \Request::file('csv');
        if(!isset($param['bus_id'])){
            return \General::error_res('bus id required.');
        }
        return \App\Models\General::csv_routes($csv,$param);
    }
    public function getCouponCode() {

        $view_data = [
            'header' => [
                "title" => 'Coupon Code | Service Provider Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'coupon'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.coupon_list", $view_data);
    }
    
    public function postCouponFilter() {
        $param = \Input::all();
        
        $ctype = config('constant.SP_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        $parent = config('constant.SP_PARENT') ;
        $param['sp_id'] = $parent;
        if($ctype == 'Main'){
            $param['sp_id'] = $cid;
        }
        
        $data = \App\Models\CouponCode::get_coupon_filter($param);
//        $data = \App\Models\Admin\Banners::get_bannerfilter($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.coupon_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postBusName(){
        $param = \Input::all();
        $sp_id = config('constant.CURRENT_LOGIN_ID');
        $param['sp_id'] = $sp_id;
        $bus = \App\Models\Serviceprovider\Buses::get_bus($param);
        if(is_null($bus)){
            return \General::error_res('no bus found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($bus as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function getPariwisataList(){
        
        $sp = \App\Models\Serviceprovider\ServiceProvider::where('type','Main')->orderBy('first_name','asc')->get();
        $city = \App\Models\Locality\Citi::orderBy('name','asc')->get();
        $view_data = [
            'header' => [
                "title" => 'Service Provider Panel | Pariwisata',
                "js" => [
                    'parsley.min.js'
                    ,'jquery.tagsinput.js'
                    ,\URL::to('assets/ckeditor/ckeditor.js')
                    ],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id'=>'pw',
                'sp'=>$sp,
                'city'=>$city
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_list", $view_data);
    }
    public function postPariwisataFilter() {
        $param = \Input::all();
        $sp_id = config('constant.CURRENT_LOGIN_ID');
        $param['sp_id'] = $sp_id;
        $data = \App\Models\Admin\Pariwisata::get_pariwisata_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.pariwisata_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postAddPariwisata() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            
//            echo '<pre>';
//            print_r($param);
//            echo '</pre>';
//            exit;
            $data = \App\Models\Admin\Pariwisata::add_pariwisata($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postEditPariwisata() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Pariwisata::edit_pariwisata($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postDeletePariwisata() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Pariwisata::delete_pariwisata($param);
        $res = \General::success_res('pariwisata deleted successfully !!');
        return \Response::json($res, 200);
    }
    public function postCityName(){
        $param = \Input::all();
        $sp = \App\Models\Locality\Citi::get_allciti($param);
        if(is_null($sp)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function getPariwisataOrder(){
        
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Pariwisata Order',
                "js" => [],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id'=>'pwo',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_order_list", $view_data);
    }
    public function postPariwisataOrderFilter() {
        $param = \Input::all();
        $sp_id = config('constant.CURRENT_LOGIN_ID');
        $param['sp_id'] = $sp_id;
        $data = \App\Models\PariwisataOrder::get_pariwisata_order_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.pariwisata_order_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postApprovePariwisataOrder() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\PariwisataOrder::approve_order($param);
        
        return \Response::json($res, 200);
    }
    public function getCommissionManagement($type = '',$id = '') {
        
        if($id == ''){
            $id = config('constant.CURRENT_LOGIN_ID');
        }
        $ctype = config('constant.SP_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        if($ctype != 'Main'){
            $type = $ctype == 'A' ? 'spa' : 'spb';
            if($cid != $id ){
                return redirect('sp/commission-management');
            }
        }else{
            
            $chlds = \App\Models\Serviceprovider\ServiceProvider::where('parent_id',$cid)->pluck('id')->toArray();
            
            if(!in_array($id, $chlds) && $id != $cid){
                
                return redirect('sp/commission-management');
            }
        }
        $view_data = [
            'header' => [
                "title" => 'Commission Management | Service Provider Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-comm',
                'uid' => $id,
                'type' => $type,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        $blade = $type == 'spa' || $type =='spb'  ? 'admin.comm_list_sp_ab' :  'admin.comm_list_sp';
        
        return view($blade, $view_data);
    }
    
    public function postCommlistFilter() {
        $param = \Input::all();
        $ftype = isset($param['ftype']) ? $param['ftype'] : '';
        $id = config('constant.CURRENT_LOGIN_ID');
//        $param['id'] = $id;
        $data = \App\Models\SpCommission::filter_commlist($param);
        
//        dd($param);
        
        $blade = $ftype == 'spa' || $ftype == 'spb' ? 'admin.commlist_filter_sp_ab' :  'admin.commlist_filter_sp';
        
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view($blade, $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postGetOptions(){
        $param = \Input::all();
        $id = $param['id'];
        $t = $param['type'];
        $data = [];
        if($t == 'c'){
            $data = \App\Models\Locality\District::where('city_id',$id)->orderBy('name','asc')->get()->toArray();
        }elseif($t == 'd'){
            $data = \App\Models\Locality\Terminal::where('district_id',$id)->orderBy('name','asc')->get()->toArray();
        }
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
}
