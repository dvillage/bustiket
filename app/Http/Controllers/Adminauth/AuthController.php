<?php 
namespace App\Http\Controllers\Adminauth;

use App\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';
    protected $guard = 'admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin',["except" => ["getRegister","postRegister","getLogin","postLogin"]]);
    }
    
    public function index(){
        return view('admin.dashboard');
    }
    
    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('admin.auth.login');
    }
    public function getRegister()
    {
        return view('admin.auth.register');
    }
    
    public function postLogin()
    {
        $input = \Request::all();
        $user = \App\Models\Admin::where("email",$input['email'])->first();
        if(is_null($user))
            //dd("Email not found");
        \Auth::guard("admin")->logout();
//        dd($user);
//        $res = \Auth::guard("admin")->loginUsingId($user->_id);
//        $res = \Auth::attempt(["email" => $data['email'],"password" => $data['password']]);
//        $res = \Auth::attempt(["email1" => $data['email']]);
        $res = \Auth::guard("admin")->loginUsingId($user->_id);
//        $res = \Auth::guard("admin")->loginUsingId("aaa");
//        $auth = auth()->guard('admin');
//        $credentials = [
//            'email' =>  $input['email'],
//            'password' =>  $input['password'],
//        ];
//        $res = $auth->loginUsingId($user->_id);
//        dd($res->toArray());
//        if ($auth->attempt($credentials)) {
        if (\Auth::guard("admin")->check()) {
            //dd(\Auth::guard("admin")->user()->toArray());
            //\Auth::guard("admin")->user()->toArray()

           // dd("Yesssssssssssss");
             return redirect()->action('LoginController@profile');                     
        } else { 
           // dd('Error');
        }
//        $res = \Auth::guard("admin")->login($user);
//        dd($res);
//        $user = \Auth::user();
//        dd($user);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function postRegister()
    {
        $data = \Request::all();
        return \App\Models\Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    protected function getLogout()
    {
        \Auth::gurd("admin")->logout();
        return redirect("/admin/login");
    }
}
