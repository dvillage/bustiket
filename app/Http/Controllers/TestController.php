<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TestController extends Controller {

    public function __construct() {
        
    }

    public function getIndex() {
        $l_id = 1;
        $layout = \App\Models\BusLayout::find($l_id)->toArray();
        $data = \App\Models\SeatMap::where("layout_id", $layout['id'])->orderBy("row", "ASC")->orderBy("col", "ASC")->get()->toArray();
        $seats = \App\Models\SeatMap::prepare_seatmap_array($data,$layout['rows'],$layout['columns']);
       // dd($seats);
        $data = [
            "layouts" => [
                $l_id => [
                    "row" => $layout['rows'],
                    "col" => $layout['columns'],
                    "map" => $data,
                    "seats" => $seats,
                ],
                
            ],
            'header' => [
                    'js' => ['ThrowPropsPlugin.min.js','TweenLite.min.js','CSSPlugin.min.js','Draggable.min.js']
            ]
        ];
        
        
        
//        \General::dd($data);
        return view("site.test", $data);
    }
    
    public function getTest(){
        $data = [
            
            'header' => [
                    'css' => ['gridstack.css'],
                    'js' => ['lodash.js','gridstack.js','gridstack.jQueryUI.js','jquery-ui.js'],
//                    'js' => ['jquery-ui.js','gridstack.all.js']
                    
            ]
        ];
        
        
        
//        \General::dd($data);
        return view("site.grid_test", $data);
    }
    
    public function getTestDemo($no){
//        echo $no;
        $view_data = [
            'header' => [
                'title' => 'Test Demo',
            ],
            'body' => [
                'no' => $no,
            ],
            'footer' => [],
        ];
        
        return view('site.testdemo',$view_data);
    }
    
    public function getBarCode()
    {
        echo \DNS2D::getBarcodeHTML("Paresh Thummar", "QRCODE");
    }
    public function getInfo()
    {
        echo phpinfo();
    }
    public function getPdf(){
        
        $tiket_detail = \App\Models\Bookings::where('id',200)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
        return view('site.e_ticket_mail',array('data'=>$tiket_detail));
    }
}
