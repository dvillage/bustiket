<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\User;
use App\Models\Serviceprovider;
use Illuminate\Support\Facades\Mail;
use DB;


class AdminController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin'];
    private static $logger = '';


    public function __construct() {
        $this->middleware('AdminAuth', ['except' => self::$bypass_url]);
        self::$logger = config('constant.LOGGER');
    }

    public function getIndex() {
        if (!\Auth::guard('admin')->check()) {
//            return \Response::view('errors.401', array(), 401);
            return \Redirect::to('/');
        }

        return \Redirect::to('admin/dashboard');
    }

    public function getDashboard() {

        if (!\Auth::guard('admin')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $total_spMain = Serviceprovider\ServiceProvider::where('type','Main')->count();
        $total_users= \App\Models\Users::count();
        $total_buses = Serviceprovider\Buses::count();
        $total_booked_ticket= \App\Models\Bookings::where('status','1')->count();
        $data=['spMain'=>$total_spMain,'users'=>$total_users,'buses'=>$total_buses,'booked_ticket'=>$total_booked_ticket];
        $view_data = [
            'header' => [
                "title" => 'Dashboard | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'Dashboard',
                'logger'=> 'A',
                'data'=>$data,
                
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.dashboard", $view_data);
    }

    
    public function getLogin($sec_token = "") {

        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        if ($sec_token != $s['login_url_token']) {
            return \Response::view('errors.404', array(), 404);
        }

        if (\Auth::guard("admin")->check()) {
            return \Redirect::to("admin/dashboard");
        }
        $view_data = [
            'body'=> [
                'logger' => 'Admin',
                'type' => 'A'
            ]
        ];
        return view('admin.login',$view_data);
    }

    public function postLogin(Request $req) {
        $view_data = [
            'body'=> [
                'logger' => 'Admin',
                'type' => 'A'
            ]
        ];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "login"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            
            return view('admin.login',$view_data)->withErrors($validator);
        }
        $param = $req->input();
        $res = User::doLogin($param);
        if ($res['flag'] == 0) {
            return view('admin.login',$view_data)->withErrors('Wrong User Id or Password !!');
//            return \Redirect::to('/');
        }
        return \Redirect::to("admin/dashboard");
    }

    public function getLogout() {

        \App\Models\Admin\Token::delete_token();
        \Auth::guard('admin')->logout();
        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        return redirect("admin/login/" . $s['login_url_token']);
    }

    public function getProfile($msg = "") {
        $res = User::getProfile();
        $view_data = [
            'header' => [
                "title" => 'Profile | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'name' => isset($res['name']) ? $res['name'] : "",
                'email' => isset($res['email']) ? $res['email'] : "",
                'msg' => $msg
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.profile", $view_data);
    }
    
    public function postChangeAdminPassword() {
        $param = \Input::all();
//        dd($param);
        $res = User::change_admin_password($param);
//        dd($res);
        if (isset($res['flag'])) {
            if ($res['flag'] == 0) {
                return \Redirect::to('admin/profile/' . $res['msg']);
            } else if ($res['flag'] == 1) {
//                return \Redirect::to("admin/dashboard");
                return \Redirect::to("admin/logout");
            }
        }
    }

    public function getServiceProvider($type = '',$id = 0) {
        if($type != '' && $type != 'main' && $type != 'A' && $type != 'B' && $type != 'buses'){
            return \Response::view('errors.404', array(), 404);
        }
        $param['type'] = $type;
        $param['start'] = 0;
        $param['len'] = 5;

        $view_data = [
            'header' => [
                "title" => 'Service Provider | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' || $type == 'buses' ? 'main' : $type),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        if($type == 'buses'){
            if($id > 0){
                
                $view_data['header']['js'] = [
                    \URL::to('assets/ckeditor/ckeditor.js'),
                    
                ];
                
                $allSp = Serviceprovider\ServiceProvider::active()->where('type','Main')->get();
                $buses = Serviceprovider\ServiceProvider::with('spBuses.routeCount')->where('id',$id)->first();
//                $routes = Serviceprovider\ServiceProvider::with('spBuses.routeCount')->first()->toArray();
//                echo '<pre>';
//                print_r($buses);
//                echo '</pre>';
                $view_data['body']['buses'] = $buses;
                $view_data['body']['all_sp'] = $allSp;
//                $view_data['body']['routes_count'] = $routes;
                return view("admin.buses_list", $view_data);
            }else{
                return \Response::view('errors.404', array(), 404);
            }
        }
        return view("admin.sp_main", $view_data);
    }
    public function getTicketBookNew() {
        
        $city = \App\Models\Locality\Citi::get_allciti();
        if(is_null($city)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($city as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $r = json_encode($r);
        
        $view_data = [
            'header' => [
                "title" => 'Book New Ticket | Admin Panel BusTiket',
                "js" => ['parsley.min.js','admin.search.min.js','jquery.inputmask.bundle.min.js'],
                "css" => ['sprite-general.css','admin.search.min.css'],
            ],
            'body' => [
                'id' => 'tkt-book',
                'city_list' => $r,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_book", $view_data);
    }
    
    public function postDelTicket() {
        $param = \Input::all();
//        $data = \App\Models\Bookings::delete_ticket($param);
        
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or already cancelled or not found.');
        }
        
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    public function postRefundTicket() {
        $param = \Input::all();
//        $data = \App\Models\Bookings::delete_ticket($param);
        
        $data = \App\Models\Bookings::refund_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not cancelled yet or already refunded or not found.');
        }
        
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    public function getTicketList($id = 0,$type = -1) {
        $cityList = \App\Models\Locality\Citi::get_allciti();
        $city = [];
        foreach($cityList as $c){
            $city[$c['name']] = null;
        }
//        dd(json_encode($city));
        $bb = [];
        if($id > 0 && $type > -1 ){
            $bb['id'] = $id;
            $bb['book_by'] = $type;
        }
        $view_data = [
            'header' => [
                "title" => 'Ticket List | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'tkt-book',
                'filter' => $bb,
                'city' => json_encode($city),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_list", $view_data);
    }
    
    public function getCommissionManagement($type = '',$id = '') {
        
        $name = '';
        $balance = 0;
        if($id != ''){
            if($type == 'ss' || $type == 'ssa' || $type == 'ssb'){
                $ss= \App\Models\Admin\SeatSeller::where('id',$id)->first();
//                dd($ss->toArray());
                if($ss){
                    $t      = $ss->type;
                    $name   = $ss->name;
                    $balance= $ss->balance;
                    $t = $t == 'Main' ? 'ss' : ($t == 'A' ? 'ssa' : 'ssb');
                    if($t !=$type){
                        return redirect('admin/commission-management');
                    }
                }else{
                    return redirect('admin/commission-management');
                }
            }elseif($type == 'sp' || $type == 'spa' || $type == 'spb'){
                $ss= Serviceprovider\ServiceProvider::where('id',$id)->first();
                if($ss){
                    $t = $ss->type;
                    $t = $t == 'Main' ? 'sp' : ($t == 'A' ? 'spa' : 'spb');
                    if($t !=$type){
                        return redirect('admin/commission-management');
                    }
                }else{
                    return redirect('admin/commission-management');
                }
            }
        }
        
        $view_data = [
            'header' => [
                "title" => 'Commission Management | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'    => 'admin-comm',
                'uid'   => $id,
                'type'  => $type,
                'name'  => $name,
                'balance'   => $balance,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        $blade = $type == 'ss' ? 'admin.comm_list_ss' : ($type == 'ssa' || $type == 'ssb' ? 'admin.comm_list_ss_ab' : ($type == 'sp' ? 'admin.comm_list_sp' : ($type == 'spa' || $type == 'spb' ? 'admin.comm_list_sp_ab' : 'admin.comm_list')));
        
        return view($blade, $view_data);
    }
    
    public function postCommlistFilter() {
        $param = \Input::all();
        $ftype = isset($param['ftype']) ? $param['ftype'] : '';
        if($ftype == 'ss' || $ftype == 'ssa' || $ftype == 'ssb'){
            $data = \App\Models\SsCommission::filter_commlist($param);
        }elseif($ftype == 'sp' || $ftype == 'spa' || $ftype == 'spb'){
            $data = \App\Models\SpCommission::filter_commlist($param);
        }else{
            $data = \App\Models\AdminCommission::filter_commlist($param);
        }
        
//        dd($param);
        
        $blade = $ftype == 'ss' ? 'admin.commlist_filter_ss' : ($ftype == 'ssa' || $ftype == 'ssb' ? 'admin.commlist_filter_ss_ab' : ($ftype == 'sp' ? 'admin.commlist_filter_sp' : ($ftype == 'spa' || $ftype == 'spb' ? 'admin.commlist_filter_sp_ab' : 'admin.commlist_filter')));
        
        if ($data['flag'] == 1 && count($data['result']) > 0) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view($blade, $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postApproveTicket(){
        $param = \Input::all();
//        dd($param['id']);
        $apr = \App\Models\Bookings::approve_ticket($param['id']);
        return \Response::json($apr,200);
//        dd($apr);
    }
    public function getTicketDetails($id = '') {
//        $id = '09PM06UR18L2U';
        if($id == ''){
            return \Response::view('errors.404', array(), 404);
        }
        $data = \App\Models\Bookings::get_ticket_detail($id);
//        dd($data);
        if($data != 0){
            $view_data = [
                'header' => [
                    "title" => 'Ticket List | Admin Panel BusTiket',
                    "js" => [\URL::to('assets/ckeditor/ckeditor.js')],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-list',
                    'data' => $data,
                    
                ],
                'flag' =>1,
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("admin.ticket_detail", $view_data);
        }
        else{
            return \Response::view('errors.404', array(), 404);
        }
    }
    public function postTicketCheckout(){
        $param = \Input::all();
//        dd($param);
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($param['sp_id']);

        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( ($param['total_amount'] * $handlefee[0]['handling_fee']) / 100 );
        }
        $param['handle_fee'] = $hfval;
        
        $board = json_decode($param['board_point'],true);
        $drop = json_decode($param['drop_point'],true);
//        dd($board);
        $pickdt =  date('Y-m-d H:i:s', strtotime($board['boarding_datetime'])) ;
        $dropdt =  date('Y-m-d H:i:s', strtotime($drop['droping_datetime'])) ;
        
        $fromTer = [
            'name' => $board['b_name'],
            'loc_citi' =>[
                'name'=>$board['b_city_name']
            ],
        ];
        $toTer = [
            'name' => $drop['d_name'],
            'loc_citi' =>[
                'name'=>$drop['d_city_name']
            ],
        ];
        $booking_seats = [];
        for($i = 0;$i < $param['total_seat'];$i++){
            
            $dob = date('Y-m-d',  strtotime($param['pass_dob'][$i]));
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dob), date_create($today));

            $age = isset($param['pass_dob'][$i]) ? $diff->format('%y'):'';
            
            $booking_seats[] = [
                'passenger_name' => $param['pass_name'][$i],
                'passenger_gender' => $param['pass_gender'][$i],
                'passenger_age' => $age,
                'ticket_id' => 'not generated yet',
                'id' => '',
                'seat_index' => isset($param['seat_index']) ? $param['seat_index'][$i] : '',
                'seat_lbl' => isset($param['seat_label']) ? $param['seat_label'][$i] : '',
            ];
        }
        $total = ($param['total_amount'] + $hfval);
        $data = [
            'booking_id'=>'not generated yet',
            'booker_name'=>$param['booker_name'],
            'booker_email'=>$param['booker_email'],
            'booker_mo'=>$param['booker_mo'],
            'created_at'=>date('Y-m-d'),
            'journey_date'=>$param['journey_date'],
            'handle_fee'=>$param['handle_fee'],
            'sp_name'=>$param['sp_name'].' & Main',
            'from_terminal'=>$fromTer,
            'to_terminal'=>$toTer,
            'pickup_date'=>$pickdt,
            'dropping_date'=>$dropdt,
            'nos'=>$param['total_seat'],
            'base_amount'=>$param['total_amount'],
            'total_amount'=> $total,
            'booking_seats'=> $booking_seats,
        ];
        
        $view_data = [
                'header' => [
                    "title" => 'Ticket List | Admin Panel BusTiket',
                    "js" => [],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-book',
                    'data' => $data,
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
                'flag' =>0,
                'param'=>$param,
            ];
            return view("admin.ticket_detail", $view_data);
    }
    public function postSendEmail(){
        $param = \Input::all();

        Mail::raw($param["data"], function ($message) use ($param) {
            $message->from('us@example.com', 'Bustiket');

            $message->to($param['useremail']);
        });
        
        $res = \General::success_res('Mail Send Successfully.');
        return $res;
    }
    
    public function postSendSms(){
        $param = \Input::all();
//        dd($param);
        $res = \General::success_res('SMS Send Successfully.');
        return $res;
    }
    
    public function postCancelTicket(){
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
//        dd($data);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or not found.');
        }
        $res = \General::success_res('Ticket Cancelled Successfully.');
        $res['data'] = $data;
        return $res;
    }

    public function postSpFilter() {
        $param = \Input::all();
        $data = \App\Models\Serviceprovider\ServiceProvider::get_sp_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $data['type'] = $param['type'];
            return view("admin.sp_filter", $data);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postTlistFilter() {
        $param = \Input::all();
        $data = \App\Models\Bookings::filter_ticketlist($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view("admin.ticketlist_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function getAddProvider($type = '') {
        $data=self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Add Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        $t = $type != '' ? '_t' : '';
        return view("admin.add_provider" . $t, $view_data);
    }

    public function getUpdateProvider($type,$id) {
        $param['id']=$id;
        $param['type']=$type;
        $details= \App\Models\Serviceprovider\ServiceProvider::providers_details($param);
        
        $data=self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Update Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'data' => $details['data'][0],
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        
        return view("admin.providers_detail", $view_data);
    }
    
    public function getBanner() {

        $view_data = [
            'header' => [
                "title" => 'Banner | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'Banner'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.banner_list", $view_data);
    }
    public function getCouponCode() {

        $view_data = [
            'header' => [
                "title" => 'Coupon Code | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'coupon'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.coupon_list", $view_data);
    }
    
    public function postCouponFilter() {
        $param = \Input::all();
        
        $data = \App\Models\CouponCode::get_coupon_filter($param);
//        $data = \App\Models\Admin\Banners::get_bannerfilter($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.coupon_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postBannerFilter() {
        $param = \Input::all();
        
        $data = \App\Models\Admin\Banners::get_bannerfilter($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.banner_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postDelBanner() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Banners::delete_banner($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    
    public function postAddBanner(){
        $param = \Input::all();
        $addBanner = \App\Models\Admin\Banners::add_new_banner($param);

        return \Response::json($addBanner, 200);
    }
    public function postEditBanner(){
        $param = \Input::all();
        $addBanner = \App\Models\Admin\Banners::update_banner($param);

        return \Response::json($addBanner, 200);
    }
    
    public function getLocality($type='') {

        $plist= \App\Models\Locality\Province::get_allprovince();
        $dlist= \App\Models\Locality\District::get_alldistrict();
        $clist= \App\Models\Locality\Citi::get_allciti();
//        dd($dlist,$dlist);
        $view_data = [
            'header' => [
                "title" => 'Locality | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'loc-'.$type,
                'plist' => $plist,
                'dlist' => $dlist,
                'clist' => $clist,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        
        return view("admin.locality", $view_data);
    }
    
    public function getSeatseller($type='') {
        if($type != 'Main' && $type != 'A' && $type != 'B' && $type != 'D'){
            return \Response::view('errors.404', array(), 404);
        }
        $mainss='';
        if($type!='Main')
            $mainss= \App\Models\Admin\SeatSeller::get_mainss();
//        $clist= \App\Models\Locality\Citi::get_allciti();
//        dd($dlist,$dlist);
        $view_data = [
            'header' => [
                "title" => 'Seat Seller | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'ss-'.$type,
                'plist' => $mainss,
                'logger'=>'A'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        if($type=='D'){
            return view("admin.seat_seller_deposit", $view_data);
        }else{
            return view("admin.seat_seller", $view_data);
        }
        
    }
    
    public function postSeatSellerFilter() {
        $param = \Input::all();
        
        $data = \App\Models\Admin\SeatSeller::get_seatsellerfilter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            $res['logger'] = 'A';
            return view("admin.seat_seller_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postSeatSellerDeposit() {
        $param = \Input::all();
        
        $data = \App\Models\Admin\SeatSellerDeposit::get_deposite_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.seat_seller_deposite_request", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postApproveSsDeposite() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\Admin\SeatSellerDeposit::approve_deposit($param);
        
        return \Response::json($res, 200);
    }
    public function postDelSeatseller() {
        $param = \Input::all();
        $data = \App\Models\Admin\SeatSeller::delete_seatseller($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    public function postAddSeatseller(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'ssemail'=>'unique:ss,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $addss = \App\Models\Admin\SeatSeller::add_seatseller($param);

        return \Response::json($addss, 200);
    }
    public function postEditSeatseller(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'ssemail'=>'unique:ss,email,'.$param['sid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $addss = \App\Models\Admin\SeatSeller::edit_seatseller($param);

        return \Response::json($addss, 200);
    }
    
    public function getSeatsellerDetails($id=''){
        $ss= \App\Models\Admin\SeatSeller::get_seatseller_details($id);
//        dd($ss);
        
        $view_data = [
            'header' => [
                "title" => 'Seat Seller Details| Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => $ss['type'],
                'id' => 'ss-'.$ss['type'],
                'data' => isset($ss)?$ss:'',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        
        return view("admin.seatseller_details", $view_data);
    }
    
    public function postLocalityFilter() {
        $param = \Input::all();
        if($param['type'] == 'Province'){
            $data = \App\Models\Locality\Province::get_province($param);
        }
        if($param['type'] == 'District'){
            $data = \App\Models\Locality\District::get_district($param);
        }
        elseif($param['type'] == 'City'){
            $data = \App\Models\Locality\Citi::get_citi($param);
        }
        elseif($param['type'] == 'Terminal'){
            $data = \App\Models\Locality\Terminal::get_terminal($param);
        }
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            return view("admin.locality_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postDelLocalityDistrict() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\District::delete_district($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    public function postAddLocality() {
        $param = \Input::all();
//        dd($param);
        
        if($param['type']=='Province'){
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:provinces,name'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Locality\Province::add_province($param);
        }
        elseif($param['type']=='District'){
            $ex = \App\Models\Locality\District::dist_exist($param);
            if($ex != 1){
                return \General::error_res('District is already Exist ..');
            }
            
            $data = \App\Models\Locality\District::add_district($param);
        }
        elseif ($param['type']=='City') {
            $ex = \App\Models\Locality\Citi::city_exist($param);
            if($ex != 1){
                return \General::error_res('City is already Exist ..');
            }
            $data = \App\Models\Locality\Citi::add_citi($param);
        }
        elseif($param['type']=='Terminal'){
            $ex = \App\Models\Locality\Terminal::terminal_exist($param);
            if($ex != 1){
                return \General::error_res('Terminal is already Exist ..');
            }
            $data = \App\Models\Locality\Terminal::add_terminal($param);
        }

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postEditLocality() {
        $param = \Input::all();
//        dd($param);
        
        if($param['type']=='Province'){
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:provinces,name,'.$param['id']
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Locality\Province::edit_province($param);
        }
        if($param['type']=='District'){
            $ex = \App\Models\Locality\District::dist_exist($param,1);
            if($ex != 1){
                return \General::error_res('District is already Exist ..');
            }
            $data = \App\Models\Locality\District::edit_district($param);
        }
        elseif ($param['type']=='City') {
            $ex = \App\Models\Locality\Citi::city_exist($param,1);
            if($ex != 1){
                return \General::error_res('city is already exist ..');
            }
            $data = \App\Models\Locality\Citi::edit_citi($param);
        }
        elseif($param['type']=='Terminal'){
            $ex = \App\Models\Locality\Terminal::terminal_exist($param, 1);
            if($ex != 1){
                return \General::error_res('Terminal is already exist ..');
            }
            $data = \App\Models\Locality\Terminal::edit_terminal($param);
        }

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    
    public function postDelLocalityCity() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Citi::delete_citi($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    public function postDelLocalityProvince() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Province::delete_province($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    public function postDelLocalityTerminal() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Terminal::delete_terminal($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    public function postFilterDist() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\District::get_distfilter($param);
        $dlist = [];
        foreach($data as $d){
            $dlist[$d['name']] = $d['id'];
        }

        $res = \General::success_res();
        $res['data']=$dlist;
//        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postFilterCity() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Citi::get_citifilter($param);
        
//        $clist = [];
//        foreach($data as $d){
//            $clist[$d['name']] = $d['id'];
//        }
//        dd(json_encode($clist));
        $res = \General::success_res();
//        $res['data']=$clist;
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postCheckSpEmail(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'email'=>'unique:sp,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return \Redirect::back()->withErrors($validator);
        }
        return \General::success_res();
    }

    public function postEditServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'email'=>'unique:sp,email,'.$param['spid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
//            return \Response::json($json, 200);
//            dd($validator);
//            return \Redirect::back()->withErrors($validator);
            return \Redirect::back()->withErrors(['email' => ['The email has already been taken.']]);
        }
//        dd($param);
        $res = \App\Models\Serviceprovider\ServiceProvider::edit_sp($param);
        if ($res['flag'] == 1) {
            if ($param['type'] == 'Main') {
                return \Redirect::to('admin/service-provider');
            }
            return \Redirect::to('admin/service-provider/'.$param['type']);
        }
    }
    public function postAddServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'email'=>'unique:sp,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return view('admin.login',$view_data)->withErrors($validator);
//            return \Redirect::back()->withErrors($validator)->withInput();
        }
//        dd($param);
        $res = \App\Models\Serviceprovider\ServiceProvider::save_sp($param);
//        return $res;
        if ($res['flag'] == 1) {
            if ($param['type'] == 'Main') {
                return \Redirect::to('admin/service-provider');
            }
            return \Redirect::to('admin/service-provider/'.$param['type']);
        }
    }

    public function postGetMainProvider() {
        $res = \General::success_res();
        $data = \App\Models\Serviceprovider\ServiceProvider::get_sp();
        $res['data'] = $data;
        return $res;
    }

    public function postDelServiceProvider() {
        $param = \Input::all();
        $data = \App\Models\Serviceprovider\ServiceProvider::delete_ServiceProvider($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postUpdateBusName(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "update_bus_name"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = Serviceprovider\Buses::update_bus_name(\Input::all());
        return \Response::json($res, 200);
    }
    public function postAddBus(){
        $param = \Input::all();
//        print_r($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "add_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $addBus = Serviceprovider\Buses::add_new_bus($param);
//        $res = \General::success_res();
//        $res['data'] = $param;
        return \Response::json($addBus, 200);
    }
    
    public function postGetBus(){
        $param = \Input::all();
        $bus = Serviceprovider\Buses::where('id',$param['id'])->with(['sp','amenities','busLayout.seats','busPrice','busRoutes','drivers.driver','couponCode'])->first()->toArray();
        $routes = \App\Models\Admin\BusRoutes::where('bus_id',$param['id'])->with(['fromCity','toCity','fromTerminal','toTerminal'])->orderBy('from_city_id','asc')->orderBy('to_city_id','asc')->get();
//        $b = Serviceprovider\Buses::where('id',$param['id'])->with('busLayout.seats')->get()->toArray();
//        echo '<pre>';
//        print_r($bus);
//        echo '</pre>';
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'sp-main',
                'bus'=>$bus,
                'routes'=>$routes,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.bus_detail", $view_data);
    }
    
    public function getBusStructure( $bus_id = 0,$sp_id = 0){
        if(!(int)$bus_id || !(int)$sp_id){
            return \Response::view('errors.404', array(), 404);
        }
        
        $bus = Serviceprovider\Buses::where('id',$bus_id)->with('amenities')->with('busPrice')->with('blockDates')->with('busRoutes')->with('busLayout')->with('couponCode')->first();
        $buses = Serviceprovider\ServiceProvider::with('spBuses')->where('id',$sp_id)->first();
        $cities = \App\Models\Locality\Citi::orderBy('name','asc')->lists('name','id');
        $dist = \App\Models\Locality\District::orderBy('name','asc')->lists('name','id');
        $routes = \App\Models\Admin\BusRoutes::where('bus_id',$bus_id)->with(['fromCity','toCity','fromTerminal','fromDistrict','toDistrict','toTerminal','prices'])->orderBy('id','asc')->get();
        $lay = \App\Models\BusLayout::where('bus_id',$bus_id)->first();
        $drivers = \App\Models\BusDrivers::where('bus_id',$bus_id)->with('driver')->get();
        if(!is_null($lay) && $bus['layout']){
            $layout = \App\Models\SeatMap::where('layout_id',$lay->id)->get()->toArray();
            $layout = json_encode($layout,true);
        }else{
            $layout = '';
        }
        
//        print_r($layout);
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => ['jquery.tagsinput.js'
//                    ,'jquery-ui.js'
                    ,'parsley.min.js'
                    ,'jquery.inputmask.bundle.min.js'
                    ,'ThrowPropsPlugin.min.js','TweenLite.min.js','CSSPlugin.min.js','Draggable.min.js'
                    ,\URL::to('assets/ckeditor/ckeditor.js')
                    ],
                "css" => ['jquery.tagsinput.css'
//                    ,'jquery-ui.css'
                    ],
            ],
            'body' => [
                'id' => 'sp-main',
                'bus'=>$bus,
                'all_bus'=>$buses,
                'city'=>$cities,
                'dist'=>$dist,
                'routes'=>$routes,
                'drivers'=>$drivers,
                'layout'=>$layout
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.bus_structure", $view_data);
//        return view("admin.bus_structure_old", $view_data);
    }
    
    public function postGetTerminals(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "get_terminals"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $param = \Input::all();
        $ter_from = \App\Models\Locality\Terminal::get_terminal_from_city($param['from_id']);
        $ter_f = $ter_from->toArray();
        if(count($ter_f) == 0){
            return \General::error_res('no terminal found for from city');
        }
        $ter_to = \App\Models\Locality\Terminal::get_terminal_from_city($param['to_id']);
        $ter_t = $ter_to->toArray();
        if(count($ter_t) == 0){
            return \General::error_res('no terminal found for to city');
        }
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'from'=>$ter_from,
                'to'=>$ter_to,
                'param'=>$param
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.bus_terminals", $view_data);
//        return view("admin.bus_terminals_old", $view_data);
    }
    
    public function postGetAmenites(){
        $param = \Input::all();
        $am = \App\Models\Admin\Amenities::get_amenities($param);
        if(is_null($am)){
            return \General::error_res('no eminities found');
        }
        $r = [];
        foreach($am as $a){
            $r[$a['name']] = $a['id'];
        }
        $res = \General::success_res();
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function postEditBus(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "edit_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
//        dd($param);
        if(isset($param['is_layout'])){
            if(isset($param['seat_array'])){
                $sa = json_decode($param['seat_array'],true);
//                print_r($sa);
                if(count($sa) == 0){
                    return \General::error_res('seat map is required.');
                }
            }else{
                return \General::error_res('seat map is required.');
            }
        }
        $editBus = Serviceprovider\Buses::edit_bus($param);
//        print_r($param);
        return $editBus;
    }
    public function postDrivers(){
        $param = \Input::all();
        $drivers = \App\Models\Drivers::get_driver($param);
        if(is_null($drivers)){
            return \General::error_res('no drivers found');
        }
        $res = \General::success_res();
        if(isset($param['all']) && $param['all'] == 1){
            $res['data'] = $drivers;
            return \Response::json($res, 200);
        }
        $r = [];
        foreach($drivers as $a){
            $r[$a['name'].'-'.$a['mobile']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function postAddDriverToBus(){
        $param = \Input::all();
        $driver = \App\Models\Drivers::get_driver($param);
        if(!is_null($driver)){
            $view_data = [
                'driver' => $driver[0]
            ];

            return view("admin.add_driver_to_bus", $view_data);
        }
    }
    public function getDriversList(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Drivers',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'driver',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.bus_drivers", $view_data);
    }
    public function postDriversFilter(){
        $param = \Input::all();
        
        $data = \App\Models\Drivers::get_driver_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.driver_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postAddNewDriver(){
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "driver"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        
        $addD = \App\Models\Drivers::add_new_driver($param);
        return $addD;
    }
    public function postEditDriver(){
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "driver"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        
        $addD = \App\Models\Drivers::edit_driver($param);
        return $addD;
    }
    public function postDeleteDriver(){
        $param = \Input::all();
        
        return \App\Models\Drivers::delete_driver($param);
    }
    
    public function postAddCouponCode(){
        $param = \Input::all();
       
        $validator = \Validator::make(\Input::all(), [
        'ccode'=>'unique:couponcode,code'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::add_new_coupon_code($param);
    }
    public function postEditCouponCode(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
        'ccode'=>'unique:couponcode,code,'.$param['ccid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::edit_coupon_code($param);
    }
    public function postDeleteCouponCode(){
        $param = \Input::all();
        
        return \App\Models\CouponCode::delete_coupon_code($param);
    }
    
    public function getAmenitiesList(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Amenities',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'amenity',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.amenities_list", $view_data);
    }
    public function postAmenitiesFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Amenities::get_amenities_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            return view("admin.amenities_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postDeleteAmenities() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Amenities::delete_amenity($param);
        $res = \General::success_res('Amenity deleted successfully !!');
        return \Response::json($res, 200);
    }
    
    public function postAddAmenities() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:amenities,name'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Amenities::add_amenity($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postEditAmenities() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:amenities,name,'.$param['a_id']
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Amenities::edit_amenity($param);
        
        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function getPariwisataList(){
        
        $sp = Serviceprovider\ServiceProvider::where('type','Main')->orderBy('first_name','asc')->get();
        $city = \App\Models\Locality\Citi::orderBy('name','asc')->get();
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Pariwisata',
                "js" => [
                    'parsley.min.js'
                    ,'jquery.tagsinput.js'
                    ,\URL::to('assets/ckeditor/ckeditor.js')
                    ],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id'=>'pw',
                'sp'=>$sp,
                'city'=>$city
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_list", $view_data);
    }
    public function postPariwisataFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Pariwisata::get_pariwisata_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.pariwisata_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postAddPariwisata() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            
//            echo '<pre>';
//            print_r($param);
//            echo '</pre>';
//            exit;
            $data = \App\Models\Admin\Pariwisata::add_pariwisata($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postEditPariwisata() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Pariwisata::edit_pariwisata($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postDeletePariwisata() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Pariwisata::delete_pariwisata($param);
        $res = \General::success_res('Pariwisata deleted successfully !!');
        return \Response::json($res, 200);
    }
    public function postSpName(){
        $param = \Input::all();
        $sp = Serviceprovider\ServiceProvider::get_sp($param);
        if(is_null($sp)){
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['first_name'].' '.$a['last_name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postAllSpName(){
        $param = \Input::all();
        $sp = Serviceprovider\ServiceProvider::get_allsp($param);
        if(is_null($sp)){
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['first_name'].' '.$a['last_name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postAllSsName(){
        $param = \Input::all();
        $sp = \App\Models\Admin\SeatSeller::get_allss($param);
        if(is_null($sp)){
            return \General::error_res('No Seat Seller found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postSeller(){
        $param = \Input::all();
        $seller = Serviceprovider\ServiceProvider::get_sp($param);
        if(is_null($seller)){
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($seller as $a){
            $r[$a['first_name'].' '.$a['last_name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postCityName(){
        $param = \Input::all();
        $sp = \App\Models\Locality\Citi::get_allciti($param);
        if(is_null($sp)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function getCardList(){
        
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Commuter Management',
                "js" => [
                    'parsley.min.js'
                    ,'jquery.tagsinput.js'
                    ,\URL::to('assets/ckeditor/ckeditor.js')
                    ],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id'=>'card',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.card_list", $view_data);
    }
    public function postCardFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Card::get_card_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.card_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postAddCard() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "card"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:cards,name'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
//            echo '<pre>';
//            print_r($param);
//            echo '</pre>';
//            exit;
            $data = \App\Models\Admin\Card::add_card($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postEditCard() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "card"));
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:cards,name,'.$param['cid']
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            
            $data = \App\Models\Admin\Card::edit_card($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    public function postDeleteCard() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Card::delete_card($param);
        $res = \General::success_res('Commuter deleted successfully !!');
        return \Response::json($res, 200);
    }
    public function postGetCouriers(){
        $param = \Input::all();
        $am = \App\Models\Admin\Courier::get_courier($param);
        if(is_null($am)){
            return \General::error_res('no courier found');
        }
        $r = [];
        foreach($am as $a){
            $r[$a['name']] = $a['id'];
        }
        $res = \General::success_res();
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function getCouriersList(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Couriers',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'crr',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.couriers_list", $view_data);
    }
    public function postCouriersFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Courier::get_courier_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.couriers_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postDeleteCouriers() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Courier::delete_courier($param);
        $res = \General::success_res('Courier deleted successfully !!');
        return \Response::json($res, 200);
    }
    
    public function postAddCouriers() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:courier,name'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Courier::add_courier($param);
        

        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postEditCouriers() {
        $param = \Input::all();
//        dd($param);
        
        
            $validator = \Validator::make(\Input::all(), [
            'name'=>'unique:courier,name,'.$param['c_id']
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Admin\Courier::edit_courier($param);
        
        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function getSettings(){
        
        $settings = \App\Models\Admin\Settings::all_settings();
        $settings = $settings['data'];
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Settings',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'settings',
                'settings'=>$settings,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.settings_list", $view_data);
    }
    
    public function postSaveSettings(){
        $param = \Input::all();
//        print_r($param);
        $set = \App\Models\Admin\Settings::save_settings($param);
        return $set;
    }
    
    public function getUsersList(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Users',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id'=>'list',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.users_list", $view_data);
    }
    public function postUsersFilter() {
        $param = \Input::all();
        $data = \App\Models\Users::get_user_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.users_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postGetUser(){
        $param = \Input::all();
        $user = \App\Models\Users::where('id',$param['id'])->first()->toArray();

        $view_data = [
            'header' => [
                "title" => 'Admin | Users',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'list',
                'user'=>$user,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.user_detail", $view_data);
    }
    public function postEditUser(){
        $param = \Input::all();
        $user = \App\Models\Users::edit_user($param);
        return $user;
    }
    public function postDeleteUser(){
        $param = \Input::all();
        $user = \App\Models\Users::delete_user($param);
        return $user;
    }
    public function getFavoriteRoute(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Favorite Routes',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'fav_route',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.fav_route_list", $view_data);
    }
    
    public function postRouteFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\FavoriteRoute::get_route_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.fav_route_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postAddRoute() {
        $param = \Input::all();
        //dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "fav_route"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $exist = \App\Models\Admin\FavoriteRoute::where(['from'=>$param['from_city_id'],'to'=>$param['to_city_id'],'bus_id'=>$param['bus_id']])->first();
        if(!is_null($exist)){
            return \General::error_res('this route is already exist !!');
        }
        $data = \App\Models\Admin\FavoriteRoute::add_route($param);
        $res = \General::success_res();
        $res['data']=$data;
        return \Response::json($res, 200);
    }
    
    public function postEditRoute(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "fav_route"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $exist = \App\Models\Admin\FavoriteRoute::where(['from'=>$param['from_city_id'],'to'=>$param['to_city_id'],'bus_id'=>$param['bus_id']])->where('id','!=',$param['cid'])->first();
        if(!is_null($exist)){
            return \General::error_res('this route is already exist !!');
        }
        $user = \App\Models\Admin\FavoriteRoute::edit_route($param);
        $res = \General::success_res();
        $res['data']=$user;
        return \Response::json($res, 200);
        
    }
    public function postDeleteRoute(){
        $param = \Input::all();
        $user = \App\Models\Admin\FavoriteRoute::delete_route($param);
        return $user;
    }
    public function postBusName(){
        $param = \Input::all();
        $bus = Serviceprovider\Buses::get_bus($param);
        if(is_null($bus)){
            return \General::error_res('no bus found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($bus as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postBus(){
        $param = \Input::all();
        $bus = Serviceprovider\Buses::get_bus($param);
        if(is_null($bus)){
            return \General::error_res('no bus found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($bus as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function getPariwisataOrder(){
        
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Pariwisata Order',
                "js" => [],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id'=>'pwo',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_order_list", $view_data);
    }
    public function postPariwisataOrderFilter() {
        $param = \Input::all();
        $data = \App\Models\PariwisataOrder::get_pariwisata_order_list($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.pariwisata_order_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postApprovePariwisataOrder() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\PariwisataOrder::approve_order($param);
        
        return \Response::json($res, 200);
    }
    public function postGetOptions(){
        $param = \Input::all();
        $id = $param['id'];
        $t = $param['type'];
        $data = [];
        if($t == 'c'){
            $data = \App\Models\Locality\District::where('city_id',$id)->orderBy('name','asc')->get()->toArray();
        }elseif($t == 'd'){
            $data = \App\Models\Locality\Terminal::where('district_id',$id)->orderBy('name','asc')->get()->toArray();
        }
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    public function getTestimonialList() {

        if (!\Auth::guard('admin')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Testimonial | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-testimonial',
                'logger'=> 'A'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.testimonial_list", $view_data);
    }
    
    public function postAddTestimonial(){    
        $param=\Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin","testimonial"));
        if($validator->fails()){
            
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
            
        }else{
            $data=\App\Models\Admin\Testimonial::addTestimonial($param);
            $res = \General::success_res();
            $res['data']=$data;
            return \Response::json($res,200);
        }        
    }
    
    
     public function postUpdateTestimonial(){    
        $param=\Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin","testimonial"));
        if($validator->fails()){
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }else{
            $data=\App\Models\Admin\Testimonial::updateTestimonial($param);
            $res = \General::success_res();
            $res['data']=$data;
            return \Response::json($res, 200);
        }        
    }
    
    
    public function postDeleteTestimonial(){    
        $param=\Input::all();
        if(is_numeric($param['id'])){
            $data=\App\Models\Admin\Testimonial::deleteTestimonial($param);
            $res = \General::success_res();
            $res['data']=$data;
            return \Response::json($res, 200);
        }
        else{
            return \General::error_res("Enter parameter Should numeric");
            
        }        
    }
 
    public function postTestimonialFilter() {
        
        $param =\Input::all();
        
        $data = \App\Models\Admin\Testimonial::getTestimonialList($param);

        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.testimonial_filter", $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);
        return \Response::json($res, 200);

    }
    
    
    
    
    public function getPushNotification() {

        if (!\Auth::guard('admin')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Push Notification | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-push-notify',
                'logger'=> 'A'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.push_notification_list", $view_data);
    }
    
    public function postPushNotificationFilter() {
        
        $param =\Input::all();
        $optype = isset($param['search']) ? $param['search'] : '';
        
        switch($optype){
            
            case 'spm':
                case 'ssm':
                        $param['type']="Main";
                        break;
                        
            case 'spa':
                case 'ssa':
                        $param['type']="A";
                        break;        

            case 'spb':
                case 'ssb':
                        $param['type']="B";
                        break;
            
            default:
                    $param['type']='Main';
                    
        }
        
        unset($param['search']);
        
       if($optype=='ssm' || $optype=='ssa' || $optype=='ssb'){ 
//           dd($optype);
           $data = \App\Models\Admin\SeatSeller::get_seatsellerfilter($param);
           
       } 
       else{
            $data = \App\Models\Serviceprovider\ServiceProvider::get_sp_filter($param);
       }
        
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            return view("admin.push_notification_filter",$data);
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);
        return \Response::json($res, 200);
        
    }
    
    public static function postNewPushNotification(){
        $param=\Input::all();

        $email=explode(",",$param['email']);
        $optype=$param['gtype'];
        if($optype=='ssm' || $optype=='ssa' || $optype=='ssb'){ 
          
//          $device_token = \App\Models\Admin\SeatSeller::select('device_token')->where('email',$em)->where('device_type','=',1)->get()->toArray(); 
            $device_token =DB::table('ss')->select('device_token')->whereIn('email',$email)->where('device_type','=',1)->get();
                               
          
           $res=\App\Lib\Push::android_push($device_token, $msg =$param['message'], $title = $param['title'], $badge = [1], $screen = array(), $meta = array());
           return $res;
       } 
       else{
     
//          $device_token = \App\Models\Serviceprovider\ServiceProvider::Select('device_token')->where('email',$em)->where('device_type','=','1')->get()->toArray();
            $device_token =DB::table('sp')->select('device_token')->whereIn('email',$email)->where('device_type','=',1)->get();
   
           $res=\App\Lib\Push::android_push($device_token, $msg =$param['message'], $title = $param['title'], $badge = [1], $screen = array(), $meta = array());
           return $res ;
       }
        
    }
    
    public function getBulkSms() {

        if (!\Auth::guard('admin')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Bulk SMS | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-bulk-sms',
                'logger'=> 'A'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.bulk_sms_list", $view_data);
    }
    
    public static  function postBulkSmsFilter(){
        
        $param = \Input::all();
        $data = \App\Models\Users::getUserListForBulkSms($param);
        
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
             
            return view("admin.bulk_sms_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public static  function postNewBulkSms(){
              
        $param = \Input::all();
        
        $email=explode(",",$param['email']);
        $data = \App\Models\Users::select('mobile')->whereIn('email',$email)->get()->toArray();
       
        foreach($data as $key){
          foreach($key as $mobileno){
              if($mobileno==''){
                  return \General::error_res("Mobile no not found");
              }else{
                  $res=  \App\Models\Services\General::sendSMS($mobileno,$param['message'],'Bustiket');
                  if($res=='') {
                        $msgstatus="Page error";
                        $smsstatus=2;
                    } elseif($res=='nocredit') {
                        $msgstatus="No sms balance";
                        $smsstatus=2;
                    } elseif($res<0) {
                        $msgstatus="Number error";
                        $smsstatus=2;
                    } elseif(strlen($res)>15) {
                        $msgstatus="progress";	
                        $smsstatus=0;
                    } else {
                        $msgstatus="failed";
                        $smsstatus=2;
                    }

                   return \General::error_res($msgstatus);
              }
          }
        }
    }
    
    public function getReports(){
        
        $ctype = config('constant.SS_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        $parent = config('constant.SS_PARENT') ;
        $param = [];
        $param['id'] = $cid;
        $smdata = json_encode([]);
        $sadata = json_encode([]);
        $sbdata = json_encode([]);
        $tsale = json_encode([]);
        $smcomm = json_encode([]);
        $sabcomm = json_encode([]);
        $smonth = json_encode([]);
        $sdaily = json_encode([]);
        $ssasale = json_encode([]);
        $ssbsale = json_encode([]);
        
        
//            $param['book_by'] = 0;
            $smdata = \App\Models\General::get_total_sale($param);
            
            $ids = \App\Models\Admin\SeatSeller::where('type','Main')->pluck('id');
//            dd($ids);
//            $param['child'] = $ids;
            $param['book_by'] = [2,3,4];
            $sadata = \App\Models\General::get_total_sale($param);
            $param['book_by'] = [5,6,7];
            $sbdata = \App\Models\General::get_total_sale($param);
//            $param['book_by'] = [5,6,7];
            $tsale = \App\Models\General::get_total_sale_amount($param);
//            unset($param['child']);
            $smcomm = \App\Models\General::get_total_commission_from_sp($param);
            $param['child'] = $ids;
            $sabcomm = \App\Models\Admin\SeatSeller::get_total_commission_of_ss($param);
//            
            $smonth = \App\Models\General::get_total_sale_by_month($param);
//            
            $sdaily = \App\Models\General::get_total_daily_sale($param);
            unset($param['child']);
            $param['book_by'] = '2,3,4';
            $ssasale = Serviceprovider\ServiceProvider::get_child_by_type($param);
            $param['book_by'] = '5,6,7';
            $ssbsale = \App\Models\Admin\SeatSeller::get_child_by_type($param);
       
        
        $view_data = [
            'header' => [
                "title" => 'Reports | Seat Seller Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'reporting',
                'logger'=> 'SS',
                'smdata'=>$smdata,
                'sadata'=>$sadata,
                'sbdata'=>$sbdata,
                'tsale'=>$tsale,
                'smcomm'=>$smcomm,
                'sabcomm'=>$sabcomm,
                'smonth'=>$smonth,
                'sdaily'=>$sdaily,
                'ssasale'=>$ssasale,
                'ssbsale'=>$ssbsale,
            ],
            'footer' => [
                "js" => ['d3.min.js','nv.d3.js','stream_layers.js','jchart.js'
//                    ,'charts.min.js'
                    ,'charts_temp.js'
                    ],
                "css" => ['nv.d3.min.css']
            ],
        ];
        
        return view("admin.admin_reports", $view_data);
    }
    
    public function getCancelTicketPolicy(){
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Drivers',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id'=>'cp',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.cancel_policy", $view_data);
    }
    public function postPolicyFilter(){
        $param = \Input::all();
        
        $data = \App\Models\CancelPolicy::get_policy_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.policy_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postAddNewPolicy(){
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "policy"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $addD = \App\Models\CancelPolicy::add_new_policy($param);
        return $addD;
    }
    public function postEditPolicy(){
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "policy"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $addD = \App\Models\CancelPolicy::edit_policy($param);
        return $addD;
    }
    public function postDeletePolicy(){
        $param = \Input::all();
        
        return \App\Models\CancelPolicy::delete_policy($param);
    }
    public function postCsvRoute(){
        $param = \Input::all();
        $csv = \Request::file('csv');
        if(!isset($param['bus_id'])){
            return \General::error_res('bus id required.');
        }
        return \App\Models\General::csv_routes($csv,$param);
    }
    public function getCached(){
        
        $key = isset($_REQUEST['key']) ? $_REQUEST['key'] : '';
        $data = [];
        if($key == 'all_term'){
            $chk = 'lorena_terminal_list';
            if(\Cache::has($chk)){
                $data = \Cache::get($chk);
            }
        }elseif($key == 'bus_list'){
            $chk = 'lorena_bus_list';
            if(\Cache::has($chk)){
                $data = \Cache::get($chk);
            }
        }elseif($key == 'bus_info'){
            $chk = 'lorena_bus_info';
            if(\Cache::has($chk)){
                $data = \Cache::get($chk);
            }
        }
        $view_data = [
            'header' => [
                "title" => 'Dashboard | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'Dashboard',
                'key'=>$key,
                'data'=>$data
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.cached", $view_data);
    }
    
   
      
   
  
    
    
    public function getCreateCouponCodeCsv() {
        
        $data=  \App\Models\CouponCode::select('id','status','code','start_date','end_date','type','value','min_amount','max_amount')->get();
        
        $arrColumns = array('id','status','code','start_date','end_date','type','value','min_amount','max_amount');
	     
	    // define the first row which will come as the first row in the csv
	$arrFirstRow = array('id','status','code','start_date','end_date','type','value','min_amount','max_amount');
	     
	    // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
	    // creating the Files object from the Utility package.
	 
	return \App\Models\General::convertToCSV($data, $options);
    }
    
    public function postUploadCouponCodeCsv(){
        $new_coupon=array();
        $param = \Input::all();
        $csv = \Request::file('csv');
        $csv_ext = array('csv');
        if(isset($csv)){
            $ext = $csv->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$csv_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $csv_ext);
                $res = response()->json($json,200);
                return $res;
            }
        }else{
            return \General::error_res('please select valid csv file.');
        }
//        dd($csv);
        
        $csvAr = array();
       
        $file = fopen($csv->getPathName(), 'r');
        $all_data = array();
        $cnt=0;$ncc=0;$ecc=0;
        while ( ($data = fgetcsv($file, 200, ",")) !==FALSE) {
           if($cnt==0){
                
                $cnt++;
                continue; 
           }
           
            $coupon['ccid'] = isset($data[0])?$data[0]:'';
            $coupon['cstatus'] = $data[1];
            $coupon['ccode']=$data[2];
            $coupon['csdate']=$data[3];
            $coupon['cedate']=$data[4];
            $coupon['ctype']=$data[5];
            $coupon['cvalue']=$data[6];
            $coupon['cminvalue']=$data[7];
            $coupon['cmaxvalue']=$data[8];
            
            $data=\App\Models\CouponCode::where('id',$coupon['ccid'])->first();
            if(is_null($data)){
                $res=\App\Models\CouponCode::import_add_new_coupon_code($coupon);
                 array_push($new_coupon,$res['data']);
                 $ncc++;
                
            }else{
                $res=\App\Models\CouponCode::import_edit_coupon_code($coupon);
                $ecc++;
            }
            
//           if($cnt==100){break;}
            $cnt++;
         }
         fclose($file);
         
        $sres=\General::success_res('CSV File Uploaded');
        $sres['new_coupon_id']=$new_coupon;
        $sres['new_coupon_id_count']=$ncc;
        $sres['edit_coupon_id_count']=$ecc;
        $sres['loop_count']=$cnt-1;
        return $sres;
        
    }
}
