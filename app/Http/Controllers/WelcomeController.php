<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\User;
use Mail;
use Log;
use Input;
use Session;
use DB;

class WelcomeController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $type = [
            'P' => 'province',
            'C' => 'city',
            'D' => 'district',
            'T' => 'terminal',
        ];
        
        
        $banners = \App\Models\Admin\Banners::active()->where('group', 'header-slider')->get()->toArray();
//        dd($banners);

        $mobile_banners = \App\Models\Admin\Banners::active()->where('group', 'web-mobile-footer-slider')->get()->toArray();
//        dd($mobile_banners);

        $rlist = \App\Models\Admin\FavoriteRoute::get_fav_routes();
        $footer_fav_list = \App\Models\Admin\FavoriteRoute::get_footer_fav_routes();
        $testimoniallist = \App\Models\Admin\Testimonial::getTestimonialListAll();
        
//        dd($testimoniallist);

//        $districts = \App\Models\Locality\District::get_locDist();
//        $city = \App\Models\Locality\Citi::get_locCity();
//        $province = \App\Models\Locality\Province::get_locPro();
//        $terminal = \App\Models\Locality\Terminal::get_locTerm();
//        dd($province, $districts,$city,$terminal);
//        $locality = \App\Models\Locality::where('type','C')->orWhere('type','T')->get()->toArray();
        $locality = \App\Models\Locality::orderBy('type','ASC')->orderBy('name','ASC')->get()->toArray();
        $r = [];
        foreach($locality as $l){
//            $type = 'city';
//            if($l['type'] == 'T'){
//                $type = 'terminal';
//            }
            $d = [
                'id' => $l['lid'],
                'type' => $type[$l['type']],
                'name' => trim($l['name']),
            ];
            $r[] = $d;
        }
//        dd($r);
       /* foreach ($province['data'] as $x) {
            $d = [
                'id' => $x['id'],
                'type' => 'province',
                'name' => $x['name'],
            ];
            $r[] = $d;
            foreach ($city['data'] as $y) {
                if ($y['province_id'] == $x['id']) {
                    $d1 = [
                        'id' => $y['id'],
                        'type' => 'city',
                        'name' => $y['name'],
                    ];
                    $r[] = $d1;

                    foreach ($districts['data'] as $i) {
                        if ($i['city_id'] == $y['id']) {
                            $d2 = [
                                'id' => $i['id'],
                                'type' => 'district',
                                'name' => $i['name'],
                            ];
                            $r[] = $d2;
                            foreach ($terminal['data'] as $j) {
                                if ($j['district_id'] == $i['id']) {
                                    $d3 = [
                                        'id' => $j['id'],
                                        'type' => 'terminal',
                                        'name' => $j['name'],
                                    ];
                                    $r[] = $d3;
                                }
                            }
                        }
                    }
                }
            }
        } */

//        dd($r);
//        foreach($districts['data'] as $x){
//            $d=[
//                'id'=> $x['id'],
//                'type'=> 'district',
//                'name'=> $x['name'],
//               ];
//            $r[]=  $d;
//            
//            foreach($city['data'] as $y){
//                if($y['district_id'] == $x['id']){
//                    $d1=[
//                        'id'=> $y['id'],
//                        'type'=> 'city',
//                        'name'=> $y['name'],
//                       ];
//                    $r[]=  $d1;
//                }
//            }
//        }
//        $sesson_id = session()->getId();
//        session(['session_id' => $sesson_id]);
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"],
            ],
            'body' => [
                'banners' => $banners,
                'locations' => json_encode($r),
                'fav_routes' => $rlist,
                'footer_fav_routes' => $footer_fav_list,
                'testimonial'=> $testimoniallist,
                'mobile_footer_banners'=> $mobile_banners,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("site.index", $view_data);
    }
    
    public function postFooterFavList(){
        $footer_fav_list = \App\Models\Admin\FavoriteRoute::get_footer_fav_routes();
//        dd(count($footer_fav_list));
        if(count($footer_fav_list) == 0){
            $res = \General::error_res('No Routes found.');
            return \Response::json($res,200);
        }
        $res = \General::success_res();
        $res['data'] = $footer_fav_list;
        return \Response::json($res,200);
        
    }

    public function from() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.fromcity", $view_data);
    }
    
    public function aboutUs() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.about", $view_data);
    }

    
    public function doc(){
        
      $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => ["assets/doc/jquery.min.js","assets/doc/polyfill.js","assets/doc/bootstrap.min.js","assets/doc/prettify.js"],
                "css" => ["assets/doc/bootstrap.min.css", "assets/doc/prettify.css","assets/doc/style.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.docs",$view_data);  
        
    }
    
    
    public function contactUs() {
        if (request()->isMethod('post')) {
            $resp= \App\Models\General::contactUsValidation(\Input::all());
            if($resp['flag']==1)   
            {
                $res = \App\Models\General::sendContactUsMail(\Input::all());
                return $res;
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        }
        else
        {
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("site.contact_us", $view_data);
        }
    }
    
        public function faq() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.faq", $view_data);
    }
    public function howToOrder() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.how_to_order", $view_data);
    }
    public function paymentConfirmation() {
        if (request()->isMethod('post')) {
//           dd(\Input::all());
            $resp= \App\Models\General::paymentCnfFormValidation(\Input::all());
            if($resp['flag']==1)   
            {
                $res = \App\Models\General::sendPaymentConfirmationMail(\Input::all());
                return $res;
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        }
        else
        {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.payment_confirmation", $view_data);
        }
    }
    public function checkBooking() {
        
//        \App\lib\Services::test_sms_send();
        
        if (request()->isMethod('post')) {
            $param = \Input::all();
            $resp= \App\Models\General::checkBookingFormValidation(\Input::all());
            if($resp['flag']==1)   
            {
//                dd($param);
                $tik_id = $param['bookingData']['ticketNumber'];
                $email = $param['bookingData']['email'];
                $type = $param['bookingData']['flags'];
                $flag = 1;
                if($type == 1){
                    $flag = 0;
                }
                
                $chekBook = \App\Models\BookingSeats::check_booking_exist($tik_id,$email,$flag);
                if($chekBook['flag'] != 1){
                    return $chekBook;
                }
                if($type == 2){
                    $tiket_detail = \App\Models\Bookings::where('booker_email',$email)->whereHas('bookingSeats',function($q)use($tik_id){
                        $q->where('ticket_id',$tik_id);
                    })->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//                    dd($tiket_detail);
                    if($tiket_detail['status'] != 1){
                        
                        return \General::error_res('ticket is not confirmed yet.');
                    }
                        $tiket_detail['mail_subject'] = "E-tiket Detail Perjalanan Anda dari www.bustiket.com";
                        $file_path = config('constant.TICKET_PDF_PATH').'bustiket_'.$tiket_detail['booking_id'].'.pdf';
                        $html = \Mail::send('site.e_ticket', array('data'=>$tiket_detail), function ($message) use ($tiket_detail,$file_path) {
                            $message->to($tiket_detail['booker_email'])->subject($tiket_detail['mail_subject']);
                            $message->attach(($file_path));
                        });
                        $chekBook['msg'] = 'email sent succussfully.';
                    return $chekBook;
                }else if($type == 1){
                    $sms = \App\lib\Services::send_sms($tik_id);
                    return $sms;
                }
                $chekBook['flag'] = 3;
                return $chekBook;
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        }
        else
        {
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css","assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("site.check_booking", $view_data);
        }
    }
    public function tnc() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.tnc", $view_data);
    }

    public function privacyPolicy() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.privacyPolicy", $view_data);
    }

    public function cancelTicket() {
        if (request()->isMethod('post')) {
            $resp= \App\Models\General::cancelTicketFormValidation(\Input::all());
            $param = \Input::all();
            if($resp['flag']==1)   
            {
                $tik_id = $param['bookingData']['ticketNumber'];
                $email = $param['bookingData']['email'];
                $flag = 1;
                
                $chekBook = \App\Models\BookingSeats::check_booking_exist($tik_id,$email,$flag);
                if($chekBook['flag'] != 1){
                    return $chekBook;
                }
                $tiket_detail = \App\Models\Bookings::where('booker_email',$email)->whereHas('bookingSeats',function($q)use($tik_id){
                    $q->where('ticket_id',$tik_id)->where('status',1);
                })->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first();
//                    dd($tiket_detail);
                if($tiket_detail){
                    $tiket_detail = $tiket_detail->toArray();
                }
                if($tiket_detail['status'] != 1){

                    return \General::error_res('ticket is not confirmed yet or already cancelled');
                }
                
                $view_data = [
                    'header' => [
                        "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                        "js" => [],
                        "css" => ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css']
                    ],
                    'body' => [
                        'data'=>$tiket_detail,
                        'ticket_id'=>$tik_id,
                    ],
                    'footer' => [
                        "js" => [],
                        "css" => []
                    ],
                ];
                
                return view("site.ticket_list", $view_data);
                
                
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        }
        else
        {
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => ['assets/js/jquery.form.min.js',"assets/js/common.min.js"],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("site.cancel_ticket", $view_data);
        }
    }

    public function career() {
        if (request()->isMethod('post')) {
            $resp= \App\Models\General::careerFormValidation(\Input::all());
            if($resp['flag']==1)   
            {
                $res = \App\Models\General::sendCareerMail(\Input::all());
                \Session::set("msg",json_encode($res));
                return redirect("career");
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
            
        } else {
            $session = \Session::get("msg");
            $msg = [];
            if($session != "")
            {
                $msg = json_decode($session,true);
                \Session::forget("msg");
            }
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                    "msg" => $msg
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            
            return view("site.career", $view_data);
        }
    }

    public function partnerBusticket() {
        $msg = '';
        $msg = \Session::get('msg');
        $flag = 0;
        if (request()->isMethod('post')) {
            $msg = 'Thank you. Your request has been registred. We will contact you soon.';
            $param = \Input::all();
            if(isset($param['submit'])){
                $flag = 1;
                $res = \App\Models\General::sendPartnerMail($param);
//                dd($param);
            }
        }
        
//        \Session::forget('msg');
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
                'msg'=>$msg
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
//        echo $msg;
        
        if($flag){
            \Session::put('msg', $msg);
            return redirect('/partner');
        }
        
        return view("site.partner_busticket", $view_data);
    }

    public function companyBus() {
        if (request()->isMethod('post')) {
            $resp= \App\Models\General::companyBusFormValidation(\Input::all());
            if($resp['flag']==1)   
            {
                $res = \App\Models\General::sendCompanyBusMail(\Input::all());
                return $res;
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        }
        else
        {
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("site.company_bus", $view_data);
        }
    }
    public function sendMailNewSubscriber() {
       
        if (request()->isMethod('post')) {
            
            $resp= \App\Models\General::subscribeFormValidation(\Input::all());
            if($resp['flag']==1)   
            {
                $res = \App\Models\General::sendNewSubscriberMail(\Input::all());
                return $res;
            }
            else{
                return \General::error_res(json_encode($resp['va']));
            }
        } 
    }

    public function requestOperator() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.request_operator", $view_data);
    }

    public function forgotPassword() {
        $view_data = [
            'header' => [
                "title" => "",
                "active_menu" => "active_login",
                "js" => [""],
                "css" => [""]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.forgotPassword", $view_data);
    }

    public function signin() {
        $view_data = [
            'header' => [
                "title" => "Signup & Login | Romspos",
                "description" => "RomsPOS is the fastest & smartest way to handle restaurant's orders",
                "keywords" => "",
                "url" => \URL::to("/login"),
                "active_menu" => "active_login",
                "js" => [""],
                "css" => [""]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.signin", $view_data);
    }

    public function registration() {
        $view_data = [
            'header' => [
                "title" => "Signup & Login | Romspos",
                "description" => "RomsPOS is the fastest & smartest way to handle restaurant's orders",
                "keywords" => "",
                "url" => \URL::to("/login"),
                "active_menu" => "active_login",
                "js" => [""],
                "css" => [""]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.registration", $view_data);
    }

    public function getQrCode($id) {

        $ticket_no = $id;
        $qrImg = "qr_" . $ticket_no . '.png';
        $dir_path = config('constant.QRCODE_PATH');

        $data = \DNS2D::getBarcodePNG($ticket_no, "QRCODE", 5, 5);
        $data = base64_decode($data);
        file_put_contents($dir_path . $qrImg, $data);

//        return Image::make($qrImg)->response();
//                return response()->file($dir_path);
        return \Response::download($dir_path . $qrImg);
    }

    public function userLogin() {
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view("site.login", $view_data);
    }
    
    public function cityList(){
        if (request()->isMethod('post')) {
            $cityArray=array();
            $json_array=array();
            $sval=\Input::all();
            $lval='%'.$sval['sval'].'%';
            $citylist=DB::table('cities')->select('name')->where('name','like',$lval)->OrderBy('name','asc')->get();
            foreach($citylist as $key=>$value)
            {
                $cityArray[]=$value['name'];
            }
            if(count($cityArray) > 0){
                 $json_array['flag']=1;
                 $json_array['msg']="Success";
                 $json_array['data']=$cityArray;
                 return $json_array;
            }else{
                 return \General::error_res("No Data found");
            }
           
        }else{ 
            $cityArray = \Cache::get('citylist');

            if(!is_null($cityArray)){
                $cityArray = json_decode($cityArray,true);
            }
            else{
                $cityArray = [];
                $citylist = DB::table('cities')->OrderBy('name','asc')->get();
                foreach($citylist as $key=>$value)
                {
                    $cityArray[] = $value['name'];
                }
    
                \Cache::rememberForever('citylist',function() use($cityArray){
                    return json_encode($cityArray);
                });
            }
            
            $view_data = [
                'header'    => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js"    => [],
                    "css"   => ["assets/css/app.min.css","assets/css/app.css", "assets/css/search.min.css","assets/css/bootstrap.css","assets/css/font-awesome.min.css"]
                ],
                'body'      => [$cityArray],
                'footer'    => [
                    "js"    => [],
                    "css"   => []
                ],
            ];
            return view("site.city_list", $view_data);
        }
    }
     
    
    public function cityListFrom($cityname) {
        
        $cityArray = \Cache::get('citylist');

        if(!is_null($cityArray)){
            $cityArray = json_decode($cityArray,true);
        }
        else{
            $cityArray = [];
            $citylist = DB::table('cities')->OrderBy('name','asc')->get();
            foreach($citylist as $key=>$value)
            {
                $cityArray[] = $value['name'];
            }

            \Cache::rememberForever('citylist',function() use($cityArray){
                return json_encode($cityArray);
            });
        }
            
        
        
//        $cityArray = [];
//        $citylist=DB::table('cities')->OrderBy('name','asc')->get();
//        foreach($citylist as $key=>$value)
//        {
//            $cityArray[]=$value['name'];
//        }
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css","assets/css/bootstrap.css","assets/css/font-awesome/css/font-awesome.min.css"]
            ],
            'body' => [$cityArray,$cityname],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.city_list_from", $view_data);
    }
    
    public function allRoute() {
        $cityArray = \Cache::get('citylist');

        if(!is_null($cityArray)){
            $cityArray = json_decode($cityArray,true);
        }
        else{
            $cityArray = [];
            $citylist = DB::table('cities')->OrderBy('name','asc')->get();
            foreach($citylist as $key=>$value)
            {
                $cityArray[] = $value['name'];
            }

            \Cache::rememberForever('citylist',function() use($cityArray){
                return json_encode($cityArray);
            });
        }
        
//        dd(!is_null($cityArray));
        
//        $cityArray=array();
//        $citylist=DB::table('cities')->select('name')->OrderBy('name','asc')->get();
//        foreach($citylist as $key=>$value)
//        {
//            $cityArray[]=$value['name'];
//        }
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css","assets/css/bootstrap.css","assets/css/font-awesome.min.css"]
            ],
            'body' => [$cityArray],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.all_route", $view_data);
    }
    
    
    public function sendTicketMail($id = 0,$email = ''){
        /* if error occures here than compare this pdf generation code with downloadpdf.php file's pdf generation code */
        if($id == 0){
            return 'please enter valid id';
        }
        $ticket_detail = \App\Models\Bookings::where('booking_id',$id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first();
        if(!$ticket_detail){
            return 'no booking found ';
        }
        if($email != ''){
            $param = ['email'=>$email];
            $validator = \Validator::make($param, [
                'email'  => 'email',
                ]);
            if($validator->fails()){
                return 'please enter valid email id';
            }
        }
        $ticket_detail=$ticket_detail->toArray();
//        dd($ticket_detail);
        $ticket_detail['mail_subject'] = "E-tiket Detail Perjalanan Anda dari www.bustiket.com";
       
        $file_path = config('constant.TICKET_PDF_PATH').'bustiket_'.$ticket_detail['booking_id'].'.pdf';
        
        
//        $pdf = \PDF::loadView("site.e_ticket_mail",array('data'=>$ticket_detail))->save($file_path);
        $pdf = \PDF::loadView("site.e_ticket_mail",array('data'=>$ticket_detail), [], [
                                                    'margin_top' => 0,
                                                    'margin_left' => 0,
                                                    'margin_bottom' => 0,
                                                    'margin_right' => 0,
                                                  ])->save($file_path);
        
        if($email != ''){
            $ticket_detail['booker_email'] = $email;
        }
        
        $html = \Mail::send('site.e_ticket', array('data'=>$ticket_detail), function ($message) use ($ticket_detail,$file_path) {
            $message->to($ticket_detail['booker_email'])->subject($ticket_detail['mail_subject']);
            $message->to("info@bustiket.com")->subject($ticket_detail['mail_subject']);
            $message->attach(($file_path));
        });
        
        return 'ticket successfully sent to '.$ticket_detail['booker_email'];
    }
    
    public function removeBlockSeat(){
        
//        $seats = \App\Models\BookingSeats::where('booking_id',null)->whereRaw('NOW( ) >= DATE_ADD( created_at, INTERVAL ? HOUR )', [1])->orderBy('id','desc')->get()->toArray();
        $now = date('Y-m-d H:i:s');
        $seats = \App\Models\BookingSeats::where('booking_id',null)->whereRaw('"'.$now.'" >= DATE_ADD( created_at, INTERVAL ? hour )', [1])->orderBy('id','desc')->get()->toArray();
        echo count($seats). ' blocked seats found.<br>';
        if($seats){
            $seats_delete = \App\Models\BookingSeats::where('booking_id',null)->whereRaw('"'.$now.'" >= DATE_ADD( created_at, INTERVAL ? hour )', [1])->delete();
            echo count($seats). ' blocked seats deleted.<br>';
        }
        
        
    }
}
