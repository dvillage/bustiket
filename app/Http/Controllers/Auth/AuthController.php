<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use \App\Models\SignupHistory;
use Illuminate\Http\Request;
use \Illuminate\Http\Exception\HttpResponseException;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $request;
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    
    public function __construct(Request $request)
    {
//        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
//        $this->middleware("throttle:2",['except' => 'logout']);
        $this->request = $request;
    }

    public function doLogin($input)
    {
        $res =  $this->validateLoginData($input)
             ->authenticate($input);
        return $res;
        
    }

    public function doSignUp($input)
    {
        $res =  $this->validatesignUpData($input)
             ->checkMultipleSignUp($input)
             ->signUp($input);
        return $res;
        
    }
    
    public function checkMultipleSignUp($input)
    {
        $signup_history = SignupHistory::where("ip",\Request::getClientIp())->first();
        if(!is_null($signup_history))
        {
            if(count($signup_history->ua) >= config("constant.MAX_SIGNUP_PER_IP"))
            {
                $res = response()->json(\General::validation_error_res("Multiple signup"),422);
                throw new HttpResponseException($res);
            }
        }
        return $this;
    }
    
    public function validateSignUpData($input)
    {
        $rules = [
                'email' => 'required|email|unique:users,email',
                'password' => 'sometimes|min:6',
                'postal_code' => 'bail|required|max:10', //exists:postal_code,code
                'postal_code_lat' => 'required',
                'postal_code_long' => 'required'
            
            ];
        
        $this->validate($this->request, $rules);
        return $this;
    }
    
    
    public function validateLoginData($input)
    {
        $rules = [
                'email' => 'required|email',
                'password' => 'sometimes|min:6',
                'type'  => 'required|in:normal,facebook,google'
            ];
        $this->validate($this->request, $rules);
        return $this;
    }
    
    public function signUp($input)
    {
        $user = new User();
        $user->city = new \stdClass();
        $user->country = new \stdClass();
                
        //get city & country id by postalcode
        /*$base_url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCm5tdOUgA4UP-cIl2eDE59Lo8RDjUV_bc&address=";
        //$base_url = "http://maps.googleapis.com/maps/api/geocode/json?address=";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_url.$input['postal_code']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        $buffer = curl_exec($ch);
        
        $res = json_decode($buffer);
        
        if($res->status == 'OK'){
            $address = $res->results[0]->address_components;
            $cityName = "";
            $countryName = "";
            
            foreach($address as $add){
                if(in_array('locality', $add->types)){
                    $cityName = $add->long_name;
                }
                else if(in_array('country', $add->types)){
                    $countryName = $add->long_name;
                }                
            }
            if($cityName != ''){
                $ch1 = curl_init();                
                curl_setopt($ch, CURLOPT_URL, $base_url.$cityName);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                $buffer = curl_exec($ch1);
                $cityRes = json_decode($buffer);                
                
                if($cityRes != false){
                    $cityLoc = $cityRes->results[0]->geometry->location;

                    $city = array();
                    $city['id'] = $cityRes->results[0]->place_id;
                    $city['loc'] = array(
                        "type" => "Point",
                        "coordinates" => [ 
                            $cityLoc->lng,
                            $cityLoc->lat
                        ]
                    );

                    $city['lat'] = $cityLoc->lat;
                    $city['long'] = $cityLoc->lng;
                    $city['name'] = $cityName;
                    $city['image'] = '';
                    $user->city = $city;
                }
            }
            
            if($countryName != ''){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $base_url.$countryName);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                $buffer = curl_exec($ch);
                $countryRes = json_decode($buffer);

                if($countryRes != false){
                    $countryLoc = $countryRes->results[0]->geometry->location;

                    $country = array();            
                    $country['id'] = $countryRes->results[0]->place_id;
                    $country['loc'] = array(
                        "type" => "Point",
                        "coordinates" => [ 
                            $countryLoc->lng,
                            $countryLoc->lat
                        ]
                    );

                    $country['lat'] = $countryLoc->lat;
                    $country['long'] = $countryLoc->lng;
                    $country['name'] = $countryName;
                    $country['image'] = '';            

                    $user->country = $country;
                }
            }
        }*/
        
        $user->email = $input['email'];
        $user->password = \Hash::make($input['password']);
        $user->postal_code = $input['postal_code'];
        $user->firstname = "";
        $user->lastname = "";
        $user->gender = "";
        $user->avatar = "";
        $user->loc = (object) array(
            "type"=>"Point",
            "coordinates"=>array(
                /*\Config::get('constant.DEFAULT_LATITUDE'),
                \Config::get('constant.DEFAULT_LONGITUDE')*/
                (double)$input['postal_code_long'],
                (double)$input['postal_code_lat']
                ));
        $user->table_topics = array();
        $user->table_places = array();
        $user->device_tokens = [];  //$this->validateDeviceToken(array());
        $user->tokens = [];
        $user->status = config("constant.USER_INACTIVE_STATUS");
        $user->mail_verification = config("constant.INACTIVE_STATUS");
        $user->distance_measure = \Config::get('constant.DISTANCE_MILE');
        $user->distance_limit = \Config::get('constant.DEFAULT_DISTANCE_LIMIT');
        $user->table_alert = \Config::get('constant.DEFAULT_TABLE_ALERT');
        $user->alert_gender_only = \Config::get('constant.DEFAULT_ALERT_GENDER_ONLY');
        $user->is_chat_active = \Config::get('constant.DEFAULT_CHAT_ACTIVE');
        $user->private_table_page = \Config::get('constant.DEFAULT_PRIVATE_TABLE_PAGE');
        $user->signup_platform = $this->getDevicePlatform();
        //$user->wall = (object) array('image'=>array(),"video"=>array());
        $user->wall = array();
        $user->cover = "";
        $user->private_wall = \Config::get('constant.DEFAULT_WALL_PRIVACY');
        $user->hasProfileCompleted = 0;
        $user->save();
        
        $trigger_data= \Event::fire(new \App\Events\Signup($user));
        
        $res = \General::success_res("Your signup is successful.");
        //$res['data'] = $user->toArray();
        //$res['data']['auth_token'] = $trigger_data[0];
        return \Response::json($res);
    }
    
    public function authenticate($input){
        if (($input['type'] == "facebook" || $input['type'] == "google") && (!isset($input['access_token']) || $input['access_token'] == "")) {
            $res = response()->json(\General::validation_error_res("access_token_missing"),422);
                throw new HttpResponseException($res);
        }
        
        if ($input['type'] == "facebook") {
            \Log::info($input);
            $res = \App\Models\Services\General::check_facebook_access_token($input['access_token']);
            if ($res['flag'] != 1)
            {
                $res = response()->json($res,422);
                throw new HttpResponseException($res);
            }
            $input['email'] = $res['data']['email'];
        } elseif ($input['type'] == "google") {
            $res = \App\Models\Services\General::check_google_access_token($input['access_token']);
            if ($res['flag'] != 1)
            {
                $res = response()->json($res,422);
                throw new HttpResponseException($res);
            }
            $input['email'] = $res['data']['email'];
        }
        $user = \App\Models\User::where("email", $input['email'])->first();
        if (is_null($user))
        {
            $res = response()->json(\General::error_res("invalid_email_password"),422);
            if($input['type'] != "normal")
            {
                $res = response()->json(\General::error_res("You are not registerd with us using this email. Please Signup first"),422);
            }
            throw new HttpResponseException($res);
        }
        if ($input['type'] == "normal" && !\Hash::check($input['password'], $user->password)) {
            $res = response()->json(\General::error_res("invalid_email_password"),422);
                throw new HttpResponseException($res);
        }
        
        $platform = $this->getDevicePlatform();
        
        if($platform == \Config::get('constant.ANDROID_APP_DEVICE') || $platform == \Config::get('constant.IPHONE_APP_DEVICE')){
            $device_token = $this->getDeviceToken();
            $count = User::where('device_tokens.token', $device_token)->count();
            if($count > 0){
                \General::remove_device_token($device_token);                
            }
            
            $device_tokens = $user->device_tokens;
            $device_tokens[] = array(
                'platform' => $this->getDevicePlatform(), 
                'token' => $device_token,
                'ua' => \Request::server("HTTP_USER_AGENT"),
                'ip' => \Request::getClientIp(),
                'created_at'=> strtotime(date("Y-m-d H:i:s")) * 1000);
            $user->device_tokens = $device_tokens;            
        }
        
        if($user->status == \Config::get("constant.USER_INACTIVE_STATUS"))
        {
            $res = response()->json(\General::mobile_verify_error_res("mobile_not_verified"),422);
            
            throw new HttpResponseException($res);
        }
        else if ($user->status == \Config::get("constant.USER_SUSPEND_STATUS")) {
            $res = response()->json(\General::error_res("account_suspended"),422);
            throw new HttpResponseException($res);
        }

        \Log::info($user);
        $token = \App\Models\User::generate_new_token();
        $tokens = $user->tokens;
        $tokens[] = $token;
        $user->tokens = $tokens;
        $user->save();
        
        //$user_data = $user->toArray();
        $user_data = array(
            'firstname' => $user->firstaname,
            'lastname'  => $user->lastname,
            '_id'       => $user->_id,
            'email'     => $user->email,
            'gender'    => $user->gender,
            'avatar'    => $user->avatar,
            'distance_measure'  => $user->distance_measure,
            'distance_limit'    => $user->distance_limit,
            'table_alert'       => $user->table_alert,
            'alert_gender_only' => $user->alert_gender_only,
            'is_chat_active'    => $user->is_chat_active,
            'private_table_page'=> $user->private_table_page,
            'hasProfileCompleted'=> $user->hasProfileCompleted,
            'auth_token'=> $user->auth_token,
            'city'=> $user->city,
            'country' => $user->country,
            'lat'=>$user->loc['coordinates'][1],
            'long'=>$user->loc['coordinates'][0]
        );
        
        $settings = app('settings');
        if(isset($settings['foursquare'])){
            $foursquare = json_decode($settings['foursquare']);
            $user_data['foursquare_client_id'] = $foursquare->client_id;
            $user_data['foursquare_client_secret'] = $foursquare->client_secret;
        }
        else{
            $user_data['foursquare_client_id'] = '';
            $user_data['foursquare_client_secret'] = '';
        }

        $user_data['socket_url'] = env("SOCKET_URL","http://192.168.0.104:7273");
        
        /*$user_data['hasProfileCompleted'] = 1;
        
        if($user_data['firstname'] == "" || $user_data['lastname'] == "" || $user_data['avatar'] == ""){
            $user_data['hasProfileCompleted'] = 0;
        }
        else if(!($user_data["gender"] == \Config::get('constant.USER_GENDER_MALE') || $user_data["gender"] == \Config::get('constant.USER_GENDER_FEMALE'))){
            $user_data['hasProfileCompleted'] = 0;
        }
        //return $user_data;
        else if(!(isset($user_data['city']['_id']) && isset($user_data['country']['_id']))){
            $user_data['hasProfileCompleted'] = 0;
        }*/
        
        $user_data['auth_token'] = $user->_id."_".$token['token'];
        $res = \General::success_res("login");
        //$user_data['socket_url'] = \URL::to("/");
//        $user_data['socket_url'] = 'http://'.$_SERVER['SERVER_NAME'].':'.\Config::get('constant.SOCKET_PORT').'/';
        $res['data']['socket_url'] = env("SOCKET_URL","http://192.168.0.104:7273");
        $res['data'] = $user_data;
//        $res['socket_url'] = \Config::get('constant.SOCKET_URL');
        return \Response::json($res);
    }    
    
    public function validateDeviceToken($device_tokens){
        $active = false;
        
        $device_token = $this->getDeviceToken();
        if(!$active){
            foreach ($device_tokens as $token){
                $created = $token['created_at']/1000;
                $expiry = $created + (\Config::get('constant.DEVICE_TOKEN_EXPIRE_HOURS')*\Config::get('constant.HOUR_TIMESTAMP'));
                $current = strtotime("now");

                if($expiry<$current){
                    $device_tokens = \General::search_and_splice("token",$token['token'],$device_tokens);
                }
            }
            
            $device_tokens[] = array(
                'platform' => $this->getDevicePlatform(), 
                'token' => $device_token,
                'ua' => \Request::server("HTTP_USER_AGENT"),
                'ip' => \Request::getClientIp(),
                'created_at'=> strtotime(date("Y-m-d H:i:s")) * 1000);
        }
        
        return $device_tokens;
    }
    
    public function getDeviceToken(){
        $ua = \Request::server("HTTP_USER_AGENT");
        $ary = explode(' ', $ua);
        return isset($ary[2]) ? $ary[2] : '';
    }
    
    public function getDevicePlatform(){
        $platform = \Config::get('constant.WEB_DEVICE');
        
        $ua = \Request::server("HTTP_USER_AGENT");
        
        $ary = explode(' ', $ua);
        if($ary[0] == 'android'){
            $platform = \Config::get('constant.ANDROID_APP_DEVICE');
        }
        else if($ary[0] == 'iphone'){
            $platform = \Config::get('constant.IPHONE_APP_DEVICE');
        }
        
        return $platform;
    }
}
