<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class RodaController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin'];
    private static $bypass_url_for_sp = ['getIndex', 'getLogin', 'postLogin','getDashboard','getLogout','getProfile','postChangeAdminPassword'];
    private static $logger = '';

    public function __construct() {
//        $this->middleware('sp', ['except' => self::$bypass_url]);
//        
//        $this->middleware('spAccess', ['except' => self::$bypass_url_for_sp]);
//        self::$logger = config('constant.LOGGER');
        $this->middleware('services');
    }
    
}
