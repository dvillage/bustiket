<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\SeatSeller;
use App\Models\Serviceprovider;

class SeatSellerController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin'];
    private static $logger = '';
    private static $bypass_url_for_ss = ['getIndex', 'getLogin', 'postLogin','getDashboard','getLogout','getProfile','postChangeAdminPassword','getPaymentRequest','postPaymentRequestFilter'
        ,'postNewPaymentRequest','getTicketList','getTicketBookNew'
        ,'getTicketDetails','postDelTicket','postApproveTicket','postTlistFilter','postSendEmail','postSendSms','postCancelTicket','postTicketCheckout','getCommissionManagement','postCommlistFilter'
        ,'getCouponCode','postCouponFilter','postEditSeatseller'];
    
    public function __construct() {
        echo config('constant.SS_TYPE');
        $this->middleware('ss', ['except' => self::$bypass_url]);
        $this->middleware('ssAccess', ['except' => self::$bypass_url_for_ss]);
        self::$logger = config('constant.LOGGER');
    }

    public function getIndex() {
        if (!\Auth::guard('ss')->check()) {
//            return \Response::view('errors.401', array(), 401);
            return \Redirect::to('/');
        }

        return \Redirect::to('ss/dashboard');
    }

    public function getDashboard() {
//        exit;
        
        if (!\Auth::guard('ss')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Dashboard | Seat Seller Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'Dashboard',
                'logger'=> 'SS'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        
        return view("admin.ss_dashboard", $view_data);
    }

    
    public function getLogin($sec_token = "") {

        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        if ($sec_token != $s['login_url_token']) {
            return \Response::view('errors.404', array(), 404);
        }

        if (\Auth::guard("ss")->check()) {
            return \Redirect::to("ss/dashboard");
        }
        $view_data = [
            'body'=> [
                'logger' => 'Seat Seller',
                'type' => 'SS'
            ]
        ];
        return view('admin.login',$view_data);
    }

    public function postLogin(Request $req) {
        $view_data = [
            'body'=> [
                'logger' => 'Seat Seller',
                'type' => 'SS'
            ]
        ];
        
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "login"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            
            return view('admin.login',$view_data)->withErrors($validator);
        }
        $param = $req->input();
        $res = SeatSeller::doLogin($param);
        if ($res['flag'] == 0) {
            return view('admin.login',$view_data)->withErrors('Wrong User Id or Password !!');
//            return \Redirect::to('/');
        }
//        dd($res);
        return \Redirect::to("ss/dashboard");
    }

    public function getLogout() {

        \App\Models\SeatSeller\Token::delete_token();
        \Auth::guard('ss')->logout();
        $s = \App\Models\Admin\Settings::get_config('login_url_token');
        return redirect("/ss/login/" . $s['login_url_token']);
    }

    public function getProfile($msg = "") {
        $ss_id = config('constant.CURRENT_LOGIN_ID');
        $type = config('constant.SS_TYPE');
        $sdetail = SeatSeller::where('id',$ss_id)->first()->toArray();
//        dd($sdetail);
        $view_data = [
            'header' => [
                "title" => 'Profile | Seat Seller Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => '',
                'type' =>$type,
                'data' => $sdetail,
                
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ss_profile", $view_data);
    }

    public function postChangeAdminPassword() {
        $param = \Input::all();
//        dd($param);
        $res = SeatSeller::change_admin_password($param);
//        dd($res);
        if (isset($res['flag'])) {
            if ($res['flag'] == 0) {
                return \Redirect::to('admin/profile/' . $res['msg']);
            } else if ($res['flag'] == 1) {
//                return \Redirect::to("admin/dashboard");
                return \Redirect::to("admin/logout");
            }
        }
    }

    
    public function getSeatseller($type='') {

        $mainss='';
        if($type != 'A' && $type != 'B' && $type != 'D'){
            return \Response::view('errors.404', array(), 404);
        }
        if($type!='Main')
            $mainss= \App\Models\Admin\SeatSeller::get_mainss();
        
//        $clist= \App\Models\Locality\Citi::get_allciti();
//        dd($dlist,$dlist);
        $view_data = [
            'header' => [
                "title" => 'Seat Seller | Seat Seller Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'ss-'.$type,
                'plist' => $mainss,
                'logger'=>'SS'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        if($type=='D'){
            return view("admin.seat_seller_deposit", $view_data);
        }else{
            return view("admin.seat_seller", $view_data);
        }
        
    }
    
    public function postSeatSellerFilter() {
        $param = \Input::all();
        $login_id = \Auth::guard('ss')->user()->id;
        $data = \App\Models\Admin\SeatSeller::get_seatsellerfilter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            $res['logger'] = 'SS';
            return view("admin.seat_seller_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postSeatSellerDeposit() {
        $param = \Input::all();
        
        $data = \App\Models\Admin\SeatSellerDeposit::get_deposite_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            
            return view("admin.seat_seller_deposite_request", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    
    public function postApproveSsDeposite() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\Admin\SeatSellerDeposit::approve_deposit($param);
        
        return \Response::json($res, 200);
    }
    public function postDelSeatseller() {
        $param = \Input::all();
        $data = \App\Models\Admin\SeatSeller::delete_seatseller($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }   
    
    public function postAddSeatseller(){
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
            'ssemail'=>'unique:ss,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $addss = \App\Models\Admin\SeatSeller::add_seatseller($param);

        return \Response::json($addss, 200);
    }
    public function postEditSeatseller(){
        $param = \Input::all();
        if(!isset($param['profile'])){
            $checkParent= SeatSeller::check_parent($param['sid'],config('constant.CURRENT_LOGIN_ID'));
            if(!$checkParent){
                return \Response::view('errors.404', array(), 404);
            }
        }
        $addss = \App\Models\Admin\SeatSeller::edit_seatseller($param);
        if(isset($param['profile'])){
            return redirect('ss/profile');
        }
        return \Response::json($addss, 200);
    }
    
    public function getSeatsellerDetails($id=''){
        $ss= \App\Models\Admin\SeatSeller::get_seatseller_details($id);
//        dd($ss);
        
        $view_data = [
            'header' => [
                "title" => 'Seat Seller Details| Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => $ss['type'],
                'id' => 'ss-'.$ss['type'],
                'data' => isset($ss)?$ss:'',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        
        return view("admin.seatseller_details", $view_data);
    }
    public function getPaymentRequest(){
        
        $view_data = [
            'header' => [
                "title" => 'Seat Seller Payment Request | Seat Seller Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'pay_req',
            ],
            'footer' => [
                "js" => ['parsley.min.js'],
                "css" => []
            ],
        ];

        
        return view("admin.payment_request_list", $view_data);
    }
    public function postPaymentRequestFilter() {
        $param = \Input::all();
        $login_id = \Auth::guard('ss')->user()->id;
        $data = \App\Models\Admin\SeatSellerDeposit::get_payment_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['logger'] = 'SS';
            return view("admin.payment_request_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
     
    public function postNewPaymentRequest(){
        $param = \Input::all();
        $param['pay_bank'] = isset($param['pay_bank']) ? 1 : 0;
        $rules = [
            'id'=>'required',
            'amount'=>'required',
        ];
        if($param['pay_bank']){
            $rules['acc_name'] = 'required';
            $rules['acc_number'] = 'required';
        }
        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $add = \App\Models\Admin\SeatSellerDeposit::add_new_payment_request($param);
        return $add;
//        print_r($param);
    }
    public function getTicketBookNew() {
        
        $city = \App\Models\Locality\Citi::get_allciti();
        if(is_null($city)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($city as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $r = json_encode($r);
        
        $view_data = [
            'header' => [
                "title" => 'Book New Ticket | Seat Seller Panel BusTiket',
                "js" => ['parsley.min.js','admin.search.min.js','jquery.inputmask.bundle.min.js'],
                "css" => ['sprite-general.css','admin.search.min.css'],
            ],
            'body' => [
                'id' => 'tkt-book',
                'city_list' => $r,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_book", $view_data);
    }
    
    public function postDelTicket() {
        $param = \Input::all();
//        $data = \App\Models\Bookings::delete_ticket($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or already cancelled or not found.');
        }
        $res = \General::success_res();
        return \Response::json($res, 200);
    }
    
    public function getTicketList($id = 0,$type = -1) {
        $cityList = \App\Models\Locality\Citi::get_allciti();
        $city = [];
        foreach($cityList as $c){
            $city[$c['name']] = null;
        }
//        dd(json_encode($city));
//        dd(config());
        $bb = [];
        if($id > 0 && $type > -1 ){
            $bb['id'] = $id;
            $bb['book_by'] = $type;
        
            $lgr = config('constant.SS_TYPE');
            $ss_id = config('constant.CURRENT_LOGIN_ID') ;
            $parent = -1;
            if($lgr != 'Main'){
                $parent = config('constant.SS_PARENT') ;
            }
            $bookby = ['5','6','7'];
            if($ss_id != $id && $parent != $id && !in_array($type, $bookby)){
                return redirect('ss/ticket-list');
            }
            $user_id = config('constant.CURRENT_LOGIN_ID');
            if($lgr != 'Main'){
                if($user_id != $id){
                    return redirect('ss/ticket-list');
                }
            }
        }
        $view_data = [
            'header' => [
                "title" => 'Ticket List | Seat Seller Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'tkt-book',
                'filter' => $bb,
                'city' => json_encode($city),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_list", $view_data);
    }
    public function postApproveTicket(){
        $param = \Input::all();
//        dd($param['id']);
        $apr = \App\Models\Bookings::approve_ticket($param['id']);
       // dd($apr);
    }
    public function getTicketDetails($id = '') {
//        $id = '09PM06UR18L2U';
        if($id == ''){
            return \Response::view('errors.404', array(), 404);
        }
        $lgr = config('constant.SS_TYPE');
        $ss_id = config('constant.CURRENT_LOGIN_ID') ;
        $data = \App\Models\Bookings::get_ticket_detail($id);
//        dd($data);
        $parent = -1;
        if($lgr != 'Main'){
            $parent = config('constant.SS_PARENT') ;
        }
        $bb = ['5','6','7'];
        if($ss_id != $data['user_id'] && $parent != $data['user_id'] && !in_array($data['book_by'], $bb)){
            return redirect('ss/ticket-list');
        }
        $user_id = config('constant.CURRENT_LOGIN_ID');
        if($lgr != 'Main'){
            if($user_id != $data['user_id']){
                return redirect('ss/ticket-list');
            }
        }
        if($data != 0){
            $view_data = [
                'header' => [
                    "title" => 'Ticket List | Seat Seller Panel BusTiket',
                    "js" => [\URL::to('assets/ckeditor/ckeditor.js')],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-list',
                    'data' => $data,
                    
                ],
                'flag' =>1,
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("admin.ticket_detail", $view_data);
        }
        else{
            return \Response::view('errors.404', array(), 404);
        }
    }
    public function postTlistFilter() {
        $param = \Input::all();
        $lgr = config('constant.SS_TYPE');
        $ss_id = config('constant.CURRENT_LOGIN_ID') ;
        
        if($lgr == 'Main'){
            $ids = SeatSeller::where('parent_id',$ss_id)->pluck('id');
//            dd($ids);
            $param['parent'] = $ids;
        }
        if(!isset($param['user_id'])){
            $param['user_id'] = $ss_id;
        }else{
             unset($param['parent']);
        }
        if(!isset($param['book_by']))
            $param['book_by'] = [5,6,7];
        $data = \App\Models\Bookings::filter_ticketlist($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view("admin.ticketlist_filter", $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function postTicketCheckout(){
        $param = \Input::all();
//        dd($param);
        $lgr = config('constant.SS_TYPE');
        
        $handlefee = SeatSeller::get_handle_fee(config('constant.CURRENT_LOGIN_ID'));

        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( ($param['total_amount'] * $handlefee[0]['handling_fee']) / 100 );
        }
        
        $param['handle_fee'] = $hfval;
        
        $board = json_decode($param['board_point'],true);
        $drop = json_decode($param['drop_point'],true);
//        dd($board);
        $pickdt =  date('Y-m-d H:i:s', strtotime($board['boarding_datetime'])) ;
        $dropdt =  date('Y-m-d H:i:s', strtotime($drop['droping_datetime'])) ;
        
        $fromTer = [
            'name' => $board['b_name'],
            'loc_citi' =>[
                'name'=>$board['b_city_name']
            ],
        ];
        $toTer = [
            'name' => $drop['d_name'],
            'loc_citi' =>[
                'name'=>$drop['d_city_name']
            ],
        ];
        $booking_seats = [];
        
        
        
        for($i = 0;$i < $param['total_seat'];$i++){
            $dob = date('Y-m-d',  strtotime($param['pass_dob'][$i]));
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dob), date_create($today));

            $age = isset($param['pass_dob'][$i]) ? $diff->format('%y'):'';
            
            $booking_seats[] = [
                'passenger_name' => $param['pass_name'][$i],
                'passenger_gender' => $param['pass_gender'][$i],
                'passenger_age' => $age,
                'ticket_id' => 'not generated yet',
                'id' => '',
                'seat_index' => isset($param['seat_index']) ? $param['seat_index'][$i] : '',
                'seat_lbl' => isset($param['seat_label']) ? $param['seat_label'][$i] : '',
            ];
        }
        $total = ($param['total_amount'] + $hfval);
        $data = [
            'booking_id'=>'not generated yet',
            'booker_name'=>$param['booker_name'],
            'booker_email'=>$param['booker_email'],
            'booker_mo'=>$param['booker_mo'],
            'created_at'=>date('Y-m-d'),
            'journey_date'=>$param['journey_date'],
            'handle_fee'=>$param['handle_fee'],
            'sp_name'=>$param['sp_name'].' & Main',
            'from_terminal'=>$fromTer,
            'to_terminal'=>$toTer,
            'pickup_date'=>$pickdt,
            'dropping_date'=>$dropdt,
            'nos'=>$param['total_seat'],
            'base_amount'=>$param['total_amount'],
            'total_amount'=> $total,
            'booking_seats'=> $booking_seats,
        ];
        
        $view_data = [
                'header' => [
                    "title" => 'Ticket List | Seat Seller Panel BusTiket',
                    "js" => [],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-book',
                    'data' => $data,
                ],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
                'flag' =>0,
                'param'=>$param,
            ];
            return view("admin.ticket_detail", $view_data);
    }
    public function postSendEmail(){
        $param = \Input::all();

        \Mail::raw($param["data"], function ($message) use ($param) {
            $message->from('us@example.com', 'Bustiket');

            $message->to($param['useremail']);
        });
        
        $res = \General::success_res('Mail Send Successfully.');
        return $res;
    }
    
    public function postSendSms(){
        $param = \Input::all();
//        dd($param);
        $res = \General::success_res('SMS Send Successfully.');
        return $res;
    }
    
    public function postCancelTicket(){
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or not found.');
        }
        $res = \General::success_res('Ticket Cancelled Successfully.');
        $res['data'] = $data;
        return $res;
    }
    public function getCommissionManagement($type = '',$id = '') {
        $name = config('constant.SS_NAME');
        $balance = config('constant.SS_BALANCE');
        if($id == ''){
            $id = config('constant.CURRENT_LOGIN_ID');
        }
        
        $ctype = config('constant.SS_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        if($ctype != 'Main'){
            $type = $ctype == 'A' ? 'ssa' : 'ssb';
            if($cid != $id ){
                return redirect('ss/commission-management');
            }
        }else{
            
            $chlds = SeatSeller::where('parent_id',$cid)->pluck('id')->toArray();
            
            if(!in_array($id, $chlds) && $id != $cid){
                
                return redirect('ss/commission-management');
            }
        }
        
        $ss= \App\Models\Admin\SeatSeller::where('id',$id)->first();
        $name = $ss->name;
        $balance = $ss->balance;
        
        $view_data = [
            'header' => [
                "title" => 'Commission Management | Seat Seller Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-comm',
                'uid' => $id,
                'type' => $type,
                'name'  => $name,
                'balance'   => $balance,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        $blade = $type == 'ssa' || $type =='ssb'  ? 'admin.comm_list_ss_ab' :  'admin.comm_list_ss';
        
        return view($blade, $view_data);
    }
    
    public function postCommlistFilter() {
        $param = \Input::all();
        $ftype = isset($param['ftype']) ? $param['ftype'] : '';
        $id = config('constant.CURRENT_LOGIN_ID');
//        $param['id'] = $id;
        $data = \App\Models\SsCommission::filter_commlist($param);
        
//        dd($param);
        
        $blade = $ftype == 'ssa' || $ftype == 'ssb' ? 'admin.commlist_filter_ss_ab' :  'admin.commlist_filter_ss';
        
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view($blade, $res);
            
        }
        
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
    public function getReports(){
        
        $ctype = config('constant.SS_TYPE');
        $cid = config('constant.CURRENT_LOGIN_ID');
        $parent = config('constant.SS_PARENT') ;
        $param = [];
        $param['id'] = $cid;
        $smdata = json_encode([]);
        $sadata = json_encode([]);
        $sbdata = json_encode([]);
        $tsale = json_encode([]);
        $smcomm = json_encode([]);
        $sabcomm = json_encode([]);
        $smonth = json_encode([]);
        $sdaily = json_encode([]);
        $ssasale = json_encode([]);
        $ssbsale = json_encode([]);
        
        if($ctype == 'Main'){
            $param['book_by'] = 5;
            $smdata = SeatSeller::get_ticket_booked_by_ss($param);
            
            $ids = SeatSeller::where('parent_id',$cid)->pluck('id');
//            dd($ids);
            $param['child'] = $ids;
            $param['book_by'] = 6;
            $sadata = SeatSeller::get_ticket_booked_by_ss($param);
            $param['book_by'] = 7;
            $sbdata = SeatSeller::get_ticket_booked_by_ss($param);
            $param['book_by'] = [5,6,7];
            $tsale = SeatSeller::get_total_sale_by_ss($param);
            unset($param['child']);
            $smcomm = SeatSeller::get_total_commission_of_ss($param);
            $param['child'] = $ids;
            $sabcomm = SeatSeller::get_total_commission_of_ss($param);
            
            $smonth = SeatSeller::get_total_sale_by_month($param);
            
            $sdaily = SeatSeller::get_total_daily_sale($param);
            $param['book_by'] = 6;
            $ssasale = SeatSeller::get_child_by_type($param);
            $param['book_by'] = 7;
            $ssbsale = SeatSeller::get_child_by_type($param);
        }
        
        $view_data = [
            'header' => [
                "title" => 'Reports | Seat Seller Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'reporting',
                'logger'=> 'SS',
                'smdata'=>$smdata,
                'sadata'=>$sadata,
                'sbdata'=>$sbdata,
                'tsale'=>$tsale,
                'smcomm'=>$smcomm,
                'sabcomm'=>$sabcomm,
                'smonth'=>$smonth,
                'sdaily'=>$sdaily,
                'ssasale'=>$ssasale,
                'ssbsale'=>$ssbsale,
            ],
            'footer' => [
                "js" => ['d3.min.js','nv.d3.js','stream_layers.js','jchart.js'
//                    ,'charts.min.js'
                    ,'charts_temp.js'
                    ],
                "css" => ['nv.d3.min.css']
            ],
        ];
        
        return view("admin.reports", $view_data);
    }
    public function getCouponCode() {

        $view_data = [
            'header' => [
                "title" => 'Coupon Code | Seat Seller Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'coupon'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.coupon_list", $view_data);
    }
    
    public function postCouponFilter() {
        $param = \Input::all();
        
        $data = \App\Models\CouponCode::get_coupon_filter($param);
//        $data = \App\Models\Admin\Banners::get_bannerfilter($param);
        
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.coupon_filter", $res);
            
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }
}
