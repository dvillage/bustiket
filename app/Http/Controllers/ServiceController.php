<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class ServiceController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin'];
    private static $bypass_url_for_sp = ['getIndex', 'getLogin', 'postLogin','getDashboard','getLogout','getProfile','postChangeAdminPassword'];
    private static $logger = '';

    public function __construct() {
//        $this->middleware('sp', ['except' => self::$bypass_url]);
//        
//        $this->middleware('spAccess', ['except' => self::$bypass_url_for_sp]);
//        self::$logger = config('constant.LOGGER');
        $this->middleware('services');
    }

    
    public function postSearchBus()
    {
        $param = \Input::all();
        $arr = explode(".", $param['id']);
        
        if( count($arr) < 6 ){
            return \General::error_res('Something is missing in search criteria.');
        }
        $from = $arr[0];
        $to = $arr[2];
        $nop = $arr[5];
        $param = [
            'from'=>$from,
            'to'=>$to,
            'date'=>$arr[4],
            'nop'=>$nop,
            'backend'=>isset($param['backend'])?1:0,
        ];
        $validator = \Validator::make($param, \Validation::get_rules("user", "bus_search"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            $view_data = [
                "msg" => "No Buses Found !",
            ];
            $device = app('device');
            if($device == 'mobile'){
                return view("m.blank_info",$view_data);
            }
            return view("site.blank_info",$view_data);
            
            return \Response::json($json, 200);
        }
        
        $traveling_date = $param['date'];
//        $three_days = date("Y-m-d",strtotime("+3 days"));     // changed as per client requirement of bus search according to data selected
        $three_days = date("Y-m-d");
        $diff = strtotime($traveling_date) - strtotime($three_days);
        if( $diff < 0 )
        {
            $view_data = [
                "msg" => "No Buses Found !"
            ];
            $device = app('device');
            if($device == 'mobile'){
                return view("m.blank_info",$view_data);
            }
            return view("site.blank_info",$view_data);
        }
        
        $data = [
            'from' => [
                'name' => $from,
                'type' => $arr[1],
            ],
            'to' => [
                'name' => $to,
                'type' => $arr[3],
            ],
            'date' => $arr[4],
            'nop' => $nop,
        ];

        $buses = \App\Models\Serviceprovider\Buses::search($data);
        $price15 = \App\Models\Serviceprovider\Buses::price_for15($data);
//        dd($price15);
        
        $session_id = session('session_id');
        $alreadyBooked = \App\Models\BookingSeats::where('session_id',$session_id)->where('status',0)->where('booking_id',null)->get();
//        dd($alreadyBooked);
        $board_points = [];
        $drop_points = [];
        $bexist = [];
        $dexist = [];
//        dd(count($buses));
        foreach($buses as $k=>$val ){
            if(isset($val['routes'])){
                foreach($val['routes'] as $r=>$v){
                    $board_points[$v['bus_id']][$v['from_terminal_id']] = [
                            'b_terminal_id'=>$v['from_terminal_id'],
                            'boarding_datetime'=>$v['boarding_time'],
                            'boarding_minute'=>$v['boarding_minute'],
                            'boarding_time'=>date('H:i',strtotime($v['boarding_time'])),
    //                        'boarding_time'=>$v['boarding_time'],
                            'b_name'=>$v['from_terminal']['name'],
                            'b_city_name'=>$v['from_city']['name'],
                            'b_district_name'=>$v['from_district']['name'],
                        ];

                    $drop_points[$v['bus_id']][$v['to_terminal_id']] = [
                            'd_terminal_id'=>$v['to_terminal_id'],
                            'droping_datetime'=>$v['droping_time'],
                            'droping_minute'=>$v['droping_minute'],
                            'droping_time'=>date('H:i',strtotime($v['droping_time'])),
    //                        'droping_time'=>$v['droping_time'],
                            'd_name'=>$v['to_terminal']['name'],
                            'd_city_name'=>$v['to_city']['name'],
                            'd_district_name'=>$v['to_district']['name'],
                        ];
                }
            }else{
                foreach($val['bus'] as $rk=>$rv){
                    foreach($rv['terminals'] as $rtk=>$rtv){
//                        dd($rtv,$rv);
                        $board_points[$rk][$rtv[0]['org_code']] = [
                            'b_terminal_id'=>$rtv[0]['org_code'],
                            'boarding_datetime'=>date('Y-m-d',strtotime($rtv[0]['departure_date'])).' '.$rtv[0]['departure_time'],
                            'boarding_minute'=>0,
                            'boarding_time'=>$rtv[0]['departure_time'],
    //                        'boarding_time'=>$v['boarding_time'],
                            'b_name'=>$rtv[0]['board_point'],
                            'b_city_name'=>$rv['from'],
                            'b_district_name'=>'',
                        ];
                        
                        foreach($rtv as $dr){
                            $drop_points[$rk][$dr['dest_code']] = [
                                'd_terminal_id'=>$dr['dest_code'],
                                'droping_datetime'=>date('Y-m-d',strtotime($dr['arrival_date'])).' '.$dr['arrival_time'],
                                'droping_minute'=>0,
                                'droping_time'=>$dr['arrival_time'],
        //                        'droping_time'=>$v['droping_time'],
                                'd_name'=>$dr['drop_point'],
                                'd_city_name'=>$rv['to'],
                                'd_district_name'=>'',
                            ];
                        }
                    
                    }
                }
//                dd($board_points,$drop_points);
            }
        }
        
//        echo '<pre>';
//        print_r($board_points);
//        print_r($drop_points);
//        echo '</pre>';
        if(count($buses) <=0 ){
            $view_data = [
                "msg" => "No Buses Found !",
            ];
            $device = app('device');
            if($device == 'mobile'){
                return view("m.blank_info",$view_data);
            }
            return view("site.blank_info",$view_data);
        }
        $view_data = [
            'param' =>  json_encode($param,true),
            'buses' => $buses,
            'nop' => $nop,
            'board_points' => $board_points,
            'drop_points' => $drop_points,
            'already_booked' => $alreadyBooked,
            'already_booked_bus_id' => count($alreadyBooked) > 0 ? $alreadyBooked[0]['bus_id'] : 0,
            'price15'   => $price15,
        ];
        if(isset($param['backend']) && $param['backend'] ==1){
            return view("admin.bus_list",$view_data);
        }
        $device = app('device');
        if($device == 'mobile'){
            return view("m.bus_list",$view_data);
        }
        return view("site.bus_list",$view_data);
    }
    public function postPriceFor15()
    {
        $param = \Input::all();
        $arr = explode(".", $param['id']);
        $p = $param;
        if( count($arr) < 6 ){
            return \General::error_res('Something is missing in search criteria.');
        }
        $from = $arr[0];
        $to = $arr[2];
        $nop = $arr[5];
        $param = [
            'from'=>$from,
            'to'=>$to,
            'date'=>$arr[4],
            'nop'=>$nop,
            'backend'=>isset($param['backend'])?1:0,
        ];
        $validator = \Validator::make($param, \Validation::get_rules("user", "bus_search"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            
            return \Response::json($json, 200);
        }
        
        $data = [
            'from' => [
                'name' => $from,
                'type' => $arr[1],
            ],
            'to' => [
                'name' => $to,
                'type' => $arr[3],
            ],
            'date' => $arr[4],
            'nop' => $nop,
        ];
        
        $sdate = $arr[4];
        $key = $arr[0].'.'.$arr[1].'.'.$arr[2].'.'.$arr[3];
        if(\Cache::has($key)){
            $cache = \Cache::get($key);
            $result = [];
            for($i = 0 ; $i < 16 ; $i++){
                $d1 = date('Y-m-d',strtotime($param['date'].' +'.$i.' day '));
                $date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($d1)),date("l", strtotime($d1)));
                $day = explode(',', $date);
                $dtmonth = explode(' ', $day[1]);
                $srtdatre = substr($day[0],0,3).', '.$dtmonth[0].' '.substr($dtmonth[1],0,3);
                if(isset($cache[$d1])){
                    $result[$srtdatre] = [
                        'price' => $cache[$d1]['price'],
                        'date' => $d1,
                    ];
                }else{
                    $result[$srtdatre] = [
                        'price' => '-',
                        'date' => $d1,
                    ];
                }
            }
            $res = \General::success_res();
            $res['data'] = $result;
//            dd($result);
            return \Response::json($res,200);
        }
//        dd(\Cache::get($key));
        $price15 = \App\Models\Serviceprovider\Buses::price_for15($data);
//        dd($price15);
//        \Cache::forever($key,$price15);
        
        return \Response::json($price15,200);
    }
    public function postGetSeatMap(){
        $param = \Input::all();
        $device = app('device');
//        dd($device);
        $view_data = \App\Models\SeatMap::seatmap($param);
//        dd($view_data);
//        $l_id = 1;
//        $bus_id =$param['bus_id'];
//        
////        $layout = \App\Models\BusLayout::find($l_id)->toArray();
//        $layout = \App\Models\BusLayout::where('bus_id',$bus_id)->first();
//        $view_data = [];
//        $view_data['bus_id']= $bus_id;
//        $booked = \App\Models\BookingSeats::get_booked_seats($bus_id, $param['date']);
//        $blocked = \App\Models\BookingSeats::get_blocked_seats($bus_id, $param['date']);
//        
//        $session_id = session('session_id');
//        $alreadyBooked = \App\Models\BookingSeats::where('session_id',$session_id)->where('status',0)->where('booking_id',null)->get();
//        $blkd = [];
//        if(count($alreadyBooked) > 0){
//            foreach($alreadyBooked as $ab){
//                $blkd[] = $ab->seat_lbl;
//                if(($key = array_search($ab->seat_lbl, $blocked)) !== false) {
//                    unset($blocked[$key]);
//                }
//
//            }
//        }
////        dd($blkd,$blocked);
//        $total_seat = \App\Models\BusLayout::total_seats($bus_id);
//        $remain_seat = $total_seat - (count($blocked)+count($booked));
//        $view_data['nop']= $param['nop'];
//        $view_data['remain_seat']= $remain_seat;
//        $view_data['block_count']= count($blkd);
//        if($layout){
//            $layout = $layout->toArray();
//            $data = \App\Models\SeatMap::where("layout_id", $layout['id'])->orderBy("row", "ASC")->orderBy("col", "ASC")->get()->toArray();
////            $seats = \App\Models\SeatMap::prepare_seatmap_array($data,$layout['rows'],$layout['columns']);
//            
////            echo '<pre>';
////            print_r($seats);
////            echo '</pre>';exit;
////            $view_data = [
////                'rows'=>$layout['rows'],
////                'cols'=>$layout['columns'],
//////                'seats' =>  $seats,
////                'seats' =>  $data,
////                'booked' =>  $booked,
////                'blocked' =>  $blocked,
////                'bus_id' =>  $bus_id,
////            ];
//            $view_data['rows'] = $layout['rows'];
//            $view_data['cols'] = $layout['columns'];
//            $view_data['seats'] = $data;
//            $view_data['booked'] = $booked;
//            $view_data['blocked'] = $blocked;
//            $view_data['already_blocked'] = $blkd;
//        }
        
        if(app('platform') != 0){
            return \Response::json($view_data,200);
        }
        if(isset($view_data['flag']) && $view_data['flag'] != 1){
            return $view_data;
        }
        if(isset($param['backend']) && $param['backend'] ==1){
            return view("admin.seat_map",$view_data);
        }
        if($device == 'mobile'){
            return view("m.seat_map",$view_data);
        }
        return view("site.seat_map",$view_data);
    }
    
    public function postCityName(){
        $param = \Input::all();
        $sp = \App\Models\Locality\Citi::get_allciti($param);
        if(is_null($sp)){
            return \General::error_res('no city found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    public function postBookTicket(){
        $new_param = \Input::all();
        $param = json_decode($new_param['param'],true);
        $param['total_amount'] = $new_param['total_amount'];
        $param['base_amount'] = $new_param['base_amount'];
        $param['coupon_discount'] = $new_param['coupon_discount'];
        $param['coupon_code'] = $new_param['coupon_code'];
//        dd($new_param,$param);
        $rules = [
            'booker_name'=>'required',
            'bus_id'=>'required',
            'sp_id'=>'required',
            'bus_name'=>'required',
            'total_seat'=>'required',
            'per_head_amount'=>'required',
            'booker_mo'=>'required',
            'booker_email'=>'required|email',
            'booker_dob'=>'required|date|date_format:d-m-Y',
            'journey_date'=>'required|date|date_format:Y-m-d',
        ];
        $validator = \Validator::make($param, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
//        dd($param);
        $board = json_decode($param['board_point'],true);
        $drop = json_decode($param['drop_point'],true);
        
        $param['board_point'] = $board;
        $param['drop_point'] = $drop;
        
        $book = \App\Models\Bookings::book_ticket($param);
        return $book;
    }
    
    public function postSearchPariwisata()
    {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "pariwisata_search"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param['type'] = isset($param['type'])?$param['type']:'C';
        $param['bus_type'] = $param['bus_type'] == 'all' ? -1 : ($param['bus_type'] == 'ac' ? 1 : 0);
        $data = [
            'from' => [
                'name' => $param['from_city'],
                'type' => $param['type'],
            ],
            'bus_type' => $param['bus_type'],
            'date' => $param['date'],
        ];
//        $buses = \App\Models\Serviceprovider\Buses::search($data);
        $pariwisata = \App\Models\Admin\Pariwisata::search($data);
//        dd($pariwisata);
        
        $view_data = [
            'param' =>  json_encode($param,true),
            'data'=>$pariwisata,
            'count'=>count($pariwisata),
        ];
//        if(isset($param['backend']) && $param['backend'] ==1){
//            return view("admin.bus_list",$view_data);
//        }
//        dd($view_data);
        $device = app('device');
        if($device == 'mobile'){
            return view("m.pariwisata_list",$view_data);
        }
        if(count($pariwisata) == 0){
            return \General::error_res('No pariwisata found');
        }
        return view("site.pariwisata_list",$view_data);
    }
    public function postGetPriceByTerminal(){
        $param = \Input::all();
        $price = \App\Models\Admin\BusPrice::get_terminal_price($param);
        return $price;
    }
    
    public function postAllSpName(){
        $param = \Input::all();
        $sp = \App\Models\Serviceprovider\ServiceProvider::get_allsp($param);
        if(is_null($sp)){
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['first_name'].' '.$a['last_name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    public function postAllSsName(){
        $param = \Input::all();
        $sp = \App\Models\Admin\SeatSeller::get_allss($param);
        if(is_null($sp)){
            return \General::error_res('No Seat Seller found');
        }
        $res = \General::success_res();
        
        $r = [];
        foreach($sp as $a){
            $r[$a['name']] = $a['id'];
        }
        
        $res['data'] = $r;
        return \Response::json($res, 200);
    }
    
    
    
    public static function postCancelTicketByTicketId(){
        $param = \Input::all();
        $cseat = \App\Models\BookingSeats::cancel_seat_by_ticket($param['ticket_id']);
        return redirect('cancel-ticket') ;
    }

    
    public static function getPaymentSucces($booking_id)
    {
        $apr = '';
        if($ticket_id != ''){
            $apr = \App\Models\Bookings::approve_ticket($booking_id);
        }
        
        $view_data = [
            'header' => [
                "title" => "Payment -  C",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'id' => 'Terbit',
                'ticket_id' => $ticket_id,
            ],
            'footer' => [
                'flag' => 1,
                "js" => ["assets/js/common.min.js"],
                "css" => []
            ],
            'data'=>$apr,
        ];
        $eTick = '';
        if($ticket_id){
            $apr = \App\Models\Bookings::get_ticket_detail($ticket_id);
//            \Event::fire(new \App\Events\TicketBookSuccess($apr));
           $eTick =  view("site.e_ticket",array('data'=>$apr));
        }
        $view_data['body']['ticket'] = $eTick;
        return view("m.ticket_confirm",$view_data);
    }
    
    public function anyBniCheckPay(){
        $bank = \App\Models\BankDetails::where('bank_short_name', 'BNI')->first()->toArray();
        
        
        $d1 = date('Y-m-d H:i', strtotime(date('H:i').' -'.config('constant.BANK_TIME_LIMIT').' minutes'));
//        dd($d1);
        $bni_tkt = \App\Models\Bookings::with(['paymentRecord'])
                                            ->whereHas('paymentRecord',function($q) use($bank){
                                                $q->where(['payment_by'=>config("constant.PAYMENT_BY_BANK"),'bank_id'=>$bank['id']]);
                                            })
                                            ->where('status','!=',config("constant.STATUS_BOOKED"))
                                            ->where('payment_by',config("constant.PAYMENT_BY_BANK"))
                                            ->where('created_at','>=',$d1)
                                            ->get()->toArray();
//        dd($bni_tkt,$d1);
        if(count($bni_tkt) > 0){
            foreach($bni_tkt as $key=>$tkt){
                $booking = \App\Models\Bookings::where('booking_id',$tkt['booking_id'])->first();
                $data = [
                    'booking_id' => $tkt['booking_id'],
                ];

                $res = \App\Lib\BNI\BNI::inquireInvoice($data);
                echo '<pre>';
                print_r($res);
                echo '</pre>';
                $f = 0;
                if($res['flag'] != 1)
                {
                    
                    $booking->payment_by = config("constant.PAYMENT_BY_BANK");
                    
                    $booking->pgres = json_encode($res['data']);
                    
                    $booking->save();
                    echo $res['flag'];
                    echo $tkt['booking_id']." Transaction not verified";
        //            return $res;
                    $f = 1;
                    
                }

                if($booking->status != config("constant.STATUS_PAYMENT_PENDING") && $f == 0)
                {
                    
                    echo $tkt['booking_id']." Payment Already Proccessed";
                }
                else if($f == 0)
                {
                    
                    $booking->payment_by = config("constant.PAYMENT_BY_BANK");
                    $booking->pgres = $res['data'];
                    $booking->save();

                    $apr = \App\Models\Bookings::approve_ticket($booking_id);
                    echo $tkt['booking_id']." ticket confirmed";
                }
//                return $res;
            }
        }
        else{
            echo 'No Booking is Pending.';
        }
                
    }
    
    public function getActiveCouponcode() {
        
         $couponStatus = \App\Models\CouponCode::active_not_expr_coupon_code();
         return $couponStatus;
    }
    
    public function postDownloadCsv(){
        $param = \Input::all();
//        dd($param);
        $ftype = isset($param['by']) ? $param['by'] : '';
        if($ftype == 'ss' || $ftype == 'ssab'){
            $data = \App\Models\General::csv_for_ss($param);
        }elseif($ftype == 'sp' || $ftype == 'spab'){
            $data = \App\Models\General::csv_for_sp($param);
        }else{
            $data = \App\Models\General::csv_for_admin($param);
        }
        return $data;
    }
    
}
