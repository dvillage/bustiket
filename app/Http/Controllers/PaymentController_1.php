<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin\User;
use App\mpdf\MPDF57\mpdf;
class PaymentController extends Controller {

    public function __construct() {
        
    }
    
    public function ticket_checkout(){
        $param = \Input::all();

        $validator = \Validator::make($param, [
            'booker_email'  => 'required|email',
            'booker_dob'    => 'required|date',
            'booker_mo'     => 'required|regex:/^\d{10,20}$/',
            'pass_mo'       => 'required|array',
            'pass_mo.*'     => 'required|regex:/^\d{10,20}$/',
            'pass_name.*'   => 'required',
//            'seat_label.*'  => 'sometimes|required',
//            'seat_index.*'  => 'sometimes|required',
            'pass_gender.*' => 'required',
            'pass_dob.*'    => 'required|date',
        ]);

        if($validator->fails()){
            return redirect()->back()->with(['Error' => 'Please Fill Up all details properly.']);

        }
        
        $board = json_decode($param['board_point'],true);
        $drop = json_decode($param['drop_point'],true);
        
        $param['board_point'] = $board;
        $param['drop_point'] = $drop;
        
        $seatlbl = [];
        foreach($param['seat_label'] as $sl){
            $seatlbl[] = ['seat_lbl'=>$sl];
        }
//        dd($param);
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
            $data = [
                'bus_id' => $param['bus_id'],
                'passenger' => $seatlbl,
                'total_seats' => $param['total_seat'],
                'date' => date('Y-m-d', strtotime($param['board_point']['boarding_datetime'])),
            ];
            $available = \App\Models\Bookings::check_availability($data);
            if($available['flag'] == 0){
//                return $available;
                return redirect()->back()->with(['Error', 'Booked By Other, Please Select other.']);
            }
            
            //-------- Start : Check Route exists or not --------------//
            $chk_route = \App\Models\Admin\BusRoutes::where('bus_id',$param['bus_id'])
                                    ->where('from_terminal_id',$board['b_terminal_id'])
                                    ->where('to_terminal_id',$drop['d_terminal_id'])->first();
            if($chk_route == null){
                return redirect()->back()->with(['Error', 'Invalid Route.']);
            }
            //-------- Over : Check Route exists or not --------------//
        }
        
        

        $seatdata = \App\Models\BookingSeats::save_bookingseat_detail($param);
        if($seatdata['redirect'] == 1){
            return Redirect('');
        }

        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($param['sp_id']);

        
        $bankList = \App\Models\BankDetails::get_banks();
        
        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( ($param['total_amount'] * $handlefee[0]['handling_fee']) / 100 );
        }
                
//        $param['total_amount'] = number_format(($param['total_amount']),3,".",".");
//        $param['handlefee'] = number_format($hfval,3,".",".");
        $param['handlefee'] = $hfval;

//        dd($param);
        $view_data = [
            'header' => [
                "title" => "Payment -  A",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'id' => 'Pembayaran',
                'data' => $param,
                'banklist' => json_encode($bankList),
            ],
            'footer' => [
                'flag' => 0,
                "js" => ["assets/js/common.min.js"],
                "css" => []
            ],
        ];
        
        return view("site.ticket_checkout",$view_data);
    }
    
    
    public function ticket_process(){
        
        $param = \Input::all();
        
//        dd($param);
        $bookingdata = \App\Models\Bookings::save_booking_details($param);
//        dd($param,$bookingdata);
        
        $bookingdata['data']['pass_data'] = json_decode($param['pass_data'],true);

        date_default_timezone_set("Asia/Kolkata");
        $d0 = date('Y:m:d:H:i', strtotime($bookingdata['data']['created_at']));
        $d1 = date('Y:m:d:H:i',strtotime($bookingdata['data']['created_at'].' +'. config('constant.BANK_TIME_LIMIT').' minutes'));
        
        $p_date = \App\Models\General::getIndoMonths(date("j F Y,H:i", strtotime($bookingdata['data']['pickup_date'])),date("l", strtotime($bookingdata['data']['pickup_date'])));
        $d_date = \App\Models\General::getIndoMonths(date("j F Y,H:i", strtotime($bookingdata['data']['dropping_date'])),date("l", strtotime($bookingdata['data']['dropping_date'])));
        $p = explode(',', $p_date);
        $d = explode(',', $d_date);
//        dd(date('Y:m:d:H:i', \Request::server('REQUEST_TIME')));
        $bookingdata['data']['time_now'] = $d0;
        $bookingdata['data']['time_limit'] = $d1;
        $bookingdata['data']['indo_p_date'] = $p_date;
        $bookingdata['data']['indo_p_time'] = $p[2];
        $bookingdata['data']['indo_d_date'] = $d_date;
        $bookingdata['data']['indo_d_time'] = $d[2];
        
        $bank = \App\Models\BankDetails::where('bank_short_name',$param['bankdetails'])->first()->toArray();
//        dd($bookingdata['data']);

//        dd($param['time_now'],$param['time_limit']);
          
        if($param['bankdetails'] == 'BNI'){
            $res = $this->BniCreate($bookingdata['data']);
//            dd($bookingdata['data'],$res['data']['virtual_account']);
            if($res['flag'] == 1){
                $bank['account_no'] = $res['data']['virtual_account'];
//                dd($res['data']['virtual_account']);
                $payment = \App\Models\PaymentRecord::where('booking_id',$bookingdata['data']['booking_id'])->update(['va_no'=>$res['data']['virtual_account']]);
//                dd($res['data']['virtual_account'],\App\Models\PaymentRecord::where('booking_id',$bookingdata['data']['booking_id'])->first()->toArray());
                $data = [
                    'booking_id' => $bookingdata['data']['booking_id'],
                ];

                $inq_res = \App\Lib\BNI\BNI::inquireInvoice($data);
                \Log::info('BNI Inquiry Response : '.json_encode($inq_res));
            }
            else if($res['flag'] == 0){
                \Log::info('BNI Invoice Creation Error :\n Booking ID : '.$bookingdata['data']['booking_id'].'\n Booker Name : '.$bookingdata['data']['booker_name'].'\n Booker Email : '.$bookingdata['data']['booker_email'].'\n Booker Mobile No. : '.$bookingdata['data']['booker_mo'].'\n BNI Failed Response : '.json_encode($res['data']));
                
                $view_data = [
                    'header' => [
                        "title" => "Payment - B",
                        "js" => [],
                        "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
                    ],

                    'body' => [
                        'id' => 'Proses',
                    ],
                    'footer' => [
                        'flag' => 0,
                        "js" => ["assets/js/common.min.js"],
                        "css" => []
                    ],
                    "msg" => "<h2 style='color:red;'>We Couldn't process your request right now.<br> Please, Try after sometime !</h2><br><a href=".\URL::to('/')." class='btn btn-orange'>Try Again</a>",
                ];
                
                return view("site.fail_info",$view_data);
            }
        }
                
        $view_data = [
            'header' => [
                "title" => "Payment - B",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'id' => 'Proses',
                'data' => $bookingdata['data'],
                'bank' => $bank,
                'param' => $param,
            ],
            'footer' => [
                'flag' => 0,
                "js" => ["assets/js/common.min.js"],
                "css" => []
            ],
        ];
        
        return view("site.ticket_process",$view_data);
    }
    
    
    public function ticket_confirm($ticket_id = ''){
        
//        $pdf = new mPDF();
        $apr = '';
        if($ticket_id != ''){
            $apr = \App\Models\Bookings::approve_ticket($ticket_id);
            
//            echo '<pre>';
//            print_r($apr);
//            echo '</pre>';
        }
        
        $view_data = [
            'header' => [
                "title" => "Payment -  C",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'id' => 'Terbit',
                'ticket_id' => $ticket_id,
            ],
            'footer' => [
                'flag' => 1,
                "js" => ["assets/js/common.min.js"],
                "css" => []
            ],
            'data'=>$apr,
        ];
        $eTick = '';
        if($ticket_id){
//            $apr = \App\Models\Bookings::get_ticket_detail($ticket_id);
////            \Event::fire(new \App\Events\TicketBookSuccess($apr));
//           $eTick =  view("site.e_ticket",array('data'=>$apr));
        }
        $view_data['body']['ticket'] = $eTick;
        return view("site.ticket_confirm",$view_data);
    }
    
    public function ticket_failed(){
        $view_data = [
            'header' => [
                "title" => "Payment -  C",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            
            'body' => [
                'id' => 'Terbit',
                'msg' => 'Ticket Booking Failed.',
            ],
            
            'footer' => [
                'flag' => 1,
                "js" => ["assets/js/common.min.js"],
                "css" => []
            ]
        ];
        
        return view('site.ticket_failed',$view_data);
    }
    
    public function postChkBookConfirm(){
        $param=\Input::all();
//        dd($param);
        $tkt = \App\Models\Bookings::where('booking_id',$param['booking_id'])->first();
        if(!is_null($tkt)){
            $tkt = $tkt->toArray();
            if($tkt['status'] == config('constant.STATUS_BOOKED')){
                $res = \General::success_res('Ticket Booked Successfully.');
                return $res;
            }
            else{
                $res = \General::error_res('Ticket Not Booked.');
                return $res;
            }
        }
        $res = \General::success_res('Ticket Not Found.');
        return $res;
    }
    public function postCouponDiscount(){
        $param=\Input::all();
//        dd($param);
        $d= \App\Models\CouponCode::find_coupon($param);
//        dd($d);
        if($d['flag'] == 1){
//            dd( $d['data'][0]['min_amount'] <= $param['amount']);
            if($d['data'][0]['min_amount'] <= $param['amount'] && $d['data'][0]['max_amount'] >= $param['amount']){
                $res = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                if($d['data'][0]['type'] == 'P'){
                    $discount = ($param['amount'] * $d['data'][0]['value'] / 100);
                }
                $res['discount'] = $discount;
            }
            else{
                $res = \General::error_res('Coupon Code Not Applied.');
            }
    
            return \Response::json($res,200);
        }
        else{
            return \Response::json($d,200);
        }
    }
    
    public function postDownloadAsPdf(){
        $param=\Input::all();
        $ticket_id = $param['ticket_id'];
        if($ticket_id == '' || $ticket_id == 0){
            return \General::error_res();
        }
        $file_path = \URL::to('assets/uploads/ticket/bustiket_'.$ticket_id.'.pdf');
//        $ticket_detail = \App\Models\Bookings::get_ticket_detail($ticket_id);
//        $pdf = \PDF::loadView("site.e_ticket_mail",array('data'=>$ticket_detail));
//        return $pdf->download('invoice.pdf');
//         \Event::fire(new \App\Events\TicketBookSuccess($ticket_detail));
        $res = \General::success_res();
        $res['data'] = $file_path;
        return $res;
    }
    
    public function BniCreate($param){
//        $param = \Input::all();
        \Log::info('BNI Create Data : '. json_encode($param));
        $res = \App\Lib\BNI\BNI::createInvoice($param);
        return $res;
    }
    
    public function anyBniInq($book_id){
        $data = [
            'booking_id' => $book_id,
        ];
        
        $res = \App\Lib\BNI\BNI::inquireInvoice($data);
        
        \Log::info('BNI Inqiury Response Data : '. json_encode($res));
        $res['data'] = json_decode($res['data'],true);
        dd($res);
    }
    
    
    public function postBniNotify(){
        \Log::info("BNI Notification Data : ");
        \Log::info(json_encode(\Input::all()));
        $param = \Input::all();
        
        $resp = [
                    "status" => "000"
                ];
        $resp = json_encode($resp);
//        dd($param);
        $param = json_encode($param);
        $nres = \App\Lib\BNI\BNI::notification($param);
//        dd($nres);
        if($nres['flag'] == 0){
//            return \Response::json($nres,200);
            return $resp;
        }
        $booking_id = $nres['data']['trx_id'];
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if(is_null($booking))
        {
            $res = \General::error_res("Order not Found");
        }
        $data = [
            'booking_id' => $booking_id,
        ];
        
        $res = \App\Lib\BNI\BNI::inquireInvoice($data);
//        dd($res);
        if($res['flag'] != 1)
        {
            $booking->payment_by = config("constant.PAYMENT_BY_BANK");
            $booking->pgres = $res['data'];
            $booking->save();
            $res = \General::error_res("Transactions not verified");
            \Log::info('BNI Notification Response : '.json_encode($res));
//            return $res;
            return $resp;
        }
        
        if($booking->status != config("constant.STATUS_PAYMENT_PENDING"))
        {
            $res = \General::error_res("Payment Already Proccessed");
        }
        else
        {
            $booking->payment_by = config("constant.PAYMENT_BY_BANK");
            $booking->pgres = $res['data'];
            $booking->save();
            
            $apr = \App\Models\Bookings::approve_ticket($booking_id);
            $res = \General::success_res("ticket confirmed");
        }
        \Log::info('BNI Notification Response : '.json_encode($res));
//        return $res;       
        return $resp;
    }
    
    public function postBniEnc(){
        $param = \Input::all();
        $res = \App\Lib\BNI\BNI::BNIEncrypt($param);
        return $res;
    }
    
    public function postBniDec(){
        $param = \Input::all();
        $res = \App\Lib\BNI\BNI::BNIDecrypt($param);
        return $res;
    }
    
    public function postDokuNotify(){
        
        \Log::info("Doku Notify response");
        \Log::info(json_encode(\Input::all()));
        $param = \Input::all();
        $order_id = $param['TRANSIDMERCHANT'];
        $booking_id = explode("_",$order_id);
        
        if(!isset($booking_id[1]))
        {
            $res = \General::error_res("Invalid order formate");
        }
        $booking_id = $booking_id[1];
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if(is_null($booking))
        {
            $res = \General::error_res("Order not Found");
        }
        
//        if($param['RESULTMSG'] != 'SUCCESS')
//        {
//            $res = \General::error_res("Transaction Failed.");
//            return $res;
//        }
        
        $res = \App\Lib\PaymentGateway\Doku::verify_transaction($param);
        
        if($res['flag'] != 1)
        {
            $booking->payment_by = config("constant.PAYMENT_BY_DOKU");
            $booking->pgres = $res['data'];
            $booking->save();
            $res = \General::error_res("Transactions not verified");
        }
        
        if($booking->status != config("constant.STATUS_PAYMENT_PENDING"))
        {
            $res = \General::error_res("Payment Already Proccessed");
        }
        else
        {
            $booking->payment_by = config("constant.PAYMENT_BY_DOKU");
            $booking->pgres = $res['data'];
            $booking->save();
            
            $apr = \App\Models\Bookings::approve_ticket($booking_id);
            $res = \Genral::success_res("ticket confirmed");
        }
        return $res;
    }
    
    public function postDokuRedirect(){
        
        $param = \Input::all();
        $order_id = $param['TRANSIDMERCHANT'];
        $booking_id = explode("_", $order_id);
        
        
        $m = new \Mobile_Detect();
        
        
        if(!isset($booking_id[1]))
        {
            $res = \General::error_res("Invalid Transactions");
            
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        $booking_id = $booking_id[1];
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if(is_null($booking))
        {
            $res = \General::error_res("Invalid Transactions !");
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
//        $res = \App\Lib\PaymentGateway\Doku::verify_transaction($order_id);
        $res = \App\Lib\PaymentGateway\Doku::verify_transaction($param);
        
        if($res['flag'] != 1)
        {
            $res = \General::error_res("Invalid Transactions !!");
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        
        if($booking->status == config("constant.STATUS_BOOKED"))
        {
            if($m->isMobile() || $m->isTablet())
            {
                return redirect(url("api/services/payment-succes/".$booking_id));
            }
            else
            {
                return Redirect('ticket-confirmed/'.$booking_id);
            }
        }
        else if($booking->status != config("constant.STATUS_PAYMENT_PENDING"))
        {
            $res = \General::error_res("Payment Already Proccessed");
            if($m->isMobile() || $m->isTablet())
            {
                return redirect(url("service/mobile-error-page"))->with("msg",  json_encode($res));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        $booking->payment_by = config("constant.PAYMENT_BY_DOKU");
        $booking->pgres = json_encode($res['data']);
        $booking->save();

        if($m->isMobile() || $m->isTablet())
        {
            return redirect(url("api/services/payment-succes/".$booking_id));
        }
        else
        {
            return Redirect('ticket-confirmed/'.$booking_id);
        }
    }
    
    public function postDokuReview(){
        \Log::info("Doku Review response");
        \Log::info(json_encode(\Input::all()));
        
    }
    public function postDokuIdentify(){
        \Log::info("Doku Identify response");
        \Log::info(json_encode(\Input::all()));
    }
    
    
    public function postMidtransredirect()
    {
        $param = \Input::all();
//        $param = ["order_id" => "tik_09TK27UR11S8X5","status_code" => 200, "transaction_status" => "settlement"];
        $order_id = $param['order_id'];
        $booking_id = explode("_", $param['order_id']);
        
        
        $m = new \Mobile_Detect();
        
        
        if(!isset($booking_id[1]))
        {
            $res = \General::error_res("Invalid Transactions");
            
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        $booking_id = $booking_id[1];
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if(is_null($booking))
        {
            $res = \General::error_res("Invalid Transactions !");
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        $res = \App\Lib\PaymentGateway\Veritrans::verify_transaction($order_id);
        
        if($res['flag'] != 1)
        {
            $res = \General::error_res("Invalid Transactions !!");
            if($m->isMobile() || $m->isTablet())
            {
                \Session::set("msg",  json_encode($res));
                return redirect(url("api/services/payment-fail"));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        
        if($booking->status == config("constant.STATUS_BOOKED"))
        {
            if($m->isMobile() || $m->isTablet())
            {
                return redirect(url("api/services/payment-succes/".$booking_id));
            }
            else
            {
                return Redirect('ticket-confirmed/'.$booking_id);
            }
        }
        else if($booking->status != config("constant.STATUS_PAYMENT_PENDING"))
        {
            $res = \General::error_res("Payment Already Proccessed");
            if($m->isMobile() || $m->isTablet())
            {
                return redirect(url("service/mobile-error-page"))->with("msg",  json_encode($res));
            }
            else
            {
                return Redirect('ticket-failed');
            }
        }
        $booking->payment_by = config("constant.PAYMENT_BY_VERITRANS");
        $booking->pgres = json_encode($res['data']);
        $booking->save();

        if($m->isMobile() || $m->isTablet())
        {
            return redirect(url("api/services/payment-succes/".$booking_id));
        }
        else
        {
            return Redirect('ticket-confirmed/'.$booking_id);
        }
        
    }

    
    public function postMidtransNotify()
    {
        $param = \Input::all();
//        $param = ["order_id" => "tik_09TK27UR11S8X5","status_code" => 200, "transaction_status" => "settlement"];
        $order_id = $param['order_id'];
        $booking_id = explode("_", $param['order_id']);
        
        
        if(!isset($booking_id[1]))
        {
            $res = \General::error_res("Invalid order formate");
        }
        $booking_id = $booking_id[1];
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if(is_null($booking))
        {
            $res = \General::error_res("Order not Found");
        }
        $res = \App\Lib\PaymentGateway\Veritrans::verify_transaction($order_id);
        
        if($res['flag'] != 1)
        {
            $booking->payment_by = config("constant.PAYMENT_BY_VERITRANS");
            $booking->pgres = $res['data'];
            $booking->save();
            $res = \General::error_res("Transactions not verified");
        }
        
        if($booking->status != config("constant.STATUS_PAYMENT_PENDING"))
        {
            $res = \General::error_res("Payment Already Proccessed");
        }
        else
        {
            $booking->payment_by = config("constant.PAYMENT_BY_VERITRANS");
            $booking->pgres = $res['data'];
            $booking->save();
            
            $apr = \App\Models\Bookings::approve_ticket($booking_id);
            $res = \Genral::success_res("ticket confirmed");
        }
        return $res;
    }
    
    public function postMidtransCancel()
    {
        $m = new \Mobile_Detect();
        if($m->isMobile() || $m->isTablet())
        {
            return redirect(url("api/services/payment-cancel"))->with("msg",  json_encode($res));
        }
        else
        {
            return Redirect('ticket-failed');
        }
        
    }

    




    public function pariwisataCheckout(){
        $param = \Input::all();

        $validator = \Validator::make($param, [
            'total_bus'  => 'required|min:1',
            'bus_detail'    => 'required',
            ]);

        if($validator->fails()){
//            dd($validator);
            return redirect()->back()->with(['Error' => 'Please Fill Up all details properly.']);

        }
        $param['bus_detail'] = json_decode($param['bus_detail'],true);
//        dd($param);
        $view_data = [
            'header' => [
                "title" => "Pariwisata booking - c",
                "js" => ['assets/js/jquery.datetimepicker.full.js'],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css","assets/css/jquery.datetimepicker.css"],
            ],
            'body' => [
                'id' => 'Pembayaran',
                'data' => $param,
            ],
            'footer' => [
                'flag' => 0,
                "js" => ["assets/js/common.min.js","assets/js/parsley.min.js",'assets/js/scripts.js',],
                "css" => []
            ],
        ];
        
        return view("site.pariwisata_book",$view_data);
    }
    
    public function pariwisataProcess(){
        
        $param = \Input::all();
        
//        $date = \App\Models\General::getInMonths($param['date'], 'M', 'S');
//        $param['date'] = $date;
//        
//        $detail = \App\Models\PariwisataOrder::where('id',3)->with('buses.bus.sp')->first()->toArray();
//        \Event::fire(new \App\Events\PariwisataBookSuccess($detail));
//        dd($detail);
//        return;
//        
        $res = \App\Models\PariwisataOrder::add_order($param);
        return $res;
    }
    
    public function pariwisataSuccess($email =''){
        
        if(!isset($email)){
            return redirect('');
        }
        $view_data = [
            'header' => [
                "title" => "Pariwisata booking - c",
                "js" => [],
                "css" => ["assets/css/app.min.css","assets/css/search.min.css"],
            ],
            'body' => [
                'id' => 'Pembayaran',
                'email' => $email,
            ],
            'footer' => [
                'flag' => 0,
                "js" => [],
                "css" => []
            ],
        ];
        return view("site.pariwisata_book_success",$view_data);
    }
}