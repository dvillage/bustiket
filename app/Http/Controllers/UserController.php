<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Users;
use App\Models\Serviceprovider;
use Illuminate\Support\Facades\Mail;
use DB;
use File;

class UserController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin', 'getResetPass', 'getForgotPass', 'postForgotPass','postResetPass', 'getSignup', 'postSignup', 'getAuthGoogle', 'redirectGoogle','getAuthFacebook','redirectFacebook'];

    public function __construct() {
        $this->middleware('UserAuth', ['except' => self::$bypass_url]);
    }

    public function getIndex() {
        if (!\Auth::guard('user')->check()) {
//            return \Response::view('errors.401', array(), 401);
            return \Redirect::to('/');
        }

        return \Redirect::to('/');
    }

    public function getDashboard() {

        if (!\Auth::guard('user')->check()) {
            return \Response::view('errors.401', array(), 401);
        }

        $view_data = [
            'header' => [
                "title" => 'Dashboard | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("/", $view_data);
    }

    public function getLogin() {

        if (\Auth::guard("user")->check()) {
//            dd(\Auth::guard('user')->user());
            return \Redirect::to("/");
        }
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view('site.login', $view_data);
    }
    public function getForgotPass() {

        if (\Auth::guard("user")->check()) {
//            dd(\Auth::guard('user')->user());
            return \Redirect::to("/");
        }
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view('site.forgotpass', $view_data);
    }
    public function postForgotPass() {

        $view_data_back = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "forget_pass"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            return view('site.forgotpass', $view_data_back)->withErrors($validator);
        }
        $param = \Input::all();
        $res = \App\Models\Users::forget_password($param);
//        dd($param,$res);
        
        return view('site.forgotpass', $view_data_back)->withErrors(['msg'=>$res['msg']]);
    }

    public function postLogin(Request $req) {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "login"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => [],
                    'flag' => 1,
                ],
            ];
            return view('site.login', $view_data)->withErrors($validator);
        }
        $param = $req->input();
        $res = Users::doLogin($param);
        if ($res['flag'] == 0) {
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
                ],
                'body' => [
                ],
                'footer' => [
                    "js" => [],
                    "css" => [],
                    'flag' => 1,
                ],
            ];
            return view('site.login', $view_data)->withErrors('Wrong User Id or Password !!');

//            return view('site.login',$view_data)->withErrors('Wrong User Id or Password !!');
//            return redirect()->back()->withErrors('Wrong User Id or Password !!');;
        }
        return \Redirect::to("/");
    }

    public function getSignup() {

        $session = session("msg") ? session("msg") : "[]";
        if (!isset($session) && \Auth::guard("user")->check()) {
            return \Redirect::to("/");
        }
//        dd(\Input::all());
        \Session::forget("msg");
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
                "new_signup" => json_decode($session, true),
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view('site.signup', $view_data);
    }

    public function postSignup(Request $req) {
        $custome_msg = [
            'user_email.required' => 'Email Address Require',
            'user_email.email' => 'Invalid Email Address',
            'user_password.required' => 'Password Require',
        ];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "signup"), $custome_msg);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $res = \General::validation_error_res($error[0]);
            $res['data'] = $error;
            \Session::set("msg", json_encode($res));
            return \Redirect::back()->withInput();
        }
        $param = \Input::all();
        $res = Users::signup($param);
        \Session::set("msg", json_encode($res));
        return redirect('signup');
    }

    public function getLogout() {
        \App\Models\Token::delete_token();
        \Auth::guard('user')->logout();
        return redirect("login");
    }

    public function getResetPass($token = '') {
//        dd($token);
        $pass_token = \App\Models\Token::active()->where('token', '=', $token)->get()->toArray();
        if (count($pass_token) <= 0) {
//            return redirect('/');
            $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
//                'forgorttoken' => null,
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view('site.resetpass', $view_data);
        }

//        dd($pass_token);

        if (\Auth::guard("user")->check()) {
//            dd(\Auth::guard('user')->user());
            return \Redirect::to("/");
        }
        $view_data = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
                'forgorttoken' => $token,
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        return view('site.resetpass', $view_data);
    }

    public function postResetPass() {
        $param = \Input::all();
//        dd($param);
        $view_data_back = [
            'header' => [
                "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                "js" => [],
                "css" => ["assets/css/app.min.css", "assets/css/search.min.css"]
            ],
            'body' => [
                'forgorttoken' => $param['forgottoken'],
            ],
            'footer' => [
                "js" => [],
                "css" => [],
                'flag' => 1,
            ],
        ];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "reset_pass"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            return view('site.resetpass', $view_data_back)->withErrors($validator);
        }
        
        if($param['new_pass'] != $param['cnew_pass']){
            return view('site.resetpass', $view_data_back)->withErrors(['msg' => 'New Password and Confirm Password not Matched.' ]);
        }
        
        $user = \App\Models\Token::where('type',3)->where('token',$param['forgottoken'])->first();
        if(!is_Null($user)){
//            dd($param,$user->toArray());
            $userInfo = \App\Models\Users::where('id',$user->user_id)->first();
            if(is_Null($userInfo)){
                return view('site.resetpass', $view_data_back)->withErrors(['msg' => 'User Not Found.' ]);
            }
            $userInfo->password = \Hash::make($param['new_pass']);
            $userInfo->save();
            $user->delete();
        }
//        dd($param,$userInfo->toArray(),$user->toArray());
        return \Redirect('login');
    }

//    public function getProfile($msg = "") {
//        $res = User::getProfile();
//        $view_data = [
//            'header' => [
//                "title" => 'Profile | Admin Panel BusTiket',
//                "js" => [],
//                "css" => [],
//            ],
//            'body' => [
//                'name' => isset($res['name']) ? $res['name'] : "",
//                'email' => isset($res['email']) ? $res['email'] : "",
//                'msg' => $msg
//            ],
//            'footer' => [
//                "js" => [],
//                "css" => []
//            ],
//        ];
//        return view("admin.profile", $view_data);
//    }

    public function postChangeAdminPassword() {
        $param = \Input::all();
//        dd($param);
        $res = User::change_admin_password($param);
//        dd($res);
        if (isset($res['flag'])) {
            if ($res['flag'] == 0) {
                return \Redirect::to('admin/profile/' . $res['msg']);
            } else if ($res['flag'] == 1) {
//                return \Redirect::to("admin/dashboard");
                return \Redirect::to("admin/logout");
            }
        }
    }

    public function getTicketBookNew() {
        $view_data = [
            'header' => [
                "title" => 'Book New Ticket | Admin Panel BusTiket',
                "js" => ['parsley.min.js', 'admin.search.min.js', 'jquery.inputmask.bundle.min.js'],
                "css" => ['sprite-general.css', 'admin.search.min.css'],
            ],
            'body' => [
                'id' => 'tkt-book',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_book", $view_data);
    }

    public function postDelTicket() {
        $param = \Input::all();
//        $data = \App\Models\Bookings::delete_ticket($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or already cancelled or not found.');
        }
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function getTicketList() {
        $cityList = \App\Models\Locality\Citi::get_allciti();
        $city = [];
        foreach ($cityList as $c) {
            $city[$c['name']] = null;
        }
//        dd(json_encode($city));

        $view_data = [
            'header' => [
                "title" => 'Ticket List | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'tkt-book',
                'city' => json_encode($city),
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.ticket_list", $view_data);
    }

    public function getCommissionManagement() {

        $view_data = [
            'header' => [
                "title" => 'Commission Management | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'admin-comm'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.comm_list", $view_data);
    }

    public function postCommlistFilter() {
        $param = \Input::all();
        $data = \App\Models\AdminCommission::filter_commlist($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view("admin.commlist_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postApproveTicket() {
        $param = \Input::all();
//        dd($param['id']);
        $apr = \App\Models\Bookings::approve_ticket($param['id']);
       // dd($apr);
    }

    public function getTicketDetails($id = '') {
//        $id = '09PM06UR18L2U';
        if ($id == '') {
            return \Response::view('errors.404', array(), 404);
        }
        $data = \App\Models\Bookings::get_ticket_detail($id);
//        dd($data);
        if ($data != 0) {
            $view_data = [
                'header' => [
                    "title" => 'Ticket List | Admin Panel BusTiket',
                    "js" => [\URL::to('assets/ckeditor/ckeditor.js')],
                    "css" => [],
                ],
                'body' => [
                    'id' => 'tkt-list',
                    'data' => $data,
                ],
                'flag' => 1,
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("admin.ticket_detail", $view_data);
        } else {
            return \Response::view('errors.404', array(), 404);
        }
    }

    public function postTicketCheckout() {
        $param = \Input::all();
//        dd($param);
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($param['sp_id']);

        $hfval = $handlefee[0]['handling_fee'];
        if ($handlefee[0]['handling_fee_type'] == 'P') {
            $hfval = ceil(($param['total_amount'] * $handlefee[0]['handling_fee']) / 100);
        }
        $param['handle_fee'] = $hfval;

        $board = json_decode($param['board_point'], true);
        $drop = json_decode($param['drop_point'], true);

        $b_h = (int) (($board['boarding_minute'] / 60) % 24);
        $b_m = (int) ($board['boarding_minute'] % 60);

        $d_h = (int) (($drop['droping_minute'] / 60) % 24);
        $d_m = (int) ($drop['droping_minute'] % 60);
        //    echo $b_h.':'.$b_m.' , '.$d_h.':'.$d_m;
        //    dd(0);
        $dtime = $b_h . ':' . $b_m;
        $ddate = date('d M y', strtotime($board['boarding_datetime']));

        $atime = $d_h . ':' . $d_m;
        $adate = date('d M y', strtotime($board['boarding_datetime']));
        if ($b_h > $d_h) {
            $adate = date('d M y', strtotime($drop['droping_datetime'] . '+1 day'));
        }

        $pickdt = date('Y-m-d H:i:s', strtotime($ddate . ' ' . $dtime));
        $dropdt = date('Y-m-d H:i:s', strtotime($adate . ' ' . $atime));

        $fromTer = [
            'name' => $board['b_name'],
            'loc_citi' => [
                'name' => $board['b_city_name']
            ],
        ];
        $toTer = [
            'name' => $drop['d_name'],
            'loc_citi' => [
                'name' => $drop['d_city_name']
            ],
        ];
        $booking_seats = [];
        for ($i = 0; $i < $param['total_seat']; $i++) {
            $booking_seats[] = [
                'passenger_name' => $param['pass_name'][$i],
                'passenger_gender' => $param['pass_gender'][$i],
                'ticket_id' => 'not generated yet',
                'id' => '',
                'seat_index' => isset($param['seat_index']) ? $param['seat_index'][$i] : '',
                'seat_lbl' => isset($param['seat_label']) ? $param['seat_label'][$i] : '',
            ];
        }
        $total = ($param['total_amount'] + $hfval);
        $data = [
            'booking_id' => 'not generated yet',
            'booker_name' => $param['booker_name'],
            'booker_email' => $param['booker_email'],
            'booker_mo' => $param['booker_mo'],
            'created_at' => date('Y-m-d'),
            'journey_date' => $param['journey_date'],
            'handle_fee' => $param['handle_fee'],
            'sp_name' => $param['sp_name'] . ' & Main',
            'from_terminal' => $fromTer,
            'to_terminal' => $toTer,
            'pickup_date' => $pickdt,
            'dropping_date' => $dropdt,
            'nos' => $param['total_seat'],
            'base_amount' => $param['total_amount'],
            'total_amount' => $total,
            'booking_seats' => $booking_seats,
        ];

        $view_data = [
            'header' => [
                "title" => 'Ticket List | Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'tkt-book',
                'data' => $data,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
            'flag' => 0,
            'param' => $param,
        ];
        return view("admin.ticket_detail", $view_data);
    }

    public function postSendEmail() {
        $param = \Input::all();

        Mail::raw($param["data"], function ($message) use ($param) {
            $message->from('us@example.com', 'Bustiket');

            $message->to($param['useremail']);
        });

        $res = \General::success_res('Mail Send Successfully.');
        return $res;
    }

    public function postSendSms() {
        $param = \Input::all();
//        dd($param);
        $res = \General::success_res('SMS Send Successfully.');
        return $res;
    }

    public function postCancelTicket() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Bookings::cancel_ticket($param['id']);
        if(isset($data['flag']) && $data['flag'] != 1){
            return $data;
        }
        if($data == 0){
            return \General::error_res('ticket not booked yet or not found.');
        }
        $res = \General::success_res('Ticket Cancelled Successfully.');
        $res['data'] = $data;
        return $res;
    }

    public function postSpFilter() {
        $param = \Input::all();
        $data = \App\Models\Serviceprovider\ServiceProvider::get_sp_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $data['type'] = $param['type'];
            return view("admin.sp_filter", $data);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postTlistFilter() {
        $param = \Input::all();
        $data = \App\Models\Bookings::filter_ticketlist($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
//            $data['type'] = $param['type'];
            return view("admin.ticketlist_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function getAddProvider($type = '') {
        $data = self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Add Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        $t = $type != '' ? '_t' : '';
        return view("admin.add_provider" . $t, $view_data);
    }

    public function getUpdateProvider($type, $id) {
        $param['id'] = $id;
        $param['type'] = $type;
        $details = \App\Models\Serviceprovider\ServiceProvider::providers_details($param);

        $data = self::postGetMainProvider();

        $view_data = [
            'header' => [
                "title" => 'Update Service Provider | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'sp-' . ($type == '' ? 'main' : $type),
                'data' => $details['data'][0],
                'splist' => $data['data'],
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];


        return view("admin.providers_detail", $view_data);
    }

    public function getBanner() {

        $view_data = [
            'header' => [
                "title" => 'Banner | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'Banner'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.banner_list", $view_data);
    }

    public function getCouponCode() {

        $view_data = [
            'header' => [
                "title" => 'Coupon Code | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'coupon'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.coupon_list", $view_data);
    }

    public function postCouponFilter() {
        $param = \Input::all();

        $data = \App\Models\CouponCode::get_coupon_filter($param);
//        $data = \App\Models\Admin\Banners::get_bannerfilter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.coupon_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postBannerFilter() {
        $param = \Input::all();

        $data = \App\Models\Admin\Banners::get_bannerfilter($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.banner_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postDelBanner() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Banners::delete_banner($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postAddBanner() {
        $param = \Input::all();
        $addBanner = \App\Models\Admin\Banners::add_new_banner($param);

        return \Response::json($addBanner, 200);
    }

    public function postEditBanner() {
        $param = \Input::all();
        $addBanner = \App\Models\Admin\Banners::update_banner($param);

        return \Response::json($addBanner, 200);
    }

    public function getLocality($type = '') {

        $plist = \App\Models\Locality\Province::get_allprovince();
        $dlist = \App\Models\Locality\District::get_alldistrict();
        $clist = \App\Models\Locality\Citi::get_allciti();
//        dd($dlist,$dlist);
        $view_data = [
            'header' => [
                "title" => 'Locality | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'loc-' . $type,
                'plist' => $plist,
                'dlist' => $dlist,
                'clist' => $clist,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];


        return view("admin.locality", $view_data);
    }

    public function getSeatseller($type = '') {
        if ($type != 'Main' && $type != 'A' && $type != 'B' && $type != 'D') {
            return \Response::view('errors.404', array(), 404);
        }
        $mainss = '';
        if ($type != 'Main')
            $mainss = \App\Models\Admin\SeatSeller::get_mainss();
//        $clist= \App\Models\Locality\Citi::get_allciti();
//        dd($dlist,$dlist);
        $view_data = [
            'header' => [
                "title" => 'Seat Seller | Admin Panel BusTiket',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'type' => $type,
                'id' => 'ss-' . $type,
                'plist' => $mainss,
                'logger' => 'A'
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        if ($type == 'D') {
            return view("admin.seat_seller_deposit", $view_data);
        } else {
            return view("admin.seat_seller", $view_data);
        }
    }

    public function postSeatSellerFilter() {
        $param = \Input::all();

        $data = \App\Models\Admin\SeatSeller::get_seatsellerfilter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            $res['logger'] = 'A';
            return view("admin.seat_seller_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postSeatSellerDeposit() {
        $param = \Input::all();

        $data = \App\Models\Admin\SeatSellerDeposit::get_deposite_request($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.seat_seller_deposite_request", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postApproveSsDeposite() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\Admin\SeatSellerDeposit::approve_deposit($param);

        return \Response::json($res, 200);
    }

    public function postDelSeatseller() {
        $param = \Input::all();
        $data = \App\Models\Admin\SeatSeller::delete_seatseller($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postAddSeatseller() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'ssemail' => 'unique:ss,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $addss = \App\Models\Admin\SeatSeller::add_seatseller($param);

        return \Response::json($addss, 200);
    }

    public function postEditSeatseller() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'ssemail' => 'unique:ss,email,' . $param['sid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $addss = \App\Models\Admin\SeatSeller::edit_seatseller($param);

        return \Response::json($addss, 200);
    }

    public function getSeatsellerDetails($id = '') {
        $ss = \App\Models\Admin\SeatSeller::get_seatseller_details($id);
//        dd($ss);

        $view_data = [
            'header' => [
                "title" => 'Seat Seller Details| Admin Panel BusTiket',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'type' => $ss['type'],
                'id' => 'ss-' . $ss['type'],
                'data' => isset($ss) ? $ss : '',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];


        return view("admin.seatseller_details", $view_data);
    }

    public function postLocalityFilter() {
        $param = \Input::all();
        if ($param['type'] == 'Province') {
            $data = \App\Models\Locality\Province::get_province($param);
        }
        if ($param['type'] == 'District') {
            $data = \App\Models\Locality\District::get_district($param);
        } elseif ($param['type'] == 'City') {
            $data = \App\Models\Locality\Citi::get_citi($param);
        } elseif ($param['type'] == 'Terminal') {
            $data = \App\Models\Locality\Terminal::get_terminal($param);
        }
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            return view("admin.locality_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postDelLocalityDistrict() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\District::delete_district($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postAddLocality() {
        $param = \Input::all();
//        dd($param);

        if ($param['type'] == 'Province') {
            $validator = \Validator::make(\Input::all(), [
                        'name' => 'unique:provinces,name'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Locality\Province::add_province($param);
        } elseif ($param['type'] == 'District') {
            $ex = \App\Models\Locality\District::dist_exist($param);
            if ($ex != 1) {
                return \General::error_res('District is already Exist ..');
            }

            $data = \App\Models\Locality\District::add_district($param);
        } elseif ($param['type'] == 'City') {
            $ex = \App\Models\Locality\Citi::city_exist($param);
            if ($ex != 1) {
                return \General::error_res('City is already Exist ..');
            }
            $data = \App\Models\Locality\Citi::add_citi($param);
        } elseif ($param['type'] == 'Terminal') {
            $ex = \App\Models\Locality\Terminal::terminal_exist($param);
            if ($ex != 1) {
                return \General::error_res('Terminal is already Exist ..');
            }
            $data = \App\Models\Locality\Terminal::add_terminal($param);
        }

        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditLocality() {
        $param = \Input::all();
//        dd($param);

        if ($param['type'] == 'Province') {
            $validator = \Validator::make(\Input::all(), [
                        'name' => 'unique:provinces,name,' . $param['id']
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $error = $messages->all();
                $json = \General::validation_error_res();
                $json['data'] = $error;
                $json['msg'] = $error[0];
                return \Response::json($json, 200);
            }
            $data = \App\Models\Locality\Province::edit_province($param);
        }
        if ($param['type'] == 'District') {
            $ex = \App\Models\Locality\District::dist_exist($param, 1);
            if ($ex != 1) {
                return \General::error_res('District is already Exist ..');
            }
            $data = \App\Models\Locality\District::edit_district($param);
        } elseif ($param['type'] == 'City') {
            $ex = \App\Models\Locality\Citi::city_exist($param, 1);
            if ($ex != 1) {
                return \General::error_res('city is already exist ..');
            }
            $data = \App\Models\Locality\Citi::edit_citi($param);
        } elseif ($param['type'] == 'Terminal') {
            $ex = \App\Models\Locality\Terminal::terminal_exist($param, 1);
            if ($ex != 1) {
                return \General::error_res('Terminal is already exist ..');
            }
            $data = \App\Models\Locality\Terminal::edit_terminal($param);
        }

        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postDelLocalityCity() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Citi::delete_citi($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postDelLocalityProvince() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Province::delete_province($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postDelLocalityTerminal() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Terminal::delete_terminal($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postFilterDist() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\District::get_distfilter($param);
        $dlist = [];
        foreach ($data as $d) {
            $dlist[$d['name']] = $d['id'];
        }

        $res = \General::success_res();
        $res['data'] = $dlist;
//        $res['data']=$data;
        return \Response::json($res, 200);
    }

    public function postFilterCity() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Locality\Citi::get_citifilter($param);

//        $clist = [];
//        foreach($data as $d){
//            $clist[$d['name']] = $d['id'];
//        }
//        dd(json_encode($clist));
        $res = \General::success_res();
//        $res['data']=$clist;
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postCheckSpEmail() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'email' => 'unique:sp,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return \Redirect::back()->withErrors($validator);
        }
        return \General::success_res();
    }

    public function postEditServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'email' => 'unique:sp,email,' . $param['spid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
//            return \Response::json($json, 200);
//            dd($validator);
//            return \Redirect::back()->withErrors($validator);
            return \Redirect::back()->withErrors(['email' => ['The email has already been taken.']]);
        }
//        dd($param);
        $res = \App\Models\Serviceprovider\ServiceProvider::edit_sp($param);
        if ($res['flag'] == 1) {
            if ($param['type'] == 'Main') {
                return \Redirect::to('admin/service-provider');
            }
            return \Redirect::to('admin/service-provider/' . $param['type']);
        }
    }

    public function postAddServiceProvider() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'email' => 'unique:sp,email'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
//            return view('admin.login',$view_data)->withErrors($validator);
//            return \Redirect::back()->withErrors($validator)->withInput();
        }
//        dd($param);
        $res = \App\Models\Serviceprovider\ServiceProvider::save_sp($param);
//        return $res;
        if ($res['flag'] == 1) {
            if ($param['type'] == 'Main') {
                return \Redirect::to('admin/service-provider');
            }
            return \Redirect::to('admin/service-provider/' . $param['type']);
        }
    }

    public function postGetMainProvider() {
        $res = \General::success_res();
        $data = \App\Models\Serviceprovider\ServiceProvider::get_sp();
        $res['data'] = $data;
        return $res;
    }

    public function postDelServiceProvider() {
        $param = \Input::all();
        $data = \App\Models\Serviceprovider\ServiceProvider::delete_ServiceProvider($param);
        $res = \General::success_res();
        return \Response::json($res, 200);
    }

    public function postUpdateBusName() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "update_bus_name"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = Serviceprovider\Buses::update_bus_name(\Input::all());
        return \Response::json($res, 200);
    }

    public function postAddBus() {
        $param = \Input::all();
//        print_r($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "add_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $addBus = Serviceprovider\Buses::add_new_bus($param);
//        $res = \General::success_res();
//        $res['data'] = $param;
        return \Response::json($addBus, 200);
    }

    public function postGetBus() {
        $param = \Input::all();
        $bus = Serviceprovider\Buses::where('id', $param['id'])->with(['sp', 'amenities', 'busLayout.seats', 'busPrice', 'busRoutes', 'drivers.driver', 'couponCode'])->first()->toArray();
        $routes = \App\Models\Admin\BusRoutes::where('bus_id', $param['id'])->with(['fromCity', 'toCity', 'fromTerminal', 'toTerminal'])->orderBy('from_city_id', 'asc')->orderBy('to_city_id', 'asc')->get();
//        $b = Serviceprovider\Buses::where('id',$param['id'])->with('busLayout.seats')->get()->toArray();
//        echo '<pre>';
//        print_r($bus);
//        echo '</pre>';
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'sp-main',
                'bus' => $bus,
                'routes' => $routes,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.bus_detail", $view_data);
    }

    public function getBusStructure($bus_id = 0, $sp_id = 0) {
        if (!(int) $bus_id || !(int) $sp_id) {
            return \Response::view('errors.404', array(), 404);
        }

        $bus = Serviceprovider\Buses::where('id', $bus_id)->with('amenities')->with('busPrice')->with('blockDates')->with('busRoutes')->with('busLayout')->with('couponCode')->first();
        $buses = Serviceprovider\ServiceProvider::with('spBuses')->where('id', $sp_id)->first();
        $cities = \App\Models\Locality\Citi::orderBy('name', 'asc')->lists('name', 'id');
        $dist = \App\Models\Locality\District::orderBy('name', 'asc')->lists('name', 'id');
        $routes = \App\Models\Admin\BusRoutes::where('bus_id', $bus_id)->with(['fromCity', 'toCity', 'fromTerminal', 'fromDistrict', 'toDistrict', 'toTerminal', 'prices'])->orderBy('id', 'asc')->get();
        $lay = \App\Models\BusLayout::where('bus_id', $bus_id)->first();
        $drivers = \App\Models\BusDrivers::where('bus_id', $bus_id)->with('driver')->get();
        if (!is_null($lay) && $bus['layout']) {
            $layout = \App\Models\SeatMap::where('layout_id', $lay->id)->get()->toArray();
            $layout = json_encode($layout, true);
        } else {
            $layout = '';
        }

//        print_r($layout);
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => ['jquery.tagsinput.js'
//                    ,'jquery-ui.js'
                    , 'parsley.min.js'
                    , 'jquery.inputmask.bundle.min.js'
                    , 'ThrowPropsPlugin.min.js', 'TweenLite.min.js', 'CSSPlugin.min.js', 'Draggable.min.js'
                    , \URL::to('assets/ckeditor/ckeditor.js')
                ],
                "css" => ['jquery.tagsinput.css'
//                    ,'jquery-ui.css'
                ],
            ],
            'body' => [
                'id' => 'sp-main',
                'bus' => $bus,
                'all_bus' => $buses,
                'city' => $cities,
                'dist' => $dist,
                'routes' => $routes,
                'drivers' => $drivers,
                'layout' => $layout
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.bus_structure", $view_data);
//        return view("admin.bus_structure_old", $view_data);
    }

    public function postGetTerminals() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "get_terminals"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $param = \Input::all();
        $ter_from = \App\Models\Locality\Terminal::get_terminal_from_city($param['from_id']);
        $ter_f = $ter_from->toArray();
        if (count($ter_f) == 0) {
            return \General::error_res('no terminal found for from city');
        }
        $ter_to = \App\Models\Locality\Terminal::get_terminal_from_city($param['to_id']);
        $ter_t = $ter_to->toArray();
        if (count($ter_t) == 0) {
            return \General::error_res('no terminal found for to city');
        }
        $view_data = [
            'header' => [
                "title" => 'Service Provider | Bus Structure',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'from' => $ter_from,
                'to' => $ter_to,
                'param' => $param
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.bus_terminals", $view_data);
//        return view("admin.bus_terminals_old", $view_data);
    }

    public function postGetAmenites() {
        $param = \Input::all();
        $am = \App\Models\Admin\Amenities::get_amenities($param);
        if (is_null($am)) {
            return \General::error_res('no eminities found');
        }
        $r = [];
        foreach ($am as $a) {
            $r[$a['name']] = $a['id'];
        }
        $res = \General::success_res();
        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postEditBus() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "edit_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
//        dd($param);
        if (isset($param['is_layout'])) {
            if (isset($param['seat_array'])) {
                $sa = json_decode($param['seat_array'], true);
//                print_r($sa);
                if (count($sa) == 0) {
                    return \General::error_res('seat map is required.');
                }
            } else {
                return \General::error_res('seat map is required.');
            }
        }
        $editBus = Serviceprovider\Buses::edit_bus($param);
//        print_r($param);
//        return $editBus;
    }

    public function postDrivers() {
        $param = \Input::all();
        $drivers = \App\Models\Drivers::get_driver($param);
        if (is_null($drivers)) {
            return \General::error_res('no drivers found');
        }
        $res = \General::success_res();
        if (isset($param['all']) && $param['all'] == 1) {
            $res['data'] = $drivers;
            return \Response::json($res, 200);
        }
        $r = [];
        foreach ($drivers as $a) {
            $r[$a['name'] . '-' . $a['mobile']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postAddDriverToBus() {
        $param = \Input::all();
        $driver = \App\Models\Drivers::get_driver($param);
        if (!is_null($driver)) {
            $view_data = [
                'driver' => $driver[0]
            ];

            return view("admin.add_driver_to_bus", $view_data);
        }
    }

    public function getDriversList() {
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Drivers',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'driver',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.bus_drivers", $view_data);
    }

    public function postDriversFilter() {
        $param = \Input::all();

        $data = \App\Models\Drivers::get_driver_filter($param);
//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.driver_filter", $res);
        }
        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postAddNewDriver() {
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "driver"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }

        $addD = \App\Models\Drivers::add_new_driver($param);
        return $addD;
    }

    public function postEditDriver() {
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "driver"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }

        $addD = \App\Models\Drivers::edit_driver($param);
        return $addD;
    }

    public function postDeleteDriver() {
        $param = \Input::all();

        return \App\Models\Drivers::delete_driver($param);
    }

    public function postAddCouponCode() {
        $param = \Input::all();

        $validator = \Validator::make(\Input::all(), [
                    'ccode' => 'unique:couponcode,code'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::add_new_coupon_code($param);
    }

    public function postEditCouponCode() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), [
                    'ccode' => 'unique:couponcode,code,' . $param['ccid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        return \App\Models\CouponCode::edit_coupon_code($param);
    }

    public function postDeleteCouponCode() {
        $param = \Input::all();

        return \App\Models\CouponCode::delete_coupon_code($param);
    }

    public function getAmenitiesList() {
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Amenities',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'amenity',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.amenities_list", $view_data);
    }

    public function postAmenitiesFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Amenities::get_amenities_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            $res['type'] = $param['type'];
            return view("admin.amenities_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postDeleteAmenities() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Amenities::delete_amenity($param);
        $res = \General::success_res('Amenity deleted successfully !!');
        return \Response::json($res, 200);
    }

    public function postAddAmenities() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:amenities,name'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $data = \App\Models\Admin\Amenities::add_amenity($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditAmenities() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:amenities,name,' . $param['a_id']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $data = \App\Models\Admin\Amenities::edit_amenity($param);

        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function getPariwisataList() {

        $sp = Serviceprovider\ServiceProvider::where('type', 'Main')->orderBy('first_name', 'asc')->get();
        $city = \App\Models\Locality\Citi::orderBy('name', 'asc')->get();
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Amenities',
                "js" => [
                    'parsley.min.js'
                    , 'jquery.tagsinput.js'
                    , \URL::to('assets/ckeditor/ckeditor.js')
                ],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id' => 'pw',
                'sp' => $sp,
                'city' => $city
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_list", $view_data);
    }

    public function postPariwisataFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Pariwisata::get_pariwisata_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.pariwisata_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postAddPariwisata() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

//            echo '<pre>';
//            print_r($param);
//            echo '</pre>';
//            exit;
        $data = \App\Models\Admin\Pariwisata::add_pariwisata($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditPariwisata() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "pariwisata"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $data = \App\Models\Admin\Pariwisata::edit_pariwisata($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postDeletePariwisata() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Pariwisata::delete_pariwisata($param);
        $res = \General::success_res('Amenity deleted successfully !!');
        return \Response::json($res, 200);
    }

    public function postSpName() {
        $param = \Input::all();
        $sp = Serviceprovider\ServiceProvider::get_sp($param);
        if (is_null($sp)) {
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($sp as $a) {
            $r[$a['first_name'] . ' ' . $a['last_name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postAllSpName() {
        $param = \Input::all();
        $sp = Serviceprovider\ServiceProvider::get_allsp($param);
        if (is_null($sp)) {
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($sp as $a) {
            $r[$a['first_name'] . ' ' . $a['last_name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postAllSsName() {
        $param = \Input::all();
        $sp = \App\Models\Admin\SeatSeller::get_allss($param);
        if (is_null($sp)) {
            return \General::error_res('No Seat Seller found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($sp as $a) {
            $r[$a['name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postSeller() {
        $param = \Input::all();
        $seller = Serviceprovider\ServiceProvider::get_sp($param);
        if (is_null($seller)) {
            return \General::error_res('no service provider found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($seller as $a) {
            $r[$a['first_name'] . ' ' . $a['last_name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postCityName() {
        $param = \Input::all();
        $sp = \App\Models\Locality\Citi::get_allciti($param);
        if (is_null($sp)) {
            return \General::error_res('no city found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($sp as $a) {
            $r[$a['name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function getCardList() {

        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Commuter Management',
                "js" => [
                    'parsley.min.js'
                    , 'jquery.tagsinput.js'
                    , \URL::to('assets/ckeditor/ckeditor.js')
                ],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id' => 'card',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.card_list", $view_data);
    }

    public function postCardFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Card::get_card_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.card_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postAddCard() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "card"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:cards,name'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
//            echo '<pre>';
//            print_r($param);
//            echo '</pre>';
//            exit;
        $data = \App\Models\Admin\Card::add_card($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditCard() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "card"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:cards,name,' . $param['cid']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $data = \App\Models\Admin\Card::edit_card($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postDeleteCard() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Card::delete_card($param);
        $res = \General::success_res('Commuter deleted successfully !!');
        return \Response::json($res, 200);
    }

    public function postGetCouriers() {
        $param = \Input::all();
        $am = \App\Models\Admin\Courier::get_courier($param);
        if (is_null($am)) {
            return \General::error_res('no courier found');
        }
        $r = [];
        foreach ($am as $a) {
            $r[$a['name']] = $a['id'];
        }
        $res = \General::success_res();
        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function getCouriersList() {
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Couriers',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'crr',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.couriers_list", $view_data);
    }

    public function postCouriersFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\Courier::get_courier_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.couriers_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postDeleteCouriers() {
        $param = \Input::all();
//        dd($param);
        $data = \App\Models\Admin\Courier::delete_courier($param);
        $res = \General::success_res('Courier deleted successfully !!');
        return \Response::json($res, 200);
    }

    public function postAddCouriers() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:courier,name'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $data = \App\Models\Admin\Courier::add_courier($param);


        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditCouriers() {
        $param = \Input::all();
//        dd($param);


        $validator = \Validator::make(\Input::all(), [
                    'name' => 'unique:courier,name,' . $param['c_id']
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $data = \App\Models\Admin\Courier::edit_courier($param);

        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function getSettings() {

        $settings = \App\Models\Admin\Settings::all_settings();
        $settings = $settings['data'];
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Settings',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'settings',
                'settings' => $settings,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.settings_list", $view_data);
    }

    public function postSaveSettings() {
        $param = \Input::all();
//        print_r($param);
        $set = \App\Models\Admin\Settings::save_settings($param);
        return $set;
    }

    public function getUsersList() {
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Users',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'list',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.users_list", $view_data);
    }

    public function postUsersFilter() {
        $param = \Input::all();
        $data = \App\Models\Users::get_user_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.users_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postGetUser() {
        $param = \Input::all();
        $user = \App\Models\Users::where('id', $param['id'])->first()->toArray();

        $view_data = [
            'header' => [
                "title" => 'Admin | Users',
                "js" => [],
                "css" => [],
            ],
            'body' => [
                'id' => 'list',
                'user' => $user,
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];

        return view("admin.user_detail", $view_data);
    }

    public function postEditUser() {
        $param = \Input::all();
        $user = \App\Models\Users::edit_user($param);
        return $user;
    }

    public function postDeleteUser() {
        $param = \Input::all();
        $user = \App\Models\Users::delete_user($param);
        return $user;
    }

    public function getFavoriteRoute() {
        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Favorite Routes',
                "js" => ['parsley.min.js'],
                "css" => [],
            ],
            'body' => [
                'id' => 'fav_route',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.fav_route_list", $view_data);
    }

    public function postRouteFilter() {
        $param = \Input::all();
        $data = \App\Models\Admin\FavoriteRoute::get_route_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;
            return view("admin.fav_route_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postAddRoute() {
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "fav_route"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $exist = \App\Models\Admin\FavoriteRoute::where(['from' => $param['from_city_id'], 'to' => $param['to_city_id'], 'bus_id' => $param['bus_id']])->first();
        if (!is_null($exist)) {
            return \General::error_res('this route is already exist !!');
        }
        $data = \App\Models\Admin\FavoriteRoute::add_route($param);
        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postEditRoute() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "fav_route"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $exist = \App\Models\Admin\FavoriteRoute::where(['from' => $param['from_city_id'], 'to' => $param['to_city_id'], 'bus_id' => $param['bus_id']])->where('id', '!=', $param['cid'])->first();
        if (!is_null($exist)) {
            return \General::error_res('this route is already exist !!');
        }
        $user = \App\Models\Admin\FavoriteRoute::edit_route($param);
        $res = \General::success_res();
        $res['data'] = $user;
        return \Response::json($res, 200);
    }

    public function postDeleteRoute() {
        $param = \Input::all();
        $user = \App\Models\Admin\FavoriteRoute::delete_route($param);
        return $user;
    }

    public function postBusName() {
        $param = \Input::all();
        $bus = Serviceprovider\Buses::get_bus($param);
        if (is_null($bus)) {
            return \General::error_res('no bus found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($bus as $a) {
            $r[$a['name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function postBus() {
        $param = \Input::all();
        $bus = Serviceprovider\Buses::get_bus($param);
        if (is_null($bus)) {
            return \General::error_res('no bus found');
        }
        $res = \General::success_res();

        $r = [];
        foreach ($bus as $a) {
            $r[$a['name']] = $a['id'];
        }

        $res['data'] = $r;
        return \Response::json($res, 200);
    }

    public function getPariwisataOrder() {

        $view_data = [
            'header' => [
                "title" => 'Admin Panel | Pariwisata Order',
                "js" => [],
                "css" => ['jquery.tagsinput.css'],
            ],
            'body' => [
                'id' => 'pwo',
            ],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("admin.pariwisata_order_list", $view_data);
    }

    public function postPariwisataOrderFilter() {
        $param = \Input::all();
        $data = \App\Models\PariwisataOrder::get_pariwisata_order_list($param);

//        dd($data);
        if ($data['flag'] == 1) {
            $res = \General::success_res();
            unset($data['flag']);
            $res['data'] = $data;

            return view("admin.pariwisata_order_filter", $res);
        }

        $res = \General::error_res('No Data Found.');
        unset($data['flag']);

        return \Response::json($res, 200);
    }

    public function postApprovePariwisataOrder() {
        $param = \Input::all();
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("admin", "ss-deposit"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 401);
        }
        $res = \App\Models\PariwisataOrder::approve_order($param);

        return \Response::json($res, 200);
    }

    public function postGetOptions() {
        $param = \Input::all();
        $id = $param['id'];
        $t = $param['type'];
        $data = [];
        if ($t == 'c') {
            $data = \App\Models\Locality\District::where('city_id', $id)->orderBy('name', 'asc')->get()->toArray();
        } elseif ($t == 'd') {
            $data = \App\Models\Locality\Terminal::where('district_id', $id)->orderBy('name', 'asc')->get()->toArray();
        }
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    public function postUpdateUserPassword() {
        $user = \Auth::guard('user')->user()->toArray();
        $data = \Input::all();
        $resp = \App\Models\General::formValidation(\Input::all());
        if ($resp['flag'] == 1) {
            $pass = \Hash::make($data['password']);
            $res = DB::table('users')->where('id', $user['id'])->update(['password' => $pass]);
            return \General::success_res("Password Updated Successfully");
        } else {
            return \General::error_res(json_encode($resp['va']));
        }
    }

    public function postUpdatePromoStatus() {
        $data = \Input::all();
        $user = \Auth::guard('user')->user()->toArray();
        $res = DB::table('users')->where('id', $user['id'])->update(['promo_offer' => $data['chkval']]);
        return \General::success_res("Your Request successfully submited ");
    }

    public function postCheckBooking() {
        $param = \Input::all();
        $user = \Auth::guard('user')->user()->toArray();
        $resp = \App\Models\General::checkBookingNewFormValidation(\Input::all());
        if ($resp['flag'] == 1) {
            $tik_id = $param['ticketNumber'];
            $email = $param['email'];
            $type = $param['flags'];
            $flag = 1;
            if ($type == 1) {
                $flag = 0;
            }

            //$travelData=DB::table('bookings')->join('booking_seats','booking_seats.booking_id','=','bookings.booking_id')->join('booking_seats','=','booking_seats.ticket_id',$tik_id)->where('bookings.user_id',$user['id'])->get();
            $tkCheck = DB::select("select count(*) as tr from booking_seats as bs,bookings as b where bs.ticket_id='{$tik_id}' and  bs.booking_id=b.booking_id and b.user_id='{$user['id']}'");

            if ($tkCheck[0]['tr'] > 0) {


                $chekBook = \App\Models\BookingSeats::check_booking_exist($tik_id, $email, $flag);
                if ($chekBook['flag'] != 1) {
                    return $chekBook;
                }
                if ($type == 2 || $type == 0) {

                    $tiket_detail = \App\Models\Bookings::where('booker_email', $email)->whereHas('bookingSeats', function($q)use($tik_id) {
                                $q->where('ticket_id', $tik_id);
                            })->with(['sp', 'bus', 'fromTerminal.locCiti', 'fromTerminal.locDistrict', 'toTerminal.locCiti', 'toTerminal.locDistrict', 'bookingSeats'])->first()->toArray();
                    if ($tiket_detail['status'] != 1) {

                        return \General::error_res('ticket is not confirmed yet.');
                    }
                    $tiket_detail['mail_subject'] = "E-tiket Detail Perjalanan Anda dari www.bustiket.com";
                    $file_path = config('constant.TICKET_PDF_PATH') . 'bustiket_' . $tiket_detail['booking_id'] . '.pdf';
                    $html = \Mail::send('site.e_ticket', array('data' => $tiket_detail), function ($message) use ($tiket_detail, $file_path) {
                                $message->to($tiket_detail['booker_email'])->subject($tiket_detail['mail_subject']);
                                if (File::exists($file_path)) {
                                    $message->attach(($file_path));
                                }
                            });
                    $chekBook['msg'] = 'email sent succussfully.';
                    return $chekBook;
                } else if ($type == 1) {
                    $sms = \App\lib\Services::send_sms($tik_id);
                    return $sms;
                }
                $chekBook['flag'] = 3;
                return $chekBook;
            } else {
                return \General::error_res("Ticket Not Found");
            }
        } else {
            return \General::error_res(json_encode($resp['va']));
        }
    }

    public function anyProfile() {
        $user = \Auth::guard('user')->user()->toArray();
        $bookId = DB::table('bookings')->select('booking_id')->where('user_id', $user['id'])->get();
        //dd($bookId);
        $travelData = DB::table('booking_seats')->join('bookings', 'booking_seats.booking_id', '=', 'bookings.booking_id')->whereIn('booking_seats.booking_id', $bookId)->get();
//        dd($travelData);
        if (request()->isMethod('post')) {
            $resp = \App\Models\General::userProfileFormValidation(\Input::all());
            if ($resp['flag'] == 1) {
                $tmpa = \Input::all();
                $keys = array_keys($tmpa);
                $tmpArray = $tmpa[$keys[1]];
                $name = $tmpArray[0]['value'];
                $mobile = $tmpArray[1]['value'];
                $email = $tmpArray[2]['value'];
                $uid = $user['id'];
                $res = Users::where('id', $uid)->update(['name' => $name, 'mobile' => $mobile, 'email' => $email]);
                return \General::success_res("Profile Information Updated successfully");
            } else {
                return \General::error_res(json_encode($resp['va']));
            }
        } else {


            $token = \App\Models\Token::where('user_id', $user['id'])->first()->toArray();
            $view_data = [
                'header' => [
                    "title" => "Tiket Bus Online Murah, Lengkap dan Aman di Indonesia – Pesan Tiket Bus Online di BUSTIKET.COM",
                    "js" => [],
                    "css" => ["assets/css/app.min.css", "assets/css/search.min.css"],
                    "authToken" => isset($token['token']) ? $token['token'] : ''
                ],
                'body' => [$user, $travelData],
                'footer' => [
                    "js" => [],
                    "css" => []
                ],
            ];
            return view("site.user_profile", $view_data);
        }
    }

    public function getAuthGoogle() {
        return \Socialite::driver('google')->redirect();
    }

    public function redirectGoogle() {
        $user = \Socialite::driver('google')->user();
        if (is_null($user) || $user->email == "" || $user->email == null) {
            $res = \General::error_res("you didn't give proper permission. Please try again");
            \Session::set("msg",  json_encode($res));
            return \Redirect::to("login");
        }
        $access_token = $user->token;
        $code = \Input::get("code");
        $user_email = $user->email;
        $user_name = $user->name;
        $param = [
            "type" => 'google',
            'access_token' => $access_token,
            "device_token" => $code,
            "user_email" => $user_email,
            "user_firstname" => $user_name
        ];
        Users::do_login($param);
        return \Redirect::to("/");
    }

    
    public function getAuthFacebook() {
        return \Socialite::driver('facebook')->scopes(['email'])->redirect();
    }

    public function redirectFacebook() {
        $user = \Socialite::driver('facebook')->user();
        if (is_null($user) || $user->email == "" || $user->email == null) {
            $res = \General::error_res("you didn't give proper permission. Please try again");
            \Session::set("msg",  json_encode($res));
            \Socialite::driver('facebook')->deauthorize($user->token);
            return \Redirect::to("login");
        }
        $access_token = $user->token;
        $code = \Input::get("code");
        $user_email = $user->email;
        $user_name = $user->name;
        $param = [
            "type" => 'facebook',
            'access_token' => $access_token,
            "device_token" => $code,
            "user_email" => $user_email,
            "user_firstname" => $user_name
        ];
        Users::do_login($param);
        return \Redirect::to("/");
    }
    
}
