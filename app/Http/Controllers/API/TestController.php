<?php 
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

class TestController extends Controller {
    
    public function __construct()
    {
//        $this->beforeFilter('isDown');
    }
    
    public function filter()
    {
        
    }
    
    public function code()
    {
        $session = session('key');
        
        if(isset($session) && $session == "default")
        {
           \Log::info("Session data alreay exist: ".$session);
//           \Session::pull('key');
        }
        else
        {
            session(['key' => 'value']);
            $session = session('key');
            \Log::info("Session data saved: ".$session);
        }
        
        
        return \General::error_res("wait");
        
        
        phpinfo();
//        $m = \App\Models\Services\General::formate_mobile_no("+62082111780780");
//        $m = \App\Models\Services\General::formate_mobile_no("82620780780");
//        $m = \App\Models\Services\General::formate_mobile_no("+3182111780780");
//        dd($m);
//        $user_detail = ['user_email' => 'a@a.a','user_password' => "123456","mail_subject" => "Test"];
//        $html = \View::make("emails.user.account_confirm",$user_detail);
//        return $html;
        
//        $user_detail = ['user_email' => 'a@a.a','user_password' => "123456","mail_subject" => "Test","user_firstname" => "OOOOOO"];
//        $html = \View::make("emails.user.forget_password",$user_detail);
//        return $html;
        
        
//        $user_detail = [
//                "mail_subject" => "Test",
//                "uniqid" => "MY1422",
//                "expire_time" => "19:20",
//                "bank_account_name" => "Abc Name",
//                "bank_account_no" => "201-2220888888",
//                "final_payable_amount" => "334.000",
//                "total_amt" => "360.000",
//                "discount_price" => "36.000",
//                "handling_fee" => "10.000",
//                "date" => "2016-05-09",
//                "bording_time" => "Agen SWAN TRANS Pulo Gadung--01:00 pm",
//                "departure_time" => "Solo--12:00 am",
//                "rute" => "Pulo Gadung - Semarang - Solo - Sragen - Ngawi - Madiun - Ponorogo",
//                "passenger" => [
//                    ["name" => "P1","lastname" => "l1","seat_no" => 2],
//                    ["name" => "P2","lastname" => "l2","seat_no" => 3],
//                ],
//            ];
//        // Make template
//        $html = \View::make("emails.user.ticket_pay_by_bank_transfer_code",$user_detail);
//        return $html;
        
//        $user_detail = [
//
//                        "email" => "ppapap@aaa.aaa",
//                        "sp_name" => "GSM",
//                        "parent_bus" => ["par_name" => "parent bus 1"],
//                        "mail_subject" => "E-Tiket from BUSTIKET.COM",
//                        "final_payable_amount" => 334.000,
//                        "total_amt" => 360.000,
//                        "discount_price" => 36.000,
//                        "handling_fee" => 10.000,
//                        "date" => "2016-02-02",
//                        "bording_time" => "Jakarta -- 02:00 am",
//                        "boading_address" => "Laxman nagar",
//                        "departure_time" => "Solo -- 02:30 am",
//                        "rute" => "aaa-bbb-ccc-xxxx",
//                        "passenger" => [
//                                    ["name" => "P1","lastname" => "l1","seat_no" => 2,"passenger_seatlabel" => 2,"passenger_Name" => "Pasresh 1","unique_id" => "22Admin171441","passenger_Gender" => "M","passenger_Age" => "20" ],
//                                    ["name" => "P2","lastname" => "l2","seat_no" => 3,"passenger_seatlabel" => 3,"passenger_Name" => "Pasresh 2","unique_id" => "22Admin171442","passenger_Gender" => "L","passenger_Age" => "22"],
//                                ],
//                        "from_city" => ["city_name" => "Surat"],
//                        "to_city" => ["city_name" => "Jaka"],
//                        "ticket_id" => "2164sdfsjj",
//                        "layout" => 0,
//                    ];
//
//        
//        $html = \View::make("emails.user.e_ticket",$user_detail);
//        return $html;
        
        $url = \App\Models\User\User::get_image_url("1","1463643074.JPG");
        //dd($url);
        
    }
    public function getPush()
    {
        $param = \Input::all();
        if(!isset($param['device_token']) || $param['device_token'] == "")
           // dd("Enter device_token");
        
        $param['msg'] = isset($param['msg']) ? $param['msg'] : "You missed msg parameter";
        $param['title'] = isset($param['title']) ? $param['title'] : "You missed title parameter";
        $param['screen'] = isset($param['screen']) ? array($param['screen']) : array("informasi");
        $push = new \App\Lib\Push();
        $meta = array(array("ticket_id" => "tkt123456"));
        $img = array("http://www.d9q.com/admin/img/photos/1468585550758.jpg");
        $res = $push->android_push([$param['device_token']],[$param['msg']],[$param['title']],[1],$param['screen'],$meta,[],[$img]);
       // dd($res);
    }
    public function getPayment()
    {
        $view_data = [
                'header' => [
                        "title" => "Payment ".\Config::get("constant.TITLE_SEPARATOR").\Config::get("constant.PLATFORM_NAME"),
                        "js" => ["home.js"],
                        "css" => [],
                    ],
                'body' => [
                       "message"=>"",
                    
                    ],
                'footer' => [
                    "js" => [],
                    "css" => []
                    ],
            ];
            return view("site.payment_success",$view_data);
    }
    
    
     public function getEvent()
    {
        $view_data = [
                'header' => [
                        "title" => "Events ".\Config::get("constant.TITLE_SEPARATOR").\Config::get("constant.PLATFORM_NAME"),
                        "js" => ["event.js","moment.js","daterangepicker.js"],
                        "css" => ["event.css","wowstyle.css"],
                    ],
                'body' => [
                       "message"=>"",
                    
                    ],
                'footer' => [
                    "js" => [],
                    "css" => []
                    ],
            ];
            return view("site.events",$view_data);
    }
    
    
    public function getButton()
    {
        $view_data = [
                'header' => [
                        "title" => "Payment ".\Config::get("constant.TITLE_SEPARATOR").\Config::get("constant.PLATFORM_NAME"),
                        "js" => ["home.js"],
                        "css" => [],
                    ],
                'body' => [
                       "message"=>"",
                    
                    ],
                'footer' => [
                    "js" => [],
                    "css" => []
                    ],
            ];
            return view("site.test_btn",$view_data);
    }
}
