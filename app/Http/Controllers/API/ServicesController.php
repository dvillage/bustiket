<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

class ServicesController extends Controller {

    private static $bypass_url = ['anyGetRequestToken','postPayTicketByCc','postTourTrip','getPayTicketByDokuWallet','getBuswayListing','getBswBankDetail','postSplash','getBswPayByDokuWallet', 'getPasswordReset', 'getMobileRecharge', 'getDthRecharge', 'getDatacardRecharge', 'getAddMoney', 'getMobileRechargeByWallet', 'getDatacardRechargeByWallet', 'getDthRechargeByWallet', 'postChangePassword', 'getGasRecharge', 'getElectricitybillRecharge', 'getElectricitybillRechargeByWallet', 'getGasRechargeByWallet', 'postDashboardScreen', 'postResendMailVerification', 'getAccountConfirmation', 'postForgetPassword', 'postOtpVerification', 'postResetPassword', 'getBusMap', 'getBusSeatSelected', 'getPrivacyPolicy', 'getTermsCondition', 'getPayTicketByBank','getHello'];
    

    public function __construct() {
        
        $this->middleware('GuestAuth', ['except' => self::$bypass_url]);
    }
    
    public function getHello(){
         return 'Hello';
    }

    public function anyGetRequestToken() {
        //echo "dd";
        \Log::info('Get Request Token Call');
        $settings = app('settings');
        $res = \General::success_res();
//      $res['data'] = ['request_token' => $settings['request_token']];
        $res['data'] = ['request_token' => csrf_token()];
        \Log::info(json_encode($res));
        if(isset($res) || !empty($res)){
          return \Response::json($res, 200);  
        }
        else{
            return "error";
        }
        
    }

    public function getPrivacyPolicy() {
        $data = ["title" => "Privacy Policy | Bustiket"];
        return \View::make("m.pp", $data);
    }

    public function getTermsCondition() {
        $data = ["title" => "Terms Condition | Bustiket"];
        return \View::make("m.tnc", $data);
    }

    public function postSplash() {
        \Log::info('Splash Screen Call');
        \Log::info(\Input::all());
        $settings = app('settings');
//        dd(app("platform"));
        $app = app("platform") == "1" ? json_decode($settings['android_app'],true) : [];
        $data = [
//            'request_token' => $settings['request_token'],
            'request_token' => csrf_token(),
            'app' => $app,
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

    public function postGetFromCityList() {
        \Log::info('Get From City List Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "get_from_city_list"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\Locality::from_city_list(\Input::all());
        \Log::info('Get From City List Response : '.json_encode($res));
//        $res = \App\Models\Services\General::from_city_list(\Input::all());
        return \Response::json($res, 200);
    }

    public function postGetPromoBanner1() {
        $res = \App\Models\Services\General::get_promo_banner1(\Input::all());
        return \Response::json($res, 200);
    }

    public function postGetPromoBanner2() {
        $res = \App\Models\Services\General::get_promo_banner2(\Input::all());
        return \Response::json($res, 200);
    }

    public function postGetToCityList() {
        \Log::info('Get To City List Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "get_to_city_list"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\Locality::to_city_list(\Input::all());
        \Log::info('Get To City List Response : '.json_encode($res));
//        $res = \App\Models\Services\General::to_city_list(\Input::all());
        return \Response::json($res, 200);
    }

    public function postSearchBus() {
        \Log::info('Search Bus Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "search_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $param['len'] = isset($param['len']) ? $param['len'] : 100;
        $param['start'] = isset($param['start']) ? $param['start'] : 0;
        $res = \App\Models\Services\General::search_bus($param);
        \Log::info('Search Bus Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function getBusMap() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "bus_map"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['title'] = $json['msg'] = $error[0];
            return \View::make("site.blank_error", $json);
        }
        $param = \Input::all();
        $data = \App\Models\BusStructure::prepar_bus_layout($param);
        if (isset($data['flag']) && $data['flag'] != 1) {
            $json['title'] = $json['msg'] = $data['msg'];
            return \View::make("site.blank_error", $json);
        }
        return \View::make("site.busmap", $data);
    }

    public function getBusSeatSelected() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "bus_seat_selected"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $data = \App\Models\BusStructure::bus_seat_selected($param);
        return \Response::json($data, 200);
    }

    public function postBusMapScreen() {
        \Log::info('Bus Map Screen Request : '.json_encode(\Input::all()));

        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "bus_map_screen"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $is_roda = 0;
        if(preg_match("/[a-z]/i", $param["bus_id"])){
            $is_roda = 1;
        }
        
        $from_city = \App\Models\Locality::where("id", $param['from_id'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $to_city = \App\Models\Locality::where("id", $param['to_id'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        
        $from_city = $from_city->toArray();
        $to_city = $to_city->toArray();
//        dd($from_city,$to_city);
        $data = [
            'from' => [
                'name' => $from_city['name'],
                'type' => $from_city['type'],
            ],
            'to' => [
                'name' => $to_city['name'],
                'type' => $to_city['type'],
            ],
            'date' => $param['date'],
            'nop' => isset($param['nop']) ? $param['nop'] : 1,
            'bus_id' => $param['bus_id'],
        ];
        
        $routes_data = \App\Models\Admin\BusRoutes::get_bus_routes($data,$param);
//        dd($routes_data);
        if(isset($routes_data['flag']) && $routes_data['flag'] == 0){
            return $routes_data;
        }
        
        $routes = [];
        foreach($routes_data as $key=>$r){
            $d = [
                'r_id'          => $r['id'],
                'r_route'       => $r['route'],
                'r_from_id'     => $r['from_terminal_id'],
                'r_from_name'   => $r['from_terminal']['name'],
                'r_to_id'       => $r['to_terminal_id'],
                'r_to_name'     => $r['to_terminal']['name'],
                'r_board_time'  => $r['boarding_time'],
                'r_drop_time'   => $r['droping_time'],
//                'r_price'       => $r['price'],
                'r_price'       => \General::number_format($r['price']),
            ];
            $routes[] = $d;
        }
//        dd($param);
        $data = [
            'bus_id'    => $param['bus_id'],
            'date'      => $param['date'],
            'nop'       => $param['nop'],
        ];
        $seatmap = \App\Models\SeatMap::seatmap($data);

        if(isset($seatmap['cols']) && isset($seatmap['rows']) && isset($seatmap['seats'])){
            $seats = [];
            $k = 0;
            for($i=0;$i<$seatmap['cols'];$i++){
                for($j=0;$j<$seatmap['rows'];$j++){
                    foreach($seatmap['seats'] as $key=>$s){
                        if($i == $s['col'] && $j == $s['row']){
                            $s['col'] = $j;
                            $s['row'] = $i;
                            $seats[$k] = $s;
                        }
                    }
                    if(!isset($seats[$k])){
                        $seats[$k] = null;
                    }
                    $k++;
                }
            }

            $t = $seatmap['rows'];
            $seatmap['rows'] = $seatmap['cols'];
            $seatmap['cols'] = $t;
            $seatmap['seats'] = $seats;
        }
        
        if(!isset($seatmap['rows'])){
            $seatmap['rows'] = 0;
        }
        if(!isset($seatmap['cols'])){
            $seatmap['cols'] = 0;
        }
        if(!isset($seatmap['seats'])){
            $seatmap['seats'] = [];
        }
        if(!isset($seatmap['booked'])){
            $seatmap['booked'] = [];
        }
        if(!isset($seatmap['blocked'])){
            $seatmap['blocked'] = [];
        }
        if(!isset($seatmap['already_blocked'])){
            $seatmap['already_blocked'] = [];
        }
        
        $res = \General::success_res();
        $res['routes'] = $routes;
        $res['seatmap'] = $seatmap;
        if(isset($seatmap['flag']) && $seatmap['flag'] == 0){
            $res = \General::error_res('no seat layout found.');
        }
        \Log::info('Bus Map Screen Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postReserveSeat() {
        \Log::info('Reserve Seat Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "reserve_seat"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
//        dd(json_decode($param['selected_seats'],true));
        $seat_data = json_decode($param['selected_seats'],true);
        $route_code = '';
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
            $data = [
                'bus_id' => $param['bus_id'],
                'passenger' => $seat_data,
                'total_seats' => $param['total_seats'],
                'date' => $param['date'],
            ];
            $available = \App\Models\Bookings::check_availability($data);
            if($available['flag'] == 0){
                return $available;
            }
        }
//        dd($seat_data);
//        dd($param);
        $ex = 0;
        if(preg_match("/[a-z]/i", $param['bus_id'])){
            $roda = \Session::get('roda_bus');
            $lorena = 0;
            if (stripos($param['bus_id'],'_LORENA') !== false) {
                $lorena = 1;
                $roda = \Session::get('lorena_bus');
            }
            $ex = 1;
            if(!$roda){
                if($lorena){
                    $rodaR = \App\Models\Lorena::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                    $route_code = str_replace('_LORENA', '', $param['bus_id']);
                }else{
                    $rodaR = \App\Models\Roda::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                    $route_code = $param['bus_id'];
                }
            }
            
            $rbus = \App\Models\Roda::get_roda_bus($roda, $param['bus_id']);
            if($lorena){
                $allTr = \App\Models\Lorena::get_terminals($rbus, $param);
            }else{
                $allTr = \App\Models\Roda::get_terminals($rbus, $param);
            }
            
            if($allTr['flag'] == 0){
                return $allTr;
            }
            $ter = $allTr['data'];
            $b_terminal = [
                'name' => $ter['board_point'],
                'loc_citi' => ['name' => $rbus['from']],
                'address' => $ter['board_point'] .' , '.$rbus['from'],
            ];
            $d_terminal = [
                'name' => $ter['drop_point'],
                'loc_citi' => ['name' => $rbus['to']],
                'address' => $ter['drop_point'] .' , '.$rbus['to'],
            ];
//            dd($b_terminal,$d_terminal);
            if($lorena){
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            
            $busname = [
                'name'=>$rbus['bus'],
                'sp'=>$rsp,
            ];
            $per_head_amount = $ter['fare'];
//            dd($ter);
        }else{
            $busname = \App\Models\Serviceprovider\Buses::where('id',$param['bus_id'])->with('sp')->first();
            if($busname == null){
                $json = \General::error_res('Bus ID is Invalid.');
                return \Response::json($json,200);
            }
            
            //-------- Start : Check Route exists or not --------------//
            $chk_route = \App\Models\Admin\BusRoutes::where('bus_id',$param['bus_id'])
                                    ->where('from_terminal_id',$param['from_term_id'])
                                    ->where('to_terminal_id',$param['to_term_id'])->first();
            if($chk_route == null){
                $json = \General::error_res('Invalid Route.');
                return \Response::json($json,200);
            }
            //-------- Over : Check Route exists or not --------------//
            
            $busname = $busname->toArray();
            $data = [
                'from_term' => $param['from_term_id'],
                'to_term'   => $param['to_term_id'],
                'nop'       => $param['total_seats'],
                'bus_id'    => $param['bus_id'],
                'date'      => $param['date'],
            ];
            $per_head_amount = \App\Models\Admin\BusRoutes::get_route_price($data);
        }
        
        $seat_index = [];
        $seat_label = [];
        $pass_name  = [];
        $pass_gender= [];
        $pass_mo    = [];
        $pass_age   = [];
        foreach($seat_data as $key=>$s){
            $seat_index[$key]   = $seat_data[$key]['seat_index'];
            $seat_label[$key]   = $seat_data[$key]['seat_lbl'];
            $pass_name[$key]    = $seat_data[$key]['passenger_name'];
            $pass_gender[$key]  = $seat_data[$key]['passenger_gender'] == 'L' ? 'M':'F';
            $pass_mo[$key]      = $seat_data[$key]['passenger_mobile'];
            $pass_age[$key]      = $seat_data[$key]['passenger_age'];
        }
        
        $data = [
            "session_id"        => $param['sess_id'],
            "bus_id"            => $param['bus_id'],
            "board_point"       => [
                "b_terminal_id" => $param['from_term_id'],
                "boarding_datetime" => $param['date'],
            ],
            'drop_point'        =>[
                "d_terminal_id" => $param['to_term_id'],
            ],
            "bus_name"          => $busname["name"],
            'per_head_amount'   => $per_head_amount,
            "total_seat"        => $param['total_seats'],
            "seat_index"        => $seat_index,
            "seat_label"        => $seat_label,
            "pass_name"         => $pass_name,
            "pass_gender"       => $pass_gender,
            "pass_mo"           => $pass_mo,
            "pass_age"          => $pass_age,
            "sp_name"           => $busname['sp']['first_name'].' '.$busname['sp']['last_name'],
        ];
        
        $saved_seat = \App\Models\BookingSeats::save_bookingseat_detail($data);
        $handlefee  = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($busname['sp']['id']);
        $hfval      = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval  = ceil( (($param['total_seats']*$per_head_amount) * $handlefee[0]['handling_fee']) / 100 );
        }
        
        if($ex == 0){
            $b_terminal = \App\Models\Locality\Terminal::where('id',$saved_seat['data']['from_terminal'])->with('locCiti')->first()->toArray();
            $d_terminal = \App\Models\Locality\Terminal::where('id',$saved_seat['data']['to_terminal'])->with('locCiti')->first()->toArray();
        }
//        dd($saved_seat,$b_terminal,$d_terminal);
        foreach($seat_label as $key=>$s){
            if($s == ''){
                $seat_label[$key] = 'Saat Keberangkatan'; 
            }
        }
        
        $b_time = $param['boarding_point'];
        $d_time = $param['dropping_point'];
        
        $pick_date = date('Y-m-d',strtotime($param['date'].' '.$b_time));
        $drop_date = date('Y-m-d',strtotime($param['date'].' '.$d_time));

        if($d_time < $b_time){
            $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' +1 day '.$d_time));
        }
        
        $res = \General::success_res();
//        $res['data']['handling_fee'] = number_format($hfval, 3, '.', '.');
        $res['data']['handling_fee']         = \General::number_format($hfval);
        $res['data']['selected_seats']       = $seat_label;
        $res['data']['boading_address']      = $b_terminal['loc_citi']['name'].' ( '.$b_terminal['name'].' )';
        $res['data']['boading_full_address'] = $b_terminal['address'];
        $res['data']['dropping_address']     = $d_terminal['loc_citi']['name'].' ( '.$d_terminal['name'].' )';
        $res['data']['dropping_full_address']= $d_terminal['address'];
        $res['data']['route_code']= $route_code;
        $res['data']['from_date']= $pick_date;
        $res['data']['to_date']= $drop_date;
//        dd($res);
//        $res = \App\Models\BookingInfo::reserve_seat($param);
        \Log::info('Reserve Seat Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postApplyCouponCode() {
        \Log::info('Apply Coupon Code Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "apply_coupon_code"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
//        dd($param);
        if(preg_match("/[a-z]/i", $param['bus_id'])){
            $res = \General::error_res('Invalid Coupon Code ..!');
            return \Response::json($res);
        }
        $data = [
            'ccode' => isset($param['coupon_code']) ? $param['coupon_code'] : '',
            'busid' => $param['bus_id'],
        ];
        $d = \App\Models\CouponCode::find_coupon($data);
        if($d['flag'] == 1){
            if($d['data'][0]['min_amount'] <= $param['total_seat_payment'] && $d['data'][0]['max_amount'] >= $param['total_seat_payment']){
                $res = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                $res['data']['discount_percent'] = 0;
                if($d['data'][0]['type'] == 'P'){
                    $discount = ($param['total_seat_payment'] * $d['data'][0]['value'] / 100);
                    $res['data']['discount_percent'] = $d['data'][0]['value'];
                }
                $res['data']['total_discounted_price'] = \General::number_format((($param['total_seat_payment'] - $discount)* 1000),3);
                $res['data']['discount_amount'] = \General::number_format(($discount * 1000),3);
                
            }
            else{
                $res = \General::error_res('Coupon Code Not Applied.');
            }
        }
        else{
            $res = \General::error_res('Invalid Coupon Code..!');
        }
        \Log::info('Apply Coupon Code Response : '.json_encode($res));
        return \Response::json($res);
    }

    public function postBookTicket() {
        \Log::info('Book Ticket Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "book_ticket"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $param = \Input::all();
        
        $b_time = explode(':', $param['boarding_time']);
        $d_time = explode(':', $param['departure_time']);
        
        $pick_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['boarding_time']));
        $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['departure_time']));

        if($d_time[0] < $b_time[0]){
            $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' +1 day '.$param['departure_time']));
        }
        
        if(preg_match("/[a-z]/i", $param['bus_id'])){
            $lorena = 0;
            $booking_system = 1;
            if (stripos($param['bus_id'],'_LORENA') !== false) {
                $lorena = 1;
                $booking_system = 2;
            }
            if($lorena){
                $rodaR = \App\Models\Lorena::get_bus_from_db($param);
//                \Log::info('lorena bus : '.json_encode($rodaR));
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }else{
                $rodaR = \App\Models\Roda::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }
            
            $rbus = \App\Models\Roda::get_roda_bus($roda, $param['bus_id']);
            
            if($lorena){
                $allTr = \App\Models\Lorena::get_terminals($rbus, $param);
            }else{
                $allTr = \App\Models\Roda::get_terminals($rbus, $allTr);
            }
            
            if($allTr['flag'] == 0){
                return $allTr;
            }
            $ter = $allTr['data'];
            $board_point = [
                'name' => $ter['board_point'],
                'loc_citi' => ['name' => $rbus['from']],
                'loc_district' => ['name' => ''],
            ];
            $drop_point = [
                'name' => $ter['drop_point'],
                'loc_citi' => ['name' => $rbus['to']],
                'loc_district' => ['name' => ''],
            ];
//            dd($b_terminal,$d_terminal);
            if($lorena){
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            
            $bus_details = [
                'name'=>$rbus['bus'],
                'sp'=>$rsp,
            ];
            $base_amount = $ter['fare'];
        }else{
            $booking_system = 0;
            $bus_details = \App\Models\Serviceprovider\Buses::where('id',$param['bus_id'])->with('sp')->first()->toArray();
            $board_point = \App\Models\Locality\Terminal::where('id',$param['from_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();
            $drop_point = \App\Models\Locality\Terminal::where('id',$param['to_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();

            $data = [
                'from_term' => $param['from_term_id'],
                'to_term'   => $param['to_term_id'],
                'nop'       => $param['total_seats'],
                'bus_id'    => $param['bus_id'],
                'date'      => $param['date'],
            ];
            $base_amount = \App\Models\Admin\BusRoutes::get_route_price($data);
        }
        
        $totalamount = $base_amount * $param['total_seats'];
        
        $data = [
            'ccode' => isset($param['coupon_code']) ? $param['coupon_code']:'',
            'busid' => $param['bus_id'],
        ];
        
        $hfval = 0;
        $discount = 0;
        $d = \App\Models\CouponCode::find_coupon($data);
        if($d['flag'] == 1){
            if($d['data'][0]['min_amount'] <= ($totalamount / 1000) && $d['data'][0]['max_amount'] >= ($totalamount / 1000)){
                $cc = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                if($d['data'][0]['type'] == 'P'){
                    $discount = (($totalamount / 1000) * $d['data'][0]['value'] / 100);
                    
                }
                $discount = ($discount * 1000);
            }
            else{
                $cc = \General::error_res('Coupon Code Not Applied.');
            }
        }
        else{
            $cc = \General::error_res('Invalid Coupon Code..!');
        } 
        
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($bus_details['sp']['id']);
        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( (($totalamount) * $handlefee[0]['handling_fee']) / 100 );
        }
        
        $total_amount = $totalamount - $discount + $hfval;
        
//        dd($b_time,$d_time,$pick_date,$drop_date,$discount,$hfval,$total_amount,$base_amount,$param,$bus_details,$board_point,$drop_point);
        $t = date("Y-m-d");
        
        $data = [
            "sessionid"         => $param['sess_id'],
            "spname"            => $bus_details['sp']['first_name'].' '.$bus_details['sp']['last_name'],
            "sp_id"             => $bus_details['sp']['id'],
            "bus_id"            => $param['bus_id'],
            "user_id"           => $param['user_id'],
            "book_by"           => 0,
            "booking_system"    => $booking_system,
            "status"            => config("constant.STATUS_PAYMENT_PENDING"),
            "payment_by"        => 0,
            "journey_date"      => $param['date'],
            "from_term_id"      => $param['from_term_id'],
            "to_term_id"        => $param['to_term_id'],
            "from_term"         => $board_point['name'],
            "to_term"           => $drop_point['name'],
            "from_city"         => $board_point['loc_citi']['name'],
            "to_city"           => $drop_point['loc_citi']['name'],
            "from_district"     => $board_point['loc_district']['name'],
            "to_district"       => $drop_point['loc_district']['name'],
            "pickup_date"       => $pick_date,
            "drop_date"         => $drop_date,
            "nos"               => $param['total_seats'],
            "total_amount"      => $total_amount,
            "coupon_code"       => isset($param['coupon_code']) ? $param['coupon_code'] : '',
            "coupon_discount"   => $discount,
            "handle_fee"        => $hfval,
            "base_amount"       => $base_amount,
            "booker_name"       => $param['user_firstname'],
            "booker_email"      => $param['user_email'],
            "booker_dob"        => date("Y-m-d H:i:s", strtotime($t." -".$param['user_age']." year")),
            "booker_no"         => $param['user_mobileno'],
            "payment"           => '',    
            "device"           => 'android',    
        ];
        
//        dd($data);
        $booking = \App\Models\Bookings::save_booking_details($data);
        if(isset($booking['flag']) && $booking['flag'] == 0){
            \Log::info('save booking Response : '.json_encode($booking));
            return $booking;
        }
        $bank = \App\Models\BankDetails::get_banks_for_api();
        
        $res = \General::success_res();
        
        $res['data']['ticket_id'] = $booking['data']['booking_id'];
        $res['data']['pt_id'] = -1;
        $res['data']['transfer_code'] = $booking['data']['bt_id'];
        $res['data']['bank_detail'] = $bank;
        $res['data']['cc_url'] = \URL::to('api/services/ticket-checkout/'.\Mycrypt::encrypt($booking['data']['id']));
        $res['data']['payable_amount'] = $booking['data']['total_amount'] / 1000;
        $res['data']['payable_amount'] = \General::number_format($booking['data']['total_amount'],3);
        
        
        
//        dd($res);
//        $res = \App\Models\BookingInfo::book_ticket($param);
        \Log::info('Book Ticket Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postPayTicketByBank() {
        \Log::info('Pay Ticket By Bank Request : '. json_encode(\Input::all()));
//    public function getPayTicketByBank() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "pay_by_bank"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
//        dd($param);
//        $res = \App\Models\BookingInfo::pay_ticket_by_bank($param);
        $res = \App\Models\Bookings::pay_ticket_by_bank($param);
//        return $res;
        \Log::info('Pay Ticket By Bank Response : '. json_encode($res));
        return \Response::json($res, 200);
    }

    public function postPayTicketByCc() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "charge_cc"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res = \App\Models\PaymentTransaction::charge_cc($param);
        return \Response::json($res, 200);
    }

    public function postTicketDetail() {
        \Log::info('Ticket Detail Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_detail"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();

        $token = \Request::header('AuthToken');
        $res = \App\Models\Users::is_logged_in($token);
//        if ($res['flag'] != 1) {
//            $res = \App\Models\SeatSeller\User::is_logged_in($token);
//            if ($res['flag'] != 1) {
//                $res = \App\Models\Operator\User::is_logged_in($token);
//                if ($res['flag'] != 1) {
//                    return $res;
//                }
//            }
//        }

//        if(!isset($param['email']))
//        {
//            $token = \Request::header('AuthToken');
//            $res = \App\Models\Users::is_logged_in($token);
//            if($res['flag'] != 1)
//                return $res;
//            $user = app("logged_in_user");
////            dd($user);
//            $param['email'] = $user['email'];
//        }
        $res = \App\Models\Bookings::get_ticket_full_detail($param);
        \Log::info('Ticket Detail Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
//busway api
    
    public function postBswPayByCreditCard()
    {
        $validator= \Validator::make(\Input::all(),\Validation::get_rules("busway","busway_charge_cc"));
        if($validator->fails())
        {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res= \App\Models\BuswayOrder::pay_by_creditcard($param);
        if($res)
        {
           return \Response::json($res,200);
        }
        else
        {
            return \General::error_res();
        }
        
    }
    public function getBswPayByDokuWallet()
    {
        $validator= \Validator::make(\Input::all(),\Validation::get_rules("busway","busway_doku_wallet"));
        if($validator->fails())
        {
            $messages=$validator->messages();
            $error=$messages->all();
            $json=\General::Validation_error_res();
            $json['data']=$error;
            $json['msg']=$error[0];
            return \Response::json($json,200);
        }
        $param=\Input::all();
        
        $res=  \App\Models\BuswayOrder::dokuwallet($param);
        
        return view('busway.buswaydokuwallet',$res);
        
        
    }
    public function postBswPayByBank()
    {
        
        $validator=\Validator::make(\Input::all(),\Validation::get_rules("busway","pay_by_bank"));
        if($validator->fails())
        {
            $messages=$validator->messages();
            $error=$messages->all();
            $json=\General::Validation_error_res();
            $json['data']=$error;
            $json['msg']=$error[0];
            return \Response::json($json,200);
        } 
        $param=(\Input::all());
        
        
        $res= \App\Models\BuswayOrder::pay_by_bank($param);
        //return \General::success_res("Data Inserted Successfully..!");
        return \Response::json($res,200);
    }
    public function getBswBankDetail()
    {
        $bank_detail = \App\Models\BuswayOrder::getbankdetail();
        if($bank_detail)
        {
            return \Response::json($bank_detail,200);
        }
        else
        {
            return \General::error_res("No Detail Found For Bank");
        }
    }
    
    public function getBuswayListing()
    {
       
        $data= \App\Models\BuswayOrder::getBuswayList();
        if(!is_null($data))
        {
            return \Response::json($data,200);
        }
        else
        {
            return \General::error_res("Card Not Found...!");
        }
    }
    
    
    public function postBswSaveOrder()
    {
        $param=\Input::all();
        $validator=\Validator::make($param,\Validation::get_rules("busway","card_order"));
        if($validator->fails())
        {
            $messages=$validator->messages();
            $error=$messages->all();
            $json=\General::validation_error_res();
            //$json['data']=$error;
            $json['msg']=$error[0];
            return \Response::json($json,200);
        }    
        $data=\App\Models\BuswayOrder::save_busway_order($param);
        
        return \Response::json($data,200);      
    }
    
//busway api over    
    
    
    
    
    
    public function postTicketByUniqueId() {
        \Log::info('Ticket By Unique_ID Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_by_unique_id"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        if (!isset($param['email'])) {
            $token = \Request::header('AuthToken');
            $res = \App\Models\Users::is_logged_in($token);
            $user = app("logged_in_user");
            if (isset($user['email']))
                $param['email'] = $user['email'];
            else
                $param['email'] = "";
        }
        $ticket = \App\Models\BookingSeats::where("booking_id", $param['unique_id'])->first();
        if (!is_null($ticket)) { // this is just hack, user can check ticket by unique id only
            return \General::error_res("Ticket not found");
        }
        $param['ticket_id'] = $param['unique_id'];
        $res = \App\Models\Bookings::get_ticket_full_detail($param);
        \Log::info('Ticket By Unique_ID Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDashboardScreen() {
//        \Log::info('Dashboard Screen ..!');
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "dashboard"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res = \App\Models\Services\General::dashboard($param);
//        \Log::info('Dashboard Screen Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postTourTrip(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("trip","tour"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $param = \Input::all();
        //$param = array_merge($user, $param);
        $res = \General::success_res("");
        $res['data'] = \App\Models\Pariwisata::get_available_tour_trip_details($param);
        $res['bus_count'] = sizeof($res['data']);
        
        return \Response::json($res, 200);   
    }
    
    public function postBookPariwisataTrip(){
        
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("trip","tour_trip"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $token = \Request::header('AuthToken');
        
        $is_login = \App\Models\User\User::is_logged_in($token);
//        \General::dd($token);
//        \General::dd($is_login);
        $user = [];
        if ($is_login['flag']) {
            $user = app("logged_in_user");
        }
        else
        {
            return $is_login;
        }
//        \General::dd($user,1);
        $param = \Input::all();        
        $param['user_id'] = $user['user_id'];
        $order_res = \App\Models\PariwisataOrder::save_tour_order($param);
        
        if(isset($order_res['order_id'])){
            $param['order_id'] = $order_res['order_id'];
            $bus_order_res_flag = \App\Models\PariwisataOrderBus::save_tour_bus($param);
            
            if(!$bus_order_res_flag){
                return \General::error_res("error while saving order information");
            }
            return \General::success_res("Your request saved successfully");
        }
        else{
            return \General::error_res("error while saving order information");
        }
        
    }
    
     public function getPaymentSucces($booking_id) {
        $booking = \App\Models\Bookings::where("booking_id",$booking_id)->first();
        if (is_null($booking)) {
            $data = [
                'title' => "Invalid booking id",
                'msg' => "Invalid booking id",
            ];
            return \View::make("m.blank_error", $data);
        }
        $apr = \App\Models\Bookings::approve_ticket($booking_id);
        $data = [
            'title' => "E-Ticket payment success",
            'ticket_id' => $booking_id,
            'BT_id' => $booking->bt_id,
        ];
        return \View::make("m.e_ticket_success", $data);
    }
    
     public function getPaymentFail() {
        $s = \Session::get("msg");
        $s = $s == "" ? json_encode(["msg" => "Payment fail"]) : $s;
        $s = json_decode($s,true);
        $data = [
            'title' => $s['msg'],
            'msg' => $s['msg'],
        ];
        return \View::make("m.payment_fail", $data);
    }
    
     public function getPaymentCancel($booking_id = "") {
        $data = [
            'title' => "E-Ticket payment Cancel",
            'ticket_id' => $booking_id,
        ];
        return \View::make("m.e_ticket_cancel", $data);
    }
    
    
    public function getTicketCheckout($booking_id)
    {
        $booking_id = \Mycrypt::decrypt($booking_id);
        $booking = \App\Models\Bookings::find($booking_id);
        
        if(is_null($booking))
        {
            return view("m.blank_error");
        }
       
        $param = array(
            'transaction_details' => array(
                'order_id' => "tik_".$booking->booking_id,
                'gross_amount' => round($booking->total_amount)
            )
        );
        return \App\Lib\PaymentGateway\Veritrans::get_redirection_url($param);
    }
    
    
}
