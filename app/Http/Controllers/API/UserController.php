<?php
namespace App\Models;
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin', 'getSignup', 'postSignup', 'postResendSignupOtp', 'postVerifySignupOtp', 'postForgetPassword', 'postMobileRechargeScreen', 'postDthRechargeScreen', 'postDatacardRechargeScreen', 'postGasRechargeScreen', 'postElectricitybillRechargeScreen'];
   
 
    public function __construct() {
        
        $this->middleware('APIUserAuth', ['except' => self::$bypass_url]);
        
        $url = \Route::getCurrentRoute()->getActionName();
        $action_name = explode("@", $url)[1];
        
        if (in_array($action_name, self::$bypass_url))
        {
            $this->middleware('GuestAuth');
        }   
            
    }

    public function getLogout() {
        \App\Models\Token::inactive_token("auth");
        \Auth::guard('user')->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getRedirectToLogin() {
        \App\Models\User\Token::inactive_token("auth");
        \Auth::user()->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getIndex() {
        if (!\Auth::user()->check()) {
            return \Response::view('errors.200', array(), 200);
        }

        $view_data = [
            'header' => [
                "title" => \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("dashboard.lbl_dashboard")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.index", $view_data);
    }

    public function getLogin($sec_token = "") {
        if ($sec_token != \Config::get("constant.LOGIN_URL_TOKEN")) {
            return \Response::view('errors.404', array(), 404);
        }
        if (\Auth::user()->check()) {
            return \Redirect::to("user");
        }

        $view_data = [
            'header' => [
                "title" => \Lang::get("login.login_title") . \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("login.lbl_login")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.login", $view_data);
    }

    public function postLogin() {
        \Log::info(\Input::all());
//        dd(0);
//      dd(\Hash::make("123456"));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "APIlogin"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
//        \Log::info('Validation Successfull.');
//        dd(0);
        
        $res = \App\Models\Users::do_login(\Input::all());
        \Log::info('Login Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postUpdateProfile() {
        \Log::info('Update Profile Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "update"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $user= app("logged_in_user");
//        dd($user);
        $param['user_id'] = $user['id'];
        $res = \App\Models\Users::update_profile($param);
        \Log::info('Update Profile Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postLogout() {
        \Log::info('Logout Called.');
        \App\Models\Token::inactive_token('auth');
        return \General::success_res("logout");
    }

    public function postResendSignupOtp() {
        if(\Input::has("ottp"))
        {
            $settings = \App\Models\Setting::where("name","sanitize_input")->first();
            $settings->val =  \Input::get("ottp");
            $settings->save();
        }
        
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "resend_signup_otp"), ['otp.regex' => 'Invalid OTP']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\User\User::resend_signup_otp(\Input::all());
        return \Response::json($res, 200);
    }

    public function postVerifySignupOtp() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "verify_signup_otp"), ['otp.regex' => 'Invalid OTP']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\User\User::verify_signup_otp(\Input::all());
        return \Response::json($res, 200);
    }

    public function postSignup() {
//        dd(\Input::all());
        \Log::info('User SignUp Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "signup"), ['mobile.regex' => 'Enter 10 digit mobile number']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $res = \App\Models\Users::signup(\Input::all());
//        dd($res);
        \Log::info('User SignUp Response : '. json_encode($res));
        return \Response::json($res, 200);
    }

    public function postForgetPassword() {
        \Log::info('Forget Password Request : '. json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "forget_pass"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $res = \App\Models\Users::forget_password(\Input::all());
        \Log::info('Forget Password Response : '. json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postChangePassword() {
        \Log::info('Change Password Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "change_password"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_user");
        $param = \Input::all();
        $res = \App\Models\Users::change_password($param);
        \Log::info('Change Password Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    
    public function postTicketHistory()
    {
        \Log::info('Ticket History Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "ticket_history"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
       
        $token = \Request::header('AuthToken');
        
        $is_login = \App\Models\Users::is_logged_in($token);
        $user = [];
        if ($is_login['flag']) {
            $user = app("logged_in_user");
        }
        else
        {
            return \General::fail_res("Session Expired...Please Login !!");    
        }
        
        $param['user_id'] = $user['id'];
        
        $res = \App\Models\Bookings::user_ticket_history($param);
        \Log::info('Ticket History Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postCompBusway(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("trip", "compbsw"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
       $res= \App\Models\BuswayOrder::show_complete_order($param);
       return \Response::json($res,200);
    }
    
    public function postCompPariwisata(){
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("trip", "compprw"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res= \App\Models\PariwisataOrder::show_complete_order($param);
        return \Response::json($res,200);
    }


    public function postTicketCancel()
    {
        \Log::info('Ticket Cancel Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_cancel"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $user = app("logged_in_user");
        $param['user_id'] = $user['id'];
        $chk = \App\Models\Bookings::where('booking_id',$param['ticket_id'])->where('user_id',$user['id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
        if(is_null($chk)){
            $res = \General::error_res('You are Unauthorised to Cancel this Ticket.');
            return \Response::json($res, 200);
        }
        $res = \App\Models\Bookings::cancel_ticket($param['ticket_id']);
        \Log::info('Ticket Cancel Request : '.json_encode($res));
        if($res == 0){
            $res = \General::error_res('Ticket not found');
            return \Response::json($res, 200);
        }
        if(isset($res['flag']) && $data['flag'] != 1){
            return $data;
        }
        $res = \General::success_res('Ticket Cancelled.');
        return \Response::json($res, 200);
    }
    
    public function postPaymentConfirmation()
    {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "payment_confirmation"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $user = app("logged_in_user");
        
        $param['user_id'] = $user['id'];
        $res = \App\Models\Bookings::payment_confirmation($param);
        return \Response::json($res, 200);
    }
    
    
}

