<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

class OperatorController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin', 'postResendSignupOtp', 'postForgetPassword','getRequestDeposite'];

    public function __construct() {
        $this->middleware('OperatorAuth', ['except' => self::$bypass_url]);
        $url = \Route::getCurrentRoute()->getActionName();
        $action_name = explode("@", $url)[1];
        if (in_array($action_name, self::$bypass_url))
            $this->middleware('GuestAuth');
    }

    public function getLogout() {
        \App\Models\User\Token::inactive_token("auth");
        \Auth::user()->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getRedirectToLogin() {
        \App\Models\User\Token::inactive_token("auth");
        \Auth::user()->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getIndex() {
        if (!\Auth::user()->check()) {
            return \Response::view('errors.200', array(), 200);
        }

        $view_data = [
            'header' => [
                "title" => \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("dashboard.lbl_dashboard")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.index", $view_data);
    }

    public function getLogin($sec_token = "") {
        if ($sec_token != \Config::get("constant.LOGIN_URL_TOKEN")) {
            return \Response::view('errors.404', array(), 404);
        }
        if (\Auth::user()->check()) {
            return \Redirect::to("user");
        }

        $view_data = [
            'header' => [
                "title" => \Lang::get("login.login_title") . \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("login.lbl_login")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.login", $view_data);
    }

    public function postLogin() {
        \Log::info('Operator Login Request : '.json_encode(\Input::all()));
//        dd(\Hash::make("123456"));
        $custom_msg = ["SP_email.required" => "Email address missing", "SP_email.email" => "Invalid email address", "SP_password.required" => "Password missing"];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "login"), $custom_msg);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\Serviceprovider\ServiceProvider::do_login(\Input::all());
        if($res['flag'] == 0){
            return $res;
        }
        $avtar=\URL::to('assets/images/sp/default.png');
        if($res['data']['avatar'] != ''){
            $file = config('constant.UPLOAD_SP_LOGO_DIR_PATH').'/'.$res['data']['avatar'];
            if(file_exists($file)){
                $avtar=\URL::to('assets/images/sp/'.$res['data']['avatar']);
            }
            
        }
//        dd($res);
        $data = [
            'SP_id'             => $res['data']['id'],
            'SP_name'           => $res['data']['first_name'].' '.$res['data']['last_name'],
            'SP_email'          => $res['data']['email'],
            'SP_vat'            => $res['data']['sp_more']['vat'],
            'SP_address'        => $res['data']['sp_more']['address'],
            'SP_city'           => $res['data']['sp_more']['city'],
            'SP_state'          => $res['data']['sp_more']['state'],
            'SP_landNo'         => $res['data']['sp_more']['land_no'],
            'SP_mobile1'        => $res['data']['sp_more']['mobile1'],
            'SP_mobile2'        => $res['data']['sp_more']['mobile2'],
            'SP_fax'            => $res['data']['sp_more']['fax'],
            'comm_sp'           => $res['data']['comm_fm_sp'],
            'comm_sp_type'      => $res['data']['comm_fm_sp_type'],
            'SP_cpname'         => $res['data']['sp_more']['cp_name'],
            'SP_cpdesignation'  => $res['data']['sp_more']['cp_designation'],
            'SP_cpmobile'       => $res['data']['sp_more']['cp_mobile'],
            'SP_comments'       => $res['data']['sp_more']['comment'],
//            'tax'               => $res['data']['tax'],
            'SP_status'         => $res['data']['status'],
            'SP_verified'       => $res['data']['email_verified'],
            'sp_type'           => $res['data']['type'],
            'balance'           => $res['data']['balance'],
            'parent_id'         => $res['data']['parent_id'],
            'paypal_id'         => $res['data']['sp_more']['paypal_email'],
            'avatar'             => $avtar,
            'auth_token'        => $res['data']['auth_token'],
        ];
        
        $res['data'] = $data;
        
        \Log::info('Operator Login Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postUpdateProfile() {
        \Log::info('Operator Update Profile Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "update"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $res = \App\Models\Serviceprovider\ServiceProvider::update_profile(\Input::all());
        if($res['flag'] == 0){
            return $res;
        }
        
        $avtar=\URL::to('assets/images/sp/default.png');
        if($res['data']['avatar'] != ''){
            $file = config('constant.UPLOAD_SP_LOGO_DIR_PATH').'/'.$res['data']['avatar'];
            if(file_exists($file)){
                $avtar=\URL::to('assets/images/sp/'.$res['data']['avatar']);
            }
        }
        
        $data = [
            'SP_id'             => $res['data']['id'],
            'SP_name'           => $res['data']['first_name'].' '.$res['data']['last_name'],
            'SP_email'          => $res['data']['email'],
            'SP_vat'            => $res['data']['sp_more']['vat'],
            'SP_address'        => $res['data']['sp_more']['address'],
            'SP_city'           => $res['data']['sp_more']['city'],
            'SP_state'          => $res['data']['sp_more']['state'],
            'SP_landNo'         => $res['data']['sp_more']['land_no'],
            'SP_mobile1'        => $res['data']['sp_more']['mobile1'],
            'SP_mobile2'        => $res['data']['sp_more']['mobile2'],
            'SP_fax'            => $res['data']['sp_more']['fax'],
            'comm_sp'           => $res['data']['comm_fm_sp'],
            'comm_sp_type'      => $res['data']['comm_fm_sp_type'],
            'SP_cpname'         => $res['data']['sp_more']['cp_name'],
            'SP_cpdesignation'  => $res['data']['sp_more']['cp_designation'],
            'SP_cpmobile'       => $res['data']['sp_more']['cp_mobile'],
            'SP_comments'       => $res['data']['sp_more']['comment'],
            'tax'               => $res['data']['tax'],
            'SP_status'         => $res['data']['status'],
            'SP_verified'       => $res['data']['email_verified'],
            'sp_type'           => $res['data']['type'],
            'balance'           => $res['data']['balance'],
            'parent_id'         => $res['data']['parent_id'],
            'paypal_id'         => $res['data']['sp_more']['paypal_email'],
            'avatar'            => $avtar,
            'auth_token'        => \Request::header('AuthToken'),
        ];
        
        $res['data'] = $data;
        \Log::info('Operator Update Profile Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postLogout() {
        \App\Models\Serviceprovider\Token::inactive_token(config("constant.AUTH_TOKEN_STATUS"));
        return \General::success_res("logout");
    }

    public function postForgetPassword() {
        \Log::info('Operator Forget Password Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "forget_pass"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $res = \App\Models\Serviceprovider\ServiceProvider::forget_password(\Input::all());
        \Log::info('Operator Forget Password Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postChangePassword() {
        \Log::info('Operator Change Password Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "change_password"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res = \App\Models\Serviceprovider\ServiceProvider::change_password($param);
        \Log::info('Operator Change Password Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postSearchBus() {
        \Log::info('Operator Search Bus Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "search_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        //dd(\Input::all());
        $user = app("logged_in_operator");
        $param = \Input::all();
        $param = array_merge($user, $param);
        $param['len'] = isset($param['len']) ? $param['len'] : 10;
        $param['start'] = isset($param['start']) ? $param['start'] : 0;
        $res = \App\Models\Serviceprovider\ServiceProvider::search_bus($param);
        \Log::info('Operator Search Bus Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postBookTicket() {
        \Log::info('Operator Book ticket Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "book_ticket"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_operator");
        $param = \Input::all();
        $param = array_merge($user, $param);
        $res = \App\Models\Serviceprovider\ServiceProvider::book_ticket($param);
        \Log::info('Operator Book ticket Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDatewiseReport() {
        \Log::info('Operator Datewise Report Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "datewise_report"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_operator");
        $user['agent'] = 'sp';
        $agent = \App\Models\Serviceprovider\ServiceProvider::where("email",$user['email'])->first();
        $param = \Input::all();
        
//        if (!is_null($agent)) {
//            $user = $agent->toArray();
//            $param = array_merge($user, $param);
//            $res = \App\Models\SeatSeller\User::datewise_report($param);
//        } else {
            $param = array_merge($user, $param);
            $res = \App\Models\Serviceprovider\ServiceProvider::datewise_report($param);
//        }
        \Log::info('Operator Datewise Report Request : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDatewiseDetailReport() {
        \Log::info('Operator Datewise Detail Report Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "datewise_report"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_operator");
        $user['agent'] = 'sp';
        $agent = \App\Models\Serviceprovider\ServiceProvider::where("email", $user['email'])->first();
        $param = \Input::all();
//        if (!is_null($agent)) {
//            $user = $agent->toArray();
//            $param = array_merge($user, $param);
//            $res = \App\Models\SeatSeller\User::datewise_detail_report($param);
//        } else {
            $param = array_merge($user, $param);
            $res = \App\Models\Serviceprovider\ServiceProvider::datewise_detail_report($param);
//        }
        \Log::info('Operator Datewise Detail Report Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postTicketHistory() {
        \Log::info('Operator Ticket History Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "ticket_history"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $res = \App\Models\Bookings::operator_ticket_history($param);
        \Log::info('Operator Ticket History Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postTicketCancel() {
        \Log::info('Operator Ticket Cancel Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_cancel"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $operator = app("logged_in_operator");
        $param['agent_id'] = $operator['id'];

        $chk = \App\Models\Bookings::where('booking_id',$param['ticket_id'])->where('user_id',$operator['id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
        if(is_null($chk)){
            $res = \General::error_res('You are Unauthorised to Cancel this Ticket.');
            return \Response::json($res, 200);
        }
        $res = \App\Models\Bookings::cancel_ticket($param['ticket_id']);
        
        if($res == 0){
            $res = \General::error_res('Ticket not found');
            return \Response::json($res, 200);
        }
        if(isset($res['flag']) && $data['flag'] != 1){
            return $data;
        }
        
        $res = \General::success_res('Ticket Cancelled.');
        \Log::info('Operator Ticket Cancel Request : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postTicketCancelByUniqueId() {
        \Log::info('Operator Ticket Cancel by Unique ID Request : '.json_encode(\Input::all()));
//        dd(0);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_cancel"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $operator = app("logged_in_operator");
        $param['agent_id'] = $operator['id'];
        
        $tik = \App\Models\BookingSeats::where('ticket_id',$param['ticket_id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
//        dd($operator,$tik->toArray());
        $chk = \App\Models\Bookings::where('booking_id',$tik->booking_id)->where('user_id',$operator['id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
        if(is_null($chk)){
            $res = \General::error_res('You are Unauthorised to Cancel this Ticket.');
            return \Response::json($res, 200);
        }
//        dd($tik->toArray(),$chk->toArray());
        $res = \App\Models\BookingSeats::cancel_seat_by_ticketid($param['ticket_id']);
        
        if($res['flag'] != 1){
//            $res = \General::error_res('Ticket not found');
//            return \Response::json($res, 200);
            return $res;
        }
        
        $res = \General::success_res('Ticket Cancelled.');
        \Log::info('Operator Operator Ticket Cancel by Unique ID Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postHelp() {
        \Log::info('Operator Help Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("operator", "help"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $operator = app("logged_in_operator");
        $param['email'] = $operator['email'];
        $param['name'] = $operator['first_name'].' '.$operator['last_name'];
        if ($operator['type'] == config("constant.SERVICE_PROVIDER_MAIN"))
            $param['operator'] = "sp";
        else if ($operator['type'] == config("constant.SERVICE_PROVIDER_A")) {
            $param['operator'] = "spa";
        } else if ($operator['type'] == config("constant.SERVICE_PROVIDER_B")) {
            $param['operator'] = "spb";
        }
        $res = \App\Models\Serviceprovider\ServiceProvider::help($param);
        \Log::info('Operator Help Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postGetBalance(){
        $param = \Input::all();
        $token = \Request::header('AuthToken');
        $is_login = \App\Models\Serviceprovider\ServiceProvider::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_operator");
        }

        $operator_id = $logged_in_user['id'];
        
        $res = \App\Models\Serviceprovider\ServiceProvider::where('id',$operator_id)->first();
        if(!$res){
            return \General::error_res('no service provider found.');
        }
        if($res->type == 'Main'){
            $data = \General::info_res('main service provider has no limit for balance');
            $data['data'] = 0;
            return $data;
        }
        
        $ret=\General::success_res();
        $ret['data'] = $res->balance;
        return $ret;
    }
    
    public function postDepositeRequest(){
        $token = \Request::header('AuthToken');
        $is_login = \App\Models\Serviceprovider\ServiceProvider::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_operator");
        }

        $operator_id = $logged_in_user['id'];
        $param = \Input::all();
        $rules = [
            'amount'=>'required',
            'bank_id'=>'required',
        ];
        $validator = \Validator::make($param, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        $bank = \App\Models\DepositeBankDetails::where('id',$param['bank_id'])->first();
        if(!$bank){
            return \General::error_res('no bank found.');
        }
        
        $data = [
            'id'=>$operator_id,
            'amount'=>$param['amount'],
            'bank_id'=>$param['bank_id']
        ];
        $applyRequest = \App\Models\Serviceprovider\ServiceProviderDeposit::add_new_payment_request($data);
        $time_to_make_payment = config('constant.BANK_TIME_LIMIT');
        
        $bank_detail = \App\Models\DepositeBankDetails::get_banks_for_api();
        
        $res = \General::success_res('deposite request sent successfully.');
        $res['data']['ticket_id'] = '';
        $res['data']['pt_id'] = -1;
        $res['data']['transfer_code'] = $applyRequest['data']['id'];
        $res['data']['bank_detail'] = $bank_detail;
        $res['data']['cc_url'] = '';
        $res['data']['time_to_make_payment'] = $time_to_make_payment;
        $res['data']['payable_amount'] = $param['amount'];
        $res['data']['payable_amount'] = \General::number_format($param['amount'],3);
        if($bank->short_name == 'BANK'){
            
        }else{
            $res['data']['cc_url'] = \URL::to('api/operator/request-deposite/'.\Mycrypt::encrypt($applyRequest['data']['id']));
        }
        
        return $res;
    }
    public function getRequestDeposite($id){
        $id = \Mycrypt::decrypt($id);
//        dd($id);
    }
    public function postGetDetail(){
        $param = \Input::all();
        $res = \General::success_res();
        $res['screen'] = 'ticket_detail';
        if(isset($param['booking_id']) && $param['booking_id'] != ''){
            $param['ticket_id'] = $param['booking_id'];
            $booking = \App\Models\Bookings::get_ticket_full_detail($param);
            if($booking['flag'] != 1){
                return $booking;
            }
            if(count($booking['data']['passenger']) == 1){
                $res['data'] = $booking['data'];
                return $res;
            }else{
                $res['screen'] = 'ticket_history';
                $param['type'] = 'complete';
                $history = \App\Models\Bookings::operator_ticket_history($param);
                if($history['flag'] != 1){
                    return $history;
                }
                if(count($history['data']) == 0){
                    return \General::error_res('no data found');
                }
                $res['data'] = $history['data'];
                return $res;
            }
        }elseif((isset($param['name']) && $param['name'] != '') || (isset($param['mobile']) && $param['mobile'] != '')){
            $res['screen'] = 'ticket_history';
            $param['type'] = 'complete';
            $history = \App\Models\Bookings::operator_ticket_history($param);
            if($history['flag'] != 1){
                return $history;
            }
            if(count($history['data']) == 0){
                return \General::error_res('no data found');
            }
            $res['data'] = $history['data'];
            return $res;
        }
        
        return \General::error_res('please send vaild parameters.');
    }
}
