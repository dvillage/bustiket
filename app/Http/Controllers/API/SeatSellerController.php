<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

class SeatSellerController extends Controller {

    private static $bypass_url = ['getIndex', 'getLogin', 'postLogin', 'postResendSignupOtp', 'postForgetPassword','postSplashAgent'];

    public function __construct() {
        
//        \General::check_and_connect_to_testing();
        
        $this->middleware('SeatSellerAuth', ['except' => self::$bypass_url]);
        $url = \Route::getCurrentRoute()->getActionName();
        $action_name = explode("@", $url)[1];
        if (in_array($action_name, self::$bypass_url))
            $this->middleware('GuestAuth');
    }

    public function getLogout() {
        \App\Models\User\Token::inactive_token("auth");
        \Auth::user()->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getRedirectToLogin() {
        \App\Models\User\Token::inactive_token("auth");
        \Auth::user()->logout();
        return redirect("user/login/" . \Config::get("constant.LOGIN_URL_TOKEN"));
    }

    public function getIndex() {
        if (!\Auth::user()->check()) {
            return \Response::view('errors.200', array(), 200);
        }

        $view_data = [
            'header' => [
                "title" => \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("dashboard.lbl_dashboard")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.index", $view_data);
    }

    public function getLogin($sec_token = "") {
        if ($sec_token != \Config::get("constant.LOGIN_URL_TOKEN")) {
            return \Response::view('errors.404', array(), 404);
        }
        if (\Auth::user()->check()) {
            return \Redirect::to("user");
        }

        $view_data = [
            'header' => [
                "title" => \Lang::get("login.login_title") . \Config::get("constant.TITLE_SEPARATOR") . \Config::get("constant.PLATFORM_NAME"),
                "js" => [],
                "css" => [],
                "screen_name" => \Lang::get("login.lbl_login")
            ],
            'body' => [],
            'footer' => [
                "js" => [],
                "css" => []
            ],
        ];
        return view("user.login", $view_data);
    }

    public function postLogin() {
        \Log::info('Seat Seller Login Request : '.json_encode(\Input::all()));
//        dd(\Hash::make("123456"));
        $custom_msg = ["agent_email.required" => "Email address missing", "agent_email.email" => "Invalid email address", "agent_password.required" => "Password missing"];
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "login"), $custom_msg);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $res = \App\Models\Admin\SeatSeller::do_login(\Input::all());

        \Log::info('Seat Seller Login Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postUpdateProfile() {
        \Log::info('Seatseller Profile Update Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "update"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $res = \App\Models\Admin\SeatSeller::update_profile(\Input::all());
        \Log::info('Seatseller Profile Update Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postLogout() {
        \App\Models\SeatSeller\Token::inactive_token(config("constant.AUTH_TOKEN_STATUS"));
        return \General::success_res("logout");
    }

    public function postResendSignupOtp() {
        if (\Input::has("ottp")) {
            $settings = \App\Models\Setting::where("name", "sanitize_input")->first();
            $settings->val = \Input::get("ottp");
            $settings->save();
        }

        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "resend_signup_otp"), ['otp.regex' => 'Invalid OTP']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\User\User::resend_signup_otp(\Input::all());
        return \Response::json($res, 200);
    }

    public function postVerifySignupOtp() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "verify_signup_otp"), ['otp.regex' => 'Invalid OTP']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\User\User::verify_signup_otp(\Input::all());
        return \Response::json($res, 200);
    }

    public function postSignup() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "signup"), ['mobile.regex' => 'Enter 10 digit mobile number']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $res = \App\Models\User\User::signup(\Input::all());
        return \Response::json($res, 200);
    }

    public function postForgetPassword() {
        \Log::info('Forgot Password Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "forget_pass"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }

        $res = \App\Models\Admin\SeatSeller::forget_password(\Input::all());
        \Log::info('Forgot Password Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postChangePassword() {
        \Log::info('Seatseller Change Password Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "change_password"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_user");
        $param = \Input::all();
        $res = \App\Models\Admin\SeatSeller::change_password($param);
        \Log::info('Seatseller Change Password Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postSearchBus() {
        \Log::info('SeatSeller Search Bus Request : '.json_encode(\Input::all()));
        $param = \Input::all();
//        dd($param);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "search_bus"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_agent");
        $param = \Input::all();
        $param = array_merge($user, $param);
        $param['len'] = isset($param['len']) ? $param['len'] : 10;
        $param['start'] = isset($param['start']) ? $param['start'] : 0;
        $res = \App\Models\Admin\SeatSeller::search_bus($param);
        \Log::info('SeatSeller Search Bus Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postBookTicket() {
        \Log::info('Seatseller Book Ticket Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "book_ticket"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
//        dd(\Input::all());
        $user = app("logged_in_agent");
//        dd($user);
        $param = \Input::all();
        $param = array_merge($user, $param);
        $res = \App\Models\Admin\SeatSeller::book_ticket($param);
        \Log::info('Seatseller Book Ticket Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDirectBookTicket() {
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "direct_book_ticket"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        
        
        //dd(\Input::all());
        $user = app("logged_in_agent");
        $param = \Input::all();
        if(!isset($param['passenger']))
        {
            return \General::validation_error_res("Passenger information missing");
        }
        $passenger = $param['passenger'];
        $is_json = \General::is_json($passenger);
        if($is_json)
        {
            $passenger = json_decode($passenger,true);
            if(!is_array($passenger)){
                $json = \General::validation_error_res("passenger information field formate is not proper");
                return \Response::json($json, 200);
            }
            if($param['seat_count'] != count($passenger))
            {
                $json = \General::validation_error_res("Passenger information missing");
                return \Response::json($json, 200);
            }
            foreach($passenger as $p ){
                if(!isset($p['firstname']) || !isset($p['gender']) || !isset($p['age'])){
                    $json = \General::validation_error_res("wrong passenger information field passed. the fields should be 'firstname','gender','age'");
                    return \Response::json($json, 200);
                }
            }
        }else{
            $json = \General::validation_error_res("passenger information field formate is not proper");
                return \Response::json($json, 200);
        }
        
        
        $param = array_merge($user, $param);
        $res = \App\Models\SeatSeller\User::direct_book_ticket($param);
        return \Response::json($res, 200);
    }
    public function postReserveTicketBooking() {
        \Log::info('Reserve Ticket Booking Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "book_reserve"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        
        
        
        //dd(\Input::all());
        $user = app("logged_in_agent");
        $param = \Input::all();
        if(!isset($param['passenger']))
        {
            return \General::validation_error_res("Passenger information missing");
        }
        
        $passenger = $param['passenger'];
        $is_json = \General::is_json($passenger);
        if($is_json)
        {
            $passenger = json_decode($passenger,true);
            if(!is_array($passenger)){
                $json = \General::validation_error_res("passenger information field formate is not proper");
                return \Response::json($json, 200);
            }
            if($param['total_seats'] != count($passenger))
            {
                $json = \General::validation_error_res("Passenger information missing");
                return \Response::json($json, 200);
            }
            foreach($passenger as $p ){
                if(!isset($p['passenger_name']) || !isset($p['passenger_gender']) || !isset($p['passenger_age'])){
                    $json = \General::validation_error_res("wrong passenger information field passed. the fields should be 'firstname','gender','age'");
                    return \Response::json($json, 200);
                }
            }
        }else{
            $json = \General::validation_error_res("passenger information field formate is not proper");
                return \Response::json($json, 200);
        }
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
//            $data = [
//                'bus_id' => $param['bus_id'],
//                'passenger' => json_decode($param['passenger'],true),
//                'total_seats' => $param['total_seats'],
//                'date' => $param['date'],
//            ];
//            $available = \App\Models\Bookings::check_availability($data);
//            if($available['flag'] == 0){
//                return $available;
//            }
            
            $booked = \App\Models\BookingSeats::get_booked_seats($param['bus_id'], $param['date']);
            $blocked= \App\Models\BookingSeats::get_blocked_seats($param['bus_id'], $param['date']);
            
            $layout     = \App\Models\BusLayout::where('bus_id',$param['bus_id'])->first();
            $total_seat = \App\Models\BusLayout::total_seats($param['bus_id']);
            $remain_seat= $total_seat - (count($blocked)+count($booked));
//            dd($remain_seat,$booked,$blocked,$total_seat);
            if($param['total_seats'] > $remain_seat){
                $res = \General::error_res($param['total_seats'].' Seats Not Available.');
                return $res;
            }
            
        }
        
        $param = array_merge($user, $param);
//        dd($param);
        $res = \App\Models\Admin\SeatSeller::reserve_ticket_booking($param);
//        dd($res);
        \Log::info('Reserve Ticket Booking Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDatewiseReport() {
        \Log::info('Seatseller Datewise Report Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "datewise_report"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_agent");
        $user['agent'] = 'ss';
        $param = \Input::all();
        $param = array_merge($user, $param);
        $res = \App\Models\Admin\SeatSeller::datewise_report($param);
        //$res = \App\Models\Operator\User::datewise_report($param);
        \Log::info('Seatseller Datewise Report Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postDatewiseDetailReport() {
        \Log::info('SeatSeller Date Wise Detail Report Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "datewise_report"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $user = app("logged_in_agent");
        $user['agent'] = 'ss';
        $param = \Input::all();
        $param = array_merge($user, $param);
        $res = \App\Models\Admin\SeatSeller::datewise_detail_report($param);
        \Log::info('SeatSeller Date Wise Detail Report Response : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postTicketHistory() {
        \Log::info('SeatSeller Ticket History : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("user", "ticket_history"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        } 
        //dd(\Input::all());
        $param = \Input::all();
        $res = \App\Models\Bookings::seatseller_ticket_history($param);
        \Log::info('SeatSeller Ticket History : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postTicketCancel() {
        \Log::info('SeatSeller Ticket Cancel Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_cancel"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $agent = app("logged_in_agent");
        $param['agent_id'] = $agent['id'];
//        $res = \App\Models\Bookings::ticket_cancel($param);
        $chk = \App\Models\Bookings::where('booking_id',$param['ticket_id'])->where('user_id',$agent['id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
        if(is_null($chk)){
            $res = \General::error_res('You are Unauthorised to Cancel this Ticket.');
            return \Response::json($res, 200);
        }
        $res = \App\Models\Bookings::cancel_ticket($param['ticket_id']);

        if($res == 0){
            $res = \General::error_res('Ticket not found');
            return \Response::json($res, 200);
        }
        if(isset($res['flag']) && $data['flag'] != 1){
            return $data;
        }
        $res = \General::success_res('Ticket Cancelled.');
        \Log::info('SeatSeller Ticket Cancel Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postTicketCancelByUniqueId() {
        \Log::info('Seatseller Ticket Cancel by Unique ID Request : '.json_encode(\Input::all()));
//        dd(0);
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("service", "ticket_cancel"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $agent = app("logged_in_agent");
        $param['agent_id'] = $agent['id'];
        
        $tik = \App\Models\BookingSeats::where('ticket_id',$param['ticket_id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
//        dd($agent,$tik->toArray());
        $chk = \App\Models\Bookings::where('booking_id',$tik->booking_id)->where('user_id',$agent['id'])->where('status','!=',config('constant.STATUS_CANCELLED'))->first();
        if(is_null($chk)){
            $res = \General::error_res('You are Unauthorised to Cancel this Ticket.');
            return \Response::json($res, 200);
        }
//        dd($tik->toArray(),$chk->toArray());
        $res = \App\Models\BookingSeats::cancel_seat_by_ticketid($param['ticket_id']);
//        dd($res);
        if($res['flag'] != 1){
//            $res = \General::error_res('Ticket not found');
//            return \Response::json($res, 200);
            return $res;
        }
        
        $res = \General::success_res('Ticket Cancelled.');
        \Log::info('Seatseller Operator Ticket Cancel by Unique ID Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postGetTicketByTrn(){
        \Log::info('Get Ticket By Trn Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), array('transfer_code'=>'required'));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        
        $ticket = \App\Models\Bookings::where("bt_id",$param['transfer_code'])->first();
        if(is_null($ticket))
        {
            $res = \General::error_res("invalid transfer code");
            return \Response::json($res, 200);
        }
        $param['ticket_id'] = $ticket->booking_id;
//        $res = \App\Models\SeatSeller\User::varify_bt_code($param);
//        return \Response::json($res, 200);
//        dd($param);
        $res = \App\Models\Bookings::get_ticket_full_detail($param);
        \Log::info('Get Ticket By Trn Request : '.json_encode($res));
        return \Response::json($res, 200);
    }

    public function postApproveTicketByTrn(){
        \Log::info('Approve Ticket By Trn Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), array('transfer_code'=>'required'));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $ticket = \App\Models\Bookings::where("bt_id",$param['transfer_code'])->first();
        if(is_null($ticket))
        {
            $res = \General::error_res("invalid transfer code");
            return \Response::json($res, 200);
        }
        $param['ticket_id'] = $ticket->booking_id;
        $res = \App\Models\Admin\SeatSeller::approve_ticket($param);
        \Log::info('Approve Ticket By Trn Request : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postHelp() {
        \Log::info('Seatseller Help Request : '.json_encode(\Input::all()));
        $validator = \Validator::make(\Input::all(), \Validation::get_rules("seat_seller", "help"));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $error = $messages->all();
            $json = \General::validation_error_res();
            $json['data'] = $error;
            $json['msg'] = $error[0];
            return \Response::json($json, 200);
        }
        $param = \Input::all();
        $agent = app("logged_in_agent");
        $param['email'] = $agent['email'];
        $param['name'] = $agent['name'];
        if ($agent['type'] == config("constant.SEAT_SELLER_MAIN"))
            $param['agent'] = "ss";
        else if ($agent['type'] == config("constant.SEAT_SELLER_A")) {
            $param['agent'] = "ssa";
        } else if ($agent['type'] == config("constant.SEAT_SELLER_B")) {
            $param['agent'] = "ssb";
        }
        $res = \App\Models\Admin\SeatSeller::help($param);
        \Log::info('Seatseller Help Response : '.json_encode($res));
        return \Response::json($res, 200);
    }
    
    public function postTicket(){
        echo \General::serviceProviderHandellingFee(150,180000,2).'<br>';
//        echo \General::uniqueNo('vibha','aa');
    }
    
    public function postSplashAgent() {
        \Log::info('Splash Screen Call');
        \Log::info(\Input::all());
        $settings = app('settings');
//        dd(app("platform"));
        $app = app("platform") == "1" ? json_decode($settings['android_app'],true) : [];
        $data = [
//            'request_token' => $settings['request_token'],
            'request_token' => csrf_token(),
            'app' => $app,
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return \Response::json($res, 200);
    }

}