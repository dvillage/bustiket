<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Drivers extends Model {
    protected $fillable = [
        'name', 'mobile', 'email','address','id_proof','id_proof_number'
    ];
    protected $table = 'drivers';
    protected $hidden = [];
    public $timestamps=false;
    
    public function busDetails() {
        return $this->hasMany('App\Models\BusDrivers','driver_id','id');
    }
    
    public static function get_driver($param){
        $d = self::orderBy('name','asc');
        if(isset($param['name'])){
            $d = $d->where('name','like','%'.$param['name'].'%');
        }
        if(isset($param['id'])){
            $d = $d->where('id',$param['id']);
        }
        $d = $d->get();
        
        return $d;
    }
    
    public static function get_driver_filter($param){
        $count=self::OrderBY('id','desc');
        $count = $count->count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->orWhere('mobile',$param['search']);
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::orderBy('id','desc');
        $spdata = $spdata->with(['busDetails.buses.busRoutes'])->skip($start)->take($len)->get()->toArray();
        
//        print_r($spdata);
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::where('name','like','%'.$param['search'].'%')->orWhere('mobile',$param['search'])->orderBy('id','desc');
            $spdata = $spdata->with(['busDetails.buses.busRoutes'])->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function add_new_driver($param){
        $d = new self;
        $d->name = $param['name'];
        $d->email = $param['email'];
        $d->mobile = $param['mobile'];
        $d->id_proof = $param['proof_type'];
        $d->id_proof_number = $param['proof_number'];
        $d->address = $param['address'];
        $d->crew = $param['crew'];
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    public static function edit_driver($param){
        $d = self::where('id',$param['did'])->first();
        if(is_null($d)){
            return \General::error_res('no driver found');
        }
        $d->name = $param['name'];
        $d->email = $param['email'];
        $d->mobile = $param['mobile'];
        $d->id_proof = $param['proof_type'];
        $d->id_proof_number = $param['proof_number'];
        $d->address = $param['address'];
        $d->crew = $param['crew'];
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    
    public static function delete_driver($param){
        $dEx = BusDrivers::where('driver_id',$param['id'])->get()->toArray();
        if(count($dEx)>0){
//            return \General::error_res('you can not delete this driver. this is already in use!!');
        }
        self::where('id',$param['id'])->delete();
        return \General::success_res('driver deleted successfully !!');
    }
}
