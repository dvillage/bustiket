<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SeatMap extends Model {
    protected $fillable = [
        'layout_id', 'seat_lbl', 'object_type','length','height','row','col'
    ];
    protected $table = 'seat_map';
    protected $hidden = [];
    public $timestamps=false;
    
    public function layout(){
        return $this->hasOne('App\Models\BusLayout','id','layout_id');
    }
    
    
    public static function prepare_seatmap_array($data,$rows = 0,$cols = 0)
    {
        $final_data = [];
        $skip_cell = [];
        for($r=0;$r<$rows;$r++)
        {
            $final_data[$r] = [];
            $skip_this_cell = false;
            for($c=0;$c<$cols;$c++)
            {
                
                $skip_this_cell = false;
                    for($s=0;$s<count($skip_cell);$s++)
                    {
                        if($r == $skip_cell[$s]['row'] && $c == $skip_cell[$s]['col'])
                        {
                            $skip_this_cell = true;
                            break;
                        }
                    }
                    if($skip_this_cell)
                    {
                        array_splice($skip_cell, $s,1);
                        continue;
                    }
//                if($r==2)
//                {
//                    \General::dd("Row: ".$r." Col: ".$c);
//                }    
                $found = false;
                for($k=0;$k<count($data);$k++)
                {
                    if($data[$k]['row'] == $r && $data[$k]['col'] == $c)
                    {
                        $row_to_be_skip = false;
                        $column_to_be_skip = false;
                        if($data[$k]['length'] > 1 && $data[$k]['height'] > 1)
                        {
                            for($coln=0;$coln<$data[$k]['height'];$coln++)
                            {
                                $column_to_be_skip = false;
                                $row_to_be_skip = $row_to_be_skip !== false ? $row_to_be_skip+1 : $r;
                                for($rown=0;$rown<$data[$k]['length'];$rown++)
                                {
                                    $column_to_be_skip = $column_to_be_skip !== false ? $column_to_be_skip+1 : $c;
                                    $skip_cell[] = ['row' => $row_to_be_skip,'col' => $column_to_be_skip];
                                }
                                
                            }
                        }
                        else if($data[$k]['height'] > 1)
                        {
                            for($rown=0;$rown<$data[$k]['height'];$rown++)
                            {
                                $row_to_be_skip = $row_to_be_skip !== false ? $row_to_be_skip+1 : $r;
                                $skip_cell[] = ['row' => $row_to_be_skip,'col' => $c];
//                                dd($skip_cell); 
                            }
                        }
                        else if($data[$k]['length'] > 1)
                        {
                            for($rown=0;$rown<$data[$k]['length'];$rown++)
                            {
                                $column_to_be_skip = $column_to_be_skip !== false ? $column_to_be_skip+1 : $c;
                                $skip_cell[] = ['row' => $r,'col' => $column_to_be_skip];
                            }
                        }
                        
                        $final_data[$r][] = $data[$k];
                        
//                        array_splice($data, $k,1); // remove data from data array to reduce loop iteration
//                        $k--;
                        
                        $found = true;
                        break;
                    }
                }
                
//                if($r == 3 && $c == 2)
//                            dd($skip_cell);
                
                if(!$found)
                    $final_data[$r][] = null;
            }
        }
        
//        dd($final_data);
        return $final_data;
    }
    
    public static function seatmap($param){
        
        $l_id = 1;
        $bus_id =$param['bus_id'];
        $roda = \Session::get('roda_bus');
        $lorena = 0;
        if (stripos($bus_id,'_LORENA') !== false) {
            $lorena = 1;
            $roda = \Session::get('lorena_bus');
        }
//        dd($roda,$param);
        if(preg_match("/[a-z]/i", $bus_id)){
            
            if(isset($param['from_id']) && isset($param['to_id'])){
                if($lorena){
                    $rodaR = Lorena::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                }else{
                    $rodaR = Roda::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                }
                
            }elseif($roda){
                $roda = $roda;
            }else{
                return \General::error_res();
            }
            
            $rbus = Roda::get_roda_bus($roda, $bus_id);
            if(count($rbus) == 0){
                return \General::error_res('your session is expired.please try again.');
            }
//            dd($rbus);
            if($lorena){
                $Rseats = Lorena::all_seats($rbus);
            }else{
                $Rseats = Roda::all_seats($rbus);
            }
            
//            dd($Rseats);
            $view_data = [];
            $view_data['bus_id']= $bus_id;
            $booked = $Rseats['booked_seats'];
//            $blocked = $Rseats['blocked_seats'];
            $blocked = \App\Models\BookingSeats::get_blocked_seats($bus_id, $param['date']);

            $session_id = session('session_id');
            $alreadyBooked = \App\Models\BookingSeats::where('session_id',$session_id)->where('status',0)->where('booking_id',null)->get();
            $blkd = [];
            if(count($alreadyBooked) > 0){
                foreach($alreadyBooked as $ab){
                    $blkd[] = $ab->seat_lbl;
                    if(($key = array_search($ab->seat_lbl, $blocked)) !== false) {
                        unset($blocked[$key]);
                    }

                }
            }
    //        dd($blkd,$blocked);
            $total_seat = $rbus['total_seat'];
            $remain_seat = $total_seat - (count($blocked)+count($booked));
            $view_data['nop']= $param['nop'];
            $view_data['remain_seat']= $remain_seat;
            $view_data['block_count']= count($blkd);
            
            $view_data['rows'] = $Rseats['rows'];
            $view_data['cols'] = $Rseats['cols'];
            $view_data['seats'] = $Rseats['seats'];
            $view_data['booked'] = $booked;
            $view_data['blocked'] = $blocked;
            $view_data['already_blocked'] = $blkd;
            return $view_data;
        }
        
//        $device = app('device');
//        $layout = \App\Models\BusLayout::find($l_id)->toArray();
        $layout = \App\Models\BusLayout::where('bus_id',$bus_id)->first();
        $view_data = [];
        $view_data['bus_id']= $bus_id;
        $booked = \App\Models\BookingSeats::get_booked_seats($bus_id, $param['date']);
        $blocked = \App\Models\BookingSeats::get_blocked_seats($bus_id, $param['date']);
        
        $session_id = session('session_id');
        $alreadyBooked = \App\Models\BookingSeats::where('session_id',$session_id)->where('status',0)->where('booking_id',null)->get();
        $blkd = [];
        if(count($alreadyBooked) > 0){
            foreach($alreadyBooked as $ab){
                $blkd[] = $ab->seat_lbl;
                if(($key = array_search($ab->seat_lbl, $blocked)) !== false) {
                    unset($blocked[$key]);
                }

            }
        }
//        dd($blkd,$blocked);
        $total_seat = \App\Models\BusLayout::total_seats($bus_id);
        $remain_seat = $total_seat - (count($blocked)+count($booked));
        $view_data['nop']= $param['nop'];
        $view_data['remain_seat']= $remain_seat;
        $view_data['block_count']= count($blkd);
        if($layout){
            $layout = $layout->toArray();
            $data = self::where("layout_id", $layout['id'])->orderBy("row", "ASC")->orderBy("col", "ASC")->get()->toArray();
            $view_data['rows'] = $layout['rows'];
            $view_data['cols'] = $layout['columns'];
            $view_data['seats'] = $data;
            $view_data['booked'] = $booked;
            $view_data['blocked'] = $blocked;
            $view_data['already_blocked'] = $blkd;
        }
        
        return $view_data;
    }

}
