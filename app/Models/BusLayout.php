<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BusLayout extends Model {
    protected $fillable = [
        'name', 'short_name', 'bus_id','columns','rows'
    ];
    protected $table = 'bus_layouts';
    protected $hidden = [];
    public $timestamps=false;
    
    public function seatmap(){
        return $this->hasMany('App\Models\SeatMap','layout_id','id');
    }
    public function seats(){
        return $this->seatmap()->where('object_type','CS')->orWhere('object_type','SS') ;
    }
    
    public static function total_seats($bus_id){
        $bus = Serviceprovider\Buses::where('id',$bus_id)->first();
        $layout = $bus->layout;
        
        if($layout){
            
            $seats = self::where('id',$bus->layout_id)->with('seats')->first();
            $total = count($seats['seats']);

            
            return $total;
        }
        
        return $bus->seat_capacity;
    }
}
