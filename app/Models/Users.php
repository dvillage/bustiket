<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Users extends Model implements Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use AuthenticableTrait;
    
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthIdentifierName() {
        return $this->getKeyName();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
        return $this->{$this->getRememberTokenName()};
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public function setRememberToken($value) {
        $this->{$this->getRememberTokenName()} = $value;
    }

//    use Authenticatable, CanResetPassword;
    protected $fillable = [
        'name', 'email', 'password','email_verified', 'status', 'mobile','mobile_verified', 'gender', 'wb','dob', 'avatar', 'last_ip','last_ua', 'login_date', 'device_type','device_token', 'remember_token','promo_offer'
    ];
    
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static function doLogin($param){
        if(isset($param['remember']))
        {
            \Cookie::get("remember",1);
            if($param['remember']=='on')
                $param['remember']=1;
            else
                $param['remember']=0;
//            \App\Models\Admin\Settings::set_config(['sanitize_input' => $param['remember']]);
        }
        $user = self::where("email", $param['u_email'])->first();
        $res['data']=$user;
        $res['flag']=0;
        if (is_null($user)) {
            $res['flag']=0;
            return $res;
        }
        if (!\Hash::check($param['u_pass'], $user->password)) {
            $res['flag']=0;
            return $res;
        }
        if(isset($param['remember']) && $param['remember']==1)
        {
            $auth_token = \App\Models\Token::generate_auth_token();
            
//            $token_data = ['user_id' => $user->id,'token' => $auth_token,'type' => 'auth'];
            $token_data = ['user_id' => $user->id,'token' => $auth_token,'type' => \Config::get("constant.AUTH_TOKEN_STATUS")];
            \App\Models\Token::save_token($token_data);
            \Auth::guard("user")->loginUsingId($user->id,true);
        }
        else{
            $auth_token = \App\Models\Token::generate_auth_token();
            
//            $token_data = ['user_id' => $user->id,'token' => $auth_token,'type' => 'auth'];
            $token_data = ['user_id' => $user->id,'token' => $auth_token,'type' => \Config::get("constant.AUTH_TOKEN_STATUS")];
            \App\Models\Token::save_token($token_data);
            \Auth::guard("user")->loginUsingId($user->id);
        }
        \Auth::guard('ss')->logout();
        \Auth::guard('admin')->logout();
        \Auth::guard('sp')->logout();
        $res['flag']=1;
        return $res;
    }
    
    public static function do_login($param) {
        if (($param['type'] == "facebook" || $param['type'] == "google") && (!isset($param['access_token']) || $param['access_token'] == "")) {
            return \General::error_res("access_token_missing");
        }
        if ($param['type'] == "facebook") {
            $res = \App\Models\Services\General::check_facebook_access_token($param['access_token']);
            if ($res['flag'] != 1)
                return $res;
            $param['user_email'] = $res['data']['email'];
        } 
        elseif ($param['type'] == "google") {
            $res = \App\Models\Services\General::check_google_access_token($param['access_token']);
            if ($res['flag'] != 1)
                return $res;
            $param['user_email'] = $res['data']['email'];
        }
        $user = self::where("email", $param['user_email'])->first();
        if (is_null($user)) {
            $data = ['user_email' => $param['user_email'], "user_password" => \General::rand_str(5), "user_firstname" => isset($param['user_firstname']) ? $param['user_firstname'] : "","device_token" => $param['device_token']];
            if ($param['type'] == "facebook") {
                $data['fb_status'] = 1;
            } else if ($param['type'] == "google") {
                $data['g_status'] = 1;
            }
            self::signup($data);
            $user = self::where("email", $param['user_email'])->first();
        }
        
        if ($param['type'] == "normal" && !\Hash::check($param['user_password'], $user->password) ) {
            return \General::error_res("invalid_email_password");
        }
        if (($param['type'] == "normal") && $user->status == \Config::get("constant.USER_INACTIVE_STATUS")) {
            return \General::error_res("email_not_verified");
        }
        if ($user->status == \Config::get("constant.USER_SUSPEND_STATUS")) {
            return \General::error_res("account_suspended");
        }
        $user->device_token = $param['device_token'];
        $user->device_type = app("platform");
        $user->save();
        
//        $dead_token_id = \App\Models\Token::find_dead_token_id('auth', $user->id);
        $dead_token_id = \App\Models\Token::find_dead_token_id(\Config::get("constant.AUTH_TOKEN_STATUS"), $user->id);
        $platform = app("platform");
        $token = \App\Models\Token::generate_auth_token();
        if ($token == "")
            return \General::error_res("try_again");

        $data = ["type" => \Config::get("constant.AUTH_TOKEN_STATUS"), "platform" => $platform, "user_id" => $user->id, "token" => $token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
//        $data = ["type" => 'auth', "platform" => $platform, "user_id" => $user->id, "token" => $token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];

        if ($dead_token_id) {
            $data['id'] = $dead_token_id;
        }
        \App\Models\Token::save_token($data);
        $user_data = $user->toArray();
        $user_data['avatar'] = self::get_image_url($user['id'],$user['avatar']);
        $user_data['auth_token'] = $token;
        $user_data['mobile'] = \App\Models\Services\General::formate_mobile_no($user_data['mobile']);
        unset($user_data['password']);
        if(!\Request::wantsJson())
        {
            \Auth::guard("user")->loginUsingId($user['id']);
        }
        $res = \General::success_res();
        $res['data'] = $user_data;
        return $res;
    }
    
    public static function get_image_url($id, $file_name = "") {
        $default_path = \URL::to("assets/uploads/user/default.png");
        $file_path = 'assets/uploads/user/'. $file_name;
        $file_path1 = 'assets\uploads\user\\'. $file_name;
        if ($file_name != '' && file_exists(public_path().'\\' . $file_path1))
        {
            $file_url = asset($file_path);
        }
        else
        {
            $file_url = asset($default_path);
        }
        return $file_url;
    }

    
    public static function get_user_list($param){
        
//        dd($param);
        $count=self::orderBy('id','desc');
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->orWhere('mobile','like','%'.$param['search'].'%');
        }
        if(isset($param['status']) && $param['status']!=''){
            $count = $count->where('status',$param['status']);
        }
        $count = $count->count();
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
//        dd(config('constant.SP_TYPE'));
        $udata=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $udata=self::where('name','like','%'.$param['search'].'%')->orWhere('mobile','like','%'.$param['search'].'%');
        }
        if(isset($param['status']) && $param['status']!=''){
            $udata = $udata->where('status',$param['status']);
        }
        $udata = $udata->skip($start)->take($len)->get()->toArray();
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$udata;
        $res['flag']=$flag;
        return $res;
    }
    public static function edit_user($param){
        if(isset($param['id'])){
            $u = self::where('id',$param['id'])->first();
            if(is_null($u)){
                return \General::error_res('no user found');
            }
            if(isset($param['status'])){
                $u->status = $param['status'];
            }
            $u->save();
            $res = \General::success_res('user edited successfully !!');
            $res['data'] = $u;
            return $res;
        }else{
            return \General::error_res('no user found');
        }
    }
    public static function delete_user($param){
        if(isset($param['id'])){
            $u = self::where('id',$param['id'])->first();
            if(is_null($u)){
                return \General::error_res('no user found');
            }
            $u = self::where('id',$param['id'])->delete();
            $res = \General::success_res('user deleted successfully !!');
            return $res;
        }else{
            return \General::error_res('no user found');
        }
    }
    
    public static function signup($param) {
//        echo 'In Model';
//        dd($param);
        $user = new Users();
        $user->email = $param['user_email'];
        $user->email_verified = 0;
        $user->password = \Hash::make($param['user_password']);
        $user->status = 1;
        $user->wb = 0;
        $user->gender = 'M';
        $user->login_date = date("Y-m-d");
        $user->last_ip = \Request::getClientIp();
        $user->last_ua = \Request::server("HTTP_USER_AGENT");
        $user->device_token = $param['device_token'];
        $user->device_type = app("platform");
        if (isset($param['fb_status'])) {
            $user->fb_status = 1;
        }
        if (isset($param['g_status'])) {
            $user->g_status = 1;
        }
        if (isset($param['user_firstname'])) {
            $user->name = $param['user_firstname'];
        }
        if(isset($param['user_mobileno']))
            $user->mobile = $param['user_mobileno'];
            $user->mobile_verified = 0;
        if(isset($param['user_dob']))
            $user->dob = $param['user_dob'];
        $user->save();

//        \Auth::user()->loginUsingId($user->user_id);
        if(!\Request::wantsJson())
        {
            \Auth::guard("user")->loginUsingId($user->id);
        }
        $activation_token = \App\Models\Token::generate_activation_token();
        $user['activation_token'] = $activation_token;
        $data = ['status' => 1, 'type' => \Config::get("constant.ACCOUNT_ACTIVATION_TOKEN_STATUS"), 'platform' => app("platform"), 'user_id' => $user->id, 'token' => $activation_token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
        $user_obj = $user;
        $user = $user->toArray();
        \App\Models\Token::save_token($data);
//        $res = \Event::fire(new \App\Events\NewUserSignup($user));
        $user['auth_token'] = \App\Models\Token::generate_auth_token();
//        $data = ['status' => 1, 'type' => 'auth', 'platform' => app("platform"), 'user_id' => $user['id'], 'token' => $user['auth_token'], "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
        $data = ['status' => 1, 'type' => \Config::get("constant.AUTH_TOKEN_STATUS"), 'platform' => app("platform"), 'user_id' => $user['id'], 'token' => $user['auth_token'], "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
        \App\Models\Token::save_token($data);
        
        
        unset($user['activation_token']);
        unset($user['user_password']);
        $res = \General::success_res("signup");
        $res['data'] = $user;
        return $res;
    }
    
    public static function is_logged_in($token) {
        if (\Request::wantsJson()) {
            if ($token == "") {
                return \General::session_expire_res();
            }
//            $already_login = \App\Models\Token::is_active('auth', $token);
            $already_login = \App\Models\Token::is_active(\Config::get("constant.AUTH_TOKEN_STATUS"), $token);
            
            if (!$already_login)
                return \General::session_expire_res("unauthorise");
            else {
                $user = \App\Models\Users::where("id", $already_login)->first()->toArray();
                unset($user['password']);
                $user['auth_token'] = $token;
                app()->instance('logged_in_user', $user);
            }
        } else {
            if (!\Auth::guard('user')->check()) {
                \Auth::guard('user')->logout();
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return \General::session_expire_res("unauthorise");
            } else {
                $user_data = \Auth::guard('user')->user();
                $user = [
                    'id'        => $user_data->id,
                    'email'     => $user_data->email,
                    'name'      => $user_data->name,
                    'gender'    => $user_data->gender,
                    'dob'       => $user_data->dob,
//                    'user_landno' => $user_data->user_landno,
                    'mobile'    => $user_data->mobile,
//                    'user_maritalstatus' => $user_data->user_maritalstatus,
//                    'user_occupation' => $user_data->user_occupation,
//                    'user_address1_1' => $user_data->user_address1_1,
//                    'user_address1_2' => $user_data->user_address1_2,
//                    'user_address1_city' => $user_data->user_address1_city,
//                    'user_address1_state' => $user_data->user_address1_state,
//                    'user_address1_pin' => $user_data->user_address1_pin,
//                    'user_address1_country' => $user_data->user_address1_country,
//                    'user_address2_1' => $user_data->user_address2_1,
//                    'user_address2_2' => $user_data->user_address2_2,
//                    'user_address2_city' => $user_data->user_address2_city,
//                    'user_address2_state' => $user_data->user_address2_state,
//                    'user_address2_country' => $user_data->user_address2_country,
//                    'user_address2_pin' => $user_data->user_address2_pin,
//                    'user_typeID' => $user_data->user_typeID,
                    'last_ip'   => $user_data->last_ip,
                    'date'      => $user_data->date,
                    'status'    => $user_data->status,
                    'fb_status' => $user_data->fb_status,
                    'g_status'  => $user_data->g_status,
                    'updated_at'=> $user_data->updated_at,
                ];

                $ua = \Request::server("HTTP_USER_AGENT");
                $ip = \Request::server("REMOTE_ADDR");

//                $session = \App\Models\Token::active()->where("type", 'auth')->where("ua", $ua)->where("ip", $ip)->where("user_id", $user['id'])->first();
                $session = \App\Models\Token::active()->where("type", \Config::get("constant.AUTH_TOKEN_STATUS"))->where("ua", $ua)->where("ip", $ip)->where("user_id", $user['id'])->first();
                if (is_null($session)) {
                    \Auth::guard('user')->logout();
                    $user['auth_token'] = "";
                } else {
                    $user['auth_token'] = $session['token'];
                }
                app()->instance('logged_in_user', $user);
            }
        }
        return \General::success_res();
    }
    
    public static function update_profile($param) {
        $id = $param['user_id'];
        $user = self::where("id", $id)->first();
        if (is_null($user)) {
            return \General::error_res("invalid_user");
        }
//        dd($user);
        if (\Input::hasFile('avatar') || (isset($param['avatar']) && $param['avatar'] != "")) {
            $old_avatar = $user->avatar;
            $old_avatar = substr($old_avatar, strrpos($old_avatar, "/") + 1);
            $dir_path = \Config::get('constant.USER_AVATAR_PATH');
            if (!file_exists($dir_path)) {
                mkdir($dir_path, 0777, true);
            }
            if (\Input::hasFile('avatar')) {
                $ext = \Input::file('avatar')->getClientOriginalExtension();
                if(!in_array(strtolower($ext), ["jpg","jpeg","png"]))
                {
                    return \General::error_res("File must be image");
                }
                $fileName = time() . "." . $ext;
                \Input::file('avatar')->move(\Config::get('constant.USER_AVATAR_PATH'), $fileName);
            } else if (isset($param['avatar'])) {
                $fileName = time() . ".png";
                $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $param['avatar']));
                file_put_contents($dir_path . '/' . $fileName, $data);
            }
            $user->avatar = $fileName;
            $param['avatar'] = $fileName;
        } else {
            unset($param['avatar']);
        }
//        dd($param);
        if (isset($param['user_password']))
            $user->password = \Hash::make($param['user_password']);

        if (isset($param['user_firstname']))
            $user->name = $param['user_firstname'];

//        if (isset($param['user_lastname']))
//            $user->user_lastname = trim($param['user_lastname']);

        if (isset($param['user_gender']))
            $user->gender = trim($param['user_gender']);

        if (isset($param['user_dob']))
            $user->dob = $param['user_dob'];

//        if (isset($param['user_landno']))
//            $user->user_landno = $param['user_landno'];

        if (isset($param['user_mobileno']))
        {
            $param['user_mobileno'] = \App\Models\Services\General::formate_mobile_no($param['user_mobileno']);
            $user->mobile = $param['user_mobileno'];
        }

        $user->last_ip = \Request::getClientIp();
//        $user->date = date("Y-m-d");

        $user->save();
        $res = \General::success_res("user_profile_updated");
        $user_data = $user->toArray();
        $user_data['avatar'] = self::get_image_url($user_data['id'],$user_data['avatar']);
        unset($user_data['password']);
        $res['data'] = $user_data;
        return $res;
    }
    
    public static function change_password($param) {
        $logged_in_user = app("logged_in_user");
        $id = $logged_in_user['id'];
        $user = self::where("id", $id)->first();
        if (is_null($user)) {
            return \General::error_res("user_not_found");
        }
        if ($user->status == config("constant.USER_SUSPEND_STATUS")) {
            return \General::error_res("account_suspended");
        }
        if(!\Hash::check($param['old_password'], $user->password)){
            return \General::error_res("invalid_old_password");
        }
        \Log::info('Old Password : '.$user->password);
        $user->password = \Hash::make($param['new_password']);
        $user->save();
        \Log::info('New Password : '.$user->password);
        return \General::success_res("password_changed");
    }
    
    public static function forget_password($param) {
        $user = self::where("email", $param['user_email'])->first();
        if (is_null($user)) {
            return \General::error_res("email_not_found");
        }
        if ($user->status == config("constant.USER_SUSPEND_STATUS")) {
            return \General::error_res("account_suspended");
        }
        $platform = app("platform");
        $user_detail = $user->toArray();
        
        $forgotpass_token = \App\Models\Token::generate_forgotpass_token();
        $user_detail['forgotpass_token'] = $forgotpass_token;
        $data = ['status' => 1, 'type' => \Config::get("constant.FORGETPASS_TOKEN_STATUS"), 'platform' => app("platform"), 'user_id' => $user->id, 'token' => $forgotpass_token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
        
        $token = \App\Models\Token::save_token($data);
//        dd($token);
        $user_detail['mail_subject'] = \Lang::get("success.forgetpassword_mail_subject");
//        dd($user_detail);
//        echo \View::make("emails.user.forget_password",$user_detail)->render();
//        exit;

        \Mail::send('emails.user.forget_password', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject(\Lang::get("success.forgetpassword_mail_subject"));
        });

        return \General::success_res("forgot_password_mail_sent");
    }
    
     public static function getUserListForBulkSms($param){
//         dd($param);
        $count = self::orderBy('id','desc');
        if(isset($param['search']) && $param['search'] != ''){
//            $count=self::where('name','like','%'.$param['search'].'%');
            $count=self::where('name','like','%'.$param['search'].'%')
                            ->orWhere(function($q2) use ($param){
                                $q2->where('city','like','%'.$param['search'].'%')
                                    ->orWhere(function($q3) use ($param){
                                        $q3->where('state','like','%'.$param['search'].'%')
                                                ->orWhere(function($q4) use ($param){
                                                    $q4->where('mobile','like','%'.$param['search'].'%')
                                                        ->orWhere(function($q5) use ($param){
                                                            $q5->where('email','like','%'.$param['search'].'%')
                                                                ->orWhere(function($q6) use ($param){
                                                                    $q6->where('zipcode','like','%'.$param['search'].'%');
                                                                });
                                                        });
                                                });
                                    });
                            });
        }
        
        if(isset($param['status']) && $param['status'] != ''){
            $count = $count->where('status',$param['status']);
        }
        
        if(isset($param['gender']) && $param['gender'] != ''){

            $count = $count->where('gender',$param['gender']);
        }
        
        $count      = $count->count();
        $page       = $param['crnt'];
        $len        = $param['len'];
        $op         = $param['opr'];
        $total_page = ceil($count/$len);
        $flag       = 1;
        
        $start;
        
        if($op != ''){
            if($op == 'first'){
                $crnt_page  = 1;
                $start      = ($crnt_page-1)*$len;
            }
            
            elseif($op == 'prev'){
                $crnt_page = $page - 1;
                if($crnt_page <= 0){
                    $crnt_page = 1;
                }
                $start = ($crnt_page - 1) * $len;
            }

            elseif($op == 'next'){
                $crnt_page = $page + 1;
                if($crnt_page >= $total_page){
                    $crnt_page = $total_page;
                }
                $start = ($crnt_page - 1) * $len;
            }

            else{
                $crnt_page = $total_page;
                $start = ($crnt_page - 1) * $len;
            }
        }

        else{
            if($page > $total_page){
                $crnt_page = $page - 1;
                $start = ($crnt_page - 1 ) * $len;
            }
            else{
                $crnt_page = $page;
                $start = ($crnt_page - 1) * $len;
                
            }
        }
            
        
        $udata=self::orderBy('id','desc');
       
        if(isset($param['search']) && $param['search'] != ''){
//            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
//            $udata=self::where('name','like','%'.$param['search'].'%');
            $udata = $udata->where('name','like','%'.$param['search'].'%')
                            ->orWhere(function($q2) use ($param){
                                $q2->where('city','like','%'.$param['search'].'%')
                                    ->orWhere(function($q3) use ($param){
                                        $q3->where('state','like','%'.$param['search'].'%')
                                                ->orWhere(function($q4) use ($param){
                                                    $q4->where('mobile','like','%'.$param['search'].'%')
                                                        ->orWhere(function($q5) use ($param){
                                                            $q5->where('email','like','%'.$param['search'].'%')
                                                                ->orWhere(function($q6) use ($param){
                                                                    $q6->where('zipcode','like','%'.$param['search'].'%');
                                                                });
                                                        });
                                                });
                                    });
                            });
        }
       
        if(isset($param['status']) && $param['status'] != ''){
            $udata = $udata->where('status',$param['status']);
        }
       
        if(isset($param['gender']) && $param['gender'] != ''){
               
            $udata = $udata->where('gender',$param['gender']);
        }
        
        $udata = $udata->skip($start)->take($len)->get()->toArray();
       
        $res['len']         = $len;
        $res['crnt_page']   = $crnt_page;
        $res['total_page']  = $total_page;
        
        $res['result']  = $udata;
        $res['flag']    = $flag;
       
        return $res;
    }
}
