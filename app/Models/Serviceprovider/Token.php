<?php 

namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Token extends Eloquent {
    
    public $table = 'sp_token';
    protected $hidden = [];
    protected $fillable = array('type','sp_id','token', 'ip', 'ua','status');
    
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function scopeDeactive($query) {
        return $query->where('status', '=', 0);
    }
    
    
    public function Sp() {
        
        return $this->belongsTo('App\Models\Serviceprovider\ServiceProvider', "id", "sp_id");
    }
    
    
//    public static function inactive_token($type)
//    {
//        $token = self::active()
//                ->where("type","=",$type)
//                ->where("ua","=",\Request::server("HTTP_USER_AGENT"))
//                ->where("ip","=",\Request::getClientIp())
//                ->get()->first();
//        if(!is_null($token))
//        {
//            $token->status = 0;
//            $token->save();
//        }
//    }
    
//    public static function generate_auth_token()
//    {
//        static $call_cnt = 0;
//        if($call_cnt > 10)
//            return "";
//        ++$call_cnt;
//        $token = \General::rand_str(15);
//        $user = self::active()->where("type",'=',"auth")->where("token",'=',$token)->first();
//        if(isset($user->token))
//        {
//            return self::generate_auth_token();
//        }
//        return $token;
//    }
    
//    public static function save_token($param)
//    {
//        
//        $token = new Token();
//        $token->fill($param);
//        $token->ip = \Request::getClientIp();
//        $token->ua = \Request::server("HTTP_USER_AGENT");
//        $token->status = isset($param['status']) ? $param['status'] : 1;
//        $id = $token->save();
//        return \General::success_res();
//    }
    public static function delete_token()
    {
        $token = self::where('sp_id', \Auth::guard('sp')->user()->id)->delete();
        return \General::success_res();
    }
    
//    public static function is_active($type,$token)
//    {
//        $user = self::active()->where("type",'=',$type)->where("token",'=',$token)->first();
//        if(isset($user->token))
//        {
//            return TRUE;
//        }
//        return FALSE;
//    }
    
    public static function get_active_token($token_type)
    {
        $token = self::active()
                ->where("type","=",$token_type)
                ->where("ua","=",\Request::server("HTTP_USER_AGENT"))
                ->where("ip","=",\Request::getClientIp())
                ->first();
        if(!is_null($token))
        {
            $token = $token->toArray();
            return $token['token'];
        }
        return FALSE;
    }
    
    public static function inactive_token($type)
    {
        $token = self::active()
                ->where("type","=",$type)
                ->where("ua","=",\Request::server("HTTP_USER_AGENT"))
                ->where("ip","=",\Request::getClientIp())
                ->get()->first();
        if(!is_null($token))
        {
            $token->status = 0;
            $token->save();
        }
    }
    
    public static function generate_auth_token()
    {
        static $call_cnt = 0;
        if($call_cnt > 10)
            return "";
        ++$call_cnt;
        $token = \General::rand_str(15);
        $user = self::active()->where("type",\Config::get("constant.AUTH_TOKEN_STATUS"))->where("token",'=',$token)->first();
        if(isset($user->token))
        {
            return self::generate_auth_token();
        }
        return $token;
    }
    
    public static function generate_forgetpassword_token()
    {
        static $call_cnt = 0;
        if($call_cnt > 10)
            return "";
        ++$call_cnt;
        $token = \General::rand_str(15);
        $user = self::active()->where("type",\Config::get("constant.FORGETPASS_TOKEN_STATUS"))->where("token",'=',$token)->first();
        if(isset($user->token))
        {
            return self::generate_forgetpassword_token();
        }
        return $token;
    }
    
    public static function generate_activation_token()
    {
        static $call_cnt = 0;
        if($call_cnt > 50)
            return "";
        ++$call_cnt;
        $token = \General::rand_str(15);
        $user = self::active()->where("type",'=',\Config::get("constant.ACCOUNT_ACTIVATION_TOKEN_STATUS"))->where("token",'=',$token)->first();
        if(isset($user->token))
        {
            return self::generate_activation_token();
        }
        return $token;
    }
    
    public static function save_token($param)
    {
        if(!isset($param['id']))
            $token = new Token();
        else
        {
            $token = self::where("id",$param['id'])->first();
            if(is_null($token))
               $token = new Token(); 
        }
        $token->fill($param);
        $token->status = isset($param['status']) ? $param['status'] : 1;
        $token->save();
        return \General::success_res();
    }
    
    public static function is_active($type,$token)
    {
        $user = self::active()->where("type",'=',$type)->where("token",'=',$token)->first();
        if(!is_null($user))
        {
            return $user->sp_id;
        }
        return FALSE;
    }
    
//    public static function get_active_token($token_type)
//    {
//        $token = self::active()
//                ->where("type","=",$token_type)
//                ->where("ua","=",\Request::server("HTTP_USER_AGENT"))
//                ->where("ip","=",\Request::getClientIp())
//                ->get()->first()->toArray();
//        if(isset($token['token']))
//            return $token['token'];
//        return FALSE;
//    }
   
    public static function find_dead_token_id($token_type,$user_id)
    {
        $token = self::where("type",$token_type)
                ->where("ua","=",\Request::server("HTTP_USER_AGENT"))
                ->where("ip","=",\Request::getClientIp())
                ->where("platform","=",app("platform"))
                ->where("sp_id",$user_id)->first();
        
        if(is_null($token))
        {
            return FALSE;
        }
        return $token->id;
    }
    
    public static function generate_otp_for($user_id)
    {
        $data = [];
        $data['otp'] = rand(100000,999999);
        $user = self::active()->where("type",\Config::get("constant.AUTH_TOKEN_STATUS"))->where("token",'=',$token)->first();
        if(isset($user->token))
        {
            return self::generate_auth_token();
        }
        return $token;
    }
    
    public static function filter_user_login_history($param) {
        $start = ($param['page'] - 1) * $param['len'];
        $len = $param['len'];
        $login_history = self::where("sp_id",$param['rechargeValue']['uid']);
        $total = $login_history = $login_history->orderBy("id","DESC");
       // $total = $user;
        $total = $login_history->count();
        $login_history = $login_history->skip($start)->take($len)->get();
        $login_history = $login_history->toArray();
        foreach ($login_history as $key => $val) {
            $login_history[$key]['created_at'] =  date('d-m-Y H:i:s ',strtotime($login_history[$key]['created_at']));
        }
        $res = \General::success_res();
        $res['data'] = $login_history;
        $res['total_record'] = $total;
        return $res;
    }
    
    public static function signout_user_login_history($param) {
        
        //$user_token = self::where("id",'=',$param)->where("type",'=',\Config::get("constant.AUTH_TOKEN_STATUS"))->delete();
        $user_token = self::where("id",'=',$param)->delete();
 
        return \General::success_res();
        
        //detail
        
    }
        
}
