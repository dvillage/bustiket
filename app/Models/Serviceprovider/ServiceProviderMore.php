<?php

namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ServiceProviderMore extends Eloquent {
    
    public $table = 'sp_more';
    protected $hidden = [];
    protected $fillable = array('type','admin_id','token', 'ip', 'ua','status');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }

    
    public static function save_sp($param,$id){
//        dd($param,$id);
        $sp = new ServiceProviderMore;
        $sp->sp_id=$id;
        $sp->vat=$param['vat'];
        $sp->address=$param['address'];
        $sp->city=$param['city'];
        $sp->state=$param['state'];
        $sp->postal_code=$param['pincode'];
        $sp->land_no=$param['landline'];
        $sp->mobile1=$param['mobile1'];
        $sp->mobile2=$param['mobile2'];
        $sp->fax=$param['fax'];
        $sp->cp_name=$param['cpname'];
        $sp->cp_mobile=$param['cpno'];
        $sp->cp_designation = isset($param['designation']) ? $param['designation'] : '';
        $sp->comment=$param['comment'];
        $sp->paypal_email=$param['paypal_id'];
        $sp->save();
        
        return \General::success_res();
    }
    
    public static function edit_sp($param,$id){
//        dd($param,$id);
        $sp = self::where('sp_id',$id)->first();
        
        if(!is_null($sp)){
            $sp->vat=$param['vat'];
            $sp->address=$param['address'];
            $sp->city=$param['city'];
            $sp->state=$param['state'];
            $sp->postal_code=$param['pincode'];
            $sp->land_no=$param['landline'];
            $sp->mobile1=$param['mobile1'];
            $sp->mobile2=$param['mobile2'];
            $sp->fax=$param['fax'];
            $sp->cp_name=$param['cpname'];
            $sp->cp_mobile=$param['cpno'];
            $sp->comment=$param['comment'];
            $sp->paypal_email=$param['paypal_id'];
            $sp->save();
        }else{
            $sp =  new self;
            $sp->sp_id=$id;
            $sp->vat=$param['vat'];
            $sp->address=$param['address'];
            $sp->city=$param['city'];
            $sp->state=$param['state'];
            $sp->postal_code=$param['pincode'];
            $sp->land_no=$param['landline'];
            $sp->mobile1=$param['mobile1'];
            $sp->mobile2=$param['mobile2'];
            $sp->fax=$param['fax'];
            $sp->cp_name=$param['cpname'];
            $sp->cp_mobile=$param['cpno'];
            $sp->comment=$param['comment'];
            $sp->paypal_email=$param['paypal_id'];
            $sp->save();
        }
        
        
        return \General::success_res();
    }
    
}