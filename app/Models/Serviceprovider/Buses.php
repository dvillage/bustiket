<?php
namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Buses extends Eloquent {
    
    public $table = 'buses';
    public $timestamps=true;


    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function sp(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    public function amenities(){
        return $this->hasMany('App\Models\Admin\BusAmenities','bus_id','id');
    }
    public function blockDates(){
        return $this->hasMany('App\Models\Admin\BlockDates','bus_id','id');
    }
    public function busLayout(){
        return $this->hasMany('App\Models\BusLayout','bus_id','id');
    }
    public function busPrice(){
        return $this->hasMany('App\Models\Admin\BusPrice','bus_id','id');
    }
    public function busRoutes(){
        return $this->hasMany('App\Models\Admin\BusRoutes','bus_id','id');
    }
    public function routes(){
        return $this->hasMany('App\Models\Admin\BusRoutes','bus_id','id');
    }
    public function drivers(){
        return $this->hasMany('App\Models\BusDrivers','bus_id','id');
    }
    public function couponCode(){
        return $this->hasMany('App\Models\CouponCode','bus_id','id');
    }
    public function routeCount(){
        return $this->routes()
//                ->select(\DB::raw('id,count(*) as total_routes'))
                ->groupBy('bus_id','from_city_id','to_city_id');
    }
    
    public function setCount(){
//        print_r($this->busLayout());
        return $this->busLayout();
//                ->select(\DB::raw('id,count(*) as total_routes'))
                
    }
    public static function get_bus($param){
        $bus = self::active()->OrderBy('name','asc');
        if(isset($param['name'])){
            $bus = $bus->where('name','like','%'.$param['name'].'%');
        }
        if(isset($param['sp_id'])){
            $bus = $bus->where('sp_id',$param['sp_id']);
        }
        $bus = $bus->get();
        return $bus;
    }

    public static function update_bus_name($param){
        
        $bus = self::where('id',$param['id'])->first();
        if(is_null($bus)){
            return \General::error_res('no bus found for editing.');
        }
        $bus->name = $param['name'];
        $bus->save();
        
        return \General::success_res();
    }
    public static function add_new_bus($param){
        
        $video = \Request::file('video');
        $video_ext = array('mp4','mkv','avi');
        $video_name = '';
        if(isset($video)){
            $ext = $video->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$video_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $video_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $folderName = config('constant.UPLOAD_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $video->getClientOriginalExtension();
            $filePath = $folderName.'/videos/' . $param['sp_id'] . '/' ;
            $video->move($filePath,$FileName);
            $video_name = $FileName;
        }
        
        $new = new self;
        $new->name = $param['bus_name'];
        $new->sp_id = $param['sp_id'];
        $new->type = $param['bus_type'];
        $new->layout = 0;
        $new->status = 1;
        $new->seat_capacity = isset($param['seat_capacity']) ? $param['seat_capacity'] : 0;
        $new->video = $video_name;
        $new->tnc = isset($param['tnc']) ? $param['tnc'] : '';
        $new->active_days = isset($param['active_days']) ? $param['active_days'] : 365;
        $new->save();
        
        $res = \General::success_res();
        return $res;
    }
    
    public static function edit_bus($param){
        $bus_id = $param['bus_id'];
        $sp_id = $param['sp_id'];
        $status = isset($param['bus_status']) ? 1 : 0;
        
        $bus = self::where('id',$bus_id)->first();
        if(is_null($bus)){
            return \General::error_res('no bus found.');
        }
        $layout = isset($param['is_layout']) ? 1 : 0;
        
//        dd($param);
        if(isset($param['amenities'])){
            $amn = \App\Models\Admin\BusAmenities::where('bus_id',$bus_id)->delete();
            
                $amnities = explode(',', $param['amenities']);
                foreach($amnities as $a){
                    if($a){
                        $am = new \App\Models\Admin\BusAmenities();
                        $am->aname = $a;
                        $am->bus_id = $bus_id;

                        $ame = \App\Models\Admin\Amenities::where('name',$a)->first();
                        $am->a_id = $ame->id;
                        $am->aimg = $ame->img;
                        $am->save();
                    }
                }
        }
        
        if(isset($param['block_date'])){
            $bdate = $param['block_date'];
            $p = \App\Models\Admin\BlockDates::where('bus_id',$bus_id)->delete();
            $j = 1;
            foreach($bdate as $pr){
                if($pr['from'] != '' && $pr['to'] != ''){
                    $p = new \App\Models\Admin\BlockDates();
                    $p->from_date = date('Y-m-d',strtotime($pr['from']));
                    $p->to_date = date('Y-m-d',strtotime($pr['to']));
                    $p->bus_id = $bus_id;
                    $p->save();
                    $j++;
                }
            }
        }
        
        if(isset($param['route'])){
            $from = $param['route']['from'];
            $to = $param['route']['to'];
//            dd($param);
            $seat_count = isset($param['seat_count'])?$param['seat_count']:array();
            $price = isset($param['price'])?$param['price']:array();
            $price_ids = isset($param['price_ids'])?$param['price_ids']:array();
            $route_ids = isset($param['route_id'])?$param['route_id']:array();
            $bf = \App\Models\Admin\BusRoutes::where('bus_id',$bus_id)->get()->toArray();
            if(count($bf)>0){
                
                $toC = $to;
                $rexist = array();
                foreach($from as $k=>$fr){
                    
                    $f = json_decode($fr,true);
                    $to = json_decode($toC[$k],true);
                    
                    if(isset($route_ids[$k])){
                        
                        array_push($rexist, $route_ids[$k]);
                        
                        $rts = \App\Models\Admin\BusRoutes::where('id',$route_ids[$k])->first();
                        $rts->name = $f['cname'].'-'.$to['cname'];
                        $rts->route = $f['cname'].'-'.$f['dname'].'-'.$f['tname'].'-'.$to['tname'].'-'.$to['dname'].'-'.$to['cname'];
                        $rts->save();
                        \App\Models\Admin\BusPrice::edit_price($seat_count, $price, $bus_id, $route_ids[$k],$k,$price_ids);
                    }else{
                        
                        $fp = \App\Models\Locality\Citi::where('id',$f['cid'])->first();
                        $fp = $fp->province_id;
                        $top = \App\Models\Locality\Citi::where('id',$to['cid'])->first();
                        $top = $top->province_id;
                        $rts = new \App\Models\Admin\BusRoutes();
                        $rts->name = $f['cname'].'-'.$to['cname'];
                        $rts->route = $f['cname'].'-'.$f['dname'].'-'.$f['tname'].'-'.$to['tname'].'-'.$to['dname'].'-'.$to['cname'];
                        $rts->bus_id = $bus_id;
                        $rts->from_city_id = $f['cid'];
                        $rts->to_city_id = $to['cid'];
                        $rts->from_district_id = $f['did'];
                        $rts->to_district_id = $to['did'];
                        $rts->from_terminal_id = $f['tid'];
                        $rts->to_terminal_id = $to['tid'];
                        $rts->from_province = $fp;
                        $rts->to_province = $top;
                        $rts->boarding_time = $f['btime'];
                        $rts->droping_time = $to['btime'];
                        $rts->save();

                        \App\Models\Admin\BusPrice::add_price($seat_count, $price, $bus_id, $rts->id,$k);
//                        array_push($rexist, $rts->id);
                    }
                }
                $allR = array();
                foreach($bf as $b){
                    array_push($allR, $b['id']);
                }
                foreach($allR as $al){
                    if(!in_array($al, $rexist)){
                        \App\Models\Admin\BusRoutes::where('id',$al)->delete();
                    }
                }
                
            }else{
                
                \App\Models\Admin\BusRoutes::add_new_route($from, $to,$bus_id,$seat_count,$price);
            }
        }else{
            \App\Models\Admin\BusRoutes::where('bus_id',$bus_id)->delete();
        }
        
        if($layout){
            $bl = \App\Models\BusLayout::where('bus_id',$bus_id)->first();
            if(!is_null($bl)){
                $bl->rows = $param['total_rows'];
                $bl->columns = $param['total_columns'];
                $bl->save();
            }else{
                $bl =new \App\Models\BusLayout();
                $bl->name = 'Lower Birth';
                $bl->short_name = 'lb';
                $bl->bus_id = $bus_id;
                $bl->rows = $param['total_rows'];
                $bl->columns = $param['total_columns'];
                $bl->save();
            }
            $seats = $param['seat_array'];
            $seats = json_decode($seats,true);
            $sl = \App\Models\SeatMap::where('layout_id',$bl->id)->get()->toArray();
            if(count($sl)==0){
                $idx = 1;
                foreach($seats as $k=>$v){
                    if($seats[$k] != ''){
                        $rc = explode('-', $k);
                        $r = $rc[0];
                        $c = $rc[1];
                        $vl = explode('-', $v);
                        $ve = $vl[0];
                        $lable = $ve == 'DS' ? 'DS' : ( $ve == 'T' ? 'T' : ( $ve == 'D' ? 'D' : $vl[1] ));
                        
                        $ns = new \App\Models\SeatMap();
                        $ns->layout_id = $bl->id;
                        $ns->seat_index = $ve =='CS' ? $idx : 0;
                        $ns->seat_lbl = $lable;
                        $ns->object_type = $ve;
                        $ns->length = 1;
                        $ns->height = 1;
                        $ns->row = $r;
                        $ns->col = $c;
                        $ns->save();
                        if($ve == 'CS'){
                            $idx++;
                        }
                    }
                }
            }else{
//                $slA = $sl->toArray();
                $slA = $sl;
                foreach($slA as $va){
                    if(array_key_exists($va['row'].'-'.$va['col'],$seats)){
                        $slE = \App\Models\SeatMap::where('layout_id',$bl->id)->where('row',$va['row'])->where('col',$va['col'])->first();
                        if(!is_null($slE)){
                            $v = $seats[$va['row'].'-'.$va['col']];
//                            print_r($seats['3-7']);
                            $vl = explode('-', $v);
                            $ve = trim($vl[0]);
                            $lbl = '';
                            if(isset($vl[1])){
                                $lbl = $vl[1];
                            }
                            
                            $lable = $ve == 'DS' ? 'DS' : ( $ve == 'T' ? 'T' : ( $ve == 'D' ? 'D' : $vl[1] ));
                            $slE->seat_lbl =$lable;
                            $slE->object_type = $ve;
                            $slE->save();
                        }
                    }else{
                        $slE = \App\Models\SeatMap::where('layout_id',$bl->id)->where('row',$va['row'])->where('col',$va['col'])->delete();
                    }
                }
                
                
                foreach($seats as $k=>$va){
                    $rc = explode('-', $k);
                    $r = $rc[0];
                    $c = $rc[1];
                    $slE = \App\Models\SeatMap::where('layout_id',$bl->id)->where('row',$r)->where('col',$c)->first();
                    $maxIdx = \App\Models\SeatMap::where('layout_id',$bl->id)->orderBy('seat_index','desc')->first();
//                    $maxIdx = \App\Models\SeatMap::where('layout_id',$bl->id)->first();
//                    print_r($bl->id);exit;
                    $maxIdx = $maxIdx->seat_index +1;
                    if(is_null($slE)){
                        $vl = explode('-', $va);
                        $ve = $vl[0];
                        $lbl = '';
                            if(isset($vl[1])){
                                $lbl = $vl[1];
                            }
                            
                        $lable = $ve == 'DS' ? 'DS' : ( $ve == 'T' ? 'T' : ( $ve == 'D' ? 'D' : $vl[1] ));
                        $ns = new \App\Models\SeatMap();
                        $ns->layout_id = $bl->id;
                        $ns->seat_index = $ve =='CS' ? $maxIdx : 0;
                        $ns->seat_lbl = $lable;
                        $ns->object_type = $ve;
                        $ns->length = 1;
                        $ns->height = 1;
                        $ns->row = $r;
                        $ns->col = $c;
                        $ns->save();
                        
                    }
                }
            }
        }else{
            $bl = \App\Models\BusLayout::where('bus_id',$bus_id)->delete();
            $bl_id = null;
        }
        
        if(isset($param['drivers'])){
            $dr = $param['drivers'];
            $bdr = \App\Models\BusDrivers::where('bus_id',$bus_id)->get();
            if(is_null($bdr)){
                foreach($dr as $d){
                    $ndr = new \App\Models\BusDrivers();
                    $ndr->bus_id = $bus_id;
                    $ndr->driver_id = $d;
                    $ndr->save();
                }
            }else{
                foreach($bdr as $bd){
                    if(!in_array($bd['driver_id'], $dr)){
                        \App\Models\BusDrivers::where('bus_id',$bus_id)->where('driver_id',$bd['driver_id'])->delete();
                    }
                }
                foreach($dr as $d){
                    $cdr = \App\Models\BusDrivers::where('bus_id',$bus_id)->where('driver_id',$d)->first();
                    if(is_null($cdr)){
                        $ndr = new \App\Models\BusDrivers();
                        $ndr->bus_id = $bus_id;
                        $ndr->driver_id = $d;
                        $ndr->save();
                    }
                }
            }
        }else{
            $drv = \App\Models\BusDrivers::where('bus_id',$bus_id)->delete();
        }
        
        $filename = $bus->images;
        
        $oimg = \Request::file('images');
        
        $other_img = self::add_image($oimg);
        
        if($other_img != ''){
            if($bus->images == ''){
                $bus->images = $other_img;
            }else{
                $bus->images = $bus->images.','.$other_img;
            }
            
        }
        
        if(isset($param['removed_images'])){
            $images = json_decode($param['removed_images'],true);
            $old_img = explode(',', $bus->images);
            if(count($images) > 0){
                foreach($images as $im){
                    if(($key = array_search($im, $old_img)) !== false) {
                        unset($old_img[$key]);
                        $filePath = config('constant.BUS_IMAGE_PATH');
                        if(file_exists($filePath.$im)){
                            unlink($filePath.$im);
                        }
                    }
                }
//                dd($old_img);
                $bus->images = implode(',', $old_img);
            }
        }
        
        
        $bus->name = $param['bus_name'];
        $bus->bus_number = $param['bus_number'];
        $bus->status = $status;
        $bus->layout = $layout;
        $bus->layout_id = isset($bl->id) ? $bl->id : $bl_id;
        $bus->seat_capacity = $param['total_seats'];
        $bus->tnc = $param['tnc'];
        $bus->save();
        
        return \General::success_res('bus edited successfully !!');
        
    }
    
    public static function add_image($bnrimg){
        
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$img_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    $filePath = config('constant.BUS_IMAGE_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($img_name != ''){
                        $img_name = $img_name.','.$FileName;
                    }else{
                        $img_name = $FileName;
                    }
                }
//            
            
        }
        return $img_name;
    }

    public static function search($param)
    {
//        dd($param);
        $filter_fields = \App\Models\Admin\BusRoutes::get_from_to_fileds_by_inpute($param);
//        dd($filter_fields);
        if($filter_fields['flag'] != 1)
            return [];
        $routes = \App\Models\Admin\BusRoutes::with(["fromTerminal","toTerminal",'fromCity','toCity','fromDistrict','toDistrict'])->where($filter_fields['data']['from_field'],$filter_fields['data']['from_val'])
                ->where($filter_fields['data']['to_field'],$filter_fields['data']['to_val'])
                ->get()->toArray();
//        dd($routes);
        $result = \App\Models\Admin\BusRoutes::group_by_bus($routes,$param);
//        echo count($result);
        return $result;
        
    }
    
    public static function price_for15($param){
        $filter_fields = \App\Models\Admin\BusRoutes::get_from_to_fileds_by_inpute($param);
//        dd($filter_fields);
        if($filter_fields['flag'] != 1)
            return [];
        $routes = \App\Models\Admin\BusRoutes::where($filter_fields['data']['from_field'],$filter_fields['data']['from_val'])
                                                ->where($filter_fields['data']['to_field'],$filter_fields['data']['to_val'])
                                                ->get(['id','bus_id'])->toArray();
        $bus_routes = [];
        foreach ($routes as $key => $val){
            $bus_routes[$val['bus_id']]['routes'][] = $val;
        }
        $min_price = [];
        
        if(count($bus_routes) > 0){
            for($i = 0 ; $i < 16 ; $i++){
                $price = [];
                $d1 = date('Y-m-d',strtotime($param['date'].' +'.$i.' day '));

                foreach($bus_routes as $r=>$route){
                    $price[] = \App\Models\Admin\BusPrice::getSeatPriceBySale($r,$route['routes'],$param['nop'], $d1); 
                }
                $date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($d1)),date("l", strtotime($d1)));
                $day = explode(',', $date);
                $dtmonth = explode(' ', $day[1]);
    //            $dtmonth = explode(' ', $day[1]);
    //            $min_price[$d1] = min($price);
                $min_price[substr($day[0],0,3).', '.$dtmonth[0].' '.substr($dtmonth[1],0,3)] = [
                    'price' => \General::number_format(min($price),3),
                    'date'  => $d1,
                ];
            }
        }
        if(count($min_price) == 0){
            $res = \General::error_res('No Bus Price Founds.');
            return $res;
        }
        $res = \General::success_res();
        $res['data'] = $min_price;
        return $res;
    }
     
}