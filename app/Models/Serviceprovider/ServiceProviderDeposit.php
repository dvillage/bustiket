<?php 

namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderDeposit extends Model {

    
    public $table = 'sp_depo_request';
    protected $fillable = array('name','status','sp_id','amount','payment_by','bank_acc_name','bank_acc_no');
//    protected $appends = array('role');
    public $timestamps=true;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function serviceProvider(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    
    public static function get_deposite_request($param){
        
        $count=self::orderBy('id','desc');
        
        if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $count = $count->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
                        });
        }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
            $count = $count->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('id',config('constant.CURRENT_LOGIN_ID'));
                        });
        }else{
            $count=$count->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('type','Main');
                        });
        }
        $count = $count->count();
        if(isset($param['search']) && $param['search']!=''){
            $count=self::orderBy('id','desc')->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('first_name','like','%'.$param['search'].'%');
                        });
            if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $count = $count->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
                        });
            }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
                $count = $count->whereHas('serviceProvider', function ($query) use($param) {
                                $query->where('id',config('constant.CURRENT_LOGIN_ID'));
                            });
            }else{
                $count=$count->whereHas('serviceProvider', function ($query) use($param) {
                                $query->where('type','Main');
                            });
            }
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::orderBy('id','desc')->with('serviceProvider');
        if(isset($param['ss_type']) && $param['ss_type'] != ''){
            
            $spdata=$spdata->whereHas('serviceProvider', function ($query)  use($param)  {
                                        $query->where('type',$param['ss_type']);
                                    });
        }
        if(isset($param['status']) && $param['status'] != ''){
            $spdata = $spdata->where('status',$param['status']);
        }
        if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $spdata = $spdata->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
                        });
        }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
            $spdata = $spdata->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('id',config('constant.CURRENT_LOGIN_ID'));
                        });
        }else{
            $spdata=$spdata->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('type','Main');
                        });
        }
        
        $spdata=$spdata->skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!='' || isset($param['status']) && $param['status'] != '' || isset($param['ss_type']) && $param['ss_type'] != ''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::orderBy('id','desc')->with('serviceProvider');
            if(isset($param['search']) && $param['search']!=''){
            $spdata=$spdata->whereHas('serviceProvider', function ($query)  use($param)  {
                                        $query->where('first_name','like','%'.$param['search'].'%');
                                    });
            }
             if(isset($param['ss_type']) && $param['ss_type'] != ''){
             $spdata=$spdata->whereHas('serviceProvider', function ($query)  use($param)  {
                                        $query->where('type',$param['ss_type']);
                                    });
             }
             if(isset($param['status']) && $param['status'] != ''){
             $spdata = $spdata->where('status',$param['status']);
             }
             
            if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $spdata = $spdata->whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
                            });
            }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
                $spdata = $spdata->whereHas('serviceProvider', function ($query) use($param) {
                                $query->where('id',config('constant.CURRENT_LOGIN_ID'));
                            });
            }else{
                $spdata=$spdata->whereHas('serviceProvider', function ($query) use($param) {
                                $query->where('type','Main');
                            });
            }
            $spdata = $spdata->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function approve_deposit($param){
        
        $id = $param['id'];
        $t = $param['type'];
        
        $dep = self::where('id',$id)->where('status',0)->first();
        if(is_null($dep)){
            return \General::error_res('no deposit request found.');
        }
        $msg = 'no changes done.';
        if($t == 'A'){
            $dep->status = 1;
            $msg = 'deposit request approved';
            
            $ss = \App\Models\Serviceprovider\ServiceProvider::where('id',$dep->sp_id)->first();
            if(is_null($ss)){
                return \General::error_res('no service provider found.');
            }
            $ss->balance = $ss->balance + $dep->amount;
            $ss->save();
            
            $swallet = new ServiceProviderWalletHistory();
            $swallet->sp_id = $dep->sp_id;
            $swallet->amount = $dep->amount;
            $swallet->comment = 'wallet balance request credited #'.$dep->id;
            $swallet->save();
            
            
            $device_token = $ss->device_token;
            $device_tokens = array($device_token);
            $msgs = array("your deposite request for amount '.$dep->amount .' has been approved.");
            $title = array("Bustiket");
            if($device_token){
                $push =\App\Lib\Push::android_push($device_tokens, $msgs, $title, $badge = [1], $screen = array(), $meta = array());
            }
            
            
        }elseif($t == 'R'){
            $dep->status = 2;
            $msg = 'deposit request rejected';
        }
        $dep->save();
        
        return \General::success_res($msg);
    }
    public static function get_payment_request($param){
        $count=self::orderBy('id','desc');
        $count = $count->where('sp_id',config('constant.CURRENT_LOGIN_ID'));
        
        
        if(isset($param['search']) && $param['search']!=''){
            $count=$count->where('bank_acc_name','like','%'.$param['search'].'%')->orWhere('bank_acc_no','like','%'.$param['search'].'%');
        
        }
        $count = $count->count();
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::orderBy('id','desc')->with('serviceProvider')->where('sp_id',config('constant.CURRENT_LOGIN_ID'));
        
        $spdata=$spdata->skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!='' || isset($param['status']) && $param['status'] != '' || isset($param['ss_type']) && $param['ss_type'] != ''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::orderBy('id','desc')->with('serviceProvider')->where('sp_id',config('constant.CURRENT_LOGIN_ID'));
            if(isset($param['search']) && $param['search']!=''){
                $spdata=$spdata->where('bank_acc_name','like','%'.$param['search'].'%')->orWhere('bank_acc_no','like','%'.$param['search'].'%');
            }
             
             if(isset($param['status']) && $param['status'] != ''){
             $spdata = $spdata->where('status',$param['status']);
             }
             
            $spdata = $spdata->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    public static function add_new_payment_request($param){
        $n = new self;
        $n->status = 0;
        $n->sp_id = $param['id'];
        $n->amount = $param['amount'];
        $n->payment_by = isset($param['pay_bank']) ? $param['pay_bank'] : '';
        $n->bank_id = isset($param['bank_id']) ? $param['bank_id'] : null;
        $n->bank_acc_name = isset($param['acc_name']) ? $param['acc_name'] : '';
        $n->bank_acc_no = isset($param['acc_number']) ? $param['acc_number'] : '';
        $n->save();
        
        $res = \General::success_res('payment request sent.');
        $res['data'] = $n;
        return $res;
    }
}
