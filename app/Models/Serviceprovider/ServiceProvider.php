<?php
namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class ServiceProvider extends Eloquent implements Authenticatable {
    
    use AuthenticableTrait;
    
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthIdentifierName() {
        return $this->getKeyName();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
        return $this->{$this->getRememberTokenName()};
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public function setRememberToken($value) {
        $this->{$this->getRememberTokenName()} = $value;
    }
    
    public $table = 'sp';
    protected $hidden = ['password','device_token','remember_token'];
    public $timestamps=false;


    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function spMore(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProviderMore','sp_id','id');
    }
    
    public function spBuses(){
        return $this->hasMany('App\Models\Serviceprovider\Buses','sp_id','id');
    }
    public function policies(){
        return $this->hasMany('App\Models\CancelPolicy','sp_id','id');
    }
    
    
    public static function doLogin($param){
        
        if(isset($param['remember']))
        {
            \Cookie::get("remember",1);
            if($param['remember']=='on')
                $param['remember']=1;
            else
                $param['remember']=0;
//            \App\Models\Admin\Settings::set_config(['sanitize_input' => $param['remember']]);
        }
        $user = ServiceProvider::active()->where("email", $param['uname'])->first();
        $res['data']=$user;
        $res['flag']=0;
        if (is_null($user)) {
            $res['flag']=0;
            return $res;
        }
        if (!\Hash::check($param['password'], $user->password)) {
            $res['flag']=0;
            return $res;
        }
        if(isset($param['remember']) && $param['remember']==1)
        {
            
            $auth_token = \App\Models\Serviceprovider\Token::generate_auth_token();
            
            $token_data = ['sp_id' => $user->id,'token' => $auth_token,'type' => 'auth'];
            \App\Models\Serviceprovider\Token::save_token($token_data);
            
            \Auth::guard("sp")->loginUsingId($user->id,true);
            
        }
        else{
            \Auth::guard("sp")->loginUsingId($user->id);
        }
        \Auth::guard('ss')->logout();
        \Auth::guard('admin')->logout();
        \Auth::guard('user')->logout();
        $res['flag']=1;
        return $res;
    }
    
    public static function get_handle_fee($param){
        $handlefee = self::where('id',$param)->get(['id','first_name','last_name','type','handling_fee','handling_fee_type'])->toArray();
        return $handlefee;
    }
    
    
    public static function save_sp($param){
//        dd($param);
        
        $bnrimg = \Request::file('sp_logo');
        $img_ext = ['jpg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){
            $ext = $bnrimg->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$img_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $filePath = config('constant.UPLOAD_SP_LOGO_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimg->getClientOriginalExtension();
            $bnrimg->move($filePath,$FileName);
            $img_name = $FileName;
        }
//        dd($img_name);
        
        $sp = new ServiceProvider;
        $sp->status=1;
        $sp->type=$param['type'];
        $sp->parent_id=0;
        if(isset($param['parentsp']))
            $sp->parent_id=$param['parentsp'];
        $sp->first_name=$param['fname'];
        $sp->last_name=$param['lname'];
        $sp->email=$param['email'];
        $sp->password=\Hash::make($param['password']);
       
        $sp->balance=0;
        $sp->handling_fee=$param['handlefee'];
        $sp->handling_fee_type=$param['handletype'];
        $sp->comm_fm_sp=$param['cfsp'];
        $sp->comm_fm_sp_type=$param['cfsptype'];
        $sp->email_verified=0;
        $sp->device_type=0;
        $sp->device_token=0;
        $sp->remember_token=0;
        $sp->avatar = $img_name;
        $sp->save();
        
        $res=\App\Models\Serviceprovider\ServiceProviderMore::save_sp($param,$sp->id);
        if($res['flag']==0)
            return \General::error_res();
        return \General::success_res();
    }
    
    public static function edit_sp($param){
       // dd($param);
        
        $bnrimg = \Request::file('sp_logo');
        $img_ext = ['jpg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){
            $ext = $bnrimg->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$img_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $filePath = config('constant.UPLOAD_SP_LOGO_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimg->getClientOriginalExtension();
            $bnrimg->move($filePath,$FileName);
            $img_name = $FileName;
        }
        
        $sp = self::where('id',$param['spid'])->first();
        $sp->parent_id=0;
        if(isset($param['parentsp']))
            $sp->parent_id=$param['parentsp'];
        $sp->first_name=$param['fname'];
        $sp->last_name=$param['lname'];
//        $sp->email=$param['email'];
        if(!empty($param['password'])){
            $sp->password=\Hash::make($param['password']);
        }
        
        $sp->handling_fee=isset($param['handlefee']) ? $param['handlefee'] : $sp->handling_fee;
        $sp->handling_fee_type=isset($param['handletype']) ? $param['handletype'] : $sp->handling_fee_type;
        $sp->comm_fm_sp=isset($param['cfsp'])?$param['cfsp']:$sp->comm_fm_sp;
        
        $sp->comm_fm_sp_type=isset($param['cfsptype'])?$param['cfsptype']:$sp->comm_fm_sp_type;
        
        if(!isset($param['profile'])){
            $sp->status=isset($param['status'])?1:0;
        }
        
        if($img_name != ''){
            $sp->avatar = $img_name;
        }
        $sp->save();
        
        $res=\App\Models\Serviceprovider\ServiceProviderMore::edit_sp($param,$sp->id);
        if($res['flag']==0)
            return \General::error_res();
        return \General::success_res();
    }
    
    public static function get_sp($param = array()){
        $splist=self::where('type','Main');
        if(isset($param['name'])){
            $splist = $splist->active()->where(function($query) use ($param){
                                                            $query->where('first_name','like','%'.$param['name'].'%')
                                                                  ->orWhere('last_name','like','%'.$param['name'].'%');
                                                        });
        }
        $splist = $splist->get(['id','first_name','last_name'])->toArray();
        return $splist;
    }
    
    public static function get_allsp($param = array()){
//        $splist=self::orderBy('created_at','DESC');
        if(isset($param['name'])){
            $splist = self::active()->where(function($query) use ($param){
                                                            $query->where('first_name','like','%'.$param['name'].'%')
                                                                  ->orWhere('last_name','like','%'.$param['name'].'%');
                                                        });
        }
        $splist = $splist->get(['id','first_name','last_name'])->toArray();
        return $splist;
    }
    
    public static function get_sp_filter($param){
        
        $count=self::where('type',$param['type']);
        
        if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $count = $count->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
        }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
            $count = $count->where('id',config('constant.CURRENT_LOGIN_ID'));
        }
        $count = $count->count();
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('type',$param['type'])->where(function($query) use ($param){
                                                            $query->where('first_name','like','%'.$param['search'].'%')
                                                                  ->orWhere('last_name','like','%'.$param['search'].'%');
                                                        });
            if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
                $count = $count->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
            }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
                $spdata = $spdata->where('id',config('constant.CURRENT_LOGIN_ID'));
            }
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
//        dd(config('constant.SP_TYPE'));
        $spdata=self::where('type',$param['type'])->with("spMore")->with('spBuses');
        if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
            $spdata = $spdata->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
        }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
            $spdata = $spdata->where('id',config('constant.CURRENT_LOGIN_ID'));
        }
        $spdata = $spdata->skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::where('type',$param['type'])->with('spBuses')->where(function($query) use($param){
                                                        $query->where('first_name','like','%'.$param['search'].'%')
                                                              ->orWhere('last_name','like','%'.$param['search'].'%');
                                                })->with("spMore");
            if(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') == 'Main'){
                $spdata = $spdata->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
            }elseif(config('constant.LOGGER') == 'SP' && config('constant.SP_TYPE') != 'Main'){
                $spdata = $spdata->where('id',config('constant.CURRENT_LOGIN_ID'));
            }
            $spdata = $spdata->skip($start)->take($len)->get()->toArray();

        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_ServiceProvider($param){
        $res=self::where('id',$param['id'])->delete();

        return $res;
    }
    
    public static function providers_details($param){
//        dd($param);
        $details=self::where('id',$param['id'])->with("spMore")->get()->toArray();
        
        $res['data']=$details;
        return $res;
    }
    
    public static function getProfile(){
        $admin_detail = self::where("id", \Auth::guard('sp')->user()->id)->first()->toArray();
//        dd($admin_detail);
        $res['name']=$admin_detail['first_name'];
        $res['lname']=$admin_detail['last_name'];
        $res['email']=$admin_detail['email'];
        $res['flag']=1;
        
        return $res;
    }
    public static function check_parent($id,$parent){
        $sp = self::where('id',$id)->where('parent_id',$parent)->first();
        if(is_null($sp)){
            return 0;
        }
        return 1;
    }
    
    public static function change_admin_password($param)
    {
//        dd($param);
        $admin_detail = self::where("id", \Auth::guard('sp')->user()->id)->first();
        $res['data']= $admin_detail;
        $res['flag']=0;
        $res['msg']="";
        
        if (is_null($admin_detail)) {
            
            return $res;
        }
           
        if(\Hash::check($param['old_password'],$admin_detail->password))
        {
            if($param['new_password'] == $param['confirm_password'])
            {
                $admin_detail->first_name = $param['name'];
                $admin_detail->last_name = $param['lname'];
//                $admin_detail->email = $param['email'];
                $admin_detail->password = \Hash::make($param['new_password']);
                $admin_detail->save();
                
//                \App\Models\Admin\Token::delete_token();
                
                $res['data']= $admin_detail;
                $res['flag']=1;
                $res['msg']="Admin password updated successfullly.";
                return $res;
            }
            else
            {
                $res['data']= $admin_detail;
                $res['flag']=0;
                $res['msg']="New and Confirm password do not match.";
                return $res;
            }
        }
        else
        {
            $res['msg']="Wrong Old Password.";
            return $res;
//            return \General::error_res("Wrong Password.");
        }
    }
    
    public static function is_logged_in($token) {
        if (\Request::wantsJson()) {
            if ($token == "") {
                return \General::session_expire_res();
            }
            $already_login = \App\Models\Serviceprovider\Token::is_active(\Config::get("constant.AUTH_TOKEN_STATUS"), $token);

            if (!$already_login)
                return \General::session_expire_res("unauthorise");
            else {
                $user = self::where("id", $already_login)->with('spMore')->first()->toArray();
                unset($user['password']);
                $user['auth_token'] = $token;
                app()->instance('logged_in_operator', $user);
            }
        } else {
            if (!\Auth::guard('sp')->check()) {
                \Auth::guard('sp')->logout();
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return \General::session_expire_res("unauthorise");
            } else {
                $operator_data = \Auth::guard('sp')->user();
                $operator = [
                    'SP_id'             => $operator_data->id,
                    'SP_name'           => $operator_data->first_name.' '.$operator_data->last_name,
                    'SP_email'          => $operator_data->email,
                    'SP_vat'            => $operator_data->sp_more->vat,
                    'SP_address'        => $operator_data->sp_more->address,
                    'SP_city'           => $operator_data->sp_more->city,
                    'SP_state'          => $operator_data->sp_more->state,
                    'SP_landNo'         => $operator_data->sp_more->land_no,
                    'SP_mobile1'        => $operator_data->sp_more->mobile1,
                    'SP_mobile2'        => $operator_data->sp_more->mobile2,
                    'SP_fax'            => $operator_data->sp_more->fax,
                    'comm_sp'           => $operator_data->comm_fm_sp,
                    'comm_sp_type'      => $operator_data->comm_fm_sp_type,
                    'SP_cpname'         => $operator_data->sp_more->cp_name,
                    'SP_cpdesignation'  => $operator_data->sp_more->cp_designation,
                    'SP_cpmobile'       => $operator_data->sp_more->cp_mobile,
                    'SP_comments'       => $operator_data->sp_more->comment,
                    'tax'               => $operator_data->tax,
                    'SP_status'         => $operator_data->status,
                    'SP_verified'       => $operator_data->email_verified,
                    'sp_type'           => $operator_data->type,
                    'balance'           => $operator_data->balance,
                    'parent_id'         => $operator_data->parent_id,
                    'paypal_id'         => $operator_data->sp_more->paypal_email,
        //            'auth_token'        => $res['data']['auth_token'],
                ];

                $ua = \Request::server("HTTP_USER_AGENT");
                $ip = \Request::server("REMOTE_ADDR");

                $session = \App\Models\Serviceprovider\Token::active()->where("type", config("constant.AUTH_TOKEN_STATUS"))->where("ua", $ua)->where("ip", $ip)->where("user_id", $operator['SP_id'])->first();
//                dd($session);
                if (is_null($session)) {
                    \Auth::guard('sp')->logout();
                    $operator['auth_token'] = "";
                } else {
                    $operator['auth_token'] = $session['token'];
                }
                app()->instance('logged_in_operator', $operator);
            }
        }
        return \General::success_res();
    }
    
    public static function do_login($param) {
        $operator = self::active()->with('spMore')
                ->where("email", $param['SP_email'])
                ->first();
        if (is_null($operator)) {
            return \General::error_res("Account does not exists");
        }
        
//        dd($operator->toArray());
        
        if (!\Hash::check($param['SP_password'], $operator->password) ) {
            return \General::error_res("invalid_email_password");
        }
        
        //dd($operator);
        $dead_token_id = \App\Models\Serviceprovider\Token::find_dead_token_id(\Config::get("constant.AUTH_TOKEN_STATUS"), $operator->id);
        $platform = app("platform");
        $token = \App\Models\Serviceprovider\Token::generate_auth_token();
        if ($token == "")
            return \General::error_res("try_again");

        $data = ["type" => \Config::get("constant.AUTH_TOKEN_STATUS"), "platform" => $platform, "sp_id" => $operator->id, "token" => $token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];
        if ($dead_token_id) {
            $data['id'] = $dead_token_id;
        }
        \App\Models\Serviceprovider\Token::save_token($data);
        if (isset($param['device_token'])) {
            $operator->device_token = $param['device_token'];
            $operator->device_type = app("platform");
            $operator->save();
        }
        $operator_data = $operator->toArray();
        $operator_data['auth_token'] = $token;
        unset($operator_data['password']);
//        if ($operator_data['type'] != config("constant.SERVICE_PROVIDER_MAIN")) {
//            $seat_seller = \App\Models\SeatSeller\User::where("agent_email", $operator_data['SP_email'])->first();
//            if (isset($param['device_token'])) {
//                $seat_seller->device_token = $param['device_token'];
//                $seat_seller->device_type = app("platform");
//                $seat_seller->save();
//            }
//            $seat_seller = $seat_seller->toArray();
//        }
        \Auth::guard("sp")->loginUsingId($operator->id);
        $operator_data['agnt_amt'] = $operator_data['balance'];
        $operator_data['SP_mobile1'] = \App\Models\Services\General::formate_mobile_no($operator_data['sp_more']['mobile1']);
        $res = \General::success_res("Login Success");
        $res['data'] = $operator_data;
        return $res;
    }
    
    public static function change_password($param) {
        $logged_in_user = app("logged_in_operator");
//        dd($logged_in_user);
        $id = $logged_in_user['id'];
        $user = ServiceProvider::where("id", $id)->first();
        if (is_null($user)) {
            return \General::error_res("user_not_found");
        }
        if ($user->status == config("constant.USER_SUSPEND_STATUS")) {
            return \General::error_res("account_suspended");
        }
//        \General::dd($param);
//        \General::dd($user->user_password,1);
        if (!\Hash::check($param['old_password'], $user->password)) {
            return \General::error_res("invalid_old_password");
        }
        $user->password = \Hash::make($param['new_password']);;
        $user->save();
        return \General::success_res("password_changed");
    }
    
    public static function forget_password($param) {
        $user = self::where("email", $param['SP_email'])->first();
        if (is_null($user)) {
            return \General::error_res("email_not_found");
        }
        if ($user->status == config("constant.USER_INACTIVE_STATUS")) {
            return \General::error_res("Account Inactive");
        }
//        $pass = $user->password;
        $user_detail = $user->toArray();
//        $user_detail['password'] = $pass;
        $user_detail['mail_subject'] = \Lang::get("success.forgetpassword_mail_subject");
        \Mail::send('emails.operator.forget_password', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject(\Lang::get("success.forgetpassword_mail_subject"));
        });
        return \General::success_res("forgot_password_mail_sent");
    }
    
    public static function update_profile($param) {
        $user = self::where("id", $param['SP_id'])->with('spMore')->first();
        if (is_null($user)) {
            return \General::error_res("invalid_user");
        }
        if (isset($param['SP_name']))
            $sp = explode (' ', $param['SP_name']);
            $user->first_name   = isset($sp[0]) ? $sp[0] : '';
            $user->last_name    = isset($sp[1]) ? $sp[1] : '';
            

        if (isset($param['SP_mobile1'])) {
            $param['SP_mobile1'] = \App\Models\Services\General::formate_mobile_no($param['SP_mobile1']);
            ServiceProviderMore::where('sp_id',$user->id)->update(['mobile1'=>trim($param['SP_mobile1'])]);
//            $user->mobile1 = trim($param['SP_mobile1']);
        }
        $user->save();
        $user1 = self::where("id", $param['SP_id'])->with('spMore')->first();
        $res = \General::success_res("user_profile_updated");
        $user_data = $user1->toArray();
        unset($user_data['password']);
        $res['data'] = $user_data;
        return $res;
    }
    
    public static function search_bus($param) {
        $res = \General::error_res("No bus found");
        $traveling_date = $param['date'];
//        $three_days = date("Y-m-d",strtotime("+3 days"));
        $today = date("Y-m-d");
        $diff = strtotime($today) - strtotime($traveling_date);
        if( $diff > 0 )
        {
            return \General::error_res("Select booking from today or after today's date");
        }
        
        if ($param['type'] == config("constant.SERVICE_PROVIDER_MAIN")) {
//            dd(1);
            $res = self::search_bus_service_provider_main($param);
        } else if ($param['type'] == config("constant.SERVICE_PROVIDER_A")) {
            $res = self::search_bus_service_provider_a($param);
        } else if ($param['type'] == config("constant.SERVICE_PROVIDER_B")) {
            $res = self::search_bus_service_provider_b($param);
        }
        return $res;
    }
    
    public static function search_bus_service_provider_main($param) {
//        dd($param);
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
//            dd(1);
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from'      => [
                                'name' => $from_city['name'],
                                'type' => $from_city['type'],
                            ],
            'to'        => [
                                'name' => $to_city['name'],
                                'type' => $to_city['type'],
                            ],
            'date'      => $param['date'],
            'nop'       => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
            'operator'  => 1,
            'parent'    => $param['id'],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
        
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

                $businfo_data[] = [
                    'Bus_id' => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem' => rtrim($luxitem, ", "),
                    'rute' => $val['routes'][0]['route'],
                    'parent_busname' => $val['bus']['name'],
                    'available_seats' => $val['bus']['total_seats'],
                    'bus_fare' => \General::number_format($val['bus']['price'], 3),
                    'time' => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration' => $val['routes'][0]['duration'],
                ];
            }else{
                foreach($val['bus'] as $b){
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];

        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }
        
        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
    public static function search_bus_service_provider_main_old($param) {
        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];

        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];
        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();
        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }
        $businfo = \App\Models\Businfo::
                        select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                        ->join('parentbus', 'parentbus.pid', '=', 'businfo.parbus')
                        ->whereNotIn("businfo.sp_id", $sp_ids)
//                        ->where("Bus_status", 1)
                        ->where("businfo.SP_id", $param['SP_id'])
                        ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                        ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                        ->where('businfo.Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                        ->where('businfo.Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"))->get();

        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->toArray();

        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }
        $bus_data = \App\Models\Businfo::
                        select("*")
                        ->join('serviceprovider_info', 'serviceprovider_info.SP_id', '=', 'businfo.SP_id')
                        ->join('parentbus', 'parentbus.pid', '=', 'businfo.parbus')
                        ->whereIn("businfo.Bus_id", $bus_id_valid)
                        ->where('businfo.Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                        ->where('businfo.Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"))
                        ->groupBy("Bus_id")
                        ->skip(0)
                        ->take($param['len'])
                        ->get()->toArray();

        $businfo_data = self::search_bus_formated($bus_data, $param);

        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->where("SP_id", $param['SP_id'])
                        ->get()->toArray();

        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operators,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    public static function search_bus_service_provider_a($param) {
//        dd($param);
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from'      => [
                                'name' => $from_city['name'],
                                'type' => $from_city['type'],
                            ],
            'to'        => [
                                'name' => $to_city['name'],
                                'type' => $to_city['type'],
                            ],
            'date'      => $param['date'],
            'nop'       => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
            'operator'  => 1,
            'parent'    => $param['parent_id'],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
        
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

                $businfo_data[] = [
                    'Bus_id' => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem' => rtrim($luxitem, ", "),
                    'rute' => $val['routes'][0]['route'],
                    'parent_busname' => $val['bus']['name'],
                    'available_seats' => $val['bus']['total_seats'],
                    'bus_fare' => \General::number_format($val['bus']['price'], 3),
                    'time' => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration' => $val['routes'][0]['duration'],
                ];
            }else{
                foreach($val['bus'] as $b){
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }

        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        
        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
    public static function search_bus_service_provider_a_old($param) {

        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];

        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];

        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();

        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }
        //dd($param['SP_id']);
        //\DB::enableQueryLog();
        //dd($param['date']);
//        $businfo = \App\Models\Businfo::select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
//                ->join('parentbus', 'parentbus.pid', '=', 'businfo.parbus')
//                ->whereNotIn("sp_id", $sp_ids)
//                ->where("Bus_status", 1)
//                ->where("businfo.SP_id", $param['SP_id'])
//                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
//                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
//                ->where('businfo.Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
//                ->where('businfo.Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));
        //dd(\DB::getQueryLog());
        $businfo = \App\Models\Businfo::active()
                ->select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                ->whereNotIn("sp_id", $sp_ids)
                ->where("Bus_status", 1)
                ->where("SP_id", $param['Bus_Admin_Parent'])
                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                ->where('Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                ->where('Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));

        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->get()->toArray();
        //dd($businfo);
        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }
        //dd($bus_id_valid);
        $bus_data = \App\Models\Businfo::with("ServiceProviderInfo", "ParentBus")
                        ->select("*")
                        ->whereIn("Bus_id", $bus_id_valid)
                        ->where("Bus_fromcity", $param['from_id'])
                        ->where("Bus_tocity", $param['to_id'])
                        ->groupBy("Bus_id")
                        ->skip(0)
                        ->take($param['len'])
                        ->get()->toArray();
        //dd($bus_data);
        $businfo_data = self::search_bus_formated($bus_data, $param);


        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->where("SP_id", $param['SP_id'])
                        ->get()->toArray();

        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operators,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    public static function search_bus_service_provider_b($param) {
//        dd($param);
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from'      => [
                                'name' => $from_city['name'],
                                'type' => $from_city['type'],
                            ],
            'to'        => [
                                'name' => $to_city['name'],
                                'type' => $to_city['type'],
                            ],
            'date'      => $param['date'],
            'nop'       => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
            'operator'  => 1,
            'parent'    => $param['parent_id'],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
        
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

                $businfo_data[] = [
                    'Bus_id' => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem' => rtrim($luxitem, ", "),
                    'rute' => $val['routes'][0]['route'],
                    'parent_busname' => $val['bus']['name'],
                    'available_seats' => $val['bus']['total_seats'],
                    'bus_fare' => \General::number_format($val['bus']['price'], 3),
                    'time' => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration' => $val['routes'][0]['duration'],
                ];
            }else{
                foreach($val['bus'] as $b){
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }

        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        
        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
        
    }
    
    public static function search_bus_service_provider_b_old($param) {
        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];

        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];

        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();
        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }
        $businfo = \App\Models\Businfo::active()
                ->select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                ->whereNotIn("sp_id", $sp_ids)
                ->where("Bus_status", 1)
                ->where("SP_id", $param['Bus_Admin_Parent'])
                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                ->where('Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                ->where('Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));

        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->get()->toArray();
        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }
        $bus_data = \App\Models\Businfo::with("ServiceProviderInfo", "ParentBus")
                        ->select("*")
                        ->whereIn("Bus_id", $bus_id_valid)
                        ->where("Bus_fromcity", $param['from_id'])
                        ->where("Bus_tocity", $param['to_id'])
                        ->groupBy("Bus_id")
                        ->skip(0)
                        ->take($param['len'])
                        ->get()->toArray();

        $businfo_data = self::search_bus_formated($bus_data, $param);


        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->where("SP_id", $param['SP_id'])
                        ->get()->toArray();

        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operators,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
    public static function book_ticket($param) {
        
        $token = \Request::header('AuthToken');
        $is_login = self::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_operator");
        }

        $operator_id = $logged_in_user['id'];
        
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
            $book_system = 0;
            $bus_details = \App\Models\Serviceprovider\Buses::where('id',$param['bus_id'])->with('sp')->first()->toArray();
            $board_point = \App\Models\Locality\Terminal::where('id',$param['from_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();
            $drop_point = \App\Models\Locality\Terminal::where('id',$param['to_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();

            $data = [
                'from_term' => $param['from_term_id'],
                'to_term'   => $param['to_term_id'],
                'nop'       => $param['total_seats'],
                'bus_id'    => $param['bus_id'],
                'date'      => $param['date'],
            ];
            $base_amount = \App\Models\Admin\BusRoutes::get_route_price($data);
        }else{
            $book_system = 1;
            $lorena = 0;
            if (stripos($param['bus_id'],'_LORENA') !== false) {
                $lorena = 1;
                $book_system = 2;
            }
            if($lorena){
                $rodaR = \App\Models\Lorena::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }else{
                $rodaR = \App\Models\Roda::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }
            
            $rbus = \App\Models\Roda::get_roda_bus($roda, $param['bus_id']);
            if($lorena){
                $allTr = \App\Models\Lorena::get_terminals($rbus, $param);
            }else{
                $allTr = \App\Models\Roda::get_terminals($rbus, $param);
            }
            
            if($allTr['flag'] == 0){
                return $allTr;
            }
            $ter = $allTr['data'];
            $board_point = [
                'name' => $ter['board_point'],
                'loc_citi' => ['name' => $rbus['from']],
                'loc_district' => ['name' => ''],
            ];
            $drop_point = [
                'name' => $ter['drop_point'],
                'loc_citi' => ['name' => $rbus['to']],
                'loc_district' => ['name' => ''],
            ];
//            dd($b_terminal,$d_terminal);
            if($lorena){
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            
            $bus_details = [
                'name'=>$rbus['bus'],
                'sp'=>$rsp,
            ];
            $base_amount = $ter['fare'];
        }
        

        if ($param['type'] == config("constant.SERVICE_PROVIDER_MAIN")) {
            $usertype   = 2; 
            $booked_by  = 2;
        } else if ($param['type'] == config("constant.SERVICE_PROVIDER_A") || $param['type'] == config("constant.SERVICE_PROVIDER_B")) {
            if ($param['type'] == config("constant.SERVICE_PROVIDER_A")) {
                $usertype   = 3;
                $booked_by  = 3;
            } else if ($param['type'] == config("constant.SERVICE_PROVIDER_B")) {
                $usertype   = 4;
                $booked_by  = 4;
            }
        }

        $param['passenger'] = json_decode($param['passenger'], true);

        $param = \Input::all();
        
        $b_time = explode(':', $param['boarding_time']);
        $d_time = explode(':', $param['departure_time']);
        
        $pick_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['boarding_time']));
        $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['departure_time']));

        if($d_time[0] < $b_time[0]){
            $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' +1 day '.$param['departure_time']));
        }
        
        $totalamount = $base_amount * $param['total_seats'];
        
        $data = [
            'ccode' => isset($param['coupon_code']) ? $param['coupon_code']:'',
            'busid' => $param['bus_id'],
        ];
        
        $hfval = 0;
        $discount = 0;
        $d = \App\Models\CouponCode::find_coupon($data);
        if($d['flag'] == 1){
            if($d['data'][0]['min_amount'] <= ($totalamount / 1000) && $d['data'][0]['max_amount'] >= ($totalamount / 1000)){
                $cc = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                if($d['data'][0]['type'] == 'P'){
                    $discount = (($totalamount / 1000) * $d['data'][0]['value'] / 100);
                    
                }
                $discount = ($discount * 1000);
            }
            else{
                $cc = \General::error_res('Coupon Code Not Applied.');
            }
        }
        else{
            $cc = \General::error_res('Invalid Coupon Code..!');
        } 
        
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($bus_details['sp']['id']);
        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( (($totalamount) * $handlefee[0]['handling_fee']) / 100 );
        }
        
        $total_amount = $totalamount - $discount + $hfval;

        $t = date("Y-m-d");
//        dd($logged_in_user);
        
        if ($booked_by == 3 && $total_amount > $logged_in_user['balance'])
            return \General::error_res("Silahkan Hubungi Call Center Untuk Menambah Saldo Anda");
        
        $data = [
            "sessionid"         => $param['sess_id'],
            "spname"            => $bus_details['sp']['first_name'].' '.$bus_details['sp']['last_name'],
            "sp_id"             => $bus_details['sp']['id'],
            "bus_id"            => $param['bus_id'],
            "user_id"           => $operator_id,
            "book_by"           => $booked_by,
            "booking_system"    => $book_system,
//            "status"            => config("constant.STATUS_BOOKED"),
            "payment_by"        => 0,
            "journey_date"      => $param['date'],
            "from_term_id"      => $param['from_term_id'],
            "to_term_id"        => $param['to_term_id'],
            "from_term"         => $board_point['name'],
            "to_term"           => $drop_point['name'],
            "from_city"         => $board_point['loc_citi']['name'],
            "to_city"           => $drop_point['loc_citi']['name'],
            "from_district"     => $board_point['loc_district']['name'],
            "to_district"       => $drop_point['loc_district']['name'],
            "pickup_date"       => $pick_date,
            "drop_date"         => $drop_date,
            "nos"               => $param['total_seats'],
            "total_amount"      => $total_amount,
            "coupon_code"       => isset($param['coupon_code']) ? $param['coupon_code'] : '',
            "coupon_discount"   => $discount,
            "handle_fee"        => $hfval,
            "base_amount"       => $base_amount,
            "booker_name"       => $param['user_firstname'],
            "booker_email"      => $param['user_email'],
            "booker_dob"        => date("Y-m-d H:i:s", strtotime($t." -".$param['user_age']." year")),
            "booker_no"         => $param['user_mobileno'],
            "payment"           => '',    
            "device"           => 'android',    
        ];
        
        $booking = \App\Models\Bookings::save_booking_details($data);
        
        $res = \General::success_res();
        
        $res['data']['ticket_id'] = $booking['data']['booking_id'];
        $res['data']['pt_id'] = -1;
        $res['data']['transfer_code'] = $booking['data']['bt_id'];
        $res['data']['cc_url'] = \URL::to('api/services/ticket-checkout/'.\Mycrypt::encrypt($booking['data']['id']));
//        $res['data']['payable_amount'] = $booking['data']['total_amount'] / 1000;
        $res['data']['payable_amount'] = \General::number_format($booking['data']['total_amount'],3);
        
        return $res;
    }
    
    public static function book_ticket_old($param) {
        $logged_in_user = app("logged_in_operator");
        $agent_id = 0;
        if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_MAIN")) {
            $pay_trans_id = "sp";
            $booked_by = 1;
            $bb = 'BO';
        } else if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_A") || $param['sp_type'] == config("constant.SERVICE_PROVIDER_B")) {

            $agent = \App\Models\SeatSeller\User::where("agent_email", $logged_in_user['SP_email'])->first();
            if (!is_null($agent))
                $agent_id = $agent->agent_id;

            if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_A")) {
                $pay_trans_id = "spa";
                $booked_by = 2;
                $bb = 'BA';
            } else if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_B")) {
                $pay_trans_id = "spb";
                $booked_by = 3;
                $bb = 'BB';
            }
        }
        
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
            $ex = 0;
            $bookingApi = 0;
        }else{
            $bookingApi = 1;
            $ex = 1;
            $busList = \App\Models\Services\Buslist::where('from_city',  $param['from_id'])->where('to_city',  $param['to_id'])->where('travelling_date',  $param['date'])->first()->toArray();
            if(count($busList)> 0){
                $list = unserialize($busList['bus_list']);
                
                $businfo = \App\Models\Services\General::getDetailApiBus($list, $param['bus_id']);
                $businfo['no_layout'] = 0;
                $businfo['SP_id'] = config('constant.SP_FOR_API');
            }
        }

        $user = \App\Models\User\User::where("user_email", $param['user_email'])->first();
        if (is_null($user)) {
            $user = new \App\Models\User\User();
            $user->user_email = $param['user_email'];
            $user->user_firstname = $param['user_firstname'];
            $user->user_mobileno = $param['user_mobileno'];
            $user->user_typeID = 4;
            $user->user_status = 1;
            $user->save();
        }

        $param['passenger'] = json_decode($param['passenger'], true);

        if($ex == 0){
        $businfo = \App\Models\Businfo::where("Bus_id", $param['bus_id'])->first();
        if (is_null($businfo)) {
            return \General::error_res("Bus Not found");
        }
        $businfo = $businfo->toArray();
        }
        $selected_seats = explode(",", $param['selected_seats']);
        $tot_seat = count($selected_seats);
        $time = rand(5, 99999) . time() . rand(5, 99999);
//        $ticket = '';
//        for ($x = 3; $x < 10; $x++) {
//            $ticket .= substr($time, $x, 1);
//        }

        $service_provider_name = \App\Models\ServiceproviderInfo::where("SP_id", $businfo['SP_id'])->pluck("SP_name");
        //$ticket_id = \General::getNewTicketNo($service_provider_name);
        $ticket_id = \General::ticketNo($service_provider_name,$bb);


        $seat_fare = 0;
        if($ex == 0)
            $bus_fare = \App\Models\BusFare::getBusFareByCity($businfo['Bus_fare'], $businfo['Bus_fromcity'], $businfo['Bus_tocity'], $param['from_id'], $param['to_id'], $param['bus_id'],$param['date']);
        else
            $bus_fare = $businfo['fare'];
        for ($i = 0; $i < $tot_seat; $i++) {
            if($ex == 0){
            $bus_structure = \App\Models\BusStructure::where("bus_id", $param['bus_id'])->where("seat_no", $selected_seats[$i])->first()->toArray();
            if ($bus_structure['vip']) {
                $seat_fare += $businfo['vip_fare'];
            } else {
                $seat_fare += $bus_fare;
            }
            }else{
                $seat_fare += $bus_fare;
            }
        }
        $total_seat_payment = $seat_fare;

        $settings = \DB::table("generalsettings")->take(1)->skip(0)->get();
        $handling_fee = isset($settings[0]) ? $settings[0]['handlingfee'] : 0;
        $total_handling_fee = ($tot_seat * $handling_fee);

        $discount = -1;
        $total_discount = -1;
        if (isset($param['coupon_code']) && $param['coupon_code'] != "") {

            $coupon_data = [
                'total_seat_payment' => $total_seat_payment,
                'coupon_code' => $param['coupon_code'],
                'date' => $param['date'],
                'total_seat_count' => $tot_seat,
                'bus_id' => $param['bus_id'],
                'SP_id' => $businfo['SP_id'],
                'ticket_id' => $ticket_id,
                'booked_by' => $booked_by,
                'agent_id' => $agent_id,
            ];
            $coupon = \App\Models\BookingInfo::apply_coupon_code($coupon_data);
            if ($coupon['flag'] == 1) {
                $percent = $coupon['data']['discount_percent'];
                $total_discount = $coupon['data']['total_discounted_price'];
                $discount = $coupon['data']['discount_amount'];
            }
        }

        $auto_id = 0;
        if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_A")) {
            $final_pay = $total_discount > -1 ? $total_discount : ($total_seat_payment + $total_handling_fee);
            if ($final_pay > $agent->agnt_amt)
                return \General::error_res("You don't have sufficiant balance");
        }
        
        $bs = 0;
                        if($ex ==1){

                            $busDetail = $businfo;
                            $fare = $busDetail['fare'] * $tot_seat;
                            $bus = $busDetail['bus'];
                            $Bus_id = $param['bus_id'];
                            $booking = \App\Models\Services\General::makeRodaBooking($busDetail, $param['user_firstname'], $param['user_mobileno'],$tot_seat,$param);
                //           pr($booking);

                            if($booking['err_code'] != 0){
                                $apiBusInfo = new \App\Models\Services\ApiBusInfo();
                                $apiBusInfo->ticket_id = $ticket_id;
                                $apiBusInfo->route_code = $Bus_id;
                                $apiBusInfo->bus_name = $busDetail['bus'];
                                $apiBusInfo->pay_status = 3;
                                $apiBusInfo->book_status = 0;
                                $apiBusInfo->routes = $busDetail['route'];
                                $apiBusInfo->booking_response = json_encode($booking);
                                $apiBusInfo->save();
                                
                                return \General::error_res('something went wrong please try again to book ticket.');
                                
                            }else{
                                $book_code = $booking['book_code'];
                                $params = 'app=information&action=get_book_info&book_code='.$book_code;
                                $bookinInfo = \App\Models\Services\General::makeRodaRequest($params);
                                if($bookinInfo['err_code'] == 0){
                                    $passlist = $bookinInfo['pax_list'];
                //                    pr($passlist);
                //                    $lables = '';
                                    $uniqIds = array();
                                    foreach($passlist as $k=>$p){
                                        $uniqIds[$k]= $p[6];
                                        $route_code = explode('-', $busDetail['route_code']);
                                        $params = 'app=transaction&action=take_seat&book_code='.$book_code.'&pax_first_name='.urlencode($p[0]).'&trip_date='.$busDetail['trip_date'].'&route_code='.$route_code[1].'&org='.$busDetail['main_org_code'].'&des='.$busDetail['main_dest_code'].'&pax_seat='.$selected_seats[$k].'&old_seat='.$p[7];
                                        if($selected_seats[$k] != $p[7]){
                                            $seatChange = \App\Models\Services\General::makeRodaRequest($params);
                    //                        pr($seatChange);
                                            if($seatChange['err_code'] != 0){
                                                $apiBusInfo = new \App\Models\Services\ApiBusInfo();
                                                $apiBusInfo->ticket_id = $ticket_id;
                                                $apiBusInfo->route_code = $Bus_id;
                                                $apiBusInfo->bus_name = $busDetail['bus'];
                                                $apiBusInfo->pay_status = 3;
                                                $apiBusInfo->book_status = 0;
                                                $apiBusInfo->routes = $busDetail['route'];
                                                $apiBusInfo->booking_response = json_encode($booking);
                                                $apiBusInfo->save();

                                                return \General::error_res('The selected seat is not avilable now.please try again with another seat.');

                                            }else{

                                            }
                                        }
                                    }
                                }else{
                                            $apiBusInfo = new \App\Models\Services\ApiBusInfo();
                                            $apiBusInfo->ticket_id = $ticket_id;
                                            $apiBusInfo->route_code = $Bus_id;
                                            $apiBusInfo->bus_name = $busDetail['bus'];
                                            $apiBusInfo->pay_status = 3;
                                            $apiBusInfo->book_status = 0;
                                            $apiBusInfo->routes = $busDetail['route'];
                                            $apiBusInfo->booking_response = json_encode($booking);
                                            $apiBusInfo->save();

                                            return \General::error_res('Something is wrong please try agian.');
                                     
                                }
                                $apiBusInfo = new \App\Models\Services\ApiBusInfo();
                                $apiBusInfo->ticket_id = $book_code;
                                $apiBusInfo->book_code = $book_code;
                                $apiBusInfo->route_code = $Bus_id;
                                $apiBusInfo->bus_name = $busDetail['bus'];
                                $apiBusInfo->pay_status = 3;
                                $apiBusInfo->book_status = 3;
                                $apiBusInfo->routes = $busDetail['route'];
                                $apiBusInfo->booking_response = json_encode($booking);
                                $apiBusInfo->save();
                               
                                $amount = $fare;
                                $params = 'app=transaction&action=payment&book_code='.$book_code.'&amount='.$amount;
                                $paymentApi = \App\Models\Services\General::makeRodaRequest($params);
                        //        pr($paymentApi);

                                if($paymentApi['err_code'] != 0){
                                    
                                    $upd = \App\Models\Services\ApiBusInfo::where('ticket_id',$book_code)->first();
                                    $upd->pay_status = 0;
                                    $upd->payment_response =  json_encode($paymentApi);
                                    $upd->save();
                                    return \General::error_res('payment to roda failed.');
                                    
                                }else{
                                    $bs = 1;
                                    $upd = \App\Models\Services\ApiBusInfo::where('ticket_id',$book_code)->first();
                                    $upd->pay_status = 1;
                                    $upd->book_status = 1;
                                    $upd->payment_response =  json_encode($paymentApi);
                                    $upd->save();
                                    
                                    $param = 'app=information&action=get_book_info&book_code='.$book_code;
                                    $bookinInfo = \App\Models\Services\General::makeRodaRequest($param);
                                    if($bookinInfo['err_code'] == 0){
                                        $passlist = $bookinInfo['pax_list'];
                                        $uniqIds = array();
                                        foreach($passlist as $k=>$p){
                                            $uniqIds[$k] = $p[6];
                                        }
                                    }
                                }
                            }
                        }else{
                            $bs = 1;
                        }

        for ($i = 0; $i < $tot_seat; $i++) {

            $ticket = '';
            for ($x = 3; $x < 8; $x++) {
                $ticket .= substr($time, $x, 1);
            }
            //$uniq_id = date("d") . strtoupper(substr($logged_in_user['SP_name'], 0, 10)) . $ticket . $i;
//            $uniq_id = \General::getNewUniqueId($logged_in_user['SP_name'],$i);
            $uniq_id = \General::uniqueNo($service_provider_name,$param['passenger'][$i]['firstname']);
            if($ex == 1){
                $uniq_id = $uniqIds[$i] ? $uniqIds[$i] : $uniq_id;
                $ticket_id = $book_code;
            }
            $booked_route = $param['from_id'] . "," . $param['to_id'];


            $bookinginfo = \App\Models\BookingInfo::where("Bus_id", $param['bus_id'])
                    ->where("travelling_date", $param['date'])
                    ->where("SeatNo", $selected_seats[$i])
                    ->where("booked_route", $booked_route)
                    ->where("session_id", $param['sess_id'])
                    ->first();

//            \Log::info("Operator Book ticket");
//            \Log::info(json_encode($param));
//            \Log::info("Route : ".$booked_route);
            if (is_null($bookinginfo)) {
                return \General::error_res("Oops! One or more seats you have selected are already booked. Please try again");
            }

            $bookinginfo->SP_id = $businfo['SP_id'];
            $bookinginfo->unique_id = $uniq_id;
            $bookinginfo->booking_type = 1; // static don't know
            $bookinginfo->booked_date = \DB::raw("NOW()");
            $bookinginfo->Ticket_ID = $ticket_id;
            $bookinginfo->userid = $user->user_id;
            $bookinginfo->usertype = $logged_in_user['user_typeID'];
            $bookinginfo->cancelledStatus = 0;
            $bookinginfo->Blocked = 1;
            $bookinginfo->BlockedBy = 0;
            $bookinginfo->BlockedType = 0;
            $bookinginfo->booking_amt = $bus_fare;
            $bookinginfo->pay_status = 1;
            $bookinginfo->pay_trans_id = $pay_trans_id;
            $bookinginfo->agent_id = $agent_id;
            $bookinginfo->booked_by = $booked_by;
            $bookinginfo->save();


            if (!$i)
                $auto_id = $bookinginfo->auto_id;


            $passengerinfo = new \App\Models\PassengerInfo();
            $passengerinfo->Ticket_ID = $ticket_id;
            $passengerinfo->unique_id = $uniq_id;
            $passengerinfo->passenger_Name = $param['passenger'][$i]['firstname'];
            $passengerinfo->passenger_seatNo = $selected_seats[$i];
            $passengerinfo->passenger_seatlabel = $selected_seats[$i];
            $passengerinfo->passenger_Gender = $param['passenger'][$i]['gender'];
            $passengerinfo->passenger_Age = $param['passenger'][$i]['age'];
            $passengerinfo->bus_fare = $total_discount > -1 ? $total_discount : ($total_seat_payment);
            $passengerinfo->save();
        }



        $booker_detail = new \App\Models\BookerDetails();
        $booker_detail->user_id = isset($logged_in_user['user_id']) ? $logged_in_user['user_id'] : 0;
        $booker_detail->Ticket_ID = $ticket_id;
        $booker_detail->unique_id = $uniq_id;
        $booker_detail->Booker_name = $param['user_firstname'];
        $booker_detail->Booker_email = $param['user_email'];
        $booker_detail->Booker_mobile = $param['user_mobileno'];
        $booker_detail->booking_total = $total_seat_payment;
        $booker_detail->Booker_coup = isset($param['coupon_code']) && isset($coupon['flag']) && $coupon['flag'] == 1 ? $param['coupon_code'] : "";
        $booker_detail->Booker_discount = $discount > -1 ? $discount : 0;
        $booker_detail->booking_amt = $total_discount > -1 ? $total_discount : ($total_seat_payment);
        $booker_detail->payment_type = 0; // still not decided (By Bank or By CC)
        $booker_detail->boarding_point = $param['boarding_time'];
        $booker_detail->dropping_point = $param['departure_time'];
        $booker_detail->save();

        $pt = new \App\Models\PaymentTransaction();
        $pt->ticket_id = $ticket_id;
        $pt->Service_provider_id = $businfo['SP_id'];
        $pt->bus_id = $param['bus_id'];
        $pt->pay_tele = $param['user_mobileno'];
        $pt->pay_email = $param['user_email'];
        $pt->pay_fare = $total_seat_payment;
        $pt->pay_coupon = isset($param['coupon_code']) ? $param['coupon_code'] : "";
        $pt->pay_discount = $discount > -1 ? $discount : 0;
        $pt->pay_amount = $total_discount > -1 ? $total_discount : ($total_seat_payment);
        $pt->pay_st_time = date("Y-m-d H:i:s");
        $pt->pay_ip = \Request::ip();
        $pt->ticket_count = $tot_seat;
        $pt->pay_trans_id = "sp";
        $pt->save();

        $res = \General::success_res();
        $transfer_code = \App\Models\BookingInfo::get_transfer_code($param['user_firstname'], $auto_id);
        $all_bank = \App\Models\Services\General::get_all_bank();
        $payable_amount = $total_discount > -1 ? $total_discount : ($total_seat_payment);
        $payable_amount = \General::number_format($payable_amount, 3);
        if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_A")) {
            $agent->agnt_amt = $agent->agnt_amt - $final_pay;
            $agent->save();
            
            $spagent = self::where('SP_email',$logged_in_user['SP_email'])->first();
            $spagent->agnt_amt = $spagent->agnt_amt - $final_pay;
            $spagent->save();
        }
        $fare = $bus_fare;
        
        if ($param['sp_type'] == config("constant.SERVICE_PROVIDER_A") || $param['sp_type'] == config("constant.SERVICE_PROVIDER_B")) {
            self::serviceProviderCommission($logged_in_user['SP_id'], $ticket_id, $param['bus_id'], $fare, $tot_seat);
        }
        \App\Models\SeatSeller\User::adminCommission($businfo['SP_id'], $ticket_id, $param['bus_id'], $fare, $tot_seat, $booked_by);
        
        $res['data'] = ["ticket_id" => $ticket_id, "pt_id" => $pt->pay_id, "transfer_code" => $transfer_code, "bank_detail" => $all_bank, "cc_url" => \URL::to("/payment/process/" . $pt->pay_id), "payable_amount" => $payable_amount];
        \App\Models\BookingInfo::fire_success_event($ticket_id);
        return $res;
    }
   
    public static function datewise_report($param) {
//        dd($param);
        $code = [
            'ss' => [
                'Main'  => [5,6,7],
                'A'     => 6,
                'B'     => 7,
            ],
            'sp' => [
                'Main'  => [2,3,4],
                'A' => 3,
                'B' => 4,
            ],
        ];
        $total_commission = 0;
        if ($param['type'] == 'A' ||$param['type'] == 'B') {
            $book_by = $code[$param['agent']][$param['type']];

            $count_q = \App\Models\Bookings::where('status','=',1)
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            
            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->where('book_by',$book_by)
                                    ->where('user_id',$param['id']);
            
            if($param['agent'] == 'sp'){
                $total_commission = \App\Models\SpCommission::where('sp_id',$param['id']);
            }

            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');
                if($param['agent'] == 'sp'){
                    $total_commission = $total_commission->whereDate('created_at','=',$from_date)
                                            ->sum('com_amount');
                }
            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
                if($param['agent'] == 'sp'){
                    $total_commission = $total_commission->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('com_amount');
                }
            }
        }
        else{
            $book_by = $code[$param['agent']][$param['type']];

            $count_q = \App\Models\Bookings::where('status','=',1)->with('userSs','userSp')
                                                ->whereIn('book_by',$book_by)
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSs',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                })
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSp',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                });

            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->whereIn('book_by',$book_by)
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSs',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    })
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSp',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    });

            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');

            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
            }
        }
        $total_amount = \General::number_format($total_amt, 3);
        $total_commission = \General::number_format($total_commission, 3);
        $bank = \App\Models\DepositeBankDetails::get_banks_for_api();
        $res = \General::success_res();
//        $res['data'] = ["ticket_count" => count($tickets), "total_amount" => $total_amount];
        $res['data'] = ["ticket_count" => $count, "total_amount" => $total_amount,'total_commission'=>$total_commission,'bank_detail'=>$bank];
        return $res;
    }
    
    public static function datewise_detail_report($param) {
//        dd($param);
        $code = [
            'ss' => [
                'Main'  => [5,6,7],
                'A'     => 6,
                'B'     => 7,
            ],
            'sp' => [
                'Main'  => [2,3,4],
                'A' => 3,
                'B' => 4,
            ],
        ];
        $deposits = [];
        if ($param['type'] == 'A' ||$param['type'] == 'B') {
            $book_by = $code[$param['agent']][$param['type']];
            $tickets = \App\Models\Bookings::where('status','=',1)->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            $count_q = \App\Models\Bookings::where('status','=',1)
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            
            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->where('book_by',$book_by)
                                    ->where('user_id',$param['id']);
            
            if($param['agent'] == 'sp'){
                $deposits = ServiceProviderDeposit::active()->where('sp_id',$param['id']);
            }
            
            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $tickets = $tickets->whereDate('created_at','=',$from_date);
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');
                
                if($param['agent'] == 'sp'){
                    $deposits = $deposits->whereDate('created_at','=',$from_date)->get()->toArray();
                }
            }
            else{
                $from_date = date('Y-m-d', strtotime($param['from_date']));
//                $from_date = \Carbon::createFromFormat('Y-m-d', $param['from_date'])->toDateString();
//                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d', strtotime($param['to_date']));
                
//                $to_date = \Carbon::createFromFormat('Y-m-d', $param['to_date'])->toDateString();
//                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
//                dd($from_date,$to_date);
//                $tickets = $tickets->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date);
                $tickets = $tickets->where(\DB::raw('date(created_at)'),'>=',$from_date)->where(\DB::raw('date(created_at)'),'<=',$to_date);
//                dd($tickets,$from_date,$to_date);
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
                if($param['agent'] == 'sp'){
                    $deposits = $deposits->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)->get()->toArray();
                }
            }
        }
        else{
            $book_by = $code[$param['agent']][$param['type']];

//            $tickets = $count_q = \App\Models\Bookings::where('status','=',1)->with('userSs','userSp')
            $tickets = $count_q = \App\Models\Bookings::where('status','=',1)->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord','userSs','userSp'])
                                                ->whereIn('book_by',$book_by)
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSs',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                })
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSp',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                });

            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->whereIn('book_by',$book_by)
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSs',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    })
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSp',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    });

            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $tickets = $count_q->whereDate('created_at','=',$from_date);
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');

            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $tickets = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date);
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
            }
        }
        
        if(isset($param['name']) && $param['name'] != ''){
            $tickets = $tickets->where('booker_name',$param['name']);
        }
        if(isset($param['mobile']) && $param['mobile'] != ''){
            $tickets = $tickets->where('booker_mo',$param['mobile']);
        }
        if(isset($param['email']) && $param['email'] != ''){
            $tickets = $tickets->where('booker_email',$param['email']);
        }
        if(isset($param['dob']) && $param['dob'] != ''){
            $dob = date('Y-m-d H:i:s', strtotime($param['dob']));
            $tickets = $tickets->where('booker_dob',$dob);
        }
        
        $tickets = $tickets->get()->toArray();
        $ticket_data = [];
        $total_amount = 0;
        
        
        foreach ($tickets as $key => $val) {
            $service_provider = $val['sp'];
            if (!is_null($service_provider)) {
                $service_provider_name = $service_provider['first_name'].' '.$service_provider['last_name'];
            }
            if($val['booking_system'] > 0){
//                $busList = \App\Models\ApiBusInfo::where('booking_id',$val['booking_id'])->first();
                if($val['booking_system'] == 2){
                    $busList = \App\Models\Lorena::get_bus_info($val['booking_id']);
                }else{
                    $busList = ApiBusInfo::where('booking_id',$val['booking_id'])->first();
                }
                if($busList){
                        $params = [
                            'from_term_id' => $val['booking_seats'][0]['from_terminal'],
                            'to_term_id' => $val['booking_seats'][0]['to_terminal'],
                        ];
                        $list = json_decode($busList['bus_detail'],true);
                        $term = \App\Models\Roda::get_terminals($list, $params);
    //                    DD($term);
                        $val['from_terminal'] = [
                            'name' =>$term['data']['board_point'],
                            'address' =>$term['data']['board_point'].','.$list['from'],
                            'loc_citi' =>[
                                'name'=>$list['from'],
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>$term['data']['drop_point'],
                            'address' =>$term['data']['drop_point'].','.$list['to'],
                            'loc_citi' =>[
                                'name'=>$list['to'],

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = $list['route'];
                        
                }else{
                    $val['from_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = '';
                }
                
            }else{
                $route_detail = \App\Models\Admin\BusRoutes::with("bus.sp")
                            ->where("bus_id", $val['bus_id'])
                            ->where("from_terminal_id", $val['from_terminal_id'])
                            ->where("to_terminal_id", $val['to_terminal_id'])
                            ->first()->toArray();

                $booked_route = $route_detail['route'];
            }
        
            
            $from_city = $val['from_terminal']['loc_citi'];
            $to_city = $val['to_terminal']['loc_citi'];
            $boading_address = $val['from_terminal']['name'];
            $dropping_address = $val['to_terminal']['name'];
            $boading_full_address = $val['from_terminal']['address'];
            $dropping_full_address = $val['to_terminal']['address'];
            $handling_fee = $val['handling_fee'];
            $unit_handling_fee = $val['handling_fee'] / $val['nos'];
            $pay_status = $val['status'];
            $journey_completed = 0;
            $current_date = date("Y-m-d H:i:s");
            $travelling_date = $val['journey_date'];
            if (\App\Models\Services\General::date_dif($current_date, $travelling_date, "i") < 1) {
                $journey_completed = 1;
            }
            $transfer_code = $val['bt_id'];
            $data = [
                'ticket_id' => $val['booking_id'],
                'coupon_code' => $val['coupon_code'],
                'date' => date("D, j F, Y", strtotime($val['journey_date'])),
                "from_city" => $val['from_terminal']['loc_citi']['name'],
                "to_city" => $val['to_terminal']['loc_citi']['name'],
                "boading_address" => $boading_address,
                "boading_full_address" => is_null($boading_full_address) ? "" : $boading_full_address,
                "drop_address" => $dropping_address,
                "drop_full_address" => is_null($dropping_full_address) ? "" : $dropping_full_address,
                "user_name" => $val['booker_name'],
                "user_email" => $val['booker_email'],
                "user_mobile" => $val['booker_mo'],
                "total_amt" => \General::number_format(($val['total_amount']), 3),
                "ticket_count" => $val['nos'],
                "payment_mode" => $val['payment_record']['payment_by'] == 1 || $val['payment_record']['payment_by'] == 0 ? "CASH" : "CC",
                "pay_status" => $pay_status,
                "service_provider_name" => $service_provider_name,
                "book_date" => date('Y-m-d',strtotime($val['created_at'])),
            ];
            $ticket_data[] = $data;
        }
        $dep = [];
        if(count($deposits > 0)){
            foreach($deposits as $k=>$v){
                $dep[$k] = [
                    'date' => date("D, j F, Y", strtotime($v['created_at'])),
                    'deposite_date' => $v['created_at'],
                    'amount' => $v['amount'],
                ];
            }
        }
        $res = \General::success_res();
        $res['data'] = $ticket_data;
        $res['deposite'] = $dep;
        return $res;
    }
    
    public static function help($param) {
        \Mail::raw($param['message'], function ($message) use ($param) {
            $message->from($param['email'], $param['name']);
            $message->to("support@bustiket.com")->subject("bantuan operator");
        });
        return \General::success_res("We received your request. We will contact you soon");
    }
    
    public static function get_total_commission_of_sp($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\SpCommission::where(function($q)use($param){
                    $q->whereIn('sp_id',$param['child'])  ;
                  })->whereDate('created_at','=',$today)->sum('com_amount');
            $mcount = \App\Models\SpCommission::where(function($q)use($param){
                    $q->whereIn('sp_id',$param['child']);
                  })->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('com_amount');
            $ycount = \App\Models\SpCommission::where(function($q)use($param){
                    $q->whereIn('sp_id',$param['child']) ;
                  })->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('com_amount');
        }else{
            $tdcount = \App\Models\AdminCommission::where('sp_id',$param['id'])->whereIn('booked_by',$param['book_by'])->whereDate('created_at','=',$today)->sum('com_amount');
            $mcount = \App\Models\AdminCommission::where('sp_id',$param['id'])->whereIn('booked_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('com_amount');
            $ycount = \App\Models\AdminCommission::where('sp_id',$param['id'])->whereIn('booked_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('com_amount');
        }
        
//        dd($tdcount,$mcount,$ycount);
        $allD = [
            [
                'label'=>'Today',
                'value'=>ceil($tdcount),
            ],
            [
                'label'=>'Month',
                'value'=>ceil($mcount),
            ],
            [
                'label'=>'Year',
                'value'=>ceil($ycount),
            ],
        ];
        
        $data = [
            [
                'key'=>'Commission in rupies',
                'values'=>$allD,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    public static function  get_child_by_type($param){
        $bb = $param['book_by'];
        $type = $bb == 3 ? 'A' : 'B';
        $start = 0;
        $len = 10;
        if(isset($param['child'])){
            
            $ad = \DB::table('sp')->where('type',$type)
                    ->select(\DB::raw('concat(first_name," ",last_name) as name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` = '.$param['book_by'].' and `status` = 1 and user_id = sp.id) as total'))
                    ->where('parent_id',$param['id'])
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;
        }else{
            
            /*$ad = \DB::table('sp')
                    ->select(\DB::raw('concat(first_name," ",last_name) as name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` in( '.$param['book_by'].') and `status` = 1 and user_id = sp.id) as total'))
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;*/
            $ad = \DB::table('sp')
                    ->select(\DB::raw('concat(first_name," ",last_name) as name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` in( '.$param['book_by'].') and `status` = 1 and user_id = sp.id) as total'))
                    ->orderBy('total','desc')->skip($start)->take($len)
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;
        }
//        dd($ad);
        return \App\Models\General::prepare_data_for_individual_chart($ad);
    }
}