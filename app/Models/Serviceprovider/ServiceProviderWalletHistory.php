<?php 

namespace App\Models\Serviceprovider;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderWalletHistory extends Model {

    
    public $table = 'sp_wallet_history';
    protected $fillable = array('sp_id','amount','comment','meta');
//    protected $appends = array('role');
    public $timestamps=true;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function serviceProvider(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    
    public static function get_wallet_history($param){
        
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::whereHas('serviceProvider', function ($query) use($param) {
                            $query->where('first_name','like','%'.$param['search'].'%');
                        })->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::with('serviceProvider');
        if(isset($param['ss_type']) && $param['ss_type'] != ''){
            $spdata=$spdata->whereHas('serviceProvider', function ($query)  use($param)  {
                                        $query->where('type',$param['ss_type']);
                                    });
        }
        if(isset($param['status']) && $param['status'] != ''){
            $spdata = $spdata->where('status',$param['status']);
        }
        $spdata=$spdata->skip($start)->take($len)->orderBy('id','desc')->get()->toArray();
        
        
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::whereHas('serviceProvider', function ($query)  use($param)  {
                                        $query->where('first_name','like','%'.$param['search'].'%');
                                    })->with('serviceProvider')->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
}
