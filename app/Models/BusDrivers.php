<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BusDrivers extends Model {
    protected $fillable = [
        'bus_id', 'driver_id'
    ];
    protected $table = 'bus_drivers';
    protected $hidden = [];
    public $timestamps=false;
    
    public function buses(){
        return $this->hasOne('App\Models\Serviceprovider\Buses','id','bus_id');
    }
    public function driver(){
        return $this->hasOne('App\Models\Drivers','id','driver_id');
    }
    
}
