<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class CouponCode extends Model {
    protected $fillable = [
        'status', 'code', 'start_date','end_date','type','value','min_amount','max_amount','bus_id'
    ];
    protected $table = 'couponcode';
    protected $hidden = [];
    public $timestamps=true;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function busDetails(){
        return $this->hasOne('App\Models\Serviceprovider\Buses','id','bus_id');
    }
    
    public static function find_coupon($param){
//        dd($param);
        $d = self::active()->where('code',$param['ccode']);

        $data = $d->get()->toArray();

        if(count($data) > 0){
            $data1 = $d->whereDate('start_date', '<=', Carbon::today()->toDateString())->whereDate('end_date', '>=', Carbon::today()->toDateString());
            if(($data1->first()) == null){
                $res = \General::error_res('Coupon Code Expired.');
                $res['data'] = $data1->first();
            }
            
            $data2 = self::active()->where('code',$param['ccode'])->whereDate('start_date', '<=', Carbon::today()->toDateString())->whereDate('end_date', '>=', Carbon::today()->toDateString())->where('bus_id',$param['busid'])->get()->toArray();
            
            if(count($data2) <= 0){
                // check bus_id == null
                $data3 = self::active()->where('code',$param['ccode'])->whereDate('start_date', '<=', Carbon::today()->toDateString())->whereDate('end_date', '>=', Carbon::today()->toDateString())->where('bus_id',null)->get()->toArray();

                if(count($data3) <= 0){
                    $res = \General::error_res('Invalid Coupon Code.');
                    $res['data'] = $data;
                }
                else{
                    $res = \General::success_res('Code Applied.');
                    $res['data'] = $data3;
                }
            }
            else{
                $res = \General::success_res('Code Applied.');
                $res['data'] = $data2;
            }
        }
        else{
            $res = \General::error_res('Invalid Coupon Code.');
            $res['data'] = $data;
        }
//        dd($res);
        return $res;
    }
    
    public static function get_coupon_code($param){
        $d = self::orderBy('id','desc');
        if(isset($param['code'])){
            $d = $d->where('code','like','%'.$param['code'].'%');
        }
        if(isset($param['id'])){
            $d = $d->where('id',$param['id']);
        }
        if(isset($param['bus_id'])){
            $d = $d->where('bus_id',$param['bus_id']);
        }
        $d = $d->get();
        
        return $d;
    }
    
    public static function get_driver_filter($param){
        $count=self::OrderBY('id','desc');
        $count = $count->count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('code','like','%'.$param['search'].'%');
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::orderBy('id','desc');
        $spdata = $spdata->skip($start)->take($len)->get()->toArray();
        
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::where('code','like','%'.$param['search'].'%')->orderBy('id','desc');
            $spdata = $spdata->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function add_new_coupon_code($param){
//        dd($param['bus_id']);
//        dd(isset($param['bus_id']) && $param['bus_id'] != '');
        
        $d = new self;
        $d->status = isset($param['cstatus']) ? 1 : 0;
        $d->code = $param['ccode'];
        $d->start_date = $param['csdate'];
        $d->end_date = $param['cedate'];
        $d->type = $param['ctype'];
        $d->value = $param['cvalue'];
        $d->min_amount = $param['cminvalue'];
        $d->max_amount = $param['cmaxvalue'];
        $d->bus_id = isset($param['bus_id']) && $param['bus_id'] != '' ? $param['bus_id'] : null;
        $d->save();
      
        $res = \General::success_res();
        return $res;
    }
    public static function edit_coupon_code($param){
//        dd($param);
        $d = self::where('id',$param['ccid'])->first();
        if(is_null($d)){
            return \General::error_res('no driver found');
        }
        $d->status = isset($param['cstatus']) ? 1 : 0;
        $d->code = $param['ccode'];
        $d->start_date = $param['csdate'];
        $d->end_date = $param['cedate'];
        $d->type = $param['ctype'];
        $d->value = $param['cvalue'];
        $d->min_amount = $param['cminvalue'];
        $d->max_amount = $param['cmaxvalue'];
        $d->bus_id = isset($param['bus_id']) && $param['bus_id'] != ''? $param['bus_id'] : null;
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    
    public static function delete_coupon_code($param){
        
        self::where('id',$param['id'])->delete();
        return \General::success_res('coupon code deleted successfully !!');
    }
    
    public static function get_coupon_filter($param){
        

        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('code','like','%'.$param['search'].'%');
        }
        
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            
            $count=$count->whereHas('busDetails',function($q)use($param){
                    $q->where('sp_id',$param['sp_id']);
                });  
        }
        
        
        $count = $count->count();
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $coupondata = self::with('busDetails')->orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $coupondata=$coupondata->where('code','like','%'.$param['search'].'%');
        }
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            $coupondata=$coupondata->whereHas('busDetails',function($q)use($param){
                    $q->where('sp_id',$param['sp_id']);
            });
        }
        
        $coupondata = $coupondata->skip($start)->take($len)->get()->toArray();
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$coupondata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function active_not_expr_coupon_code(){

        $totalActiveCoupon = self::where('end_date','>=',date('Y-m-d'))->update(['status' => '1']);
        
        $res = \General::success_res('All Coupon Code Are Active which date is greater than today date');
        return $res;
    }
    
    
        public static function import_add_new_coupon_code($param){
        
        $d = new self;
        $d->status = isset($param['cstatus']) ? 1 : 0;
        $d->code = $param['ccode'];
        $d->start_date = $param['csdate'];
        $d->end_date = $param['cedate'];
        $d->type = $param['ctype'];
        $d->value = $param['cvalue'];
        $d->min_amount = $param['cminvalue'];
        $d->max_amount = $param['cmaxvalue'];
        $d->bus_id = isset($param['bus_id']) && $param['bus_id'] != '' ? $param['bus_id'] : null;
        $d->save();
        
        $res = \General::success_res();
        $res['data']=$d['id'];
        return $res;
    }
    
    public static function import_edit_coupon_code($param){
//        dd($param);
        $d = self::where('id',$param['ccid'])->first();
        if(is_null($d)){
            return \General::error_res('no driver found');
        }
        $d->status = isset($param['cstatus']) ? 1 : 0;
        $d->code = $param['ccode'];
        $d->start_date = $param['csdate'];
        $d->end_date = $param['cedate'];
        $d->type = $param['ctype'];
        $d->value = $param['cvalue'];
        $d->min_amount = $param['cminvalue'];
        $d->max_amount = $param['cmaxvalue'];
        $d->bus_id = isset($param['bus_id']) && $param['bus_id'] != ''? $param['bus_id'] : null;
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    
}
