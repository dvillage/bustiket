<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PariwisataOrderBus extends Model {
    protected $fillable = [
        'order_id', 'tour_bus_id'
    ];
    protected $table = 'tour_bus_order_buses';
    protected $hidden = [];
    public $timestamps=false;
    
    public function bus(){
        return $this->hasOne('App\Models\Admin\Pariwisata','id','tour_bus_id');
    }
    
}
