<?php

namespace App\Models\Locality;
use Illuminate\Database\Eloquent\Model as Eloquent;

class District extends Eloquent {
    
    public $table = 'districts';
    protected $hidden = [];
    public $timestamps=false;
    
    public function locProvince(){
        return $this->hasOne('App\Models\Locality\Province','id','province_id');
    }
    public function locCiti(){
        return $this->hasOne('App\Models\Locality\Citi','id','city_id');
    }
    public function locTerminal(){
        return $this->hasMany('App\Models\Locality\Terminal','district_id','id');
    }
    
    
    public static function get_alldistrict(){
        $dlist=self::get(['id','name'])->toArray();
        return $dlist;
    }
    public static function get_locDist(){

        $dlist = self::get(['id','name','city_id'])->toArray();
        $res = \General::success_res();
        $res['data'] = $dlist;
        return $res;
    }
    
    public static function get_distfilter($param){
//        dd($param);
        $clist=self::where('city_id',$param['cityid'])->get(['id','name'])->toArray();
        return $clist;
    }
    
    public static function dist_exist($param,$upd = 0){
//        dd($param);
        
        $c = self::where('name',$param['name'])->where('province_id',$param['pro']);
        if($upd){
            $c= $c->where('id','!=',$param['id']);
        }
        $c = $c->first();
        if(!is_null($c)){
            return 0;
        }
        return 1;
    }
    
    public static function add_district($param){
//        dd($param);
        
        $d = new District;
        $d->name=trim($param['name']);
        $d->province_id = $param['pro'];
        $d->city_id = $param['city'];
        $d->save();
        
        \Event::fire(new \App\Events\SetLocality($d->name, 'Insert', 'District', $d->id));
        
        return $d;
    }
    
    public static function edit_district($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        $d->name = trim($param['name']);
        $d->province_id = $param['pro'];
        $d->city_id = $param['city'];
        $d->save();
        
        \Event::fire(new \App\Events\SetLocality($d->name, 'Update', 'District', $d->id));
        \Event::fire(new \App\Events\UpdateRoutes($d, 'D'));
        return $d;
    }
    
    public static function get_district($param){
//        dd($param);
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::with(["locProvince","locCiti"])->skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
//            $crnt_page=1;
//            $start=($crnt_page-1)*$len;
            $district=self::where('name','like','%'.$param['search'].'%')->with(["locProvince","locCiti"])->skip($start)->take($len)->get()->toArray();
        }

//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_district($param){
        $res=self::where('id',$param['id'])->delete();
        
        \Event::fire(new \App\Events\SetLocality('', 'Delete', 'District', $param['id']));

        return $res;
    }

}