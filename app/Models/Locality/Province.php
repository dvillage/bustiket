<?php

namespace App\Models\Locality;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Province extends Eloquent {
    
    public $table = 'provinces';
    protected $hidden = [];
    public $timestamps=false;
    
    public function locDistrict(){
        return $this->hasMany('App\Models\Locality\District','province_id','id');
    }
    public function locCiti(){
        return $this->hasMany('App\Models\Locality\Citi','province_id','id');
    }
    public function locTerminal(){
        return $this->hasMany('App\Models\Locality\Terminal','province_id','id');
    }
    
    public static function get_allprovince(){
        $dlist=self::get(['id','name'])->toArray();
        return $dlist;
    }
    
    public static function get_locPro(){
        $plist = self::get(['id','name'])->toArray();
        $res = \General::success_res();
        $res['data'] = $plist;
        return $res;
    }
    
    public static function get_locProvince(){
//        dd($param);
        $dlist = self::get(['id','name'])->toArray();
//        $dlist = self::where('name','like','%'.$param['loc'].'%')->get(['id','name'])->toArray();
        $res = \General::success_res();
        $res['data'] = $dlist;
        return $res;
    }
    
    public static function add_province($param){
//        dd($param);
        
        $d = new Province;
        $d->name=trim($param['name']);
        $d->save();
        
        \Event::fire(new \App\Events\SetLocality($d->name, 'Insert', 'Province', $d->id));
        
        return $d;
    }
    
    public static function edit_province($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        $d->name=trim($param['name']);
        $d->save();
        
        \Event::fire(new \App\Events\SetLocality($d->name, 'Update', 'Province', $d->id));
        
        return $d;
    }
    
    public static function get_province($param){
//        dd($param);
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
//            $crnt_page=1;
//            $start=($crnt_page-1)*$len;
            $district=self::where('name','like','%'.$param['search'].'%')->skip($start)->take($len)->get()->toArray();
        }

//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_province($param){
        $res=self::where('id',$param['id'])->delete();
        
        \Event::fire(new \App\Events\SetLocality('', 'Delete', 'Province', $param['id']));

        return $res;
    }

}