<?php

namespace App\Models\Locality;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Citi extends Eloquent {
    
    public $table = 'cities';
    protected $hidden = [];
    public $timestamps=false;
    
    public function locProvince(){
        return $this->hasOne('App\Models\Locality\Province','id','province_id');
    }
    public function locDistrict(){
        return $this->hasMany('App\Models\Locality\District','city_id','id');
    }
    public function locTerminal(){
        return $this->hasMany('App\Models\Locality\Terminal','city_id','id');
    }
    
    public static function get_locCity(){
//        dd($param);
//        $dlist = self::where('name','like','%'.$param['loc'].'%')->get(['id','name','district_id'])->toArray();
        $dlist = self::get(['id','name','province_id'])->toArray();
        $res = \General::success_res();
        $res['data'] = $dlist;
        return $res;
    }
    
    public static function get_allciti($param = array()){
        $clist=self::orderBy('name','asc');
        if(isset($param['name'])){
            $clist->where('name','like','%'.$param['name'].'%');
        }
        if(isset($param['id']) && $param['id']!= ''){
            $clist->where('id','!=',$param['id']);
        }
        $clist = $clist->get(['id','name'])->toArray();
        return $clist;
    }
    public static function get_citifilter($param){
//        dd($param);
        $clist=self::where('province_id',$param['proid'])->get(['id','name'])->toArray();
        return $clist;
    }
    
    public static function city_exist($param,$upd = 0){
//        dd($param);
        
//        $c = self::where('name',$param['name'])->where('district_id',$param['dist']);
        $c = self::where('name',$param['name'])->where('province_id',$param['pro']);
        if($upd){
            $c= $c->where('id','!=',$param['id']);
        }
        $c = $c->first();
        if(!is_null($c)){
            return 0;
        }
        return 1;
    }
    
    public static function add_citi($param){
//        dd($param);
        
        $c = new Citi;
        $c->name=trim($param['name']);
        $c->province_id=$param['pro'];
        $c->save();
        
        \Event::fire(new \App\Events\SetLocality($c->name, 'Insert', 'City', $c->id));
        
        return $c;
    }
    public static function edit_citi($param){
//        dd($param);
        
        $c = self::where('id',$param['id'])->first();
        $c->name=trim($param['name']);
        $c->province_id=$param['pro'];
        $c->save();
        
        \Event::fire(new \App\Events\SetLocality($c->name, 'Update', 'City', $c->id));
        \Event::fire(new \App\Events\UpdateRoutes($c, 'C'));
        
        return $c;
    }
    
    public static function get_citi($param){
//        dd($param);
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $citi=self::skip($start)->with(["locProvince"])->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
//            $crnt_page=1;
//            $start=($crnt_page-1)*$len;
            $citi=self::where('name','like','%'.$param['search'].'%')->with(["locProvince"])->skip($start)->take($len)->get()->toArray();
        }

//        dd($citi);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$citi;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_citi($param){
        $res=self::where('id',$param['id'])->delete();

        \Event::fire(new \App\Events\SetLocality('', 'Delete', 'City', $param['id']));
        
        return $res;
    }

}