<?php

namespace App\Models\Locality;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Terminal extends Eloquent {
    
    public $table = 'terminals';
    protected $hidden = [];
    public $timestamps=false;

    public function locProvince(){
        return $this->hasOne('App\Models\Locality\Province','id','province_id');
    }
    public function locCiti(){
        return $this->hasOne('App\Models\Locality\Citi','id','city_id');
    }
    public function locDistrict(){
        return $this->hasOne('App\Models\Locality\District','id','district_id');
    }
    
    
    public static function get_locTerm(){
        $tlist = self::get(['id','name','district_id'])->toArray();
        $res = \General::success_res();
        $res['data'] = $tlist;
        return $res;
    }
    
    public static function terminal_exist($param,$upd = 0){
//        dd($param);
        
        $c = self::where('name',$param['name'])->where(['province_id'=>$param['dist'],'district_id'=>$param['dist'],'city_id'=>$param['city']]);
        if($upd){
            $c= $c->where('id','!=',$param['id']);
        }
        $c = $c->first();
        if(!is_null($c)){
            return 0;
        }
        return 1;
    }
    public static function add_terminal($param){
//        dd($param);
        
        $t = new Terminal;
        $t->name = trim($param['name']);
        $t->city_id = $param['city'];
        $t->district_id = $param['dist'];
        $t->province_id = $param['pro'];
        $t->address = $param['address'];
        $t->zip_code = $param['zipcode'];
        $t->agent_name = $param['agentname'];
        $t->agent_phone = $param['agentphone'];
        $t->agent_email = $param['agentemail'];
        $t->google_map_add = $param['googlemap'];
        $t->save();
        
        \Event::fire(new \App\Events\SetLocality($t->name, 'Insert', 'Terminal', $t->id));
        
        return $t;
    }
    
    public static function edit_terminal($param){
//        dd($param);
        
        $t = self::where('id',$param['id'])->first();
        $t->name=trim($param['name']);
        $t->city_id=$param['city'];
        $t->district_id=$param['dist'];
        $t->province_id = $param['pro'];
        $t->address = $param['address'];
        $t->zip_code = $param['zipcode'];
        $t->agent_name = $param['agentname'];
        $t->agent_phone = $param['agentphone'];
        $t->agent_email = $param['agentemail'];
        $t->google_map_add = $param['googlemap'];
        $t->save();
        
        \Event::fire(new \App\Events\SetLocality($t->name, 'Update', 'Terminal', $t->id));
        \Event::fire(new \App\Events\UpdateRoutes($t, 'T'));
        return $t;
    }
    
    public static function get_terminal($param){
//        dd($param);
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $citi=self::skip($start)->with(["locCiti","locDistrict",'locProvince'])->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
//            $crnt_page=1;
//            $start=($crnt_page-1)*$len;
            $citi=self::where('name','like','%'.$param['search'].'%')->with(["locCiti","locDistrict",'locProvince'])->skip($start)->take($len)->get()->toArray();
        }

//        dd($citi);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$citi;
        $res['flag']=$flag;
        return $res;
    }  
    
    public static function get_terminal_from_city($city_id){
        $ter = self::where('city_id',$city_id)->orderBy('name','asc')->get();
        return $ter;
    }
    
    public static function delete_terminal($param){
        $res=self::where('id',$param['id'])->delete();
        
        \Event::fire(new \App\Events\SetLocality('', 'Delete', 'Terminal', $param['id']));

        return $res;
    }
}