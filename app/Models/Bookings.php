<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use stdClass;

class Bookings extends Model {
    protected $fillable = [
        'booking_id','bus_id','sp_id','user_id','book_by','booking_system','status',
        'payment_by','journey_date','from_terminal','to_terminal','pickup_date',
        'dropping_date','nos','total_amount','coupon_discount','handling_fee',
        'base_amount','coupon_code','booker_name','booker_email','booker_dob','booker_mo',
        'ip','ua'
    ];
    protected $table = 'bookings';
    protected $hidden = ['payment_by','pgres'];
    public $dates=['journey_date','pickup_date','dropping_date'];
    
    public function bookingSeats(){
        return $this->hasMany('App\Models\BookingSeats','booking_id','booking_id');
    }
    public function paymentRecord(){
        return $this->hasOne('App\Models\PaymentRecord','booking_id','booking_id');
    }
    
    public function sp(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    public function bus(){
        return $this->hasOne('App\Models\Serviceprovider\Buses','id','bus_id');
    }
    
    public function fromTerminal(){
        return $this->hasOne('\App\Models\Locality\Terminal','id','from_terminal_id');
    }
    public function toTerminal(){
        return $this->hasOne('\App\Models\Locality\Terminal','id','to_terminal_id');
    }
    
    public function ss_comm(){
        return $this->hasMany('App\Models\SsCommission','booking_id','booking_id');
    }
    public function sp_comm(){
        return $this->hasOne('\App\Models\SpCommission','booking_id','booking_id');
    }
    public function admin_comm(){
        return $this->hasOne('\App\Models\AdminCommission','booking_id','booking_id');
    }
    public function userSs(){
        return $this->hasOne('\App\Models\Admin\SeatSeller','id','user_id');
    }
    public function userSp(){
        return $this->hasOne('\App\Models\Serviceprovider\ServiceProvider','id','user_id');
    }
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function approve_ticket($ticket_id){
        $t = self::where('booking_id',$ticket_id)->first();
//        dd($t->toArray());
//        dd($t->status != config('constant.STATUS_PAYMENT_PENDING') && $t->status != config('constant.STATUS_PENDING'));
        if(is_null($t)){
            return 0;
        }
        if($t->status != config('constant.STATUS_PAYMENT_PENDING') && $t->status != config('constant.STATUS_PENDING'))
        {
            return 0;
        }
//        \Log::info("oooooooooooooooooooooooooooooooo");
        $tc = $t->toArray();
        if(count($tc) == 0){
            return 0;
        }
        $t->status = config('constant.STATUS_BOOKED');
        
        if(preg_match("/[a-z]/i", $t->bus_id) && $t->booking_system > 0){
            if($t->booking_system == 2){
               $pay =  Lorena::make_lorena_payment($t);
               if(isset($pay['flag']) && $pay['flag'] == 0){
                   return;
               }
            }else{
                Roda::make_roda_payment($t);
            }
            
        }
        
        $t->save();
        
        $seats = BookingSeats::where('booking_id',$ticket_id)->update(['status'=>config('constant.STATUS_BOOKED')]);
        
        $t = self::where('booking_id',$ticket_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
        \Event::fire(new \App\Events\TicketBookSuccess($t));
        return $t;
    }
    
    public static function cancel_ticket($ticket_id){
        $t = self::where('booking_id',$ticket_id)->where('status',config('constant.STATUS_BOOKED'))->first();
//        dd($t);
        if(is_null($t)){
            return 0;
        }
        $tc = $t->toArray();
        if(count($tc) == 0){
            return 0;
        }
        
        $seats = BookingSeats::where('booking_id',$ticket_id)->get();
        
        $external = $t->booking_system;
        if($external > 0){
            $param = [
                    'journey_date'=> $t->journey_date,
                    'amount'=> $t->total_amount,
                    'booking_id'=> $t->booking_id,
                ];
            $chk = General::check_is_able_to_cancel($param);
                if($chk['flag'] != 1){
                    return $chk;
                }
            if($external == 2){
                $rodaCanc = Lorena::lorena_cancel_ticket($t);
            }else{
                $rodaCanc = Roda::roda_cancel_ticket($t);
            }    
            
            if($rodaCanc['flag'] != 1){
                return $rodaCanc;
            }
            $refund = $rodaCanc['refund'];
            $perseatRefund = $refund/$t->nos;
            
            BookingSeats::where('booking_id',$ticket_id)->update(['status'=>2,'refund_amount'=>$perseatRefund,'cancelled_at'=>date('Y-m-d H:i:s')]);
//            return;
        }else{
            foreach($seats as $s){
                $param = [
                    'journey_date'=> $s->journey_date,
                    'amount'=> $s->amount,
                    'booking_id'=> $s->booking_id,
                ];

                $chk = General::check_is_able_to_cancel($param);
                if($chk['flag'] != 1){
                    return $chk;
                }
                $seat = BookingSeats::where('id',$s->id)->first();
                $seat->status = 2;
                $seat->refund_amount = $chk['refund'];
                $seat->cancelled_at = date('Y-m-d H:i:s');
                $seat->save();
            }
        }
        
        $t->status = config('constant.STATUS_CANCELLED');
        $t->save();
        
//        $seats = BookingSeats::where('booking_id',$ticket_id)->update(['status'=>2]);  // For Bookseats, Code 2 = Cancelled,
        
        $t = self::where('booking_id',$ticket_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
        \Event::fire(new \App\Events\TicketCancel($t));
        return $t;
    }
    
    public static function get_ticket_detail($ticket_id){
        $t = self::where('booking_id',$ticket_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])
                ->first();
        if(!is_null($t)){
            $t = $t->toArray();
        }else{
            return 0;
        }
        
        
//            dd($t);
            if($t['booking_system'] > 0){
                if($t['booking_system'] == 2){
                    $busInf = \App\Models\Lorena::get_bus_info($t['booking_id']);
                }else{
                    $busInf = ApiBusInfo::where('booking_id',$t['booking_id'])->first();
                    if($busInf){
                        $busInf = $busInf->toArray();
                    }
                }
                
                if($busInf){
                    $param = [
                        'from_term_id' => $t['booking_seats'][0]['from_terminal'],
                        'to_term_id' => $t['booking_seats'][0]['to_terminal'],
                    ];
                    $bus = json_decode($busInf['bus_detail'],true);
                    $term = Roda::get_terminals($bus, $param);
//                    DD($term);
                    $t['from_terminal'] = [
                        'name' =>$term['data']['board_point'],
                        'loc_citi' =>[
                            'name'=>$bus['from'],
                            ],
                        'loc_district' => [],
                    ];
                    $t['to_terminal'] = [
                        'name' =>$term['data']['drop_point'],
                        'loc_citi' =>[
                            'name'=>$bus['to'],
                            
                            ],
                        'loc_district' => [],
                    ];
                }else{
                    $t['from_terminal'] = [
                        'name' =>'',
                        'loc_citi' =>[
                            'name'=>'',
                            ],
                        'loc_district' => [],
                    ];
                    $t['to_terminal'] = [
                        'name' =>'',
                        'loc_citi' =>[
                            'name'=>'',
                            
                            ],
                        'loc_district' => [],
                    ];
                }
            }
        
        return $t;
    }
    
    public static function save_booking_details($param,$ua = "",$ip = ""){
//        dd($param);
        
        $isAvalable = \App\Models\BookingSeats::where('session_id',$param['sessionid'])->get()->toArray();
        if(count($isAvalable) == 0){
            if(isset($param['device']) && $param['device'] == 'android'){
                return \General::error_res('your session is expired.please try again.');
            }else{
                echo '<script>alert("your session is expired.please try again.");window.location="'.\URL::to('').'";</script>';exit;
            }
        }
        
        $chkdata = \App\Models\BookingSeats::where('session_id',$param['sessionid'])->where('booking_id','!=',null)->get()->toArray();
//        dd($param,$chkdata);
        if (count($chkdata) > 0){
            $booking = self::where('booking_id',$chkdata[0]['booking_id'])->first();
//            dd($booking);
            $res = \General::success_res('Already Exist');
            $res['data'] = $booking->toArray();
            return $res;
        }
        else{
            $booked_by = isset($param['book_by']) ? $param['book_by'] : 0;
            $shortN = $booked_by == 1 ? 'AA' : ($booked_by == 2 ? 'BO' : ( $booked_by == 3 ? 'BA' : ($booked_by == 4 ? 'BB' : ($booked_by == 5 ? 'SS' : ($booked_by == 6 ? 'SA' : 'SB')))));
            if($booked_by == 0){
                $shortN = 'UR';
            }
            $booking_id = \App\Models\General::ticketNo($param['spname'], $shortN);
            
            $bus_id = isset($param['bus_id']) ? $param['bus_id'] : '';
            if(preg_match("/[a-z]/i", $bus_id)){
                if (stripos($bus_id,'_LORENA') !== false) {
                    $lorena = 1;
                    $roda = \Session::get('lorena_bus');
                    $param['booking_system'] = 2;
                }else{
                    $lorena = 0;
                    $param['booking_system'] = 1;
                    $roda = \Session::get('roda_bus');
                }
                
                $rbus = Roda::get_roda_bus($roda, $bus_id);
                if(count($rbus) == 0){
                    
                    if(isset($param['device']) && $param['device'] == 'android'){
                        return \General::error_res('your session is expired.please try again.');
                    }else{
                        echo '<script>alert("your session is expired.please try again.");window.location="'.\URL::to('').'";</script>';exit;
                    }
                }
                $pass_list = \App\Models\BookingSeats::where('session_id',$param['sessionid'])->get()->toArray();
                if($lorena){
                    $rbook = Lorena::makeLorenaBooking($rbus, $pass_list,$booking_id);
                }else{
                    $rbook = Roda::makeRodaBooking($rbus, $param['booker_name'], $param['booker_no'], $pass_list,$booking_id);
                }
                
                if($rbook['flag'] == 0){
                    if(isset($param['device']) && $param['device'] == 'android'){
                        return $rbook;
                    }else{
                        echo '<script>alert("'.$rbook['msg'].'");window.location="'.\URL::to('').'";</script>';exit;
                    }
                }else{
                    $booking_id = $rbook['booking_id'];
                }
//                dd($rbus);
            }
            
            
    
            $ua = $ua == "" ? \Request::server("HTTP_USER_AGENT") : $ua;
            $ip = $ip == "" ? \Request::getClientIp() : $ip;

            $booking =  new Bookings;
            $booking->booking_id        = $booking_id;
            $booking->bus_id            = isset($param['bus_id']) ? $param['bus_id'] : 0;
            $booking->sp_id             = isset($param['sp_id']) ? $param['sp_id'] : 0;
            $booking->user_id           = (isset($param['user_id']) && $param['user_id'] != 0) ? $param['user_id'] : null;
            $booking->book_by           = isset($param['book_by']) ? $param['book_by'] : 0;
            $booking->booking_system    = isset($param['booking_system']) ? $param['booking_system'] : 0;
            $booking->status            = isset($param['status']) ? $param['status'] : 0;
            $booking->payment_by        = isset($param['payment_by']) ? $param['payment_by'] : 0;
            $booking->journey_date      = isset($param['journey_date']) ? $param['journey_date'] : '';
            $booking->from_terminal_id  = isset($param['from_term_id']) ? $param['from_term_id'] : '';
            $booking->to_terminal_id    = isset($param['to_term_id']) ? $param['to_term_id'] : '';
            $booking->from_terminal     = isset($param['from_term']) ? $param['from_term'] : '';
            $booking->to_terminal       = isset($param['to_term']) ? $param['to_term'] : '';
            $booking->from_city         = isset($param['from_city']) ? $param['from_city'] : '';
            $booking->to_city           = isset($param['to_city']) ? $param['to_city'] : '';
            $booking->from_district     = isset($param['from_district']) ? $param['from_district'] : '';
            $booking->to_district       = isset($param['to_district']) ? $param['to_district'] : '';
            $booking->pickup_date       = isset($param['pickup_date']) ? $param['pickup_date'] : '';
            $booking->dropping_date     = isset($param['drop_date']) ? $param['drop_date'] : '';
            $booking->nos               = isset($param['nos']) ? $param['nos'] : '';
            $booking->total_amount      = isset($param['total_amount']) ? $param['total_amount'] : '';
            $booking->coupon_discount   = isset($param['coupon_discount']) ? $param['coupon_discount'] : '';
            $booking->handling_fee      = isset($param['handle_fee']) ? $param['handle_fee'] : '';
            $booking->base_amount       = isset($param['base_amount']) ? $param['base_amount'] : '';
            $booking->coupon_code       = isset($param['coupon_code']) ? $param['coupon_code'] : '';
            $booking->booker_name       = isset($param['booker_name']) ? $param['booker_name'] : '';
            $booking->booker_email      = isset($param['booker_email']) ? $param['booker_email'] : '';
            $booking->booker_dob        = isset($param['booker_dob']) ? $param['booker_dob'] : '';
            $booking->booker_mo         = isset($param['booker_no']) ? $param['booker_no'] : '';
            $booking->ip                = $ip;
            $booking->ua                = $ua;
//            $booking->request_time = date('Y:m:d:H:i', \Request::server('REQUEST_TIME'));

            $booking->save();
            $payment_record = new PaymentRecord;
            $payment_record->booking_id = $booking->booking_id;
            $payment_record->payment_by = $booking->payment_by;
            
            if($param['payment'] == 1 || (isset($param['device']) && $param['device'] == 'android')) {
                $result = substr(strtoupper($booking->booker_name), 0, 2);
                $bt_id = $result.$booking->id;
                $booking->bt_id = strlen($bt_id) < 5 ? $result.str_pad($booking->id,3,"0",STR_PAD_LEFT) : $bt_id;
                
                $booking->save();
                if(!isset($param['device']) || $param['device'] != 'android'){
                    $bank = BankDetails::where('bank_short_name', $param['bankdetails'])->first()->toArray();
                    $payment_record->bank_id = $bank['id'];
                    $payment_record->bank_details = json_encode($bank);
                }
            }
            
            $payment_record->save();
            
            $bookseat = \App\Models\BookingSeats::where('session_id',$param['sessionid'])->update(['booking_id'=>$booking_id]);
            
//            if ($booking->status == 1){
//                
//            }
            if (($booking->book_by == 5 || $booking->book_by == 6 || $booking->book_by == 7) && isset($param['direct'])){
                $ss = Admin\SeatSeller::where('id',$booking->user_id)->first();
                $ss->balance = $ss->balance - $param['total_amount'];
                $ss->save();
//                $t = self::where('booking_id',$booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//                $bookseat = \App\Models\BookingSeats::where('booking_id',$booking_id)->update(['status'=>1]);
//                \Event::fire(new \App\Events\TicketBookSuccess($t));
                self::approve_ticket($booking_id);
                
            }else if(($booking->book_by == 3 || $booking->book_by == 4) ){
                $sp = Serviceprovider\ServiceProvider::where('id',$booking->user_id)->first();
                $sp->balance = $sp->balance - $param['total_amount'];
                $sp->save();
//                $t = self::where('booking_id',$booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//                $bookseat = \App\Models\BookingSeats::where('booking_id',$booking_id)->update(['status'=>1]);
//                \Event::fire(new \App\Events\TicketBookSuccess($t));
                self::approve_ticket($booking_id);
            }
            else if(($booking->book_by == 2 ) ){
//                $sp = Serviceprovider\ServiceProvider::where('id',$booking->user_id)->first();
//                $sp->balance = $sp->balance - $param['total_amount'];
//                $sp->save();
//                $t = self::where('booking_id',$booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//                $bookseat = \App\Models\BookingSeats::where('booking_id',$booking_id)->update(['status'=>1]);
//                \Event::fire(new \App\Events\TicketBookSuccess($t));
                self::approve_ticket($booking_id);
            }
            else if($booking->payment_by == config("constant.PAYMENT_BY_BANK"))
            {
                $t = self::where('booking_id',$booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
                \Event::fire(new \App\Events\TicketBookByBank($t));
            }
            
            
            
            

            $res = \General::success_res();
            $res['data'] = $booking->toArray();
            return $res;
        }
    }
    
    public static function filter_ticketlist($param){

//        dd($param);

        $count = self::orderBy('created_at','DESC');
//        dd($count);

        if(isset($param['tid']) && $param['tid'] != ''){
            $count = $count->where('booking_id',$param['tid']);
        }
        if(isset($param['date']) && $param['date'] != ''){
            $count = $count->whereDate('journey_date','=',date('Y-m-d', strtotime($param['date'])));
        }
        if(isset($param['from']) && $param['from'] != ''){
            $count = $count->where('from_city',$param['from']);
        }
        if(isset($param['to']) && $param['to'] != ''){
            $count = $count->where('to_city',$param['to']);
        }
        if(isset($param['sp_id']) && $param['sp_id'] != ''){
            $count = $count->where('sp_id',$param['sp_id']);
        }
        if(isset($param['parent']) && $param['parent'] != '' && isset($param['user_id']) && $param['user_id'] != ''){
            $count = $count->where(function($q)use($param){
                $q->where('user_id',$param['user_id'])->orWhereIn('user_id',$param['parent']);
            });
        }elseif(isset($param['user_id']) && $param['user_id'] != ''){
            $count = $count->where('user_id',$param['user_id']);
        }
        if(isset($param['book_by'])){
            $count = $count->whereIn('book_by',$param['book_by']);
        }
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        $tlist = self::orderBy('created_at','DESC');
        
        if(isset($param['tid']) && $param['tid'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->where('booking_id',$param['tid']);
        }
        if(isset($param['date']) && $param['date'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->whereDate('journey_date','=',date('Y-m-d', strtotime($param['date'])));
        }
        if(isset($param['from']) && $param['from'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->where('from_city',$param['from']);
        }
        if(isset($param['to']) && $param['to'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->where('to_city',$param['to']);
        }
        if(isset($param['sp_id']) && $param['sp_id'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->where('sp_id',$param['sp_id']);
        }
        if(isset($param['parent']) && $param['parent'] != '' && isset($param['user_id']) && $param['user_id'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $tlist = $tlist->where('user_id',$param['user_id'])->orWhereIn('user_id',$param['parent']);
            $tlist = $tlist->where(function($q)use($param){
                $q->where('user_id',$param['user_id'])->orWhereIn('user_id',$param['parent']);
            });
        }elseif(isset($param['user_id']) && $param['user_id'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
            $tlist = $tlist->where('user_id',$param['user_id']);
        }
        if(isset($param['book_by'])){
            
            $tlist = $tlist->whereIn('book_by',$param['book_by']);
        }
        $tlist = $tlist->with(['fromTerminal.locCiti.locDistrict','toTerminal.locCiti.locDistrict','sp','bookingSeats'])->skip($start)->take($len)->get()->toArray();
//        echo $start;
        if(count($tlist) > 0){
            foreach($tlist as $k=>$ts){
                if($ts['booking_system'] > 0){
                    if($ts['booking_system'] == 2){
                        $busInf = \App\Models\Lorena::get_bus_info($ts['booking_id']);
//                        dd($busInf);
                    }else{
                        $busInf = ApiBusInfo::where('booking_id',$ts['booking_id'])->first();
                        if($busInf){
                            $busInf =$busInf->toArray();
                        }
                    }
                    
                    if($busInf){
                        $bus = json_decode($busInf['bus_detail'],true);
                        $tlist[$k]['from_terminal'] = [
                            'name' =>'',
                            'loc_citi' =>[
                                'name'=>$bus['from'],
                                'loc_district' => [],
                                ],
                        ];
                        $tlist[$k]['to_terminal'] = [
                            'name' =>'',
                            'loc_citi' =>[
                                'name'=>$bus['to'],
                                'loc_district' => [],
                                ],
                        ];
                        $tlist[$k]['roda_pay'] = $busInf['pay_status'];
                    }else{
                        $tlist[$k]['from_terminal'] = [
                            'name' =>'',
                            'loc_citi' =>[
                                'name'=>'',
                                'loc_district' => [],
                                ],
                        ];
                        $tlist[$k]['to_terminal'] = [
                            'name' =>'',
                            'loc_citi' =>[
                                'name'=>'',
                                'loc_district' => [],
                                ],
                        ];
                        $tlist[$k]['roda_pay'] = 3;
                    }
                }
            }
        }
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$tlist;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function pay_ticket_by_bank($param) {
//        $param['ticket_id'] = "160611GMS8414656";
//        $param['bank_id'] = 1;
        $pt = self::where("booking_id", $param['ticket_id'])->first();
        if (is_null($pt)) {
            return \General::error_res("No Ticket Found");
        }
        if ($pt->status == config("constant.STATUS_BOOKED"))
            return \General::error_res("This Ticket Already Paid");

        $pt->payment_by = 1;
        $pt->status = 0;

        $pt->save();
        
        $bank = BankDetails::where('id', $param['bank_id'])->first()->toArray();
        $payment = \App\Models\PaymentRecord::where('booking_id',$param['ticket_id'])->update(['payment_by'=>1,'bank_id'=>$bank['id'],'bank_details'=>json_encode($bank)]);

        if($pt->payment_by == config("constant.PAYMENT_BY_BANK"))
        {
            $t = self::where('booking_id',$param['ticket_id'])->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
          
            if($bank['bank_short_name'] == 'BNI'){
                $d0 = date('Y:m:d:H:i', strtotime($t['created_at']));
                $d1 = date('Y:m:d:H:i',strtotime($t['created_at'].' +'. config('constant.BANK_TIME_LIMIT').' minutes'));
                
                $bookingdata = $t;
                $bookingdata['time_now'] = $d0;
                $bookingdata['time_limit'] = $d1;
                
                $pc = new \App\Http\Controllers\PaymentController;
                
                $res = $pc->BniCreate($bookingdata);
    
                if($res['flag'] == 1){
                    $bank['account_no'] = $res['data']['virtual_account'];
    
                    $payment = \App\Models\PaymentRecord::where('booking_id',$bookingdata['booking_id'])->update(['va_no'=>$res['data']['virtual_account']]);
    
                    $data = [
                        'booking_id' => $t['booking_id'],
                    ];

                    $inq_res = \App\Lib\BNI\BNI::inquireInvoice($data);
                    \Log::info('BNI Inquiry Response : '.json_encode($inq_res));
                }
                else if($res['flag'] == 0){
                    \Log::info('BNI Invoice Creation Error :\n Booking ID : '.$bookingdata['booking_id'].'\n Booker Name : '.$bookingdata['booker_name'].'\n Booker Email : '.$bookingdata['booker_email'].'\n Booker Mobile No. : '.$bookingdata['booker_mo'].'\n BNI Failed Response : '.json_encode($res['data']));

                    return \General::error_res("We Couldn't process your request right now. Please, Try after sometime !");
                    
                }
            }
            
            \Event::fire(new \App\Events\TicketBookByBank($t));
        }
        $res = \General::success_res();
        $time_to_make_payment = config('constant.BANK_TIME_LIMIT');
        $res['data'] = [
            "time_to_make_payment" => $time_to_make_payment
        ];
        
        return $res;
    }
    
    public static function delete_ticket($param){
        
        $t = self::where('id',$param['id'])->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();

        \Event::fire(new \App\Events\TicketCancel($t));
        
        $res=self::where('id',$param['id'])->delete();

        return $res;
    }
    
    public static function book_ticket($param){

        $chkdata = \App\Models\BookingSeats::where('session_id',$param['session_id'])->where('booking_id','!=',null)->get()->toArray();

        if (count($chkdata) > 0){
            $booking = self::where('booking_id',$chkdata[0]['booking_id'])->first();

            $res = \General::error_res('Already Exist');
            $res['data'] = $booking->toArray();
            return $res;
        }
        else{
            
            $bdob = date('Y-m-d H:i:s', strtotime($param['booker_dob'])) ;
            $jdt = date('Y-m-d', strtotime($param["board_point"]['boarding_datetime'])) ;
            $pickdt =  date('Y-m-d H:i:s', strtotime($param["board_point"]['boarding_datetime'])) ;
            $dropdt =  date('Y-m-d H:i:s', strtotime($param["drop_point"]['droping_datetime'])) ;
            
            $l = config('constant.LOGGER');
            $sst = config('constant.SS_TYPE');
            $spt = config('constant.SP_TYPE');
            $booked_by = $l == 'SS' ? ($sst == 'Main' ? 5 : ($sst == 'A' ? 6 : 7)) : ($l == 'SP' ? ($spt == 'Main' ? 2 : ($spt == 'A' ? 3 : 4 )) : 1);
            $shortN = $booked_by == 1 ? 'AA' : ($booked_by == 2 ? 'BO' : ( $booked_by == 3 ? 'BA' : ($booked_by == 4 ? 'BB' : ($booked_by == 5 ? 'SS' : ($booked_by == 6 ? 'SA' : 'SB')))));
            $allow = 1;
            $id = $param['sp_id'];
            $tbl = 'sp';
            if($booked_by == 3 || $booked_by == 4){
                $id = config('constant.CURRENT_LOGIN_ID');
            }

            if($booked_by == 3){
                $balance = config('constant.SP_BALANCE');
                $allow = $balance < $param['total_amount'] ? 0 : 1;
            }else if ($booked_by == 5 || $booked_by == 6 || $booked_by == 7){
                $balance = config('constant.SS_BALANCE');
                $allow = $balance < $param['total_amount'] ? 0 : 1;
                $id = config('constant.CURRENT_LOGIN_ID');
                $tbl = 'ss';
            }
            if($booked_by == 1){
                $id = null;
            }
            
            $hf = Serviceprovider\ServiceProvider::where('id',$id)->first();
            if($tbl == 'ss'){
                $hf = Admin\SeatSeller::where('id',$id)->first();
            }
            if($allow == 0){
                return \General::error_res('You do not have sufficient balance to book this ticket.');
            }
            
            /*$handval=$hf->handling_fee;
            $handtype=$hf->handling_fee_type;
            if($handtype == 'P'){
                $hval=(($param['base_amount']*$handval)/100);
            }else{
                $hval=$handval*$param['total_seat'];
            }
            $param['handle_fee'] = $hval; */
            $booking_id = \App\Models\General::ticketNo($param['sp_name'], $shortN);
            
            $bookseat = \App\Models\BookingSeats::save_bookingseat_detail($param);
            
            $bus_id = isset($param['bus_id']) ? $param['bus_id'] : '';
            if(preg_match("/[a-z]/i", $bus_id)){
                if (stripos($bus_id,'_LORENA') !== false) {
                    $lorena = 1;
                    $roda = \Session::get('lorena_bus');
                    $param['booking_system'] = 2;
                }else{
                    $lorena = 0;
                    $param['booking_system'] = 1;
                    $roda = \Session::get('roda_bus');
                }
//                $param['booking_system'] = 1;
//                $roda = \Session::get('roda_bus');
                $rbus = Roda::get_roda_bus($roda, $bus_id);
                if(count($rbus) == 0){
                    return \General::error_res('your session is expired.please try again.');
                }
                $pass_list = \App\Models\BookingSeats::where('session_id',$param['session_id'])->get()->toArray();
                
                if($lorena){
                    $rbook = Lorena::makeLorenaBooking($rbus, $pass_list,$booking_id);
                }else{
                    $rbook = Roda::makeRodaBooking($rbus, $param['booker_name'], $param['booker_mo'], $pass_list,$booking_id);
                }
                
                if($rbook['flag'] == 0){
                    if(isset($param['device']) && $param['device'] == 'android'){
                        return $rbook;
                    }else{
//                        echo '<script>alert("'.$rbook['msg'].'");window.location="'.\URL::to('').'";</script>';exit;
                        return $rbook;
                    }
                }else{
                    $booking_id = $rbook['booking_id'];
                }
//                dd($rbus);
            }
            
            $ua =  \Request::server("HTTP_USER_AGENT") ;
            $ip =  \Request::getClientIp() ;

            $booking =  new Bookings;
            $booking->booking_id        = $booking_id;
            $booking->bus_id            = isset($param['bus_id']) ? $param['bus_id'] : '';
            $booking->sp_id             = isset($param['sp_id']) ? $param['sp_id'] : '';
            $booking->user_id           = $id;
            $booking->book_by           = $booked_by;
            $booking->booking_system    = isset($param['booking_system']) ? $param['booking_system'] : 0;
            $booking->status            = config("constant.STATUS_PAYMENT_PENDING");
            $booking->payment_by        = 0;
            $booking->journey_date      = $jdt;
            $booking->from_terminal_id  = $param['board_point']['b_terminal_id'];
            $booking->to_terminal_id    = $param['drop_point']['d_terminal_id'];
            $booking->from_terminal     = $param['board_point']['b_name'];
            $booking->to_terminal       = $param['drop_point']['d_name'];
            $booking->from_city         = $param['board_point']['b_city_name'];
            $booking->to_city           = $param['drop_point']['d_city_name'];
            $booking->from_district     = $param['board_point']['b_district_name'];
            $booking->to_district       = $param['drop_point']['d_district_name'];
            $booking->pickup_date       = $pickdt;
            $booking->dropping_date     = $dropdt;
            $booking->nos               = isset($param['total_seat']) ? $param['total_seat'] : '';
            $booking->total_amount      = isset($param['total_amount']) ? $param['total_amount'] : '';
            $booking->coupon_discount   = isset($param['coupon_discount']) ? $param['coupon_discount'] : '';
            $booking->handling_fee      = isset($param['handle_fee']) ? $param['handle_fee'] : '';
            $booking->base_amount       = isset($param['base_amount']) ? $param['base_amount'] : '';
            $booking->coupon_code       = isset($param['coupon_code']) ? $param['coupon_code'] : '';
            $booking->booker_name       = isset($param['booker_name']) ? $param['booker_name'] : '';
            $booking->booker_email      = isset($param['booker_email']) ? $param['booker_email'] : '';
            $booking->booker_dob        = isset($param['booker_dob']) ? $param['booker_dob'] : '';
            $booking->booker_mo         = isset($param['booker_mo']) ? $param['booker_mo'] : '';
            $booking->ip = $ip;
            $booking->ua = $ua;

            $booking->save();
            $param['booking_id']  = $booking_id;
            $param['status']  = 1;
//            $bookseat = \App\Models\BookingSeats::save_bookingseat_detail($param);
            
            $payment_record = new PaymentRecord;
            $payment_record->booking_id = $booking->booking_id;
            $payment_record->payment_by = $booking->payment_by;
            
            
            $result = substr(strtoupper($booking->booker_name), 0, 2);
            $booking->bt_id = $result.$booking->id;

            $booking->save();
            
            $payment_record->save();
            
            $bookseat = \App\Models\BookingSeats::where('session_id',$param['session_id'])->update(['booking_id'=>$booking_id]);
            if ($booked_by == 5 || $booked_by == 6 || $booked_by == 7){
                $ss = Admin\SeatSeller::where('id',$id)->first();
                $ss->balance = $ss->balance - $param['total_amount'];
                $ss->save();
            }else if($booked_by == 3 || $booked_by == 4){
                $sp = Serviceprovider\ServiceProvider::where('id',$id)->first();
                $sp->balance = $sp->balance - $param['total_amount'];
                $sp->save();
            }
            
//            $t = self::where('booking_id',$booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//            \Event::fire(new \App\Events\TicketBookSuccess($t));
            self::approve_ticket($booking_id);
            
            $res = \General::success_res();
            $res['data'] = $booking->toArray();
            return $res;
        }
    }
    
    public static function get_ticket_full_detail($param) {
        $is_booking_id = 0;
//        dd($param);
        $bookinginfo = BookingSeats::where("ticket_id", $param['ticket_id'])->where("status",'!=',2)->first();
//        dd($bookinginfo);
        if (is_null($bookinginfo)) {
            $bookinginfo = self::where("booking_id", $param['ticket_id'])->where("status",'!=',3)->first();
            if (is_null($bookinginfo)) {
                return \General::error_res("Booking not found");
            }
            $is_booking_id = 1;
        }
        $booker_detail = BookingSeats::where("booking_id", $bookinginfo->booking_id)->get()->toArray();
        $booking_detail = self::where("booking_id", $bookinginfo->booking_id)->first()->toArray();
//        dd(!isset($booker_detail[0]['booking_id']));
        if(!isset($booker_detail[0]['booking_id']))
        {
            return \General::error_res("Booking not found !");
        }
        $route_code = '';
//        dd($booking_detail);
        

//        $from_id = $bookinginfo['from_terminal_id'];
//        $to_id = $bookinginfo['to_terminal_id'];
//        dd($booking_detail);
        $from_id = $booking_detail['from_terminal_id'];
        $to_id = $booking_detail['to_terminal_id'];
        
        if( $booking_detail['booking_system'] == 0){
            $ex = 0;
            $bookingApi = 0;
            $businfo = Admin\BusRoutes::with("bus.sp")
                        ->where("bus_id", $bookinginfo['bus_id'])
                        ->where("from_terminal_id", $from_id)
                        ->where("to_terminal_id", $to_id)
//                        ->first()->toArray();
                        ->first();
                        if(!is_null($businfo)){
                            $businfo = $businfo->toArray();
                        }
            $rute = $businfo['route'];
            $from_term = Locality\Terminal::where("id", $from_id)->with(['locCiti','locDistrict'])->first()->toArray();
            $to_term = Locality\Terminal::where("id", $to_id)->with(['locCiti','locDistrict'])->first()->toArray();
        }else{
            if($booking_detail['booking_system'] == 2){
                $route_code = str_replace('_LORENA', '', $booking_detail['bus_id']);
                $busList = \App\Models\Lorena::get_bus_info($booking_detail['booking_id']);
                $spdetail = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $route_code = $booking_detail['bus_id'];
                $busList = ApiBusInfo::where('booking_id',$booking_detail['booking_id'])->first();
                $spdetail = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            $bookingApi = 1;
            $ex = 1;
            
            if($busList){
                    $params = [
                        'from_term_id' => $booker_detail[0]['from_terminal'],
                        'to_term_id' => $booker_detail[0]['to_terminal'],
                    ];
                    $list = json_decode($busList['bus_detail'],true);
                    $term = Roda::get_terminals($list, $params);
//                    DD($term);
                    $from_term = [
                        'name' =>$term['data']['board_point'],
                        'address' =>$term['data']['board_point'].','.$list['from'],
                        'loc_citi' =>[
                            'name'=>$list['from'],
                            ],
                        'loc_district' => [],
                    ];
                    $to_term = [
                        'name' =>$term['data']['drop_point'],
                        'address' =>$term['data']['drop_point'].','.$list['to'],
                        'loc_citi' =>[
                            'name'=>$list['to'],
                            
                            ],
                        'loc_district' => [],
                    ];
                    $rute = $list['route'];
                    
                    $businfo['bus']['sp'] = $spdetail;
                    $businfo['bus']['name'] = $list['bus'];
            }else{
                $from_term = [
                        'name' =>'',
                        'address' =>'',
                        'loc_citi' =>[
                            'name'=>'',
                            ],
                        'loc_district' => [],
                    ];
                    $to_term = [
                        'name' =>'',
                        'address' =>'',
                        'loc_citi' =>[
                            'name'=>'',
                            
                            ],
                        'loc_district' => [],
                    ];
                    $rute = '';
                    
                    $businfo['bus']['sp'] = $spdetail;
                    $businfo['bus']['name'] = '';
            }
        }
        
        
        $token = \Request::header('AuthToken');
//        dd($token);
        if(isset($param['email']) && $booking_detail['booker_email'] != $param['email'])
        {
            return \General::error_res("Booking not found !!");
        }

        $bookinginfo = $bookinginfo->toArray();

        $handling_fee = $booking_detail['handling_fee'];
        $unit_handling_fee = $booking_detail['handling_fee'] / $booking_detail['nos'];
        
        
            
        $passengerinfo = BookingSeats::select(["passenger_name AS name","seat_lbl AS seat","passenger_gender AS gender","passenger_age AS age","passenger_mobile AS mobile","ticket_id AS unique_id"]);
//        dd($is_booking_id,$param['ticket_id']);
        if($is_booking_id)
        {
            $passengerinfo = $passengerinfo->where("booking_id", $param['ticket_id']);
        }
        else
        {
            $bookings = BookingSeats::where("booking_id",$param['ticket_id'])->where("status",'!=',2)->get()->toArray();
            
            $unique_ids = [];
            foreach ($bookings as $key1 => $val1)
            {
                $unique_ids[] = $val1['ticket_id'];
            }
            $passengerinfo = $passengerinfo->whereIn("ticket_id", $unique_ids)->orWhere("ticket_id",$param['ticket_id']);
        }
        $qr = [];
        $passengerinfo = $passengerinfo->get()->toArray();
        foreach ($passengerinfo as $key2 => $val2)
        {
//            \Log::info(json_encode($val2));
//            dd($val2);
            $passengerinfo[$key2]['gender'] = $passengerinfo[$key2]['gender'] == 'M' || $passengerinfo[$key2]['gender'] == 'm' ? 'L':'P'; 
            if($passengerinfo[$key2]['seat'] == ''){
                $passengerinfo[$key2]['seat'] = "Saat Keberangkatan";
            }
            $file = config('constant.QRCODE_PATH').'qr_'.$val2['unique_id'].'.png';
//            \Log::info('qr file : '.$file);
            if(file_exists($file)){
                $qr[] = [ 'ticket_id'=>$val2['unique_id'], 'path'=> \URL::to('assets/uploads/qr').'/qr_'.$val2['unique_id'].'.png'];
            }
        }
        
//        dd($param,$bookinginfo,$businfo, $rute,$handling_fee,$unit_handling_fee,$from_term,$to_term,$passengerinfo);
        
        $ticket_id = $booking_detail['booking_id'];

        $boading_address = $booking_detail['from_terminal'];
        $dropping_address = $booking_detail['to_terminal'];
        $boading_full_address = $from_term['address'];
        $dropping_full_address = $to_term['address'];

        $time_from = date('h:i A', strtotime($booking_detail['pickup_date']));
        $time_to = date('h:i A', strtotime($booking_detail['dropping_date']));
        
        $tfrm = date('H:i', strtotime($booking_detail['pickup_date']));
        $tto = date('H:i', strtotime($booking_detail['dropping_date']));
        
        $new_from_date_formated = \App\Models\General::getIndoMonths(date("j F Y", strtotime($booking_detail['pickup_date']))).' ('.$tfrm.')';
        $new_to_date_formated = \App\Models\General::getIndoMonths(date("j F Y", strtotime($booking_detail['dropping_date']))).' ('.$tto.')';
        
        $data = [
            "date"                  => date('Y-m-d',strtotime($bookinginfo['journey_date'])),
//            "date_formated"         => date("D, j F, Y", strtotime($bookinginfo['journey_date'])),
            "date_formated"         => \App\Models\General::getIndoMonths(date("j F Y", strtotime($booking_detail['pickup_date'])),date("l", strtotime($booking_detail['pickup_date']))),
            "to_date_formated"      => \App\Models\General::getIndoMonths(date("j F Y", strtotime($booking_detail['dropping_date'])),date("l", strtotime($booking_detail['dropping_date']))),
            "new_from_date_formated" => $new_from_date_formated,
            "new_to_date_formated"   => $new_to_date_formated,
            "user_name"             => $booking_detail['booker_name'],
            "user_email"            => $booking_detail['booker_email'],
            "user_mobile"           => $booking_detail['booker_mo'],
            "service_provider_name" => $businfo['bus']['sp']['first_name'].' '.$businfo['bus']['sp']['last_name'],
            "parent_bus_name"       => $businfo['bus']['name'],
            "final_payable_amount"  => \General::number_format($booking_detail['total_amount'], 3),
            "total_amt"             => \General::number_format(($booking_detail['base_amount'] * $booking_detail['nos']), 3),
            "discount_price"        => \General::number_format($booking_detail['coupon_discount'] , 3),
            "handling_fee"          => \General::number_format($handling_fee , 3),
            "unit_handling_fee"     => \General::number_format($unit_handling_fee , 3),
            "time_from"             => $time_from,
            "time_to"               => $time_to,
            "boading_address"       => $boading_address,
            "boading_full_address"  => is_null($boading_full_address) ? "" : $boading_full_address,
            "drop_address"          => $dropping_address,
            "drop_full_address"     => is_null($dropping_full_address) ? "" : $dropping_full_address,
            "rute"                  => $rute,
            "passenger"             => $passengerinfo,
            "from_city_id"          => $from_id,
            "to_city_id"            => $to_id,
            "from_city"             => $from_term['loc_citi']['name'],
            "to_city"               => $to_term['loc_citi']['name'],
            "ticket_id"             => $ticket_id,
            "layout"                => 0,
            "bus_name"              => $businfo['bus']['name'],
            "coupon_code"           => $booking_detail['coupon_code'],
            "coupon_code_discount"  => \General::number_format($booking_detail['coupon_discount'] , 3),
            "nos"                   => $booking_detail['nos'],
            "qr"                    => $qr,
            'route_code'            =>$route_code,
        ];
        
//        dd($data);
        
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
    public static function user_ticket_history($param) {
//        dd($param);
        $tickets = self::where("user_id", $param['user_id'])->orderBy("id", "DESC")->where('book_by',0);
        if ($param['type'] == "in_progress") {
            $tickets = $tickets->whereIn("status", [0, 3]);
        } else if ($param['type'] == "complete") {
            $tickets = $tickets->where("status", 1);
        }
        else{
            return \General::error_res("type missing");
        }
        
//        $busway=\DB::table('busway_card_purchase')->where('user_id',$param['user_id'])->orderBy("card_purchase_id","DESC");
//        
//        if($param['type']=="in_progress"){
//            $busway=$busway->where("pay_status",0);            
//        }
//        else if($param['type']=="complete")
//        {
//            $busway=$busway->where("pay_status",1);
//        }
//        else{
//            return \General::error_res("type missing");
//        }
//        $busway=$busway->get();
        
//        $card_detail=self::getDetailofCard($busway);
//        
//        $pariwisata=\DB::table('pariwisata_order')->where('user_id',$param['user_id'])->orderBy("pariwisata_order_id","DESC");
//        if($param['type']=="in_progress"){
//            $pariwisata=$pariwisata->where("order_status",0);            
//        }
//        else if($param['type']=="complete")
//        {
//            $pariwisata=$pariwisata->where("order_status",1);
//        }
//        else{
//            return \General::error_res("type missing");
//        }
//        $pariwisata=$pariwisata->get();
//        $pariwisata=self::getPariwisataDetail($pariwisata);
        
        $tickets = $tickets->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])->get();
        
//        dd($tickets->toArray());
        $ticket_data = self::formate_ticket_history($tickets);
        $card_detail = [];
        $pariwisata = [];
        if(count($ticket_data) <= 0 && count($card_detail) <= 0 && count($pariwisata) <= 0 ){
            $res = \General::error_res('No Ticket Found.');
            return $res;
        }
        
        $res = \General::success_res();
        $res['data'] = $ticket_data;
//        $res['busway_data'] = [];
//        $res['pariwisata_data'] = [];
        $res['busway_data'] = $card_detail;
        $res['pariwisata_data'] = $pariwisata;
        return $res;
    }
    
    public static function formate_ticket_history($tickets) {
        $tickets = $tickets->toArray();
//        dd($tickets);
        $ticket_data = [];
        foreach ($tickets as $key => $val) {
//            dd($val);
            $service_provider = $val['sp'];
            if (!is_null($service_provider)) {
                $service_provider_name = $service_provider['first_name'].' '.$service_provider['last_name'];
            }
            if($val['booking_system'] == 0){
                $route_detail = Admin\BusRoutes::with("bus.sp")
                    ->where("bus_id", $val['bus_id'])
                    ->where("from_terminal_id", $val['from_terminal_id'])
                    ->where("to_terminal_id", $val['to_terminal_id'])
//                    ->first()->toArray();
                    ->first();
                if(!is_null($route_detail)){
                    $route_detail = $route_detail->toArray();
                }
//                dd($route_detail->toArray());
                $booked_route = $route_detail['route'];
            }else{
                if($val['booking_system'] == 2){
                    $busList = \App\Models\Lorena::get_bus_info($val['booking_id']);
                }else{
                    $busList = ApiBusInfo::where('booking_id',$val['booking_id'])->first();
                }
                
                if($busList){
                        $params = [
                            'from_term_id' => $val['booking_seats'][0]['from_terminal'],
                            'to_term_id' => $val['booking_seats'][0]['to_terminal'],
                        ];
                        $list = json_decode($busList['bus_detail'],true);
                        $term = Roda::get_terminals($list, $params);
    //                    DD($term);
                        $val['from_terminal'] = [
                            'name' =>$term['data']['board_point'],
                            'address' =>$term['data']['board_point'].','.$list['from'],
                            'loc_citi' =>[
                                'name'=>$list['from'],
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>$term['data']['drop_point'],
                            'address' =>$term['data']['drop_point'].','.$list['to'],
                            'loc_citi' =>[
                                'name'=>$list['to'],

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = $list['route'];
                        
                }else{
                    $val['from_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = '';
                }
            }
            

            
            $from_city = $val['from_terminal']['loc_citi'];
            $to_city = $val['to_terminal']['loc_citi'];
            $boading_address = $val['from_terminal']['name'];
            $dropping_address = $val['to_terminal']['name'];
            $boading_full_address = $val['from_terminal']['address'];
            $dropping_full_address = $val['to_terminal']['address'];
            
            $time_to_make_payment = 0;

            $bank_detail = null;

            if ($val['payment_record']['payment_by'] == 1) {
                $bank_data = json_decode($val['payment_record']['bank_details'],true);
                $bank = BankDetails::where('id',$bank_data['id'])->first();
                $bank_detail = $bank;
                $time_to_make_payment = Services\General::get_time_to_make_payment($val['created_at']);
                $time_to_make_payment = $time_to_make_payment < 1 ? 0 : $time_to_make_payment;
            }

            $handling_fee = $val['handling_fee'];
            $unit_handling_fee = $val['handling_fee'] / $val['nos'];

            $pay_status = $val['status'];
            $journey_completed = 0;
            $current_date = date("Y-m-d H:i:s");
            $travelling_date = $val['journey_date'];
            if (Services\General::date_dif($current_date, $travelling_date, "i") < 1) {
                $journey_completed = 1;
            }
            
            $transfer_code = $val['bt_id'];
//            $data = [
//                'ticket_id' => $val['booking_id'],
//                'unique_id' => $val['booking_id'],
//                'coupon_code' => $val['coupon_code'],
//                'date' => date("D, j F, Y", strtotime($val['journey_date'])),
//                "from_city" => $val['from_terminal']['loc_citi']['name'],
//                "to_city" => $val['to_terminal']['loc_citi']['name'],
//                "boading_address" => $boading_address,
//                "boading_full_address" => is_null($boading_full_address) ? "" : $boading_full_address,
//                "drop_address" => $dropping_address,
//                "drop_full_address" => is_null($dropping_full_address) ? "" : $dropping_full_address,
//                "user_name" => $val['booker_name'],
//                "user_email" => $val['booker_email'],
//                "user_mobile" => $val['booker_mo'],
//                "total_amt" => \General::number_format(($val['total_amount']) , 3),
//                "ticket_count" => $val['nos'],
//                "payment_mode" => $val['payment_record']['payment_by'] == 1 || $val['payment_record']['payment_by'] == 0 ? "CASH" : "CC",
//                "journey_completed" => $journey_completed,
//                "cancelledStatus" => $val['status'] == 3 ? 1 : 0,
//                "pay_status" => $pay_status,
//                "service_provider_name" => $service_provider_name,
//                "payment_type" => $val['payment_record']['payment_by'],
//                "bank_detail" => $bank_detail,
//                "time_to_make_payment" => $time_to_make_payment,
//                "final_payable_amount" => \General::number_format($val['base_amount'] , 3),
//                "discount_price" => \General::number_format($val['coupon_discount'] , 3),
//                "handling_fee" => \General::number_format($handling_fee , 3),
//                "unit_handling_fee" => \General::number_format($unit_handling_fee , 3),
//                "transfer_code" => $transfer_code,
//                "passenger_Name" => $val['booking_seats'][0]['passenger_name'],
//            ];
//
//
//            $ticket_data[] = $data;
//            dd($val);
            foreach($val['booking_seats'] as $k=>$p){
                if($p['status'] != 2){
                    $data = [
                        'ticket_id' => $val['booking_id'],
                        'unique_id' => $p['ticket_id'],
                        'coupon_code' => $val['coupon_code'],
                        'date' => date("D, j F, Y", strtotime($val['journey_date'])),
                        "from_city" => $val['from_terminal']['loc_citi']['name'],
                        "to_city" => $val['to_terminal']['loc_citi']['name'],
                        "boading_address" => $boading_address,
                        "boading_full_address" => is_null($boading_full_address) ? "" : $boading_full_address,
                        "drop_address" => $dropping_address,
                        "drop_full_address" => is_null($dropping_full_address) ? "" : $dropping_full_address,
                        "user_name" => $val['booker_name'],
                        "user_email" => $val['booker_email'],
                        "user_mobile" => $val['booker_mo'],
    //                    "total_amt" => number_format(($val['total_amount']) / 1000, 3),
                        "total_amt" => \General::number_format(($val['total_amount'] / $val['nos']), 3),
                        "ticket_count" => $val['nos'],
                        "payment_mode" => $val['payment_record']['payment_by'] == 1 || $val['payment_record']['payment_by'] == 0 ? "CASH" : "CC",
                        "journey_completed" => $journey_completed,
                        "cancelledStatus" => $val['status'] == 3 ? 1 : 0,
                        "pay_status" => $pay_status,
                        "service_provider_name" => $service_provider_name,
                        "payment_type" => $val['payment_record']['payment_by'],
                        "bank_detail" => $bank_detail,
                        "time_to_make_payment" => $time_to_make_payment,
                        "final_payable_amount" => \General::number_format($val['base_amount'], 3),
                        "discount_price" => \General::number_format($val['coupon_discount'], 3),
                        "handling_fee" => \General::number_format($handling_fee, 3),
                        "unit_handling_fee" => \General::number_format($unit_handling_fee , 3),
                        "transfer_code" => $transfer_code,
                        "passenger_Name" => $p['passenger_name'],
                        "nos" => $val['nos'],
                    ];


                    $ticket_data[] = $data;
                }

            }
        }
        return $ticket_data;
    }
    
    public static function payment_confirmation($param) {
        
        
        $booking_info = self::where("bt_id", $param['transfer_code'])->first();
        if (is_null($booking_info)) {
            return \General::error_res("Tiket tidak ditemukan");
        }
        $bookerinfo = BookingSeats::where("booking_ID", $booking_info->booking_ID)->first();
        if (is_null($bookerinfo)) {
            return \General::error_res("Tiket tidak ditemukan !!");
        }
        
//        $pt = $booking_info;
//        if (is_null($pt)) {
//            return \General::error_res("No Ticket Found");
//        }
//        if ($pt->status == config("constant.STATUS_BOOKED"))
//            return \General::error_res("This Ticket Already Paid");
//
//        $pt->payment_by = 1;
//        $pt->status = 2;
//        $pt->save();
        
        
        $data = [
            'mail_subject' => 'Pembayaran Tiket "' . $param['transfer_code'].'"',
            'email' => "finance@bustiket.com",
        ];
        $data = array_merge($data, $param);
//        dd($data);
//        $html = \View::make("emails.user.payment_confirmation",$data);
//        echo $html;
//        exit;
        $html = \Mail::send('emails.user.payment_confirmation', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['mail_subject']);
                });
        return \General::success_res("Konfirmasi anda sudah diterima E-Ticket sedang kami proses.");
    }
    
    public static function seatseller_ticket_history($param) {
        $logged_in_user = app("logged_in_agent");
        $agent_id = $logged_in_user['id'];
//        $tickets = self::orderBy("auto_id", "DESC")->groupBy("Ticket_ID")->where("agent_id", $agent_id);
        
//        $tickets = self::orderBy("id", "DESC")->where("user_id", $agent_id);
//        if($param['type'] == "in_progress")
//        {
//            $tickets = $tickets->whereIn("pay_status",[0,1,3]);
//        }
//        else if($param['type'] == "complete")
//        {
//            $tickets = $tickets->where("pay_status",2);
//        }
        
        $tickets = self::where("user_id", $agent_id)->orderBy("id", "DESC")->whereIn('book_by',[5,6,7]);
        if ($param['type'] == "in_progress") {
            $tickets = $tickets->whereIn("status", [0, 3]);
        } else if ($param['type'] == "complete") {
            $tickets = $tickets->where("status", 1);
        }
        else{
            return \General::error_res("type missing");
        }
        
//        $tickets = $tickets->get();
        $tickets = $tickets->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])->get();
        $ticket_data = self::formate_ticket_history($tickets);
        $res = \General::success_res();
        $res['data'] = $ticket_data;
        return $res;
    }
    
    public static function check_availability($param){

        $booked = \App\Models\BookingSeats::get_booked_seats($param['bus_id'], $param['date']);
        $blocked = \App\Models\BookingSeats::get_blocked_seats($param['bus_id'], $param['date']);
        
        $layout = \App\Models\BusLayout::where('bus_id',$param['bus_id'])->first();
        
        $session_id = isset($param['session_id']) ? $param['session_id'] : session('session_id');
        $alreadyBooked = \App\Models\BookingSeats::where('session_id',$session_id)->where('status',0)->where('booking_id',null)->get();
        $blkd = [];
        if(count($alreadyBooked) > 0){
            foreach($alreadyBooked as $ab){
                $blkd[] = $ab->seat_lbl;
                if(($key = array_search($ab->seat_lbl, $blocked)) !== false) {
                    unset($blocked[$key]);
                }

            }
        }
        
        $total_seat = \App\Models\BusLayout::total_seats($param['bus_id']);
        $remain_seat = $total_seat - (count($blocked)+count($booked));
        $blocked_count = count($blkd);
        
//        $passenger = json_decode($param['passenger'],true);
        $passenger = $param['passenger'];
//        dd($passenger,$booked,$blocked);
        if($param['total_seats'] > $remain_seat){
            $res = \General::error_res($param['total_seats'].' Seats Not Available.');
            return $res;
        }
        else if($layout){
            foreach($passenger as $st){
                if(in_array($st['seat_lbl'], $booked)){
                    $res = \General::error_res('Already Booked By Other. Please Select Again.');
                    return $res;
                }
                if(in_array($st['seat_lbl'], $blocked)){
                    $res = \General::error_res('Already Booked By Other. Please Select Again.');
                    return $res;
                }
            }
        }
//        else if($param['total_seats'] > $remain_seat){
//            $res = \General::error_res('Already Booked By Other. Please Select Again.');
//            return $res;
//        }
        
        $res = \General::success_res();
        return $res;
    }
    
    public static function operator_ticket_history($param) {
//        $tickets = self::orderBy("auto_id", "DESC");
        
        $logged_in_user = app("logged_in_operator");
//        $agent_id = 0;
//        if (isset($logged_in_user['sp_type']) && $logged_in_user['sp_type'] == config("constant.SERVICE_PROVIDER_MAIN")) {
//            $tickets = $tickets->where("pay_trans_id", 'sp');
//        } else if ((isset($logged_in_user['sp_type']) && $logged_in_user['sp_type'] == config("constant.SERVICE_PROVIDER_A") ) || (isset($logged_in_user['sp_type']) && $logged_in_user['sp_type'] == config("constant.SERVICE_PROVIDER_B"))) {
//
//            $agent = \App\Models\SeatSeller\User::where("agent_email", $logged_in_user['SP_email'])->first();
//            if (!is_null($agent)) {
//                $tickets = $tickets->where("agent_id", $agent->agent_id);
//            } else {
//                $res = \General::success_res();
//                $res['data'] = [];
//                return $res;
//            }
//        } else {
//            $res = \General::success_res();
//            $res['data'] = [];
//            return $res;
//        }
//        if($param['type'] == "in_progress")
//        {
//            $tickets = $tickets->whereIn("pay_status",[0,1,3]);
//        }
//        else if($param['type'] == "complete")
//        {
//            $tickets = $tickets->where("pay_status",2);
//        }
        
        $agent_id = $logged_in_user['id'];
        $tickets = self::where("user_id", $agent_id)->orderBy("id", "DESC")->whereIn('book_by',[2,3,4]);
        if(isset($param['booking_id']) && $param['booking_id'] != ''){
            $tickets = $tickets->where(function($q) use ($param){
                $q->where('booking_id',$param['booking_id'])->orWhereHas('bookingSeats', function($qr)use($param) {
                                $qr->where('ticket_id', $param['booking_id']);
                            });
            });
        }
        if(isset($param['name']) && $param['name'] != ''){
            $tickets = $tickets->where('booker_name',$param['name']);
        }
        if(isset($param['mobile']) && $param['mobile'] != ''){
            $tickets = $tickets->where('booker_mo',$param['mobile']);
        }
        if(isset($param['email']) && $param['email'] != ''){
            $tickets = $tickets->where('booker_email',$param['email']);
        }
        if(isset($param['dob']) && $param['dob'] != ''){
            $dob = date('Y-m-d H:i:s', strtotime($param['dob']));
            $tickets = $tickets->where('booker_dob',$dob);
        }
        
        
        if ($param['type'] == "in_progress") {
            $tickets = $tickets->whereIn("status", [0, 3]);
        } else if ($param['type'] == "complete") {
            $tickets = $tickets->where("status", 1);
        }
        else{
            return \General::error_res("type missing");
        }
        
//        $tickets = $tickets->get();
        $tickets = $tickets->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])->get();
        $ticket_data = self::formate_ticket_history($tickets);
        $res = \General::success_res();
        $res['data'] = $ticket_data;
        return $res;
    }
    
    public static function refund_ticket($id){
        $booking = self::where('booking_id',$id)->first();
        if(!$booking){
            return 0;
        }
        $bookinkSeat = BookingSeats::where('booking_id',$booking->booking_id)->update(['status'=>3]);
        
        return \General::success_res();
    }
    
}
