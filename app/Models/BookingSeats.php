<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BookingSeats extends Model {
    protected $fillable = [
        'booking_id','ticket_id','session_id','status','bus_id','sp_id',
        'seat_index','seat_lbl','journey_date','from_terminal','to_terminal','passenger_name',
        'passenger_gender','passenger_age','amount'
    ];
    protected $table = 'booking_seats';
    protected $hidden = ['book_by','payment_by'];
    public $dates=['journey_date','pickup_date','dropping_date'];
    
    public function booking(){
        return $this->hasOne('App\Models\Bookings','booking_id','booking_id');
    }
    
    public static function save_bookingseat_detail($param){
//        dd($param);
        $chkdata = self::where(['session_id'=>$param['session_id'],'bus_id'=>$param['bus_id'],'booking_id'=>null])->delete();
        
        $chkdata = self::where(['session_id'=>$param['session_id'],'bus_id'=>$param['bus_id']])->get()->toArray();
        
        
        if(count($chkdata) == 0){
            $d = date('Y-m-d',strtotime($param['board_point']['boarding_datetime']));        
        
            $sessionid = isset($param['session_id']) ? $param['session_id']:'';
            $status = isset($param['status']) ? $param['status']:0;
            $busid = isset($param['bus_id']) ? $param['bus_id']:1;
            $busname = isset($param['bus_name']) ? $param['bus_name']:'';



            $traveldate = isset($param['board_point']['boarding_datetime']) ? $d:'';
            $from_t = isset($param['board_point']['b_terminal_id']) ? $param['board_point']['b_terminal_id']:'';
            $to_t = isset($param['drop_point']['d_terminal_id']) ? $param['drop_point']['d_terminal_id']:'';
            $amount = isset($param['per_head_amount']) ? $param['per_head_amount']:'';

            for($i=0;$i<$param['total_seat'];$i++){
                $seat = new BookingSeats;
                if(isset($param['booking_id']) && $param['booking_id'] != ''){
                    $seat->booking_id = $param['booking_id'];
                }
                $seat->session_id = $sessionid;
                $seat->status = $status;
                $seat->bus_id = $busid;
                $seat->bus_name = $busname;
                $seat->amount = $amount;
                $seat->journey_date = $traveldate;
                $seat->from_terminal = $from_t;
                $seat->to_terminal = $to_t;

                $seat->seat_index = isset($param['seat_index'][$i]) ? $param['seat_index'][$i]:'';
                $seat->seat_lbl = isset($param['seat_label'][$i]) ? $param['seat_label'][$i]:'';
                $seat->passenger_name = isset($param['pass_name'][$i]) ? $param['pass_name'][$i]:0;
                $seat->passenger_gender = isset($param['pass_gender'][$i]) ? strtolower($param['pass_gender'][$i]):0;
                $seat->passenger_mobile = isset($param['pass_mo'][$i]) ? $param['pass_mo'][$i]:0;
                
                
                $ticket_id = \App\Models\General::uniqueNo($param['sp_name'], $param['pass_name'][$i]);
                $seat->ticket_id = $ticket_id;

                if(isset($param['pass_dob'])){
                    $dob = date('Y-m-d',  strtotime($param['pass_dob'][$i]));
                    $today = date("Y-m-d");
                    $diff = date_diff(date_create($dob), date_create($today));

                    $seat->passenger_age = isset($param['pass_dob'][$i]) ? $diff->format('%y'):'';
                }
                if(isset($param['pass_age'])){
                    $seat->passenger_age = isset($param['pass_age'][$i]) ? $param['pass_age'][$i] :'';
                }
                
                $seat->save();

            }

            $res = \General::success_res();
            $res['data'] = $seat->toArray();
            $res['redirect'] = 0;
            return $res;
        }
        
        $res = \General::error_res('Already Exist..');
        $res['data'] = $chkdata;
        $res['redirect'] = 1;
        return $res;
//        
    }
    
    public static function get_blocked_seats($bus_id,$journy_date){
        $seats = self::where('status',0)->where('bus_id',$bus_id)->where('journey_date',$journy_date)->get()->toArray();
        $bs = [];
        if(count($seats)>0){
            foreach($seats as $s){
                $bs[] = $s['seat_lbl'];
            }
        }
//        print_r($bs);
        return $bs;
    }
    public static function get_booked_seats($bus_id,$journy_date){
        $seats = self::where('status',1)->where('bus_id',$bus_id)->where('journey_date',$journy_date)->get()->toArray();
        $bs = [];
        if(count($seats)>0){
            foreach($seats as $s){
                $bs[] = $s['seat_lbl'];
            }
        }
        return $bs;
    }
    public static function check_booking_exist($tik_id,$email,$flag){
        $tik = self::where('ticket_id',$tik_id);
        if($flag){
            $tik = $tik->whereHas('booking',function($q)use($email){
                $q->where('booker_email',$email);
            });
        }
        $tik = $tik->first();
//        dd($email);
        $res = \General::success_res();
        if(!$tik){
            $msg = 'ticket id not found.';
            if($flag){
                $msg = 'ticket id or email not found.Please enter the email id which was given at the time of booking.';
            }
            $res = \General::error_res($msg);
        }
        return $res;
    }
    
    public static function cancel_seat_by_ticket($ticket_id){
        $tik = self::where('ticket_id',$ticket_id)->where('status',1)->first();
        if(!$tik){
            \Session::flash('error', 'ticket not found or cancelled already.');
            return \General::error_res('no ticket found.');
        }
        
        $external = $tik->booking_system;
        if($external > 0){
            \Session::flash('error', 'You can not cancel this ticket partially.');
            return \General::error_res('You can not cancel this ticket partially.');
        }
        $param = [
            'journey_date'=> $tik->journey_date,
            'amount'=> $tik->amount,
            'booking_id'=> $tik->booking_id,
        ];
        
        $chk = General::check_is_able_to_cancel($param);
        if($chk['flag'] != 1){
            return $chk;
        }
        $tik->status = 2;
        $tik->refund_amount = $chk['refund'];
        $tik->cancelled_at = date('Y-m-d H:i:s');
        $tik->save();
        
        $t = Bookings::where('booking_id',$tik->booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
        $total_seat = $t['nos'];
        $t['total_cancelled'] = 1 ;
        
        if($total_seat == $t['total_cancelled']){
            $b = Bookings::where('booking_id',$tik->booking_id)->first();
            $b->status = 2;
            $b->save();
        }
        \Session::flash('success', 'ticket cancelled successfully');
        
        \Event::fire(new \App\Events\TicketCancel($t));
    }
    
    public static function cancel_seat_by_ticketid($ticket_id){
        $tik = self::where('ticket_id',$ticket_id)->where('status',1)->first();
        if(!$tik){
            \Session::flash('error', 'ticket not found or cancelled already.');
            return \General::error_res('no ticket found.');
        }
        $bk = Bookings::where('booking_id',$tik->booking_id)->first();
        $external = $bk->booking_system;
        if($external > 0){
            \Session::flash('error', 'You can not cancel this ticket partially.');
            return \General::error_res('You can not cancel this ticket partially.');
        }
        
        $param = [
            'journey_date'=> $tik->journey_date,
            'amount'=> $tik->amount,
            'booking_id'=> $tik->booking_id,
        ];
        
        $chk = General::check_is_able_to_cancel($param);
        if($chk['flag'] != 1){
            return $chk;
        }
//        dd($chk);
        $tik->status = 2;
        $tik->refund_amount = $chk['refund'];
        $tik->cancelled_at = date('Y-m-d H:i:s');
        $tik->save();
        
        $t = Bookings::where('booking_id',$tik->booking_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
        $total_seat = $t['nos'];
        $t['total_cancelled'] = 1 ;
        
        if($total_seat == $t['total_cancelled']){
            $b = Bookings::where('booking_id',$tik->booking_id)->first();
            $b->status = 2;
            $b->save();
        }
        \Session::flash('success', 'ticket cancelled successfully');
        
        \Event::fire(new \App\Events\TicketCancel($t));
        
        $res = \General::success_res('Ticket Cancelled.');
        return $res;
    }
    
    public static function check_session_id($param){
        $chkdata = self::where('session_id',$param['session_id'])->where('booking_id','!=',null)->get()->toArray();
        if(count($chkdata) > 0){
            return \General::error_res('session is already used.');
        }
        return \General::success_res();
    }
}
