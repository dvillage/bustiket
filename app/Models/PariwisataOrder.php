<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PariwisataOrder extends Model {
    protected $fillable = [
        'user_id','subscriber_name','email','mobile','company_name','pickup_address','order_date','order_service_type','comment','order','total_bus','bus_type','multiple_bus','order_status'
    ];
    protected $table = 'tour_bus_order';
    protected $hidden = [];
    public $timestamps=true;
    
    public function buses(){
        return $this->hasMany('App\Models\PariwisataOrderBus','order_id','id');
    }
    
    public static function add_order($param){
        
        $ord = new self();
        $ord->user_id = isset($param['user_id']) ? $param['user_id'] : '';
        $ord->subscriber_name = $param['booker_name'];
        $ord->email = $param['booker_email'];
        $ord->mobile = $param['mobile'];
        $ord->company_name = $param['institue_name'];
        $ord->pickup_address = $param['pickup_address'];
        $ord->order_date = date('Y-m-d H:i:s',strtotime($param['date']));
        $ord->order_service_type = $param['service_type'];
        $ord->comment = $param['destination_route'];
        $ord->order = $param['notes'];
        $ord->total_bus = $param['total_bus'];
        $ord->bus_type = $param['bus_type'];
        $ord->multiple_bus = isset($param['check_accept_multiple']) ? 1 : 0;
        $ord->order_status = 0;
        $ord->save();
        $allBus = json_decode($param['all_bus']);
        foreach($allBus as $b){
            $ob = new PariwisataOrderBus();
            $ob->order_id = $ord->id;
            $ob->tour_bus_id = $b->id;
            $ob->save();
        }
        
        $detail = self::where('id',$ord->id)->with('buses.bus')->first();
        \Event::fire(new \App\Events\PariwisataBookSuccess($detail));
        return \General::success_res();
    }
    
    public static function get_pariwisata_order_list($param){
        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $count=$count->where('subscriber_name','like','%'.$param['search'].'%')->orWhere('mobile','like','%'.$param['search'].'%');
        }
        if(isset($param['email']) && $param['email']!='') {
            $count=$count->where('email','like','%'.$param['email'].'%');
        }
        if(isset($param['company']) && $param['company']!=''){
            $count=$count->where('company_name','like','%'.$param['company'].'%');
        }
        if(isset($param['status']) && $param['status']!=''){
            $count=$count->where('order_status',$param['status']);
        }
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            $count=$count->whereHas('buses.bus',function($q)use($param){
                $q->where('sp_id',$param['sp_id']);
            });
        }
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::skip($start)->take($len)->orderBy('id','desc')->with('buses');
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $district=self::orderBy('id','desc')->with('buses')->skip($start)->take($len);
            $district=$district->where('subscriber_name','like','%'.$param['search'].'%')->orWhere('mobile','like','%'.$param['search'].'%');
        }
        if(isset($param['email']) && $param['email']!='') {
            $district=$district->where('email','like','%'.$param['email'].'%');
        }
        if(isset($param['company']) && $param['company']!=''){
            $district=$district->where('company_name','like','%'.$param['company'].'%');
        }
        if(isset($param['status']) && $param['status']!=''){
            $district=$district->where('order_status',$param['status']);
        }
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            $district=$district->whereHas('buses.bus',function($q)use($param){
                $q->where('sp_id',$param['sp_id']);
            });
        }
        $district = $district->get()->toArray();
//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function approve_order($param){
        
        $id = $param['id'];
        $t = $param['type'];
        
        $dep = self::where('id',$id)->first();
        if(is_null($dep)){
            return \General::error_res('no order found.');
        }
        $msg = 'no changes done.';
        if($t == 'A'){
            $dep->order_status = 1;
            $msg = 'pariwisata order approved';
        }elseif($t == 'R'){
            $dep->order_status = 2;
            $msg = 'pariwisata order cancelled';
        }
        $dep->save();
        
        if($t == 'D'){
            $dep->delete();
        }
        
        return \General::success_res($msg);
    }
}
