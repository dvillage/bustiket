<?php

namespace App\Models;
use DB;

class Lorena {


    public static function makeLorenaRequest($param,$method,$print = false,$singlePrint = false){
        $agentId = env('LORENA_AGENT_ID','ABUS');
        //$SecretKey = env('LORENA_SECRET_KEY','4bUs4g3nt!@#'); oldradhe
        $SecretKey = env('LORENA_SECRET_KEY','4bUsPRD4g3nt!@#');
        $dateTime = date('YmdHis');
        $data = [
            'AgentID'=>$agentId,
            'AgentPIN' => $agentId,
            'AgenttrxID'=>'123',
            'AgentStoreID'=>'ALL',
            'ProductID'=>'L-ORENS',
            'DateTimeRequest'=>$dateTime
        ];
//        $data = 'AgentID='.$agentId.'&AgentPIN='.$SecretKey.'&AgenttrxID=123&AgentStoreID=ALL&ProductID=L-ORENS&DateTimeRequest='.$dateTime.'&';
//        $postData = $data.$param;
        $postData = array_merge($data,$param);
        
//        $postData = $postData.'&Signature='.$signature;
        
        $sha = '';
        $prm = '';
        foreach($postData as $k=>$p){
            $sha .= $p;
            if($prm != ''){
                $prm = $prm.'&';
            }
            $prm = $prm.$k.'='.$p;
        }
//        echo $prm.'<br>';
        $sha = $sha.$SecretKey;
        $signature = sha1($sha);
//        echo 'signature seq : '.$sha.'<br>';
//        $postData['AgentPIN'] = $SecretKey;
//        $postData['Signature'] = $signature;
        
        $prm .= '&Signature='.$signature;
        
    //$url = env('LORENA_URL','http://apideveticketing.lorena-transport.com/eticketingWebService.asmx/'); oldradhe
        $url = env('LORENA_URL','apieticketing.lorena-transport.com/eticketingWebService.asmx/');

        $url = $url.$method;
        
        $postData = $prm;
//        echo 'post data : '.$postData .'<br>';
        if($print){
            echo $url.'<br>';
//            print_r($postData);
        }


        
        $ch =  curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $res = curl_exec($ch);
        \Log::info('Lorena Request URL : '.$url);
        \Log::info('payment parameters : '.$postData);
        \Log::info('sha parameters : '.$sha);
        \Log::info('curl response : '.($res));
        $getInfo = curl_getinfo($ch);
//        dd($getInfo);

        if(curl_error($ch))
        {
            
//            echo '<br>error:' . curl_error($ch);
            \Log::info('curl error : '.json_encode(curl_error($ch)));
            return [
                'ResponseCode'=>500
            ];
        }
   

        curl_close($ch);

        if($singlePrint){
            \Log::info('Lorena Request URL : '.$url);
        }
        
        return json_decode($res,true);
    }
    
    public static function getBusFromApi($from,$to,$journy_date){
        $departureTime = date('Ymd',strtotime($journy_date));
        $jdate = date('Y-m-d',  strtotime($journy_date));
        $param = [
            'DepartureTime' => $departureTime,
            'PointCatCode' => 'ALL'
        ];
        $from_id = 0;
        $to_id = 0;
        
        $key = 'lorena_terminal_list';
        $boardPoints = [];
        $dropPoints = [];
        $listFound = 0;
        if(\Cache::has($key)){
            $list = \Cache::get($key);
            $listFound = 1;
            foreach($list as $li){
                if(strtolower($li['city_name']) == strtolower($from)){
                    $boardPoints = $li['points'];
                    $from_id = $li['city_id'];
                }
                if(strtolower($li['city_name']) == strtolower($to)){
                    $dropPoints = $li['points'];
                    $to_id = $li['city_id'];
                }
            }
            
        }
//        $keyForBus = 'lorena_'.$from_id.'-'.$to_id.'-'.$jdate;
        $keyForBus = 'lorena_bus_list';
//        \Cache::forget($keyForBus);
        
        self::remove_old_bus();
        
        $keyto = 'lorena_'.$from_id.'-'.$to_id.'-'.$jdate;
        if(\Cache::has($keyForBus)){
            $cacheBus = \Cache::get($keyForBus);
//            dd($cacheBus);
            if(isset($cacheBus[$keyto])){
                $cacheBus = $cacheBus[$keyto];
                $time = strtotime($cacheBus['created_at']);
                $now = strtotime(date('Y-m-d H:i:s'));
                $diff = ceil(abs($now - $time)/60);
                if($diff < 60){
//                    dd($cacheBus['bus']);
                    \Session::put('lorena_bus', $cacheBus['bus']);
                    return $cacheBus['bus'];
                }
            }
        }
        
        if($listFound == 0){

    //        $param = 'DepartureTime='.$departureTime.'&pointCatCode=ALL';
            $AllCities = self::makeLorenaRequest($param, 'GetPointPublic');
            if($AllCities['ResponseCode'] != 00){
                return null;
            }
            $allPoints = $AllCities['Point'];
            $all = [];
            $tempMainCity = [];
            foreach($allPoints as $ap){
                if(!in_array($ap['CityCode'], $tempMainCity)){
                    array_push($tempMainCity, $ap['CityCode']);
                    $all[$ap['CityCode']][] = [ $ap['CityCode'] , $ap['CityName'] ];
                    $all[$ap['CityCode']][] = [ $ap['PointCode'] , $ap['PointName'] ];
                }else{
                    $all[$ap['CityCode']][] = [ $ap['PointCode'] , $ap['PointName'] ];
                }
            }

            $finalCity = [];
            $i = 0;
            foreach($all as $k=>$city){
                $ct = Locality\Citi::where('name',$city[0][1])->first();
                if($ct){
                    $finalCity[$i]['city_id'] = $ct->id;
                    $finalCity[$i]['city_name'] = $ct->name;
                    $finalCity[$i]['points'] = $city;
                    
                    if(strtolower($ct->name) == strtolower($from)){
                        $boardPoints = $city;
                        $from_id = $ct->id;
                    }
                    if(strtolower($ct->name) == strtolower($to)){
                        $dropPoints = $city;
                        $to_id = $ct->id;
                    }
                    
                    $i++;
                }
            }

            General::cache_api_terminals($finalCity,'lorena');
        }
        $busList = [];
        $busKey = 0;
        if(count($boardPoints) == 0 || count($dropPoints) == 0){
            return null;
        }else{
            
            $mainOrgin = $boardPoints[0][0];
            $mainDest = $dropPoints[0][0];
            
            
            unset($boardPoints[0]);
            unset($dropPoints[0]);
            
            $shedRes = [];
            $route_code_arr = [];
            foreach($boardPoints as $bps){
                foreach($dropPoints as $dps){
                    $param = [
//                        'AgenttrxID' => '1221',
//                        'AgentStoreID' => 'K329',
                        'DepartureDate' => $departureTime,
                        'Origin' => $bps[0],
                        'Destination' => $dps[0],
                    ];
                    
                    $schedule = self::makeLorenaRequest($param, 'GetTraject');
                    $shedRes[] = $schedule;
                   
//                    echo '<pre>';
//                    print_r($param);
//                    echo '</pre>';
                    if((isset($schedule['ResponseCode']) && $schedule['ResponseCode'] == 00) || isset($schedule['DepartureDate'])){
//                        dd($schedule);
                        $allRoutes = $schedule['DetailPackage'];
                        foreach($allRoutes as $ar){
                            
                            $totalSeat = $ar['UnitDetail'][0]['Available'];
                            if($totalSeat <= 0){
                                continue;
                            }
                            
                            if(!in_array($ar['PackageCode'], $route_code_arr)){
                                array_push($route_code_arr, $ar['PackageCode']);
                                $param = [
                                    'PackageCode' => $ar['PackageCode'],
                                    'DepartureTime' => $departureTime,
                                ];
                                $availSeat = self::makeLorenaRequest($param,'GetSeatAvailable');
                                if($availSeat['ResponseCode'] != 00){
                                    continue;
                                }else{
                                    $availSeats = self::prepare_available_seat($availSeat['SeatNoAvailable']);
                                }
//                                \Log::info('available seat : '.  json_encode($availSeat));
                                $leftRowCnt = $ar['LeftSeatRow'];
                                $leftColCnt = $ar['LeftSeatQty'];
                                $rightRowCnt = $ar['RightSeatRow'];
                                $rightColCnt = $ar['RightSeatQty'];
                                $seatmapArray = self::prepare_seat_map($leftColCnt,$leftRowCnt,$rightColCnt,$rightRowCnt);
                            }

                            $fare = $ar['UnitDetail'][0]['Price'];
                            
                            if($fare == 0){
                                continue;
                            }
                            
                            $busName  = $ar['Class'].' - ' .$ar['EntityCode'];
//                            $routes = $ar['PackageDescp'];
//                            $routes = $bps[1].' - '. $dps[1];
                            $routes = $from.' - '. $to;
                            
                            $stime = $ar['StartTime'];
                            $dDate = date('Ymd',strtotime($stime));
                            $dTime = date('H:i',strtotime($stime));
                            
                            $etime = $ar['EndTime'];
                            $eDate = date('Ymd',strtotime($etime));
                            $aTime = date('H:i',strtotime($etime));
                            
                            $stdate = date('Y-m-d H:i:s',strtotime($stime));
                            $endate = date('Y-m-d H:i:s',strtotime($etime));
                            
                            $duration = \General::time_elapsed_string($stdate,$endate);
                            
                            
//                            $availSeats = [];
                                     
                            $busList[$busKey]['from'] = $from;
                            $busList[$busKey]['to'] = $to;
                            $busList[$busKey]['org_code'] = $bps[0];
                            $busList[$busKey]['dest_code'] = $dps[0];
                            $busList[$busKey]['trip_date'] = $departureTime;
                            $busList[$busKey]['main_org_code'] = $mainOrgin;
                            $busList[$busKey]['main_dest_code'] = $mainDest;
                            $busList[$busKey]['route_code'] = $ar['PackageCode'].'_LORENA';
                            $busList[$busKey]['board_point'] = $bps[1];
                            $busList[$busKey]['drop_point'] = $dps[1];
                            $busList[$busKey]['total_seat'] = $totalSeat;
                            $busList[$busKey]['departure_date'] = $dDate;
                            $busList[$busKey]['arrival_date'] = $eDate;
                            $busList[$busKey]['departure_time'] = $dTime;
                            $busList[$busKey]['arrival_time'] = $aTime;
                            $busList[$busKey]['duration'] = $duration;
                            $busList[$busKey]['bus'] = $busName;
                            $busList[$busKey]['subclass'] = '';
                            $busList[$busKey]['fare'] = $fare;
                            $busList[$busKey]['route'] = $routes;
                            $busList[$busKey]['seat_map'] = $seatmapArray;
                            $busList[$busKey]['available_seat'] = $availSeats;

                            $busKey++;
                        }
                    }else{
                        $shedRes[] = $schedule;
                    }
                }
            }
            
//            dd($AllDesPoint);
//            echo '<pre>';
//            print_r($boardPoints);
//            print_r($dropPoints);
//            print_r($AllDesPoint);
//            print_r($list);
//            echo '</pre>';
            $newBusToSave = [
                'bus'=>[],
                'from_id'=>$from_id,
                'to_id'=>$to_id,
                'traveling_date'=>$jdate,
                'created_at'=>date('Y-m-d H:i:s')
            ];
            $newBus = [];
            if(count($busList) > 0){
                $newBus = self::group_bus_by_route_code($busList);

                $newBusToSave['bus'] = $newBus;
                
//                dd($newBus,$shedRes);
                
            }
            $keytosave = 'lorena_bus_list';
            $keyto = 'lorena_'.$from_id.'-'.$to_id.'-'.$jdate;
            
            if(\Cache::has($keytosave)){
                $Alllist = \Cache::get($keytosave);
            }
            $Alllist[$keyto] =$newBusToSave;
            \Cache::forever($keytosave,$Alllist);
            \Session::put('lorena_bus', $newBus);
            return $newBus;
        } 
        
        return [];
    }
    
    public static function prepare_available_seat($seats){
        $newSeats = [];
        foreach($seats as $st){
            $newSeats[] = substr($st['SeatNo'], 1);
        }
        return $newSeats;
    }
    
    public static function prepare_seat_map($leftColCnt,$leftRowCnt,$rightColCnt,$rightRowCnt){
        $seatmap = [];
        $totalRow = $leftRowCnt;
        $totalCol = $leftColCnt+$rightColCnt;
        
        for($i=1;$i<=$totalRow;$i++){
            for($j=1;$j<=$totalCol;$j++){
                
                $alp = General::getAlphabate($j-1);
                $seatmap[] = [
                    $i,$j,$i,$alp,'',''
                ];
                if($j == ($leftColCnt)){
                    $alp = '';
                    $seatmap[] = [
                        $i,9,$i,$alp,'',''
                    ];
                }
            }
        }
        
        return $seatmap;
    }
    
    public static function all_seats($bus){
        $seats = [];
        $rows = 0;
        $cols = 0;
        $r = [];
        $c = [];
//        dd($bus);
        foreach($bus['seat_map'] as $m){
            if(!in_array($m[0], $r)){
                array_push($r, $m[0]);
                $null = 0;
                $rows++;
            }
            if(!in_array($m[1], $c)){
                array_push($c, $m[1]);
                $cols++;
            }
            
            if($m[3] == ''){
                $null = 1;
            }
            if($null){
//                $m[0] = $m[0]+1;
                $m[1] = $m[1]+1;
            }
            
            if($m[3] != ''){
                $obt = 'CS';
                if($m[4] == 'T'){
                    $obt = 'T';
                }
                $seats[] = [
                    'row'=>$m[0]-1,
                    'col'=>$m[1]-1,
                    'object_type'=>$obt,
                    'seat_lbl'=>$m[2].$m[3],
                    'seat_index'=>-1
                ];
            }
            
        }
        $available_seats = $bus['available_seat'];
//        dd($bus);
        $booked = [];
        foreach($seats as $st){
            if(!in_array(trim($st['seat_lbl']), $available_seats)){
                array_push($booked, trim($st['seat_lbl']));
            }
        }
        
        if($rows > $cols){
            $seats1 = [];
            $k = 0;
            for($i=0;$i<$cols;$i++){
                for($j=0;$j<$rows;$j++){
                    foreach($seats as $key=>$s){
                        if($i == $s['col'] && $j == $s['row']){
                            $s['col'] = $j;
                            $s['row'] = $i;
                            $seats1[$k] = $s;
                        }
                    }
                    $k++;
                }
            }

            $t = $rows;
            $rows = $cols;
            $cols = $t;
            $seats = $seats1;
        }
//        dd($seats);
        $data = [
            'rows' =>$rows,
            'cols' =>$cols,
            'seats' =>$seats,
            'available_seats' =>$available_seats,
            'booked_seats' =>$booked,
            'blocked_seats' =>[],
        ];
        return $data;
    }
    
    public static function group_bus_by_route_code($rodaBus){
//        dd($rodaBus);
        $bus = [];
        $code = [];
        
        $newBus = [];
        foreach($rodaBus as $k=>$rb){
//            if(!in_array($rb['route_code'], $code)){
//                array_push($code, $rb['route_code']);
//                $bus[$rb['route_code']][] = $rb;
//            }else{
                $bus[$rb['route_code']][] = $rb;
//            }
        }
        foreach($bus as $k=>$b){
//            dd($b);
//            if(!in_array($b['org_code'], $code)){
//                array_push($code, $b['org_code']);
            $term = [];
            foreach($b as $bk=>$bb){
                if(count($term) == 0){
                    array_push($term, $bb);
                    $newBus[$k] = [
                        "from" => $bb['from'],
                        "to" => $bb['to'],
                        "trip_date" => $bb['trip_date'],
                        "main_org_code" => $bb['main_org_code'],
                        "main_dest_code" => $bb['main_dest_code'],
                        "route_code" => $bb['route_code'],
                        "total_seat" => $bb['total_seat'],
                        "bus" => $bb['bus'],
                        "route" => $bb['route'],
                        "fare" => $bb['fare'],
                        "seat_map" => $bb['seat_map'],
                        "available_seat" => $bb['available_seat'],
                    ];
                }
                $newBus[$k]['terminals'][$bb['org_code']][] = [
                    "org_code" => $bb['org_code'],
                    "dest_code" => $bb['dest_code'],
                    "board_point" => $bb['board_point'],
                    "drop_point" => $bb['drop_point'],
                    "departure_date" => $bb['departure_date'],
                    "arrival_date" => $bb['arrival_date'],
                    "departure_time" => date('H:i',strtotime($bb['departure_time'])),
                    "arrival_time" => date('H:i',strtotime($bb['arrival_time'])),
                    "duration" => $bb['duration'],
                    "subclass" => $bb['subclass'],
                    "fare" => $bb['fare'],
                ];
                
            }
                
//            }else{
//                $bus[$rb['route_code']][] = $rb;
//            }
        }
//        echo '<pre>';
//        print_r($newBus);
//        echo '</pre>';
//        echo json_encode($newBus);
        return $newBus;
    }
    
    public static function get_bus_from_db($param){
        $from_id = $param['from_id'];
        $to_id = $param['to_id'];
        $jdate = $param['date'];
        $keytosave = 'lorena_bus_list';
        $keyForBus = 'lorena_'.$from_id.'-'.$to_id.'-'.$jdate;
        if(\Cache::has($keytosave)){
            $cacheBus = \Cache::get($keytosave);
            $cacheBus = $cacheBus[$keyForBus];
            $bus = $cacheBus['bus'];
        }else{
            return \General::error_res();
        }
        $res = \General::success_res();
        $res['data'] = $bus;
        return $res;
    }
    public static function group_terminals($rbus,$param){
        $r = [];
        foreach($rbus['terminals'] as $ter){
            foreach($ter as $t){
                $r[] = [
                    'id'          => 0,
                    'route'       => $rbus['route'],
                    'from_terminal_id'     => $t['org_code'],
                    'from_terminal'   => ['name' => $t['board_point']],
                    'to_terminal_id'       => $t['dest_code'],
                    'to_terminal'     => ['name' => $t['drop_point']],
                    'boarding_time'  => $t['departure_time'],
                    'droping_time'   => $t['arrival_time'],
                    'price'       => $t['fare'],
                ]; 
            }
        }
        
//        dd($r);
        return $r;
    }
    public static function get_terminals($rbus,$param){
        $r = [];
//        dd($param);
        foreach($rbus['terminals'] as $ter){
            foreach($ter as $t){
                if($t['org_code'] == $param['from_term_id'] && $t['dest_code'] == $param['to_term_id']){
                    $r = $t;
                }
            }
        }
        if(count($r) == 0){
            return \General::error_res('no terminal combination found');
        }
        $res = \General::success_res();
        $res['data'] = $r;
//        dd($r);
        return $res;
    }
    
    public static function remove_old_bus(){
        $key = 'lorena_bus_list';
        if(\Cache::has($key)){
            $list = \Cache::get($key);
            $today = date('Y-m-d');
            $strtotime = strtotime($today);
            foreach($list as $k=>$v){
                $past = strtotime($v['traveling_date']);
//                echo $strtotime .' <--> '.$past .'<br>';
//                $list[$k]['traveling_date'] = '2017-10-31';
                if($strtotime > $past){
                    
                    unset($list[$k]);
                }
            }
            \Cache::forever($key,$list);
        }
    }
    
    public static function get_bus_info($booking_id){
        $key = 'lorena_bus_info';
        if(\Cache::has($key)){
            $list = \Cache::get($key);
            foreach($list as $k=>$v){
                if(isset($v[$booking_id])){
                    return $v[$booking_id];
                }
            }
            
        }
        return [];
    }

    public static function makeLorenaBooking($busDetail,$pass_list,$booking_id){
            $res = \General::error_res();
            $origin = $pass_list[0]['from_terminal'];
            $dest = $pass_list[0]['to_terminal'];
            $routecode = $busDetail['route_code'];
            $code   = explode('_', $routecode);
            $packageCode = $code[0];
            $session_id = $pass_list[0]['session_id'];
            $bookingUnitQnt = [
                [
                    'UnitName'=>'Penumpang',
                    'Qty'=>count($pass_list),
                ]
            ];
            $bookingUnitQnt = json_encode($bookingUnitQnt);
            $seatNo = [];
            $isSeat = 0;
            foreach($pass_list as $pas){
                if($pas['seat_lbl'] != ''){
                    $isSeat = 1;
                }
                $seatNo[] = [
                    'SeatNo'=>'0'.$pas['seat_lbl']
                ];
            }
            $seatNo = json_encode($seatNo);
            
            $date = date('Ymd',strtotime($pass_list[0]['journey_date']));
            
            $param = [
                'DepartureDate'=>$date,
                'Origin'=>$origin,
                'Destination'=>$dest,
                'PackageCode'=>$packageCode,
                'QtyBookingUnit'=>$bookingUnitQnt,
//                'SeatNo'=>$seatNo,
            ];
            
            if($isSeat){
                $param['SeatNo'] = $seatNo;
            }
            $method = $isSeat ? 'BookingTicketWithSeat' : 'BookingTicket';
            $book = self::makeLorenaRequest($param, $method);
            
            $data = [
                    'booking_id'=>$booking_id,
                    'route_code'=>$busDetail['route_code'],
                    'bus_name'=>$busDetail['bus'],
                    'book_code'=>'',
                    'pay_status'=>3,
                    'book_status'=>0,
                    'is_seat'=>$isSeat,
                    'booking_response'=>json_encode($book),
                    'payment_response'=>'',
                    'cancel_response'=>'',
                    'routes'=>$busDetail['route'],
                    'bus_detail'=>json_encode($busDetail),
                    'created_at'=>date('Y-m-d H:i:s'),
                ];
            if($book['ResponseCode'] != 00){
                $res = \General::error_res();
                $res['msg'] = 'something went wrong please try again to book ticket.';
                BookingSeats::where('session_id',$session_id)->delete();
//                return $res;
            }else{
                $book_code = $book['PartnerBookingCode'];
                $data['booking_id'] = $book_code;
                $data['book_code'] = $book_code;
                $data['book_status'] = 1;
                $res = \General::success_res();
                $res['booking_id'] = $book_code;
                $booking_id = $book_code;
            }
            
            $keyToSAve = 'lorena_bus_info';
            $saveData = [
                $booking_id =>$data
            ]; 
            if(\Cache::has($keyToSAve)){
                $list = \Cache::get($keyToSAve);
                $list[] = $saveData;
                \Cache::forever($keyToSAve,$list);
            }else{
                $list[] = $saveData;
                \Cache::forever($keyToSAve,$list);
            }
//            dd($book);
            return $res;
    }
    public static function make_lorena_payment($booking){
        $booking_id = $booking->booking_id;
        $apiBus = self::get_bus_info($booking_id);
        $book_code = $apiBus['book_code'];
        $isSeat = isset($apiBus['is_seat']) ? $apiBus['is_seat'] : 1;
//        if($apiBus['pay_status'] != 1){
            $isBook = 1;
            $checkBooking = self::lorena_booking_inquiry($book_code, $booking_id);
            if($checkBooking['ResponseCode'] == 00){
                $bookingSeats = BookingSeats::where('booking_id',$booking_id)->get()->toArray();

                $route_code = explode('_',$apiBus['route_code'])[0];
                $bookingUnitQnt = [
                        [
                            'UnitName'=>'Penumpang',
                            'Qty'=>count($bookingSeats),
                        ]
                    ];
                    $bookingUnitQnt = json_encode($bookingUnitQnt);
                    $seatNo = [];
                    $customer_detail = [];
                    $seats = [];
                    foreach($bookingSeats as $pas){
                        $seatNo[] = [
                            'SeatNo'=>'0'.$pas['seat_lbl']
                        ];
                        $seats[] = $pas['seat_lbl'];
                        $customer_detail[] = [
                            $pas['passenger_name'],
                            $pas['passenger_mobile'],
                            General::randomString(16,0)
                        ];
                    }
                    $seatNo = json_encode($seatNo);
                $detailBooking[] = [
                    'UnitName'=>'penumpang',
                    'CustomerDetail'=> $customer_detail
                ];
                $detailBooking = json_encode($detailBooking);

                $param = [
                        'PartnerBookingCode'=>$book_code,
                        'QtyBookingUnit'=>$bookingUnitQnt,
                        'DetailBookingUnit'=>$detailBooking,
//                        'SeatNo'=>$seatNo,
                    ];
                
                if($isSeat){
                    $param['SeatNo'] = $seatNo;
                }
                
                $method = $isSeat ? 'PaymentTicketWithSeat' : 'PaymentTicket';
                
                $payment = self::makeLorenaRequest($param, $method);
                if($payment == null){
                    $isBook = 0;
                }
    //            dd($payment);
                $payment_response = json_encode($payment);
                if($payment['ResponseCode'] == 00){
                    
                    $checkPayment = self::lorena_payment_inquiry($book_code, $booking_id);
                    
                    if($checkPayment['ResponseCode'] == 00){
                    
                        $newSeatsStr = explode('|',$payment['PartnerPaymentInfo'])[0];
                        $newSeatsArr = explode(';', $newSeatsStr);
                        $newSeatsArr = array_filter($newSeatsArr);

                        $newLbl = explode(',',$payment['NoSeat']);

                        foreach($bookingSeats as $k=>$bs){
                            if($isSeat == 0){
                                $bs['seat_lbl'] = trim($newLbl[$k],'0');
                            }
                            BookingSeats::where('id',$bs['id'])->update(['ticket_id'=>$newSeatsArr[$k],'seat_lbl'=>$bs['seat_lbl']]);
                        }

                        $pay_status = 1;
                        $book_status = 1;
                        $departureTime = date('Ymd',strtotime($booking->journey_date));
                        $param = [
                            'PackageCode' => $route_code,
                            'DepartureTime' => $departureTime,
                        ];
                        $availSeat = self::makeLorenaRequest($param,'GetSeatAvailable');
    //                    dd($availSeat);
                        if($availSeat['ResponseCode'] != 00){
                            $availSeats = [];
                        }else{
                            $availSeats = self::prepare_available_seat($availSeat['SeatNoAvailable']);
                        }

                        $jdate = date('Y-m-d',strtotime($booking->journey_date));
                        $f_city_id = 0;
                        $from_city = Locality\Citi::where('name',$booking->from_city)->first();
                        if($from_city){
                            $f_city_id = $from_city->id;
                        }
                        $t_city_id = 0;
                        $to_city = Locality\Citi::where('name',$booking->to_city)->first();
                        if($to_city){
                            $t_city_id = $to_city->id;
                        }
                        $keyList = 'lorena_bus_list';
                        $keyto = 'lorena_'.$f_city_id.'-'.$t_city_id.'-'.$jdate;
                        if(\Cache::has($keyList)){
                            $busList = \Cache::get($keyList);
                            $route_code = $apiBus['route_code'];
    //                        $avalSeat = $busList[$keyto]['bus'][$route_code]['available_seat'];
    //                        $leftSeat = array_diff($avalSeat, $seats);
                            $leftSeat = $availSeats;
                            $busList[$keyto]['bus'][$route_code]['available_seat'] = $leftSeat;
                            $busList[$keyto]['bus'][$route_code]['total_seat'] = count($leftSeat);
                            \Cache::forever($keyList,$busList);
                        }
                    }else{
                        $isBook = 0;
                        $pay_status = 0;
                        $book_status = 0;
                        $payment_response = json_encode($checkPayment);
                    }
                }else{
                    $pay_status = 0;
                    $book_status = 1;
                }
                
//            dd($param);

            }else{
                $isBook = 0;
                $pay_status = 0;
                $book_status = 0;
                $payment_response = json_encode($checkBooking);
            }
       
            $infoKey = 'lorena_bus_info';
            if(\Cache::has($infoKey)){
                $infoBus = \Cache::get($infoKey);
                foreach($infoBus as $ke=>$ib){
                    if(isset($ib[$booking_id])){
                        $infoBus[$ke][$booking_id]['pay_status'] = $pay_status;
                        $infoBus[$ke][$booking_id]['book_status'] = $book_status;
                        $infoBus[$ke][$booking_id]['payment_response'] = $payment_response;
                    }
                }
                \Cache::forever($infoKey,$infoBus);
            }
            
            if($isBook == 0){
                return \General::error_res();
            }
//        }     
    }
    
    public static function lorena_booking_inquiry($book_code,$booking_id){
        $param = [
            'PaymentTrxID' => $booking_id,
            'PartnerBookingCode' => $book_code,
        ];
        
        $inq = self::makeLorenaRequest($param, 'GetInquiryStatusBooking');
        return $inq;
    }
    public static function lorena_payment_inquiry($book_code,$booking_id){
        $param = [
            'PaymentTrxID' => $booking_id,
            'PartnerBookingCode' => $book_code,
        ];
        
        $inq = self::makeLorenaRequest($param, 'GetInquiryStatusPayment');
        return $inq;
    }
    
    public static function lorena_cancel_ticket($booking){
        
        $booking_id = $booking->booking_id;
        $apiBus = self::get_bus_info($booking_id);
        $book_code = $apiBus['book_code'];
        
        /*$param = [
            'PartnerBookingCode'=>$book_code,
            'ReversalType'=>'REVBOOK',
        ];
        $method = 'ReversalTicket';
        $revBook = self::makeLorenaRequest($param, $method);
        
        $response = json_encode($revBook);
        if($revBook['ResponseCode'] == 00){
            
            $param['ReversalType'] = 'REVPAY';
            
            $revPay = self::makeLorenaRequest($param, $method);
            $response = json_encode($revPay);
            if($revPay['ResponseCode'] == 00){
                $res = \General::success_res();
                $res['refund'] = 0;
            }else{
                \Log::info('lorena payment reverse fail. : '.$response);
                $res =  \General::error_res('booking not found or already cancelled.');
            }
        }else{
            \Log::info('lorena booking reverse fail. : '.$response);
            $res =  \General::error_res('booking not found or already cancelled.');
        }*/

        $isBook = self::lorena_payment_inquiry($book_code, $booking_id);
//        dd($isBook);
        $response = json_encode($isBook);
        if($isBook['ResponseCode'] == 00){
            $param = [
                'PartnerBookingCode'=>$book_code,
                'ReversalType'=>'REVPAY',
            ];
            $method = 'ReversalTicket';
            $revPay = self::makeLorenaRequest($param, $method);

            $response = json_encode($revPay);
            if($revPay['ResponseCode'] == 00){
                $res = \General::success_res();
                $res['refund'] = 0;

            }else{
                \Log::info('lorena payment reverse fail. : '.$response);
                $res =  \General::error_res('booking not found or already cancelled.');
            }
        }else{
            \Log::info('lorena payment not found. : '.$response);
            $res =  \General::error_res('booking not found or already cancelled.');
        }
        
        $infoKey = 'lorena_bus_info';
        if(\Cache::has($infoKey)){
            $infoBus = \Cache::get($infoKey);
            foreach($infoBus as $ke=>$ib){
                if(isset($ib[$booking_id])){
                    $infoBus[$ke][$booking_id]['cancel_response'] = $response;
                }
            }
            \Cache::forever($infoKey,$infoBus);
        }
        
        return $res;
    }
    
    public static function lorena_all_city(){
        
        $journy_date = date('Y-m-d');
        
        $departureTime = date('Ymd',strtotime($journy_date));
        $jdate = date('Y-m-d',  strtotime($journy_date));
        $param = [
            'DepartureTime' => $departureTime,
//            'PointCatCode' => 'ALL'
        ];
        
        $AllCities = self::makeLorenaRequest($param, 'GetPointPublic');
        dd($AllCities);
    }
        
}


/* $scheduleSample = '{
            "DepartureDate":"20160126",
            "Origin":"BBGR",
            "Destination":"BKKE",
            "ProductID":"L-ORENS",
            "DateTimeResponse":"20160124214425",
            "DetailPackage":[
                {
                    "EntityCode":"ESLT",
                    "PackageCode":"KE-554",
                    "PackageDescp":"Bogor - Sumenep",
                    "StartTime":"20160126090000",
                    "EndTime":"20160127120000",
                    "Class":"Executive",
                    "LeftSeatQty":"2",
                    "LeftSeatRow":"8",
                    "RightSeatQty":"2",
                    "RightSeatRow":"8",
                    "PartnerInfo":"Fasilitas = Makan 1 kali",
                    "UnitDetail":[
                        {
                            "UnitName":"Penumpang",
                            "LevelMin":"0 TAHUN",
                            "LevelMax":"99 TAHUN",
                            "Price":"395000",
                            "Available":"6"
                        }
                    ]
                }
            ]
        }';
    $schedule = json_decode($scheduleSample,true);*/