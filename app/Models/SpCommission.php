<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SpCommission extends Model {
    protected $fillable = [
         'sp_id','booking_id','com_amount','handle_fee','comm_type','h_type','pay_status'
    ];
    protected $table = 'sp_commission';
    protected $hidden = [];
    public $timestamps=true;
    
    public function sp(){
        return $this->hasOne('\App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    public function sp_comm(){
        return $this->hasOne('\App\Models\SpCommission','booking_id','booking_id');
    }
    
    
    public static function sp_commission($login_id,$ticket_id,$fare,$tot_seat){
                $ex= self::where('booking_id',$ticket_id)->where('sp_id',$login_id)->first();
                if($ex){
                    return;
                }
                
                $sp = Serviceprovider\ServiceProvider::where('id',$login_id)->first();
                $fetchagent = $sp->toArray();

                $commval=$fetchagent['comm_fm_sp'];
                $commtype=$fetchagent['comm_fm_sp_type'];
                $handval=$fetchagent['handling_fee'];
                $handtype=$fetchagent['handling_fee_type'];
                $totam=$fare * $tot_seat;
                if($commtype == 'P'){
                   $comval=(($totam*$commval)/100);
               }else{
                   $comval=$commval*$tot_seat;
               }
               if($handtype == 'P'){
                    $hval=  ceil(($totam*$handval)/100);
                }else{
                    $hval=$handval*$tot_seat;
                }
               
                $com = new self;
                $com->sp_id = $login_id;
                $com->booking_id = $ticket_id;
                $com->com_amount = $comval;
                $com->handle_fee = $hval;
                $com->comm_type = $commtype;
                $com->h_type = $handtype;
                $com->pay_status = 0;
                $com->save();
                
                
                $sp->balance = $sp->balance + $comval;
                $sp->save();
                
                
    }
    public static function delete_commission($booking_id,$user_id){
        
        $com = self::where('booking_id',$booking_id)->where('sp_id',$user_id)->first();
        if($com){
            $sp = Serviceprovider\ServiceProvider::where('id',$user_id)->first();
            $sp->balance = $sp->balance - $com->com_amount;
            $sp->save();
            
            self::where('booking_id',$booking_id)->where('sp_id',$user_id)->delete();
        }
    }
    public static function update_commission($booking_id,$total_cancelled,$total_seats,$user_id){
        $com = self::where('booking_id',$booking_id)->where('sp_id',$user_id)->first();
        $amt = $com->com_amount;
        $ded = $amt/$total_seats;
        $com->com_amount = $com->com_amount - ($ded*$total_cancelled);
        $com->save();
        
        $ss = Serviceprovider\ServiceProvider::where('id',$user_id)->first();
        $ss->balance = $ss->balance - $com->com_amount;
        $ss->save();
    }
    public static function filter_commlist($param){

//        dd($param);
        $prvd = '';
        $count = Bookings::active()->orderBy('created_at','DESC');
//        dd($count);
        if(isset($param['fromdate']) && $param['fromdate'] != '' && isset($param['todate']) && $param['todate'] != ''){
            
            $start_date = date('Y-m-d', strtotime($param['fromdate']));
            $end_date   = date('Y-m-d', strtotime($param['todate']));
            $count      = $count->whereBetween('journey_date', [$start_date, $end_date]);
        }else{
            if(isset($param['fromdate']) && $param['fromdate'] != ''){
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date   = date('Y-m-d');
                $count      = $count->whereBetween('journey_date', [$start_date, $end_date]);
            }
            if(isset($param['todate']) && $param['todate'] != ''){
                $start_date = date('Y-m-d');
                $end_date   = date('Y-m-d', strtotime($param['todate']));
                $count      = $count->whereBetween('journey_date', [$start_date, $end_date]);
            }
        }
        
        if(isset($param['busop']) && $param['busop'] != ''){
            $count = $count->where('sp_id',$param['busop']);
        }
        if(isset($param['seller']) && $param['seller'] != ''){
            $count = $count->where('user_id',$param['seller']);
        }
        if(isset($param['month']) && $param['month'] != ''){
            $count = $count->whereMonth('journey_date','=',$param['month']);
        }
        if(isset($param['utype']) && $param['utype'] != ''){
            $count = $count->where('book_by',$param['utype']);
        }
        if(isset($param['any']) && $param['any'] != ''){
//            $count = $count->where('to_city',$param['to']);
        }
        if(isset($param['id']) && $param['id'] != ''){
            $chlds = Serviceprovider\ServiceProvider::where('parent_id',$param['id'])->pluck('id');
            $prvd = Serviceprovider\ServiceProvider::where('id',$param['id'])->value('type');
            if($prvd != 'Main'){
                $count = $count->whereIn('book_by',[2,3,4]);
                
                $count = $count->where(function($q) use($param,$chlds){
                    return $q->where('user_id',$param['id'])->orWhereIn('user_id',$chlds);
                });
            }else{
                $count = $count->where('sp_id',$param['id']);
            }
//            $count = $count->where('user_id',$param['id'])->orWhereIn('user_id',$chlds);
            
        }
        
        $count = $count->count();
        $len = 0;
        
        if(!isset($param['download'])){
            $page = $param['crnt'];
            $len  = $param['len'];
            $op   = $param['opr'];
            $total_page=ceil($count/$len);
            $flag = 1;

            $start;

            if($op != ''){
                if($op == 'first'){
                    $crnt_page = 1;
                    $start = ($crnt_page - 1) * $len;
                }elseif($op == 'prev'){
                    $crnt_page = $page - 1;
                    if($crnt_page <= 0){
                        $crnt_page = 1;
                    }
                    $start = ($crnt_page - 1 ) * $len;
                }elseif($op == 'next'){
                    $crnt_page = $page + 1;
                    if($crnt_page >= $total_page){
                        $crnt_page = $total_page;
                    }
                    $start = ($crnt_page - 1 ) * $len;
                }else{
                    $crnt_page = $total_page;
                    $start = ($crnt_page - 1 ) * $len;
                }
            }else{
                if($page > $total_page){
                    $flag = 0;
                    $crnt_page = $page - 1;
                    $start = ($crnt_page - 1) * $len;
                }else{
                    $crnt_page = $page;
                    $start = ($crnt_page - 1) * $len;
                }
            }
        }
        $commlist = Bookings::active()->orderBy('created_at','DESC');
        
        if(isset($param['fromdate']) && $param['fromdate'] != '' && isset($param['todate']) && $param['todate'] != ''){
                $crnt_page  = 1;
                $start      = ($crnt_page - 1) * $len;
                
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date   = date('Y-m-d', strtotime($param['todate']));
                $commlist   = $commlist->whereBetween('journey_date', [$start_date, $end_date]);
        }else{
            if(isset($param['fromdate']) && $param['fromdate'] != ''){
                $crnt_page  = 1;
                $start      = ($crnt_page - 1) * $len;
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date   = date('Y-m-d');
                $commlist   = $commlist->whereBetween('journey_date', [$start_date, $end_date]);
            }
            if(isset($param['todate']) && $param['todate'] != ''){
                $crnt_page  = 1;
                $start      = ($crnt_page - 1) * $len;
                $start_date = date('Y-m-d');
                $end_date   = date('Y-m-d', strtotime($param['todate']));
                $commlist   = $commlist->whereBetween('journey_date', [$start_date, $end_date]);
            
                }
        }
        
        if(isset($param['busop']) && $param['busop'] != ''){
            $crnt_page  = 1;
            $start      = ($crnt_page - 1) * $len;
            $commlist   = $commlist->where('sp_id',$param['busop']);
        }
        if(isset($param['seller']) && $param['seller'] != ''){
            $crnt_page  = 1;
            $start      = ($crnt_page - 1) * $len;
            $commlist   = $commlist->where('user_id',$param['seller']);
        }
        
        if(isset($param['month']) && $param['month'] != ''){
            $crnt_page  = 1;
            $start      = ($crnt_page - 1) * $len;
            $commlist   = $commlist->whereMonth('journey_date','=',$param['month']);
        }
        if(isset($param['utype']) && $param['utype'] != ''){
            $crnt_page  = 1;
            $start      = ($crnt_page - 1) * $len;
            $commlist   = $commlist->where('book_by', $param['utype']);
        }
        
        if(isset($param['id']) && $param['id'] != ''){
//            $crnt_page  = 1;
//            $start      = ($crnt_page - 1) * $len;
            $chlds      = Serviceprovider\ServiceProvider::where('parent_id',$param['id'])->pluck('id');
            if($prvd != 'Main'){
                $commlist = $commlist->whereIn('book_by',[2,3,4]);
    //            $commlist = $commlist->where('user_id',$param['id'])->orWhereIn('user_id',$chlds);
                $commlist   = $commlist->where(function($q) use($param,$chlds){
                    return $q->where('user_id',$param['id'])->orWhereIn('user_id',$chlds); 
                });
            }else{
                $commlist = $commlist->where('sp_id',$param['id']);
            }
        }
        
        
//        if(isset($param['tid']) && $param['tid'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('booking_id',$param['tid']);
//        }
//        if(isset($param['date']) && $param['date'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->whereDate('journey_date','=',date('Y-m-d', strtotime($param['date'])));
//        }
//        if(isset($param['from']) && $param['from'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('from_city',$param['from']);
//        }
//        if(isset($param['to']) && $param['to'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('to_city',$param['to']);
//        }
        
        if(isset($param['download'])){
            $commlist = $commlist->with(['sp_comm','sp_comm.sp','sp','admin_comm'])->get()->toArray();
            return $commlist;
        }
        
        $commlist = $commlist->with(['sp_comm','sp_comm.sp','sp','admin_comm'])->skip($start)->take($len)->get()->toArray();

        $res['len'] = $len;
        $res['crnt_page']   = $crnt_page;
        $res['total_page']  = $total_page;
        
        $res['result']  = $commlist;
        $res['flag']    = $flag;
        return $res;
    }
}
