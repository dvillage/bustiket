<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ApiBusInfo extends Model {
    protected $fillable = [
        'booking_id', 'route_code', 'bus_name','book_code','pay_status','book_status','booking_response','payment_response','routes','bus_detail'
    ];
    protected $table = 'api_bus_info';
    protected $hidden = [];
    public $timestamps=false;
    
    
    
    public static function add_bus_info($param){
        $in = new self;
        $in->booking_id = $param['booking_id'];
        $in->route_code = $param['route_code'];
        $in->bus_name = $param['bus_name'];
        $in->is_seat = $param['is_seat'];
        $in->book_code = $param['book_code'];
        $in->pay_status = $param['pay_status'];
        $in->book_status = $param['book_status'];
        $in->booking_response = $param['booking_response'];
        $in->payment_response = $param['payment_response'];
        $in->routes = $param['routes'];
        $in->bus_detail = $param['bus_detail'];
        $in->save();
    }

}
