<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Card extends Model {

    
    public $table = 'cards';
    protected $fillable = array('status','name','price','charge','bank_name','card_image','detail','functionality','use');
    public $timestamps=true;
//    protected $appends = array('role');
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function courier(){
        return $this->hasMany('App\Models\Admin\CardCourier','card_id','id');
    }
    
    public static function get_card($param){
        $am = self::orderBy('id','desc');
        if(isset($param['name'])){
            $am = $am->where('name','like','%'.$param['name'].'%');
        }
        $am = $am->get();
        return $am;
    }
    
    public static function add_card($param){
//        dd($param);
        $bnrimg = \Request::file('card_image');
        
        $img_name = self::add_image($bnrimg);
        
        $d = new self;
        $d->status=isset($param['status']) ? 1 : 0 ;
        $d->name=$param['name'];
        $d->price=$param['price'];
        $d->charge=$param['charge'];
        $d->card_image=$img_name;
        $d->bank_name=$param['bank_name'];
        $d->detail=$param['detail'];
        $d->functionality=$param['functionality'];
        $d->use=$param['use'];
        $d->save();
        
        if(isset($param['courier_array'])){
            $amen = json_decode($param['courier_array'],true);
            if(count($amen) > 0){
                foreach($amen as $a){
                    $ad = new CardCourier();
                    $ad->card_id = $d->id;
                    $ad->courier_id = $a;
                    $ad->save();
                }
            }
        }
        
        return $d;
    }
    
    public static function edit_card($param){
//        dd($param);
        $d = self::where('id',$param['cid'])->first();
        $filename = $d->card_image;
        $bnrimg = \Request::file('card_image');
        $img_name = self::add_image($bnrimg);
        
        if($img_name != ''){
            $d->card_image=$img_name;
            $filePath = config('constant.UPLOAD_CARD_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        
        $d->status=isset($param['status']) ? 1 : 0 ;
        $d->name=$param['name'];
        $d->price=$param['price'];
        $d->charge=$param['charge'];
        $d->bank_name=$param['bank_name'];
        $d->detail=$param['detail'];
        $d->functionality=$param['functionality'];
        $d->use=$param['use'];
        $d->save();
        
        if(isset($param['courier_array'])){
            $amen = json_decode($param['courier_array'],true);
            if(count($amen) > 0){
                foreach($amen as $a){
                    $ae = CardCourier::where('card_id',$d->id)->where('courier_id',$a)->first();
                    if(is_null($ae)){
                        $ad = new CardCourier();
                        $ad->card_id = $d->id;
                        $ad->courier_id = $a;
                        $ad->save();
                    }
                    
                }
                $aem = CardCourier::where('card_id',$d->id)->get();
                foreach($aem as $am){
                    if(!in_array($am['courier_id'], $amen)){
                        $aem = CardCourier::where('card_id',$d->id)->where('courier_id',$am['courier_id'])->delete();
                    }
                }
            }else{
                CardCourier::where('card_id',$d->id)->delete();
            }
        }else{
            CardCourier::where('card_id',$d->id)->delete();
        }
        
        return $d;
    }
    public static function delete_card($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        $filename = $d->card_image;
        
        $d->delete();
        
        if($filename != ''){
            $filePath = config('constant.UPLOAD_CARD_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        
        return $d;
    }
    
    public static function add_image($bnrimg){
        
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$img_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    $filePath = config('constant.UPLOAD_CARD_DIR_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($img_name != ''){
                        $img_name = $img_name.','.$FileName;
                    }else{
                        $img_name = $FileName;
                    }
                }
//            
            
        }
        return $img_name;
    }


    public static function get_card_list($param){
        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $count=$count->where('name','like','%'.$param['search'].'%');
        }
        
        if(isset($param['status']) && $param['status']!=''){
            $count=$count->where('status',$param['status']);
        }
        if(isset($param['bank_name']) && $param['bank_name']!=''){
            $count=$count->where('bank_name','like','%'.$param['bank_name'].'%');
        }
        
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $card=self::skip($start)->take($len)->orderBy('id','desc')->with('courier.courier');
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $card=self::orderBy('id','desc')->with('courier.courier')->skip($start)->take($len);
            $card=$card->where('name','like','%'.$param['search'].'%');
           
        }
        
        if(isset($param['status']) && $param['status']!=''){
            $card=$card->where('status',$param['status']);
        }
        if(isset($param['bank_name']) && $param['bank_name']!=''){
            $card=$card->where('bank_name','like','%'.$param['bank_name'].'%');
        }
        $card = $card->get()->toArray();
//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$card;
        $res['flag']=$flag;
        return $res;
    }
}
