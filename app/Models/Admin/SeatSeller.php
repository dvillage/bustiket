<?php

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class SeatSeller extends Model implements Authenticatable {

    use AuthenticableTrait;
    
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthIdentifierName() {
        return $this->getKeyName();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
        return $this->{$this->getRememberTokenName()};
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public function setRememberToken($value) {
        $this->{$this->getRememberTokenName()} = $value;
    }
    
    public function locParent(){
        return $this->hasOne('App\Models\Admin\SeatSeller','id','parent_id');
    }
    
    public $table = 'ss';
    protected $fillable = array('status', 'parent_id', 'name', 'email', 'mobile', 'balance','comm','comm_type');
//    protected $appends = array('role');
    public $timestamps = true;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    public function parentInfo(){
        return $this->hasOne('App\Models\Admin\SeatSeller','id','parent_id');
    }
    
    public static function get_mainss(){
        $splist=self::where('type','Main')->get(['id','name'])->toArray();
        return $splist;
    }
    
    public static function doLogin($param){
        if(isset($param['remember']))
        {
            \Cookie::get("remember",1);
            if($param['remember']=='on')
                $param['remember']=1;
            else
                $param['remember']=0;
//            \App\Models\Admin\Settings::set_config(['sanitize_input' => $param['remember']]);
        }
        $user = SeatSeller::active()->where("email", $param['uname'])->first();
        $res['data']=$user;
        $res['flag']=0;
        if (is_null($user)) {
            $res['flag']=0;
            return $res;
        }
        if (!\Hash::check($param['password'], $user->password)) {
            $res['flag']=0;
            return $res;
        }
        if(isset($param['remember']) && $param['remember']==1)
        {
            $auth_token = \App\Models\SeatSeller\Token::generate_auth_token();
            
            $token_data = ['ss_id' => $user->id,'token' => $auth_token,'type' => 'auth'];
            \App\Models\SeatSeller\Token::save_token($token_data);
            \Auth::guard("ss")->loginUsingId($user->id,true);
        }
        else{
            \Auth::guard("ss")->loginUsingId($user->id);
        }
        \Auth::guard('sp')->logout();
        \Auth::guard('admin')->logout();
        \Auth::guard('user')->logout();
        $res['flag']=1;
        return $res;
    }
    
    public static function do_login($param) {
        
//        \General::dd("do_login");
//        dd($param);
        $seatSeller = self::active()->with('locParent')
                ->where("email", $param['agent_email'])
                ->first();
        if (is_null($seatSeller)) {
            return \General::error_res("Account does not exists");
        }
        
        if (!\Hash::check($param['agent_password'], $seatSeller->password) ) {
            return \General::error_res("invalid_email_password");
        }

        $dead_token_id = \App\Models\SeatSeller\Token::find_dead_token_id(\Config::get("constant.AUTH_TOKEN_STATUS"), $seatSeller->id);
        $platform = app("platform");
        $token = \App\Models\SeatSeller\Token::generate_auth_token();
        if ($token == "")
            return \General::error_res("try_again");

        $data = ["type" => \Config::get("constant.AUTH_TOKEN_STATUS"), "platform" => $platform, "ss_id" => $seatSeller->id, "token" => $token, "ip" => \Request::getClientIp(), "ua" => \Request::server("HTTP_USER_AGENT")];

        if ($dead_token_id) {
            $data['id'] = $dead_token_id;
        }
        \App\Models\SeatSeller\Token::save_token($data);
        if (isset($param['device_token'])) {
            $seatSeller->device_token = $param['device_token'];
            $seatSeller->device_type = app("platform");
            $seatSeller->save();
        }

        $seatSeller_data = $seatSeller->toArray();
        $seatSeller_data['auth_token'] = $token;
        $seatSeller_data['mobile'] = \App\Models\Services\General::formate_mobile_no($seatSeller_data['mobile']);
        $data = [
            'agent_id'          => $seatSeller_data['id'],
            'agent_name'        => $seatSeller_data['name'],
            'agent_email'       => $seatSeller_data['email'],
            'agent_mobile'      => $seatSeller_data['mobile'],
            'agent_address'     => $seatSeller_data['address'],
            'agent_city'        => $seatSeller_data['city'],
            'agent_commission'  => $seatSeller_data['comm'],
            'agent_joindate'    => $seatSeller_data['created_at'],
            'agent_status'      => $seatSeller_data['status'],
            'agnt_amt'          => $seatSeller_data['balance'],
            'agent_type'        => $seatSeller_data['type'],
            'tax'               => $seatSeller_data['tax'],
            'paypal_email'      => $seatSeller_data['paypal_email'],
            'auth_token'        => $seatSeller_data['auth_token'],
        ];
        $res = \General::success_res("Login Success");
//        $res['data'] = $seatSeller_data;
        $res['data'] = $data;
        return $res;
    }
    
    public static function forget_password($param) {
        $user = self::where("email", $param['agent_email'])->first();
        if (is_null($user)) {
            return \General::error_res("email_not_found");
        }
        if ($user->status == config("constant.USER_INACTIVE_STATUS")) {
            return \General::error_res("Account Inactive");
        }
        $pass = $user->password;
        $user_detail = $user->toArray();
        $user_detail['password'] = $pass;
        $user_detail['mail_subject'] = \Lang::get("success.forgetpassword_mail_subject");
        \Mail::send('emails.seatseller.forget_password', $user_detail, function ($message) use ($user_detail) {
            $message->to($user_detail['email'])->subject(\Lang::get("success.forgetpassword_mail_subject"));
        });
        return \General::success_res("forgot_password_mail_sent");
    }
    
    
    public static function get_seatsellerfilter($param){
       
        $count=self::where('type',$param['type']);
        if(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') == 'Main'){
            $count = $count->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
        }elseif(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') != 'Main'){
            $count = $count->where('id',config('constant.CURRENT_LOGIN_ID'));
        }
        $count = $count->count();
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('type',$param['type'])->where('name','like','%'.$param['search'].'%');
            if(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') == 'Main'){
                $count = $count->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
            }elseif(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') != 'Main'){
                $count = $count->where('id',config('constant.CURRENT_LOGIN_ID'));
            }
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::where('type',$param['type'])->with('parentInfo');
        if(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') == 'Main'){
            $spdata = $spdata->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
        }elseif(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') != 'Main'){
            $spdata = $spdata->where('id',config('constant.CURRENT_LOGIN_ID'));
        }
        $spdata = $spdata->skip($start)->take($len)->get()->toArray();
        
//        print_r($spdata);
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::where('type',$param['type'])->where('name','like','%'.$param['search'].'%')->with('parentInfo');
            if(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') == 'Main'){
                $spdata = $spdata->where('parent_id',config('constant.CURRENT_LOGIN_ID'));
            }elseif(config('constant.LOGGER') == 'SS' && config('constant.SS_TYPE') != 'Main'){
                $spdata = $spdata->where('id',config('constant.CURRENT_LOGIN_ID'));
            }
            $spdata = $spdata->skip($start)->take($len)->get()->toArray();

        }
        
//        dd($spdata);
        $in = [5,6,7];
        foreach($spdata as $k=>$v){
            $tik = \App\Models\Bookings::where('user_id',$v['id'])->whereIn('book_by',$in)->count();
            $spdata[$k]['total_ticket'] = $tik;
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        
        return $res;
    }
    
    public static function delete_seatseller($param){
        $res=self::where('id',$param['id'])->delete();
        return $res;
    }
    
    public static function add_seatseller($param){
//        dd($param);
        $new = new SeatSeller;
        $new->type=$param['type'];
        $new->status=isset($param['status'])?$param['status']:0;
        $new->parent_id=isset($param['ssparent'])?$param['ssparent']:0;
        $new->name=$param['ssname'];
        $new->email=$param['ssemail'];
        $new->mobile=$param['sscon'];
        $new->city=$param['sscity'];
        $new->password=\Hash::make($param['sspassword']);
        $new->balance=0;
        $new->comm=$param['sscomm'];
        $new->comm_type=$param['comm_type'];
        $new->handling_fee=$param['sshf'];
        $new->handling_fee_type=$param['hf_type'];
        $new->save();
        
        $res = \General::success_res();
        return $res;
        
    }
    public static function edit_seatseller($param){
//        dd($param);
        $new = self::where('id',$param['sid'])->first();
        $new->type=$param['type'];
        if(!isset($param['profile'])){
            $new->status=isset($param['status'])?$param['status']:0;
        }
        $new->parent_id=isset($param['ssparent'])?$param['ssparent']:0;
        $new->name=$param['ssname'];
//        $new->email=$param['ssemail'];
        if(!empty($param['sspassword'])){
            $new->password=\Hash::make($param['sspassword']);
        }
        
        $new->mobile=$param['sscon'];
        $new->city=$param['sscity'];
        $new->comm=isset($param['sscomm'])?$param['sscomm'] : $new->comm;
        $new->comm_type=isset($param['comm_type'])?$param['comm_type']:$new->comm_type;
        $new->handling_fee=$param['sshf'];
        $new->handling_fee_type=$param['hf_type'];
        $new->save();

        $res = \General::success_res();
        return $res;
        
    }
    
    public static function get_seatseller_details($id){
        $ss = self::where('id',$id)->first()->toArray();
        return $ss;
    }
    public static function getProfile(){
        $admin_detail = self::where("id", \Auth::guard('ss')->user()->id)->first()->toArray();
//        dd($admin_detail);
        $res['name']=$admin_detail['name'];
//        $res['lname']=$admin_detail['last_name'];
        $res['email']=$admin_detail['email'];
        $res['flag']=1;
        
        return $res;
    }
    public static function check_parent($id,$parent){
        $sp = self::where('id',$id)->where('parent_id',$parent)->first();
        if(is_null($sp)){
            return 0;
        }
        return 1;
    }
    
    public static function get_allss($param = array()){
//        $splist=self::orderBy('created_at','DESC');
        if(isset($param['name'])){
            $sslist = self::active()->where('name','like','%'.$param['name'].'%');
        }
        $sslist = $sslist->get(['id','name'])->toArray();
        return $sslist;
    }
    
    public static function change_admin_password($param)
    {
//        dd($param);
        $admin_detail = self::where("id", \Auth::guard('ss')->user()->id)->first();
        $res['data']= $admin_detail;
        $res['flag']=0;
        $res['msg']="";
        
        if (is_null($admin_detail)) {
            
            return $res;
        }
           
        if(\Hash::check($param['old_password'],$admin_detail->password))
        {
            if($param['new_password'] == $param['confirm_password'])
            {
                $admin_detail->name = $param['name'];
//                $admin_detail->last_name = $param['lname'];
//                $admin_detail->email = $param['email'];
                $admin_detail->password = \Hash::make($param['new_password']);
                $admin_detail->save();
                
//                \App\Models\Admin\Token::delete_token();
                
                $res['data']= $admin_detail;
                $res['flag']=1;
                $res['msg']="Admin password updated successfullly.";
                return $res;
            }
            else
            {
                $res['data']= $admin_detail;
                $res['flag']=0;
                $res['msg']="New and Confirm password do not match.";
                return $res;
            }
        }
        else
        {
            $res['msg']="Wrong Old Password.";
            return $res;
//            return \General::error_res("Wrong Password.");
        }
    }
    
    public static function get_handle_fee($param){
        $handlefee = self::where('id',$param)->get(['id','name','type','handling_fee','handling_fee_type'])->toArray();
        return $handlefee;
    }
    
    public static function search_bus($param) {
        
        $res = \General::error_res("No bus found");
        $traveling_date = $param['date'];
//        $three_days = date("Y-m-d",strtotime("+3 days"));
        $today = date("Y-m-d");
        $diff = strtotime($today) - strtotime($traveling_date);
        if( $diff > 0 )
        {
            return \General::error_res("Select booking from today or after today's date");
        }
        if ($param['type'] == config("constant.SEAT_SELLER_MAIN")) {
            $res = self::search_bus_seat_seller_main($param);
        } else if ($param['type'] == config("constant.SEAT_SELLER_A")) {
            $res = self::search_bus_seat_seller_a($param);
        } else if ($param['type'] == config("constant.SEAT_SELLER_B")) {
            $res = self::search_bus_seat_seller_b($param);
        }
        return $res;
    }
    
    public static function search_bus_seat_seller_main($param) {
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from' => [
                'name' => $from_city['name'],
                'type' => $from_city['type'],
            ],
            'to' => [
                'name' => $to_city['name'],
                'type' => $to_city['type'],
            ],
            'date'      => $param['date'],
            'nop'       => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);

        $businfo_data = [];
        $operator = [];
//        dd($buses);
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }
                
                $data = [
                    'from' => [
                        'name' => $from_city['name'],
                        'type' => $from_city['type'],
                    ],
                    'to' => [
                        'name' => $to_city['name'],
                        'type' => $to_city['type'],
                    ],
                    'date' => $param['date'],
                    'nop' => isset($param['nop']) ? $param['nop'] : 1,
//                    'bus_id' => $param['bus_id'],
                    'bus_id' => $val['bus']['id'],
                ];

                $routes_data = \App\Models\Admin\BusRoutes::get_bus_routes($data,$param);
                
                $routes = [];
                foreach($routes_data as $key=>$r){
                    $d = [
                        'r_id'          => $r['id'],
                        'r_route'       => $r['route'],
                        'r_from_id'     => $r['from_terminal_id'],
                        'r_from_name'   => $r['from_terminal']['name'],
                        'r_to_id'       => $r['to_terminal_id'],
                        'r_to_name'     => $r['to_terminal']['name'],
                        'r_board_time'  => $r['boarding_time'],
                        'r_drop_time'   => $r['droping_time'],
        //                'r_price'       => $r['price'],
                        'r_price'       => \General::number_format($r['price']),
                    ];
                    $routes[] = $d;
                }
//        dd($routes_data);
//        if(isset($routes_data['flag']) && $routes_data['flag'] == 0){
//            return $routes_data;
//        }

                $businfo_data[] = [
                    'Bus_id'            => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem'           => rtrim($luxitem, ", "),
                    'rute'              => $val['routes'][0]['route'],
                    'parent_busname'    => $val['bus']['name'],
                    'available_seats'   => $val['bus']['total_seats'],
                    'bus_fare'          => \General::number_format($val['bus']['price'], 3),
                    'time'              => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration'          => $val['routes'][0]['duration'],
//                    'points'            => [
//                        "boarding" =>[
//                            "boarding_name"     => $val['routes'][0]['from_terminal']['name'],
//                            "boarding_address"  => $val['routes'][0]['from_terminal']['address'],
//                            "boarding_time"     => date('H:i', strtotime($val['routes'][0]['boarding_time'])),
//                        ],
//                        "dropping" =>[
//                            "dropping_name"     => $val['routes'][0]['to_terminal']['name'],
//                            "dropping_address"  => $val['routes'][0]['to_terminal']['address'],
//                            "dropping_time"     => date('H:i', strtotime($val['routes'][0]['droping_time'])),
//                        ],
//                    ],
                    'routes'            => $routes,
                ];
            }else{
                foreach($val['bus'] as $b){
//                    dd($b);
                    $codes = array_keys($b['terminals']);

                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $data = [
                        'from' => [
                            'name' => $from_city['name'],
                            'type' => $from_city['type'],
                        ],
                        'to' => [
                            'name' => $to_city['name'],
                            'type' => $to_city['type'],
                        ],
                        'date' => $param['date'],
                        'nop' => isset($param['nop']) ? $param['nop'] : 1,
    //                    'bus_id' => $param['bus_id'],
                        'bus_id' => $b['route_code'],
                    ];

                    $routes_data = \App\Models\Admin\BusRoutes::get_bus_routes($data,$param);

                    $routes = [];
                    foreach($routes_data as $key=>$r){
                        $d = [
                            'r_id'          => $r['id'],
                            'r_route'       => $r['route'],
                            'r_from_id'     => $r['from_terminal_id'],
                            'r_from_name'   => $r['from_terminal']['name'],
                            'r_to_id'       => $r['to_terminal_id'],
                            'r_to_name'     => $r['to_terminal']['name'],
                            'r_board_time'  => $r['boarding_time'],
                            'r_drop_time'   => $r['droping_time'],
            //                'r_price'       => $r['price'],
                            'r_price'       => \General::number_format($r['price']),
                        ];
                        $routes[] = $d;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
//                        'points'            => [
//                            "boarding" =>[
//                                "boarding_name"     => $b['terminals'][$codes[0]][0]['board_point'],
//                                "boarding_address"  => $b['terminals'][$codes[0]][0]['board_point'].', '.$b['from'],
//                                "boarding_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['departure_time'])),
//                            ],
//                            "dropping" =>[
//                                "dropping_name"     => $b['terminals'][$codes[0]][0]['drop_point'],
//                                "dropping_address"  => $b['terminals'][$codes[0]][0]['drop_point'].', '.$b['to'],
//                                "dropping_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['arrival_time'])),
//                            ],
//                        ],
                        'routes'            => $routes,
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',      "typeName" => "Bus",   ],
            [   "typeID" => 'shuttle',  "typeName" => "Shuttle",   ],
            [   "typeID" => 'travel',   "typeName" => "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }
        
        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    public static function search_bus_seat_seller_main_old($param) {
        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];
        
        $boardingArray = array();
        if(isset($param['boarding_points'])){
            $boP = explode(',', $param['boarding_points']);
            $boardingArray = array_filter($boP);
        }
        $droppingArray = array();
        if(isset($param['dropping_points'])){
            $drP = explode(',', $param['dropping_points']);
            $droppingArray = array_filter($drP);
        }
        
        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];

        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();
        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }




        $businfo = \App\Models\Businfo::active()
                ->select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                ->whereNotIn("sp_id", $sp_ids)
                ->where("Bus_status", 1)
                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                ->where('Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                ->where('Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));

        $bq = array();        
        if(!empty($boardingArray)){
            $bque = '';
            foreach($boardingArray as $v){
                $bq[] = ' Bus_boarding_time like "%'.$v.'%" ';
            }
            $bque .= ' ( '.  implode(' or ', $bq) . ' ) ';
            
            $businfo = $businfo->whereRaw($bque);    
        }
        $dq = array();        
        if(!empty($droppingArray)){
            $dque = '';
            foreach($droppingArray as $v){
                $dq[] = ' Bus_departure_time like "%'.$v.'%" ';
            }
            $dque .= ' ( '.  implode(' or ', $dq) . ' ) ';
            
            $businfo = $businfo->whereRaw($dque);    
        }
        
        
        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->take($param['len'])->skip($param['start'])->get()->toArray();
//        $businfo = $businfo->take($param['len'])->skip($param['start'])->toSql();exit;

        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }

        $bus_data = \App\Models\Businfo::with("ServiceProviderInfo", "ParentBus")
                        ->select("*")
                        ->whereIn("Bus_id", $bus_id_valid)
                        ->groupBy("Bus_id")
                        ->get()->toArray();
        $businfo_data = self::search_bus_formated($bus_data, $param);
        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->get()->toArray();
        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator'  => $operators,
                'type'      => $type,
                'feature'   => $feature,
            ]
        ];
      //  \Log::info('data of bus : '.  json_encode($data));
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    
    public static function search_bus_seat_seller_a($param) {
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from'      => [
                                'name' => $from_city['name'],
                                'type' => $from_city['type'],
                            ],
            'to'        => [
                                'name' => $to_city['name'],
                                'type' => $to_city['type'],
                            ],
            'date'      => $param['date'],
            'nop'       => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
       
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

                $businfo_data[] = [
                    'Bus_id' => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem' => rtrim($luxitem, ", "),
                    'rute' => $val['routes'][0]['route'],
                    'parent_busname' => $val['bus']['name'],
                    'available_seats' => $val['bus']['total_seats'],
                    'bus_fare' => \General::number_format($val['bus']['price'], 3),
                    'time' => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration' => $val['routes'][0]['duration'],
                    'points'            => [
                        "boarding" =>[
                            "boarding_name"     => $val['routes'][0]['from_terminal']['name'],
                            "boarding_address"  => $val['routes'][0]['from_terminal']['address'],
                            "boarding_time"     => date('H:i', strtotime($val['routes'][0]['boarding_time'])),
                        ],
                        "dropping" =>[
                            "dropping_name"     => $val['routes'][0]['to_terminal']['name'],
                            "dropping_address"  => $val['routes'][0]['to_terminal']['address'],
                            "dropping_time"     => date('H:i', strtotime($val['routes'][0]['droping_time'])),
                        ],
                    ],
                ];
            }else{
                
                foreach($val['bus'] as $b){
//                    dd($b);
                    $codes = array_keys($b['terminals']);
                    
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                        'points'            => [
                            "boarding" =>[
                                "boarding_name"     => $b['terminals'][$codes[0]][0]['board_point'],
                                "boarding_address"  => $b['terminals'][$codes[0]][0]['board_point'].', '.$b['from'],
                                "boarding_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['departure_time'])),
                            ],
                            "dropping" =>[
                                "dropping_name"     => $b['terminals'][$codes[0]][0]['drop_point'],
                                "dropping_address"  => $b['terminals'][$codes[0]][0]['drop_point'].', '.$b['to'],
                                "dropping_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['arrival_time'])),
                            ],
                        ],
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }

        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    public static function search_bus_seat_seller_a_old($param) {
        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];
        
        $boardingArray = array();
        if(isset($param['boarding_points'])){
            $boP = explode(',', $param['boarding_points']);
            $boardingArray = array_filter($boP);
        }
        $droppingArray = array();
        if(isset($param['dropping_points'])){
            $drP = explode(',', $param['dropping_points']);
            $droppingArray = array_filter($drP);
        }

        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];

        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();
        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }




        $businfo = \App\Models\Businfo::active()
                ->select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                ->whereNotIn("sp_id", $sp_ids)
                ->where("Bus_status", 1)
                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                ->where('Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                ->where('Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));

         $bq = array();        
        if(!empty($boardingArray)){
            $bque = '';
            foreach($boardingArray as $v){
                $bq[] = ' Bus_boarding_time like "%'.$v.'%" ';
            }
            $bque .= ' ( '.  implode(' or ', $bq) . ' ) ';
            
            $businfo = $businfo->whereRaw($bque);    
        }
        $dq = array();        
        if(!empty($droppingArray)){
            $dque = '';
            foreach($droppingArray as $v){
                $dq[] = ' Bus_departure_time like "%'.$v.'%" ';
            }
            $dque .= ' ( '.  implode(' or ', $dq) . ' ) ';
            
            $businfo = $businfo->whereRaw($dque);    
        }
                
                
        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->take($param['len'])->skip($param['start'])->get()->toArray();

        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }

        $bus_data = \App\Models\Businfo::with("ServiceProviderInfo", "ParentBus")
                        ->select("*")
                        ->whereIn("Bus_id", $bus_id_valid)
                        ->groupBy("Bus_id")
                        ->get()->toArray();
        $businfo_data = self::search_bus_formated($bus_data, $param);
        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->get()->toArray();
        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operators,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    
    public static function search_bus_seat_seller_b($param) {
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where("id", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where("id", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from' => [
                'name' => $from_city['name'],
                'type' => $from_city['type'],
            ],
            'to' => [
                'name' => $to_city['name'],
                'type' => $to_city['type'],
            ],
            'date' => $param['date'],
            'nop' => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
        
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda'  && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                        ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

                $businfo_data[] = [
                    'Bus_id' => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem' => rtrim($luxitem, ", "),
                    'rute' => $val['routes'][0]['route'],
                    'parent_busname' => $val['bus']['name'],
                    'available_seats' => $val['bus']['total_seats'],
                    'bus_fare' => \General::number_format($val['bus']['price'], 3),
                    'time' => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'duration' => $val['routes'][0]['duration'],
                    'points'            => [
                        "boarding" =>[
                            "boarding_name"     => $val['routes'][0]['from_terminal']['name'],
                            "boarding_address"  => $val['routes'][0]['from_terminal']['address'],
                            "boarding_time"     => date('H:i', strtotime($val['routes'][0]['boarding_time'])),
                        ],
                        "dropping" =>[
                            "dropping_name"     => $val['routes'][0]['to_terminal']['name'],
                            "dropping_address"  => $val['routes'][0]['to_terminal']['address'],
                            "dropping_time"     => date('H:i', strtotime($val['routes'][0]['droping_time'])),
                        ],
                    ],
                ];
            }else{
                foreach($val['bus'] as $b){
//                    dd($b);
                    $codes = array_keys($b['terminals']);
                    
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                        'points'            => [
                            "boarding" =>[
                                "boarding_name"     => $b['terminals'][$codes[0]][0]['board_point'],
                                "boarding_address"  => $b['terminals'][$codes[0]][0]['board_point'].', '.$b['from'],
                                "boarding_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['departure_time'])),
                            ],
                            "dropping" =>[
                                "dropping_name"     => $b['terminals'][$codes[0]][0]['drop_point'],
                                "dropping_address"  => $b['terminals'][$codes[0]][0]['drop_point'].', '.$b['to'],
                                "dropping_time"     => date('H:i', strtotime($b['terminals'][$codes[0]][0]['arrival_time'])),
                            ],
                        ],
                    ];
                }
            }
            
        }

        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }

        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];

        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    public static function search_bus_seat_seller_b_old($param) {
        $from_city = \App\Models\Cities::where("city_name", $param['from_city'])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['id'];
        
        $boardingArray = array();
        if(isset($param['boarding_points'])){
            $boP = explode(',', $param['boarding_points']);
            $boardingArray = array_filter($boP);
        }
        $droppingArray = array();
        if(isset($param['dropping_points'])){
            $drP = explode(',', $param['dropping_points']);
            $droppingArray = array_filter($drP);
        }

        $to_city = \App\Models\Cities::where("city_name", $param['to_city'])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['id'];

        $current_date = date('Y-m-d');
        $formated_date = date('d-m-Y', strtotime($param['date']));
        $block_date = \App\Models\ServiceproviderBlockdates::orWhere(function ($query) use ($param) {
                            $query->where('start_date', $param['date'])
                            ->orWhere('end_date', $param['date']);
                        })
                        ->orWhere(function ($query) use ($param) {
                            $query->where('start_date', "<", $param['date'])
                            ->where('end_date', ">", $param['date']);
                        })->groupBy("sp_id")->get()->toArray();
        $sp_ids = [];
        foreach ($block_date as $key => $val) {
            $sp_ids[] = $val['sp_id'];
        }




        $businfo = \App\Models\Businfo::active()
                ->select("Bus_id", 'Bus_fromcity', 'Bus_tocity')
                ->whereNotIn("sp_id", $sp_ids)
                ->where("Bus_status", 1)
                ->where(\DB::raw("'" . $param['date'] . "'"), "<=", \DB::raw("DATE_ADD('$current_date',INTERVAL `active_days` DAY)"))
                ->where('disable_date', "NOT LIKE", "'%" . $formated_date . "%'")
                ->where('Bus_fromcity', "REGEXP", \DB::raw("'^({$param['from_id']})[,]|[,]({$param['from_id']})[,]|[,]({$param['from_id']})$|^({$param['from_id']})$'"))
                ->where('Bus_tocity', "REGEXP", \DB::raw("'^({$param['to_id']})[,]|[,]({$param['to_id']})[,]|[,]({$param['to_id']})$|^({$param['to_id']})$'"));

         $bq = array();        
        if(!empty($boardingArray)){
            $bque = '';
            foreach($boardingArray as $v){
                $bq[] = ' Bus_boarding_time like "%'.$v.'%" ';
            }
            $bque .= ' ( '.  implode(' or ', $bq) . ' ) ';
            
            $businfo = $businfo->whereRaw($bque);    
        }
        $dq = array();        
        if(!empty($droppingArray)){
            $dque = '';
            foreach($droppingArray as $v){
                $dq[] = ' Bus_departure_time like "%'.$v.'%" ';
            }
            $dque .= ' ( '.  implode(' or ', $dq) . ' ) ';
            
            $businfo = $businfo->whereRaw($dque);    
        }
                
                
        if (isset($param['operator_id']) && $param['operator_id'] != "") {
            $allowed_sp_ids = explode(',', $param['operator_id']);
            $businfo = $businfo->whereIn("sp_id", $allowed_sp_ids);
        }
        if (isset($param['type_id']) && $param['type_id'] != "") {
            $allowed_type_ids = explode(',', $param['type_id']);
            $businfo = $businfo->whereIn("Bus_type", $allowed_type_ids);
        }
        if (isset($param['feature_id']) && $param['feature_id'] != "") {
            $allowed_feature_ids = explode(',', $param['feature_id']);
            foreach ($allowed_feature_ids as $value) {

                $lux_qry[] = "luxury_item REGEXP '^($value)[,]|[,]($value)[,]|[,]($value)$|^($value)$'";
            }
            $lux_qry = " ( " . implode(" OR ", $lux_qry) . " ) ";
            $businfo = $businfo->whereRaw($lux_qry);
        }
        $businfo = $businfo->take($param['len'])->skip($param['start'])->get()->toArray();

        $bus_id_valid = [];
        foreach ($businfo as $key => $row) {
            $busid = $row['Bus_id'];
            $frm_arr = explode(',', $row['Bus_fromcity']);
            $to_arr = explode(',', $row['Bus_tocity']);
            $to_array_flip = array_flip($to_arr);

            if (array_key_exists($param['from_id'], $to_array_flip)) {
                $from_pos = $to_array_flip[$param['from_id']];
                $to_pos = $to_array_flip[$param['to_id']];
                if ($to_pos > $from_pos) {
                    $bus_id_valid[] = $busid;
                }
            } else {
                $bus_id_valid[] = $busid;
            }
        }

        $bus_data = \App\Models\Businfo::with("ServiceProviderInfo", "ParentBus")
                        ->select("*")
                        ->whereIn("Bus_id", $bus_id_valid)
                        ->groupBy("Bus_id")
                        ->get()->toArray();
        $businfo_data = self::search_bus_formated($bus_data, $param);
        $operators = \App\Models\ServiceproviderInfo::active()
                        ->select("SP_name", "SP_id")
                        ->get()->toArray();
        $type = \App\Models\Bustypes::where("typeStatus", 1)->select("typeID", "typeName")->get()->toArray();
        $feature = \App\Models\BusLuxitem::where("lux_status", 0)->select("lux_id", "lux_name")->get()->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operators,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }
    
    
    public static function is_logged_in($token) {
//        dd($token);
        if (\Request::wantsJson()) {
            if ($token == "") {
                return \General::session_expire_res();
            }
            $already_login = \App\Models\SeatSeller\Token::is_active(\Config::get("constant.AUTH_TOKEN_STATUS"), $token);
//            dd($already_login);
            if (!$already_login)
                return \General::session_expire_res("unauthorise");
            else {
                $user = self::where("id", $already_login)->first();

                if (is_null($user))
                    return \General::session_expire_res();

                $user = $user->toArray();
                unset($user['password']);
                $user['auth_token'] = $token;
                app()->instance('logged_in_agent', $user);
            }
        } else {
            if (!\Auth::guard('ss')->check()) {
                \Auth::guard('ss')->logout();
                $validator = \Validator::make([], []);
                $validator->errors()->add('attempt', \Lang::get('error.session_expired', []));
                return \General::session_expire_res("unauthorise");
            } else {
                $operator_data = \Auth::guard('ss')->user();
                $operator = [
                    'agent_id' => $operator_data->id,
                    'agent_name' => $operator_data->name,
                    'agent_email' => $operator_data->email,
                    'agent_type' => $operator_data->type,
                    'agent_city' => $operator_data->city,
                    'agent_address' => $operator_data->address,
                    'agent_mobile1' => $operator_data->mobile,
                    'tax' => $operator_data->tax,
                    'agent_status' => $operator_data->status,
                    'agnt_amt' => $operator_data->balance,
                    'Bus_Admin_Parent' => $operator_data->parent_id,
                    'paypal_email' => $operator_data->paypal_email,
                    
//                    'agent_vat' => $operator_data->agent_vat,
//                    'agent_address2' => $operator_data->agent_address2,
//                    'agent_state' => $operator_data->agent_state,
//                    'agent_landNo1' => $operator_data->agent_landNo1,
//                    'agent_landNo2' => $operator_data->agent_landNo2,
//                    'agent_mobile2' => $operator_data->agent_mobile2,
//                    'agent_mobile3' => $operator_data->agent_mobile3,
//                    'agent_fax' => $operator_data->agent_fax,
//                    'comm_user' => $operator_data->comm_user,
//                    'comm_sp' => $operator_data->comm_sp,
//                    'agent_emgFname' => $operator_data->agent_emgFname,
//                    'agent_emgDesignation' => $operator_data->agent_emgDesignation,
//                    'agent_emgCallno' => $operator_data->agent_emgCallno,
//                    'agent_comments' => $operator_data->agent_comments,
//                    'user_typeID' => $operator_data->user_typeID,
//                    'agent_verified' => $operator_data->agent_verified,
//                    'sp_type' => $operator_data->sp_type,
//                    'paypal' => $operator_data->paypal,
                    
                ];

                $ua = \Request::server("HTTP_USER_AGENT");
                $ip = \Request::server("REMOTE_ADDR");

                $session = \App\Models\SeatSeller\Token::active()->where("type", config("constant.AUTH_TOKEN_STATUS"))->where("ua", $ua)->where("ip", $ip)->where("user_id", $operator['agent_id'])->first();
                if (is_null($session)) {
                    \Auth::guard('ss')->logout();
                    $operator['auth_token'] = "";
                } else {
                    $operator['auth_token'] = $session['token'];
                }
                app()->instance('logged_in_agent', $operator);
            }
        }
        return \General::success_res();
    }
    
    public static function book_ticket($param) {
        
        $token = \Request::header('AuthToken');
        $is_login = self::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_agent");
        }

        $agent_id = $logged_in_user['id'];
        
        if(!preg_match("/[a-z]/i", $param['bus_id'])){
            $book_system = 0;
            $bus_details = \App\Models\Serviceprovider\Buses::where('id',$param['bus_id'])->with('sp')->first()->toArray();
            $board_point = \App\Models\Locality\Terminal::where('id',$param['from_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();
            $drop_point = \App\Models\Locality\Terminal::where('id',$param['to_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();

            $data = [
                'from_term' => $param['from_term_id'],
                'to_term'   => $param['to_term_id'],
                'nop'       => $param['total_seats'],
                'bus_id'    => $param['bus_id'],
                'date'      => $param['date'],
            ];
            $base_amount = \App\Models\Admin\BusRoutes::get_route_price($data);
        }else{
            $book_system = 1;
            $lorena = 0;
            if (stripos($param['bus_id'],'_LORENA') !== false) {
                $lorena = 1;
                $book_system = 2;
            }
            if($lorena){
                $rodaR = \App\Models\Lorena::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }else{
                $rodaR = \App\Models\Roda::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }
            
            $rbus = \App\Models\Roda::get_roda_bus($roda, $param['bus_id']);
            
            if($lorena){
                $allTr = \App\Models\Lorena::get_terminals($rbus, $param);
            }else{
                $allTr = \App\Models\Roda::get_terminals($rbus, $param);
            }
            
            if($allTr['flag'] == 0){
                return $allTr;
            }
            $ter = $allTr['data'];
            $board_point = [
                'name' => $ter['board_point'],
                'loc_citi' => ['name' => $rbus['from']],
                'loc_district' => ['name' => ''],
            ];
            $drop_point = [
                'name' => $ter['drop_point'],
                'loc_citi' => ['name' => $rbus['to']],
                'loc_district' => ['name' => ''],
            ];
//            dd($b_terminal,$d_terminal);
            if($lorena){
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            
            $bus_details = [
                'name'=>$rbus['bus'],
                'sp'=>$rsp,
            ];
            $base_amount = $ter['fare'];
        }
        

        if ($param['type'] == config("constant.SEAT_SELLER_MAIN")) {
            $usertype = 5; 
            $booked_by = 5;
        } else if ($param['type'] == config("constant.SEAT_SELLER_A") || $param['type'] == config("constant.SEAT_SELLER_B")) {
            if ($param['type'] == config("constant.SEAT_SELLER_A")) {
                $usertype = 6;
                $booked_by = 6;
            } else if ($param['type'] == config("constant.SEAT_SELLER_B")) {
                $usertype = 7;
                $booked_by = 7;
            }
        }

        $param['passenger'] = json_decode($param['passenger'], true);

        $param = \Input::all();
        
        $b_time = explode(':', $param['boarding_time']);
        $d_time = explode(':', $param['departure_time']);
        
        $pick_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['boarding_time']));
        $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['departure_time']));

        if($d_time[0] < $b_time[0]){
            $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' +1 day '.$param['departure_time']));
        }
        
        
        $totalamount = $base_amount * $param['total_seats'];
        
        $data = [
            'ccode' => isset($param['coupon_code']) ? $param['coupon_code']:'',
            'busid' => $param['bus_id'],
        ];
        
        $hfval = 0;
        $discount = 0;
        $d = \App\Models\CouponCode::find_coupon($data);
        if($d['flag'] == 1){
            if($d['data'][0]['min_amount'] <= ($totalamount / 1000) && $d['data'][0]['max_amount'] >= ($totalamount / 1000)){
                $cc = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                if($d['data'][0]['type'] == 'P'){
                    $discount = (($totalamount / 1000) * $d['data'][0]['value'] / 100);
                    
                }
                $discount = ($discount * 1000);
            }
            else{
                $cc = \General::error_res('Coupon Code Not Applied.');
            }
        }
        else{
            $cc = \General::error_res('Invalid Coupon Code..!');
        } 
        
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($bus_details['sp']['id']);
        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( (($totalamount) * $handlefee[0]['handling_fee']) / 100 );
        }
        
        $total_amount = $totalamount - $discount + $hfval;

        $t = date("Y-m-d");
        
        if ($total_amount > $logged_in_user['balance'])
            return \General::error_res("Silahkan Hubungi Call Center Untuk Menambah Saldo Anda");
        
        $data = [
            "sessionid"         => $param['sess_id'],
            "spname"            => $bus_details['sp']['first_name'].' '.$bus_details['sp']['last_name'],
            "sp_id"             => $bus_details['sp']['id'],
            "bus_id"            => $param['bus_id'],
            "user_id"           => $agent_id,
            "book_by"           => $booked_by,
            "booking_system"    => $book_system,
//            "status"            => config("constant.STATUS_BOOKED"),
            "payment_by"        => 0,
            "journey_date"      => $param['date'],
            "from_term_id"      => $param['from_term_id'],
            "to_term_id"        => $param['to_term_id'],
            "from_term"         => $board_point['name'],
            "to_term"           => $drop_point['name'],
            "from_city"         => $board_point['loc_citi']['name'],
            "to_city"           => $drop_point['loc_citi']['name'],
            "from_district"     => $board_point['loc_district']['name'],
            "to_district"       => $drop_point['loc_district']['name'],
            "pickup_date"       => $pick_date,
            "drop_date"         => $drop_date,
            "nos"               => $param['total_seats'],
            "total_amount"      => $total_amount,
            "coupon_code"       => isset($param['coupon_code']) ? $param['coupon_code'] : '',
            "coupon_discount"   => $discount,
            "handle_fee"        => $hfval,
            "base_amount"       => $base_amount,
            "booker_name"       => $param['user_firstname'],
            "booker_email"      => $param['user_email'],
            "booker_dob"        => date("Y-m-d H:i:s", strtotime($t." -".$param['user_age']." year")),
            "booker_no"         => $param['user_mobileno'],
            "payment"           => '',    
            "device"           => 'android',    
            "direct"           => 0,    
        ];
        
        $booking = \App\Models\Bookings::save_booking_details($data);
        if(isset($booking['flag']) && $booking['flag'] != 1){
            return $booking;
        }
        $res = \General::success_res();
        
        $res['data']['ticket_id'] = $booking['data']['booking_id'];
        $res['data']['pt_id'] = -1;
        $res['data']['transfer_code'] = $booking['data']['bt_id'];
        $res['data']['cc_url'] = \URL::to('api/services/ticket-checkout/'.\Mycrypt::encrypt($booking['data']['id']));
//        $res['data']['payable_amount'] = $booking['data']['total_amount'] / 1000;
        $res['data']['payable_amount'] = \General::number_format($booking['data']['total_amount'],3);
        
        return $res;
    }
    
    public static function datewise_report($param) {
//        dd($param);
        $code = [
            'ss' => [
                'Main'  => [5,6,7],
                'A'     => 6,
                'B'     => 7,
            ],
            'sp' => [
                'Main'  => [2,3,4],
                'A' => 3,
                'B' => 4,
            ],
        ];
        $count = 0;
        
        if ($param['type'] == 'A' ||$param['type'] == 'B') {
            $book_by = $code[$param['agent']][$param['type']];

            $count_q = \App\Models\Bookings::where('status','=',1)
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            
            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->where('book_by',$book_by)
                                    ->where('user_id',$param['id']);

            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $count = $count_q->whereDate('created_at','=',$from_date)
                                        ->count();
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');
            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                        ->count();
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
            }
        }
        else{
            $book_by = $code[$param['agent']][$param['type']];

            $count_q = \App\Models\Bookings::where('status','=',1)->with('userSs','userSp')
                                                ->whereIn('book_by',$book_by)
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSs',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                })
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSp',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                });

            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->whereIn('book_by',$book_by)
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSs',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    })
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSp',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    });
            
            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');

            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
            }
        }
        if($count <= 0){
            $res = \General::error_res('No Records Founds.');
            return $res;
        }
        $total_amount = \General::number_format($total_amt, 3);
        $res = \General::success_res();
//        $res['data'] = ["ticket_count" => count($tickets), "total_amount" => $total_amount];
        $res['data'] = ["ticket_count" => $count, "total_amount" => $total_amount];
        return $res;
    }
    
    public static function datewise_detail_report($param) {
//        dd($param);
        $code = [
            'ss' => [
                'Main'  => [5,6,7],
                'A'     => 6,
                'B'     => 7,
            ],
            'sp' => [
                'Main'  => [2,3,4],
                'A' => 3,
                'B' => 4,
            ],
        ];
        $count = 0;
        
//        dd($param);
        
        if ($param['type'] == 'A' ||$param['type'] == 'B') {
            $book_by = $code[$param['agent']][$param['type']];
            $tickets = \App\Models\Bookings::where('status','=',1)->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            $count_q = \App\Models\Bookings::where('status','=',1)
                                                ->where('book_by',$book_by)
                                                ->where('user_id',$param['id']);
            
            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->where('book_by',$book_by)
                                    ->where('user_id',$param['id']);

            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $tickets = $tickets->whereDate('created_at','=',$from_date);
                $count = $count_q->whereDate('created_at','=',$from_date)
                                        ->count();
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');
            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $tickets = $tickets->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date);
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                        ->count();
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
                
            }
        }
        else{
            $book_by = $code[$param['agent']][$param['type']];

            $count_q = \App\Models\Bookings::where('status','=',1)->with('userSs','userSp')->with(['bookingSeats','sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','paymentRecord'])
                                                ->whereIn('book_by',$book_by)
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSs',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                })
                                                ->where(function($q) use($param){
                                                    return $q->where('user_id',$param['id'])
                                                                ->orWhereHas('userSp',function ($query)  use($param)  {
                                                                    $query->where('parent_id',$param['id']);
                                                            });
                                                });

            $total_amt_q = \App\Models\Bookings::where('status','=',1)
                                    ->whereIn('book_by',$book_by)
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSs',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    })
                                    ->where(function($q) use($param){
                                        return $q->where('user_id',$param['id'])
                                                    ->orWhereHas('userSp',function ($query)  use($param)  {
                                                        $query->where('parent_id',$param['id']);
                                                });
                                    });
            
            if (!isset($param['to_date']) || $param['to_date'] == "") {
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $tickets = $count_q->whereDate('created_at','=',$from_date);
                $count = $count_q->whereDate('created_at','=',$from_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','=',$from_date)
                                            ->sum('total_amount');

            }
            else{
                $from_date = date('Y-m-d H:i:s', strtotime($param['from_date']));
                $to_date = date('Y-m-d H:i:s', strtotime($param['to_date']));
                $tickets = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date);
                $count = $count_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
//                                        ->count();
                                        ->sum('nos');
                
                $total_amt = $total_amt_q->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)
                                            ->sum('total_amount');
            }
        }
        
        if($count <= 0){
            $res = \General::error_res('No Records Found.');
            return $res;
        }
        $tickets = $tickets->get()->toArray();
        $ticket_data = [];
        $total_amount = 0;
        
//        dd($tickets);
        foreach ($tickets as $key => $val) {
            $service_provider = $val['sp'];
            if (!is_null($service_provider)) {
                $service_provider_name = $service_provider['first_name'].' '.$service_provider['last_name'];
            }
            if($val['booking_system'] > 0){
//                $busList = \App\Models\ApiBusInfo::where('booking_id',$val['booking_id'])->first();
                if($val['booking_system'] == 2){
                    $busList = \App\Models\Lorena::get_bus_info($val['booking_id']);
                }else{
                    $busList = ApiBusInfo::where('booking_id',$val['booking_id'])->first();
                }
                if($busList){
                        $params = [
                            'from_term_id' => $val['booking_seats'][0]['from_terminal'],
                            'to_term_id' => $val['booking_seats'][0]['to_terminal'],
                        ];
                        $list = json_decode($busList['bus_detail'],true);
                        $term = \App\Models\Roda::get_terminals($list, $params);
    //                    DD($term);
                        $val['from_terminal'] = [
                            'name' =>$term['data']['board_point'],
                            'address' =>$term['data']['board_point'].','.$list['from'],
                            'loc_citi' =>[
                                'name'=>$list['from'],
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>$term['data']['drop_point'],
                            'address' =>$term['data']['drop_point'].','.$list['to'],
                            'loc_citi' =>[
                                'name'=>$list['to'],

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = $list['route'];
                        
                }else{
                    $val['from_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',
                                ],
                            'loc_district' => [],
                        ];
                        $val['to_terminal'] = [
                            'name' =>'',
                            'address' =>'',
                            'loc_citi' =>[
                                'name'=>'',

                                ],
                            'loc_district' => [],
                        ];
                        $booked_route = '';
                }
                
            }else{
                $route_detail = BusRoutes::with("bus.sp")
                            ->where("bus_id", $val['bus_id'])
                            ->where("from_terminal_id", $val['from_terminal_id'])
                            ->where("to_terminal_id", $val['to_terminal_id'])
                            ->first()->toArray();

                $booked_route = $route_detail['route'];
            }
            
            $from_city = $val['from_terminal']['loc_citi'];
            $to_city = $val['to_terminal']['loc_citi'];
            $boading_address = $val['from_terminal']['name'];
            $dropping_address = $val['to_terminal']['name'];
            $boading_full_address = $val['from_terminal']['address'];
            $dropping_full_address = $val['to_terminal']['address'];
            $handling_fee = $val['handling_fee'];
            $unit_handling_fee = $val['handling_fee'] / $val['nos'];
            $pay_status = $val['status'];
            $journey_completed = 0;
            $current_date = date("Y-m-d H:i:s");
            $travelling_date = $val['journey_date'];
            if (\App\Models\Services\General::date_dif($current_date, $travelling_date, "i") < 1) {
                $journey_completed = 1;
            }
            $transfer_code = $val['bt_id'];
            $data = [
                'ticket_id' => $val['booking_id'],
                'coupon_code' => $val['coupon_code'],
                'date' => date("D, j F, Y", strtotime($val['journey_date'])),
                "from_city" => $val['from_terminal']['loc_citi']['name'],
                "to_city" => $val['to_terminal']['loc_citi']['name'],
                "boading_address" => $boading_address,
                "boading_full_address" => is_null($boading_full_address) ? "" : $boading_full_address,
                "drop_address" => $dropping_address,
                "drop_full_address" => is_null($dropping_full_address) ? "" : $dropping_full_address,
                "user_name" => $val['booker_name'],
                "user_email" => $val['booker_email'],
                "user_mobile" => $val['booker_mo'],
                "total_amt" => \General::number_format(($val['total_amount']), 3),
                "ticket_count" => $val['nos'],
                "payment_mode" => $val['payment_record']['payment_by'] == 1 || $val['payment_record']['payment_by'] == 0 ? "CASH" : "CC",
                "pay_status" => $pay_status,
                "service_provider_name" => $service_provider_name,
                "book_date" => date('Y-m-d',strtotime($val['created_at'])),
            ];
            $ticket_data[] = $data;
        }
        $res = \General::success_res();
        $res['data'] = $ticket_data;
        return $res;
    }
    
    public static function change_password($param) {
        $logged_in_user = app("logged_in_agent");
        $id = $logged_in_user['id'];
        $user = self::where("id", $id)->first();
        if (is_null($user)) {
            return \General::error_res("user_not_found");
        }
        if ($user->status == config("constant.USER_SUSPEND_STATUS")) {
            return \General::error_res("account_suspended");
        }
        if (!\Hash::check($param['old_password'], $user->password)) {
            return \General::error_res("invalid_old_password");
        }
        $user->password = \Hash::make($param['new_password']);;
        $user->save();
        return \General::success_res("password_changed");
    }
    
    public static function update_profile($param) {
        $user = self::where("id", $param['agent_id'])->first();
        if (is_null($user)) {
            return \General::error_res("invalid_user");
        }
        if (isset($param['agent_name']))
            $user->name = $param['agent_name'];

        if (isset($param['agent_mobile'])) {
            $param['agent_mobile'] = \App\Models\Services\General::formate_mobile_no($param['agent_mobile']);
            $user->mobile = trim($param['agent_mobile']);
        }
        $user->save();
        $res = \General::success_res("user_profile_updated");
        $user_data = $user->toArray();
        unset($user_data['password']);
        
        $data = [
            'agent_id'          => $user_data['id'],
            'agent_name'        => $user_data['name'],
            'agent_email'       => $user_data['email'],
            'agent_mobile'      => $user_data['mobile'],
            'agent_address'     => $user_data['address'],
            'agent_city'        => $user_data['city'],
            'agent_commission'  => $user_data['comm'],
            'agent_joindate'    => $user_data['created_at'],
            'agent_status'      => $user_data['status'],
            'agnt_amt'          => $user_data['balance'],
            'agent_type'        => $user_data['type'],
            'tax'               => $user_data['tax'],
            'paypal_email'      => $user_data['paypal_email'],
            'auth_token'        => \Request::header('Authtoken'),
        ];
        
        
//        $res['data'] = $user_data;
        $res['data'] = $data;
        return $res;
    }
    
    public static function help($param) {
        \Mail::raw($param['message'], function ($message) use ($param) {
            $message->from($param['email'], $param['name']);
            $message->to("support@bustiket.com")->subject("bantuan seatseller");
        });
        return \General::success_res("We received your request. We will contact you soon");
    }
    
    public static function reserve_ticket_booking($param) {
//        dd($param);
        
        $token = \Request::header('AuthToken');
        $is_login = self::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_agent");
        }
        
        $traveling_date = $param['date'];
        $three_days = date("Y-m-d",strtotime("+3 days"));
        $diff = strtotime($traveling_date) - strtotime($three_days);
        if( $diff < 0 )
        {
            return \General::error_res("Select booking after 3 days");
        }
        
        $agent_id = $logged_in_user['id'];
        
        
        
        // Booking from Third Party API.
//        if(!preg_match("/[a-z]/i", $param['bus_id'])){ 
//            $ex = 0;
//            $bookingApi = 0;
//        }else{
//            $bookingApi = 1;
//            $ex = 1;
//            $busList = \App\Models\Services\Buslist::where('from_city',  $param['from_id'])->where('to_city',  $param['to_id'])->where('travelling_date',  $param['date'])->first()->toArray();
//            if(count($busList)> 0){
//                $list = unserialize($busList['bus_list']);
//                
//                $businfo = \App\Models\Services\General::getDetailApiBus($list, $param['bus_id']);
//                $businfo['no_layout'] = 0;
//                $businfo['SP_id'] = config('constant.SP_FOR_API');
//            }
//        }
        
        if ($param['type'] == config("constant.SEAT_SELLER_MAIN")) {
            $pay_trans_id = "ss";
            $usertype = 5; 
            $booked_by = 5;
            $book_system = 0;
        } else if ($param['type'] == config("constant.SEAT_SELLER_A") || $param['type'] == config("constant.SEAT_SELLER_B")) {
            if ($param['type'] == config("constant.SEAT_SELLER_A")) {
                $pay_trans_id = "ssa";
                $usertype = 6;
                $booked_by = 6;
                $book_system = 0;
            } else if ($param['type'] == config("constant.SEAT_SELLER_B")) {
                $pay_trans_id = "ssb";
                $usertype = 7;
                $booked_by = 7;
                $book_system = 0;
            }
        }
        
        $seat_data = json_decode($param['passenger'],true);
        
        
        $layout = \App\Models\BusLayout::where('bus_id',$param['bus_id'])->first();
//        dd($layout);
        // To allocate Seat Label and Seat Index if seatlayout available for bus
        if($layout){
            $booked  = \App\Models\BookingSeats::get_booked_seats($param['bus_id'], $param['date']);
            $blocked = \App\Models\BookingSeats::get_blocked_seats($param['bus_id'], $param['date']);
            
            $total_seat = \App\Models\BusLayout::total_seats($param['bus_id']);
            $remain_seat = $total_seat - (count($blocked)+count($booked));
            $data = \App\Models\SeatMap::where("layout_id", $layout['id'])->orderBy("row", "ASC")->orderBy("col", "ASC")->get()->toArray();
//            dd($data,$booked,$blocked);
            foreach($seat_data as $k=>$d){
                foreach($data as $key=>$seat){
                    $flag_booked  = 1;
                    $flag_blocked = 1;

                    foreach($booked as $bo){
                        if($seat['seat_lbl'] == $bo){
                            $flag_booked = 0;
                            break;
                        }
                    }

                    foreach($blocked as $bl){
                        if($seat['seat_lbl'] == $bl){
                            $flag_blocked = 0;
                            break;
                        }
                    }
                    if($flag_blocked == 1 && $flag_booked == 1 && $seat['object_type'] != 'T' && $seat['object_type'] != 'D' && $seat['object_type'] != 'DS'){
                        $seat_data[$k]['seat_lbl'] = $seat['seat_lbl'];
                        $seat_data[$k]['seat_index'] = $seat['seat_index'];
                        unset($data[$key]);
                        break;
                    }
                }
            }
//            dd($seat_data,$data,$booked,$blocked);
        }
        
        $ex = 0;
        // Booking from Third Party API.
        if(preg_match("/[a-z]/i", $param['bus_id'])){
            $ex = 1;
            $book_system = 1;
            $lorena = 0;
            if (stripos($param['bus_id'],'_LORENA') !== false) {
                $lorena = 1;
                $book_system = 2;
            }
            if($lorena){
                $rodaR = \App\Models\Lorena::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }else{
                $rodaR = \App\Models\Roda::get_bus_from_db($param);
                if($rodaR['flag'] !=1){
                    return $rodaR;
                }
                $roda = $rodaR['data'];
            }
            
            $rbus = \App\Models\Roda::get_roda_bus($roda, $param['bus_id']);
            if($lorena){
                $allTr = \App\Models\Lorena::get_terminals($rbus, $param);
            }else{
                $allTr = \App\Models\Roda::get_terminals($rbus, $param);
            }
            
            if($allTr['flag'] == 0){
                return $allTr;
            }
            $ter = $allTr['data'];
            $board_point = [
                'name' => $ter['board_point'],
                'loc_citi' => ['name' => $rbus['from']],
                'loc_district' => ['name' =>''],
                'address' => $ter['board_point'] .' , '.$rbus['from'],
            ];
            $drop_point = [
                'name' => $ter['drop_point'],
                'loc_citi' => ['name' => $rbus['to']],
                'loc_district' => ['name' =>''],
                'address' => $ter['drop_point'] .' , '.$rbus['to'],
            ];
//            dd($b_terminal,$d_terminal);
            if($lorena){
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first()->toArray();
            }else{
                $rsp = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first()->toArray();
            }
            
            $busname = [
                'name'=>$rbus['bus'],
                'sp'=>$rsp,
            ];
            $per_head_amount = $ter['fare'];
            $base_amount = $per_head_amount;
//            dd($ter);
        }else{
            $busname = \App\Models\Serviceprovider\Buses::where('id',$param['bus_id'])->with(['sp','routes.fromTerminal.locCiti','routes.fromTerminal.locDistrict','routes.toTerminal.locCiti','routes.toTerminal.locDistrict'])->first();
            if($busname == null){
                $json = \General::error_res('Bus ID is Invalid.');
                return \Response::json($json,200);
            }
            $bus_data = $busname->toArray();
//            dd($bus_data);
            $board_point = \App\Models\Locality\Terminal::where('id',$param['from_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();
            $drop_point = \App\Models\Locality\Terminal::where('id',$param['to_term_id'])->with(['locCiti','locDistrict'])->first()->toArray();
//            $board_point = $bus_data['routes'][0]['from_terminal'];
//            $drop_point = $bus_data['routes'][0]['to_terminal'];
//            dd($board_point,$drop_point);
            
            //-------- Start : Check Route exists or not --------------//
            $chk_route = BusRoutes::where('bus_id',$param['bus_id'])
                                    ->where('from_terminal_id',$board_point['id'])
                                    ->where('to_terminal_id',$drop_point['id'])->first();
//            dd($chk_route);
            if($chk_route == null){
                $json = \General::error_res('Invalid Route.');
//                dd(\Response::json($json,200));
                return $json;
            }
            //-------- Over : Check Route exists or not --------------//
            
            $busname = $busname->toArray();
            $data = [
//                'from_term' => $param['from_term_id'],
//                'to_term'   => $param['to_term_id'],
                'from_term' => $board_point['id'],
                'to_term'   => $drop_point['id'],
                'nop'       => $param['total_seats'],
                'bus_id'    => $param['bus_id'],
                'date'      => $param['date'],
            ];
            
            $per_head_amount = \App\Models\Admin\BusRoutes::get_route_price($data);
            $base_amount = $per_head_amount;
        }
        
        $layout = \App\Models\BusLayout::where('bus_id',$param['bus_id'])->first();
        
        $seat_index = [];
        $seat_label = [];
        $pass_name = [];
        $pass_gender = [];
        $pass_mo = [];
        $pass_age = [];
        foreach($seat_data as $key=>$s){
            $seat_index[$key]   = isset($seat_data[$key]['seat_index']) ? $seat_data[$key]['seat_index'] : '';
            $seat_label[$key]   = isset($seat_data[$key]['seat_lbl']) ? $seat_data[$key]['seat_lbl'] : '';
            $pass_name[$key]    = $seat_data[$key]['passenger_name'];
            $pass_gender[$key]  = $seat_data[$key]['passenger_gender'] == 'L' ? 'M' : 'F';
            $pass_mo[$key]      = $seat_data[$key]['passenger_mobile'];
            $pass_age[$key]     = $seat_data[$key]['passenger_age'];
        }
        
        $session_id = $pay_trans_id."direct".  time();
        
//        dd($param, $seat_data,$busname,$per_head_amount);
        
        $data = [
            "session_id" =>$session_id,
            "bus_id" =>$param['bus_id'],
            "board_point" => [
                "b_terminal_id" => $param['from_term_id'],
//                "b_terminal_id" => $board_point['id'],
                "boarding_datetime" => $param['date'],
            ],
            'drop_point' =>[
                "d_terminal_id" => $param['to_term_id'],
//                "d_terminal_id" => $drop_point['id'],
            ],
            "bus_name"=> $busname["name"],
            'per_head_amount'=> $per_head_amount,
            "total_seat"=> $param['total_seats'],
            "seat_index"=>$seat_index,
            "seat_label"=>$seat_label,
            "pass_name" =>$pass_name,
            "pass_gender"=>$pass_gender,
            "pass_mo"=>$pass_mo,
            "pass_age"=>$pass_age,
            "sp_name"=>$busname['sp']['first_name'].' '.$busname['sp']['last_name'],
        ];
        
        
        $saved_seat = \App\Models\BookingSeats::save_bookingseat_detail($data);
        
        $b_time = explode(':', $param['boarding_time']);
        $d_time = explode(':', $param['departure_time']);
        
        $pick_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['boarding_time']));
        $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' '.$param['departure_time']));

        if($d_time[0] < $b_time[0]){
            $drop_date = date('Y-m-d H:i:s',strtotime($param['date'].' +1 day '.$param['departure_time']));
        }
        
        $totalamount = $param['total_seats'] * $per_head_amount;
        $hfval = 0;
        $discount = 0;
        
        $handlefee = \App\Models\Serviceprovider\ServiceProvider::get_handle_fee($busname['sp']['id']);
        $hfval = $handlefee[0]['handling_fee'];
        if($handlefee[0]['handling_fee_type'] == 'P'){
            $hfval = ceil( ($totalamount * $handlefee[0]['handling_fee']) / 100 );
        }
        
        $data = [
            'ccode' => isset($param['coupon_code']) ? $param['coupon_code']:'',
            'busid' => $param['bus_id'],
        ];
        
        $d = \App\Models\CouponCode::find_coupon($data);
        if($d['flag'] == 1){
            if($d['data'][0]['min_amount'] <= ($totalamount / 1000) && $d['data'][0]['max_amount'] >= ($totalamount / 1000)){
                $cc = \General::success_res('Coupon Code Applied.');
                $discount = $d['data'][0]['value'];
                if($d['data'][0]['type'] == 'P'){
                    $discount = (($totalamount / 1000) * $d['data'][0]['value'] / 100);
                    
                }
                $discount = ($discount * 1000);
            }
            else{
                $cc = \General::error_res('Coupon Code Not Applied.');
            }
        }
        else{
            $cc = \General::error_res('Invalid Coupon Code..!');
        } 
        
        $total_amount = $totalamount - $discount + $hfval;
        $t = date("Y-m-d");
        
        $data = [
            "sessionid"         => $session_id,
            "spname"            => $busname['sp']['first_name'].' '.$busname['sp']['last_name'],
            "sp_id"             => $busname['sp']['id'],
            "bus_id"            => $param['bus_id'],
            "user_id"           => $agent_id,
            "book_by"           => $booked_by,
            "booking_system"    => $book_system,
            "status"            => config("constant.STATUS_PENDING"),
            "payment_by"        => 0,
            "journey_date"      => $param['date'],
            "from_term_id"      => $param['from_term_id'],
            "to_term_id"        => $param['to_term_id'],
//            "from_term_id"      => $board_point['id'],
//            "to_term_id"        => $drop_point['id'],
            "from_term"         => $board_point['name'],
            "to_term"           => $drop_point['name'],
            "from_city"         => $board_point['loc_citi']['name'],
            "to_city"           => $drop_point['loc_citi']['name'],
            "from_district"     => $board_point['loc_district']['name'],
            "to_district"       => $drop_point['loc_district']['name'],
            "pickup_date"       => $pick_date,
            "drop_date"         => $drop_date,
            "nos"               => $param['total_seats'],
            "total_amount"      => $total_amount,
            "coupon_code"       => isset($param['coupon_code']) ? $param['coupon_code'] : '',
            "coupon_discount"   => $discount,
            "handle_fee"        => $hfval,
            "base_amount"       => $base_amount,
            "booker_name"       => $param['user_firstname'],
            "booker_email"      => $param['user_email'],
            "booker_dob"        => date("Y-m-d H:i:s", strtotime($t." -".$param['user_age']." year")),
            "booker_no"         => $param['user_mobileno'],
            "payment"           => '',    
            "device"           => 'android',    
        ];
        
        $booking = \App\Models\Bookings::save_booking_details($data);
        if(isset($booking['flag']) && $booking['flag'] != 1){
            return $booking;
        }
        $bank = \App\Models\BankDetails::get()->toArray();
        $res = \General::success_res("Ticket Reservation Success");
        
        $res['data']['ticket_id'] = $booking['data']['booking_id'];
        $res['data']['pt_id'] = -1;
        $res['data']['transfer_code'] = $booking['data']['bt_id'];
        $res['data']['bank_detail'] = $bank;
        $res['data']['cc_url'] = \URL::to('api/services/ticket-checkout/'.\Mycrypt::encrypt($booking['data']['id']));
//        $res['data']['payable_amount'] = $booking['data']['total_amount'] / 1000;
        $res['data']['payable_amount'] = \General::number_format($booking['data']['total_amount'],3);

        return $res;

    }
    
    public static function approve_ticket($param){
        $token = \Request::header('AuthToken');
        $is_login = self::is_logged_in($token);
        $logged_in_user = [];
        if ($is_login['flag']) {
            $logged_in_user = app("logged_in_agent");
        }

        $agent_id = $logged_in_user['id'];
        
        $tickets = \App\Models\Bookings::where("booking_id", $param['ticket_id'])
                ->where('status',0)
                ->where('user_id',$agent_id)
                ->get()->toArray();
        if(count($tickets) == 0){
            return \General::error_res('no tickets found or ticket already approved');
        }
        
       //echo date('H:i:s');
        //$booking_time = date( 'H:i:s' ,  strtotime($tickets[0]['book_time']));
        $booking_time = strtotime($tickets[0]['created_at']);
        $current_time = strtotime(date('H:i:s'));
        $diff = round(($current_time - $booking_time) / 60);
        
        if($diff > 50){
            return \General::error_res("Sorry you can't book this ticket because you exceed the time limit of booking ticket.");
        }
        
        $SP_id = $tickets[0]['sp_id'];
        $Bus_id = $tickets[0]['bus_id'];
        $booked_by = $tickets[0]['book_by'];
        $bookingAmt = $tickets[0]['total_amount'];
        $tot_seat = count($tickets);
        $agent = self::find($logged_in_user['id']);
        
        if($booked_by == 4  || $booked_by == 5 ){
            if(($bookingAmt * $tot_seat) > $agent->balance){
                return \General::error_res("Sorry you can't book this ticket because you have no sufficient balance for booking ticket.");
            }
        }
        
        $ticket_id = $param['ticket_id'];
        
        if($tickets[0]['booking_system'] == 1){
//                $amount = $bookingAmt;
//                $upd = \App\Models\Services\ApiBusInfo::where('ticket_id',$ticket_id)->first();
//                $book_code = $upd->book_code;
//                $params = 'app=transaction&action=payment&book_code='.$book_code.'&amount='.$amount;
//                $paymentApi = \App\Models\Services\General::makeRodaRequest($params);
//        //        pr($paymentApi);
//
//                if($paymentApi['err_code'] != 0){
//
//                    
//                    $upd->pay_status = 0;
//                    $upd->payment_response =  json_encode($paymentApi);
//                    $upd->save();
//                    return \General::error_res('payment to roda failed.');
//
//                }else{
//                    $bs = 1;
////                    $upd = \App\Models\Services\ApiBusInfo::where('ticket_id',$ticket_id)->first();
//                    $upd->pay_status = 1;
//                    $upd->book_status = 1;
//                    $upd->payment_response =  json_encode($paymentApi);
//                    $upd->save();
//                    
//                    $param = 'app=information&action=get_book_info&book_code='.$book_code;
//                    $bookinInfo = \App\Models\Services\General::makeRodaRequest($param);
//                    if($bookinInfo['err_code'] == 0){
//                        $passlist = $bookinInfo['pax_list'];
//                        \Log::info('passanger list from roda '.  json_encode($passlist));
//                        $uniqIds = array();
//                        foreach($passlist as $k=>$p){
//                            $uniqIds[$k] = $p[6];
//                        }
//                        \Log::info('unique id list from roda '.  json_encode($uniqIds));
//                         $allTik = \App\Models\BookingInfo::where("Ticket_ID", $ticket_id)->get();
//                        $i = 0;
////                        while($row = mysql_fetch_array($allTik)){
//                        foreach($allTik as $row){
//                            \App\Models\BookingInfo::where("auto_id", $row['auto_id'])->update(['unique_id'=>$uniqIds[$i] ]);
////                            mysql_query("UPDATE bookinginfo SET unique_id = ".$uniqIds[$i]." WHERE auto_id=".$row['auto_id']) or mysql_error();
//                            $i++;
//                        }
//                        \App\Models\BookerDetails::where("Ticket_ID", $ticket_id)->update(['unique_id'=>$uniqIds[0]]);
////                        mysql_query("UPDATE booker_details set unique_id = ".$uniqIds[0]."  WHERE Ticket_ID='$ticket'");
////                        $allPass = mysql_query("SELECT * FROM passengerinfo WHERE Ticket_ID='$ticket'");
//                        $allTik = \App\Models\PassengerInfo::where("Ticket_ID", $ticket_id)->get();
//                        $i = 0;
////                        while($row = mysql_fetch_array($allPass)){
//                        foreach($allTik as $row){
//                            \App\Models\PassengerInfo::where("passenger_ID", $row['passenger_ID'])->update(['unique_id'=>$uniqIds[$i]]);
////                            mysql_query("UPDATE passengerinfo SET unique_id = ".$uniqIds[$i]." WHERE passenger_ID=".$row['passenger_ID']) or mysql_error();
//                            $i++;
//                        }
//                    }
//                }
        }
        
//        \App\Models\Bookings::where("booking_id", $ticket_id)->update(['status'=>1]);
        
        $bookerDetail = \App\Models\Bookings::where("booking_id", $ticket_id)->first();
        
        
//        $bookerDetail = \App\Models\BookingSeats::where('booking_id',$ticket_id)->first();
//        \App\Models\BookingSeats::where('booking_id',$ticket_id)->first();
        
        \Log::info('agent id : '.$agent_id);
        
        if (($bookerDetail->book_by == 5 || $bookerDetail->book_by == 6 || $bookerDetail->book_by == 7)){
            $ss = SeatSeller::where('id',$bookerDetail->user_id)->first();
            $ss->balance = $ss->balance - $bookerDetail->total_amount;
            $ss->save();
//            $t = \App\Models\Bookings::where('booking_id',$ticket_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
//            $bookseat = \App\Models\BookingSeats::where('booking_id',$ticket_id)->update(['status'=>1]);
//            \Event::fire(new \App\Events\TicketBookSuccess($t));
            \App\Models\Bookings::approve_ticket($ticket_id);

        }else if(($bookerDetail->book_by == 3 || $bookerDetail->book_by == 4) && $bookerDetail->status == 1){
            $sp = Serviceprovider\ServiceProvider::where('id',$bookerDetail->user_id)->first();
            $sp->balance = $sp->balance - $bookerDetail->total_amount;
            $sp->save();
            $t = \App\Models\Bookings::where('booking_id',$ticket_id)->with(['sp','bus','fromTerminal.locCiti','fromTerminal.locDistrict','toTerminal.locCiti','toTerminal.locDistrict','bookingSeats'])->first()->toArray();
            $bookseat = \App\Models\BookingSeats::where('booking_id',$ticket_id)->update(['status'=>1]);
            \Event::fire(new \App\Events\TicketBookSuccess($t));
        }
        
//        $fare = $bookerDetail->total_amount;
//        
//        
//        //if ($booked_by == 5 ) {
//            
//            $agent->balance = $agent->balance - $fare;
//            $agent->save();
        //}
        
        
        //seat seller commission
//        if ($booked_by != 4) {
//            
//            $fetchagent = self::seatSellerCommission($agent_id, $SP_id, $ticket_id, $Bus_id, $bookingAmt, $tot_seat,1);
//            //commission to main seat seller
//            self::seatSellerCommission($fetchagent['bus_admin_parent'], $SP_id, $ticket_id, $Bus_id, $bookingAmt, $tot_seat);
//        }else{
//            $fetchagent = self::seatSellerCommission($agent_id, $SP_id, $ticket_id,$Bus_id, $bookingAmt, $tot_seat,1);
//        }
//        
//        //admin commission 
//        
//        self::adminCommission($SP_id, $ticket_id, $Bus_id, $bookingAmt, $tot_seat, $booked_by, $agent_id);
        
        
//        \App\Models\BookingInfo::fire_success_event($ticket_id);
        
        
        
        
        $res = \General::success_res("Ticket Approved successfully");
        return $res;
    }
    public static function get_ticket_booked_by_ss($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\Bookings::active()->whereIn('user_id',$param['child'])->where('book_by',$param['book_by'])->whereDate('created_at','=',$today)->count();
            $mcount = \App\Models\Bookings::active()->whereIn('user_id',$param['child'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->count();
            $ycount = \App\Models\Bookings::active()->whereIn('user_id',$param['child'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->count();
        }else{
            $tdcount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','=',$today)->count();
            $mcount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->count();
            $ycount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->count();
        }
        
//        dd($tdcount,$mcount,$ycount);
        $data = [
            [
                'key'=>'Today('.$tdcount.')',
                'y'=>$tdcount,
            ],
            [
                'key'=>'Month('.$mcount.')',
                'y'=>$mcount,
            ],
            [
                'key'=>'Year('.$ycount.')',
                'y'=>$ycount,
            ],
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    public static function get_total_sale_by_ss($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\Bookings::active()->where(function($q)use($param){
                    $q->whereIn('user_id',$param['child'])->orWhere('user_id',$param['id'])  ;
                  })->whereIn('book_by',$param['book_by'])->whereDate('created_at','=',$today)->sum('total_amount');
            $mcount = \App\Models\Bookings::active()->where(function($q)use($param){
                    $q->whereIn('user_id',$param['child'])->orWhere('user_id',$param['id'])  ;
                  })->whereIn('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('total_amount');
            $ycount = \App\Models\Bookings::active()->where(function($q)use($param){
                    $q->whereIn('user_id',$param['child'])->orWhere('user_id',$param['id'])  ;
                  })->whereIn('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('total_amount');
        }else{
            $tdcount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','=',$today)->sum('total_amount');
            $mcount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('total_amount');
            $ycount = \App\Models\Bookings::active()->where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('total_amount');
        }
        
//        dd($tdcount,$mcount,$ycount);
        $allD = [
            [
                'label'=>'Today',
                'value'=>ceil($tdcount),
            ],
            [
                'label'=>'Month',
                'value'=>ceil($mcount),
            ],
            [
                'label'=>'Year',
                'value'=>ceil($ycount),
            ],
        ];
        
        $data = [
            [
                'key'=>'Total sale in rupies',
                'values'=>$allD,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    public static function get_total_commission_of_ss($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\SsCommission::where(function($q)use($param){
                    $q->whereIn('ss_id',$param['child'])  ;
                  })->whereDate('created_at','=',$today)->sum('com_amount');
            $mcount = \App\Models\SsCommission::where(function($q)use($param){
                    $q->whereIn('ss_id',$param['child']);
                  })->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('com_amount');
            $ycount = \App\Models\SsCommission::where(function($q)use($param){
                    $q->whereIn('ss_id',$param['child']) ;
                  })->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('com_amount');
        }else{
            $tdcount = \App\Models\SsCommission::where('ss_id',$param['id'])->whereDate('created_at','=',$today)->sum('com_amount');
            $mcount = \App\Models\SsCommission::where('ss_id',$param['id'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('com_amount');
            $ycount = \App\Models\SsCommission::where('ss_id',$param['id'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('com_amount');
        }
        
//        dd($tdcount,$mcount,$ycount);
        $allD = [
            [
                'label'=>'Today',
                'value'=>ceil($tdcount),
            ],
            [
                'label'=>'Month',
                'value'=>ceil($mcount),
            ],
            [
                'label'=>'Year',
                'value'=>ceil($ycount),
            ],
        ];
        
        $data = [
            [
                'key'=>'Commission in rupies',
                'values'=>$allD,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_sale_by_month($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\Bookings::active()->where(function($q)use($param){
                    $q->whereIn('user_id',$param['child'])->orWhere('user_id',$param['id']) ;
                  })->whereIn('book_by',$param['book_by'])->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))
                ->select(\DB::raw('count(id) as tot'),\DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                ->groupby('year','month');
                  
            $allM = \DB::table( \DB::raw("({$tdcount->toSql()}) as m") )
                    ->mergeBindings($tdcount->getQuery())
                    ->select(\DB::raw("SUM(IF(month = 1, tot, 0)) AS '1',
                                        SUM(IF(month = 2, tot, 0)) AS '2',
                                        SUM(IF(month = 3, tot, 0)) AS '3',
                                        SUM(IF(month = 4, tot, 0)) AS '4',
                                        SUM(IF(month = 5, tot, 0)) AS '5',
                                        SUM(IF(month = 6, tot, 0)) AS '6',
                                        SUM(IF(month = 7, tot, 0)) AS '7',
                                        SUM(IF(month = 8, tot, 0)) AS '8',
                                        SUM(IF(month = 9, tot, 0)) AS '9',
                                        SUM(IF(month = 10, tot, 0)) AS '10',
                                        SUM(IF(month = 11, tot, 0)) AS '11',
                                        SUM(IF(month = 12, tot, 0)) AS '12'"))->first()
                    ;
        }else{
            $tdcount = \App\Models\Bookings::active()->Where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))
                ->select(\DB::raw('count(id) as tot'),\DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                ->groupby('year','month');
                  
            $allM = \DB::table( \DB::raw("({$tdcount->toSql()}) as m") )
                    ->mergeBindings($tdcount->getQuery())
                    ->select(\DB::raw("SUM(IF(month = 1, tot, 0)) AS '1',
                                        SUM(IF(month = 2, tot, 0)) AS '2',
                                        SUM(IF(month = 3, tot, 0)) AS '3',
                                        SUM(IF(month = 4, tot, 0)) AS '4',
                                        SUM(IF(month = 5, tot, 0)) AS '5',
                                        SUM(IF(month = 6, tot, 0)) AS '6',
                                        SUM(IF(month = 7, tot, 0)) AS '7',
                                        SUM(IF(month = 8, tot, 0)) AS '8',
                                        SUM(IF(month = 9, tot, 0)) AS '9',
                                        SUM(IF(month = 10, tot, 0)) AS '10',
                                        SUM(IF(month = 11, tot, 0)) AS '11',
                                        SUM(IF(month = 12, tot, 0)) AS '12'"))->first()
                    ;
        }
        
//        dd($allM);
        $marr = [];
        foreach($allM as $k=>$m){
            $mon = \App\Models\General::get_month_from_number($k-1);
            $marr[] = [
                
                    'label'=>$mon,
                    'value'=>$m,
                
            ];
        }
//        dd($marr);
        $data = [
            [
                'key'=>'Total sale in each month',
                'values'=>$marr,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_daily_sale($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['child'])){
            $tdcount = \App\Models\Bookings::active()->where(function($q)use($param){
                    $q->whereIn('user_id',$param['child'])->orWhere('user_id',$param['id']) ;
                  })->whereIn('book_by',$param['book_by'])->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))->whereRaw(\DB::raw('MONTH(created_at) = MONTH(curdate())'))
                ->select(\DB::raw('count(id) as tot,created_at'),\DB::raw('DAY(created_at) day, MONTH(created_at) month'))
                ->groupby(\DB::raw('CAST(created_at AS DATE)'))->get()->toArray()
                    ;
        }else{
            $tdcount = \App\Models\Bookings::active()->Where('user_id',$param['id'])->where('book_by',$param['book_by'])->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))
                    ->whereRaw(\DB::raw('MONTH(created_at) = MONTH(curdate())'))
                ->select(\DB::raw('count(id) as tot'),\DB::raw('DAY(created_at) day, MONTH(created_at) month'))
                ->groupby(\DB::raw('CAST(created_at AS DATE)'))->get()->toArray()
                    ;
        }
        
//        dd($tdcount);
        $darr = [];
        $mon = 0;
        $day = [];
        $val = [];
        
        foreach($tdcount as $k=>$m){
            $mon = $m['month'];
            array_push($day, $m['day']);
            
            $val[$m['day']] = [
                'day'=>$m['day'].'-'.$m['month'],
                'ticket' => $m['tot'],
            ];
        }
        $totalD = date('t');
        for($i=1;$i<=$totalD;$i++){
            if(!in_array($i, $day)){
                array_push($day, $i);
                
                $val[$i] = [
                    'day'=>$i.'-'.$mon,
                    'ticket' => 0,
                ];
            }
        }
        ksort($val);
        foreach($val as $k=>$v){
            $darr[] = [
                
                    'label'=>$v['day'],
                    'value'=>$v['ticket'],
                
            ];
        }
//        dd($val);
        $data = [
            [
                'key'=>'Total sale in each day',
                'values'=>$darr,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    
    public static function  get_child_by_type($param){
        $bb = $param['book_by'];
        $type = $bb == 6 ? 'A' : 'B';
        $start = 0;
        $len = 10;
        if(isset($param['child'])){
            
            $ad = \DB::table('ss')->where('type',$type)
                    ->select(\DB::raw('name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` = '.$param['book_by'].' and `status` = 1 and user_id = ss.id) as total'))
                    ->where('parent_id',$param['id'])
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;
        }else{
            /*$ad = \DB::table('ss')
                    ->select(\DB::raw('name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` in('.$param['book_by'].') and `status` = 1 and user_id = ss.id) as total'))
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;*/
            $ad = \DB::table('ss')
                    ->select(\DB::raw('name'),\DB::raw('(select count(id) as total from `bookings` where `book_by` in('.$param['book_by'].') and `status` = 1 and user_id = ss.id) as total'))
                    ->orderBy('total','desc')->skip($start)->take($len)
//                    ->mergeBindings($tdcount)->toSql()
                    ->get()
                ;
        }
//        dd($ad);
        return \App\Models\General::prepare_data_for_individual_chart($ad);
    }
}