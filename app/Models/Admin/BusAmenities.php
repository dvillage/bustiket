<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class BusAmenities extends Model {

    
    public $table = 'bus_amenities';
    protected $fillable = array('bus_id','aname','aimg');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    
    
}
