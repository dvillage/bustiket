<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Banners extends Model {

    
    public $table = 'banners';
    protected $fillable = array('file', 'group', 'height', 'width', 'height_type', 'width_type','alt','redirect_to','status');
//    protected $appends = array('role');
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function get_bannerfilter($param){
        
//        dd($param);
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('group','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::skip($start)->take($len)->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::where('group','like','%'.$param['search'].'%')->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_banner($param){
        $res=self::where('id',$param['id'])->delete();
        return $res;
    }
    
    public static function add_new_banner($param){
//        dd($param);
        $bnrimg = \Request::file('bnr_img');
        $img_ext = ['jpg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){
            $ext = $bnrimg->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$img_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $filePath = config('constant.UPLOAD_BANNER_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimg->getClientOriginalExtension();
            $bnrimg->move($filePath,$FileName);
            $img_name = $FileName;
        }
        
        $new = new Banners;
        $new->file = $img_name;
        $new->group = $param['group'];
        $new->height = $param['height'];
        $new->width = $param['width'];
        $new->height_type = $param['height_type'];
        $new->width_type = $param['width_type'];
        $new->alt = $param['alt'];
        $new->redirect_to = $param['red_url'];
        $new->status = isset($param['status'])?$param['status']:0;
        $new->save();
        
        $res = \General::success_res();
        return $res;
    }
    public static function update_banner($param){
//        dd($param);
        
        $new = self::where('id',$param['bid'])->first();
                
        $bnrimg = \Request::file('bnr_img');
        $img_ext = ['jpg','png','icn'];
        $img_name = '';
        if(isset($bnrimg) && $param['bnr_img']!=''){
            $ext = $bnrimg->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$img_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $filePath = config('constant.UPLOAD_BANNER_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimg->getClientOriginalExtension();
            $bnrimg->move($filePath,$FileName);
            $img_name = $FileName;
            $new->file = $img_name;
        }
        
        
        
        $new->group = $param['group'];
        $new->height = $param['height'];
        $new->width = $param['width'];
        $new->height_type = isset($param['height_type'])?$param['height_type']:'px';
        $new->width_type = isset($param['width_type'])?$param['width_type']:'px';
        $new->alt = $param['alt'];
        $new->redirect_to = $param['red_url'];
        $new->status = isset($param['status'])?$param['status']:0;
        $new->save();
        
        $res = \General::success_res();
        return $res;
    }
    
}
