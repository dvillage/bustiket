<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class CardCourier extends Model {

    
    public $table = 'card_courier';
    protected $fillable = array('card_id','courier_id');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function courier(){
        return $this->hasOne('App\Models\Admin\Courier','id','courier_id');
    }
    
}
