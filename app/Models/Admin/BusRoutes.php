<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use App\Models\Roda as Roda;
class BusRoutes extends Model {

    
    public $table = 'bus_routes';
    protected $fillable = array('name','route','bus_id','from_city_id','to_city_id','from_district_id','to_district_id','from_terminal_id','to_terminal_id','boarding_time','droping_time');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function allDetail(){
        $data = [
//            'from_city' => $this->hasOne('\App\Models\Locality\Citi','id','from_city_id'),
//            'to_city' => $this->hasOne('\App\Models\Locality\Citi','id','to_city_id'),
//            'from_terminal' => $this->hasOne('\App\Models\Locality\Terminal','id','from_terminal_id'),
//            'to_terminal' => $this->hasOne('\App\Models\Locality\Terminal','id','to_terminal_id'),
//            'from_district' => $this->hasOne('\App\Models\Locality\District','id','from_district_id'),
//            'to_district' => $this->hasOne('\App\Models\Locality\District','id','to_district_id'),
        ];
        return $data;
    }
    public function fromCity(){
        return $this->hasOne('\App\Models\Locality\Citi','id','from_city_id');
    }
    public function toCity(){
        return $this->hasOne('\App\Models\Locality\Citi','id','to_city_id');
    }
    public function fromTerminal(){
        return $this->hasOne('\App\Models\Locality\Terminal','id','from_terminal_id');
    }
    public function toTerminal(){
        return $this->hasOne('\App\Models\Locality\Terminal','id','to_terminal_id');
    }
    public function fromDistrict(){
        return $this->hasOne('\App\Models\Locality\District','id','from_district_id');
    }
    public function toDistrict(){
        return $this->hasOne('\App\Models\Locality\District','id','to_district_id');
    }
    public function prices(){
        return $this->hasMany('\App\Models\Admin\BusPrice','route_id','id');
    }
    public function bus(){
        return $this->hasOne('\App\Models\Serviceprovider\Buses','id','bus_id');
    }
    
    
    
    public static function get_from_to_fileds_by_inpute($param)
    {
        $res = \General::error_res("invalid type");
        if(isset($param['from']['type']) && $param['from']['type'] == "C")
        {
            $from_field = "from_city_id";
        }
        else if(isset($param['from']['type']) && $param['from']['type'] == "D")
        {
            $from_field = "from_district_id";
        }
        
        else if(isset($param['from']['type']) && $param['from']['type'] == "T")
        {
            $from_field = "from_terminal_id";
        }
        else if(isset($param['from']['type']) && $param['from']['type'] == "P")
        {
            $from_field = "from_province";
        }
        else 
            return $res;
        
        $from = \App\Models\Locality::where('name',$param['from']['name'])->where('type',$param['from']['type'])->first();
        if(!$from){
            return $res;
        }
        $from_val = $from->lid;
        
        if(isset($param['to']['type']) && $param['to']['type'] == "C")
        {
            $to_field = "to_city_id";
        }
        else if(isset($param['to']['type']) && $param['to']['type'] == "D")
        {
            $to_field = "to_district_id";
        }
        
        else if(isset($param['to']['type']) && $param['to']['type'] == "T")
        {
            $to_field = "to_terminal_id";
        }
        else if(isset($param['to']['type']) && $param['to']['type'] == "P")
        {
            $to_field = "to_province";
        }
        else 
            return $res;
        
        $to = \App\Models\Locality::where('name',$param['to']['name'])->where('type',$param['to']['type'])->first();
        if(!$to){
            return $res;
        }
        $to_val = $to->lid;
        
        
        $res = \General::success_res();
        $res['data'] = [
            'from_field' => $from_field,
            'from_val' => $from_val,
            'to_field' => $to_field,
            'to_val' => $to_val
        ];
        return $res;
    }
    
    public static function group_by_bus($routes,$param){
        
        $current_date = date('Y-m-d');
        $date_diff = \General::date_dif($current_date, $param['date'], "d");
        
        $lflag = $rflag = $sflag = 0;
        
//        dd(\Auth::guard('sp')->check());
        
        if($date_diff == 0){
            $rflag = 1;
            if(\Auth::guard('admin')->check() || \Auth::guard('sp')->check()){
                $sflag = 1;
            }
        }elseif($date_diff == 1){
            $lflag = $rflag = 1;
            if(\Auth::guard('admin')->check() || \Auth::guard('sp')->check()){
                $sflag = 1;
            }
        }else{
            $lflag = $rflag = $sflag = 1;
        }
        
//        dd($sflag);
        
        $data = $lorena = $roda = $system = [];
        
        if($lflag)
            $lorena = self::lorena_bus($routes, $param);
   //         dd($lorena);
        if($rflag)
            $roda = self::roda_bus($routes, $param);
        
        if($sflag)
            $system = self::system_bus($routes, $param);

//        $data = array_merge($roda,$system);
//        dd($data);
        
        $priority = config('constant.SEARCH_PRIORITY');
//        dd($priority);
        
        $par = '$data = array_merge(';
        foreach($priority as $pr){
            $d = '$c = isset($'.$pr.') ? 1 : 0;';
            eval($d);
            if($c){
                $par .= '$'.$pr.',';
            }
        }
        $par=substr($par,0,-1);
        $par.=");";

        eval($par);
        
        return $data;
    }
    
    public static function system_bus($routes,$param){
        $data = [];
        $bus_routes = [];
        
        foreach ($routes as $key => $val)
        {
            $btimem = strtotime($val['boarding_time']);
            $dtimem = strtotime($val['droping_time']);
            
            $bdattim = date('Y-m-d',strtotime(date('Y-m-d',strtotime($param['date'])))).' '.$val['boarding_time'];
            $ddattim = date('Y-m-d',strtotime(date('Y-m-d',strtotime($param['date'])))).' '.$val['droping_time'];
            
            if($btimem>$dtimem){
                $ddattim = date('Y-m-d',strtotime(date('Y-m-d',strtotime($ddattim)).' + 1 day')).' '.$val['droping_time'];
            }
            $diff = strtotime($ddattim) - strtotime($bdattim);
            $val['duration'] = $diff/60;
            $hours = $val['duration'] / 60;
            $val['boarding_minute'] = $val['boarding_time'];
            $val['droping_minute'] = $val['droping_time'];
            $val['duration'] = $hours >= 1 ? ceil($hours)." Jam" : $val['duration']." min";
//            $val['boarding_time'] = strtotime(date("Y-m-d"))+$val['boarding_time'];
//            $val['boarding_time'] = strtotime($param['date'].' '.$val['boarding_time']);
            $val['boarding_time'] = $bdattim;
//            $val['boarding_time'] = date("Y-m-d",strtotime($param['date'])).' '.$bt;
//            $val['droping_time'] = strtotime(date("Y-m-d"))+$val['droping_time'];
//            $val['droping_time'] = strtotime($param['date'].' '.$val['droping_time']);
            $val['droping_time'] = $ddattim;
//            $val['droping_time'] = date("Y-m-d",strtotime($param['date'])).' '.$dt;
            $bus_routes[$val['bus_id']]['routes'][] = $val;
            
        }
//        dd($bus_routes);
        
        $date = date('Y-m-d H:i:s', strtotime($param['date']));
        
        $block_date = BlockDates::Where(function ($query) use ($date) {
                            $query->whereDate('from_date','=', $date)
                            ->orWhereDate('to_date','=', $date);
                        })
                        ->orWhere(function ($query) use ($date) {
                            $query->whereDate('from_date', "<", $date)
                            ->whereDate('to_date', ">", $date);
                        })->groupBy("bus_id")->get()->toArray();
        $bus_ids = [];
        foreach ($block_date as $key => $val) {
            $bus_ids[] = $val['bus_id'];
        }
//        dd($bus_ids);
        $ids = array_keys($bus_routes);
        $buses = \App\Models\Serviceprovider\Buses::active()->with(["sp","amenities"])
                                                            ->whereIn("id",$ids)
                                                            ->whereNotIn('id',$bus_ids);
//        dd($buses->get()->toArray());
        $lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
        
        if($lt == 'sp'){
            
            $lgr = config('constant.SP_TYPE');
            $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
            $buses = $buses->whereHas('sp',function($q)use($sp_id){
                $q->where('id',$sp_id);
            });
        }
        
//        dd($buses->get()->toArray());
        if(isset($param['operator']) && $param['operator'] == 1){   // for API use
            
//            $lgr = config('constant.SP_TYPE');
//            $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
            $sp_id = $param['parent'];
//            dd($sp_id);
            $buses = $buses->whereHas('sp',function($q)use($sp_id){
                $q->where('id',$sp_id);
            });
        }
//        dd($buses->get()->toArray());
        if(isset($param['sp_list']) && count($param['sp_list']) > 0){       // for SP Filtering
            $splist = array_filter($param['sp_list'], function($var){return $var != '';} );
            $buses = $buses->whereHas('sp',function($q)use($splist){
                $q->whereIn('id',$splist);
            });
        }
//        dd($buses->get()->toArray());
        if(isset($param['type']) && count($param['type']) > 0){             // For Type Filtering
//            dd($param['type']);
            $type = array_filter($param['type'], function($var){return $var != '';} );
            $buses = $buses->whereIn('type',$type);
            
        }
//        dd($buses->get()->toArray());
        if(isset($param['amenities']) && count($param['amenities']) > 0){   // For Amenities Filtering
            $amenities = array_filter($param['amenities'], function($var){return $var != '';} );
            foreach($amenities as $am){
                $buses = $buses->whereHas('amenities',function($q)use($am){
                    $q->where('a_id',$am);
                });
            }
//            $buses = $buses->whereHas('amenities',function($q)use($amenities){
//                $q->whereIn('a_id',$amenities);
//            });
        }
//        dd($buses->get()->toArray());
        $buses = $buses->get()->toArray();
        $minPrice = [];
        
        foreach ($bus_routes as $r => $route)
        {
            foreach ($buses as $b => $bus)
            {
                if($bus['id'] == $r)
                {
                    
                    $price = BusPrice::getSeatPriceBySale($bus['id'],$route['routes'],$param['nop'], $param['date']); 
                    $total_seat = \App\Models\BusLayout::total_seats($bus['id']);
                    $booked = \App\Models\BookingSeats::get_booked_seats($bus['id'], $param['date']);
                    $blocked = \App\Models\BookingSeats::get_blocked_seats($bus['id'], $param['date']);
                    if(!(int)$price)
                        continue;
                    $minPrice[] = $price;
                    $bus['price'] = $price;
                    $bus['total_seats'] = $total_seat - (count($blocked)+count($booked));
//                    $bus['sp']['avatar'] = $bus['sp']['avatar'] == "" ? url("assets/images/sp/".$bus['sp']['avatar']."default.png") : url("assets/images/sp/".$bus['sp']['avatar']);
                    $bus['sp']['avatar'] = url("assets/images/sp/default.png");
                    $bus_route = [];
                    $bus_route['routes'] = $route['routes'];
                    $bus_route['bus'] = $bus;
                    $data[] = $bus_route;
                    break;
                }
            }
        }
//        dd($param);
        $d1 = date('Y-m-d',strtotime($param['date']));
        $date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($d1)),date("l", strtotime($d1)));
        if(count($minPrice) > 0){
            $mprice = min($minPrice);
            $keytosave = $param['from']['name'].'.'.$param['from']['type'].'.'.$param['to']['name'].'.'.$param['to']['type'];
            $set = [
                    'price'=> \General::number_format(($mprice),3),
                    'date'=> $date,
                ];
            if(\Cache::has($keytosave)){
                $cache = \Cache::get($keytosave);
                $cache[$d1] = $set;
                ksort($cache);
                \Cache::forever($keytosave,$cache);
            }else{
                $cache = [];
                $cache[$d1] = $set;
                \Cache::forever($keytosave,$cache);
            }
        }
//        dd($data);
        return $data;
    }

    public static function roda_bus($routes,$param){
        $fname = $param['from']['name'];
        $tname = $param['to']['name'];

        $data = [];
        
//        dd($routes);
//        echo app('env');
//        \Log::info('Platform : '.(app('platform')));
//        if(app('platform') == 0){
            if(config('constant.USE_RODA') == 1){
//                if($param['from']['type'] == 'C' && $param['to']['type'] == 'C' ){
                    
                    
                    if($param['from']['type'] == 'P' || $param['to']['type'] == 'P'){
                        goto skipRoda;
                    }
                    
                    $chk = self::check_roda_terminal($param);
                    
                    if($chk['flag'] != 1){
                        goto skipRoda;
                    }
                    
                    $fname = $chk['fname'];
                    $tname = $chk['tname'];
                    
//                    dd($fname,$tname);
                        $spdetail = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.RODA_USER_EMAIL'))->first();
                        
                        if($spdetail){
                            
                            $lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
                            $flg = 0;
                            $othr = 1;
                            if($lt == 'sp'){
                                $othr = 0;
                                $lgr = config('constant.SP_TYPE');
                                $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
                                if($sp_id == $spdetail->id){
                                    $flg = 1;
                                }
                            }
                            if(isset($param['operator']) && $param['operator'] == 1){   // for API use
                                $othr = 0;
                                $sp_id = $param['parent'];
                    //            dd($sp_id);
                                if($sp_id == $spdetail->id){
                                    $flg = 1;
                                }
                            }
                            if($othr == 1 || $flg == 1){
                                $rodaBus = \App\Models\Roda::getBusFromApi($fname, $tname, $param['date']);
    //                            $gropuRodaBus = \App\Models\Roda::grou_bus_by_route_code($rodaBus);
    //                            $formatedBus= \App\Models\Roda::formate_bus_detail($rodaBus);
//                                dd($rodaBus);
                                if(isset($rodaBus)){
                                    $spdetail = $spdetail->toArray();
                                    $data['roda']['bus'] = $rodaBus;
                                    $data['roda']['sp'] = $spdetail;
                                }
                            }
                            
                        }
                    
//                }

            }
//        }
        
        skipRoda:
            
            return $data;
    }

    public static function lorena_bus($routes,$param){
        $fname = $param['from']['name'];
        $tname = $param['to']['name'];

        $data = [];
        
        if(config('constant.USE_LORENA') == 1){
            
//                if($param['from']['type'] == 'C' && $param['to']['type'] == 'C' ){
                    
                    
                    if($param['from']['type'] == 'P' || $param['to']['type'] == 'P'){
                        goto skipRoda;
                    }
                    
                    $chk = self::check_roda_terminal($param,'lorena');
                    
                    if($chk['flag'] != 1){
                        goto skipRoda;
                    }
                    
                    $fname = $chk['fname'];
                    $tname = $chk['tname'];
                    
//                    dd($fname,$tname);
                        $spdetail = \App\Models\Serviceprovider\ServiceProvider::where('email',  config('constant.LORENA_USER_EMAIL'))->first();
                        
                        if($spdetail){
                            
                            $lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
                            $flg = 0;
                            $othr = 1;
                            if($lt == 'sp'){
                                $othr = 0;
                                $lgr = config('constant.SP_TYPE');
                                $sp_id = $lgr == 'Main' ? config('constant.CURRENT_LOGIN_ID') : config('constant.SP_PARENT');
                                if($sp_id == $spdetail->id){
                                    $flg = 1;
                                }
                            }
                            if(isset($param['operator']) && $param['operator'] == 1){   // for API use
                                $othr = 0;
                                $sp_id = $param['parent'];
                    //            dd($sp_id);
                                if($sp_id == $spdetail->id){
                                    $flg = 1;
                                }
                            }
                            if($othr == 1 || $flg == 1){
//                                $rodaBus = \App\Models\Roda::getBusFromApi($fname, $tname, $param['date']);
                                $rodaBus = \App\Models\Lorena::getBusFromApi($fname, $tname, $param['date']);
    //                            $gropuRodaBus = \App\Models\Roda::grou_bus_by_route_code($rodaBus);
    //                            $formatedBus= \App\Models\Roda::formate_bus_detail($rodaBus);
//                                dd($rodaBus);
                                if(isset($rodaBus)){
                                    $spdetail = $spdetail->toArray();
                                    $data['lorena']['bus'] = $rodaBus;
                                    $data['lorena']['sp'] = $spdetail;
                                }
                            }
                            
                        }
                    
//                }

            }
//        }
        
        skipRoda:
        
        return $data;
    }
    
    public static function add_new_route($from,$toC,$bus_id,$seat_count,$price){
//        dd($from,$to);
        foreach($from as $k=>$fr){
            $f = json_decode($fr,true);
            $to = json_decode($toC[$k],true);
            $fp = \App\Models\Locality\Citi::where('id',$f['cid'])->first();
            $fp = $fp->province_id;
            $top = \App\Models\Locality\Citi::where('id',$to['cid'])->first();
            $top = $top->province_id;
            $rts = new self();
            $rts->name = $f['cname'].'-'.$to['cname'];
            $rts->route = $f['cname'].'-'.$f['dname'].'-'.$f['tname'].'-'.$to['tname'].'-'.$to['dname'].'-'.$to['cname'];
            $rts->bus_id = $bus_id;
            $rts->from_city_id = $f['cid'];
            $rts->to_city_id = $to['cid'];
            $rts->from_district_id = $f['did'];
            $rts->to_district_id = $to['did'];
            $rts->from_terminal_id = $f['tid'];
            $rts->to_terminal_id = $to['tid'];
            $rts->from_province = $fp;
            $rts->to_province = $top;
            $rts->boarding_time = $f['btime'];
            $rts->droping_time = $to['btime'];
            $rts->save();
            
            BusPrice::add_price($seat_count, $price, $bus_id, $rts->id,$k);
        }
        
    }
    
    public static function get_bus_routes($param,$extra){
        
        $bus_id = $param['bus_id'];
        $roda = \Session::get('roda_bus');
        $lorena = 0;
        if (stripos($bus_id,'_LORENA') !== false) {
            $lorena = 1;
            $roda = \Session::get('lorena_bus');
        }
        if(preg_match("/[a-z]/i", $bus_id)){
            
//            if(isset($extra['from_id']) && isset($extra['to_id'])){
//                $rodaR = \App\Models\Roda::get_bus_from_db($extra);
//                if($rodaR['flag'] !=1){
//                    return $rodaR;
//                }
//                $roda = $rodaR['data'];
//                $rbus = \App\Models\Roda::get_roda_bus($roda, $bus_id);
//                
//                $route = \App\Models\Roda::group_terminals($rbus,$param);
//                
////                dd($route);
//                return $route;
//            }
            if(isset($param['from_id']) && isset($param['to_id'])){
                
                if($lorena){
                    $rodaR = \App\Models\Lorena::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                }else{
                    $rodaR = \App\Models\Roda::get_bus_from_db($param);
                    if($rodaR['flag'] !=1){
                        return $rodaR;
                    }
                    $roda = $rodaR['data'];
                }
            }elseif($roda){
                $roda = $roda;
            }else{
                return \General::error_res('no route found');
            }
            
            $rbus = Roda::get_roda_bus($roda, $bus_id);
            if($lorena){
                $route = \App\Models\Lorena::group_terminals($rbus,$param);
            }else{
                $route = \App\Models\Roda::group_terminals($rbus,$param);
            }
            
//            dd($route);
//            return \General::error_res('no route found');
            return $route;
            
        }
        $filter_fields = \App\Models\Admin\BusRoutes::get_from_to_fileds_by_inpute($param);
        if($filter_fields['flag'] != 1)
            return [];
//        dd($filter_fields);
        $routes = \App\Models\Admin\BusRoutes::with(["fromTerminal","toTerminal",'fromCity','toCity','fromDistrict','toDistrict'])
                    ->where($filter_fields['data']['from_field'],$filter_fields['data']['from_val'])
                    ->where($filter_fields['data']['to_field'],$filter_fields['data']['to_val'])
                    ->where('bus_id',$param['bus_id'])
                    ->get()->toArray();
//        dd($routes);
        foreach($routes as $key=>$r){
//            dd($routes);
            $data = [
                'from_terminal_id'=>$r['from_terminal_id'],
                'to_terminal_id'=>$r['to_terminal_id'],
                'nop'=>$param['nop'],
                'bus_id'=>$param['bus_id'],
                'date'=>$param['date'],
            ];
            $price = BusPrice::get_terminal_price($data);
            $routes[$key]['price'] = $price['data'];
        }
        
        return $routes;
    }
    
    public static function get_route_price($param){
        $data = [
                'from_terminal_id'=>$param['from_term'],
                'to_terminal_id'=>$param['to_term'],
                'nop'=>$param['nop'],
                'bus_id'=>$param['bus_id'],
                'date'=>$param['date'],
            ];
            $price = BusPrice::get_terminal_price($data);
            return $price['data'];
    }
    
    public static function check_roda_terminal($param,$other=''){
        
        $fname = $param['from']['name'];
        $tname = $param['to']['name'];
        if($param['from']['type'] == 'D'){
            $terminals = \App\Models\Locality\Terminal::whereHas('locDistrict',function($q)use($param){
                $q->where('name',$param['from']['name']);
            })->with('locCiti')->get()->toArray();
            if(count($terminals) == 0){
                return \General::error_res();
            }
            $rexist = 0;
            foreach($terminals as $tr){
                if($other == 'lorena'){
                    if($tr['lorena_code'] != ''){
                        $rexist++;
                    }
                }else{
                    if($tr['roda_code'] != ''){
                        $rexist++;
                    }
                }
                
                $fname = $tr['loc_citi']['name'];
            }
            if($rexist == 0){
                return \General::error_res();
            }
        }
        if($param['to']['type'] == 'D'){
            $terminals = \App\Models\Locality\Terminal::whereHas('locDistrict',function($q)use($param){
                $q->where('name',$param['to']['name']);
            })->with('locCiti')->get()->toArray();
            if(count($terminals) == 0){
                return \General::error_res();
            }
            $rexist = 0;
            foreach($terminals as $tr){
                if($other == 'lorena'){
                    if($tr['lorena_code'] != ''){
                        $rexist++;
                    }
                }else{
                    if($tr['roda_code'] != ''){
                        $rexist++;
                    }
                }
                $tname = $tr['loc_citi']['name'];
            }
            if($rexist == 0){
                return \General::error_res();
            }
        }
        
        if($param['from']['type'] == 'T'){
            $terminals = \App\Models\Locality\Terminal::where('name',$param['from']['name'])->with('locCiti')->first();
            if(!$terminals){
                return \General::error_res();
            }
            if($other == 'lorena'){
                if($terminals->lorena_code == ''){
                    return \General::error_res();
                }
            }else{
                if($terminals->roda_code == ''){
                    return \General::error_res();
                }
            }
            
            $fname = $terminals['locCiti']['name'];
        }
        if($param['to']['type'] == 'T'){
            $terminals = \App\Models\Locality\Terminal::where('name',$param['to']['name'])->with('locCiti')->first();
            if(!$terminals){
                return \General::error_res();
            }
            if($other == 'lorena'){
                if($terminals->lorena_code == ''){
                    return \General::error_res();
                }
            }else{
                if($terminals->roda_code == ''){
                    return \General::error_res();
                }
            }
            $tname = $terminals['locCiti']['name'];
        }
        
        $res = \General::success_res();
        $res['fname'] = $fname;
        $res['tname'] = $tname;
        
        return $res;
    }
}
