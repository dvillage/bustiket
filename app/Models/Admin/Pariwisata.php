<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Pariwisata extends Model {

    
    public $table = 'tour_buses';
    protected $fillable = array('sp_id','type','ac','fleet_detail','image','other_images','note','city_id','engine','body_manufacturer','price');
    public $timestamps=true;
//    protected $appends = array('role');
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function sp(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    
    public function city(){
        return $this->hasOne('App\Models\Locality\Citi','id','city_id');
    }
    
    public function amenities(){
        return $this->hasMany('App\Models\Admin\PariwisataAmenities','bus_id','id');
    }
    public function prices(){
        return $this->hasMany('App\Models\Admin\PariwisataPrice','tour_bus_id','id');
    }
    
    public static function get_pariwisata($param){
        $am = self::orderBy('id','desc');
        if(isset($param['name'])){
            $am = $am->where('name','like','%'.$param['name'].'%');
        }
        $am = $am->get();
        return $am;
    }
    
    public static function add_pariwisata($param){
//        dd($param);
        $bnrimg = \Request::file('bus_img');
        $oimg = \Request::file('other_img');
        
        $img_name = self::add_image($bnrimg);
        $other_img = self::add_image($oimg);
        
        $d = new self;
        $d->sp_id=$param['sp_id'];
        $d->city_id=$param['city_id'];
        $d->type=$param['bus_type'];
        $d->ac=isset($param['ac']) ? 1 : 0 ;
        $d->fleet_detail=$param['fleet_detail'];
        $d->image=$img_name;
        $d->other_images=$other_img;
        $d->note=$param['note'];
        $d->engine=$param['engine'];
        $d->body_manufacturer=$param['manufacturer'];
        $d->price=$param['price'];
        $d->save();
        
        if(isset($param['amenity_array'])){
            $amen = json_decode($param['amenity_array'],true);
            if(count($amen) > 0){
                foreach($amen as $a){
                    $ad = new PariwisataAmenities();
                    $ad->bus_id = $d->id;
                    $ad->amenities_id = $a;
                    $ad->save();
                }
            }
        }
        if(isset($param['all_price'])){
//            $price = json_decode($param['all_price']);
            $price = $param;
            PariwisataPrice::add_pricing($price,$d->id);
        }else{
            PariwisataPrice::where('tour_bus_id',$d->id)->delete();
        }
        
        return $d;
    }
    
    public static function edit_pariwisata($param){
//        dd($param);
        $d = self::where('id',$param['pid'])->first();
        $filename = $d->image;
        $bnrimg = \Request::file('bus_img');
        $oimg = \Request::file('other_img');
        
        $img_name = self::add_image($bnrimg);
        $other_img = self::add_image($oimg);
        
        
        if($img_name != ''){
            $d->image=$img_name;
            $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        if($other_img != ''){
            $d->other_images = $d->other_images.','.$other_img;
//            $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
//            if(file_exists($filePath.$filename)){
//                unlink($filePath.$filename);
//            }
        }
        
        if(isset($param['removed_images'])){
            $images = json_decode($param['removed_images'],true);
            $old_img = explode(',', $d->other_images);
            if(count($images) > 0){
                foreach($images as $im){
                    if(($key = array_search($im, $old_img)) !== false) {
                        unset($old_img[$key]);
                        $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
                        if(file_exists($filePath.$im)){
                            unlink($filePath.$im);
                        }
                    }
                }
//                dd($old_img);
                $d->other_images = implode(',', $old_img);
            }
        }
        
        $d->sp_id=$param['sp_id'];
        $d->city_id=$param['city_id'];
        $d->type=$param['bus_type'];
        $d->ac=isset($param['ac']) ? 1 : 0 ;
        $d->fleet_detail=$param['fleet_detail'];
        $d->note=$param['note'];
        $d->engine=$param['engine'];
        $d->body_manufacturer=$param['manufacturer'];
        $d->price=$param['price'];
        $d->save();
        
        if(isset($param['amenity_array'])){
            $amen = json_decode($param['amenity_array'],true);
            if(count($amen) > 0){
                foreach($amen as $a){
                    $ae = PariwisataAmenities::where('bus_id',$d->id)->where('amenities_id',$a)->first();
                    if(is_null($ae)){
                        $ad = new PariwisataAmenities();
                        $ad->bus_id = $d->id;
                        $ad->amenities_id = $a;
                        $ad->save();
                    }
                    
                }
                $aem = PariwisataAmenities::where('bus_id',$d->id)->get();
                foreach($aem as $am){
                    if(!in_array($am['amenities_id'], $amen)){
                        $aem = PariwisataAmenities::where('bus_id',$d->id)->where('amenities_id',$am['amenities_id'])->delete();
                    }
                }
            }else{
                PariwisataAmenities::where('bus_id',$d->id)->delete();
            }
        }else{
            PariwisataAmenities::where('bus_id',$d->id)->delete();
        }
        
        if(isset($param['all_price'])){
//            $price = json_decode($param['all_price']);
            $price = $param;
            PariwisataPrice::add_pricing($price,$d->id);
        }else{
            PariwisataPrice::where('tour_bus_id',$d->id)->delete();
        }
        
        return $d;
    }
    public static function delete_pariwisata($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        $filename = $d->image;
        $other = $d->other_images;
        $d->delete();
        
        if($filename != ''){
            $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        if($other != ''){
            $other = explode(',', $other);
            foreach($other as $o){
                $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
                if(file_exists($filePath.$o)){
                    unlink($filePath.$o);
                }
            }
            
        }
        
        
        return $d;
    }
    
    public static function add_image($bnrimg){
        
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$img_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    $filePath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($img_name != ''){
                        $img_name = $img_name.','.$FileName;
                    }else{
                        $img_name = $FileName;
                    }
                }
//            
            
        }
        return $img_name;
    }


    public static function get_pariwisata_list($param){
        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $count=$count->whereHas('sp',function($q) use($param){
                $q->where('first_name','like','%'.$param['search'].'%')->orWhere('last_name','like','%'.$param['search'].'%');
            });
        }
        if(isset($param['city']) && $param['city']!='') {
            $count=$count->whereHas('city',function($q) use($param){
                $q->where('name','like','%'.$param['city'].'%');
            });
        }
        if(isset($param['bus_type']) && $param['bus_type']!=''){
            $count=$count->where('type',$param['bus_type']);
        }
        if(isset($param['ac']) && $param['ac']!=''){
            $count=$count->where('ac',$param['ac']);
        }
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            $count=$count->where('sp_id',$param['sp_id']);
        }
        
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::skip($start)->take($len)->orderBy('id','desc')->with('sp')->with('city')->with('amenities.amenities')->with('prices');
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $district=self::orderBy('id','desc')->with('sp')->with('city')->with('amenities.amenities')->with('prices')->skip($start)->take($len);
            $district=$district->whereHas('sp',function($q) use($param){
                $q->where('first_name','like','%'.$param['search'].'%')->orWhere('last_name','like','%'.$param['search'].'%');
            });
        }
        if(isset($param['city']) && $param['city']!='') {
            $district=$district->whereHas('city',function($q) use($param){
                $q->where('name','like','%'.$param['city'].'%');
            });
        }
        if(isset($param['bus_type']) && $param['bus_type']!=''){
            $district=$district->where('type',$param['bus_type']);
        }
        if(isset($param['ac']) && $param['ac']!=''){
            $district=$district->where('ac',$param['ac']);
        }
        if(isset($param['sp_id']) && $param['sp_id']!=''){
            $district=$district->where('sp_id',$param['sp_id']);
        }
        $district = $district->get()->toArray();
//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function search($param)
    {
//        dd($param);
        $filter_fields = self::get_from_to_fileds_by_inpute($param);
        if($filter_fields['flag'] != 1)
            return [];
        $routes = self::with(["sp","city",'amenities.amenities','prices'])->where($filter_fields['data']['from_field'],$filter_fields['data']['from_val']);
        if($param['bus_type'] != -1){
            $routes = $routes->where('ac',$param['bus_type']);
        }
        $routes = $routes->get()->toArray();
//        dd($routes);
        foreach($routes as $k=>$r){
            if(count($r['prices'])>0){
                foreach($r['prices'] as $pk=>$pv){
                    $routes[$k]['prices'][$pk]['price'] = \General::number_format($pv['price'],3);
                }
            }
        }
//        $result = \App\Models\Admin\BusRoutes::group_by_bus($routes,$param);
        return $routes;
        
    }
    public static function get_from_to_fileds_by_inpute($param)
    {
        $res = \General::error_res("invalid type");
        if(isset($param['from']['type']) && $param['from']['type'] == "C")
        {
            $from_field = "city_id";
        }
        else 
            return $res;
        $from = \App\Models\Locality::where('name',$param['from']['name'])->where('type',$param['from']['type'])->first();
        $res = \General::error_res();
        $res['data'] = [];
        if(!is_null($from))
        {
            $res = \General::success_res();
            $from_val = $from->lid;
            $res['data'] = [
                'from_field' => $from_field,
                'from_val' => $from_val,
            ];
        }
        return $res;
    }
}
