<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class BlockDates extends Model {

    
    public $table = 'bus_block_dates';
    protected $fillable = array('bus_id','from_date','to_date');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    
    
}
