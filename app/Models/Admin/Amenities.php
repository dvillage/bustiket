<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Amenities extends Model {

    
    public $table = 'amenities';
    protected $fillable = array('name','img');
    public $timestamps=false;
//    protected $appends = array('role');
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function get_amenities($param){
        $am = self::orderBy('name','asc');
        if(isset($param['name'])){
            $am = $am->where('name','like','%'.$param['name'].'%');
        }
        $am = $am->get();
        return $am;
    }
    
    public static function add_amenity($param){
//        dd($param);
        
        $img_name = self::add_image();
        
        $d = new self;
        $d->name=$param['name'];
        $d->img=$img_name;
        $d->save();
        
        return $d;
    }
    
    public static function edit_amenity($param){
//        dd($param);
        $d = self::where('id',$param['a_id'])->first();
        $filename = $d->img;
        $img_name = self::add_image();
        
        $ba = BusAmenities::where('aname',$d->name)->get()->toArray();
        if(count($ba)>0){
            foreach($ba as $b){
                $be = BusAmenities::where('id',$b['id'])->first();
                $be->aname = $param['name'];
                if($img_name != ''){
                    $be->aimg = $img_name;
                }
                $be->save();
            }
        }
        
        if($img_name != '' ){
            $d->img = $img_name;
            $filePath = config('constant.UPLOAD_AMENITY_DIR_PATH');
            if($filename != ''){
                if(file_exists($filePath.$filename)){
                    unlink($filePath.$filename);
                }
            }
        }
        
        $d->name=$param['name'];
        $d->save();
        
        return $d;
    }
    public static function delete_amenity($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        if($d){
            $filename = $d->img;
            $ba = BusAmenities::where('aname',$d->name)->get()->toArray();
            if(count($ba)>0){
                foreach($ba as $b){
                    $be = BusAmenities::where('id',$b['id'])->delete();
                }
            }
            $d->delete();

            $filePath = config('constant.UPLOAD_AMENITY_DIR_PATH');
            if($filename){
                if(file_exists($filePath.$filename)){
                    unlink($filePath.$filename);
                }
            }
        }
        return $d;
    }
    
    public static function add_image(){
        $bnrimg = \Request::file('amenity_img');
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){
            $ext = $bnrimg->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$img_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                $res = response()->json($json,422);
                return $res;
            }
            $filePath = config('constant.UPLOAD_AMENITY_DIR_PATH');
            $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimg->getClientOriginalExtension();
            $bnrimg->move($filePath,$FileName);
            $img_name = $FileName;
        }
        return $img_name;
    }


    public static function get_amenities_list($param){
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::skip($start)->take($len)->orderBy('id','desc')->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $district=self::where('name','like','%'.$param['search'].'%')->orderBy('id','desc')->skip($start)->take($len)->get()->toArray();
        }

//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
}
