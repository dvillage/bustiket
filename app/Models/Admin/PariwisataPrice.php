<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class PariwisataPrice extends Model {

    
    public $table = 'tour_bus_price';
    protected $fillable = array('tour_bus_id','direction','type','price','seat_count','hour');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function add_pricing($param,$id){
        $p_city = array_splice($param['p_city_name'],1);
        $p_hour = array_splice($param['p_hour'],1);
        $p_type = array_splice($param['p_bus_type'],1);
        $p_seat = array_splice($param['p_seat_count'],1);
        $p_price = array_splice($param['p_price'],1);
        $ids = array_splice($param['id'],1);
//        dd($p_city,$p_hour,$p_type,$p_seat,$p_price,$ids);
        if(count($p_city) > 0){
            $old = self::where('tour_bus_id',$id)->get()->toArray();
            if(count($old)>0){
                $o_id = $ids;
//                foreach($price as $p){
//                    if(isset($p->id) && $p->id!= ''){
//                        $o_id[] = $p->id;
//                    }
//                }
                foreach($old as $o){
                    if(!in_array($o['id'], $o_id)){
                        self::where('id',$o['id'])->delete();
                    }
                }
                foreach($p_city as $k=>$p){
                    if(isset($ids[$k]) && $ids[$k] != ''){
                        $up = self::where('id',$ids[$k])->first();
                        $up->direction = $p_city[$k];
                        $up->type = $p_type[$k];
                        $up->price = $p_price[$k];
                        $up->seat_count = $p_seat[$k];
                        $up->hour = $p_hour[$k];
                        $up->save();
                    }else{
                        $up = new self;
                        $up->tour_bus_id = $id;
                        $up->direction = $p_city[$k];
                        $up->type = $p_type[$k];
                        $up->price = $p_price[$k];
                        $up->seat_count = $p_seat[$k];
                        $up->hour = $p_hour[$k];
                        $up->save();
                    }
                }
                
            }else{
                foreach($p_city as $k=>$p){
                    $up = new self;
                    $up->tour_bus_id = $id;
                    $up->direction = $p_city[$k];
                    $up->type = $p_type[$k];
                    $up->price = $p_price[$k];
                    $up->seat_count = $p_seat[$k];
                    $up->hour = $p_hour[$k];
                    $up->save();
                }
            }
        }else{
            self::where('tour_bus_id',$id)->delete();
        }
    }
}
