<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class FavoriteRoute extends Model {

    
    public $table = 'favorite_route';
    protected $fillable = array('from','to','bus_id','image','in_footer');
    public $timestamps=false;
//    protected $appends = array('role');
    
//    public function scopeActive($query) {
//        return $query->where('status', '=', 1);
//    }
    
    public function fromCity(){
        return $this->hasOne('App\Models\Locality\Citi','id','from');
    }
    
    public function toCity(){
        return $this->hasOne('App\Models\Locality\Citi','id','to');
    }
    
    public function bus(){
        return $this->hasOne('App\Models\Serviceprovider\Buses','id','bus_id');
    }
    
    public function busPrice(){
        return $this->hasOne('App\Models\Admin\BusPrice','bus_id','bus_id');
    }
    
    public static function get_route($param){
        $am = self::orderBy('id','desc');
        if(isset($param['id'])){
            $am = $am->where('id',$param['id']);
        }
        $am = $am->get();
        
        return $am;
        
    }
    
    public static function add_route($param){
        $bnrimg = \Request::file('image');
        $img_name = self::add_image($bnrimg);
        $d = new self;
        $d->in_footer=isset($param['in_footer']) ? 1 : 0 ;
        $d->from=$param['from_city_id'];
        $d->to=$param['to_city_id'];
        $d->price=$param['price'];
        $d->bus_id=$param['bus_id'];
        $d->image=$img_name;
        $d->save();
        return $d;
    }
    
    public static function edit_route($param){
//        dd($param);
        $d = self::where('id',$param['cid'])->first();
        $filename = $d->image;
        $bnrimg = \Request::file('image');
        $img_name = self::add_image($bnrimg);
        
        if($img_name != ''){
            $d->image=$img_name;
            $filePath = config('constant.UPLOAD_FAV_ROUTE_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        
        $d->in_footer=isset($param['in_footer']) ? 1 : 0 ;
        $d->from=$param['from_city_id'];
        $d->to=$param['to_city_id'];
        $d->price=$param['price'];
        $d->bus_id=$param['bus_id'];
        $d->save();
        
        return $d;
    }
    
    public static function delete_route($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        $filename = $d->image;
        
        $d->delete();
        
        if($filename != ''){
            $filePath = config('constant.UPLOAD_FAV_ROUTE_DIR_PATH');
            if(file_exists($filePath.$filename)){
                unlink($filePath.$filename);
            }
        }
        
        return $d;
    }
    
    public static function add_image($bnrimg){
        
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$img_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    $filePath = config('constant.UPLOAD_FAV_ROUTE_DIR_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($img_name != ''){
                        $img_name = $img_name.','.$FileName;
                    }else{
                        $img_name = $FileName;
                    }
                }
//            
            
        }
        return $img_name;
    }


    public static function get_route_list($param){
        
        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            $count=$count->whereHas('fromCity',function($q)use($param){
                $q->where('name','like','%'.$param['search'].'%');
            });
        }
        if(isset($param['to_city']) && $param['to_city']!=''){
            $count=$count->whereHas('toCity',function($q)use($param){
                $q->where('name','like','%'.$param['to_city'].'%');
            });
        }
        if(isset($param['bus']) && $param['bus']!=''){
            $count=$count->whereHas('bus',function($q)use($param){
                $q->where('name','like','%'.$param['bus'].'%');
            });
        }
        
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $route=self::skip($start)->take($len)->orderBy('id','desc')->with(['fromCity','toCity','bus']);
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $route=self::orderBy('id','desc')->with(['fromCity','toCity','bus']);
            
            $route=$route->whereHas('fromCity',function($q)use($param){
                $q->where('name','like','%'.$param['search'].'%');
            });
            
        }
        
        if(isset($param['to_city']) && $param['to_city']!=''){
            $route=$route->whereHas('toCity',function($q)use($param){
                $q->where('name','like','%'.$param['to_city'].'%');
            });
        }
        if(isset($param['bus']) && $param['bus']!=''){
            $route=$route->whereHas('bus',function($q)use($param){
                $q->where('name','like','%'.$param['bus'].'%');
            });
        }
        $route = $route->get()->toArray();
//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$route;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function get_fav_routes(){
        $rlist = self::where('in_footer',0)->with(['fromCity','toCity','busPrice'])->get()->toArray();
        foreach($rlist as $key=>$r){
//            $rlist[$key]['bus_price']['price'] = \General::number_format($r['bus_price']['price']);
            $rlist[$key]['bus_price']['price'] = \General::number_format($r['price']);
        }
        return $rlist;
    }
    
    public static function get_footer_fav_routes(){
        $rlist = self::where('in_footer',1)->with(['fromCity','toCity','busPrice'])->get()->toArray();
        foreach($rlist as $key=>$r){
//            $rlist[$key]['bus_price']['price'] = \General::number_format($r['bus_price']['price']);
            $rlist[$key]['bus_price']['price'] = \General::number_format($r['price']);
            $rlist[$key]['date'] = date('Y-m-d',strtotime('+3 day'));
        }
        return $rlist;
    }
}
