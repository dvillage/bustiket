<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class BusPrice extends Model {

    
    public $table = 'bus_price';
    protected $fillable = array('bus_id','sorting','seat_count','price','route_id');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function getSeatPriceBySale($bus_id,$route,$nop,$date)
    {
//        dd($route);
        $pr = [];
        // best solution: cache buswise seat sell data in bus table field
        $seatSold = \App\Models\BookingSeats::where("bus_id",$bus_id)->where('journey_date',$date)
            ->where("status",  config("constant.STATUS_BOOKED"))->count();
//        $seatSold= 15;
        $seatSold = $seatSold + $nop ;
        foreach($route as $r){
            $price = self::where("bus_id",$bus_id)->where('route_id',$r['id'])->orderby("sorting","ASC")->get()->toArray();
//            $pr[] = $price->price; 
//            dd($price);
            $totalC = 0;
           
            $fl = 0;
            foreach($price as $p){
                $totalC = $totalC + $p['seat_count'] ;
                if($totalC >= $seatSold){
                    $fl = 1;
                    $pr[] = $p['price']; 
                    break;
                }
            }
            if(!$fl){
                $lastPr = end($price);
                $pr[] = $lastPr['price']; 
//                dd($pr);
            }
            
        }
        $rp = min($pr);
//        dd($pr);
//        if($seatSold == 0){
//            $price = self::where("bus_id",$bus_id)->orderby("sorting","ASC")->first();
//        }
//        if(is_null($price))
//            return 0;
//        return $price->price;
        if($rp)
            return $rp;
        return 0;
        
    }
    
    public static function add_price($seat_count,$price,$bus_id,$route_id,$i){
        if(count($seat_count)>0){
            $j = 1;
            foreach($seat_count as $k=>$s){
                $pr = new self();
                $pr->bus_id = $bus_id;
                $pr->sorting = $j;
                $pr->seat_count = $s;
                $pr->price = $price[$k][$i];
                $pr->route_id = $route_id;
                $pr->save();
                $j++;
            }
        }else{
            self::where('route_id',$route_id)->delete();
        }
    }
    public static function edit_price($seat_count,$price,$bus_id,$route_id,$i,$price_ids){
        if(count($seat_count)>0){
            $pre = array();
            $j = 1;
            $oldP = self::where('route_id',$route_id)->get()->toArray();
            $newP = array();
            
            foreach($oldP as $o){
               array_push($newP, $o['id']); 
            }
//            echo $route_id;
//            print_r($newP);
            foreach($seat_count as $k=>$s){
//                dd($price_ids);
                if(isset($price_ids[$k][$i]) && $price_ids[$k][$i] > 0){
                    
                    array_push($pre, (int)$price_ids[$k][$i]);
                    $pr = self::where('id',$price_ids[$k][$i])->first();
                    $pr->sorting = $j;
                    $pr->seat_count = $s;
                    $pr->price = $price[$k][$i];
                    $pr->save();
                }else{
//                    dd($price_ids[$k]);
                    $pr = new self();
                    $pr->bus_id = $bus_id;
                    $pr->sorting = $j;
                    $pr->seat_count = $s;
                    $pr->price = $price[$k][$i];
                    $pr->route_id = $route_id;
                    $pr->save();
//                    array_push($pre, $pr->id);
                }
                
                $j++;
            }
//            dd($pre);
            

            foreach($newP as $n){
                if(!in_array($n, $pre)){
                    self::where('id',$n)->delete();
                }
            }
        }else{
            self::where('route_id',$route_id)->delete();
        }
    }
    public static function get_terminal_price($param){
        $seatSold = \App\Models\BookingSeats::where("bus_id",$param['bus_id'])->where('journey_date',$param['date'])
            ->where("status",  config("constant.STATUS_BOOKED"))->count();
//        $seatSold= 15;
        $seatSold = $seatSold + $param['nop'] ;
        
        $route = BusRoutes::where(['bus_id'=>$param['bus_id'],'from_terminal_id'=>$param['from_terminal_id'],'to_terminal_id'=>$param['to_terminal_id']])->first();
        $price = self::where('route_id',$route->id)->get();
        $totalC = 0;
           
            $pr = 0;
            foreach($price as $p){
                $totalC = $totalC + $p['seat_count'] ;
                if($totalC >= $seatSold){
                    $pr = $p['price']; 
                    break;
                }
            }
        if($pr){
            $res = \General::success_res();
            $res['data'] = $pr;
        }else{
            $res = \General::error_res();
        }
        return $res;
    }
}
