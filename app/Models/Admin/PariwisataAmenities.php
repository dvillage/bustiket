<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class PariwisataAmenities extends Model {

    
    public $table = 'tour_bus_amenities';
    protected $fillable = array('bus_id','amenities_id');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function amenities(){
        return $this->hasOne('App\Models\Admin\Amenities','id','amenities_id');
    }
    
}
