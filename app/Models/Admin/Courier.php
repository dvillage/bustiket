<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model {

    
    public $table = 'courier';
    protected $fillable = array('name','status');
    public $timestamps=false;
//    protected $appends = array('role');
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public static function get_courier($param){
        $am = self::active()->orderBy('name','asc');
        if(isset($param['name'])){
            $am = $am->where('name','like','%'.$param['name'].'%');
        }
        $am = $am->get();
        return $am;
    }
    
    public static function add_courier($param){
//        dd($param);
        
        
        $d = new self;
        $d->name=$param['name'];
        $d->status=isset($param['status']) ? 1 : 0 ;
        $d->save();
        
        return $d;
    }
    
    public static function edit_courier($param){
//        dd($param);
        $d = self::where('id',$param['c_id'])->first();
        $d->name=$param['name'];
        $d->status=isset($param['status']) ? 1 : 0 ;
        $d->save();
        
        return $d;
    }
    public static function delete_courier($param){
//        dd($param);
        $d = self::where('id',$param['id'])->first();
        
        $d->delete();
        
        return $d;
    }
    
    
    public static function get_courier_list($param){
        $count=self::count();
        
        if(isset($param['search']) && $param['search']!=''){
            $count=self::where('name','like','%'.$param['search'].'%')->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $district=self::skip($start)->take($len)->orderBy('id','desc')->get()->toArray();
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $district=self::where('name','like','%'.$param['search'].'%')->orderBy('id','desc')->skip($start)->take($len)->get()->toArray();
        }

//        dd($district);
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$district;
        $res['flag']=$flag;
        return $res;
    }
}
