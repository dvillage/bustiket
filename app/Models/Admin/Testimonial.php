<?php 

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model {

    
    public $table = 'testimonial';
    protected $fillable = array('email','name','message','image','video','status');
    public $timestamps=true;

    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    
    
    public static function addTestimonial($param){
        
        $tbl=new Self;
        
        $tmpImage=\Request::file('image');
        $image = self::addImage($tmpImage);
        $tmpVideo=\Request::file('video');
        $video = self::addVideo($tmpVideo);
       
        $tbl->name=$param['uname'];
        $tbl->email=$param['email'];
        $tbl->message=$param['message'];
        $tbl->image=$image;
        $tbl->video=$video;
        $tbl->status=isset($param['status'])?1:0;
        $tbl->save();
        
        return $tbl;
    }
    
    public static  function updateTestimonial($param){
        
                $tbl = self::where('id',$param['tid'])->first();
                
                $imageFilename = $tbl->image;
                $videoFilename = $tbl->video;
                
                $bnrimg = \Request::file('image');
                $image = self::addImage($bnrimg);
                
                $bnrimg = \Request::file('video');
                $video = self::addVideo($bnrimg);
                
                

                if($image != ''){
                    $tbl->image=$image;
                    $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
                    if(file_exists($filePath.$imageFilename)){
                        unlink($filePath.$imageFilename);
                    }
                }
                
                if($video != ''){
                    $tbl->video=$video;
                    $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
                    if($videoFilename==''){
                        $videoFilename='sample.mp4';
                    }
                    if(file_exists($filePath.$videoFilename)){
                        unlink($filePath.$videoFilename);
                    }
                }
                
                $tbl->name=$param['uname'];
                $tbl->email=$param['email'];
                $tbl->message=$param['message'];
                $tbl->status=isset($param['status'])?1:0;
                $tbl->save();
        
                return $tbl;
    }
    
    
     public static function deleteTestimonial($param){
       
        $tbl = self::where('id',$param['id'])->first();
        $imageFilename = $tbl->image;
        $videoFilename = $tbl->video;
        $tbl->delete();

        if($imageFilename != ''){
            $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
            if(file_exists($filePath.$imageFilename)){
                $v=unlink($filePath.$imageFilename);
            }
        }
        
        if($videoFilename != ''){
            $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
            if(file_exists($filePath.$videoFilename)){
                unlink($filePath.$videoFilename);
            }
        }
        
        return $tbl;
    }


    public static function addImage($bnrimg){
        
        $img_ext = ['jpg','jpeg','png','icn'];
        $img_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$img_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".  implode(',', $img_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($img_name != ''){
                        $img_name = $img_name.','.$FileName;
                    }else{
                        $img_name = $FileName;
                    }
                }
        }
        return $img_name;
    }
    
    
       
    public static function addVideo($bnrimg){
        
        $video_ext = ['wav','mp4','.mkv'];
        $video_name = '';
        if(isset($bnrimg)){

                foreach($bnrimg as $bnrimgs){
                    $ext = $bnrimgs->getClientOriginalExtension();
                    if(!in_array(strtolower($ext),$video_ext)){
                        $json = \General::validation_error_res();
                        $json['msg'] = "Supported Extenstion are ".implode(',', $video_ext);
                        $res = response()->json($json,422);
                        return $res;
                    }
                    
                    $filePath = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH');
                    $FileName = date('YmdHis').rand(100,999) . '.' . $bnrimgs->getClientOriginalExtension();
                    $bnrimgs->move($filePath,$FileName);
                    if($video_name != ''){
                        $video_name = $video_name.','.$FileName;
                    }else{
                        $video_name = $FileName;
                    }
                }            
        }
        return $video_name;
    }
    
    public static function getTestimonialList($param){
       
        $count=self::orderBy('id','desc');
        
        if(isset($param['search']) && $param['search']!=''){
            
           $count=$count->where('email','like','%'.$param['search'].'%');
           
        }
        
        $count = $count->count();
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
                
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $route=self::skip($start)->take($len)->orderBy('id','desc');
        
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $route=self::orderBy('id','desc');
            
            $route= $route->where('email','like','%'.$param['search'].'%');
            
            
        }
        
        $route = $route->get()->toArray();
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$route;
        $res['flag']=$flag;
        return $res;
        
    }
    
     public static function getTestimonialListAll(){
        $data=self::active()->orderBy('updated_at','DSC')->get()->toArray();
        return $data;
     }
}
