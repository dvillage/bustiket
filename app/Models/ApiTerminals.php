<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ApiTerminals extends Model {
    protected $fillable = [
        'city_id', 'city_name', 'pick_up_points'
    ];
    protected $table = 'api_pickup_points';
    protected $hidden = [];
    public $timestamps=true;
    
    
    
    

}
