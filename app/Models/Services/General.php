<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

class General extends Model {

    public static function check_facebook_access_token($token) {
        $url = "https://graph.facebook.com/me?fields=email,name,id&access_token=" . $token;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (isset($result['email'])) {
            $res = \General::success_res();
            $res['data']['email'] = $result['email'];
            $res['data']['name'] = $result['name'];
            return $res;
        }
        $res = \General::error_res("not_authorise");
        return $res;
    }

    public static function check_google_access_token($token) {
        $url = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=" . $token;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (isset($result['email'])) {
            $res = \General::success_res();
            $res['data']['email'] = $result['email'];
            $res['data']['name'] = "";
            return $res;
        }
        $res = \General::error_res("not_authorise");
        return $res;
    }

    public static function get_promo_banner1() {
        $prom_banner = \DB::table('promo_banner1')->get();
        if (is_null($prom_banner)) {
            $res = \General::success_res("Promo Banner Not found");
            $res['data'] = [];
            return $res;
        }
        $res = \General::success_res();
        foreach ($prom_banner as $banner) {
            $res['data'][] = ['id' => $banner['id'],
                'banner_img' => self::trim_parent_url(\URL::to('/../images/' . $banner['banner_img'])),
                'link' => $banner['link']
            ];
        }
        return $res;
    }

    public static function get_promo_banner2() {
        $prom_banner = \DB::table('promo_banner2')->get();
        if (is_null($prom_banner)) {
            $res = \General::success_res("Promo Banner Not found");
            $res['data'] = [];
            return $res;
        }
        $res = \General::success_res();
        foreach ($prom_banner as $banner) {
            if(env("APP_ENV") == "live"){
                $res['data'][] = ['id' => $banner['id'],
                'banner_img' => self::trim_parent_url(secure_url('/../images/' . $banner['banner_img'])),
                'description' => $banner['description'],
                'link' => $banner['link']
                ];
            }else{
                $res['data'][] = ['id' => $banner['id'],
                'banner_img' => self::trim_parent_url(\URL::to('/../images/' . $banner['banner_img'])),
                'description' => $banner['description'],
                'link' => $banner['link']
                ];
            }
            
        }
        return isset($res['data']) ? $res['data'] : [];
    }

    public static function search_bus($param) {
        $loc = [
            'Province'  => 'P',
            'City'      => 'C',
            'District'  => 'D',
            'Terminal'  => 'T',
        ];
        
        $current_date = date('Y-m-d');
//        $three_day = \Date('Y-m-d', strtotime("+2 days"));
        $three_day = \Date('Y-m-d');
        $date_diff = \General::date_dif($three_day, $param['date'], "d");
        
        if ($date_diff < 0)
            return \General::error_res("Invalid Date Selection");

        $fc = explode('(',$param['from_city']);
//        dd($fc);
        $fc_name = trim($fc[0]);
        $fc_type = $loc['City'];
        if(count($fc) > 1 && $fc[1] != ''){
            $fc = explode(')',$fc[1]);
            $fc_type = $loc[trim($fc[0])];
        }
        
        $tc = explode('(',$param['to_city']);
        $tc_name = trim($tc[0]);
        $tc_type = $loc['City'];
        if(count($tc) > 1 && $tc[1] != ''){
            $tc = explode(')',$tc[1]);
            $tc_type = $loc[trim($tc[0])];
        }
        
        
//        dd($fc_name,$fc_type,$tc_name,$tc_type);
//        $from_city = \App\Models\Locality::where("name", $param['from_city'])->first();
        $from_city = \App\Models\Locality::where(["name"=>$fc_name,'type'=>$fc_type])->first();
        if (is_null($from_city)) {
            return \General::error_res("No bus found");
        }
        $param['from_id'] = $from_city['lid'];

//        $to_city = \App\Models\Locality::where("name", $param['to_city'])->first();
        $to_city = \App\Models\Locality::where(["name"=>$tc_name,'type'=>$tc_type])->first();
        if (is_null($to_city)) {
            return \General::error_res("No bus found");
        }
        $param['to_id'] = $to_city['lid'];
        
//        dd($from_city->toArray(),$to_city->toArray());

        $formated_date = date('d-m-Y', strtotime($param['date']));

        $data = [
            'from' => [
                'name' => $from_city['name'],
                'type' => $from_city['type'],
            ],
            'to' => [
                'name' => $to_city['name'],
                'type' => $to_city['type'],
            ],
            'date' => $param['date'],
            'nop' => isset($param['nop']) ? $param['nop'] : 1,
            'sp_list'   => isset($param['operator_id']) && $param['operator_id'] != '' ? explode(',',$param['operator_id']) : [],
            'type'      => isset($param['type_id']) && $param['type_id'] != '' ? explode(',',$param['type_id']) : [],
            'amenities' => isset($param['feature_id']) && $param['feature_id'] != '' ? explode(',',$param['feature_id']) : [],
        ];
        $buses = \App\Models\Serviceprovider\Buses::search($data);
        
        $businfo_data = [];
        $operator = [];
        foreach ($buses as $key => $val) {
            if($key !== 'roda' && $key !== 'lorena'){
                $op = [
                    'SP_name'   => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'SP_id'     => $val['bus']['sp']['id'],
                ];
                $f = 1;
                foreach($operator as $k=>$o){
                    if($operator[$k]['SP_id'] == $val['bus']['sp']['id']){
                        $f = 0;
                        break;
                    }
                }
                if($f == 1){
                    $operator[] = $op;
                }

                $luxitem = '';
                if(isset($val['bus']['amenities']) && count($val['bus']['amenities']) > 0 ){
                    foreach($val['bus']['amenities'] as $am){
                        $luxitem = $luxitem.$am['aname'].',';
                    }
                }

//                $businfo_data[$key] = [
                $businfo_data[] = [
                    'Bus_id'    => $val['bus']['id'],
                    'service_provider_name' => $val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'],
                    'luxitem'   => rtrim($luxitem, ", "),
                    'rute'      => $val['routes'][0]['route'],
                    'parent_busname'    => $val['bus']['name'],
                    'available_seats'   => $val['bus']['total_seats'],
                    'bus_fare'  => \General::number_format($val['bus']['price'], 3),
                    'time'      => date('H:i', strtotime($val['routes'][0]['boarding_time'])).' - '.date('H:i', strtotime($val['routes'][0]['droping_time'])),
                    'from_date' => date("Y-m-d", strtotime($val['routes'][0]['boarding_time'])),
                    'to_date'   => date("Y-m-d", strtotime($val['routes'][0]['droping_time'])),
                    'duration'  => $val['routes'][0]['duration'],
                ];
            }else{
                foreach($val['bus'] as $b){
//                    dd($val['sp']);
                    $time = '';
                    $dur = '';
                    foreach($b['terminals'] as $t ){
                        $time = $t[0]['departure_time'].' - ' .$t[0]['arrival_time'];
                        $dur = $t[0]['duration'];
                        break;
                    }
                    
                    $op = [
                        'SP_name'   => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'SP_id'     => $val['sp']['id'],
                    ];
                    $f = 1;
                    foreach($operator as $k=>$o){
                        if($operator[$k]['SP_id'] == $val['sp']['id']){
                            $f = 0;
                            break;
                        }
                    }
                    if($f == 1){
                        $operator[] = $op;
                    }
//                    dd($val);
                    $businfo_data[] = [
                        'Bus_id' => $b['route_code'],
                        'service_provider_name' => $val['sp']['first_name'].' '.$val['sp']['last_name'],
                        'luxitem' => '',
                        'rute' => $b['route'],
                        'parent_busname' => $b['bus'],
                        'available_seats' => $b['total_seat'],
                        'bus_fare' => \General::number_format($b['fare'], 3),
                        'time' => $time,
                        'duration' => $dur,
                    ];
                }
            }
            
        }
      
//        $apiBus = \App\Models\Services\General::getBusFromApi($param['from_city'], $param['to_city'], $param['date']);
//        $extraBus = array();
//        if(count($apiBus) > 0){
//            $operators = \App\Models\ServiceproviderInfo::where('SP_id',  config('constant.SP_FOR_API'))->first();
//            $sp_name = $operators->SP_name;
//            
//            foreach($apiBus as $k=>$aBus){
//                $boarding_time = date("H:i", strtotime($aBus['departure_time']));
//                $departure_time = date("H:i", strtotime($aBus['arrival_time']));
//                
//                $dt = "2016-04-29 " . $departure_time . ":00";
//                $bt = "2016-04-29 " . $boarding_time . ":00";
//                $duration = \General::time_elapsed_string($bt, $dt);
//                
//                $bus_time = $boarding_time . " - " . $departure_time;
//                $boarding = array();
//                $dropping = array();
//                $boarding[0]['boarding_name']=$aBus['board_point'];
//                $boarding[0]['boarding_address']='';
//                $boarding[0]['boarding_time']=$boarding_time;
//                
//                $dropping[0]['dropping_name']=$aBus['drop_point'];
//                $dropping[0]['dropping_address']='';
//                $dropping[0]['dropping_time']=$departure_time;
//                $points = array();
//                $points['boarding']=$boarding;
//                $points['dropping']=$dropping;
//                
//                $extraBus[$k] = [
//                    'Bus_id' => $aBus['route_code'],
//                    'service_provider_name' => $sp_name,
//                    'luxitem' => '',
//                    'rute' => $aBus['route'],
//                    'parent_busname' => $aBus['bus'],
//                    'available_seats' => count($aBus['available_seat']),
//                    'bus_fare' => \App\Models\BusFare::bus_fare_formated_string($aBus['fare']),
//                    'time' => $bus_time,
//                    'duration' => $duration,
//                    'points'=>$points,
//                ];
//            }
//        }
        
//        $businfo_data = array_merge($businfo_data, $extraBus);
        \Log::info('bus list : '.  json_encode($businfo_data));

        $type = [
            [   "typeID" => 'bus',"typeName"=> "Bus",   ],
            [   "typeID" => 'shuttle',"typeName"=> "Shuttle",   ],
            [   "typeID" => 'travel',"typeName"=> "Travel",   ],
            
        ];
        
        if(count($businfo_data) == 0){
            $res = \General::error_res('No Bus Found.');
            return $res;
        }
        
        $businfo_data = \App\Models\General::set_by_price_order($businfo_data);
        
        $feature = \App\Models\Admin\Amenities::get(['id as lux_id','name as lux_name'])->toArray();
        $data = [
            'bus' => $businfo_data,
            'from_id' => $from_city['id'],
            'to_id' => $to_city['id'],
            'filter' => [
                'operator' => $operator,
                'type' => $type,
                'feature' => $feature,
            ]
        ];
        
//        $apiBus = General::getBusFromApi($param['from_city'], $param['to_city'], $param['date']);
        //print_r($apiBus);exit;
        $res = \General::success_res();
        $res['data'] = $data;
        return $res;
    }

    public static function get_total_seat($bus_id)
	{
		$i=0;
                $seat_no = [];
		//echo "select seat_no from bus_structure where bus_id = $bus_id and seat_no != 'xx' and seat_no !='' ";

		$sel = \DB::select("select seat_no from bus_structure where bus_id = $bus_id and seat_no != 'xx' and seat_no !='' ");
                foreach($sel as $s){
                    		 $seat_no[$i]= $s['seat_no']; $i++;
                }
			
		 
			
		 return $seat_no;
	}

    public static function get_all_bank($id = 0) {
        $bank = [
            [
                "id" => 1,
                "bank_name" => "Bank Central Asia (BCA)",
                "bank_short_name" => "BCA",
                "bank_address" => "KCP Blok A Cipete",
                "account_name" => "PT.BUSTIKET Global Technology",
                "account_no" => "218-1888-280",
            ],
            /*[
                "id" => 1,
                "bank_name" => "Bank Central Asia (BCA)",
                "bank_short_name" => "BCA",
                "bank_address" => "KCP Blok A Cipete",
                "account_name" => "CV. Mitra Indo Visi Group",
                "account_no" => "218-008-8809",
            ],
            [
                "id" => 2,
                "bank_name" => "Bank Mandiri",
                "bank_short_name" => "MB",
                "bank_address" => "Pondok Indah Office Tower 3",
                "account_name" => "CV. Mitra Indo Visi Group",
                "account_no" => "1010007320813",
            ],*/
        ];
        if ($id) {
            foreach ($bank as $key => $val) {
                if ($val['id'] == $id)
                    return $val;
            }
        }
        return $bank;
    }

    public static function get_time_to_make_payment($book_date = "") {

        $ttmp = config("constant.TIME_TO_MAKE_PAYMENT");
        $cd = date("Y-m-d H:i:s");
//        \Log::info("Booking date");
//        \Log::info($book_date);
//        \Log::info("Current Date");
//        \Log::info($cd);
        if ($book_date != "") {

            $diff_minute = self::date_dif($book_date, $cd, "i");
            $ttmp -= $diff_minute;
        }
        return $ttmp;
    }
    
    public static function date_dif($start, $end, $return_type) {
        $dEnd = strtotime($end);
        $dStart = strtotime($start);
        $dDiff = ($dEnd - $dStart);
        if (strtolower($return_type) == "y") {
            $dDiff = $dDiff / 60 / 60 / 24 / 365;
        } else if (strtolower($return_type) == "m") {
            $dDiff = ($dEnd - $dStart) / 60 / 60 / 24 / 30;
        } else if (strtolower($return_type) == "d") {
            $dDiff = ($dEnd - $dStart) / 60 / 60 / 24;
        } else if (strtolower($return_type) == "h") {
            $dDiff = $dDiff / 60 / 60;
        } else if (strtolower($return_type) == "i") {
            $dDiff = ($dEnd - $dStart) / 60;
        } else if (strtolower($return_type) == "s") {
            $dDiff = $dEnd - $dStart;
        }
        return floor($dDiff);
    }

    public static function send_nusa_sms($mobile, $sms) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => config("constant.NUSA_SMS_URL"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'user' => config("constant.NUSA_SMS_USERNAME"),
                'password' => config("constant.NUSA_SMS_PASSWORD"),
                'SMSText' => $sms,
                'GSM' => '62' . $mobile
            )
        ));
        if (env("APP_ENV") != "local") {
            $res = curl_exec($curl);
            curl_close($curl);
        } else {
            $res = true;
        }

        return \General::success_res();
    }

    public static function dashboard($param) {
        $date = date('Y-m-d', strtotime('+3 days'));
        $rlist = \App\Models\Admin\FavoriteRoute::get_fav_routes();
        $banners = \App\Models\Admin\Banners::active()->where('group', 'header-slider')->get()->toArray();
        $promobnr = \App\Models\Admin\Banners::active()->where('group', 'passenger-app-promo-slider')->orderBy('created_at','DESC')->first();
        if(!is_null($promobnr))
                $promobnr = $promobnr->toArray();
        else
            $promobnr = [];
//        dd($rlist);
        $data = [];
        $favorite_cities = [];
        $banner = [];
        $promo=  [];
        foreach($rlist as $key=>$f){
            $fr = [];
            $fr['title'] = $f['from_city']['name'];
            $fr['from_to'] = $f['from_city']['name'].' - '.$f['to_city']['name'];
            $fr['price'] = "Rp. ".$f['bus_price']['price'];
            $fr['from_id'] = $f['from_city']['id'];
            $fr['to_id'] = $f['to_city']['id'];
            $fr['img'] = \URL::to("assets/uploads/fav_routes").'/'.$f['image'];
            $fr['date'] = $date;
            $data[] = $fr;
            
            $fc = [];
            $fc['name'] = $f['from_city']['name'];
            $fc['id'] = $f['from_city']['id'];
            $favorite_cities[] = $fc;
        }
        
        $bnr = [];
        $bnr['image'] = \URL::to("assets/images/banner").'/'.$banners[0]['file'];
        $bnr['description'] = '';
        $bnr['weburl'] = $banners[0]['redirect_to'];
        $banner[]=$bnr;
        
//        $prm = [];
//        $prm['id'] = $banners[1]['id'];
//        $prm['image'] = \URL::to("assets/images/banner").'/'.$banners[1]['file'];
//        $prm['description'] = '';
//        $prm['link'] = $banners[1]['redirect_to'];
//        $promo[] = $prm;
        
        $prm = [];
        if(!empty($promobnr))
        {
            $prm['id'] = $promobnr['id'];
            $prm['image'] = \URL::to("assets/images/banner").'/'.$promobnr['file'];
            $prm['description'] = '';
            $prm['link'] = $promobnr['redirect_to'];
        }
        $promo[] = $prm;
        
        $res = \General::success_res();
        $res['data'] = [
            "favorite_cities" => $favorite_cities,
            "favorite_place" => $data,
            "banner" => $banner,
            "banner2"=>$promo
        ];
        return $res;
    }

    public static function do_bus_operator_login($param) {
        \App\Models\Operator\User::where("");
    }

    public static function trim_parent_url($url) {
        $url = str_replace("api/../", "", $url);
        return $url;
    }

    public static function get_promocode_banner() {
        $prom_banner = \DB::table('promo_banner1')->get();
        $data = [];
        foreach ($prom_banner as $banner) {
            if(env("APP_ENV") == "live"){
                $data[] = [
                "image" => self::trim_parent_url(secure_url('/../images/' . $banner['banner_img'])),
                "description" => $banner['description'],
                "weburl" => $banner['link']
                ];
            }else{
                $data[] = [
                "image" => self::trim_parent_url(\URL::to('/../images/' . $banner['banner_img'])),
                "description" => $banner['description'],
                "weburl" => $banner['link']
                ];
            }
            
            
            
        }
        return $data;
    }

    public static function formate_mobile_no($mobile) {
        $first_four = substr($mobile, 0, 4);
        $rest_of_four = substr($mobile, 4);
        if (preg_match('/^62/', $first_four) === 0) {
            if (preg_match('/^\+/', $first_four) === 0) {
                $first_four = "+62" . $first_four;
            }
        }
        if (preg_match('/^\+/', $first_four) === 0) {
            $first_four = "+" . $first_four;
        }
        $first_four = preg_replace('/^\+620/', "+62", $first_four);

        $formated_str = $first_four . $rest_of_four;
        return $formated_str;
    }
    
    public static function getBusFromApi($from,$to,$journy_date,$sitToBook = 0){
    //orgin = BLORA,PEMALANG,Purbalingga;
    //dest = CEPU,Jakarta;
    $param = 'app=data&action=get_pickup_point';
    $pickUpPoint = self::makeRodaRequest($param);
//    echo $_SERVER['HTTP_USER_AGENT'] ;
//    pr($pickUpPoint);
    $org_code = '';
    $terminalPoints = array();
    $destPoints = array();
    $singleTerminal = '';
    $mainOrigin = '';
    $mainDest = '';
    $busKey = 0;
    if(isset($pickUpPoint)){
    if($pickUpPoint['err_code'] != 0){
        return null;
    }else{
        $brb = 0;
        $drb = 0;
        foreach($pickUpPoint['pickup_point'] as $pp){
            
            if(trim(strtolower($pp[1])) == trim(strtolower($from)) ){
                $org_code = $pp[0];
                $mainOrigin = $pp[0];   
                $terminalPoints = $pp[2];
                $brb = 1;
            }elseif(count($pp[2]) > 0){
                foreach($pp[2] as $p){
                    if(trim(strtolower($p[1])) == trim(strtolower($from)) ){
                        $org_code = $p[0];
                        $mainOrigin = $p[0];   
                        $terminalPoints = $pp[2];
                        $brb = 1;
                    }
                }
            }
            
            if(trim(strtolower($pp[1])) == trim(strtolower($to)) ){
                $mainDest = $pp[0];
                $destPoints = $pp[2];
                $drb = 1;
            }elseif(count($pp[2]) > 0){
                foreach($pp[2] as $p){
                    if(trim(strtolower($p[1])) == trim(strtolower($to)) ){
                        $mainDest = $p[0];
                        $destPoints = $pp[2];
                        $drb = 1;
                    }
                }
            }
            
            if($brb == 1 && $drb == 1){
                break;
            }
        }
        if($mainOrigin != ''){
            $tc = 0;
            foreach($terminalPoints as $tp){
                
                if(!in_array($mainOrigin, $tp)){
                    $tc++;    
                }else{
                    $tc = 0;
                    break;
                }
            }
            if($tc > 0 )
                $ter = array_push($terminalPoints, array($mainOrigin,$from));
        }
        if($mainDest != ''){
            $tc = 0;
            foreach($destPoints as $tp){
                
                if(!in_array($mainDest, $tp)){
                    $tc++;    
                }else{
                    $tc = 0;
                    break;
                }
            }
            if($tc > 0 )
                $des = array_push($destPoints, array($mainDest,$to));
        }
//        pr($terminalPoints);
//        pr($destPoints);
//        echo $org_code;
        if($org_code == ''){
            return null;
        }else{
            $busList = array();
           
          foreach($terminalPoints as $ter){
              
            
            $param = 'app=data&action=get_des_by_org&org='.$ter[0];
//            $param = 'app=data&action=get_des_by_org&org=TPB';
            $destPoint = self::makeRodaRequest($param);
            
//            echo '<br>';
//            pr($destPoint);
            $dest_code = '';
            if(isset($destPoint)){
            if($destPoint['err_code'] != 0){
                \Log::info('get destination by origin : '. json_encode($destPoint));
//                return null;
            }else{
                $destPointArry = array();
                if(count($destPoint['origin_destination']) > 0){
                    
//            pr($destPoint['origin_destination']);
            
                    foreach($destPoint['origin_destination'] as $dp){
                        
                        if(is_array($dp)){
                            
                            if(count($dp) > 0){
                                
                                foreach($dp as $d){
                                    foreach($destPoints as $k=>$dps){
                                        if(strtolower($d[0]) == strtolower($dps[0])){
                                            $dest_code = $d[0];
                                          
                                            $destPointArry[$k][0] = $d[0];
                                            $destPointArry[$k][1] = $d[1];
                                        }
//                                        echo $dps[0];
//                                        echo '<br>';
                                    }
//                                    echo '<br>';
                                }
                               
                                if($dest_code == ''){
//                                    return null;
                                } 
//                                else{
//                                    echo $dest_code;
//                                }
                            }else{
//                                return null;
                                            continue;
                            }
                        }
                    }
                      
                }else{
                    return null;
                }
//                pr($destPointArry);
                $route_code = '';
                
//                if($dest_code != ''){
                if(count($destPointArry) > 0){
                    foreach($destPointArry as $dest_code){
                    $date = date('Ymd',  strtotime($journy_date));
                    $param = 'app=information&action=get_schedule_v2&org='.$ter[0].'&des='.$dest_code[0].'&trip_date='.$date;
                    $schedule = self::makeRodaRequest($param);
//                    echo '<br>';
//                    pr($schedule);
                    if(isset($schedule)){
                    if($schedule['err_code'] != 0){
                        \Log::info('get schedule v2 : '. json_encode($schedule));
//                        return null;
                    }else{
                        
                        $direct = $schedule['schedule'][0];
                        foreach($direct as $k=>$dir){
                            
                          //$busList[$k]['route'] = $dir[11];
                            $seatAvabilityArray = $dir[10];
                            $totalSeat = 0;
                            foreach($seatAvabilityArray as $sa){
                                $totalSeat = $totalSeat + (int)$sa[1];
                            }
                            
                            if($totalSeat == 0){
//                                return null;
                            }
                            
                            $route = explode('-', $dir[11]);
                            $routes = '';
                             foreach($route as $r=>$v){
                                 foreach($pickUpPoint['pickup_point'] as $pp){
            
                                        if(trim(strtolower($pp[0])) == trim(strtolower($v)) ){
                                            $routes .= $pp[1].'-';
                                        }elseif(count($pp[2]) > 0){
                                            foreach($pp[2] as $p){
                                                if(trim(strtolower($p[0])) == trim(strtolower($v)) ){
                                                    $routes .= $p[1].'-';
                                                }
                                            }
                                        }

                                    }
                             }
                            
                            $route_code = explode('-', $dir[0]);
                            
                            $param = 'reg_code='.$dir[8].'app=information&action=get_seat_map';
                            $seatMap = self::makeRodaRequest($param);
                            
                            $seatmapArray = array();
                            $seatclass = '';
                            if($seatMap['err_code'] != 0){
                                \Log::info('get seat map: '. json_encode($seatMap));
                                continue;
                            }else{
                                $seatmapArray = $seatMap['seat_map'];
                                $seatclass = $seatmapArray[0][4];
                            }
                            
                            if(count($seatmapArray) == 0){
                                \Log::info('seat map array : '. json_encode($seatMap));
                            }
                            
                            $param = 'app=information&action=get_available_seat&org='.$mainOrigin.'&des='.$mainDest.'&trip_date='.$date.'&route_code='.$route_code[1].'&class='.$seatclass;
                            $availSeat = self::makeRodaRequest($param);
//                            pr($availSeat);
                            if($availSeat['err_code'] != 0){
                                \Log::info('available seat : '. json_encode($payfare));
                                continue;
                            }else{
                                $availSeat = $availSeat['available_seat'];
                            }
                            if(count($availSeat) > 0){
                                $totalSeat = count($availSeat);
                            }else{
                                continue;
                            }
                            $param = 'app=information&action=get_fare_v2_new&org='.$ter[0].'&des='.$dest_code[0].'&trip_date='.$date.'&route_code='.$route_code[1].'&return_trip=0';
                            $payfare = self::makeRodaRequest($param);
//                            pr($payfare);
                            if($payfare['err_code'] != 0){
                                \Log::info('get fare v2 new : '. json_encode($payfare));
//                                continue;
                            }else{
                                $fare = 0;
                                $subclass ='';
                                foreach($seatAvabilityArray as $k1=>$sa){
                                    if($fare == 0 && $subclass == ''){
                                        if( (int) $sa[1] > 0 && $sa[1] >= $sitToBook){
                                        
                                            $subclass = $payfare['fare_info'][$k1][0];
                                            $subc = explode('/', $subclass)[0];
                                            if($subc == $sa[0]){
                                                $fare = $payfare['fare_info'][$k1][1][0];
                                                $subclass = $payfare['fare_info'][$k1][0];
                                            }
                                        }
                                    }
                                    
                                }
                                
                                $busList[$busKey]['from'] = $from;
                                $busList[$busKey]['to'] = $to;
                                $busList[$busKey]['org_code'] = $ter[0];
                                $busList[$busKey]['dest_code'] = $dest_code[0];
                                $busList[$busKey]['trip_date'] = $date;
                                $busList[$busKey]['main_org_code'] = $mainOrigin;
                                $busList[$busKey]['main_dest_code'] = $mainDest;
                                $busList[$busKey]['route_code'] = $dir[0];
                                $busList[$busKey]['board_point'] = $ter[1];
                                $busList[$busKey]['drop_point'] = $dest_code[1];
                                $busList[$busKey]['total_seat'] = $totalSeat;
                                $busList[$busKey]['departure_date'] = $dir[3];
                                $busList[$busKey]['arrival_date'] = $dir[4];
                                $busList[$busKey]['departure_time'] = substr(trim($dir[5]),0,4);
                                $busList[$busKey]['arrival_time'] = substr(trim($dir[6]),0,4);
                                $busList[$busKey]['duration'] = $dir[7];
                                $busList[$busKey]['bus'] = $busName.$dir[8];
                                $busList[$busKey]['subclass'] = $subclass;
                                $busList[$busKey]['fare'] = $fare;
                                $busList[$busKey]['route_code'] = $dir[0];
                                $busList[$busKey]['route'] = rtrim($routes,'- ');
                                $busList[$busKey]['seat_map'] = $seatmapArray;
                                $busList[$busKey]['available_seat'] = $availSeat;
                               
                                $busKey++;
                                
                                
                            }
                            
                        }
                               
                    }
                  }
                }
                }
                
                
            }
          }
          }
          $_SESSION['api_bus_list'] = $busList;
          
            $from_city = \App\Models\Cities::where("city_name", $from)->first();
            if (is_null($from_city)) {
                
            }
            $from_city = $from_city['id'];
            $to_city = \App\Models\Cities::where("city_name", $to)->first();
            if (is_null($from_city)) {
                
            }
            $to_city = $to_city['id'];
          
          $travel_date = date('Y-m-d',  strtotime($journy_date));
          $today = date('Y-m-d');
          $deleteOldBus = Buslist::whereDate('travelling_date', '<', $today)->delete();
          
          
          $ifExist = Buslist::where('from_city',  $from_city)->where('to_city',  $to_city)->where('travelling_date',  $travel_date)->first();
          
          if(count($ifExist) == 0){
              $saveBus = new Buslist();
              $saveBus->from_city = $from_city;
              $saveBus->to_city = $to_city;
              $saveBus->travelling_date = strtolower($travel_date);
              $saveBus->bus_list = serialize($busList);
              $saveBus->save();
          }else{
              $ifExist->bus_list = serialize($busList);
              $ifExist->save();
          }
//          session(['api_bus_list' => $busList]);
          \Session::set('api_bus_list', $busList);

          self::$apiBus = $busList;
//          pr($busList);
          return $busList;
        }
        
    }
    }
    //echo $org_code;
    
}
public static $apiBus = [];

public static function getDetailApiBus($busList,$route_code,$type = ''){
    foreach($busList as $list){
        if(trim($list['route_code']) == trim($route_code)){
            return $list;
        }
    }
}

    public static function makeRodaRequest($param,$print = false){
        
        $host = $_SERVER['HTTP_HOST'];
        $rqid = env("RQID","");
        $op_code = env("OP_CODE","");
        $roda_url = env("RODA_URL",'http://ws.roda.demo.mis.sqiva.com/?');
        $param = $param.'&rqid='.$rqid.'&op_code='.$op_code;
        $url = $roda_url.$param;
        if($print){
            echo $url;
        }
        
//        $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
        $strCookie = 'PHPSESSID=' . session_id() . '; path=/';


        $ch =  curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $res = curl_exec($ch);


        if(curl_error($ch))
        {
//            echo 'error:' . curl_error($ch);
        }
    
        curl_close($ch);
        $r = json_decode($res,true);
        if($r['err_code'] != 0){
           \Log::info('roda url : '.  $url);
        }
    
            return json_decode($res,true);
   
    }
    
    public static function makeRodaBooking($busDetail,$booker_name,$mobile,$tot_seat,$param){
            
            $action = 'booking_v2';
            $org = $busDetail['org_code'];
            $dest = $busDetail['dest_code'];
            $dep_route_code = explode('-',$busDetail['route_code'])[1];
            $dep_date = $busDetail['departure_date'];
            $subclass_dep = urlencode($busDetail['subclass']);
            $caller = urlencode(substr($booker_name,0,30));
            $contact_1 = 'testAPI';//urlencode(substr($booker_name,0,30));
            $adult_detail = '';
            $num_pax_adult = trim($tot_seat);
            
            $bus_fare = $busDetail['fare'] * $tot_seat;
            $j = 1 ;
            for ($i = 0; $i < $tot_seat; $i++) {
                $pass_name = urlencode(substr($param['passenger'][$i]['firstname'],0,50));
                $gender    = $param['passenger'][$i]['gender'];
                if($gender == 'L'){
                    $g = 'MR';
                }else{
                    $g = 'MS';
                }
                
                $adult_detail .= '&a_first_name_'.$j.'='.$pass_name.'&a_salutation_'.$j.'='.$g.'&a_mobile_'.$j.'='.$mobile;
                $j++;
            }
            
        $param = 'app=transaction&action=booking_v2&org='.$org.'&des='.$dest.'&round_trip=0&dep_route_code='.$dep_route_code.'&dep_date='.$dep_date.'&subclass_dep='.$subclass_dep.'&caller='.$caller.'&contact_1='.$contact_1.'&num_pax_adult='.$num_pax_adult.$adult_detail;
        //echo htmlspecialchars($param).'<br>' ;
            
            $booking = self::makeRodaRequest($param);
            
            return $booking;
}   
   public function smsresponse($transid)
    {
        $smsusername = "bluezeal";
        $smspassword = "bluezeal";
        $responseurl = "http://api2.planetgroupbd.com/api/dlrpull?user=$smsusername&password=$smspassword&messageid=$transid";
        $ressms      = get_url_contents($responseurl);
        return $ressms;
    }
    public static function get_url_contents($url)
    {
            $crl     = curl_init();
            $timeout = 25;
            curl_setopt($crl, CURLOPT_URL, $url);
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
            $ret = curl_exec($crl);
            curl_close($crl);
            return $ret;
    }
    public static function sendSMS($mobile,$message,$from){
             
            $smsusername = "bluezeal";
            $smspassword = "bluezeal";
            $url         = "http://api2.planetgroupbd.com/api/sendsms/plain?user=" . $smsusername . "&password=" . $smspassword . "&sender=" . $from . "&SMSText=" . urlencode($message) . "&GSM=" . $mobile;
            
            $credit      = "http://api2.planetgroupbd.com/api/command?username=$smsusername&password=$smspassword&cmd=Credits";
          
            $credittxt   = number_format(Self::get_url_contents($credit), 0);
            if (!$credittxt > 0) {
               return "nocredit";  
            }
            $sendsms = Self::get_url_contents($url);
            return $sendsms;
        }    
    

}
