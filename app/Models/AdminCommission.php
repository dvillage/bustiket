<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AdminCommission extends Model {
    protected $fillable = [
        'user_id', 'sp_id','booking_id','com_amount','handle_fee','comm_type','h_type','pay_status','booked_by'
    ];
    protected $table    = 'admin_commission';
    protected $hidden   = [];
    public $timestamps  = true;
    
    public function sp(){
        return $this->hasOne('\App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    
    public function booking(){
        return $this->hasOne('\App\Models\Bookings','booking_id','booking_id');
    }
    
    public function sp_comm(){
        return $this->hasOne('\App\Models\SpCommission','booking_id','booking_id');
    }
    
    public function ss_comm(){
        return $this->hasMany('\App\Models\SsCommission','booking_id','booking_id');
    }
    
    public static function admin_commission($SP_id,$ticket_id,$fare,$tot_seat,$booked_by,$agnt_id = '',$hF = 0){
            
            $ex= self::where('booking_id',$ticket_id)->where('user_id',$agnt_id)->first();
            if($ex){
                return;
            }
            
            $sp = Serviceprovider\ServiceProvider::where('id',$SP_id)->first()->toArray();

            $commval=$sp['comm_fm_sp'];
            $commtype=$sp['comm_fm_sp_type'];
            $handval=$sp['handling_fee'];
            $handtype=$sp['handling_fee_type'];
            $totam=$fare * $tot_seat;
            if($commtype == 'P'){
               $comval=(($totam*$commval)/100);
           }else{
               $comval=$commval*$tot_seat;
           }
           if($hF == 1){
                if($handtype == 'P'){
                    $hval=  ceil(($totam*$handval)/100);
                }else{
                    $hval=$handval*$tot_seat;
                }
            }else{
                $hval = 0;
            }
            
            $com = new self;
            $com->user_id = $agnt_id;
            $com->sp_id = $SP_id;
            $com->booking_id = $ticket_id;
            $com->com_amount = $comval;
            $com->handle_fee = $hval;
            $com->comm_type = $commtype;
            $com->h_type = $handtype;
            $com->pay_status = 0;
            $com->booked_by = $booked_by;
            $com->save();
                          	
    }
    
    public static function update_commission($booking_id,$total_cancelled,$total_seats){
        $com = self::where('booking_id',$booking_id)->first();
        $amt = $com->com_amount;
        $ded = $amt/$total_seats;
        $com->com_amount = $com->com_amount - ($ded*$total_cancelled);
        $com->save();
    }

    public static function filter_commlist($param){

//        dd($param);

        $count = self::orderBy('created_at','DESC')->with('booking')->whereHas('booking',function($q) use($param) {
                    $q->where('status','=',1);
            });
//        dd($count);
        if(isset($param['fromdate']) && $param['fromdate'] != '' && isset($param['todate']) && $param['todate'] != ''){
            
            $start_date = date('Y-m-d', strtotime($param['fromdate']));
            $end_date = date('Y-m-d', strtotime($param['todate']));
            $count = $count->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                    $q->whereBetween('journey_date', [$start_date, $end_date]);
            });
            
        }
        else{
            if(isset($param['fromdate']) && $param['fromdate'] != ''){
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date = date('Y-m-d');
                $count = $count->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                        $q->whereBetween('journey_date', [$start_date, $end_date]);
                });
            }
            if(isset($param['todate']) && $param['todate'] != ''){
                $start_date = date('Y-m-d');
                $end_date = date('Y-m-d', strtotime($param['todate']));
                $count = $count->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                    $q->whereBetween('journey_date', [$start_date, $end_date]);
                });
            }
        }
        
        if(isset($param['busop']) && $param['busop'] != ''){
            $count = $count->where('sp_id',$param['busop']);
        }
        if(isset($param['seller']) && $param['seller'] != ''){
            $count = $count->where('user_id',$param['seller']);
        }
        if(isset($param['month']) && $param['month'] != ''){
            $count = $count->with('booking')->whereHas('booking',function($q) use($param) {
                    $q->whereMonth('journey_date','=',$param['month']);
            });
        }
        if(isset($param['utype']) && $param['utype'] != ''){
            $count = $count->where('booked_by',$param['utype']);
            
        }
        if(isset($param['any']) && $param['any'] != ''){
//            $count = $count->where('to_city',$param['to']);
        }
        
        $count = $count->count();
        $len = 0;
        
        if(!isset($param['download'])){
        
            $page=$param['crnt'];
            $len=$param['len'];
            $op=$param['opr'];
            $total_page=ceil($count/$len);
            $flag=1;

            $start;

            if($op!=''){
                if($op=='first'){
                    $crnt_page=1;
                    $start=($crnt_page-1)*$len;
                }

                elseif($op=='prev'){
                    $crnt_page=$page-1;
                    if($crnt_page<=0){
                        $crnt_page=1;
                    }
                    $start=($crnt_page-1)*$len;
                }

                elseif($op=='next'){
                    $crnt_page=$page+1;
                    if($crnt_page>=$total_page){
                        $crnt_page=$total_page;
                    }
                    $start=($crnt_page-1)*$len;
                }

                else{
                    $crnt_page=$total_page;
                    $start=($crnt_page-1)*$len;
                }
            }

            else{
                if($page>$total_page){
                    $flag=0;
                    $crnt_page=$page-1;
                    $start=($crnt_page-1)*$len;
                }
                else{
                    $crnt_page=$page;
                    $start=($crnt_page-1)*$len;
                }
            }
        }
        $commlist = self::orderBy('created_at','DESC')->with('booking')->whereHas('booking',function($q) use($param) {
                    $q->where('status','=',1);
            });
        
        if(isset($param['fromdate']) && $param['fromdate'] != '' && isset($param['todate']) && $param['todate'] != ''){
                $crnt_page = 1;
                $start = ($crnt_page - 1) * $len;
                
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date = date('Y-m-d', strtotime($param['todate']));
                $commlist = $commlist->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                    $q->whereBetween('journey_date', [$start_date, $end_date]);
                });
        }
        else{
            if(isset($param['fromdate']) && $param['fromdate'] != ''){
                $crnt_page = 1;
                $start = ($crnt_page - 1) * $len;
                $start_date = date('Y-m-d', strtotime($param['fromdate']));
                $end_date = date('Y-m-d');
                $commlist = $commlist->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                        $q->whereBetween('journey_date', [$start_date, $end_date]);
                });
            }
            if(isset($param['todate']) && $param['todate'] != ''){
                $crnt_page = 1;
                $start = ($crnt_page - 1) * $len;
                $start_date = date('Y-m-d');
                $end_date = date('Y-m-d', strtotime($param['todate']));
                $commlist = $commlist->with('booking')->whereHas('booking',function($q) use($start_date,$end_date) {
                        $q->whereBetween('journey_date', [$start_date, $end_date]);
                });
            }
        }
        
        if(isset($param['busop']) && $param['busop'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $commlist = $commlist->where('sp_id',$param['busop']);
        }
        if(isset($param['seller']) && $param['seller'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $commlist = $commlist->where('user_id',$param['seller']);
        }
        
        if(isset($param['month']) && $param['month'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $commlist = $commlist->with('booking')->whereHas('booking',function($q) use($param) {
                    $q->whereMonth('journey_date','=',$param['month']);
            });
        }
        if(isset($param['utype']) && $param['utype'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
            $commlist = $commlist->where('booked_by',$param['utype']);
            
        }
        
        if(isset($param['any']) && $param['any'] != ''){
            $crnt_page = 1;
            $start = ($crnt_page - 1) * $len;
//            $count = $count->where('to_city',$param['to']);
        }
        
        
//        if(isset($param['tid']) && $param['tid'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('booking_id',$param['tid']);
//        }
//        if(isset($param['date']) && $param['date'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->whereDate('journey_date','=',date('Y-m-d', strtotime($param['date'])));
//        }
//        if(isset($param['from']) && $param['from'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('from_city',$param['from']);
//        }
//        if(isset($param['to']) && $param['to'] != ''){
//            $crnt_page = 1;
//            $start = ($crnt_page - 1) * $len;
//            $commlist = $commlist->where('to_city',$param['to']);
//        }
        
        
        
        if(isset($param['download'])){
            $commlist = $commlist->with(['booking','sp','sp_comm.sp','ss_comm.ss'])->get()->toArray();
            return $commlist;
        }

        $commlist = $commlist->with(['booking','sp','sp_comm.sp','ss_comm.ss'])->skip($start)->take($len)->get()->toArray();
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$commlist;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function delete_commission($booking_id){
        self::where('booking_id',$booking_id)->delete();
    }
}
