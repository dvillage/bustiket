<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Locality extends Model {
    public $table = 'locality';
    protected $fillable = array('name','lid','type');
//    protected $appends = array('role');
    public $timestamps=false;
    
    public static function from_city_list($param) {
        $loc = [
            'P' => 'Province',
            'C' => 'City',
            'D' => 'District',
            'T' => 'Terminal',
        ];
        $city_list = self::where('name','LIKE','%'.$param['key'].'%')->get(['name as city_name','id','type'])->toArray();
        $localities = [];
        foreach($city_list as $c){
            $p = [];
            $p['id'] = $c['id'];
            $p['city_name'] = $c['city_name'].' ('.$loc[$c['type']] .')';
            $localities[] = $p;
        }
//        dd($localities);
        $res = \General::success_res();
//        $res['data'] = $city_list;
        $res['data'] = $localities;
        return $res;
    }
    
    public static function to_city_list($param) {
        $loc = [
            'P' => 'Province',
            'C' => 'City',
            'D' => 'District',
            'T' => 'Terminal',
        ];
        $city_list = self::where('name','LIKE','%'.$param['key'].'%')->get(['name as city_name','id','type'])->toArray();
        $localities = [];
        foreach($city_list as $c){
            $p = [];
            $p['id'] = $c['id'];
            $p['city_name'] = $c['city_name'].' ('.$loc[$c['type']] .')';
            $localities[] = $p;
        }
        $res = \General::success_res();
//        $res['data'] = $city_list;
        $res['data'] = $localities;
        return $res;
    }
}
