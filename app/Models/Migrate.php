<?php 

nameSPace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Migrate extends Model {
    
    public static function migrate_sp($old_sp){
        $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'Service provider migrattion failed';
//        $old_sp = $old_sp->toArray();
//        $sp_new = \App\Models\Serviceprovider\ServiceProvider::get()->toArray();
//        dd($old_sp,$sp_new);
        foreach($old_sp as $sp){
//            dd($sp->SP_name);
            $sp_exist = \App\Models\Serviceprovider\ServiceProvider::where('email',$sp->SP_email)->get()->toArray();
            if(count($sp_exist) == 0){
                $parent = 0;
                $type = $sp->sp_type == 0 ? 'Main' : ($sp->sp_type == 2 ? 'A' : 'B');
                if($type != 'Main'){
                    $p = \App\Models\Serviceprovider\ServiceProvider::where('email',$sp->parent_email)->first();
                    $parent = $p->id;
                }
                
                if($sp->logo_image != ''){
                    $oldPath = '/var/www/html/bustiket/busadmin/image/'.$sp->logo_image;
                    if($host == 'redesign.bustiket.com'){
                        $oldPath = '/home/busadmin/public_testing/busadmin/image/'.$sp->logo_image;
                    }else if($host == 'bustiket.com'){
                        $oldPath = '/home/busadmin/public_html/busadmin/image/'.$sp->logo_image;
                    }
                    
                    $newPath = config('constant.UPLOAD_SP_LOGO_DIR_PATH').$sp->logo_image ;
                    \File::copy($oldPath , $newPath);        
                }
                
                $ns = new \App\Models\Serviceprovider\ServiceProvider();
                $ns->first_name = $sp->SP_name;
                $ns->status = $sp->SP_status;
                $ns->type = $type;
                $ns->parent_id = $parent;
                $ns->email = $sp->SP_email;
                $ns->password = \Hash::make($sp->SP_password);
                $ns->balance = $sp->agnt_amt;
                $ns->handling_fee = $sp->comm_user;
                $ns->handling_fee_type = $sp->comm_user_type;
                $ns->comm_fm_sp = $sp->comm_sp;
                $ns->comm_fm_sp_type = $sp->comm_sp_type;
                $ns->email_verified = $sp->SP_verified;
                $ns->avatar = $sp->logo_image;
                $ns->device_type = $sp->device_type;
                $ns->device_token = $sp->device_token;
                $ns->save();
                
                $param = [
                    'vat'=>$sp->SP_vat,
                    'address'=>$sp->SP_address1,
                    'city'=>$sp->SP_city,
                    'state'=>$sp->SP_state,
                    'pincode'=>'',
                    'landline'=>$sp->SP_landNo1,
                    'mobile1'=>$sp->SP_mobile1,
                    'mobile2'=>$sp->SP_mobile2,
                    'fax'=>$sp->SP_fax,
                    'cpname'=>$sp->SP_emgFname,
                    'cpno'=>$sp->SP_emgCallno,
                    'designation'=>$sp->SP_emgDesignation,
                    'comment'=>$sp->SP_comments,
                    'paypal_id'=>$sp->paypal_id,
                ];
                
                $res=\App\Models\Serviceprovider\ServiceProviderMore::save_sp($param,$ns->id);
                $res['data'] = 'Service provider migrated successfully';
                
            }
        }
        return $res; 
    }
    
    public static function migrate_district(){
        
        $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'district migrattion failed';
        $dist_id = Locality\District::where('name','Jakarta')->get()->toArray();
        if(count($dist_id) == 0){
            $ds = new Locality\District();
            $ds->name = 'Jakarta';
            $ds->save();
            
            $res['data'] = 'District migrated successfully';
        }
        
        $allTer = Locality\District::get();
        foreach($allTer as $at){
            $terEx = Locality::where('name',$at->name)->where('type','D')->get()->toArray();
            if(count($terEx) > 0){
                continue;
            }
            $nt = new Locality();
            $nt->name = trim($at->name);
            $nt->lid = $at->id;
            $nt->type = 'D';
            $nt->save();
        }
        
        return $res;
    }
    public static function migrate_city($old_city){
        $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'City migrattion failed';
        
        foreach($old_city as $city){
            $city_exist = Locality\Citi::where('name',$city->city_name)->get()->toArray();
            if(count($city_exist) == 0){
                $nc = new Locality\Citi();
                $nc->name = trim($city->city_name);
                $nc->province_id = null;
                $nc->save();
            }
            $res['data'] = 'City migrated successfully';
        }
        $allTer = Locality\Citi::get();
            foreach($allTer as $at){
                $terEx = Locality::where('name',$at->name)->where('type','C')->get()->toArray();
                if(count($terEx) > 0){
                    continue;
                }
                $nt = new Locality();
                $nt->name = trim($at->name);
                $nt->lid = $at->id;
                $nt->type = 'C';
                $nt->save();
            }
        return $res;
    }
    public static function migrate_terminal($old_terminal){
        $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'Terminal migrattion failed';
        $i = 0;
//        dd(count($old_terminal));
        foreach($old_terminal as $ter){
//            dd($ter);
            $i++;
//            if($i<1200){
//                continue;
//            }
            if($ter->city_name == ''){
                continue;
            }
            $city = Locality\Citi::where('name',$ter->city_name)->first();
            $ter_exist = Locality\Terminal::where('name',$ter->board_name)->where('city_id',$city->id)->get()->toArray();
            if(count($ter_exist) == 0){
                $nt = new Locality\Terminal();
                $nt->name = trim($ter->board_name);
                $nt->city_id = $city->id;
                $nt->district_id = null;
                $nt->province_id = null;
                $nt->save();
                
            }
            
        }
        $allTer = Locality\Terminal::get();
        foreach($allTer as $at){
            $terEx = Locality::where('name',$at->name)->where('type','T')->get()->toArray();
            if(count($terEx) > 0){
                continue;
            }
            $nt = new Locality();
            $nt->name = trim($at->name);
            $nt->lid = $at->id;
            $nt->type = 'T';
            $nt->save();
        }
        $res['data'] = 'Terminal migrated successfully';
        return $res;
    }
    public static function migrate_amenity($old_amenity){
         $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'Amenity migrattion failed';
        foreach($old_amenity as $am){
            $am_exist = Admin\Amenities::where('name',$am->lux_name)->get()->toArray();
            if(count($am_exist) == 0){
                $na = new Admin\Amenities();
                $na->name = $am->lux_name;
                $na->save();
            }
            $res['data'] = 'Amenity migrated successfully';
        }
        return $res;
    }
    
    public static function migrate_promo($old_promo){
         $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'Promo Code migration failed';
        foreach($old_promo as $promo){
            $ncc_exist = CouponCode::where('code',$promo->promo_title)->get()->toArray();
            if(count($ncc_exist) == 0){
                $ncc = new CouponCode();
                $ncc->code              = $promo->promo_title;
                $ncc->type              = $promo->promo_atype == 1 ? 'F' : 'P';
                $ncc->value             = $promo->promo_value;
                $ncc->start_date        = date('Y-m-d H:i:s', strtotime($promo->promo_from));
                $ncc->end_date          = date('Y-m-d H:i:s', strtotime($promo->promo_to));
                $ncc->status            = $promo->promo_status;
                $ncc->bus_id            = null;
                $ncc->coupon_type       = $promo->promo_type;
                $ncc->from_time         = date('H:i:s', strtotime($promo->promo_ftime));
                $ncc->to_time           = date('H:i:s', strtotime($promo->promo_ttime));
                $ncc->min_amount        = 0;
                $ncc->max_amount        = 9999000;
                $ncc->max_per_user_usage= 100;
                $ncc->save();
            }
            $res['data'] = 'Promo Code migrated successfully';
        }
        
        return $res;
    }
    public static function migrate_coupon($old_coupon){
        $host = request()->getHttpHost();
        $res=\General::success_res();
        $res['data'] = 'Coupon Code migration failed';
        foreach($old_coupon as $cc){
            $ncc_exist = CouponCode::where('code',$cc->coup_unique)->get()->toArray();
            if(count($ncc_exist) == 0){
                $bus = Serviceprovider\Buses::where('old_bus_id',$cc->coup_bus)->first();
                $ncc = new CouponCode();
                $ncc->code              = $cc->coup_unique;
                $ncc->type              = 'P';
                $ncc->value             = $cc->coup_perc;
                $ncc->start_date        = date('Y-m-d H:i:s', strtotime($cc->coup_date));
                $ncc->end_date          = date('Y-m-d H:i:s', strtotime($cc->coup_date1));
                $ncc->status            = $cc->coup_status == 0 || $cc->coup_status == 2 ? 1 : 0;
                $ncc->bus_id            = ($bus != null) ? $bus->id : null;
                $ncc->coupon_type       = 1;
                $ncc->min_amount        = 0;
                $ncc->max_amount        = 9999000;
                $ncc->max_per_user_usage= 100;
                $ncc->save();
            }
            $res['data'] = 'Coupon Code migrated successfully';
        }
        
        return $res;
    }
    
    public static function migrate_bus($old_bus){
        $res=\General::success_res();
//        echo config('database.default');exit;
        $res['data'] = 'Bus migrattion failed';
        $allBus = [];
        foreach($old_bus as $bus){
            $from_city = explode(',', $bus->Bus_fromcity);
            $to_city = explode(',', $bus->Bus_tocity);
            $board = unserialize($bus->Bus_boarding_time);
            $drop = unserialize($bus->Bus_departure_time);
            $count = 1;
//            $fare_ar = explode(',', $bus->Bus_fare);
            $fare_ar = @unserialize($bus->Bus_fare);
//            var_dump($fare_ar);
            if(!$fare_ar){
                $fare_ar = explode(',', $bus->Bus_fare);
            }
            $route = [];
            $cnt = 0;
            $fcn = 0;
            $tcn = 0;
            $tseats = $bus->tseats;
            if($bus->Bus_structure != 0){
                $tseats = count(json_decode($bus->position));
            }
            foreach ($from_city as $key => $val) {
                 $cnt++;                      
                $from_c = $from_city[$key];
                $temp = $to_city[$key] ;
                
                $qryfrom = \DB::table('cities')->where('id',$from_c)->first();
                if(!$qryfrom){
                    $fcn++;
                    continue;
                }
                $frm_nm = $qryfrom->city_name;
                $qryto = \DB::table('cities')->where('id',$temp)->first();
                if(!$qryto){
                    $tcn++;
                    continue;
                }
                $to_nm = $qryto->city_name;
                
                $bp = (explode(',',$board[$val]));
                $dp = (explode(',',$drop[$temp]));
                $boarding_stop = '';$boarding_time = '';
//                $fare = $fare_ar[0];
//                dd($board);
                
                   foreach ($bp as $keyb1 => $val1){
                       if($val == $temp){
                           continue;
                       }
                      $bp1 = (explode('--',$val1));

                      $boarding_stop = isset($bp1[0]) ? $bp1[0] : '' ;
                      
                      $boarding_time = isset($bp1[1]) ? $bp1[1] : '' ;
                      $btime = date('H:i',strtotime($boarding_time));
                      $bt = explode(':', $btime);
                      $bmin = ($bt[0] * 60)+$bt[1];
                      
                      $dropping_stop = '';$dropping_time = '';
                        foreach ($dp as $key1 => $val1){
                            
                            $dp1 = (explode('--',$val1));

                           $dropping_stop = isset($dp1[0]) ? $dp1[0] : '' ;
                           $dropping_time = isset($dp1[1])?$dp1[1]:'' ;
                           $dtime = date('H:i',strtotime($dropping_time));
                           
                           $fare = isset($fare_ar[$from_c.','.$temp]) ? $fare_ar[$from_c.','.$temp] : (isset($fare_ar[$key])?$fare_ar[$key]: min($fare_ar) );
                           $nfare = (int)$fare;
                           if($nfare == 0){
                               continue;
                           }
                            $route[] = [
                                    'route_name'=> $bus->Bus_name,
                                    'from_city'=>$frm_nm,
                                    'to_city'=>$to_nm,
                                    'board_point'=>$boarding_stop,
//                                    'board_time'=>$bmin,
                                    'board_time'=>$btime,
                                    'drop_point'=>$dropping_stop,
                                    'drop_time'=>$dtime,
                                    'from_city_id'=>$from_c,
                                    'to_city_id'=>$temp,
                                    'total_seat'=>$tseats,
                                    'fare'=>$nfare,
                                ];
                        }
                   }



                   

//                   dd($route);
            }
             
            $amn = explode(',',$bus->luxury_item);
            $am = '';
            if(count($amn) > 0){
                foreach($amn as $a){
                    if($a != ''){
                       $ab =  \DB::table('bus_luxitem')->where('lux_id',$a)->first();
                       if($ab){
                           if($am !=''){
                                $am .= ',';
                            }
                            $am .= $ab->lux_name;
                       }
                       
                    }
                }
            }
            
            $allBus[$bus->Bus_id]['routes'] = $route;
            $allBus[$bus->Bus_id]['fare_array'] = $fare_ar;
            $allBus[$bus->Bus_id]['bus_name'] = $bus->par_name;
            $allBus[$bus->Bus_id]['sp_email'] = $bus->sp_email;
            $allBus[$bus->Bus_id]['bus_status'] = $bus->Bus_status;
            $allBus[$bus->Bus_id]['total_seat'] = $tseats;
            $allBus[$bus->Bus_id]['bus_type'] = strtolower($bus->typeName);
            $allBus[$bus->Bus_id]['amenities'] = $am;
            $allBus[$bus->Bus_id]['layout'] = 0;
            $allBus[$bus->Bus_id]['active_days'] = $bus->active_days;
            $allBus[$bus->Bus_id]['tnc'] = $bus->conditions;
            $allBus[$bus->Bus_id]['cnt'] = $cnt;
            $allBus[$bus->Bus_id]['from city not found'] = $fcn;
            $allBus[$bus->Bus_id]['to city not found'] = $tcn;
            $allBus[$bus->Bus_id]['bus_id'] = $bus->Bus_id;
            
            
//            \Config::set('database.default', 'old');
//            echo '<pre>';
//            print_r($route);
//            echo '<pre>';
        }
        
        \Config::set('database.default', 'mysql');
        
        $newBuses = [];
        foreach($allBus as $k=>$ab){
            
            $sp = Serviceprovider\ServiceProvider::where('email',$ab['sp_email'])->first();
            $sp_id = $sp->id;
            
            $amenities = explode(',',$ab['amenities']);
            $am = [];
            foreach($amenities as $a){
                if($a){
                    $amn = Admin\Amenities::where('name',$a)->first()->toArray();
                    $am[] = $amn;
                }
                
            }
            $route = [];
            foreach($ab['routes'] as $rk=>$rt){
                $fromC = Locality\Citi::where('name',$rt['from_city'])->first();
                if(!$fromC){
                    continue;
                }
                $from_city = $fromC->id;
                $toC = Locality\Citi::where('name',$rt['to_city'])->first();
                if(!$toC){
                    continue;
                }
                $to_city = $toC->id;
                $fromT = Locality\Terminal::where('name',$rt['board_point'])->first();
                if(!$fromT){
                    continue;
                }
                $from_ter = $fromT->id;
                $toT = Locality\Terminal::where('name',$rt['drop_point'])->first();
                if(!$toT){
                    continue;
                }
                $to_ter = $toT->id;
                
                $route[$rk]['from_city_id'] = $from_city;
                $route[$rk]['to_city_id'] = $to_city;
                $route[$rk]['from_terminal_id'] = $from_ter;
                $route[$rk]['to_terminal_id'] = $to_ter;
                $route[$rk]['from_city_name'] = $rt['from_city'];
                $route[$rk]['to_city_name'] = $rt['to_city'];
                $route[$rk]['from_terminal_name'] = $rt['board_point'];
                $route[$rk]['to_terminal_name'] = $rt['drop_point'];
                $route[$rk]['board_time'] = $rt['board_time'];
                $route[$rk]['drop_time'] = $rt['drop_time'];
                $route[$rk]['fare'] = $rt['fare'];
                $route[$rk]['total_seat'] = $rt['total_seat'];
                $route[$rk]['route_name'] = $rt['route_name'];
            }
            $newBuses[$k]['routes'] = $route;
            $newBuses[$k]['bus_name'] = $ab['bus_name'];
            $newBuses[$k]['sp_email'] = $ab['sp_email'];
            $newBuses[$k]['sp_id'] = $sp_id;
            $newBuses[$k]['bus_status'] = $ab['bus_status'];
            $newBuses[$k]['total_seat'] = $ab['total_seat'];
            $newBuses[$k]['bus_type'] = $ab['bus_type'];
            $newBuses[$k]['amenities'] = $am;
            $newBuses[$k]['layout'] = 0;
            $newBuses[$k]['active_days'] = $ab['active_days'];
            $newBuses[$k]['tnc'] = $ab['tnc'];
            $newBuses[$k]['bus_id'] = $ab['bus_id'];
            
        }
        $ac = 0;
        foreach($newBuses as $n){
            $ex = 0;
            
            $br= Serviceprovider\Buses::where('old_bus_id',$n['bus_id'])->first();
            if($br){
                $ex++;
            }
            
            if($ex){
                $ac++;
//                dd($br);
                continue;
            }
            
            $bus = new Serviceprovider\Buses();
            $bus->status = $n['bus_status'];
            $bus->old_bus_id = $n['bus_id'];
            $bus->name = $n['bus_name'] != '' ? $n['bus_name']: 'Bus' ;
            $bus->sp_id = $n['sp_id'];
            $bus->type = $n['bus_type'];
            $bus->layout = $n['layout'];
            $bus->layout_id = null;
            $bus->seat_capacity = $n['total_seat'];
            $bus->tnc = $n['tnc'];
            $bus->active_days = $n['active_days'];
            $bus->save();
            
            foreach($n['amenities'] as $am){
                $na = new Admin\BusAmenities();
                $na->bus_id = $bus->id;
                $na->aname = $am['name'];
                $na->aimg = $am['img'];
                $na->save();
            }
            
            foreach($n['routes'] as $r){
                $rn = new Admin\BusRoutes();
                $rn->name = $r['route_name'];
                $rn->route = $r['from_city_name'].'-'.$r['from_terminal_name'].'-'.$r['to_terminal_name'].'-'.$r['to_city_name'];
                $rn->bus_id = $bus->id;
                $rn->from_city_id = $r['from_city_id'];
                $rn->to_city_id = $r['to_city_id'];
                $rn->from_terminal_id = $r['from_terminal_id'];
                $rn->to_terminal_id = $r['to_terminal_id'];
                $rn->boarding_time = $r['board_time'];
                $rn->droping_time = $r['drop_time'];
                $rn->save();
                
                $pr = new Admin\BusPrice();
                $pr->bus_id = $bus->id;
                $pr->route_id = $rn->id;
                $pr->sorting = 1;
                $pr->seat_count = $r['total_seat'];
                $pr->price = $r['fare'];
                $pr->save();
            }
            
        }
        echo 'duplicate bus found : '.$ac;
        $res=\General::success_res();
        $res['data'] = 'Buses migrated successfully';
        
        return $res;
//        echo 'total buses : '.count($newBuses);
//        echo '<pre>';
//        print_r($newBuses);
//        echo '<pre>';
    }
    
    public static function migrate_locality(){
        $province = Locality\Province::get()->toArray();
        foreach($province as $p){
            $l = Locality::where(['name'=>$p['name'],'type'=>'P'])->first();
            if(is_null($l)){
                $nl = new Locality();
                $nl->name = $p['name'];
                $nl->lid = $p['id'];
                $nl->type = 'P';
                $nl->save();
            }
        }
        
        $district = Locality\District::get()->toArray();
        foreach($district as $p){
            $l = Locality::where(['name'=>$p['name'],'type'=>'D'])->first();
            if(is_null($l)){
                $nl = new Locality();
                $nl->name = $p['name'];
                $nl->lid = $p['id'];
                $nl->type = 'D';
                $nl->save();
            }
        }
        
        $city = Locality\Citi::get()->toArray();
        foreach($city as $p){
            $l = Locality::where(['name'=>$p['name'],'type'=>'C'])->first();
            if(is_null($l)){
                $nl = new Locality();
                $nl->name = $p['name'];
                $nl->lid = $p['id'];
                $nl->type = 'C';
                $nl->save();
            }
        }
        
        $terminal = Locality\Terminal::get()->toArray();
        foreach($terminal as $p){
            $l = Locality::where(['name'=>$p['name'],'type'=>'T'])->first();
            if(is_null($l)){
                $nl = new Locality();
                $nl->name = $p['name'];
                $nl->lid = $p['id'];
                $nl->type = 'T';
                $nl->save();
            }
        }
        $res=\General::success_res();
        $res['data'] = 'Locality migrated successfully';
        return $res;
    }
    
    public static function migrate_amenity_img(){
//        $amenities = Admin\Amenities::get()->toArray();
        $amenities = Admin\BusAmenities::get()->toArray();
        foreach($amenities as $am){
            $bus_amenities = Admin\Amenities::where('id',$am['a_id'])->update(['img'=>$am['aimg']]);
        }
        $res=\General::success_res();
        $res['data'] = 'Amenities Images migrated successfully';
        return $res;
    }
    
    public static function migrate_user($olduser){
        \Config::set('database.default', 'mysql');
        $newarray=array();
        foreach ($olduser as $ou){
            $u = Users::where("email",$ou->user_email)->first();
            if(!is_null($u))
            {
                continue;
            }
            $name=$ou->user_firstname." ".$ou->user_lastname;
            $address=$ou->user_address1_1." ".$ou->user_address1_2;
            $date=date('Y-m-d');
            if(empty($ou->user_password)){
                $password="";
            }else{
                 $password=\Hash::make($ou->user_password);
            }
             $oldarray=['name'=>$name,'status'=>$ou->user_status,'email'=>$ou->user_email,'mobile'=>$ou->user_mobileno,'landline_no'=>$ou->user_landno,'gender'=>$ou->user_gender,'maritalstatus'=>$ou->user_maritalstatus,'occupation'=>$ou->user_occupation,'address'=>$address,'city'=>$ou->user_address1_city,'state'=>$ou->user_address1_state,'country'=>$ou->user_address1_country,'zipcode'=>$ou->user_address1_pin,'dob'=>$ou->user_dob,'avatar'=>$ou->avatar,'last_ip'=>$ou->ip_addr,'login_date'=>$date,'password'=>$password,'g_status'=>$ou->g_status,'fb_status'=>$ou->fb_status,'device'=>$ou->device_type,'device_token'=>$ou->device_token,'created_at'=>$date,'updated_at'=>$date];
             array_push($newarray,$oldarray);
        }
        $affecRow=\App\Models\Users::insert($newarray);
        if($affecRow>0){
            return \General::success_res("User migrated successfully");
        }else{
            return \General::error_res("Error Ocurred");
        }
    }
    
    public static function migrate_ssinfo($oldSS,$newEmail){
        $res=\General::success_res();
        $res['data'] = 'Seat seller migrattion failed';
        $newarray=array();
        \Config::set('database.default','mysql');                 
        foreach ($oldSS as $ou){
                $u =DB::table('ss')->where("email",$ou->agent_email)->first();
                 if(!is_null($u))
                 {
                     continue;
                 }
                 if(empty($ou->agent_password)){
                        $password="";
                 }else{
                        $password=\Hash::make($ou->agent_password);
                 }
                 
                $parent = 0;
                $type = $ou->agent_type == 0 ? 'Main' : ($ou->agent_type == 4 ? 'A' : 'B');
                if($type != 'Main'){
                    $p = Admin\SeatSeller::where('email',$ou->parent_email)->first();
                    if(!$p){
                        $p = Admin\SeatSeller::where('email','no_parent_ss@bustiket.com')->first();
                        if(!$p){
                            $p = new Admin\SeatSeller();
                            $p->status = 0;
                            $p->type = 'Main';
                            $p->parent_id = 0;
                            $p->name = 'No parent';
                            $p->email = 'no_parent_ss@bustiket.com';
                            $p->save();
                        }
                    }
                    $parent = $p->id;
                }
                 
                $date=date('Y-m-d H:i:s',  strtotime($ou->agent_joindate));

                $ss = new Admin\SeatSeller();
                $ss->status = $ou->agent_status;
                $ss->type = $type;
                $ss->parent_id = $parent;
                $ss->name = $ou->agent_name;
                $ss->email = $ou->agent_email;
                $ss->mobile = $ou->agent_mobile;
                $ss->address = $ou->agent_address;;
                $ss->city = $ou->agent_city;;
                $ss->password = $password;
                $ss->balance = $ou->agnt_amt;;
                $ss->comm = $ou->agent_commission;;
                $ss->comm_type = $ou->agent_commission_type;;
                $ss->agent_comments = $ou->agent_comments;;
                $ss->handling_fee = $ou->comm_user;;
                $ss->handling_fee_type = $ou->comm_user_type;;
                $ss->tax = $ou->tax;;
                $ss->paypal_email = $ou->paypal_id;;
                $ss->device_type = $ou->device_type;;
                $ss->device_token = $ou->device_token;;
                $ss->created_at = $date;
                $ss->save();
                
                $res['data'] = 'Seat seller migrated successfully.';
            }
         return $res;       
        }   
    
        public static function update_route($all_term){
            $res=\General::success_res();
            $res['data'] = 'Route district,city and province migration failed or already migrated';
            $fromTerm=array();
            $toTerm=array();
            $cnt = 0;
//            dd($all_term);
            foreach($all_term as $term){
//                dd($term);
                $rt = Admin\BusRoutes::where('from_terminal_id',$term['id'])->where(function($q){
                            $q->whereNull('from_district_id')->orWhereNull('from_province');
                        })
                        ->update(['from_district_id'=>$term['district_id'],'from_city_id'=>$term['city_id'],'from_province'=>$term['province_id']]);
                if($rt){
                    $cnt++;
                    $fromTerm[] = $rt;
                }
                $rtt = Admin\BusRoutes::where('to_terminal_id',$term['id'])->where(function($q){
                            $q->whereNull('to_district_id')->orWhereNull('to_province');
                        })
                        ->update(['to_district_id'=>$term['district_id'],'to_city_id'=>$term['city_id'],'to_province'=>$term['province_id']]);
                if($rtt){
                    $cnt++;
                    $toTerm[] = $rtt;
                }
            }
            
            if($cnt){
                $res['data'] = 'Route district,city and province migratted successfully.';
            }
//            dd($cnt,$fromTerm,$toTerm);
            return $res;
//            dd($fromTerm,$toTerm);
        }
        
        public static function all_city($all_term){
            $all_term = $all_term->toArray();
            $csvArr = [];
            $csvArr[] = ['Province','City','District','Terminal'];
            foreach($all_term as $t){
                $csvArr[] = [
                    $t['loc_province']['name'],
                    $t['loc_citi']['name'],
                    $t['loc_district']['name'],
                    $t['name'],
                ];
            }
            
//            $csvString = General::array_2_csv($csvArr);
//            dd($csvArr);
            return $csvArr;
        }
        public static function roda_cities(){
            $rodaTerm = ApiTerminals::get()->toArray();
            if(count($rodaTerm) > 0){
                $csvArr = [];
                $csvArr[] = ['City','Terminal','Code'];
                foreach($rodaTerm as $c){
                    $point = json_decode($c['pick_up_points'],true);
                    $city_name = $point[0][1];
                    $city_code = $point[0][0];
//                    $csvArr[] = [
//                            $city_name,
//                            '',
//                            $city_code,
//                        ];
//                    $csvArr[] = ['','',''];
                    for($i=1;$i<count($point);$i++){
                        $csvArr[] = [
                            $city_name,
                            $point[$i][1],
                            $point[$i][0],
                        ];
                    }
//                    $csvArr[] = ['','',''];
                }
    //            dd($csvArr);
                return $csvArr;
            }else{
                exit('no cities found.');
            }
        }
        
        public static function roda_terminals($path){
            $res=\General::success_res();
            $res['data'] = 'something is wrong.roda code not migrated.';
            $terminals = [];
            $file = fopen($path,"r");
            while(! feof($file))
            {
                $terminals[] = (fgetcsv($file));
            }
            fclose($file);
            
            $rodaTerminal = [];
            $i = 0;
            foreach($terminals as $ter){
                if($i > 0){
                        if($ter[6] != '' && $ter[3] != ''){
                        $rodaTerminal[] = $ter;
                    }
                }
                $i++;
            }
            
            foreach ($rodaTerminal as $r){
                $terminal = Locality\Terminal::where('name',$r[3])->whereHas('locCiti',function($q)use($r){
                    $q->where('name',$r[1]);
                })->first();
                if($terminal){
                    $terminal->roda_code = $r[6];
                    $terminal->save();
                    $res['data'] = 'roda code migrated successfully.';
                }else{
                    
                }
            }
            
            return $res;
        }
        public static function lorena_cities(){
//            $rodaTerm = ApiTerminals::get()->toArray();
            $key = 'lorena_terminal_list';
            $rodaTerm = [];
            if(\Cache::has($key)){
                $rodaTerm = \Cache::get($key);
            }
            if(count($rodaTerm) > 0){
                $csvArr = [];
                $csvArr[] = ['City','Terminal','Code'];
                foreach($rodaTerm as $c){
                    $point = $c['points'];
                    $city_name = $point[0][1];
                    $city_code = $point[0][0];
//                    $csvArr[] = [
//                            $city_name,
//                            '',
//                            $city_code,
//                        ];
//                    $csvArr[] = ['','',''];
                    for($i=1;$i<count($point);$i++){
                        $csvArr[] = [
                            $city_name,
                            $point[$i][1],
                            $point[$i][0],
                        ];
                    }
//                    $csvArr[] = ['','',''];
                }
    //            dd($csvArr);
                return $csvArr;
            }else{
                exit('no cities found.');
            }
        }
}
