<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ApiBusList extends Model {
    protected $fillable = [
        'from_city', 'to_city', 'travelling_date','bus_list'
    ];
    protected $table = 'api_bus_list';
    protected $hidden = [];
    public $timestamps=true;
    
    
    
    

}
