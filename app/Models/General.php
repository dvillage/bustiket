<?php

namespace App\Models;
use DB;

class General {

    public static function get_first_letters($string) {
        $element = count(array_filter(explode(' ', $string)));
        if ($element > 1) {
            return substr(strtoupper(preg_replace('/(\B.|\s+)/', '', $string)), 0, 2);
        } else {
            $return = preg_replace('/(\B.|\s+)/', '', $string);
//            $return = $return . substr($string, -1);
            $return = $return . substr(preg_replace('/\s/ ', '', $string), -1);
            return strtoupper($return);
        }
    }

    public static function ticketNo($spname, $booked_by) {
        return $ticket = date('m') . self::get_first_letters($spname) . date('d') . strtoupper($booked_by) . date('H') . self::randomString();
    }

    public static function uniqueNo($spname, $booked_by) {
        return $ticket = date('m') . self::get_first_letters($spname) . date('d') . strtoupper(self::get_first_letters($booked_by)) . self::randomString(3, 0);
    }

    public static function randomString($length = 4, $aN = 1) {
        $str = "";
        $characters = range('A', 'Z');
        $numbers = range('0', '9');
        $maxC = count($characters) - 1;
        $maxN = count($numbers) - 1;
        for ($i = 0; $i < $length; $i++) {
            if ($aN == 1) {
                if ($i % 2 == 0) {
                    $rand = mt_rand(0, $maxC);
                    $str .= $characters[$rand];
                } else {
                    $rand = mt_rand(0, $maxN);
                    $str .= $numbers[$rand];
                }
            } else {
                $rand = mt_rand(0, $maxN);
                $str .= $numbers[$rand];
            }
        }
        return $str;
    }

    public static function getIndoMonths($date, $day = '') {
        $months = [
            'January' => 'Januari',
            'February' => 'Februari',
            'March' => 'Maret',
            'April' => 'April',
            'May' => 'Mei',
            'June' => 'Juni',
            'July' => 'Juli',
            'August' => 'Agustus',
            'September' => 'September',
            'October' => 'Oktober',
            'November' => 'November',
            'December' => 'Desember'
        ];
        $daylist = [
            'Sunday' => 'Minggu',
            'Monday' => 'Senin',
            'Tuesday' => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday' => 'Kamis',
            'Friday' => 'Jumat',
            'Saturday' => 'Sabtu',
        ];

        $dates = explode(' ', $date);
        foreach ($months as $k => $m) {
            if (in_array($k, $dates)) {
                $retDate = str_replace($k, $m, $date);
                $dout = '';
                if ($day != '') {
                    $dout = $daylist[$day] . ', ';
                }
                return $dout . $retDate;
            }
        }
    }

    public static function getInMonths($date, $type = 'M', $srt = 'S') {

        $indoDF = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        $indoDS = ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'];
        $indoMS = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'];
        $indoMF = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $dates = explode(' ', $date);
//    echo count($dates);
        if (count($dates) >= 5) {
            if ($type == 'M') {
                if ($srt == 'F') {
                    $m = array_search($dates[1], $indoMF) + 1;
                } else {
                    $m = array_search($dates[1], $indoMS) + 1;
                }
                return $dates[3] . '-' . $m . '-' . $dates[2] . ' ' . $dates[4];
            }
        }
    }

    public static function getAlphabate($k = 0) {
        $alph = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $ret = $alph[$k];

        return $ret;
    }

    public static function sendCareerMail($param) {
        $resume = request()->file('resume');
        $filepath = $resume->move(public_path() . '/assets/resumes', date('Ymd') . time() . $resume->getClientOriginalName());
        $subject = "New Request from career";
        \Mail::send('emails.user.admin_mail_job', $param, function($message)use ($subject, $filepath) {

            $message->from('noreply@bustiket.com', 'System');
            $message->to('info@bustiket.com')->subject($subject);
            $message->attach($filepath);
        });
        return \General::success_res("Thank you. We have received your request. We will contact you soon"); 
    }

    public static function sendCompanyBusMail($param) {
        $subject = "New Request for Bus Operator";
        \Mail::send('emails.user.admin_mail', $param, function($message)use ($subject) {
            $message->from('noreply@bustiket.com', 'System');
            $message->to('info@bustiket.com')->subject($subject);
        });
        return \General::success_res("Thank you. Your request has been registred. We will contact you soon.");
    }
    public static function sendPartnerMail($param) {
        $subject = "New Request for Partner";
        $data = ['data'=>$param];
        \Mail::send('emails.user.admin_mail_partner', $data, function($message)use ($subject) {
            $message->from('noreply@bustiket.com', 'System');
            $message->to('info@bustiket.com')->subject($subject);
        });
        return \General::success_res("Thank you. Your request has been registred. We will contact you soon.");
    }
    public static function sendNewSubscriberMail($param) {
        $subject = "New Request for  subscription";
        \Mail::send('emails.user.admin_mail_subscribe', $param, function($message)use ($subject) {
            $message->from('noreply@bustiket.com','System');
            $message->to('subscribe@bustiket.com')->subject($subject);
        });
        return \General::success_res("Thank you for subscribing.");
    }
    
    public static function sendContactUsMail($param) {
       // dd($param);
        $subject = "New Request form Contact Us";
        \Mail::send('emails.user.admin_mail_contact_us',$param,function($message)use ($subject) {
            $message->from('noreply@bustiket.com','System');
            $message->to('info@bustiket.com')->subject($subject);
        });
        return \General::success_res("Thank you for contact us.");
    }

    
    public static function sendPaymentConfirmationMail($param) {
        $bid=DB::table('bookings')->select('booking_id','booker_email')->where('BT_id',$param['bankData'][0]['value'])->where('total_amount',$param['bankData'][4]['value'])->get();
        if(count($bid)>0){
        $bookid=$bid[0]['booking_id'];
        $bookerEmail=$bid[0]['booker_email'];

        $adminSubject = "Busticket::Ticket Request For Booking ID".$bookid;
        $bookerSubject = "Busticket::Ticket Requested For Booking ID".$bookid;
        
        \Mail::send('emails.user.payment_confirmation_mail_admin', $param, function($message)use ($adminSubject) {
            $message->from('noreply@bustiket.com','System');
            $message->to('info@bustiket.com')->subject($adminSubject);
        });
        
        \Mail::send('emails.user.payment_confirmation_mail_booker', $param, function($message)use ($bookerSubject,$bookerEmail) {
            $message->from('noreply@bustiket.com','System');
            $message->to($bookerEmail)->subject($bookerSubject);
        });
            return \General::success_res("Your request has been successfuly sent.");
        }else{
            return \General::error_res("No match Data found");
        }
    }
    
    public static function companyBusFormValidation($param){
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        $validateArr=array();
        $flag=1;
        $cnt=0;
     
        foreach($tmpParam as $key=>$value){
            if(empty($value)){
                $validateArr[$cnt++]="$key .should not blank";
                    $flag=0;
            }
        }
        
        if(!is_numeric($tmpParam['price'])){
             $validateArr[$cnt++]="price must digit ";
             $flag=0;
        }
        
        if(!preg_match("/^\d{10,}$/",$tmpParam['phoneNo']))
        {
             $validateArr[$cnt++]="phone No should greater than or equal 10 digit ";
             $flag=0;
        }
        
        if(!preg_match("/^\d{10,}$/",$tmpParam['lphoneNo']))
        {
             $validateArr[$cnt++]="lphone No should greater than or equal 10 digit ";
             $flag=0;
        }
        
        
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    public static function careerFormValidation($param){
        
        $validateArr=array();
        $flag=1;
        $cnt=0;
        $resume = request()->file('resume');
        
        if(!$resume){
            $validateArr[$cnt++]="Files should not blank";
            $flag=0;

        }
        if(!filter_var($param['email'], FILTER_VALIDATE_EMAIL)){
            $flag=0;
            $validateArr[$cnt++]="Enter valid Email Address";
           
        }
        
        foreach($param as $key=>$value){
            if(empty($value)){
                $validateArr[$cnt++]="$key .should not blank";
                    $flag=0;
            }
        }
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    
    public static function subscribeFormValidation($param){
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        $validateArr=array();
        $flag=1;
        $cnt=0;
        
        if(!filter_var($tmpParam['semail'], FILTER_VALIDATE_EMAIL)){
            $flag=0;
            $validateArr[$cnt++]="Enter valid Email Address";   
        }
        
        foreach($tmpParam as $key=>$value){
            if(empty($value)){
                $validateArr[$cnt++]="$key .should not blank";
                    $flag=0;
            }
        }
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    
    public static function paymentCnfFormValidation($param){
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        $validateArr=array();
        $flag=1;
        $cnt=0;
        foreach($tmpParam as $p){
            foreach($p as $key=>$value){
                    if(empty($p['value'])){
                        $validateArr[$cnt++]=$p['name'] ." should not blank";
                            $flag=0;
                    }
            }
        }              
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    
    public static function contactUsValidation($param){
       
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        
        $validateArr=array();
        $flag=1;
        $cnt=0;

        if(!filter_var($tmpParam[1]['value'],FILTER_VALIDATE_EMAIL)){
            $flag=0;
            $validateArr[$cnt++]="Enter valid Email Address";
           
        }
        
        if(!preg_match("/^\d{10,}$/",$tmpParam[2]['value']))
        {
             $validateArr[$cnt++]="Contact No should greater than or equal 10 digit ";
             $flag=0;
        }
        
        foreach($tmpParam as $p){
            foreach($p as $key=>$value){
                    if(empty($p['value'])){
                        $validateArr[$cnt++]=$p['name'] ." should not blank";
                            $flag=0;
                    }
            }
        }              
                      
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    public static function checkBookingFormValidation($param){
       
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        
        $validateArr=array();
        $flag=1;
        $cnt=0;
        
        if(empty($tmpParam['ticketNumber'])){
                $flag=0;
                $validateArr[$cnt++]="ticket Number should not blank";
           
        }
        
        if($tmpParam['flags']==0 || $tmpParam['flags']==2 ){
        
             if(!filter_var($tmpParam['email'],FILTER_VALIDATE_EMAIL)){
                $flag=0;
                $validateArr[$cnt++]="Enter valid Email Address";
           
            }
        }
        
        if($tmpParam['flags']==1 ){
            if(!preg_match("/^\d{10,}$/", $tmpParam['phoneNo']))
            {
                 $validateArr[$cnt++]="Contact No should greater than or equal 10 digit ";
                 $flag=0;
            }
        }
        return ["va"=>$validateArr,"flag"=>$flag];
    }
        public static function checkBookingNewFormValidation($param){
        $tmpParam=$param;
        $validateArr=array();
        $flag=1;
        $cnt=0;
        
        if(empty($tmpParam['ticketNumber'])){
                $flag=0;
                $validateArr[$cnt++]="ticket Number should not blank";
           
        }
        
        if($tmpParam['flags']==0 || $tmpParam['flags']==2 ){
        
             if(!filter_var($tmpParam['email'],FILTER_VALIDATE_EMAIL)){
                $flag=0;
                $validateArr[$cnt++]="Enter valid Email Address";
           
            }
        }
        
        if($tmpParam['flags']==1 ){
            if(!preg_match("/^\d{10,}$/", $tmpParam['phoneNo']))
            {
                 $validateArr[$cnt++]="Contact No should greater than or equal 10 digit ";
                 $flag=0;
            }
        }
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    
    public static function cancelTicketFormValidation($param){
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        
        $validateArr=array();
        $flag=1;
        $cnt=0;
        
        if(empty($tmpParam['ticketNumber'])){
                $flag=0;
                $validateArr[$cnt++]="Ticket Number should not blank";
           
        }
        
        if(!filter_var($tmpParam['email'],FILTER_VALIDATE_EMAIL)){
           $flag=0;
           $validateArr[$cnt++]="Enter valid Email Address";

       }
        
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    public static function userProfileFormValidation($param){
        $keys=  array_keys($param);
        $tmpParam=$param[$keys[1]];
        $validateArr=array();
        $flag=1;
        $cnt=0;

        if($tmpParam[0]['value']=='')
        {
             $validateArr[$cnt++]="Name should not blank ";
             $flag=0;
        }
        
        if(!preg_match("/^\+\d{10,}$/", $tmpParam[1]['value']))
        {
             $validateArr[$cnt++]="Contact No should greater than or equal 10 digit ";
             $flag=0;
        }
        
        if(!filter_var($tmpParam[2]['value'],FILTER_VALIDATE_EMAIL)){
            $flag=0;
            $validateArr[$cnt++]="Enter valid Email Address";
           
        }
        return ["va"=>$validateArr,"flag"=>$flag];
    }
    
    public static function formValidation($param){       
        $validateArr=array();
        $flag=1;
        $cnt=0;
    
        foreach($param as $key=>$value){
            if(empty($value)){
                $validateArr[$cnt++]="$key .should not blank";
                    $flag=0;
            }
        }                                            
        return ["va"=>$validateArr,"flag"=>$flag];
    }

    public static function get_month_from_number($n = 0){
        $months = [
            'January',
            'February' ,
            'March' ,
            'April' ,
            'May' ,
            'June' ,
            'July' ,
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
        
        return isset($months[$n]) ? $months[$n] : $months[0];
    }
    
    public static function  prepare_data_for_individual_chart($ad){
        $backgroundColor = [
            'rgba(31, 119, 180,0.6)',
            'rgba(174, 199, 232,0.6)',
            'rgba(255, 127, 14,0.6)',
            'rgba(255, 187, 120,0.6)',
            'rgba(44, 160, 44,0.6)',
            'rgba(152, 223, 138,0.6)',
            'rgba(214, 39, 40,0.6)',
            'rgba(255, 152, 150,0.6)',
            'rgba(148, 103, 189,0.6)',
            'rgba(197, 176, 213,0.6)',
        ];
        $borderColor = [
            'rgba(31, 119, 180,1)',
            'rgba(174, 199, 232,1)',
            'rgba(255, 127, 14,1)',
            'rgba(255, 187, 120,1',
            'rgba(44, 160, 44,1)',
            'rgba(152, 223, 138,1)',
            'rgba(214, 39, 40,1)',
            'rgba(255, 152, 150,1)',
            'rgba(148, 103, 189,1)',
            'rgba(197, 176, 213,1)',
        ];
//        dd($tdcount,$mcount,$ycount);
        $lbl = [];
        $dt = [];
        $i = 0;
        $bkcolor = [];
        $brcolor = [];
        foreach($ad as $d){
            $lbl[] = $d['name'];
            $dt[] = $d['total'];
            $bkcolor[] = $backgroundColor[$i];
            $brcolor[] = $borderColor[$i];
            $i++;
            if($i ==5){
                $i = 0;
            }
        }
        
        $data = [ 
            
                'labels'=>$lbl,
                'datasets'=>[
                    [
                        'label' => 'Individual sale',
                        'data' => $dt,
                        'backgroundColor' => $bkcolor,
                        'borderColor' => $brcolor,
                        'borderWidth' => 1,
                    ]
                ],
            
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    public static function get_total_sale($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        if(isset($param['book_by'])){
            $tdcount = \App\Models\Bookings::active()->whereIn('book_by',$param['book_by'])->whereDate('created_at','=',$today)->count();
            $mcount = \App\Models\Bookings::active()->whereIn('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->count();
            $ycount = \App\Models\Bookings::active()->whereIn('book_by',$param['book_by'])->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->count();
        }else{
            $tdcount = \App\Models\Bookings::active()->whereDate('created_at','=',$today)->count();
            $mcount = \App\Models\Bookings::active()->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->count();
            $ycount = \App\Models\Bookings::active()->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->count();
        }
        
//        dd($tdcount,$mcount,$ycount);
        $data = [
            [
                'key'=>'Today('.$tdcount.')',
                'y'=>$tdcount,
            ],
            [
                'key'=>'Month('.$mcount.')',
                'y'=>$mcount,
            ],
            [
                'key'=>'Year('.$ycount.')',
                'y'=>$ycount,
            ],
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_sale_amount($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        
        $tdcount = \App\Models\Bookings::active()->whereDate('created_at','=',$today)->sum('total_amount');
        $mcount = \App\Models\Bookings::active()->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('total_amount');
        $ycount = \App\Models\Bookings::active()->whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('total_amount');
        
        
//        dd($tdcount,$mcount,$ycount);
        $allD = [
            [
                'label'=>'Today',
                'value'=>ceil($tdcount),
            ],
            [
                'label'=>'Month',
                'value'=>ceil($mcount),
            ],
            [
                'label'=>'Year',
                'value'=>ceil($ycount),
            ],
        ];
        
        $data = [
            [
                'key'=>'Total sale in rupies',
                'values'=>$allD,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_commission_from_sp($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        
        $tdcount = \App\Models\AdminCommission::whereDate('created_at','=',$today)->sum('com_amount');
        $mcount = \App\Models\AdminCommission::whereDate('created_at','<=',$today)->whereDate('created_at','>=',$month)->sum('com_amount');
        $ycount = \App\Models\AdminCommission::whereDate('created_at','<=',$today)->whereDate('created_at','>=',$year)->sum('com_amount');
        
        
//        dd($tdcount,$mcount,$ycount);
        $allD = [
            [
                'label'=>'Today',
                'value'=>ceil($tdcount),
            ],
            [
                'label'=>'Month',
                'value'=>ceil($mcount),
            ],
            [
                'label'=>'Year',
                'value'=>ceil($ycount),
            ],
        ];
        
        $data = [
            [
                'key'=>'Commission in rupies',
                'values'=>$allD,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_sale_by_month($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
            $tdcount = \App\Models\Bookings::active()->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))
                ->select(\DB::raw('count(id) as tot'),\DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                ->groupby('year','month');
                  
            $allM = \DB::table( \DB::raw("({$tdcount->toSql()}) as m") )
                    ->mergeBindings($tdcount->getQuery())
                    ->select(\DB::raw("SUM(IF(month = 1, tot, 0)) AS '1',
                                        SUM(IF(month = 2, tot, 0)) AS '2',
                                        SUM(IF(month = 3, tot, 0)) AS '3',
                                        SUM(IF(month = 4, tot, 0)) AS '4',
                                        SUM(IF(month = 5, tot, 0)) AS '5',
                                        SUM(IF(month = 6, tot, 0)) AS '6',
                                        SUM(IF(month = 7, tot, 0)) AS '7',
                                        SUM(IF(month = 8, tot, 0)) AS '8',
                                        SUM(IF(month = 9, tot, 0)) AS '9',
                                        SUM(IF(month = 10, tot, 0)) AS '10',
                                        SUM(IF(month = 11, tot, 0)) AS '11',
                                        SUM(IF(month = 12, tot, 0)) AS '12'"))->first()
                    ;
        
        
//        dd($allM);
        $marr = [];
        foreach($allM as $k=>$m){
            $mon = \App\Models\General::get_month_from_number($k-1);
            $marr[] = [
                
                    'label'=>$mon,
                    'value'=>$m,
                
            ];
        }
//        dd($marr);
        $data = [
            [
                'key'=>'Total sale in each month',
                'values'=>$marr,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    public static function get_total_daily_sale($param){
        $today=date("Y-m-d");
        $dat2=strtotime($today);
        $dat3 = strtotime("-7 day", $dat2);
        $week=date("Y-m-d",$dat3);
        $dat5 = strtotime("-30 days", $dat2);
        $month=date("Y-m-d",$dat5); 
        $dat7 = strtotime("-365 days", $dat2);
        $year=date("Y-m-d",$dat7);
        
        
            $tdcount = \App\Models\Bookings::active()->whereRaw(\DB::raw('YEAR(created_at) = YEAR(curdate())'))
                    ->whereRaw(\DB::raw('MONTH(created_at) = MONTH(curdate())'))
                ->select(\DB::raw('count(id) as tot'),\DB::raw('DAY(created_at) day, MONTH(created_at) month'))
                ->groupby(\DB::raw('CAST(created_at AS DATE)'))->get()->toArray()
                    ;
        
        
//        dd($tdcount);
        $darr = [];
        $mon = 0;
        $day = [];
        $val = [];
        
        foreach($tdcount as $k=>$m){
            $mon = $m['month'];
            array_push($day, $m['day']);
            
            $val[$m['day']] = [
                'day'=>$m['day'].'-'.$m['month'],
                'ticket' => $m['tot'],
            ];
        }
        $totalD = date('t');
        for($i=1;$i<=$totalD;$i++){
            if(!in_array($i, $day)){
                array_push($day, $i);
                
                $val[$i] = [
                    'day'=>$i.'-'.$mon,
                    'ticket' => 0,
                ];
            }
        }
        ksort($val);
        foreach($val as $k=>$v){
            $darr[] = [
                
                    'label'=>$v['day'],
                    'value'=>$v['ticket'],
                
            ];
        }
//        dd($val);
        $data = [
            [
                'key'=>'Total sale in each day',
                'values'=>$darr,
            ]
        ];
        
        $data = json_encode($data);
        return $data;
    }
    
    public static function check_is_able_to_cancel($param){
        $booking_id = $param['booking_id'];
        $j_date = $param['journey_date'];
        $amount = $param['amount'];
        $today = date('Y-m-d');
        $j_date = date('Y-m-d',strtotime($j_date));
        
        
//        dd(strtotime($j_date),strtotime($today));
        if(strtotime($j_date)<strtotime($today)){
            return \General::error_res('you can not cancel this ticket.you already took this trip.');
        }
        $dateDiff = strtotime($today) - strtotime($j_date);
        if($dateDiff < 0){
            $dateDiff = $dateDiff * -1;
        }
        $fullDays = floor($dateDiff/(60*60*24)); 					 
        $calculatedhours=($fullDays*60*60*24)/(60*60);
        $refunPercen = 0;
        $booking = Bookings::where('booking_id',$booking_id)->with('sp.policies')->first()->toArray();
        $policies = $booking['sp']['policies'];
        if(count($policies)>0){
            foreach($policies as $p){
                if($p['status'] == 1){
                    if($calculatedhours > $p['time']){
                        $refunPercen = $p['amount'];
                        break;
                    }
                }
            }
        }else{
            $refunPercen = 100;
        }
        
        $refund = $amount * $refunPercen / 100;
        
        $res = \General::success_res();
        $res['refund'] = $refund;
        
        return $res;
//        dd($refund,$amount);
    }
    
    public static function csv_routes($csv,$param){
        $csv_ext = array('csv');
        $bus_id = $param['bus_id'];
        if(isset($csv)){
            $ext = $csv->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$csv_ext)){
                $json = \General::validation_error_res();
                $json['msg'] = "Supported Extenstion are ".  implode(',', $csv_ext);
                $res = response()->json($json,200);
                return $res;
            }
        }else{
            return \General::error_res('please select valid csv file.');
        }
//        dd($csv);
        
        $csvAr = array();
        $file = fopen($csv->getPathName(), 'r');

        while (($result = fgetcsv($file)) !== false)
        {
            $csvAr[] = $result;
        }

        fclose($file);
//        dd($csv->getPathName());
        
        
        if(strtolower($csvAr[1][0]) != 'total seat' || $csvAr[1][1] == ''){
            return \General::error_res('total seat required.');
        }
        $total_seat = $csvAr[1][1];
        $cnt = 0;
        $exist = [];
        $preRoute = [];
        $generatedRoute = [];
        $rt = 0;
        foreach($csvAr as $ca){
            if($cnt == 2){
                if(strtolower($ca[0]) != 'city' || strtolower($ca[1]) != 'district' || strtolower($ca[2]) != 'terminal' || strtolower($ca[3]) != 'time'){
                    return \General::error_res('the sequence of city,district,terminal,time not matched.');
                }
            }
            
            if(strtolower($ca[0]) == 'from city'){
                $rt = 1;
                break;
            }
            
            if($cnt > 2 && $rt == 0){
                if($ca[0] == '' || $ca[1] == '' || $ca[2] == '' || $ca[3] == ''){
                    if(isset($csvAr[$cnt+1]) && strtolower($csvAr[$cnt+1][0]) =='from city' ){
                        $rt = 1;
                        break;
                    }else{
                        return \General::error_res('city or district or terminal or time should not blank.');
                    }
                    
                }
                $comb = $ca[0].'-'.$ca[1].'-'.$ca[2].'-'.$ca[3];
                if(in_array($comb, $exist)){
                    return \General::error_res('duplicat combination found for terminal : '.$ca[2]);
                }
                
                $validate = self::validate_basic($ca);
                if($validate['flag'] != 1){
                    return $validate;
                }
                $preRoute[] = $ca;
                array_push($exist, $comb);
            }
            $cnt++;
        }
//        dd($rt);
        $newRoute = [];
        $newCsvAr = [];
        if($rt == 0){
            $l = count($preRoute);
            if($l<2){
                return \General::error_res('please enter atleast add two terminal to generate route.');
            }
            $newRoute[] = ['','','','','','','','',''];
            $newRoute[] = ['From City','From District','From Terminal','From Time','To City','To District','To Terminal','To Time','Price _ '.$total_seat];
            for($i=0;$i<$l;$i++){
                for($j = $i+1;$j<$l;$j++){
                    $newRoute[] = [
                        $preRoute[$i][0],
                        $preRoute[$i][1],
                        $preRoute[$i][2],
                        $preRoute[$i][3],
                        $preRoute[$j][0],
                        $preRoute[$j][1],
                        $preRoute[$j][2],
                        $preRoute[$j][3],
                    ];
                }
            }
            
           $newCsvAr = array_merge($csvAr,$newRoute);
           
           $forDownload = self::array_2_csv($newCsvAr);
           $res = \General::success_res();
           $res['download'] = 1;
           $res['data'] = $forDownload;
           
//           dd($forDownload);
           return $res;
        }
        $nc = $cnt+1;
        $sr = self::save_route($csvAr, $nc,$bus_id);
        if($sr['flag'] != 1){
            return $sr;
        }
//        dd($csvAr[$nc]);
        return $sr;
    }
    
    public static function validate_basic($ca){
        $term = Locality\Terminal::where('name',$ca[2])->with(['locCiti','locDistrict','locProvince'])->first();
        if(!$term){
            return \General::error_res('terminal : '.$ca[2].' not found');
        }
        $term = $term->toArray();

        if(strtolower(trim($term['loc_citi']['name'])) != strtolower(trim($ca[0]))){
            return \General::error_res('terminal : '.$ca[2].' is not of : '.$ca[0] .' city.');
        }
        if(strtolower(trim($term['loc_district']['name'])) != strtolower(trim($ca[1]))){
            return \General::error_res('terminal : '.$ca[2].' is not of : '.$ca[1] .' district.');
        }

//        $expression = '#^(([0-1][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?)$#';
//        $expression = '/(2?[0-3]|[01]?[0-9]):[0-5][0-9]/';
//        $expression = '/([0-9]+):([0-5]?[0-9]):([0-5]?[0-9])/';
        $expression = '/^(0?\d|1\d|2[0-3]):([0-5][0-9])$/';
        
//        var_dump(preg_match($expression, $ca[3]));
//        var_dump($ca[3]);
        if(!preg_match($expression, trim($ca[3]))){
            return \General::error_res('please set proper time at : '.$ca[3].' please enter in HH:MM format');
        }
        $d = [
            'terminal_id' => $term['id'],
            'district_id' => $term['loc_district']['id'],
            'city_id' => $term['loc_citi']['id'],
            'province_id' => $term['loc_province']['id'],
        ];
        $res = \General::success_res();
        $res['data'] = $d;
        return $res;
    }
    
    public static function array_2_csv($array) {
        $csv = array();
        foreach ($array as $item=>$val) {
            if (is_array($val)) { 
                $csv[] = self::array_2_csv($val);
                $csv[] = "\n";
            } else {
                $csv[] = $val;
            }
        }
        $string =  implode(',', $csv);
        return str_replace("\n,", "\n", $string);
    }
    
    public static function save_route($csvAr,$nc,$bus_id){
        $fc = 0;
        $c = 1;
        $rexist = [];
        $finalArray = [];
        for($k=$nc;$k<count($csvAr);$k++){
            $val = $csvAr[$k];
            if($fc == 0){
                if(strtolower($val[0]) != 'from city' || strtolower($val[1]) != 'from district' || strtolower($val[2]) != 'from terminal' || strtolower($val[3]) != 'from time'
                  || strtolower($val[4]) != 'to city' || strtolower($val[5]) != 'to district' || strtolower($val[6]) != 'to terminal' || strtolower($val[7]) != 'to time' ){
                    return \General::error_res('the sequence of city,district,terminal,time not matched in generated route.');
                }else if($val[8] == ''){
                    return \General::error_res('please enter price column with seat count');
                }
                $totCol = count($val);
                
                $d = 8;
                if($totCol > 9){
                    $c += ($totCol - 9);
                }
                $scount = [];
                for($j=0;$j<$c;$j++){
//                    dd($c);
                    if($val[$d] == ''){
                        return \General::error_res('please enter price column with seat count');
                    }
                    $p = explode('_',$val[$d]);
                    if(!isset($p[1]) || $p[1] <= 0){
                        return \General::error_res('seat count is required in price column');
                    }
                    $scount[] = $p[1];
                    $d++;
                }
            }
            if($val[0] == '' || $val[1] == '' || $val[2]== '' || $val[3] == '' || $val[4] == '' || $val[5] == '' || $val[6] == '' || $val[7] == ''){
                return \General::error_res('city or district or terminal or time should not blank in generated route.');
            }
            $comb = $val[0].'-'.$val[1].'-'.$val[2].'-'.$val[6].'-'.$val[5].'-'.$val[4];
            if(in_array($comb, $rexist)){
                return \General::error_res('duplicat route combination found for terminal : '.$val[6]);
            }
            array_push($rexist, $comb);
            
            if($fc > 0){
                $from = [
                    $val[0],
                    $val[1],
                    $val[2],
                    $val[3],
                ];
                $to = [
                    $val[4],
                    $val[5],
                    $val[6],
                    $val[7],
                ];

                $fromD = self::validate_basic($from);
                if($fromD['flag'] != 1){
                    return $fromD;
                }
                
                $toD = self::validate_basic($to);
                if($toD['flag'] != 1){
                    return $toD;
                }
                
                $d = 8;
                $price = [];
                for($j=0;$j<$c;$j++){
//                    dd($c);
                    if($val[$d] == '' || $val[$d] <= 0){
                        return \General::error_res('please enter price for seat count : '.$scount[$j]);
                    }
                    $price[] = [
                        'seat_count'=>$scount[$j],
                        'price'=>$val[$d],
                    ];
                    $d++;
                }
                
                $fd = $fromD['data'];
                $td = $toD['data'];
                
                $route = isset($csvAr[0][1]) ? $csvAr[0][1] : $val[0].'-'.$val[4];
                $route_name = $comb;
                $finalArray[] = [
                    'name'=>$route,
                    'route'=>$route_name,
                    'bus_id'=>$bus_id,
                    'from_terminal_id'=>$fd['terminal_id'],
                    'to_terminal_id'=>$td['terminal_id'],
                    'from_district_id'=>$fd['district_id'],
                    'to_district_id'=>$td['district_id'],
                    'from_city_id'=>$fd['city_id'],
                    'to_city_id'=>$td['city_id'],
                    'from_province'=>$fd['province_id'],
                    'to_province'=>$td['province_id'],
                    'boarding_time'=>$val[3],
                    'droping_time'=>$val[7],
                    'price'=>$price,
                ];
//                dd($fd,$td);
            }
            
            $fc++;
        }
//        dd($finalArray);
        if($finalArray){
            Admin\BusRoutes::where('bus_id',$bus_id)->delete();
            foreach($finalArray as $fa){
                $nr = new Admin\BusRoutes();
                $nr->name = $fa['name'];
                $nr->route = $fa['route'];
                $nr->bus_id = $fa['bus_id'];
                $nr->from_city_id = $fa['from_city_id'];
                $nr->to_city_id = $fa['to_city_id'];
                $nr->from_district_id = $fa['from_district_id'];
                $nr->to_district_id = $fa['to_district_id'];
                $nr->from_terminal_id = $fa['from_terminal_id'];
                $nr->to_terminal_id = $fa['to_terminal_id'];
                $nr->from_province = $fa['from_province'];
                $nr->to_province = $fa['to_province'];
                $nr->boarding_time = $fa['boarding_time'];
                $nr->droping_time = $fa['droping_time'];
                $nr->save();
                $j = 1;
                foreach($fa['price'] as $pr){
                    $prc = new Admin\BusPrice();
                    $prc->bus_id = $bus_id;
                    $prc->sorting = $j;
                    $prc->seat_count = $pr['seat_count'];
                    $prc->price = $pr['price'];
                    $prc->route_id = $nr->id;
                    $prc->save();
                    $j++;
                }
            }
        }
        return \General::success_res('csv uploaded successfully !!');
    }
    
    public static function cache_api_terminals($data,$pre='pre'){
        $key = $pre.'_terminal_list';
        
        \Cache::forever($key,$data);
        
    }
    
    public static function set_by_price_order($data){
//        dd($data);
        
        usort($data, function($a, $b) {
            return $a['bus_fare'] - $b['bus_fare'];
        });
        
        return $data;
    }
    
    public static function convertToCSV($data, $options) { // setting the csv header 
    if (is_array($options) && isset($options['headers']) && is_array($options['headers'])) { 
        $headers = $options['headers']; 
        
    } else { 
        
        $headers = array( 'Content-Type' => 'text/csv',
          'Content-Disposition' => 'attachment; filename="CouponCodeCSV.csv"'
    );
    }
    $output = '';
      
    // setting the first row of the csv if provided in options array
    if (isset($options['firstRow']) && is_array($options['firstRow'])) {
      $output .= implode(',', $options['firstRow']);
      $output .= "\n"; // new line after the first line
    }
      
    // setting the columns for the csv. if columns provided, then fetching the or else object keys
    if (isset($options['columns']) && is_array($options['columns'])) {
      $columns = $options['columns'];
    } else {
      $objectKeys = get_object_vars($data[0]);
      $columns = array_keys($objectKeys);
    }
      
    // populating the main output string
    foreach ($data as $row) {
      foreach ($columns as $column) {
        $output .= str_replace(',', ';', $row->$column);
        $output .= ',';
      }
      $output .= "\n";
    }
      
    // calling the Response class make function inside my class to send the response.
    // if our class is not a controller, this is required.
    return \Response::make($output, 200, $headers);
          
    }
    
    public static function csv_for_admin($param){
        $data = \App\Models\AdminCommission::filter_commlist($param);
        
        $arrColumns = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi dari service Provider','Harga Net','Komisi ke Seat seller','Handling Fee','Pendapatan Bersih');
	     
	    // define the first row which will come as the first row in the csv
	$arrFirstRow = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi dari service Provider','Harga Net','Komisi ke Seat seller','Handling Fee','Pendapatan Bersih');
	     
	    // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $preData = [];
        foreach($data as $r){
            $to_ss = 0;
            $netprice = $r['booking']['total_amount'] - $r['com_amount'];
            $hf = 0;
            if($r['booked_by'] == 5){
                $to_ss = $r['ss_comm'][0]['com_amount'];
            }elseif($r['booked_by'] == 6 || $r['booked_by'] == 7){
                if($r['ss_comm'][1]['ss']['parent_id'] == 0){
                    $to_ss = $r['ss_comm'][1]['com_amount'];
                }
            }
            else{
                $to_ss = 0;
            }
            if($r['booked_by'] == 5 || $r['booked_by'] == 6 || $r['booked_by'] == 7){
                $hf = 0;
            }else{
                $hf = $r['handle_fee'];
            }
            $preData[] = (object) [
                'Booking ID'=>$r['booking_id'],
                'No. Ticket'=>$r['booking']['nos'],
                'Bus Operator'=>$r['sp']['first_name'] .' '. $r['sp']['last_name'],
                'Tanggal Berangkat'=>date('d-m-Y',strtotime($r['booking']['pickup_date'])),
                'Tanggal Pesan'=>date('d-m-Y',strtotime($r['booking']['dropping_date'])),
                'Rute Perjalanan'=>$r['booking']['from_city'].' - '. $r['booking']['to_city'],
                'User Type'=> self::user_type($r['booked_by']),
                'Nama Penumpang'=>$r['booking']['booker_name'],
                'Seller Name'=> self::seller_name($r, $r['booked_by']),
                'Tipe Komisi'=> $r['comm_type'] == 'P' ? 'Percentage' : 'Fixed',
                'Harga Tiket'=>round($r['booking']['total_amount']),
                'Komisi dari service Provider'=>round($r['com_amount']),
                'Harga Net'=>round($r['booking']['total_amount'] - $r['com_amount']),
                'Komisi ke Seat seller'=>$to_ss,
                'Handling Fee'=>$hf,
                'Pendapatan Bersih'=>round($r['com_amount'] - $to_ss + $hf),
            ];
        }
        
        return self::convertToCSV($preData, $options);
        
    }
    public static function csv_for_ss($param){
        $data = \App\Models\SsCommission::filter_commlist($param);
        
        if($param['by'] == 'ss'){
            $arrColumns = $arrFirstRow = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi dari Admin','Harga Net','Komisi ke Seat seller A/B','Handling Fee','Pendapatan Bersih');
        }else{
            $arrColumns = $arrFirstRow = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi dari Seat Seller','Harga Net','Handling Fee','Pendapatan Bersih');
        }
	    // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $preData = [];
        foreach($data as $k=>$r){
            $to_ss = 0;
            $hf = 0;
            if($r['book_by'] == 5){
                $to_ss = 0;
                $comtype = $r['ss_comm'][0]['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                $comFromAdm = $r['ss_comm'][0]['com_amount'];
            }elseif($r['book_by'] == 6 || $r['book_by'] == 7){
                $comtype = $r['ss_comm'][1]['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                $comFromAdm = $r['ss_comm'][1]['com_amount'];
                $to_ss = $r['ss_comm'][0]['com_amount'];
            }
            else{
                $to_ss = 0;
            }
            if($r['book_by'] == 5){
                $hf = $r['ss_comm'][0]['handle_fee'];
            }else{
                $hf = 0;
            }
            
            if($param['by'] == 'ss'){
                
                $preData[] = (object) [
                    'Booking ID'=>$r['booking_id'],
                    'No. Ticket'=>$r['nos'],
                    'Bus Operator'=>$r['ss_comm'][0]['sp']['first_name'] .' '. $r['ss_comm'][0]['sp']['last_name'],
                    'Tanggal Berangkat'=>date('d-m-Y',strtotime($r['pickup_date'])),
                    'Tanggal Pesan'=>date('d-m-Y',strtotime($r['dropping_date'])),
                    'Rute Perjalanan'=>$r['from_city'].' - '. $r['to_city'],
                    'User Type'=> self::user_type($r['book_by']),
                    'Nama Penumpang'=>$r['booker_name'],
                    'Seller Name'=> self::seller_name($r, $r['book_by']),
                    'Tipe Komisi'=> $comtype,
                    'Harga Tiket'=>round($r['total_amount']),
                    'Komisi dari Admin'=>round($comFromAdm),
                    'Harga Net'=>round($r['total_amount'] - $comFromAdm),
                    'Komisi ke Seat seller A/B'=>$to_ss,
                    'Handling Fee'=>$hf,
                    'Pendapatan Bersih'=>round($comFromAdm - $to_ss + $hf),
                ];
            }else{
                $comFromAdm = $r['ss_comm'][0]['com_amount'];
                $to_ss = 0;
                $hf = $r['ss_comm'][0]['handle_fee'];
                $preData[] = (object) [
                    'Booking ID'=>$r['booking_id'],
                    'No. Ticket'=>$r['nos'],
                    'Bus Operator'=>$r['ss_comm'][0]['sp']['first_name'] .' '. $r['ss_comm'][0]['sp']['last_name'],
                    'Tanggal Berangkat'=>date('d-m-Y',strtotime($r['pickup_date'])),
                    'Tanggal Pesan'=>date('d-m-Y',strtotime($r['dropping_date'])),
                    'Rute Perjalanan'=>$r['from_city'].' - '. $r['to_city'],
                    'User Type'=> self::user_type($r['book_by']),
                    'Nama Penumpang'=>$r['booker_name'],
                    'Seller Name'=> self::seller_name($r, $r['book_by']),
                    'Tipe Komisi'=> $comtype,
                    'Harga Tiket'=>round($r['total_amount']),
                    'Komisi dari Seat Seller'=>round($comFromAdm),
                    'Harga Net'=>round($r['total_amount'] - $comFromAdm),
                    'Handling Fee'=>$hf,
                    'Pendapatan Bersih'=>round($comFromAdm - $to_ss + $hf),
                ];
            }
        }
        
        return self::convertToCSV($preData, $options);
    }
    public static function csv_for_sp($param){
        $data = \App\Models\SpCommission::filter_commlist($param);
        if($param['by'] == 'sp'){
            $arrColumns = $arrFirstRow = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi ke admin','Komisi ke Sercice Provider A / B','Pendapatan Bersih');
        }else{
            $arrColumns = $arrFirstRow = array('Booking ID','No. Ticket','Bus Operator','Tanggal Berangkat','Tanggal Pesan','Rute Perjalanan','User Type','Nama Penumpang','Seller Name','Tipe Komisi','Harga Tiket','Komisi dari Service Provider','Harga Net','Handling Fee','Pendapatan Bersih');
        }
	    // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        
        $preData = [];
        foreach($data as $k=>$r){
            $hf = 0;
            $commtoAB = 0;
            $comFromAdm = 0;
            $comtype = '';
            $sname = '';
            if($r['book_by'] == 2){
                if(count($r['admin_comm']) > 0){
                    $comFromAdm = $r['admin_comm']['com_amount'];
                }
                $sname = $r['sp']['first_name'].' '.$r['sp']['last_name'];
            }elseif($r['book_by'] == 3 || $r['book_by'] == 4){
                if(count($r['admin_comm']) > 0){
                    $comFromAdm = $r['admin_comm']['com_amount'];
                }
                if(count($r['sp_comm'])>0){
                    $commtoAB = $r['sp_comm']['com_amount'];
                    $sname = $r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'];
                    $comtype = $r['sp_comm']['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                    $hf = $r['sp_comm']['handle_fee'];
                }
                
            }
            else{
                if(count($r['admin_comm']) > 0){
                    $comFromAdm = $r['admin_comm']['com_amount'];
                }
                $sname = $r['sp']['first_name'].' '.$r['sp']['last_name'];
            }
            
            
            if($param['by'] == 'sp'){
                
                $preData[] = (object) [
                    'Booking ID'=>$r['booking_id'],
                    'No. Ticket'=>$r['nos'],
                    'Bus Operator'=>$r['sp']['first_name'] .' '. $r['sp']['last_name'],
                    'Tanggal Berangkat'=>date('d-m-Y',strtotime($r['pickup_date'])),
                    'Tanggal Pesan'=>date('d-m-Y',strtotime($r['dropping_date'])),
                    'Rute Perjalanan'=>$r['from_city'].' - '. $r['to_city'],
                    'User Type'=> self::user_type($r['book_by']),
                    'Nama Penumpang'=>$r['booker_name'],
                    'Seller Name'=> $sname,
                    'Tipe Komisi'=> $comtype,
                    'Harga Tiket'=>round($r['total_amount']),
                    'Komisi ke admin'=>round($comFromAdm),
                    'Komisi ke Sercice Provider A / B'=>round($commtoAB),
                    'Pendapatan Bersih'=>round($r['total_amount'] - ($comFromAdm + $commtoAB)),
                ];
            }else{
                $preData[] = (object) [
                    'Booking ID'=>$r['booking_id'],
                    'No. Ticket'=>$r['nos'],
                    'Bus Operator'=>$r['sp']['first_name'] .' '. $r['sp']['last_name'],
                    'Tanggal Berangkat'=>date('d-m-Y',strtotime($r['pickup_date'])),
                    'Tanggal Pesan'=>date('d-m-Y',strtotime($r['dropping_date'])),
                    'Rute Perjalanan'=>$r['from_city'].' - '. $r['to_city'],
                    'User Type'=> self::user_type($r['book_by']),
                    'Nama Penumpang'=>$r['booker_name'],
                    'Seller Name'=> $sname,
                    'Tipe Komisi'=> $comtype,
                    'Harga Tiket'=>round($r['total_amount']),
                    'Komisi dari Service Provider'=>round($commtoAB),
                    'Harga Net'=>round($r['total_amount'] - $commtoAB),
                    'Handling Fee'=>$hf,
                    'Pendapatan Bersih'=>round($hf + $commtoAB),
                ];
            }
        }
        
        return self::convertToCSV($preData, $options);
        
    }
    
    public static function user_type($ind){
        $user = ['User','Admin','Service Provider','Service Provider A','Service Provider B','Seat Seller','Seat Seller A','Seat Seller B'];
        return $user[$ind];
    }
    public static function seller_name($r,$ind){
        $user = [
            'Admin',
            'Admin',
            isset($r['sp']) ? $r['sp']['first_name'].' '.$r['sp']['last_name'] : '',
            isset($r['sp_comm']) ? $r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'] : '',
            isset($r['sp_comm']) ? $r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'] : '',
            isset($r['ss_comm'][0]) ? $r['ss_comm'][0]['ss']['name'] : '',
            isset($r['ss_comm'][0]) ? $r['ss_comm'][0]['ss']['name'] : '',
            isset($r['ss_comm'][0]) ? $r['ss_comm'][0]['ss']['name'] : ''
            ];
        return $user[$ind];
    }
}
