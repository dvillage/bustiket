<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class BankDetails extends Model {
    protected $fillable = [
        'bank_name', 'bank_short_name','bank_address','account_name','account_no',
    ];
    protected $table = 'bank_details';
    protected $hidden = [];
    public $timestamps=false;
    
    public static function get_banks(){
        $blist = self::where('status',1)->get()->toArray();
        return $blist;
    }
    public static function get_banks_for_api(){
        $blist = self::where('status',1)->get()->toArray();
        foreach($blist as $k=>$b){
            $blist[$k]['logo'] = \URL::to('assets/images/bank').'/'.$b['logo'];
        }
        return $blist;
    }
    
   
}
    