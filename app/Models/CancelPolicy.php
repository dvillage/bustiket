<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CancelPolicy extends Model {
    protected $fillable = [
        'sp_id', 'duration', 'time','amount','status'
    ];
    protected $table = 'cancellation_policies';
    protected $hidden = [];
    public $timestamps=false;
    
    public function scopeActive($query) {
        return $query->where('status', '=', 1);
    }
    
    public function sp(){
        return $this->hasOne('App\Models\Serviceprovider\ServiceProvider','id','sp_id');
    }
    
    
    public static function get_policy_filter($param){
        $count=self::OrderBY('id','desc');
        $count = $count->count();
        
        if(isset($param['search']) && $param['search']!=''){
//            $count=self::where('name','like','%'.$param['search'].'%')->orWhere('mobile',$param['search']);
            $count=self::whereHas('sp',function($q)use($param){
                $q->where('first_name','like','%'.$param['search'].'%')->orWhere('last_name','like','%'.$param['search'].'%');
                
            });
            $count = $count->count();
        }
        
        $page=$param['crnt'];
        $len=$param['len'];
        $op=$param['opr'];
        $total_page=ceil($count/$len);
        $flag=1;
        
        $start;
        
        if($op!=''){
            if($op=='first'){
                $crnt_page=1;
                $start=($crnt_page-1)*$len;
            }
            
            elseif($op=='prev'){
                $crnt_page=$page-1;
                if($crnt_page<=0){
                    $crnt_page=1;
                }
                $start=($crnt_page-1)*$len;
            }

            elseif($op=='next'){
                $crnt_page=$page+1;
                if($crnt_page>=$total_page){
                    $crnt_page=$total_page;
                }
                $start=($crnt_page-1)*$len;
            }

            else{
                $crnt_page=$total_page;
                $start=($crnt_page-1)*$len;
            }
        }

        else{
            if($page>$total_page){
//                $flag=0;
                $crnt_page=$page-1;
                $start=($crnt_page-1)*$len;
            }
            else{
                $crnt_page=$page;
                $start=($crnt_page-1)*$len;
            }
        }
        
        
        $spdata=self::orderBy('id','desc');
        $spdata = $spdata->with(['sp'])->skip($start)->take($len)->get()->toArray();
        
//        print_r($spdata);
        
        if(isset($param['search']) && $param['search']!=''){
            $crnt_page=1;
            $start=($crnt_page-1)*$len;
            $spdata=self::whereHas('sp',function($q)use($param){
                    $q->where('first_name','like','%'.$param['search'].'%')->orWhere('last_name','like','%'.$param['search'].'%');

                })->orderBy('id','desc');
            $spdata = $spdata->with(['sp'])->skip($start)->take($len)->get()->toArray();
//            dd($spdata);
        }
        
        $res['len']=$len;
        $res['crnt_page']=$crnt_page;
        $res['total_page']=$total_page;
        
        $res['result']=$spdata;
        $res['flag']=$flag;
        return $res;
    }
    
    public static function add_new_policy($param){
        $d = new self;
        $d->sp_id = $param['sp_id'];
        $d->duration = $param['duration'];
        $d->time = $param['time'];
        $d->amount = $param['amount'];
        $d->status = isset($param['status'])?1:0;
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    public static function edit_policy($param){
        $d = self::where('id',$param['did'])->first();
        if(is_null($d)){
            return \General::error_res('no driver found');
        }
        $d->sp_id = $param['sp_id'];
        $d->duration = $param['duration'];
        $d->time = $param['time'];
        $d->amount = $param['amount'];
        $d->status = isset($param['status'])?1:0;
        $d->save();
        
        $res = \General::success_res();
        return $res;
    }
    
    public static function delete_policy($param){
        
        self::where('id',$param['id'])->delete();
        return \General::success_res('policy deleted successfully !!');
    }
}
