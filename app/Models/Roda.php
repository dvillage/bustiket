<?php

namespace App\Models;
use DB;

class Roda {


    public static function makeRodaRequest($param,$print = false,$singlePrint = false){
//        $rqid = config('constant.RQID');
//        $opcode = config('constant.OP_CODE');
//        $demo_url = config('constant.RODA_DEMO_URL');
//        $live_url = config('constant.RODA_LIVE_URL');
//        $user_agent = config('constant.RODA_USER_AGENT');
        $rqid = env('RQID', '');
        $opcode = env('OP_CODE', '');
        $roda_url = env('RODA_URL', 'http://ws.roda.demo.mis.sqiva.com/?');
        $user_agent = env('RODA_USER_AGENT', 'WS-RODA');
        $host = $_SERVER['HTTP_HOST'];
        $param = $param.'&rqid='.$rqid.'&op_code='.$opcode;
        $url = $roda_url.($param);
        
        if($print){
            echo $url;
        }


//        $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';


        $ch =  curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    //    curl_setopt( $ch, CURLOPT_COOKIE, $strCookie );
    //    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36');
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json',"Cache-Control: no-cache"));
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $res = curl_exec($ch);


        if(curl_error($ch))
        {
    //        echo 'error:' . curl_error($ch);
            \Log::info('curl error : '.json_encode(curl_error($ch)));
            $res = '{"err_code":501,"msg":"this response is user defined due to curl error"}';
        }
   

        curl_close($ch);

        if($singlePrint){
//            write_log('roda_api_new.txt', 'url => '.$url);
            \Log::info('Roda Request URL : '.$url);
        }
    //    pr($res);
    //    if($pp > 2) exit;
    //    sleep(20);
    //    if($httpcode == 200){
        $r = json_decode($res,true);
        if($r['err_code'] != 0){
//            write_log('roda_api.txt', 'url => '.$url);
            \Log::info('Roda Request URL : '.$url);
        }
        \Log::info('Roda Request URL : '.$url);
        \Log::info('Roda Response : '.$res);
         
        
            return json_decode($res,true);
    //    }else{
    //            return '';
    //        }

          
    }
    public static function getBusFromApi($from,$to,$journy_date,$sitToBook = 0){
        //orgin = BLORA,PEMALANG,Purbalingga;
        //dest = CEPU,Jakarta;
    //    exit();
        
        $from_id = Locality\Citi::where('name',$from)->first();
        $from_id = $from_id->id;
        $to_id = Locality\Citi::where('name',$to)->first();
        $to_id = $to_id->id;
        $jdate = date('Y-m-d',  strtotime($journy_date));
        
//        $busQ = mysql_query("delete FROM api_bus_list WHERE from_city=$from_id and to_city = $to_id and NOW( ) >= DATE_ADD( created_at, INTERVAL 1 HOUR )    ") or die(mysql_error());
        $busQ = ApiBusList::where('from_city',$from_id)->where('to_city',$to_id)->whereRaw('NOW( ) >= DATE_ADD( created_at, INTERVAL ? HOUR )', [1])->delete();
        
        $q = ApiBusList::where(['from_city'=>$from_id,'to_city'=>$to_id]);
        $q = $q->where('travelling_date',$jdate);
        $env = app('env');
        if($env == 'production'  || $env == 'redesign'){
//            $busQ = ApiBusList::where('from_city',$from_id)->where('to_city',$to_id)->whereRaw('NOW( ) >= DATE_ADD( created_at, INTERVAL ? HOUR )', [1])->delete();
//            $q = $q->where('travelling_date',$jdate);
        }
        
        $q = $q->first();
//        $q = "select * from api_bus_list where from_city = $from_id and to_city= $to_id and travelling_date='".$jdate."'";
        
        if($q){
            $b = json_decode($q->bus_list,true);
//            if(count($b)>0){
//                \Config::set('constant.RODA_BUSES',$b);
                \Session::put('roda_bus', $b);
                return $b;
//            }else{
//                ApiBusList::where(['from_city'=>$from_id,'to_city'=>$to_id,'travelling_date'=>$jdate])->delete();
//            }
        }
        
//        $busQ = mysql_query($q) or die(mysql_error());
//        while($busList = mysql_fetch_assoc($busQ)){
//            if(count($busList)>0){
//    //            pr($busList);
//                 $b = unserialize($busList['bus_list']);
//                if(count($b)>0){
//                    $_SESSION['api_bus_list'] = $b;
//                    return $_SESSION['api_bus_list'];
//                }else{
//                    $q = "delete from api_bus_list where from_city = $from_id and to_city= $to_id and travelling_date='".$jdate."'";
//                    $busQ = mysql_query($q) or die(mysql_error());
//                }
//            }
//        }

//        $pick = mysql_query('select * from api_pickup_points where city_name = "'.$from.'" limit 0,1');
        $pick = ApiTerminals::where('city_name',$from)->first();
        $pickUpPoint = [];
        if($pick){
            $pickUpPoint = json_decode($pick->pick_up_points,true);
        }
//        dd($pick);
//        while($ppoints = mysql_fetch_array($pick)){
//            if(count($ppoints)>0){
//                $pickUpPoint = json_decode($ppoints['pick_up_points'],true);
//    //            $pickUpPoint = array_slice(json_decode($ppoints['pick_up_points'],true),0,4);
//            }
//        } 
//        $drop = mysql_query('select * from api_pickup_points where city_name = "'.$to.'" limit 0,1');
        $drop = ApiTerminals::where('city_name',$to)->first();
        $dropUpPoint = [];
        if($drop){
            $dropUpPoint = json_decode($drop->pick_up_points,true);
        }
//        while($dpoints = mysql_fetch_array($drop)){
//            if(count($dpoints)>0){
//                $dropUpPoint = json_decode($dpoints['pick_up_points'],true);
//    //            $dropUpPoint = array_slice(json_decode($dpoints['pick_up_points'],true),0,4);
//            }
//        } 
        
        if(count($pickUpPoint) == 0 || count($dropUpPoint) == 0){
            
            $param = 'app=data&action=get_pickup_point';
            $pickUpPoint = self::makeRodaRequest($param);
            \Log::info('Roda pickup point response : '.  json_encode($pickUpPoint));
                if($pickUpPoint['err_code'] != 0){
                return null;
            }
            if(isset($pickUpPoint['pickup_point'])){
                ApiTerminals::truncate();
            }
//            mysql_query('TRUNCATE TABLE api_pickup_points');
            
            $tc = 0;
            foreach($pickUpPoint['pickup_point'] as $pp){
//                    $from_id = get_city_id($pp[1]);
                    $from_id = Locality\Citi::where("name",$pp[1])->first();
                    if($from_id){
                        $from_id = $from_id->id ? $from_id->id : 0; 
                        $org_code = $pp[0];
                        $mainOrigin = $pp[0];   
                        $terminalPoints = $pp[2];
        //                $ter = array_push($terminalPoints, array($mainOrigin,$pp[1]));
                        foreach($terminalPoints as $t=>$tp){
                            if(!in_array($mainOrigin, $tp)){
                                $tc++;    
                            }else{
                                $tc = 0;
                                $terminalPoints[$t][1] =  $pp[1];
                                $movecolour = $terminalPoints[$t];
                                unset($terminalPoints[$t]);
                                array_unshift($terminalPoints, $movecolour);
                                break;
                            }
                        }
                    if($tc > 0 ){
                        $ter = array_unshift($terminalPoints, array($mainOrigin,$pp[1]));
                    }
                    $enP = json_encode($terminalPoints);
    //                mysql_query("insert into api_pickup_points (city_id,city_name,pick_up_points,created_at)values($from_id,'".$pp[1]."','". $enP."',now())") or die(mysql_error());
                    $inT = new ApiTerminals();
                    $inT->city_id = $from_id;
                    $inT->city_name = $pp[1];
                    $inT->pick_up_points = $enP;
                    $inT->save();
                    }
                    
                
            }
            $pick = ApiTerminals::where('city_name',$from)->first();
            $pickUpPoint = [];
            if($pick){
                $pickUpPoint = json_decode($pick->pick_up_points,true);
            }
            
            $drop = ApiTerminals::where('city_name',$to)->first();
            $dropUpPoint = [];
            if($drop){
                $dropUpPoint = json_decode($drop->pick_up_points,true);
            }

        }

        $file_name = 'roda_api.txt';

    //    echo $_SERVER['HTTP_USER_AGENT'] ;
//        dd($pickUpPoint);
        $org_code = '';
        $terminalPoints = array();
        $destPoints = array();
        $singleTerminal = '';
        $mainOrigin = '';
        $mainDest = '';
        $busKey = 0;
        if(isset($pickUpPoint) && is_array($pickUpPoint)){

            $brb = 0;
            $drb = 0;
            foreach($pickUpPoint as $pp){

                if(trim(strtolower($pp[1])) == trim(strtolower($from)) ){
                    $org_code = $pp[0];
                    $mainOrigin = $pp[0];   
                }
            }
            foreach($dropUpPoint as $pp){
                if(trim(strtolower($pp[1])) == trim(strtolower($to)) ){
                    $mainDest = $pp[0];
                    $drb = 1;
                }
            }

            if($org_code == ''){
                return null;
            }else{
                $busList = array();
                $reg = array();
                $route_code_arr = array();
//                       dd($pickUpPoint);
                foreach($pickUpPoint as $ter){
//                    pr($pickUpPoint);
                    $route_code = '';

    //                if($dest_code != ''){
                    if(count($dropUpPoint) > 0){
                        foreach($dropUpPoint as $dest_code){
                        $date = date('Ymd',  strtotime($journy_date));
                        $param = 'app=information&action=get_schedule_v2&org='.$ter[0].'&des='.$dest_code[0].'&trip_date='.$date;
                        $schedule = self::makeRodaRequest($param,0,1);
    //                    echo '<br>';
//                        dd($schedule);
                        \Log::info('schedule : '.  json_encode($schedule));
                        if(isset($schedule)){
                            if($schedule['err_code'] != 0){
    //                            write_log($file_name, 'get schedule v2 :'. json_encode($schedule) ,1);
        //                        return null;
                            }else{

                                $direct = $schedule['schedule'][0];
                                foreach($direct as $k=>$dir){

                              //$busList[$k]['route'] = $dir[11];
                                    $seatAvabilityArray = $dir[10];
                                    $totalSeat = 0;
                                    foreach($seatAvabilityArray as $sa){
                                        $totalSeat = $totalSeat + (int)$sa[1];
                                    }

                                    if($totalSeat == 0){
        //                                return null;
        //                                                    continue;
                                    }

                                    $route = explode('-', $dir[11]);
                                    $routes = '';
    //                            pr($dir[11]);
                                    foreach($route as $r=>$v){
                                        foreach($pickUpPoint as $pp){

                                                if(trim(strtolower($pp[0])) == trim(strtolower($v)) ){
                                                    $routes .= ucfirst(strtolower($pp[1])).' - ';
                                                }
                                        }
                                        foreach($dropUpPoint as $pp){

                                                if(trim(strtolower($pp[0])) == trim(strtolower($v)) ){
                                                    $routes .= ucfirst(strtolower($pp[1])).' - ';
                                                }
                                        }
                                    }

                                    $route_code = explode('-', $dir[0]);
                                    if(!in_array($dir[8], $reg)){
                                        array_push($reg, $dir[8]);
                                        $param = 'reg_code='.$dir[8].'&app=information&action=get_seat_map';
                                        $seatMap = self::makeRodaRequest($param,0,1);
                                        \Log::info('seat map : '.  json_encode($seatMap));
                                    }

    //                            pr($seatMap);
                                    $busName = '';
                                    $seatmapArray = array();
                                    $seatclass = '';
                                    if($seatMap['err_code'] != 0){
    //                                    write_log($file_name, 'get seat map :'. json_encode($seatMap) ,1);
                                        continue;
                                    }else{
                                        $seatmapArray = $seatMap['seat_map'];
                                        $seatclass = $seatmapArray[0][4];
                                    }

                                    if(count($seatmapArray) == 0){

    //                                    write_log($file_name, 'get seat map array:'. json_encode($seatMap) ,1);
    //                                    write_log('roda_api_new.txt', 'get seat map array:'. json_encode($seatMap) ,1);
                                    }

                                    if($seatclass == 'F'){
                                        $busName = 'Executive - ';
                                    }elseif($seatclass == 'C'){
                                        $busName = 'VIP - ';
                                    }elseif($seatclass == 'Y'){
                                        $busName = 'Patas - ';
                                    }
                                    if($seatclass == ''){
                                        continue;
                                    }
                                    if(!in_array($route_code[1], $route_code_arr)){
                                        array_push($route_code_arr, $route_code[1]);
                                        $param = 'app=information&action=get_available_seat&org='.$mainOrigin.'&des='.$mainDest.'&trip_date='.$date.'&route_code='.$route_code[1].'&class='.$seatclass;
                                        $availSeat = self::makeRodaRequest($param,0,1);
                                        \Log::info('available seat : '.  json_encode($availSeat));
                                    }
//                                    pr($route_code[1]);
//                                    pr($availSeat);
                                    if($availSeat['err_code'] != 0){
//                                        write_log($file_name, 'available seat :'. json_encode($availSeat) ,1);
                                        continue;
                                    }else{
                                        $availSeats = $availSeat['available_seat'];
                                    }
                                    if(count($availSeats) > 0){
                                        $totalSeat = count($availSeats);
                                    }else{
                                        continue;
                                    }
                                    $param = 'app=information&action=get_fare_v2_new&org='.$ter[0].'&des='.$dest_code[0].'&trip_date='.$date.'&route_code='.$route_code[1].'&return_trip=0';
                                    $payfare = self::makeRodaRequest($param,0,1);

//                                    pr($payfare);
                                    if($payfare['err_code'] != 0){
//                                        write_log($file_name, 'get fare v2 :'. json_encode($payfare) ,1);
//                                        continue;
                                    }else{
                                        $fare = 0;
                                        $subclass ='';
                                        foreach($seatAvabilityArray as $k1=>$sa){
                                            if($fare == 0 && $subclass == ''){
                                                if( (int) $sa[1] > 0 && $sa[1] >= $sitToBook){

                                                    $subclass = $payfare['fare_info'][$k1][0];
                                                    $subc = explode('/', $subclass)[0];
                                                    if($subc == $sa[0]){
                                                        $fare = $payfare['fare_info'][$k1][1][0];
                                                        $subclass = $payfare['fare_info'][$k1][0];
                                                    }
                                                }
                                            }
                                        }
                                        
                                        if($fare == 0){
                                            continue;
                                        }
                                        
                                        $busList[$busKey]['from'] = $from;
                                        $busList[$busKey]['to'] = $to;
                                        $busList[$busKey]['org_code'] = $ter[0];
                                        $busList[$busKey]['dest_code'] = $dest_code[0];
                                        $busList[$busKey]['trip_date'] = $date;
                                        $busList[$busKey]['main_org_code'] = $mainOrigin;
                                        $busList[$busKey]['main_dest_code'] = $mainDest;
                                        $busList[$busKey]['route_code'] = $dir[0];
                                        $busList[$busKey]['board_point'] = $ter[1];
                                        $busList[$busKey]['drop_point'] = $dest_code[1];
                                        $busList[$busKey]['total_seat'] = $totalSeat;
                                        $busList[$busKey]['departure_date'] = $dir[3];
                                        $busList[$busKey]['arrival_date'] = $dir[4];
                                        $busList[$busKey]['departure_time'] = substr(trim($dir[5]),0,4);
                                        $busList[$busKey]['arrival_time'] = substr(trim($dir[6]),0,4);
                                        $busList[$busKey]['duration'] = $dir[7];
                                        $busList[$busKey]['bus'] = $busName.$dir[8];
                                        $busList[$busKey]['subclass'] = $subclass;
                                        $busList[$busKey]['fare'] = $fare;
                                        $busList[$busKey]['route'] = rtrim($routes,'- ');
                                        $busList[$busKey]['seat_map'] = $seatmapArray;
                                        $busList[$busKey]['available_seat'] = $availSeats;

                                        $busKey++;

                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                $_SESSION['api_bus_list'] = $busList;
                $newB = [];
                if(count($busList) > 0){
//                  $q = "insert into api_bus_list (from_city,to_city,travelling_date,bus_list,created_at,updated_at)values($from_id , $to_id , '".$jdate."' ,'".  serialize($busList)."',now(),now())";
//                  $busQ = mysql_query($q) or die(mysql_error());
                    $newB = self::grou_bus_by_route_code($busList);
                }
                $nb = new ApiBusList();
                $nb->from_city = $from_id;
                $nb->to_city = $to_id;
                $nb->travelling_date = $jdate;
                $nb->bus_list = json_encode($newB);
                $nb->save();
                \Session::put('roda_bus', $newB);

                
//                \Config::set('constant.RODA_BUSES',$newB);
      //          pr($busList);
                return $newB;
              }

            }
            return [];

        //echo $org_code;

    }
    
    public static function grou_bus_by_route_code($rodaBus){
//        dd($rodaBus);
        $bus = [];
        $code = [];
        
        $newBus = [];
        foreach($rodaBus as $k=>$rb){
//            if(!in_array($rb['route_code'], $code)){
//                array_push($code, $rb['route_code']);
//                $bus[$rb['route_code']][] = $rb;
//            }else{
                $bus[$rb['route_code']][] = $rb;
//            }
        }
        foreach($bus as $k=>$b){
//            dd($b);
//            if(!in_array($b['org_code'], $code)){
//                array_push($code, $b['org_code']);
            $term = [];
            foreach($b as $bk=>$bb){
                if(count($term) == 0){
                    array_push($term, $bb);
                    $newBus[$k] = [
                        "from" => $bb['from'],
                        "to" => $bb['to'],
                        "trip_date" => $bb['trip_date'],
                        "main_org_code" => $bb['main_org_code'],
                        "main_dest_code" => $bb['main_dest_code'],
                        "route_code" => $bb['route_code'],
                        "total_seat" => $bb['total_seat'],
                        "bus" => $bb['bus'],
                        "route" => $bb['route'],
                        "fare" => $bb['fare'],
                        "seat_map" => $bb['seat_map'],
                        "available_seat" => $bb['available_seat'],
                    ];
                }
                $newBus[$k]['terminals'][$bb['org_code']][] = [
                    "org_code" => $bb['org_code'],
                    "dest_code" => $bb['dest_code'],
                    "board_point" => $bb['board_point'],
                    "drop_point" => $bb['drop_point'],
                    "departure_date" => $bb['departure_date'],
                    "arrival_date" => $bb['arrival_date'],
                    "departure_time" => date('H:i',strtotime($bb['departure_time'])),
                    "arrival_time" => date('H:i',strtotime($bb['arrival_time'])),
                    "duration" => $bb['duration'],
                    "subclass" => $bb['subclass'],
                    "fare" => $bb['fare'],
                ];
                
            }
                
//            }else{
//                $bus[$rb['route_code']][] = $rb;
//            }
        }
//        echo '<pre>';
//        print_r($newBus);
//        echo '</pre>';
//        echo json_encode($newBus);
        return $newBus;
    }
 
    public static function get_roda_bus($allBus,$key){
        $bus=[];
        foreach($allBus as $k=>$v){
            if($k==$key){
                $bus = $v;
            }
        }
        return $bus;
    }
    
    public static function all_seats($bus){
        $seats = [];
        $rows = 0;
        $cols = 0;
        $r = [];
        $c = [];
        
        foreach($bus['seat_map'] as $m){
            if(!in_array($m[0], $r)){
                array_push($r, $m[0]);
                $rows++;
            }
            if(!in_array($m[1], $c)){
                array_push($c, $m[1]);
                $cols++;
            }
            
            if($m[3] != ''){
                $obt = 'CS';
                if($m[4] == 'T'){
                    $obt = 'T';
                }
                $seats[] = [
                    'row'=>$m[0]-1,
                    'col'=>$m[1]-1,
                    'object_type'=>$obt,
                    'seat_lbl'=>$m[2].$m[3],
                    'seat_index'=>-1
                ];
            }
            
        }
        $available_seats = $bus['available_seat'];
        $booked = [];
        foreach($seats as $st){
            if(!in_array($st['seat_lbl'], $available_seats)){
                array_push($booked, $st['seat_lbl']);
            }
        }
        
        if($rows > $cols){
            $seats1 = [];
            $k = 0;
            for($i=0;$i<$cols;$i++){
                for($j=0;$j<$rows;$j++){
                    foreach($seats as $key=>$s){
                        if($i == $s['col'] && $j == $s['row']){
                            $s['col'] = $j;
                            $s['row'] = $i;
                            $seats1[$k] = $s;
                        }
                    }
                    $k++;
                }
            }

            $t = $rows;
            $rows = $cols;
            $cols = $t;
            $seats = $seats1;
        }
        
        $data = [
            'rows' =>$rows,
            'cols' =>$cols,
            'seats' =>$seats,
            'available_seats' =>$available_seats,
            'booked_seats' =>$booked,
            'blocked_seats' =>[],
        ];
        return $data;
    }
    
    public static function makeRodaBooking($busDetail,$booker_name,$mobile,$pass_list,$booking_id){
            $res = \General::error_res();
            
            $tot_seat = count($pass_list);
            $action = 'booking_v2';
            $org = $pass_list[0]['from_terminal'];
            $dest = $pass_list[0]['to_terminal'];
//            dd($busDetail);
            $Route = $busDetail['terminals'][$org];
            $session_id = $pass_list[0]['session_id'];
            $ter = [];
            foreach($Route as $r){
                if($r['dest_code'] == $dest){
                    $ter = $r;
                }
            }
            if(count($ter) == 0){
                $res['msg'] = 'no terminal combination found.'; 
                return $res;
            }
//            dd($ter);
            $dep_route_code = explode('-',$busDetail['route_code'])[1];
            $dep_date = $ter['departure_date'];
            $subclass_dep = urlencode($ter['subclass']);
            $caller = urlencode(substr($booker_name,0,30));
            $contact_1 = 'testAPI';//urlencode(substr($booker_name,0,30));
            $adult_detail = '';
            $num_pax_adult = trim($tot_seat);
            
            $bus_fare = $ter['fare'] * $tot_seat;
            
//            for ($i = 1; $i <= $tot_seat; $i++) {
            $i = 1;
            
            $isSeat = 0;
            
            foreach($pass_list as $pass){
                
                if($pass['seat_lbl'] != ''){
                    $isSeat = 1;
                }
                
                $pass_name = urlencode(substr($pass['passenger_name'],0,50));
                $gender    = $pass['passenger_gender'];
                if($gender == 'm'){
                    $g = 'MR';
                }else{
                    $g = 'MS';
                }
                $adult_detail .= '&a_first_name_'.$i.'='.$pass_name.'&a_salutation_'.$i.'='.$g.'&a_mobile_'.$i.'='.$mobile;
                $i++;
            }
            
        $param = 'app=transaction&action=booking_v2&org='.$org.'&des='.$dest.'&round_trip=0&dep_route_code='.$dep_route_code.'&dep_date='.$dep_date.'&subclass_dep='.$subclass_dep.'&caller='.$caller.'&contact_1='.$contact_1.'&num_pax_adult='.$num_pax_adult.$adult_detail;
        //echo htmlspecialchars($param).'<br>' ;
            
            $booking = self::makeRodaRequest($param,0,1);
            \Log::info('booking '.json_encode($booking));
//            dd($booking);
            if($booking['err_code'] != 0){
//                mysql_query('insert into api_bus_info (ticket_id,route_code,bus_name,pay_status,book_status,routes,booking_response)values("'.$booking_id.'","'.$Bus_id.'","'.$busDetail['bus'].'","3","0","'.$busDetail['route'].'","'.mysql_real_escape_string(json_encode($booking)).'")') or die(mysql_error());
                $data = [
                    'booking_id'=>$booking_id,
                    'route_code'=>$busDetail['route_code'],
                    'bus_name'=>$busDetail['bus'],
                    'is_seat'=>$isSeat,
                    'book_code'=>'',
                    'pay_status'=>3,
                    'book_status'=>0,
                    'booking_response'=>json_encode($booking),
                    'payment_response'=>'',
                    'routes'=>$busDetail['route'],
                    'bus_detail'=>json_encode($busDetail),
                ];
                ApiBusInfo::add_bus_info($data);
                BookingSeats::where('session_id',$session_id)->delete();
                $res['msg'] = 'something went wrong please try again to book ticket.';
                return $res;
//                mysql_query('insert into api_bus_info (ticket_id,route_code,bus_name,pay_status,book_status,routes,booking_response)values("'.$booking_id.'","'.$Bus_id.'","'.$busDetail['bus'].'","3","0","'.$busDetail['route'].'","'.mysql_real_escape_string(json_encode($booking)).'")') or die(mysql_error());
//                echo '<script>alert("something went wrong please try again to book ticket.");window.location="";</script>';exit;
            }else{
                $book_code = $booking['book_code'];
                $booking_id = $book_code;
//                dd($booking,$book_code);
                $param = 'app=information&action=get_book_info&book_code='.$book_code;
                $bookinInfo = self::makeRodaRequest($param,0,1);
                \Log::info('booking info '.json_encode($bookinInfo));
//                pr($bookinInfo);
                if($bookinInfo['err_code'] == 0){
                    $passlist = $bookinInfo['pax_list'];
                    
//                    $lables = '';
                    $uniqIds = array();
                    foreach($passlist as $k=>$p){
                        $uniqIds[$k] = $p[6];
                        $seatLabel = $pass_list[$k];
                        $route_code = $dep_route_code;
                        $param = 'app=transaction&action=take_seat&book_code='.urlencode($book_code).'&pax_first_name='.urlencode($p[0]).'&trip_date='.$busDetail['trip_date'].'&route_code='.$route_code.'&org='.$busDetail['main_org_code'].'&des='.$busDetail['main_dest_code'].'&pax_seat='.$seatLabel['seat_lbl'].'&old_seat='.$p[7];
                        if($seatLabel['seat_lbl'] != $p[7] && $isSeat == 1){
                            $seatChange = self::makeRodaRequest($param,0,1);
    //                        pr($seatChange);
                            if($seatChange['err_code'] != 0){
                                $data = [
                                    'booking_id'=>$booking_id,
                                    'route_code'=>$busDetail['route_code'],
                                    'bus_name'=>$busDetail['bus'],
                                    'is_seat'=>$isSeat,
                                    'book_code'=>$book_code,
                                    'pay_status'=>3,
                                    'book_status'=>0,
                                    'booking_response'=>json_encode($seatChange),
                                    'payment_response'=>'',
                                    'routes'=>$busDetail['route'],
                                    'bus_detail'=>json_encode($busDetail),
                                ];
                                ApiBusInfo::add_bus_info($data);
                                
                                $res['msg'] = 'The selected seat is not avilable now.please try again with another seat.';
                                return $res;
//                                mysql_query('insert into api_bus_info (ticket_id,route_code,bus_name,book_code,pay_status,book_status,routes,booking_response)values("'.$booking_id.'","'.$Bus_id.'","'.$busDetail['bus'].'","'.$book_code.'","3","0","'.$busDetail['route'].'","'.mysql_real_escape_string(json_encode($seatChange)).'")') or die(mysql_error());
//                                delete_old_bus($busDetail['from'], $busDetail['to'], $busDetail['departure_date']);
//                            echo '<script>alert("The selected seat is not avilable now.please try again with another seat.");window.location="";</script>';
//                            exit;
                            }else{

                            }
                        }else{
                            if($isSeat == 0){
                                BookingSeats::where('id',$seatLabel['id'])->update(['seat_lbl'=>$p[7]]);
                            }
                        }
//                        $lables .= $p[7].',';
                    }
//                    $lables = rtrim($lables,',');
                    //$_SESSION['book_var']['total_seats_label'] = $lables;
                }else{
                    $data = [
                        'booking_id'=>$booking_id,
                        'route_code'=>$busDetail['route_code'],
                        'bus_name'=>$busDetail['bus'],
                        'is_seat'=>$isSeat,
                        'book_code'=>$book_code,
                        'pay_status'=>3,
                        'book_status'=>0,
                        'booking_response'=>json_encode($bookinInfo),
                        'payment_response'=>'',
                        'routes'=>$busDetail['route'],
                        'bus_detail'=>json_encode($busDetail),
                    ];
                    ApiBusInfo::add_bus_info($data);
                    $res['msg'] = 'Something is wrong please try agian.';
                    return $res;
//                     mysql_query('insert into api_bus_info (ticket_id,route_code,bus_name,book_code,pay_status,book_status,routes,booking_response)values("'.$booking_id.'","'.$Bus_id.'","'.$busDetail['bus'].'","'.$book_code.'","3","0","'.$busDetail['route'].'","'.mysql_real_escape_string(json_encode($bookinInfo)).'")') or die(mysql_error());
//                        echo '<script>alert("Something is wrong please try agian.");window.location="";</script>';exit;
                }
                $data = [
                        'booking_id'=>$booking_id,
                        'route_code'=>$busDetail['route_code'],
                        'bus_name'=>$busDetail['bus'],
                        'is_seat'=>$isSeat,
                        'book_code'=>$book_code,
                        'pay_status'=>3,
                        'book_status'=>3,
                        'booking_response'=>json_encode($booking),
                        'payment_response'=>'',
                        'routes'=>$busDetail['route'],
                        'bus_detail'=>json_encode($busDetail),
                    
                    ];
                    ApiBusInfo::add_bus_info($data);
//                mysql_query('insert into api_bus_info (ticket_id,route_code,bus_name,pay_status,book_status,routes,book_code,booking_response)values("'.$book_code.'","'.$Bus_id.'","'.$busDetail['bus'].'","3","3","'.$busDetail['route'].'","'.$book_code.'","'.mysql_real_escape_string(json_encode($booking)).'")');
            }
            $res = \General::success_res();
            $res['booking_id'] = $booking_id;
            return $res;
    }
    public static function get_bus_from_db($param){
        $date = date('Ymd',strtotime($param['date']));
//        $from = Locality\Citi::where('id',$param['from_id'])->first();
//        $fname = $from->name;
//        $to = Locality\Citi::where('id',$param['to_id'])->first();
//        $tname = $to->name;
//        dd($param);
        $rb = ApiBusList::where('from_city',$param['from_id'])->where('to_city',$param['to_id'])->where('travelling_date',$param['date'])->first();
        if(!$rb){
//            dd($rb);
            return \General::error_res('no bus found');
        }
        $bus = json_decode($rb->bus_list,true);
        $res = \General::success_res();
        $res['data'] = $bus;
        return $res;
    }
    public static function group_terminals($rbus,$param){
        $r = [];
        foreach($rbus['terminals'] as $ter){
            foreach($ter as $t){
                $r[] = [
                    'id'          => 0,
                    'route'       => $rbus['route'],
                    'from_terminal_id'     => $t['org_code'],
                    'from_terminal'   => ['name' => $t['board_point']],
                    'to_terminal_id'       => $t['dest_code'],
                    'to_terminal'     => ['name' => $t['drop_point']],
                    'boarding_time'  => $t['departure_time'],
                    'droping_time'   => $t['arrival_time'],
                    'price'       => $t['fare'],
                ]; 
            }
        }
        
//        dd($r);
        return $r;
    }
    public static function get_terminals($rbus,$param){
        $r = [];
//        dd($param);
        foreach($rbus['terminals'] as $ter){
            foreach($ter as $t){
                if($t['org_code'] == $param['from_term_id'] && $t['dest_code'] == $param['to_term_id']){
                    $r = $t;
                }
            }
        }
        if(count($r) == 0){
            return \General::error_res('no terminal combination found');
        }
        $res = \General::success_res();
        $res['data'] = $r;
//        dd($r);
        return $res;
    }
    
    public static function make_roda_payment($booking){
//        $amount = $bkDetails['booking_amt'];
        
        $apiBus = ApiBusInfo::where('booking_id',$booking->booking_id)->first();
        $amount = $booking->base_amount;
//        $apiqry = 'select * from api_bus_info where ticket_id = "'.$Ticket_ID.'"';
//        $q = mysql_query($apiqry) or die(mysql_error());
//        $resapi = mysql_fetch_array($q);
//        pr($resapi);
        $book_code = $apiBus->book_code;
        $route_code = $apiBus->route_code;
        $uniqIds = array();
        
        if($apiBus->pay_status != 1){
            $param = 'app=transaction&action=payment&book_code='.$book_code.'&amount='.$amount;
            $payment = self::makeRodaRequest($param,0,1);
    //        pr($payment);

            if($payment['err_code'] != 0){
//                $upd = 'update api_bus_info set pay_status=0 , payment_response="'.  mysql_real_escape_string(json_encode($payment)).'" where ticket_id = "'.$Ticket_ID.'"';
//                mysql_query($upd);
                $apiBus->pay_status = 0;
                $apiBus->payment_response = json_encode($payment);
                $apiBus->save();
//                            echo '<script>alert("ticket booked successfully but payment transaction to roda failed.");</script>';
            }else{
                
//                $upd = 'update api_bus_info set pay_status=1,book_status=1 , payment_response="'.  mysql_real_escape_string(json_encode($payment)).'" where ticket_id = "'.$Ticket_ID.'"';
//                mysql_query($upd);
                
                $apiBus->pay_status = 1;
                $apiBus->book_status = 1;
                $apiBus->payment_response = json_encode($payment);
                $apiBus->save();
                $f_city_id = 0;
                $from_city = Locality\Citi::where('name',$booking->from_city)->first();
                if($from_city){
                    $f_city_id = $from_city->id;
                }
                $t_city_id = 0;
                $to_city = Locality\Citi::where('name',$booking->to_city)->first();
                if($to_city){
                    $t_city_id = $to_city->id;
                }
                
                
                $param = 'app=information&action=get_book_info&book_code='.$book_code;
                $bookinInfo = self::makeRodaRequest($param,0,1);
                \Log::info('booking info : '.  json_encode($bookinInfo));
                if($bookinInfo['err_code'] == 0){
                    $passlist = $bookinInfo['pax_list'];
                    
                    foreach($passlist as $k=>$p){
                        $uniqIds[$k] = $p[6];
                    }
                    
                    $bookingSeats = BookingSeats::where('booking_id',$booking->booking_id)->get()->toArray();
                    if(count($bookingSeats) > 0){
                        $seats = [];
                        foreach($bookingSeats as $k=>$bs){
                            $seats[] = $bs['seat_lbl'];
                            $bs = BookingSeats::where('id',$bs['id'])->first();
                            $bs->ticket_id = $uniqIds[$k];
                            $bs->save();
                        }
                        if($f_city_id && $t_city_id){
                            $date = date('Y-m-d',strtotime($booking->pickup_date));
                            $buslist = ApiBusList::where('from_city',$f_city_id)->where('to_city',$t_city_id)->where('travelling_date',$date)->first();
                            if($buslist){
                                $list = json_decode($buslist->bus_list,true);
                                $avalSeat = $list[$route_code]['available_seat'];
                                $leftSeat = array_diff($avalSeat, $seats);
                                $list[$route_code]['available_seat'] = $leftSeat;
                                $list[$route_code]['total_seat'] = count($leftSeat);
                                
                                $buslist->bus_list = json_encode($list);
                                $buslist->save();
                            }
                        }
                    }
                    
                }
                
            }
        }
    }
    
    public static function roda_cancel_ticket($booking){
        $apiBus = ApiBusInfo::where('booking_id',$booking->booking_id)->first();
        $book_code = $apiBus->book_code;
        
        $void = self::void_payment($book_code);
        if($void['err_code'] != 0){
            $apiBus->cancel_response = json_encode($void);
            $apiBus->save();
            return \General::error_res('booking not found or already cancelled.');
        }
        $param = 'app=transaction&action=cancel_book&book_code='.$book_code;
        $cancel = self::makeRodaRequest($param,0,1);
        if($cancel['err_code'] != 0){
            $apiBus->cancel_response = json_encode($cancel);
            $apiBus->save();
            return \General::error_res('booking not found or already cancelled.');
        }else{
            $res = \General::success_res();
            $apiBus->cancel_response = json_encode($cancel);
            $apiBus->save();
            if($cancel['status'] == 'XX'){
                $refund = $cancel['refund_amount'];
                $res['refund'] = $refund;
            }else{
                $res = \General::error_res('booking not found or already cancelled.');
            }
            return $res;
        }
    }
    
    public static function void_payment($book_code){
        $param = 'app=transaction&action=void_payment&book_code='.$book_code;
        $void = self::makeRodaRequest($param,0,1);
        return $void;
    }
    
}
