<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class PariwisataBookSuccess extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
        public $user_detail;
        public function __construct($ticket_detail)
	{
            $this->user_detail = $ticket_detail;
	}
}
