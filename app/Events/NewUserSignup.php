<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class NewUserSignup extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
        public $user_detail;
        public function __construct($user_detail = [])
	{
            $this->user_detail = $user_detail;
	}
}
