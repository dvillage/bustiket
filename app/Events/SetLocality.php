<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class SetLocality extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
        public $type;
        public $name;
        public $opr;
        public function __construct($name, $opr, $type, $id=null)
	{
//            dd($name, $opr, $type, $id);
            $this->type = $type;
            $this->name = $name;
            $this->opr = $opr;
            $this->id = $id;
	}
}
