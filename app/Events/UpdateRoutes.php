<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class UpdateRoutes extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
        public $type;
        public $data;
        
        public function __construct($data,$type)
	{
//            dd( $type, $data);
            $this->type = $type;
            $this->data = $data;
            
	}
}
