<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class TicketBookSuccess extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
        public $ticket_detail;
        public function __construct($ticket_detail)
	{
            $this->ticket_detail = $ticket_detail;
	}
}
