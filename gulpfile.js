var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.scripts([
        "jquery-1.10.2.js",
        "jquery-ui.js",
        "bootstrap.min.js",
        "owl.carousel.min.js",
        "jquery.flexslider-min.js"
    ], './public/assets/js/app.min.js')
            .styles([
                "bootstrap.min.css",
                "jquery-ui.css",
                "sprite-general.css",
                "owl.carousel.css",
                "flexslider.css",
                "app.css"
            ], './public/assets/css/app.min.css');

    mix.scripts([
        'scripts.js',
    ], './public/assets/js/scripts.js');
    mix.scripts([
        'search.js',
    ], './public/assets/js/search.min.js');
    
    mix.styles([
        'search.css',
    ], './public/assets/css/search.min.css');
    
//    mix.scripts([
//        'jquery-ui.min.js',
//        'mouse.js',
//        'jquery.ui.touch-punch.min.js',
//        'evol-colorpicker.min.js',
//        'meme.js',
////        'swiper.min.js'
//    ], './public/assets/js/meme.js')
//            .styles(['jquery-ui.min.css', 'evol-colorpicker.min.css', 'meme.css'], './public/assets/css/meme.css');

});
