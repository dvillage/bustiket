

<?php $__env->startSection('content'); ?>

	<div class="result_round_inner">
		<div class="container">
			<ul id="tab-nxw">
				<li class="tab-nxw-item1 text-center active">
					<div class="trip_link pull-right"><a id="toggleAdvanceBtn" href="#" onclick="z_black_coer()" class="btn btn-link">Ubah Pencarian&nbsp;&nbsp;<span class="caret"></span></a></div>
					<div class="pagination">
						<ul>
							<!-- <li><a href=""><b>Trip Type:</b> One-way</a></li>-->
							<li><a href="">
									  Jakarta
									  </a> </li>
							<li><a href=""><i class="fa fa-play"></i></a></li>
							<li><a href="">
								  Wonosobo
								  </a> </li>
						</ul>
						<div class="panel-date">
							<a href="results.php?ter_from=Jakarta&amp;tag=Wonosobo&amp;datepicker=01-08-2017&amp;backward">
								<span class="fa fa-angle-left"></span></a> 1 Agustus 2017 <a href="results.php?ter_from=Jakarta&amp;tag=Wonosobo&amp;datepicker=01-08-2017&amp;forward"><span class="fa fa-angle-right"></span></a>
						</div>
					</div>
                <!-- <div class="trip_link pull-right"><a  id="toggleAdvanceBtn" href="results_round.php?triptype=2&ter_from=Jakarta&tag=Wonosobo&datepicker=01-08-2017&datepicker1=02-08-2017&type=bus" class="btn btn-link">+ Return</a></div> --> 
			</li>

			</ul>
		</div>
	</div>
	<div id="roundWayModifySec">
		<div class="round_way_search ">
			<div class="filterSecPanel">
				<form name="filter1" id="filter1" action="results.php" method="get">
					<div class="left-bg">
						<div class="row row_sm listCheckBox">
						<div class="filter col-md-2 filter-one clearfix hidden-sm hidden-xs ">
							<h3>Filter by:</h3>
						</div>
		<script type="text/javascript">
			  $(document).ready(function(){

			  $(".title-select").click(function(e){e.stopPropagation();
			  $(".select_ul_li").hide();
			  $(this).next().toggle();
			  });
			 
			  $("body").click(function(){
				$(".select_ul_li").hide();

			  });   
		});
       </script>
		<div class="filter col-md-2   filter-one tips clearfix">
			<div class="title-select" style="border-right: 1px solid #aaa"><span><img src="http://192.168.0.104/bustiket/images/ico-oparator.png" width="28" height="28" alt=""></span>Operator&nbsp;<span class="caret"></span></div>
				<ul id="sp" class="form-control select_ul_li " onchange="return submitval();" style="display:none">
					<li value="">Journey Travels</li>
                    <li>
					<input name="sp[]" onchange="return submitval();" type="checkbox" value="158">
						<label for="thing"></label>
						Mutiara Trans Travel</li>
                 </ul>
        
		</div>
			<div class="filter col-md-2 filter-one tips clearfix">
				<div class="title-select" style="border-right: 1px solid #aaa"><span><img src="http://192.168.0.104/bustiket/images/ico-seat.png" width="25" height="29" alt=""></span>Tipe &nbsp; <span class="caret"></span></div>
				<ul name="bus_type" id="bus_type" class="form-control select_ul_li" onchange="return submitval();" style="display:none">
				<li value="">Tipe Bus</li>
                    <li>
					<input name="bus_type[]" onchange="return submitval();" type="checkbox" value="1">
					<label for="thing"></label>
						Bus</li>
						<li>
					<input name="bus_type[]" onchange="return submitval();" type="checkbox" value="29">
						<label for="thing"></label>
						Shuttle</li>
						</ul>
        
			</div>
		<div class="filter col-md-2 filter-one clearfix">
        <div class="title-select" style="border-right: 1px solid #aaa"><span><img src="http://192.168.0.104/bustiket/images/ico-info.png" width="29" height="28" alt=""></span>Fitur &nbsp; <span class="caret"></span></div>
				<ul id="lux_item" class="form-control select_ul_li toggle" onchange="return submitval();" style="display:none">
					<li value="">Semua</li>
                    <li>
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="6">
					<label for="thing"></label>
					Makan</li>
                   <li>
						<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="12">
					<label for="thing"></label>
					TV</li>
                   <li>
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="15">
					<label for="thing"></label>
					Snack</li>
                   <li class="showli" style="display:none">
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="16">
					<label for="thing"></label>
					Toilet</li>
                   <li class="showli" style="display:none">
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="17">
					<label for="thing"></label>
					Reclining Seat</li>
                   <li class="showli" style="display:none">
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="18">
					<label for="thing"></label>
					Bantal</li>
                   <li class="showli" style="display:none">
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="19">
					<label for="thing"></label>
					Selimut</li>
                   <li class="showli" style="display:none">
					<input name="lux_item[]" onchange="return submitval();" type="checkbox" value="20">
					<label for="thing"></label>
					WiFi</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="24">
            <label for="thing"></label>
            Smooking Room</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="26">
            <label for="thing"></label>
            Foot Rest</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="27">
            <label for="thing"></label>
            Musik/MP3</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="28">
            <label for="thing"></label>
            AC</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="29">
            <label for="thing"></label>
            Air Minum</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="30">
            <label for="thing"></label>
            DVD</li>
                   <li class="showli" style="display:none">
            <input name="lux_item[]" onchange="return submitval();" type="checkbox" value="31">
            <label for="thing"></label>
            Leg Rest</li>
         <li class="showall">See More</li>        </ul>
        
      </div>


            <div class="filter col-md-2 filter-one clearfix pick-up-pount">
        <div class="title-select" style="border-right: 1px solid #aaa"><span><img src="http://192.168.0.104/bustiket/images/ico-point.png" width="23" height="27" alt=""></span>Berangkat dari &nbsp; <span class="caret"></span></div>
        <ul id="board_point" class="form-control select_ul_li toggle" onchange="return submitval();" style="display:none">
          <li value="">PICK-UP POINT</li>
                   <li>
            <input name="board_point[]" onchange="return submitval();" type="checkbox" value="Agen ROFI Pasar Rebo">
            <label for="thing"></label>
            Agen ROFI Pasar Rebo</li>
                    <li>
            <input name="board_point[]" onchange="return submitval();" type="checkbox" value="Agen Bagus Jaya Travel">
            <label for="thing"></label>
            Agen Bagus Jaya Travel</li>
                  </ul>
          
      </div>

      <div class="filter col-md-2 filter-one clearfix pick-up-pount">
        <div class="title-select"><span><img src="http://192.168.0.104/bustiket/images/ico-point.png" width="23" height="27" alt=""></span>Tiba di &nbsp; <span class="caret"></span></div>
            
        <ul id="drop_point" class="form-control select_ul_li toggle" onchange="return submitval();" style="display:none;width: 97%">
          <li value="">DROPPING POINT</li>
                    <li>
            <input name="drop_point[]" onchange="return submitval();" type="checkbox" value="Wonosobo">
            <label for="thing"></label>
            Wonosobo          </li>
                  </ul>
        
      </div>
      
      <!-- <div class="filter col-sm-2">
        <div class="title-select" ><a href="javascript:resetSearch();" ><span><i class="fa fa-user"></i></span><strong>Reset Search</strong></a></div>
      </div> -->
      <div class="clearfix"></div>
    </div>
  </div>
				</form>
			</div>
			
			<div id="z_black_coer">
				<div class="container">
					<div id="panel-dark" class="bus-xx">
						<div class="tab-content" id="view-tab">
							<div id="table-tab1" class="tab-pane fade active in">
								<table id="busTable" class="table">
									<thead>
										<tr>
											<th class="optname_bus">OPERATOR</th>
											<th class="type_bus">TIPE</th>
											<th class="time_bus">BERANGKAT</th>
											<th class="next_bus text-center">RUTE</th>
											<th class="time_bus">TIBA</th>
											<th class="seat_bus">KURSI</th>
											<th class="price_bus">HARGA</th>
											<th class="action_bus"></th>
										</tr>
									</thead>
								<tbody>
									<tr class="hidden-sm hidden-xs">
										<td class="optname_bus">
												<h3>
													Pahala Kencana                          
												</h3>
												<p>
													Executive - 531                         
												</p>
										<div class="sea_res_b">
											<ul class="p">
												<li> 
												</li>
												<li>  </li>
											</ul>
										</div>
											<!--  gallary image part start -->
								      </td>
									<td class="type_bus"><img src="http://192.168.0.104/bustiket/images/Bus1459222124.png"><span>Bus</span></td>
									<td class="time_bus"><div class="sea_res_b"> <span>
										<p class="default">18:31</p>
										</span> </div>
									</td>
									<td class="next_bus"><div class="sea_res_b">
										<p>
										Jakarta - Bekasi - Purwakarta - Cirebon<br>
										- Brebes - Brebes. jawa tengah -<br>
										Banyumas - Purbalingga - Banjarnegara -<br>
										Banjarnegara. jawa tengah. terminal -<br>
										Wonosobo - Bekasi - Purwakarta - Cirebon<br>
										- Brebes - Brebes. jawa tengah -<br>
										Banyumas - Purbalingga - Banjarnegara -<br>
										Banjarnegara. jawa tengah. terminal                            </p>
										</div></td>
									<td class="time_bus"><div class="sea_res_b"> <span>
										<p class="default">03:59</p>
										</span> </div></td>
									<td class="seat_bus">
										<h3> 32 Kursi  </h3></td>
									<td class="price_bus"><div class="rp"> <span class="sub">Rp</span>
										<h3>
											<strong>300.</strong><sup>000</sup>
										</h3>
									</div></td>
								<td class="action_bus">
									<span class="sea_res_d"> <a name="image" id="showPK-403" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success y-btn" onclick="goNext('PK-403','2017-08-01','1'); ">Pilih Kursi</a>
									<div class="loder" id="loaderPK-403" style="display:none;"><img src="ajax-loader.gif"></div>
									</span>
								</td>
								</tr>
							<tr class="hidden-lg hidden-md">
								<td colspan="8"><table width="100%" cellpadding="0" cellspacing="0" class="table tableM no-border">
                            <tbody><tr>
                              <td class="m_data_left"><h3>Pahala Kencana </h3>
                                <p> Executive - 531</p>
                                <p>  Kursi </p></td>
                              <td class="m_data_right"><h3 class="txtYellow">Rp <strong><strong>300.</strong><sup>000</sup></strong> </h3>
                                <p> <span>
                                  03:59
                                  </span> <span class="midSlice">-</span> <span>
                                  18:31
                                  </span> </p>
                                <p>
                               </p></td>
                            </tr>
                             <tr>
                              <td colspan="2"><div class="M_action">
                                <span class="sea_res_d"> <a name="image" id="show1PK-403" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-primary btn-block" onclick="goNext('PK-403','2017-08-01','1'); ">Pilih Kursi</a>
                                  <div class="loder" id="loader1PK-403" style="display:none;"><img src="ajax-loader.gif"></div>
                                   </span></div></td>
                            </tr>
                         </tbody></table></td>
                      </tr>
                      <tr id="contentPK-403" style="display:none;" class="slidingDiv slidingDivPK-403">
                       <td colspan="8" class="herro_bg" id="txtHintPK-403">
                       </td>
                     </tr>
					<tr class="hidden-sm hidden-xs">
					<td class="optname_bus"><h3>
					  Mutiara Trans Travel
					</h3>
					<p>
					  Mt - ekonomi 7 
					</p>
					<div class="sea_res_b">
						<ul class="p">
							<li> <a class="fancybox fancybox.iframe" href="bus_detail.php?detail=375&amp;from_city=417&amp;to_city=480&amp;datee=01-08-2017"><img src="http://192.168.0.104/bustiket/images/icon-video.png" alt="Video"></a>
							</li>
							<li> <a href="javascript:void(0);" onclick="slideshow('375')"><img src="http://192.168.0.104/bustiket/images/icon-image.png" alt="Image"></a> </li>
						</ul>
					</div>
					<!--  gallary image part start --></td>
					<td class="type_bus"><img src="http://192.168.0.104/bustiket/images/Travel1459223285.png"><span>Travel</span></td>
					<td class="time_bus"><div class="sea_res_b"> <span>
						<p class="default">20:30</p>
					</span> </div></td>
					<td class="next_bus"><div class="sea_res_b">
						<p>
							Pasar Rebo - Purwokerto - Purbalingga -<br>
					Banjarnegara - Wonosobo  <br>  
						  </p>
					</div></td>
				<td class="time_bus"><div class="sea_res_b"> <span>
					  <p class="default">08:00</p>
					  </span> </div></td>
					<td class="seat_bus"><!-- <span class="sub">Rp</span> -->
						<h3> 7 Kursi </h3></td>
					<td class="price_bus"><div class="rp"> <span class="sub">Rp</span>
						<h3>
							<strong>165.</strong><sup>000</sup>
						  </h3>
						<!-- <span class="subs">7</span>--> 
					</div></td>
					<td class="action_bus">
					<!--<a href="javascript:void(0)" onclick="goNext(375,2017+'-'+08+'-'+01)" title="Book Seats In this Bus"><img src="images/book_blue.gif" border="0" alt="Book Now" /></a>-->
						<span class="sea_res_d"> <a name="image" id="show375" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success y-btn" onclick="goNext(375,2017+'-'+08+'-'+01,1); ">Pilih Kursi</a>
						<div class="loder" id="loader375" style="display:none;"><img src="ajax-loader.gif"></div>
						</span></td>
					</tr>
					<tr class="hidden-lg hidden-md">
					<td colspan="8"><table width="100%" cellpadding="0" cellspacing="0" class="table tableM no-border">
					<tbody><tr>
						<td class="m_data_left"><h3>Mutiara Trans Travel </h3>
							<p> Mt - ekonomi 7 </p>
							<p> 7 Kursi </p></td>
						<td class="m_data_right"><h3 class="txtYellow">Rp <strong><strong>165.</strong><sup>000</sup></strong> </h3>
						<p> <span>
							20:30
						</span> <span class="midSlice">-</span> <span>
							08:00
						</span> </p>
							<p>
							</p></td>
						</tr>
						<tr>
							<td colspan="2"><div class="M_action">
					<!--<a href="javascript:void(0)" onclick="goNext(375,2017+'-'+08+'-'+01)" title="Book Seats In this Bus"><img src="images/book_blue.gif" border="0" alt="Book Now" /></a>-->
						<span class="sea_res_d"> <a name="image" id="show1375" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-primary btn-block" onclick="goNext(375,2017+'-'+08+'-'+01,1); ">Pilih Kursi</a>
							<div class="loder" id="loader1375" style="display:none;"><img src="ajax-loader.gif"></div>
							</span></div></td>
						</tr>
						</tbody></table></td>
						</tr>
						<tr id="content375" style="display:none;" class="slidingDiv slidingDiv375">
							<td colspan="8" class="herro_bg" id="txtHint375">
						</td>
						</tr>
</tbody></table>
</div>
</div>
<div id="table-tab2" class="tab-pane fade "> </div>
<div id="table-tab3" class="tab-pane fade"> </div>
<div id="table-tab4" class="tab-pane fade"> </div>
</div>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>