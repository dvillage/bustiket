<?php echo $__env->make('admin.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
//echo config('constant.LOGGER');
if(config('constant.LOGGER') == 'SS'){
   if(config('constant.SS_TYPE') != 'Main'){
     ?>
<?php echo $__env->make('admin.ss_ab_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
    }else{
    ?>
<?php echo $__env->make('admin.ss_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
    }
}else if(config('constant.LOGGER') == 'SP'){
    if(config('constant.SP_TYPE') != 'Main'){
     ?>
<?php echo $__env->make('admin.sp_ab_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
    }else{
    ?>
<?php echo $__env->make('admin.sp_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
    }
}else{
    ?>
<?php echo $__env->make('admin.admin_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
}
?>

<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('admin.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>