<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]--><!--[if IE 8]><html class="ie ie8"><![endif]--><!--[if IE 9]><html class="ie ie9"><![endif]--><!--[if !(IE 7) & !(IE 8) & !(IE 9)]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo isset($header['title'])? $header['title']:''?></title>  
    <link rel="icon" type="image/png" href="<?php echo e(url('favicon.png')); ?>">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400, 700" rel="stylesheet">
    
        <?php
//        dd($header);
        if (isset($header['css']) && count($header['css'])>0)
            for ($i = 0; $i < count($header['css']); $i++)
                if (strpos($header['css'][$i], "http://") !== FALSE)
                    echo '<link rel="stylesheet" type="text/css" href="' .$header['css'][$i] . '"/>';
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . url("/")."/" .  $header['css'][$i] . '"/>';
                
                
                echo '<script type="text/javascript" src="'.URL::to('assets/js/app.min.js').'"></script>';
                
        if (isset($header['js']) && count($header['js'])>0)
            for ($i = 0; $i < count($header['js']); $i++)
            {
                if (strpos($header['js'][$i], "http://") !== FALSE)
                    echo '<script type="text/javascript" src="' . $header['js'][$i] . '"></script>';
                else
                    echo '<script type="text/javascript" src="' . url("/")."/" . $header['js'][$i] . '"></script>';
            }
        ?>
    
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
        
        <!-- Start : Zendesk Chat JS -->
        
        <!-- Over : Zendesk Chat JS -->
     
        <?php if(env("APP_ENV") == "live") { ?>
            <script>
                window.$zopim || (function(d, s) {
                    var z = $zopim = function(c) {
                            z._.push(c)
                        },
                        $ = z.s =
                        d.createElement(s),
                        e = d.getElementsByTagName(s)[0];
                    z.set = function(o) {
                        z.set.
                        _.push(o)
                    };
                    z._ = [];
                    z.set._ = [];
                    $.async = !0;
                    $.setAttribute("charset", "utf-8");
                    $.src = "//v2.zopim.com/?3Jsy8D99LnweLjoUPWmmuY39C7UKRMaU";
                    z.t = +new Date;
                    $.
                    type = "text/javascript";
                    e.parentNode.insertBefore($, e)
                })(document, "script");

                $(document).ready(function(e) {
                    $(window).load(function(e) {
                      $zopim(function() {
                        $zopim.livechat.concierge.setAvatar('<?php echo e(URL::to("assets/images/logo-new.png")); ?>');
                        $zopim.livechat.window.setTitle('BUSTIKET Customer Service');
                        $zopim.livechat.theme.setColor('#228708');
                        $zopim.livechat.theme.reload(); // 226611 apply new theme settings
                        });
                    });
                }); 

            </script>
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-72843863-1"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-72843863-1');
            </script>
        <?php } ?>
<?php //  dd($header['authToken']);?>

</head>
<body>
    <script>
        function postAjax(url,data,cb){
        
        var token='<?php echo csrf_token(); ?>';
        var jdata={_token:token};
        for(var k in data){
            jdata[k]=data[k];
        }
        $.ajax({
            type:'POST',
            url:url,
            header:{
                "AuthToken":'<?php  echo isset($header['authToken'])?$header['authToken']:''; ?>',
            },
            data:jdata,
            success: function(data){
                if(typeof(data)==='object'){
                    cb(data);
                }
                else{
                    cb(data);
                }
            }
        });
    }
    </script>
    <input type="hidden" id="base_url" value="<?php echo e(url('')); ?>/" />
    <header class="site-header">
        <div class="container">
            <div class="site-header-top pull-left clearfix">
                <div class="pull-left"> 
                    <div class="clearfix space space-15"></div>
                    <p class="site-header-text text-italic">
                        <a href="https://play.google.com/store/apps/details?id=com.bustiket.app">Download Aplikasi BUSTIKET Sekarang!</a>
                    </p>
                </div>

                <div class="pull-right">
                    <nav class="navigation-header-custom clearfix have-flag">
                        <ul>
                            <li>
                                <span>0812-8000-3919</span>
                            </li>
                            <li class="flag">
                                <a href="#">
                                    <span class="sprite icon-indonesia-flag"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/#joinUS')); ?>"  id="bePartner">Partner</a>        
                            </li>
                        </ul>
                    </nav><!-- / .navigation-header-custom -->
                </div>
            </div><!-- / .site-header-top -->

            <div class="site-header-main pull-left clearfix">
                <div class="site-header-logo">
                    <a href="<?php echo e(URL::to('')); ?>"> 
                        <img src="<?php echo e(URL::to('assets/images/logo-new.png')); ?>" alt="Bus Tiket">
                    </a>
                </div>
                
                <nav class="navigation-header-main clearfix">
                    <ul>
                        <li><a href="<?php echo e(url("check-booking")); ?>">Cek Pemesanan</a></li>
                        <?php if(\Auth::guard('user')->check()){ ?>
                        <li><a href="<?php echo e(url("user/profile")); ?>"><?php echo e(\Auth::guard('user')->user()->name); ?></a></li>
                        <li><a href="<?php echo e(URL::to('logout')); ?>"><img src="<?php echo e(URL::to('assets/images/logout-icon.png')); ?>"/></a></li>
                            
                        <?php }
                        else { ?>
                            <li><a href="<?php echo e(URL::to('login')); ?>">Login</a></li>
                           
                        <?php } ?>
                        
                    </ul>
                </nav>

            </div><!-- / .site-header-main -->
        </div>

        <div class="site-header-rwd clearfix">
            <div class="nav-collapse menu-rwd collapse clearfix">
                <header class="menu-rwd-header clearfix">
                    <span class="sprite icon-indonesia-smalest"></span>
                    <h1>Login for faster booking</h1>
                    <a href="<?php echo e(URL::to('login')); ?>" class="btn btn-orange-dark btn-radius">Masuk atau Daftar</a>
                </header>

                <ul class="menu-rwd-nav">
                    <li>
                        <a href="<?php echo e(URL::to('how-to-order')); ?>">
                            <span class="sprite icon-bus-small"></span>
                            Cara Pesan
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(URL::to('how-to-order')); ?>">
                            <span class="sprite icon-carapemesanan-rwd"></span>
                            Cara Pembayaran
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(URL::to('payment-confirmation')); ?>">
                            <span class="sprite icon-konfirmasi-rwd"></span>
                            Konfirmasi Pembayaran
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(URL::to('cancel-ticket')); ?>">
                            <span class="sprite icon-cancel-rwd"></span>
                            Cancel
                        </a>
                    </li>
                    <li>
                        <a href="http://www.blog.bustiket.com/">
                            <span class="sprite icon-rwd-blog"></span>
                            Blog
                        </a>
                    </li>
                    <li>
                        <span>Menu Lainnya</span>
                    </li>
                    <li>
                        <a href="<?php echo e(URL::to('contact-us')); ?>">
                            <span class="sprite icon-cs"></span>
                            Bantuan
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(URL::to('about')); ?>">
                            <span class="sprite icon-rwd-tentang"></span>
                            Tentang Bustiket
                        </a>
                    </li>

                </ul>
            </div>

            <span class="btn-rwd-show" data-target=".nav-collapse" data-toggle="collapse">
                <span class="line"></span>
            </span>

            <div class="site-header-rwd-logo">
                <a href="<?php echo e(URL::to('')); ?>">
                    <img src="<?php echo e(URL::to('assets/images/logo-smallest.png')); ?>" alt="">
                </a>
            </div>
        </div>
    </header><!-- / .site-header -->
    <div class="site-header-sticky clearfix"> 
        <div class="container">
            <div class="site-header-logo pull-left clearfix">
                <a href="<?php echo e(URL::to('')); ?>">
                    <img src="<?php echo e(URL::to('assets/images/logo-site.png')); ?>" alt="">
                </a>
            </div>

            <nav class="navigation-header-main navigation-header-custom have-flag clearfix pull-right">
                <ul>
                    <li>
                        <span>0812-8000-3919</span>
                    </li>
                    <li>
                        <a href="<?php echo e(url("check-booking")); ?>">Cek Pemesanan</a>
                    </li>
                    <?php if(\Auth::guard('user')->check()){ ?>
                        <li><a href="<?php echo e(url("user/profile")); ?>"><?php echo e(\Auth::guard('user')->user()->name); ?></a></li>
                        <li><a href="<?php echo e(url("logout")); ?>">Logout</a></li>
                    <?php }
                    else { ?>
                        <li><a href="<?php echo e(URL::to('login')); ?>">Log In</a></li>
                    <?php } ?>
                    
                    <li class="flag">
                        <a href="#"><span class="sprite icon-indonesia-flag"></span></a>
                    </li>
                </ul>
            </nav>        
        </div>
    </div><!-- / .site-header-sticky -->