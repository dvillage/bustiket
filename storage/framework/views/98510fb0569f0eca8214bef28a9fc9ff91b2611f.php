<?php $__env->startSection('content'); ?>
<title>Page not found !</title>

<link rel="stylesheet" href="<?php echo e(URL::to('assets/css/app.min.css')); ?>">
<script type="text/javascript" src="<?php echo e(URL::to('assets/js/app.min.js')); ?>"></script>

</head>
<body>
    <div class="errore aaaa">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="content-404" style="padding:10% 0;">
                        <div class="four">
                            <h1 style="font-size:100px;">4<span style=" color: #339933; font-weight:300;">0</span>4</h1>
                            <h1 style="color: #363432; font-size:35px; font-weight: 300; font-family: "Roboto",sans-serif;"><b>OPPS!</b> The Page you requested was not found!</h1>
                        </div>
                        <h2 style="margin-top: 40px;"><a href="<?php echo e(url("/")); ?>" class="btn btn-primary" style="padding: 6px 40px;">Go to Home Page</a></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>