
<?php $__env->startSection('content'); ?>
    <main class="site-main site-main-login">
        <div class="site-main-login-mask"></div>
        <section class="section section-login">
            <div class="container">

                <div class="section-login-wrapper">
                    <header class="section-header">
                        <h1 class="section-header-title">Login ke Akun Anda</h1>        
                    </header>

                    <div class="section-content">
                        <div class="form-wrapper">
                            <?php 
//                            dd($errors);
                            if(isset($errors) && $errors->first()!=''){ ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $errors->first(); ?></strong>
                                    </div>
                            <?php }
                            $session = \Session::get("msg");
//                            dd($session);
                            if($session != "" && \General::is_json($session)){ 
                                $session = json_decode($session,true);
                                \Session::forget("msg");
                                ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $session['msg']; ?></strong>
                                    </div>
                            <?php }
                            
                            ?>
                            <form action="<?php echo e(URL::to('login')); ?>" method="POST" class="form-login">
                                <p class="form-field">
                                    <span class="form-field-label">Alamat Email</span>
                                    <input type="email" name="u_email" name="u_email">
                                </p>

                                <p class="form-field">
                                    <span class="form-field-label">Password</span>
                                    <input type="password" name="u_pass" id="u_pass">
                                    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                                </p>

                                <p class="form-field text-right forget-password">
                                    <a href="#">Lupa Password</a>
                                </p>

                                <p class="form-field form-submit">
                                    <button type="submit" class="btn btn-green btn-radius btn-fullwidth">Log In</button>
                                </p>
                                <p class="form-field info">
                                    Belum punya akun? <a href="<?php echo e(url("signup")); ?>">Daftar Sekarang</a>
                                </p>
                                <span class="clearfix space space-30"></span> 

                                <p class="form-field text-center info">
                                    Atau Masuk dengan:
                                    <span class="clearfix space space-15"></span>
                                </p>
                                <p class="form-field reset-width clearfix">
                                    <a href="<?php echo e(url('auth/facebook')); ?>" class="btn btn-line btn-line-fb pull-left">
                                        <span class="sprite icon-facebook-square"></span> Facebook
                                    </a>
                                    <a href="<?php echo e(url('auth/google')); ?>" class="btn btn-line btn-line-google pull-right">
                                        <span class="sprite icon-google-square"></span> Google
                                    </a>
                                </p>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>