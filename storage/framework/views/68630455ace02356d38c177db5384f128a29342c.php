<?php $__env->startSection('content'); ?>
<?php 
//$param = json_decode($body['param'],true);
//
//$from = explode("-", $param['from']);
//$to = explode("-", $param['to']);
//$date = date("l, d F Y",  strtotime($param['date']));
//$nop = $param['nop'];
$param = $body['param'];
$arr = explode(".", $param);
$from = $arr[0];
$to = $arr[2];
$date = date("l, d F Y",  strtotime($arr[4]));
$date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($arr[4])),date("l", strtotime($arr[4])));
$nop = $arr[5];
?>
<input type="hidden" id="param" value='<?php echo $body['param'];?>' />
<input type="hidden" id="session_id" value='<?php echo session('session_id');?>' />
<!--<input type="hidden" id="session_id" value='<?php // echo session()->getId();?>' />-->
<!--<input type="hidden" id="session_id" value='<?php // echo csrf_token();?>' />-->
<input type="hidden" id="date" value='<?php echo $arr[4];?>' />
<input type="hidden" id="mhfrom" value='<?php echo $from.'.'.$arr[1];?>' />
<input type="hidden" id="mhto" value='<?php echo $to.'.'.$arr[3];?>' />
<input type="hidden" id="mhnop" value='<?php echo $nop;?>' />
    <main class="site-main">
        <section class="section section-hero-page have-content  hidden-995" style="background-image: url('<?php echo e(URL::to('assets/images/heropages/2.jpg')); ?>') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h3 class="section-hero-page-title">Pesan Tiket Bus Kapan saja dan dari mana saja</h3>
                    <p class="section-hero-page-subtitle">Semuanya bisa Anda lakukan dengan mudah melalui BUSTIKET.com</p>
                </div>
            </div>
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="clearfix space space-30 hidden-995"></div>
                        
                        <div class="rwd-route-content clearfix showin-995">
                            <!--<form action="<?php echo e(URL::to('search-bus')); ?>" method="GET" id="search_form" class="" data-parsley-validate>-->
                            <div class="route-content-description">
                                
                                <h2 class="route-content-title"><?php echo e(ucfirst($from)); ?><span class="sprite icon-arrow-right"></span> <?php echo e(ucfirst($to)); ?></h2>

                                <div class="route-content-calendar">
                                    <span class="sprite icon-rwd-chevron-left" id="prev_date"></span>
                                    <input type="text" id="mdate" name="date" class="cari-ulang-item-value datepicker-berangkat" value="<?php echo e($date); ?>">
                                    <span class="sprite icon-rwd-chevron-right" id="next_date"></span>
                                </div>
                                
                            </div>
                                <!--</form>-->
                            <div class="route-content-rwd-sorted">
                                <ul class="clearfix">
                                    <li><a data-toggle="modal" data-target="#sort-modal">
                                        <span class="sprite-wrapper">
                                            <span class="sprite icon-sorting-down"></span>
                                        </span>
                                        Urutkan
                                        <span class="sprite-wrapper">
                                            <span class="sprite icon-arrow-small-updown"></span>
                                        </span>
                                    </a></li>

                                    <li><a data-toggle="modal" data-target="#filter-modal">
                                        <span class="sprite-wrapper">
                                            <span class="sprite icon-forma"></span>
                                        </span>
                                        Filter
                                        <span class="sprite-wrapper">
                                            <span class="sprite icon-arrow-small-updown"></span>
                                        </span>
                                    </a></li>
                                </ul>
                            </div>
                            <div class="rwd-route-search-result showin-995 clearfix" id="searchresult">
                                <center><img src="<?php echo e(url("assets/img/spiner.gif")); ?>" /></center>
                            </div><!-- / route-search-result-items -->
                            
                           
                        </div><!-- / .rwd-route-content -->
                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->

        <section class="section bg-gray section-seo hidden-995">
            <div class="container">

                <div class="row section-content">
                    <div class="col-md-12">
                        <h5 class="text-gray-placeholder nospace-top">BUSTIKET - Penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Dalam Kota Antar Propinsi (AKDP)</h5>

                        <p class="text-gray-placeholder small-ln ">BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Kota Dalam Propinsi (AKDP) yang pertama dan terpercaya di Indonesia. Dirilis sejak awal 2015, BUSTIKET.COM memberikan layanan rute perjalanan ke berbagai kota di pulau Jawa, Sumatera, Bali, Madura, dan Kalimantan dalam waktu dekat. Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.COM menyediakan berbagai informasi dalam hal jadwal keberangkatan semua rute bus perharinya, beragam pilihan bus, waktu tempuh, fasilitas, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini. Melalui BUSTIKET.COM, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer, tiket bus sudah bisa Anda dapatkan. Karena kepuasan pelanggan adalah prioritas utama kami, maka BUSTIKET.COM tidak hanya dapat diakses melalui website, tetapi juga tersedia dalam bentuk aplikasi Android untuk smartphone Anda. Dengan aplikasi terbaru kami, pemesanan tiket bus dapat dilakukan kapanpun dan dimanapun. Gak capek, ga antri, hanya di BUSTIKET.COM!</p>
                    </div>
                </div><!-- / .section-content -->

                <div class="row section-content">
                    <div class="col-md-12">
                        <div class="clearfix space space-30"></div>
                        <h5 class="text-gray-placeholder nospace-top">RUTE BUS, TRAVEL, DAN SHUTTLE TERPOPULER</h5>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder ">Jakarta - Solo</span>
                        <span class="block-content text-gray-placeholder ">Cilegon - Wonogiri</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Wonosobo</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pekalongan</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Prabumulih</span>
                        <span class="block-content text-gray-placeholder  ">Bogor - Wonogiri</span>
                        
                        <span class="clearfix space space-30"></span>
                        <a href="#" class="link-orange font-21">Kota Lainnya >></span>
                        </a>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bandung - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Solo</span>
                        <span class="block-content text-gray-placeholder  ">Bekasi - Bojonegoro</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pamekasan</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pasuruan</span>
                        <span class="block-content text-gray-placeholder  ">Bekasi - Cepu</span>
                        
                        <span class="clearfix space space-30"></span>
                        <a href="#" class="link-orange font-21">Kota Lainnya >></a>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bekasi - Bangkalan</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Palembang</span>
                        <span class="block-content text-gray-placeholder  ">Depok - Wonosari</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang - Klaten</span>
                        <span class="block-content text-gray-placeholder  ">Bogor - Ponorogo</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Bandar jaya</span>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bogor - Semarang</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang Selatan - Bangkalan</span>
                        <span class="block-content text-gray-placeholder  ">Cilegon - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Blora</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang Selatan - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang - Purwodadi</span>
                    </div>
                </div>

            </div>
        </section>

    </main><!-- / .site-main .site-main-login -->

    <!-- MODAL BEGIN -->
<div class="modal modal-flat fade" id="sort-modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-body-title">Sort</h3>

        <div class="route-filter-search-result clearfix table-responsive table-harga">
            <table class="table">
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_opsort' style="cursor: pointer;margin-left: 0px;">Operator <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item berangkat" data-dismiss="modal" id='m_boardsort' style="cursor: pointer;margin-left: 0px;">Berangkat <span  class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_dropsort' style="cursor: pointer;margin-left: 0px;">Tiba <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_ratesort' style="cursor: pointer;margin-left: 0px;">Rating <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_availsort' style="cursor: pointer;margin-left: 0px;">Sisa Kursi <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_pricesort' style="cursor: pointer;margin-left: 0px;">Harga <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                
            </table>
        </div><!-- / .route-filter-search-result -->
        
      </div>
        
            
    </div><!-- / .modal-content -->
  </div>
</div>
<!-- MODAL END -->   

<!-- MODAL BEGIN -->
<div class="modal modal-flat fade" id="filter-modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-body-title">Sort</h3>

        <div class="route-filter-search-result clearfix table-responsive table-harga" >
            <table class="table">
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Operator <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='splist'>
<!--                            <li><a href="#" class="large" data-value="1" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 1</a></li>
                            <li><a href="#" class="large" data-value="2" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 2</a></li>
                            <li><a href="#" class="large" data-value="3" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 3</a></li>
                            <li><a href="#" class="large" data-value="4" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 4</a></li>
                            <li><a href="#" class="large" data-value="5" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 5</a></li>
                            <li><a href="#" class="large" data-value="6" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 6</a></li>-->
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Tipe <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='typelist'>
                            <li><a class="large" data-value="Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Bus</a></li>
                            <li><a class="large" data-value="Travel" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Travel</a></li>
                            <li><a class="large" data-value="Shuttle" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Shuttle</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Berangkat <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='boardlist'>
                            <li><a class="large" data-value="Pagi" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Pagi  (04:00 - 11:00)</a></li>
                            <li><a class="large" data-value="Siang" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Siang (11:00 - 15:00)</a></li>
                            <li><a class="large" data-value="Sore" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Sore  (15:00 - 18:30)</a></li>
                            <li><a class="large" data-value="Malam" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Malam (18:30 - 04:00)</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="route-filter-content-item " style='margin: 0px;'>
                            Tiba <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='droplist'>
                            <li><a class="large" data-value="Pagi" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Pagi  (04:00 - 11:00)</a></li>
                            <li><a class="large" data-value="Siang" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Siang (11:00 - 15:00)</a></li>
                            <li><a class="large" data-value="Sore" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Sore  (15:00 - 18:30)</a></li>
                            <li><a class="large" data-value="Malam" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Malam (18:30 - 04:00)</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="button" class="btn btn-orange" data-dismiss="modal">Filter Search</button>
                    </td>
                </tr>
            </table>
        </div><!-- / .route-filter-search-result -->
        
      </div>
        
            
    </div><!-- / .modal-content -->
  </div>
</div>
<!-- MODAL END -->                                
    

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.site_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>