<?php $__env->startSection('content'); ?>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
    }
</script>

    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m12 l12">
          <h1>Dashboard</h1>

         
        </div>
        
      </div>

    </div>
    <!-- /Breadcrumb -->

    <!-- Stats Panels -->
    <div class="row sortable">
      <div class="col l3 m6 s12">
        <a href="<?php echo e(URL::to('admin/users-list')); ?>" class="card-panel stats-card red lighten-2 red-text text-lighten-5">
          <i class="fa fa-users"></i>
          <span class="count"><?php echo e($body['data']['users']); ?></span>
          <div class="name">Users</div>
        </a>
      </div>
      <div class="col l3 m6 s12">
        <a href="<?php echo e(URL::to('admin/ticket-list')); ?>" class="card-panel stats-card blue lighten-2 blue-text text-lighten-5">
          <i class="fa fa-ticket"></i>
          <span class="count"><?php echo e($body['data']['booked_ticket']); ?></span>
          <div class="name">Booked Tickets</div>
        </a>
      </div>
      <div class="col l3 m6 s12">
        <a href="#" class="card-panel stats-card amber lighten-2 amber-text text-lighten-5">
          <i class="fa fa-bus"></i>
          <span class="count"><?php echo e($body['data']['buses']); ?></span>
          <div class="name">Buses</div>
        </a>
      </div>
      <div class="col l3 m6 s12">
          <a href="<?php echo e(URL::to('admin/service-provider')); ?>" class="card-panel stats-card green lighten-2 green-text text-lighten-5">
          <i class="fa fa-user"></i>
          <span class="count"> <?php echo e($body['data']['spMain']); ?></span>
          <div class="name">Servicve providers</div>
        </a>
      </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>