<?php $__env->startSection('content'); ?>
<?php 
//dd
$param = $body['param'];
$arr = explode(".", $param);
$from = $arr[0];
$to = $arr[2];
$date = date("l, d F Y",  strtotime($arr[4]));
$date = \App\Models\General::getIndoMonths(date("j F Y", strtotime($arr[4])),date("l", strtotime($arr[4])));
$nop = $arr[5];

//if(isset($param['Error'])){ 
//    echo '<script>alert()<script>';
// }

?>
<script>
var data = JSON.parse('<?php echo $body['locations']; ?>');
//console.log(data);
$(document).ready(function(){
    var md=[];
    for(x in data){
        var d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
        if(data[x].type == 'city'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:'\xa0\xa0\xa0\xa0\xa0\xa0'+data[x].name};
        }
        md.push(d);
    }

    $( "#mfrom_city" ).autocomplete({
      source: md,
      select: function(event, ui){
          $('#m_from').val(ui.item.value+'-'+ui.item.id)
      }
    });
    $( "#mfrom_city_p" ).autocomplete({
      source: md,
      select: function(event, ui){
          $('#mfromP').val(ui.item.value+'-'+ui.item.id)
      }
    });
    var flag = 0;
    $('#search_form').on('submit', function(event){
        
        if(flag == 0){
            event.preventDefault();
            var from = $('#search_form').find('#s_city_from').val();
            var to = $('#search_form').find('#s_city_to').val();
            var date = $('#search_form').find('#sdate').val();
            var nop = $('#search_form').find('#nop').val();
//            console.log(from,to,date,nop);
            var f = $('#search_form').attr('action');
//            alert(f+'/'+from+'.'+to+'.'+date+'.'+nop);
//            return false;
            $('#search_form').attr('action',f+'/'+from+'.'+to+'.'+date+'.'+nop);
            flag = 1;
            $('#search_form').submit();
        }
        
    });
    
});
</script>
<input type="hidden" id="param" value='<?php echo $body['param'];?>' />
<!--<input type="hidden" id="session_id" value='<?php // echo session()->getId();?>' />-->
<input type="hidden" id="session_id" value='<?php echo session('session_id');?>' />
<!--<input type="hidden" id="session_id" value='<?php // echo csrf_token();?>' />-->
<input type="hidden" id="date" value='<?php echo $arr[4];?>' />
    <main class="site-main">
        <section class="section section-hero-page have-content  hidden-995" style="background-image: url('<?php echo e(URL::to('assets/images/heropages/2.jpg')); ?>') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h3 class="section-hero-page-title">Pesan Tiket Bus Kapan saja dan dari mana saja</h3>
                    <p class="section-hero-page-subtitle">Semuanya bisa Anda lakukan dengan mudah melalui BUSTIKET.com</p>
                </div>
            </div>
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="clearfix space space-30 hidden-995"></div>
                        <div class="alert alert-danger" id="altmsg" style="display: none;">
                            <strong>Required !</strong> <span id="msg"></span>
                        </div>
                        
                        <div class="route-content cari-ulang clearfix hidden-995" id="search_div" data-show="0" style="display: none;">
                            <form action="<?php echo e(URL::to('search-bus')); ?>" method="POST" id="search_form" class="" data-parsley-validate>
                                <input type="hidden" name='_token' name='from' value="<?php echo e(csrf_token()); ?>" />
                            <div class="route-content-description clearfix">
                                <div class="cari-ulang-item">
                                    <h3 class="cari-ulang-item-title">Kota asal</h3>
                                    <div class="cari-ulang-item-box clearfix" style="position: relative">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-bus-right"></span>
                                        </span>

                                        <div class="cari-ulang-item-content" id="cont_from">
                                            <span class="cari-ulang-item-content-title">Dari:</span>
                                            <input type="text" class="cari-ulang-item-value" id="s_from_city" value="<?php echo e($from); ?>" autocomplete="off" onkeyup="sfilterFrom();" onfocus="sfilterFrom();"  onchange="$(this).attr('onblur','ssetLoc(this);');" required="">
                                            <input type="hidden" id='s_city_from' name='from' value="<?php echo e($from.'.'.$arr[1]); ?>" />
                                        </div>
                                        <!--<ul class="cari-tiket-field-child" id='sfromfilter'>-->
                                        <ul class="autocomplete_filter" id='sfromfilter'>
                                        </ul>
                                    </div>
                                </div><!-- / .cari-ulang-item -->

                                <div class="cari-ulang-item">
                                    <h3 class="cari-ulang-item-title">Kota Tujuan</h3>
                                    <div class="cari-ulang-item-box clearfix" style="position: relative">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-bus-left"></span>
                                        </span>

                                        <div class="cari-ulang-item-content"  id="cont_to">
                                            <span class="cari-ulang-item-content-title">Ke:</span>
                                            <input type="text" class="cari-ulang-item-value" id="s_to_city" value="<?php echo e($to); ?>"  autocomplete="off" onkeyup="sfilterTo();" onfocus="sfilterTo();" onchange="$(this).attr('onblur','ssetLoc(this);');" required="">
                                            <input type="hidden" id='s_city_to' name='to' value="<?php echo e($to.'.'.$arr[3]); ?>"/>
                                            <input type="hidden" name="nop" id="nop" value="<?php echo e($nop); ?>">
                                        </div>
                                        <ul class="autocomplete_filter" id='stofilter'>
                                        </ul>
                                    </div>
                                </div><!-- / .cari-ulang-item -->

                                <div class="cari-ulang-item berangkat">
                                    <h3 class="cari-ulang-item-title">Waktu Keberangkatan</h3>
                                    <div class="cari-ulang-item-box clearfix">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-calendar"></span>
                                        </span>

                                        <div class="cari-ulang-item-content ">
                                            <span class="cari-ulang-item-content-title">Waktu Keberangkatan:</span>
                                            <input type="text" id="sdate" name="date" class="cari-ulang-item-value datepicker-berangkat" value="<?php echo e($arr[4]); ?>" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="route-content-refind">
                                <button class="btn btn-orange btn-radius">Cari Ulang</button>
                            </div>
                            </form>
                        </div>
                        
                        <div class="route-content clearfix  hidden-995" id="search_short">
                            <div class="route-content-description">
                                <h2 class="route-content-title"><?php echo e(ucfirst($from)); ?> <span class="sprite icon-arrow-right"></span> <?php echo e(ucfirst($to)); ?></h2>
                                <p class="route-content-meta">
                                    <span><?php echo e($date); ?></span>
                                    <span><?php echo e($nop); ?> Orang</span>
                                </p>
                            </div>

                            <div class="route-content-refind">
                                <button class="btn btn-line btn-line-gray btn-radius" onclick="toggleSearch();">Ganti Pencarian</button>
                            </div>
                        </div><!-- / .route-content -->

                        <div class="route-search-result hidden-995" id="search_result">
                            <div class="route-filter clearfix">
<!--                                <div class="route-filter-content">
                                    <span class="route-filter-content-header">Filter :</span>

                                    <span class="route-filter-content-item">
                                        Operator <span class="arrow-down"></span>
                                    </span>

                                    <span class="route-filter-content-item">
                                        Tipe <span class="arrow-down"></span>
                                    </span>

                                    <span class="route-filter-content-item">
                                        Berangkat <span class="arrow-down"></span>
                                    </span>

                                    <span class="route-filter-content-item">
                                        Tiba <span class="arrow-down"></span>
                                    </span>
                                </div>-->
                                <div class="route-filter-content">
                                    
                                    
                                    <span class="route-filter-content-header col-md-3" style='margin: 0px;'>Filter :</span>

                                    <div class="col-md-2">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Operator <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='splist'>
<!--                                                <li><a href="#" class="large" data-value="1" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 1</a></li>
                                                <li><a href="#" class="large" data-value="2" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 2</a></li>
                                                <li><a href="#" class="large" data-value="3" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 3</a></li>
                                                <li><a href="#" class="large" data-value="4" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 4</a></li>
                                                <li><a href="#" class="large" data-value="5" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 5</a></li>
                                                <li><a href="#" class="large" data-value="6" tabIndex="-1"><input type="checkbox"/>&nbsp;Option 6</a></li>-->
                                        </ul>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Tipe <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='typelist'>
                                                <li><a class="large" data-value="Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Bus</a></li>
                                                <li><a class="large" data-value="Travel" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Travel</a></li>
                                                <li><a class="large" data-value="Shuttle" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Shuttle</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Berangkat <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='boardlist'>
                                            <li><a class="large" data-value="Pagi" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Pagi  (04:00 - 11:00)</a></li>
                                            <li><a class="large" data-value="Siang" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Siang (11:00 - 15:00)</a></li>
                                            <li><a class="large" data-value="Sore" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Sore  (15:00 - 18:30)</a></li>
                                            <li><a class="large" data-value="Malam" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Malam (18:30 - 04:00)</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Tiba <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='droplist'>
                                            <li><a class="large" data-value="Pagi" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Pagi  (04:00 - 11:00)</a></li>
                                            <li><a class="large" data-value="Siang" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Siang (11:00 - 15:00)</a></li>
                                            <li><a class="large" data-value="Sore" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Sore  (15:00 - 18:30)</a></li>
                                            <li><a class="large" data-value="Malam" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Malam (18:30 - 04:00)</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="route-filter-refind">
                                    <button class="btn btn-line btn-line-gray btn-radius" id="more_date"><img src="<?php echo e(url("assets/img/spiner.gif")); ?>" id="spin" style="display: block;height: 25px;width:25px;" /></button>
                                </div>

                                <div class="route-filter-cari-tanggal" id="moredateprice" style="display:none;">
                                    <div class="" id="price_15">

                                    </div>
                                </div>
                            </div><!-- / .route-filter -->

                            <div class="route-filter-search-result clearfix">
                                <a class="route-filter-search-result-item" id='opsort' style="cursor: pointer;">Operator <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item berangkat" id='boardsort' style="cursor: pointer;">Berangkat <span  class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item" id='dropsort' style="cursor: pointer;">Tiba <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item" id='ratesort' style="cursor: pointer;">Rating <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item" id='availsort' style="cursor: pointer;">Sisa Kursi <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item" id='pricesort' style="cursor: pointer;">Harga <span class="arrow-updown"></span></a>
                            </div><!-- / .route-filter-search-result -->

                            <div class="route-search-result-items" id='searchresult'>
                                <center><img src="<?php echo e(url("assets/img/spiner.gif")); ?>" /></center>
                            </div><!-- / route-search-result-items -->
                        </div><!-- / .route-search-result -->
                        
                        
                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->
    </main><!-- / .site-main .site-main-login -->
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.site_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>