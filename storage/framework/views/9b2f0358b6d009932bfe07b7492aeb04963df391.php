
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BusTiket Admin Panel Login Page</title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/nanoscroller.css')); ?>" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/font-awesome.min.css')); ?>" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/material-design-icons.min.css')); ?>" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/ionicons.min.css')); ?>" />

  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/main.min.css')); ?>" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('assets/css/light-green.min.css')); ?>" />

  <!--[if lt IE 9]>
    <script src="assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
</head>

<body>

  <section id="sign-in">

    <!-- Background Bubbles -->
    <canvas id="bubble-canvas"></canvas>
    <!-- /Background Bubbles -->

    <!-- Sign In Form -->
    <?php 
    $url = $body['type'] == 'SS' ? 'ss/login' : ( $body['type'] == 'SP' ? 'sp/login' : 'admin/login' );
    ?>
        <form action="<?php echo e(URL::to($url)); ?>" method='POST'>
      <div class="row links">
        <div class="col s12 logo" align='Center' >
                <img src="<?php echo e(URL::to('assets/img/lohoggg.png')); ?>" style='height: 75px;width: 150px;' alt=""/>
            </div>
        
      </div>

      <div class="card-panel clearfix">

        <!--<div class="row">-->
            <!--<div class="col">-->
            <div class="title pb-10" align="center" >
                <h3 style="margin: 0px"><?php echo e($body['logger']); ?> Login</h3>
            </div>
            
            <?php if(isset($errors) && $errors->first()!=''){ ?>
            <div class="alert" style="text-align: center;">
                <strong><?php echo $errors->first(); ?></strong>
            </div>
            <?php } ?>
            <h1 style="margin: 0px;"></h1>
            <!--</div>-->
        <!--</div>-->

        <!-- Username -->
        <div class="input-field">
          <i class="fa fa-user prefix"></i>
          <input id="uname" name="uname" type="text" class="validate">
          <label for="uname">Username</label>
        </div>
        <!-- /Username -->

        <!-- Password -->
        <div class="input-field">
          <i class="fa fa-unlock-alt prefix"></i>
          <input id="password" name="password" type="password" class="validate">
          <label for="password">Password</label>
          <input type="hidden" id='_token' name='_token' value='<?php echo e(csrf_token()); ?>'>
        </div>
        <!-- /Password -->

        <div class="switch">
          <label>
            <input type="checkbox" id="remember" name="remember" checked />
            <span class="lever"></span>
            Remember
          </label>
        </div>

        <button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover">Sign In</button>
      </div>

    </form>
    <!-- /Sign In Form -->

  </section>

  <!-- jQuery -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/jquery.min.js')); ?>"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/jquery.requestAnimationFrame.min.js')); ?>"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/jquery.nanoscroller.min.js')); ?>"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/materialize.min.js')); ?>"></script>

  <!-- Sortable -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/Sortable.min.js')); ?>"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?php echo e(URL::to('assets/js/main.min.js')); ?>"></script>

</body>

</html>