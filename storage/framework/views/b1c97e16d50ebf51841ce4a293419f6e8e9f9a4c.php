        <?php if(!isset($footer['flag'])){ ?>
        <?php $footer_fav_routes = \General::footer_fav_routes();
?>
            <section class="section bg-gray section-seo hidden-995">
                <div class="container container-two">

                    <div class="row section-content">
                        <div class="col-md-12">
                            <h5 class="text-gray-placeholder nospace-top">BUSTIKET - Penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Dalam Kota Antar Propinsi (AKDP)</h5>

                            <p class="text-gray-placeholder small-ln ">BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Kota Dalam Propinsi (AKDP) yang pertama dan terpercaya di Indonesia. Dirilis sejak awal 2015, BUSTIKET.COM memberikan layanan rute perjalanan ke berbagai kota di pulau Jawa, Sumatera, Bali, Madura, dan Kalimantan dalam waktu dekat. Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.COM menyediakan berbagai informasi dalam hal jadwal keberangkatan semua rute bus perharinya, beragam pilihan bus, waktu tempuh, fasilitas, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini. Melalui BUSTIKET.COM, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer, tiket bus sudah bisa Anda dapatkan. Karena kepuasan pelanggan adalah prioritas utama kami, maka BUSTIKET.COM tidak hanya dapat diakses melalui website, tetapi juga tersedia dalam bentuk aplikasi Android untuk smartphone Anda. Dengan aplikasi terbaru kami, pemesanan tiket bus dapat dilakukan kapanpun dan dimanapun. Gak capek, ga antri, hanya di BUSTIKET.COM!</p>
                        </div>
                    </div><!-- / .section-content -->

                    <div class="row section-content">
                        <div class="col-md-12">
                            <div class="clearfix space space-30"></div>
                            <h5 class="text-gray-placeholder nospace-top">RUTE BUS, TRAVEL, DAN SHUTTLE TERPOPULER</h5>
                        </div>

                        <div class="col-md-3 item-ref-kota" id="favlist1">
                            <?php 
                                if(isset($footer_fav_routes)){
                                    foreach($footer_fav_routes as $key => $routes){ 
                                        if($key % 4 == 0){ ?>
                                            <a href='<?php echo e(URL::to('search-bus')); ?>/<?php echo e($routes['from_city']['name']); ?>.C.<?php echo e($routes['to_city']['name']); ?>.C.<?php echo e(date('Y-m-d',strtotime('+3 day'))); ?>.1'><span class="block-content text-gray-placeholder "><?php echo e($routes['from_city']['name']); ?> - <?php echo e($routes['to_city']['name']); ?></span></a>
                           <?php }}}?>

                            <span class="clearfix space space-30"></span>
                            <a href="<?php echo e(URL::to('city-list')); ?>" class="link-orange font-21">Kota Lainnya >></span>
                            </a>
                        </div>

                        <div class="col-md-3 item-ref-kota" id="favlist2">
                            <?php if(isset($footer_fav_routes)){
                                    foreach($footer_fav_routes as $key => $routes){ 
                                        if($key % 4 == 1){ ?>
                                        <a href='<?php echo e(URL::to('search-bus')); ?>/<?php echo e($routes['from_city']['name']); ?>.C.<?php echo e($routes['to_city']['name']); ?>.C.<?php echo e(date('Y-m-d',strtotime('+3 day'))); ?>.1'><span class="block-content text-gray-placeholder "><?php echo e($routes['from_city']['name']); ?> - <?php echo e($routes['to_city']['name']); ?></span></a>
                            <?php }}}?>

                            <span class="clearfix space space-30"></span>
                            <a href="<?php echo e(URL::to('all-route')); ?>" class="link-orange font-21">Rute Lainnya >></a>
                        </div>

                        <div class="col-md-3 item-ref-kota" id="favlist3">
                            <?php if(isset($footer_fav_routes)){
                                    foreach($footer_fav_routes as $key => $routes){ 
                                        if($key % 4 == 2){ ?>
                                            <a href='<?php echo e(URL::to('search-bus')); ?>/<?php echo e($routes['from_city']['name']); ?>.C.<?php echo e($routes['to_city']['name']); ?>.C.<?php echo e(date('Y-m-d',strtotime('+3 day'))); ?>.1'><span class="block-content text-gray-placeholder "><?php echo e($routes['from_city']['name']); ?> - <?php echo e($routes['to_city']['name']); ?></span></a>
                            <?php }}}?>

                        </div>

                        <div class="col-md-3 item-ref-kota" id="favlist4">
                            <?php if(isset($footer_fav_routes)){
                                    foreach($footer_fav_routes as $key => $routes){ 
                                        if($key % 4 == 3){ ?>
                                            <a href='<?php echo e(URL::to('search-bus')); ?>/<?php echo e($routes['from_city']['name']); ?>.C.<?php echo e($routes['to_city']['name']); ?>.C.<?php echo e(date('Y-m-d',strtotime('+3 day'))); ?>.1'><span class="block-content text-gray-placeholder "><?php echo e($routes['from_city']['name']); ?> - <?php echo e($routes['to_city']['name']); ?></span></a>
                           <?php }}}?>

                        </div>
                    </div>

                </div>
            </section>
        <?php }?>
        
    </main>

    <footer class="site-footer ">
        <div class="container ">
            <div class="row site-footer-widget">
                <div class="col-md-12 showin-995">
                    <div class="widget widget-text widget-rwd text-center">
                        <h3 class="widget-title"><a href='https://play.google.com/store/apps/details?id=com.bustiket.app' style="color:white;">Download Aplikasi <i>BUSTIKET</i> Sekarang!</a></h3>
                        
                        <div class="text-center">
                            <img src="<?php echo e(URL::to('assets/images/logo-footer-new.png')); ?>" alt="">

                            <div class="cta-phone clearfix">
                                <span class="sprite icon-telpon pull-left"></span>
                                <p>Customer Service</p>
                                <span class="font-18">0812-8000-3919</span>
                            </div>

                            <ul class="list-socmed-rwd">
                                <!--<li><a href=""><span class="sprite icon-twitter-mobile"></span></a></li>-->
                                <li><a href="https://twitter.com/BUSTIKET"><span class="sprite icon-twitter"></span></a></li>
                                <!--<li><a href=""><span class="sprite icon-facebook-moble"></span></a></li>-->
                                <li><a href="https://www.facebook.com/BUSTIKET/"><span class="sprite icon-facebook"></span></a></li>
                                <!--<li><a href=""><span class="sprite icon-instagram-mobile"></span></a></li>-->
                                <li><a href="https://www.instagram.com/BUSTIKET/"><span class="sprite icon-instagram"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 hidden-995">
                    <div class="widget widget-payment">
                        <h3 class="widget-title">Kami menerima pembayaran dari:</h3>

                        <ul class="list-available-payment">
                            <li><span class="sprite icon-mastercard"></span></li>
                            <li><span class="sprite icon-visa"></span></li>
                            <li><span class="sprite icon-bankbca"></span></li>
                            <li><span class="sprite icon-klikbca"></span></li>
                            <li><span class="sprite icon-bri"></span></li>
                            <li><span class="sprite icon-mandiri"></span></li>
                            <li><span class="sprite icon-mandiriclickpay"></span></li>
                            <li><span class="sprite icon-alfamart"></span></li>
                            <li><span class="sprite icon-wallet"></span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 hidden-995">
                    <div class="widget widget-list">
                        <h3 class="widget-title">BUSTIKET</h3>

                        <ul>
                            <li><a href="<?php echo e(URL::to('about')); ?>">Tentang BUSTIKET</a></li>
                            <li><a href="<?php echo e(URL::to('career')); ?>">Karir</a></li>
                            <li><a href="http://www.blog.bustiket.com/">Blog</a></li>
                            <li><a href="<?php echo e(URL::to('privacy-policy')); ?>">Kebijakan Privasi</a></li>
                            <li><a href="<?php echo e(URL::to('term-condition')); ?>">Syarat & Ketentuan</a></li>
                            <li><a href="<?php echo e(URL::to('partner')); ?>">Partner</a></li>
                        </ul>
                    </div><!-- / .widget-list -->
                </div>

                <div class="col-md-3 hidden-995">
                    <div class="widget widget-list">
                        <h3 class="widget-title">BANTUAN</h3>

                        <ul>
                            <li><a href="<?php echo e(URL::to('how-to-order')); ?>">Cara Pemesanan</a></li>
                            <li><a href="<?php echo e(URL::to('check-booking')); ?>">Cek Pemesanan</a></li>
                            <li><a href="<?php echo e(URL::to('payment-confirmation')); ?>">Konfirmasi Pembayaran</a></li>
                            <li><a href="<?php echo e(URL::to('cancel-ticket')); ?>">Cancel Tiket</a></li>
                            <li><a href="<?php echo e(URL::to('faq')); ?>">FAQ</a></li>
                            <li><a href="<?php echo e(URL::to('contact-us')); ?>">Hubungi Kami</a></li>
                        </ul>
                    </div><!-- / .widget-list -->
                </div>

                <div class="col-md-3 hidden-995">
                    <div class="widget widget-list">
                        <h3 class="widget-title">Hubungi Customer Service</h3>

                        <ul>
                            <li><span class="text-orange"><b>0812-8000-3919</b></span></li>
                            <li><a href="mailto:cs@bustiket.com" class="link-orange"><b>cs@bustiket.com</b></a></li>
                        </ul>
                    </div><!-- / .widget-list -->
                </div>

                <div class="col-md-3 hidden-995">
                    <div class="widget widget-download">
                        <h3 class="widget-title">DOWNLOAD BUSTIKET APP</h3>

                        <figure class="alignleft">
                            <a href="https://play.google.com/store/apps/details?id=com.bustiket.app">
                                <span class="sprite icon-google-play-footer"></span>
                            </a>
                        </figure>

                        <ul class="list-socmed">
                            <li><a href="https://www.facebook.com/BUSTIKET/"><span class="sprite icon-facebook"></span></a></li>
                            <li><a href="https://twitter.com/BUSTIKET"><span class="sprite icon-twitter"></span></a></li>
                            <li><a href="https://www.instagram.com/BUSTIKET/"><span class="sprite icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="clearfix"></div>
                <div class="col-md-4 hidden-995">
                    <div class="widget widget-text">
                        <div class="text-center">
                            <img src="<?php echo e(URL::to('assets/images/logo-footer-2.png')); ?>" alt="">
                            
                            <div class="clearfix space space-15"></div>
                            <p>&copy; Copyright 2017 <a href="#">BUSTIKET.COM</a></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row site-footer-content hidden-995">
                <p class="copyright-proudly">
                    <span class="sprite icon-indonesia-flag-small"></span>
                    Proudly Made In Indonesia
                </p>
            </div>
        </div>
    </footer><!-- / .site-footer .site-footer-login -->
    
        <?php
        if (isset($footer['css'])  && count($footer['css']) > 0)
            for ($i = 0; $i < count($footer['css']); $i++)
                if (strpos($footer['css'][$i], "http://") !== FALSE)
                    echo '<link rel="stylesheet" type="text/css" href="' .$footer['css'][$i] . '"/>';
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . url('/')."/" .  $footer['css'][$i] . '"/>';
                
        if (isset($footer['js']) && count($footer['js']) > 0)
            for ($i = 0; $i < count($footer['js']); $i++)
            {
                if (strpos($footer['js'][$i], "http://") !== FALSE)
                    echo '<script type="text/javascript" src="' . $footer['js'][$i] . '"></script>';
                else
                    echo '<script type="text/javascript" src="' . url('/')."/" .  $footer['js'][$i] . '"></script>';
            }
        ?>

    <script type="text/javascript" src="<?php echo e(URL::to('assets/js/scripts.js')); ?>"></script>
   
    <!--<script type="text/javascript" src="<?php echo e(URL::to('assets/js/common.js')); ?>"></script>-->
</body>
</html>
        