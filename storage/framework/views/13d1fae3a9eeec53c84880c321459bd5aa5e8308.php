 <!--
  Yay Sidebar
  Options [you can use all of theme classnames]:
    .yay-hide-to-small         - no hide menu, just set it small with big icons
    .yay-static                - stop using fixed sidebar (will scroll with content)
    .yay-gestures              - to show and hide menu using gesture swipes
    .yay-light                 - light color scheme
    .yay-hide-on-content-click - hide menu on content click

  Effects [you can use one of these classnames]:
    .yay-overlay  - overlay content
    .yay-push     - push content to right
    .yay-shrink   - shrink content width
-->

  <aside class="yaybar yay-shrink yay-hide-to-small yay-gestures">

    <div class="top">
      <div>
        <!-- Sidebar toggle -->
        <a href="#" class="yay-toggle">
          <div class="burg1"></div>
          <div class="burg2"></div>
          <div class="burg3"></div>
        </a>
        <!-- Sidebar toggle -->

        <!-- Logo -->
        <a href="#!" class="brand-logo">
          <img src="<?php echo e(URL::to('assets/img/bus_image/logo2.png')); ?>" alt="Con">
        </a>
        <!-- /Logo -->
      </div>
    </div>


    <div class="nano">
        <div class="nano-content">

            <ul>

                <li class="label">Menu</li>

                <li id='Dashboard'>
                    <a href="<?php echo e(URL::to('admin/dashboard')); ?>" class="waves-effect waves-blue"><i class="fa fa-dashboard"></i> Dashboards</a>
                </li>
<!--                <li class="active">
                  <a href="<?php echo e(URL::to('admin/dashboard')); ?>" class="yay-sub-toggle waves-effect waves-blue" ><i class="fa fa-dashboard"></i> Dashboards</a>
                </li>-->
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-users"></i> Users<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='list'>
                            <a href="<?php echo e(URL::to('admin/users-list')); ?>" class="waves-effect waves-blue"> List </a>
                        </li>
                        
                    </ul>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-ticket"></i> Ticket<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='tkt-book'>
                            <a href="<?php echo e(URL::to('admin/ticket-book-new')); ?>" class="waves-effect waves-blue"> Book New</a>
                        </li>
                        <li id='tkt-list'>
                            <a href="<?php echo e(URL::to('admin/ticket-list')); ?>" class="waves-effect waves-blue"> List</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-device-dvr"></i> Service Provider<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='sp-main'>
                            <a href="<?php echo e(URL::to('admin/service-provider')); ?>" class="waves-effect waves-blue"> Service Provider Main</a>
                        </li>
                        <li id='sp-A'>
                            <a href="<?php echo e(URL::to('admin/service-provider/A')); ?>" class="waves-effect waves-blue"> Service Provider - A</a>
                        </li>
                        <li id='sp-B'>
                            <a href="<?php echo e(URL::to('admin/service-provider/B')); ?>" class="waves-effect waves-blue"> Service Provider - B</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-globe"></i> Locality<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='loc-Province'>
                            <a href="<?php echo e(URL::to('admin/locality/Province')); ?>" class="waves-effect waves-blue"> Province</a>
                        </li>
                        <li id='loc-City'>
                            <a href="<?php echo e(URL::to('admin/locality/City')); ?>" class="waves-effect waves-blue"> City</a>
                        </li>
                        <li id='loc-District'>
                            <a href="<?php echo e(URL::to('admin/locality/District')); ?>" class="waves-effect waves-blue"> District</a>
                        </li>
                        <li id='loc-Terminal'>
                            <a href="<?php echo e(URL::to('admin/locality/Terminal')); ?>" class="waves-effect waves-blue"> Terminal</a>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-users"></i> Seat Seller<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='ss-Main'>
                            <a href="<?php echo e(URL::to('admin/seatseller/Main')); ?>" class="waves-effect waves-blue"> Seat Seller</a>
                        </li>
                        <li id='ss-A'>
                            <a href="<?php echo e(URL::to('admin/seatseller/A')); ?>" class="waves-effect waves-blue"> Seat Seller - A</a>
                        </li>
                        <li id='ss-B'>
                            <a href="<?php echo e(URL::to('admin/seatseller/B')); ?>" class="waves-effect waves-blue"> Seat Seller - B</a>
                        </li>
                        <li id='ss-D'>
                            <a href="<?php echo e(URL::to('admin/seatseller/D')); ?>" class="waves-effect waves-blue"> Seat Seller Deposit Request</a>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-navigation-cancel"></i> Cancel Ticket<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='cp'>
                            <a href="<?php echo e(URL::to('admin/cancel-ticket-policy')); ?>" class="waves-effect waves-blue"> Cancellation Policies</a>
                        </li>
<!--                        <li id='pwo'>
                            <a href="<?php echo e(URL::to('admin/pariwisata-order')); ?>" class="waves-effect waves-blue"> Pariwisata Order</a>
                        </li>-->
                    </ul>
                </li>
                
                <li id='Banner'>
                    <a href="<?php echo e(URL::to('admin/banner')); ?>" class="waves-effect waves-blue"><i class="fa fa-image"></i> Banner</a>
                </li>
                <li id='driver'>
                    <a href="<?php echo e(URL::to('admin/drivers-list')); ?>" class="waves-effect waves-blue"><i class="mdi-action-perm-identity"></i> Drivers </a>
                </li>
                <li id='amenity'>
                    <a href="<?php echo e(URL::to('admin/amenities-list')); ?>" class="waves-effect waves-blue"><i class="fa fa-yelp"></i> Amenities </a>
                </li>
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-bus"></i> Pariwisata<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='pw'>
                            <a href="<?php echo e(URL::to('admin/pariwisata-list')); ?>" class="waves-effect waves-blue"> Pariwisata</a>
                        </li>
                        <li id='pwo'>
                            <a href="<?php echo e(URL::to('admin/pariwisata-order')); ?>" class="waves-effect waves-blue"> Pariwisata Order</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-action-credit-card"></i> Busway <span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='card'>
                            <a href="<?php echo e(URL::to('admin/card-list')); ?>" class="waves-effect waves-blue"> Commuter Management</a>
                        </li>
                        <li id='crr'>
                            <a href="<?php echo e(URL::to('admin/couriers-list')); ?>" class="waves-effect waves-blue"> Courier Management</a>
                        </li>
                    </ul>
                </li>
                
                <li id='fav_route'>
                    <a href="<?php echo e(URL::to('admin/favorite-route')); ?>" class="waves-effect waves-blue"><i class="mdi-av-repeat"></i> Favorite Routes </a>
                </li>
                
                <li id='coupon'>
                    <a href="<?php echo e(URL::to('admin/coupon-code')); ?>" class="waves-effect waves-blue"><i class="mdi-action-receipt"></i> Coupon Code</a>
                </li>
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-action-assignment-ind"></i> Admin <span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='admin-comm'>
                            <a href="<?php echo e(URL::to('admin/commission-management')); ?>" class="waves-effect waves-blue"> Commission Management</a>
                        </li>
                        <li id='admin-bulk-sms'>
                            <a href="<?php echo e(URL::to('admin/bulk-sms')); ?>" class="waves-effect waves-blue"> Bulk SMS</a>
                        </li>
                    </ul>
                </li>
                <li id='admin-push-notify'>
                        <a href="<?php echo e(URL::to('admin/push-notification')); ?>" class="waves-effect waves-blue"><i class="fa fa-newspaper-o"></i> Push Notification</a>
                </li>
                <li id='admin-testimonial'>
                        <a href="<?php echo e(URL::to('admin/testimonial-list')); ?>" class="waves-effect waves-blue"><i class="fa fa-quote-left"></i>Testimonial</a>
                </li>
                
                <li id='reporting'>
                    <a href="<?php echo e(URL::to('admin/reports')); ?>" class="waves-effect waves-blue"><i class="fa fa-bar-chart"></i> Reporting</a>
                </li>
                
                <li id='settings'>
                    <a href="<?php echo e(URL::to('admin/settings')); ?>" class="waves-effect waves-blue"><i class="mdi-action-settings"></i> Settings</a>
                </li>
                
            </ul>

        </div>
    </div>
  </aside>
  <!-- /Yay Sidebar -->


  <!-- Main Content -->
  <section class="content-wrap">
