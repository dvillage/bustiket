<input type="hidden" id="total_person" value="<?php echo e($nop); ?>" />
<?php 
//dd($buses);
    $splist = [];
//    echo count($buses);
foreach ($buses as $key => $bus) {
    
    if($key !== 'roda' && $key !== 'lorena'){
    ?>
<div>
    <div class="route-item clearfix" data-bus_id="<?php echo e($bus['bus']['id']); ?>">
        <div class="route-subitem clearfix">
            <div class="route-subitem-content">
                <div class="route-subitem-box">
                    <?php $bimg = explode('/', $bus['bus']['sp']['avatar']);
                          if($bimg[count($bimg)-1] == 'default.png'){ ?>
                                <figure class="route-item-ekspedisi" style="width: 111px;height: 0px;margin-top: 20px;">
                                    <img src="<?php echo e($bus['bus']['sp']['avatar']); ?>" alt="" height="25px" >
                                </figure>
                    <?php }else{ ?>
                                <figure class="route-item-ekspedisi" style="width: 111px;height: 60px">
                                    <img src="<?php echo e($bus['bus']['sp']['avatar']); ?>" alt="" height="60px" width="60px" style="margin-top: 21px;">
                                </figure>
                    <?php } ?>
                    
                </div>
                <div class="route-subitem-box ekspedisi">
                    <span class="route-item-title sp_name"><?php echo e($bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']); ?></span>
                    <?php $splist[] = $bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']; ?>
                    <span class="route-item-subtitle"><?php echo e($bus['bus']['name']); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box berangkat">
                    <span class="route-item-title board_time"><?php echo explode(" ", $bus['routes'][0]['boarding_time'])[1]; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap($bus['routes'][0]['from_terminal']['name'], 25, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box arrow">
                    <span class="sprite icon-arrow-right"></span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box tiba">
                    <span class="route-item-title drop_time"><?php echo explode(" ", $bus['routes'][0]['droping_time'])[1]; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap($bus['routes'][0]['to_terminal']['name'], 25, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box rate">
                    <span class="route-item-title rating">4/5</span>
                    <span class="route-item-subtitle">(5 Ratings)</span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box seat">
                    <span class="sprite icon-seat"></span>
                    <span class="route-item-subtitle "><?php echo e($bus['bus']['total_seats']); ?> Kursi</span>
                    <span class="hidden avail_seat"><?php echo e($bus['bus']['total_seats']); ?></span>
                </div><!-- / .route-subitem-box -->

            </div><!-- / .route-subitem-content -->

            <ul class="route-subitem-content list-tab" role="tablist">
                <li class="route-subitem-box cta ">
                    <a href="#fasi-<?php echo e($bus['bus']['id']); ?>" class="tab_click" data-bus_id="<?php echo e($bus['bus']['id']); ?>" role="tab" data-toggle="tab"  aria-controls="fasilitas-one">fasilitas</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#foto-<?php echo e($bus['bus']['id']); ?>" class="tab_click" data-bus_id="<?php echo e($bus['bus']['id']); ?>" role="tab" data-toggle="tab" aria-controls="foto-one">Foto</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#rute-<?php echo e($bus['bus']['id']); ?>" class="tab_click" data-bus_id="<?php echo e($bus['bus']['id']); ?>" role="tab" data-toggle="tab" aria-controls="rute-one">Rute</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#info-<?php echo e($bus['bus']['id']); ?>" class="tab_click" data-bus_id="<?php echo e($bus['bus']['id']); ?>" role="tab" data-toggle="tab" aria-controls="info-one">Info</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#">Rating</a>
                </li><!-- / .route-subitem-box -->

            </ul><!-- / .route-subitem-content -->
        </div>

        <div class="route-items-eksekusi">
            <span class="pricing" id="pricing_<?php echo e($bus['bus']['id']); ?>">Rp <?php echo \General::number_format($bus['bus']['price'],3)?></span>
            <input type="hidden" id="sp_id_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e($bus['bus']['sp']['id']); ?>" />
            <input type="hidden" id="sp_name_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e($bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']); ?>" />
            <input type="hidden" id="bus_name_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e($bus['bus']['name']); ?>" />
            <input type="hidden" id="board_points_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e(json_encode($board_points[$bus['bus']['id']])); ?>" />
            <input type="hidden" id="drop_points_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e(json_encode($drop_points[$bus['bus']['id']])); ?>" />
            <input type="hidden" id="fare_<?php echo e($bus['bus']['id']); ?>" value="<?php echo e($bus['bus']['price']); ?>" />
            <input type="hidden" id="fare_format_<?php echo e($bus['bus']['id']); ?>" class="fare" value="<?php echo e(\General::number_format($bus['bus']['price'],3)); ?>" />
            <input type='hidden' id='type_<?php echo e($bus['bus']['id']); ?>' class='bustype' value='Bus' />
            <span class="btn btn-radius btn-orange view-seats" id="bus_<?php echo e($bus['bus']['id']); ?>" data-bus_id="<?php echo e($bus['bus']['id']); ?>" data-map="0" style="margin-top: 30px;">Pilih Kursi</span>
            <div class="loder" id="loader_<?php echo e($bus['bus']['id']); ?>" style="display: none;"><img src="<?php echo e(URL::to('assets/img/ajax-loader.gif')); ?>"></div>
        </div>
        
        <div class="route-items-detail-cta tab-content" id="rdetail_<?php echo e($bus['bus']['id']); ?>">
            <div class="tab-pane fade detail-cta-box clearfix" id="fasi-<?php echo e($bus['bus']['id']); ?>"  role="tabpanel">
                <div class="route-items-detail-cta-container">
                    <ul class="list-cta-fasilitas">
                        <?php
//                        dd($bus['bus']);
                        foreach($bus['bus']['amenities'] as $am){
                            $file = URL::to('assets/uploads/amenities').'/'.$am['aimg'];
                            $file_check = config('constant.UPLOAD_AMENITY_DIR_PATH').$am['aimg'];
                            $im = '';
                            if(file_exists($file_check) && $am['aimg']!= ''){
                                $im =  '<img src="'.$file.'" class="" style="height:25px;width:25px"/>';
                            }
                        ?>
                        <li>
                            <span class="sprite-wrapper">
                                <span class="sprite "><?php echo $im ?></span>
                            </span>
                            <span class="title"><?php echo e($am['aname']); ?></span>
                        </li>
                        <?php } ?>
                     </ul>
                </div>
            </div><!-- / .detail-cta-box -->

            <div class="tab-pane fade detail-cta-box clearfix" role="tabpanel" id="foto-<?php echo e($bus['bus']['id']); ?>" >
                <div class="route-items-detail-cta-container">
                    <div class="foto-slider owl-carousel">
                        <?php 
                            
                            $other = $bus['bus']['images'];
                            if($other != ''){
                            $other = explode(',',$other);
                            foreach($other as $o){
                                $file = URL::to('assets/uploads/bus_images').'/'.$o;
                                $file_check = config('constant.BUS_IMAGE_PATH').$o;
                                if(file_exists($file_check) && $o!= ''){
                                    ?>
                        <div class="foto-item">
                            <img src="<?php echo e($file); ?>" height="124" width="222" alt="">
                        </div>
                        <?php
                                }
                            }

                            }
                         ?>
                        
                    </div>
                </div>
            </div><!-- / .detail-cta-box -->

            <div class="tab-pane fade detail-cta-box clearfix" role="tabpanel" id="rute-<?php echo e($bus['bus']['id']); ?>" >
                <div class="route-items-detail-cta-container rute-container">
                    <div class="cta-rute pickup">
                        <h5 class="cta-rute-title">Pickup</h5>
                        <?php 
                        $ba = [];
                        foreach($bus['routes'] as $rt){ 
                            if(!in_array($rt['from_terminal_id'], $ba)){
                                array_push($ba, $rt['from_terminal_id']);
                            ?>
                        <span class="cta-rute-pick-off">
                            <span><?php echo e(date('H:i',strtotime($rt['boarding_time']))); ?></span>
                            <span><?php echo e($rt['from_terminal']['name'].','.$rt['from_city']['name']); ?></span> <br>
                        </span>
                        <?php 
                            }
                            } ?>
                    </div><!-- / .cta-rute .pickup -->

                    <div class="cta-rute pickup">
                        <h5 class="cta-rute-title">Via</h5>
                        <?php
                        
                        foreach($bus['routes'] as $rt){ ?>
                        <span class="cta-rute-desc"><?php echo e($rt['route']); ?></span><br>
                        <?php } ?>
                    </div><!-- / .cta-rute -->

                    <div class="cta-rute pickup">
                        <h5 class="cta-rute-title">Drop Off</h5>
                        <?php 
                        $bt = [];
                        foreach($bus['routes'] as $rt){
                            if(!in_array($rt['to_terminal_id'], $bt)){
                                array_push($bt, $rt['to_terminal_id']);
                            ?>
                        <span class="cta-rute-pick-off">
                            <span><?php echo e(date('H:i',strtotime($rt['droping_time']))); ?></span> :
                            <span><?php echo e($rt['to_terminal']['name'].','.$rt['to_city']['name']); ?></span> <br>
                        </span>
                        <?php 
                            }
                            } ?>
                    </div><!-- / .cta-rute .pickup -->

                </div>
            </div><!-- / .detail-cta-box -->

            <div class="tab-pane fade detail-cta-box clearfix " role="tabpanel" id="info-<?php echo e($bus['bus']['id']); ?>">
                <div class="route-items-detail-cta-container">

                    <?php echo html_entity_decode($bus['bus']['tnc']) ?>
                </div>
            </div><!-- / .detail-cta-box -->

        </div>
        
    </div><!-- / .route-item -->
    <div class="route-item-detail clearfix">
        
    </div>
    </div>
    
<?php }else{
//    echo $key;
//    $bus['sp']['avatar'] = $bus['sp']['avatar'] == "" ? url("assets/images/sp/".$bus['sp']['avatar']."default.png") : url("assets/images/sp/".$bus['sp']['avatar']);
    $bus['sp']['avatar'] = url("assets/images/sp/default.png") ;
    foreach($bus['bus'] as $ky=>$bu){
//        echo '<pre>';
//        print_r($bus['sp']);
//        echo '</pre>';
//        continue;
        
    ?>
<div>
    <div class="route-item clearfix" data-bus_id="<?php echo e($ky); ?>">
        <div class="route-subitem clearfix">
            <div class="route-subitem-content">
                <div class="route-subitem-box">
                    <figure class="route-item-ekspedisi" style="width: 111px;height: 25px">
                        <img src="<?php echo e($bus['sp']['avatar']); ?>" alt="" height="25px">
                    </figure>
                </div>
                <div class="route-subitem-box ekspedisi">
                    <span class="route-item-title sp_name"><?php echo e($bus['sp']['first_name'].' '.$bus['sp']['last_name']); ?></span>
                    <?php $splist[] = $bus['sp']['first_name'].' '.$bus['sp']['last_name']; ?>
                    <span class="route-item-subtitle"><?php echo e($bu['bus']); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box berangkat">
                    <span class="route-item-title board_time"><?php echo current($bu['terminals'])[0]['departure_time'] ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap(current($bu['terminals'])[0]['board_point'], 20, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box arrow">
                    <span class="sprite icon-arrow-right"></span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box tiba">
                    <span class="route-item-title drop_time"><?php echo current($bu['terminals'])[0]['arrival_time']; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap(current($bu['terminals'])[0]['drop_point'], 20, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box rate">
                    <span class="route-item-title rating">4/5</span>
                    <span class="route-item-subtitle">(5 Ratings)</span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box seat">
                    <span class="sprite icon-seat"></span>
                    <span class="route-item-subtitle"><?php echo e($bu['total_seat']); ?> Kursi</span>
                    <span class="hidden avail_seat"><?php echo e($bu['total_seat']); ?></span>
                </div><!-- / .route-subitem-box -->

            </div><!-- / .route-subitem-content -->

            <ul class="route-subitem-content list-tab" role="tablist">
                <li class="route-subitem-box cta ">
                    <a href="#fasilitas-<?php echo e($ky); ?>" class="tab_click" data-bus_id="<?php echo e($ky); ?>" role="tab" data-toggle="tab" aria-controls="fasilitas-one">fasilitas</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#foto-<?php echo e($ky); ?>" class="tab_click" data-bus_id="<?php echo e($ky); ?>" role="tab" data-toggle="tab" aria-controls="foto-one">Foto</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#rute-<?php echo e($ky); ?>" class="tab_click" data-bus_id="<?php echo e($ky); ?>" role="tab" data-toggle="tab" aria-controls="rute-one">Rute</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#info-<?php echo e($ky); ?>" class="tab_click" data-bus_id="<?php echo e($ky); ?>" role="tab" data-toggle="tab" aria-controls="info-one">Info</a>
                </li><!-- / .route-subitem-box -->

                <li class="route-subitem-box cta">
                    <a href="#">Rating</a>
                </li><!-- / .route-subitem-box -->

            </ul><!-- / .route-subitem-content -->
        </div>

        <div class="route-items-eksekusi">
            <span class="pricing" id="pricing_<?php echo e($ky); ?>">Rp <?php echo \General::number_format($bu['fare'],3)?></span>
            <input type="hidden" id="sp_id_<?php echo e($ky); ?>" value="<?php echo e($bus['sp']['id']); ?>" />
            <input type="hidden" id="sp_name_<?php echo e($ky); ?>" value="<?php echo e($bus['sp']['first_name'].' '.$bus['sp']['last_name']); ?>" />
            <input type="hidden" id="bus_name_<?php echo e($ky); ?>" value="<?php echo e($bu['bus']); ?>" />
            <input type="hidden" id="board_points_<?php echo e($ky); ?>" value="<?php echo e(json_encode($board_points[$ky])); ?>" />
            <input type="hidden" id="drop_points_<?php echo e($ky); ?>" value="<?php echo e(json_encode($drop_points[$ky])); ?>" />
            <input type="hidden" id="fare_<?php echo e($ky); ?>" value="<?php echo e($bu['fare']); ?>" />
            <input type="hidden" id="fare_format_<?php echo e($ky); ?>" class="fare" value="<?php echo e(\General::number_format($bu['fare'],3)); ?>" />
            <input type='hidden' id='type_<?php echo e($ky); ?>' class='bustype' value='Bus' />
            <span class="btn btn-radius btn-orange view-seats" id="bus_<?php echo e($ky); ?>" data-bus_id="<?php echo e($ky); ?>" data-map="0">Pilih Kursi</span>
            <div class="loder" id="loader_<?php echo e($ky); ?>" style="display: none;"><img src="<?php echo e(URL::to('assets/img/ajax-loader.gif')); ?>"></div>
        </div>
        <div class="route-items-detail-cta tab-content" id="rdetail_<?php echo e($ky); ?>">
            <div class="tab-pane fade detail-cta-box clearfix" role="tabpanel" id="foto-<?php echo e($ky); ?>" >
                <div class="route-items-detail-cta-container">
                    <div class="foto-slider owl-carousel">
                        <?php 
                        $file1 = URL::to('assets/uploads/bus_images').'/pk-1.jpg';
                        $file2 = URL::to('assets/uploads/bus_images').'/pk-2.jpg';
                        $file3 = URL::to('assets/uploads/bus_images').'/pk-3.jpg';
                        ?>
                        <div class="foto-item">
                            <img src="<?php echo e($file1); ?>" height="124" width="222" alt="">
                        </div>
                        <div class="foto-item">
                            <img src="<?php echo e($file2); ?>" height="124" width="222" alt="">
                        </div>
                        <div class="foto-item">
                            <img src="<?php echo e($file3); ?>" height="124" width="222" alt="">
                        </div>
                        
                    </div>
                </div>
            </div><!-- / .detail-cta-box -->
        </div>
        
    </div><!-- / .route-item -->
    <div class="route-item-detail clearfix">
        
    </div>
</div>
    <?php
    }
}
    }
    
    $splist = array_unique($splist);
?>
    <script>
    var spname = <?php echo json_encode($splist); ?>;
    var sp_filter = [];
    var type_filter = ['Bus','Travel','Shuttle'];
    var board_filter = ['Pagi','Siang','Sore','Malam'];
    var drop_filter = ['Pagi','Siang','Sore','Malam'];
//    console.log(spname);
    for(var x in spname){
        $('#splist').append('<li><a class="large" data-value="'+spname[x]+'" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;'+spname[x]+'</a></li>');
        sp_filter.push(spname[x]);
    }
    
    
    $( '#splist a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = sp_filter.indexOf( val ) ) > -1 ) {
                    sp_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    sp_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( sp_filter );
            getFilter();
            return false;
    });
    
//    var price15 = <?php // echo json_encode($price15); ?>;
//    for(x in price15){
//        $('#price_15').append('<div class="cari-tanggal-item" style="padding:10px;"><span class="cari-tanggal-title">'+x+'</span><span class="cari-tanggal-price">Rp '+price15[x]+'</span></div>');
////        console.log(x+' : '+price15[x]);
//    }
//    $("#price_15").addClass('route-filter-cari-tanggal-slider');
//    $("#price_15").addClass('owl-carousel');
//    $(".route-filter-cari-tanggal-slider").owlCarousel({
//            mouseDrag: true,
//            margin: 0,
//            loop: true,
//            autoplay: true,
//            autoplayHoverPaus: true,
//            autoplaySpeed: 500,
//            autoplayTimeout: 5000,
//            touchDrag: true,
//            responsiveClass: true,
//            responsive: {
//                    0:{
//                            items: 1
//                    },
//                    400:{
//                            items: 1
//                    },
//                    600: {
//                            items: 2
//                    },
//                    1000: {
//                            items: 7
//                    }
//                    },
//            nav: true,
//            navText: [
//                    "<span class='sprite icon-chevron-left'></span>",
//                    "<span class='sprite icon-chevron-right'></span>"
//            ],
//    });
    
    var bus_id = '<?php echo e($already_booked_bus_id); ?>';
//    console.log(bus_id);
    $('#bus_'+bus_id).trigger('click');
    $(".foto-slider").owlCarousel({
		mouseDrag: true,
		margin: 15,
		loop: true,
		autoplay: true,
		autoplayHoverPaus: true,
		autoplaySpeed: 500,
		autoplayTimeout: 5000,
		touchDrag: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 3
			}
		},
		nav: true,
		navText: [
				"<span class='sprite icon-arrow-circle-left-small'></span>",
	        	        "<span class='sprite icon-arrow-circle-right-small'></span>"
		],
	});
        $(function(){
        var pidv="";
            $(".tab_click").click(function(){
                var idv=$(this).attr('href');
                console.log('idv='+idv);
                console.log('pidv='+pidv);
                
                var ridv=idv.substring(6);
                var rpidv=pidv.substring(6);
                
                if(idv==pidv){
                    $(idv).toggle();
                }
                else{ 
                    if(ridv==rpidv){ 
                         $(pidv).hide();   
                    }else if(ridv!=rpidv){
                        $("#fasi-"+ridv).hide();
                        $("#foto-"+ridv).hide();
                        $("#rute-"+ridv).hide();
                        $("#info-"+ridv).hide();
                    }
                   $(idv).show();
              }              
                pidv=idv;
            });
            
        });
    </script>