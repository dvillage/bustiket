function setFrom(val){
    
    var f = $(val).attr('id').split('-');
    if(f[1] == 'province'){
        $('#from').val($(val).html()+'.P');
        $('#fromcity').val($(val).html());
    }
    else if(f[1] == 'district'){
        $('#from').val($(val).html()+'.D');
        $('#fromcity').val($(val).html());
    }
    else if(f[1] == 'city'){
//    if(f[1] == 'city'){
        $('#from').val($(val).html()+'.C');
        $('#fromcity').val($(val).html());
    }
    else if(f[1] == 'terminal'){
        $('#from').val($(val).html()+'.T');
        $('#fromcity').val($(val).html());
    }

    if($('#from').val() == ''){
        $('#fromcity').click();
        $('#fromcity').focus();
    }
    else {
        if($('#to').val() == ''){
            $('#tocity').click();
            $('#tocity').focus();
        }
        $('#fromcity').removeAttr('onblur');
    }
    
    
}
function setFromPari(val){
    
    var f = $(val).attr('id').split('-');
//    if(f[1] == 'province'){
//        $('#to').val($(val).html()+'.P');
//        $('#tocity').val($(val).html());
//    }
//    else if(f[1] == 'district'){
//        $('#to').val($(val).html()+'.D');
//        $('#tocity').val($(val).html());
//    }
//    else if(f[1] == 'city'){
    if(f[1] == 'city'){
        $('#fromC').val($(val).html()+'.C');
        $('#from_city').val($(val).html());
    }
    else if(f[1] == 'terminal'){
        $('#fromC').val($(val).html()+'.T');
        $('#from_city').val($(val).html());
    }
//    $('#from_city').val($(val).html());
//    $('#fromC').val($(val).html()+'-'+$(val).attr('id'));

    $('#from_city').removeAttr('onblur');

}
function setTo(val){
    
    var f = $(val).attr('id').split('-');
    if(f[1] == 'province'){
        $('#to').val($(val).html()+'.P');
        $('#tocity').val($(val).html());
    }
    else if(f[1] == 'district'){
        $('#to').val($(val).html()+'.D');
        $('#tocity').val($(val).html());
    }
    else if(f[1] == 'city'){
//    if(f[1] == 'city'){
        $('#to').val($(val).html()+'.C');
        $('#tocity').val($(val).html());
    }
    else if(f[1] == 'terminal'){
        $('#to').val($(val).html()+'.T');
        $('#tocity').val($(val).html());
    }

    if($('#to').val() == ''){
        $('#tocity').click();
        $('#tocity').focus();
    }
    else {
        $('#tocity').removeAttr('onblur');
        $('#date').focus();
    }
    

}

function setDate(elem){
    var d = new Date(elem.value);
    $('#date').val(d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate());
//    alert($('#date').val());
}

function getBaseURL(){
    var url = $("#base_url").val();
    return url;
}




function filterFrom(){

    if($('#fromfilter').children().length == 0 ){
        $('#fromcity').parent().parent().addClass('active');
    }
    $('.altmsg').css('display','none');
    if($('#fromcity').val() !== ''){
        var val = new RegExp($('#fromcity').val(),'i');
        var pro;
        var dist;
        var city;
        var p;
        var counter = 0;
        
        $('#fromfilter').empty();
        
        //============= For Province, District, City and Terminal Dropdown ========================
//        for(x in data){
//            if(val.test(data[x].name)){
//                if(data[x].type == 'province'){
//                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bolder;">'+data[x].name+'</a><ul></ul></li>');
//                    pro=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'district'){
//                    if(typeof(pro) == 'undefined' || pro == null){
//                        $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                        pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                    }
//                    pro.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    dist = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'city'){
//                    if(typeof(dist) == 'undefined' || dist == null){
//                        if(typeof(pro) == 'undefined' || pro == null){
//                            $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                            pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                        }
//                        pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                        dist=$('#fromfilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
//                    dist.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:45px;;font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
//                    city = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'terminal'){
//                    if(typeof(city) == 'undefined' || city == null){
//                        if(typeof(dist) == 'undefined' || dist == null){
//                            if(typeof(pro) == 'undefined' || pro == null){
//                                $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                                pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                            }
//                            pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                            dist=$('#fromfilter').find('#'+data[x].id+'-District').parent().children('ul');
//                        }
//                        dist.append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
//                        city=$('#fromfilter').find('#'+data[x].id+'-City').parent().children('ul');
//                    }
//                    city.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:60px;font-size:14px;">'+data[x].name+'</a></li>');
//                }
//            }
//            if(counter == 10){
//                break;
//            }
//        }
    
            //============= For District and City Dropdown ========================
//        for(x in data){
//            if(val.test(data[x].name)){
//                if(data[x].type == 'district'){
//                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a><ul></ul></li>');
//                    p=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                    
//                }
//                else if(data[x].type == 'city'){
//                    if(typeof(p) == 'undefined' || p == null){
//                        $('#fromfilter').append('<li><a id="'+data[x].id+'-District"></a><ul></ul></li>');
//                        p=$('#fromfilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
//                    p.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a></li>');
//                }
//            }
//            if(counter == 10){
//                break;
//            }
//        }
//        
            //============= For City and Terminal Dropdown ========================
        
//        for(x in data){
//            if(data[x].type == 'city'){
//                var cname = data[x].name;
//                var cid = data[x].id;
//            }
//            if(val.test(data[x].name)){
//                if(data[x].type == 'city'){
//                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    p=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                    counter++;
//                    
//                }
//                else if(data[x].type == 'terminal'){
//                    if(typeof(p) == 'undefined' || p == null){
//                        $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+cid+'-City"  style="font-weight:bold;">'+cname+'</a><ul></ul></li>');
//                        p=$('#fromfilter').find('#'+cid+'-City').parent().children('ul');
//                    }
//                    p.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:14px;">'+data[x].name+'</a></li>');
//                    counter++;
//                }
//            }
//            
//            if(counter == 10){
//                break;
//            }
//        }

//============= For Province, District, City and Terminal Dropdown ========================
        for(x in data){
            if(val.test(data[x].name)){
                if(data[x].type == 'province'){
                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bolder;">'+data[x].name+'</a><ul></ul></li>');
//                    pro=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                    counter++;
                }
                else if(data[x].type == 'district'){
//                    if(typeof(pro) == 'undefined' || pro == null){
//                        $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                        pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                    }
                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    pro.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    dist = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                    counter++;
                }
                else if(data[x].type == 'city'){
//                    if(typeof(dist) == 'undefined' || dist == null){
//                        if(typeof(pro) == 'undefined' || pro == null){
//                            $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                            pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                        }
//                        pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                        dist=$('#fromfilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
//                    dist.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:45px;;font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
                    $('#fromfilter').append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
                    city = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                    counter++;
                }
                else if(data[x].type == 'terminal'){
                    if(typeof(city) == 'undefined' || city == null){
//                        if(typeof(dist) == 'undefined' || dist == null){
//                            if(typeof(pro) == 'undefined' || pro == null){
//                                $('#fromfilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                                pro=$('#fromfilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                            }
//                            pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                            dist=$('#fromfilter').find('#'+data[x].id+'-District').parent().children('ul');
//                        }
//                        dist.append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
                        $('#fromfilter').append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
                        city=$('#fromfilter').find('#'+data[x].id+'-City').parent().children('ul');
                    }
                    city.append('<li><a onclick="setFrom(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:14px;">'+data[x].name+'</a></li>');
                    counter++;
                }
            }
            if(counter == 10){
                break;
            }
        }
    }
    else{
        $('#fromfilter').empty();
    }
}
function filterFromPariwisata(){
    
    if($('#from_filter_pariwisata').children().length == 0 ){
        $('#from_city').parent().parent().addClass('active');
    }
    $('.altmsg').css('display','none');
    if($('#from_city').val() !== ''){
        var val = new RegExp($('#from_city').val(),'i');
        var p;
        $('#from_filter_pariwisata').empty();
        var counter = 0;
        for(x in data){
            if(val.test(data[x].name)){
                if(data[x].type == 'district'){
//                    $('#from_filter_pariwisata').append('<li><a onclick="setFromPari(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a><ul></ul></li>');
//                    p=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                    
                }
                else if(data[x].type == 'city'){
                    if(typeof(p) == 'undefined' || p == null){
                        $('#from_filter_pariwisata').append('<li><p id="'+data[x].id+'-District"></p><ul></ul></li>');
                        p=$('#from_filter_pariwisata').find('#'+data[x].id+'-District').parent().children('ul');
                    }
                    p.append('<li><a onclick="setFromPari(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a></li>');
                    counter++;
                }
            }
            
            if(counter == 10){
                break;
            }
        }
    }
    else{
        $('#from_filter_pariwisata').empty();
    }
}
function filterTo(){
    
    if($('#tofilter').children().length == 0 ){
        $('#tocity').parent().parent().addClass('active');
    }
    $('.altmsg').css('display','none');
    if($('#tocity').val() !== ''){
        var val = new RegExp($('#tocity').val(),'i');
        var pro;
        var dist;
        var city;
        var p;
        var counter = 0;
        $('#tofilter').empty();
        
        //============= For Province, District, City and Terminal Dropdown ========================
//        for(x in data){
//            if(val.test(data[x].name)){
//                if(data[x].type == 'province'){
//                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bolder;">'+data[x].name+'</a><ul></ul></li>');
//                    pro=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'district'){
//                    if(typeof(pro) == 'undefined' || pro == null){
//                        $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                        pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                    }
//                    pro.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    dist = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'city'){
//                    if(typeof(dist) == 'undefined' || dist == null){
//                        if(typeof(pro) == 'undefined' || pro == null){
//                            $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                            pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                        }
//                        pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                        dist=$('#tofilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
//                    dist.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:45px;;font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
//                    city = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                }
//                else if(data[x].type == 'terminal'){
//                    if(typeof(city) == 'undefined' || city == null){
//                        if(typeof(dist) == 'undefined' || dist == null){
//                            if(typeof(pro) == 'undefined' || pro == null){
//                                $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                                pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                            }
//                            pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                            dist=$('#tofilter').find('#'+data[x].id+'-District').parent().children('ul');
//                        }
//                        dist.append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
//                        city=$('#tofilter').find('#'+data[x].id+'-City').parent().children('ul');
//                    }
//                    city.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:60px;font-size:14px;">'+data[x].name+'</a></li>');
//                }
//            }
//            if(counter == 10){
//                break;
//            }
//        }
    
            //============= For District, City Dropdown ========================
//        for(x in data){
//            if(val.test(data[x].name)){
//                if(data[x].type == 'district'){
//                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a><ul></ul></li>');
//                    p=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                    
//                }
//                else if(data[x].type == 'city'){
//                    if(typeof(p) == 'undefined' || p == null){
//                        $('#tofilter').append('<li><a id="'+data[x].id+'-District"></a><ul></ul></li>');
//                        p=$('#tofilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
//                    p.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'">'+data[x].name+'</a></li>');
//                }
//            }
//            if(counter == 10){
//                break;
//            }
//        }
//        
            //============= For City, Terminal Dropdown ========================
        
//        for(x in data){
//            if(data[x].type == 'city'){
//                var cname = data[x].name;
//                var cid = data[x].id;
//            }
//            if(val.test(data[x].name)){
//                if(data[x].type == 'city'){
//                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    p=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
//                    counter++;
//                    
//                }
//                else if(data[x].type == 'terminal'){
//                    if(typeof(p) == 'undefined' || p == null){
//                        $('#tofilter').append('<li><a onclick="setTo(this);" id="'+cid+'-City"  style="font-weight:bold;">'+cname+'</a><ul></ul></li>');
//                        p=$('#tofilter').find('#'+cid+'-City').parent().children('ul');
//                    }
//                    p.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:14px;">'+data[x].name+'</a></li>');
//                    counter++;
//                }
//            }
//            
//            if(counter == 10){
//                break;
//            }
//        }

//============= For Province, District, City and Terminal Dropdown ========================
        for(x in data){
            if(val.test(data[x].name)){
                if(data[x].type == 'province'){
                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bolder;">'+data[x].name+'</a><ul></ul></li>');
//                    pro=$('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                    counter++;
                }
                else if(data[x].type == 'district'){
//                    if(typeof(pro) == 'undefined' || pro == null){
//                        $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                        pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                    }
                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    pro.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-weight:bold;">'+data[x].name+'</a><ul></ul></li>');
//                    dist = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                        counter++;
                }
                else if(data[x].type == 'city'){
//                    if(typeof(dist) == 'undefined' || dist == null){
//                        if(typeof(pro) == 'undefined' || pro == null){
//                            $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                            pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                        }
//                        pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                        dist=$('#tofilter').find('#'+data[x].id+'-District').parent().children('ul');
//                    }
                    $('#tofilter').append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
//                    dist.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="padding-left:45px;;font-size:15px;font-weight:900;">'+data[x].name+'</a><ul></ul></li>');
                    city = $('#'+data[x].id+'-'+data[x].type).parent().children('ul');
                    counter++;
                }
                else if(data[x].type == 'terminal'){
                    if(typeof(city) == 'undefined' || city == null){
//                        if(typeof(dist) == 'undefined' || dist == null){
//                            if(typeof(pro) == 'undefined' || pro == null){
//                                $('#tofilter').append('<li><a id="'+data[x].id+'-Province" style="padding:0px;"></a><ul></ul></li>');
//                                pro=$('#tofilter').find('#'+data[x].id+'-Province').parent().children('ul');
//                            }
//                            pro.append('<li><a id="'+data[x].id+'-District" style="padding:0px;"></a><ul></ul></li>');
//                            dist=$('#tofilter').find('#'+data[x].id+'-District').parent().children('ul');
//                        }
//                        dist.append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
                        $('#tofilter').append('<li><a id="'+data[x].id+'-City" style="padding:0px;"></a><ul></ul></li>');
                        city=$('#tofilter').find('#'+data[x].id+'-City').parent().children('ul');
                    }
                    city.append('<li><a onclick="setTo(this);" id="'+data[x].id+'-'+data[x].type+'" style="font-size:14px;">'+data[x].name+'</a></li>');
                    counter++;
                }
            }
            if(counter == 10){
                break;
            }
        }
    }
    else{
        $('#tofilter').empty();
    }
}
function selectType(obj){
    var ac = $(obj).data('ac');
    if(ac == 0){
        $('#b_type').html('Non Ac');
        $('#b_type_h').val(0);
    }else if(ac == 1){
        $('#b_type').html('Ac');
        $('#b_type_h').val(1);
    }else{
        $('#b_type').html('Ac,Non Ac');
        $('#b_type_h').val(-1);
    }
    $(obj).parents('.cari-tiket-field').toggleClass('active');
}
function validate(c){
    var from = '';
    var to = '';
    if(c == 'm'){
        var from = $('#mfrom').val();
        var to = $('#mto').val();
        var nop = $('#mticket-bus').find('#nop').val();
//        alert(nop);
        if(from == ''){
            $('#mfromcity').focus();
            $('.maltmsg').css('display','block');
            $('#mmsg').html('From City Required..!');
            return false;
        }
        if(to == ''){
            $('#mtocity').focus();
            $('.maltmsg').css('display','block');
            $('#mmsg').html('Destination City Required..!');
            return false;
        }
        if(nop > 4){
            $('#nop').focus();
            $('.maltmsg').css('display','block');
            $('#mmsg').html('Maximum 4 passanger allowed..!');
            return false;
        }
    }
    if(c == 'w'){
        var from = $('#from').val();
        var to = $('#to').val();
        var nop = $('#ticket-bus').find('#nop').val();
//        alert(nop);
        if(from == ''){
            $('#fromcity').click();
            $('#fromcity').focus();
            $('.altmsg').css('display','block');
            $('#msg').html('From City Required..!');
            return false;
        }
        if(to == ''){
            $('#tocity').click();
            $('#tocity').focus();
            $('.altmsg').css('display','block');
            $('#msg').html('Destination City Required..!');
            return false;
        }
        if(nop > 4){
            $('#nop').focus();
            $('.altmsg').css('display','block');
            $('#msg').html('Maximum 4 passanger allowed..!');
            return false;
        }
    }
    if(c == 'p'){
        var from = $('#fromC').val();
        var type = $('#b_type_h').val();
        if(from == ''){
            $('#from_city').click();
            $('#from_city').focus();
            $('.altmsg').css('display','block');
            $('#msg1').html('From City Required..!');
            return false;
        }
        if(type == ''){
            $('#b_type').click();
            $('#b_type').focus();
            $('.altmsg').css('display','block');
            $('#msg1').html('Bus type Required..!');
            return false;
        }
    }
}

function setLoc(elem){
    var c = 0;
//    console.log('Set Loc : '+elem.id);
    for(x in data){
        if((elem.value.toLowerCase()) == (data[x].name.toLowerCase())){
            
            if(elem.id == 'fromcity'){
                if($('#from').val() != ''){
                    return;
                }
                if(data[x].type == 'province'){
                    if(c == 0){
                        $('#from').val(data[x].name+'.P');
                        $('#fromcity').val(data[x].name);
                    }
                }
                else if(data[x].type == 'district'){
                    if(c == 0){
                        $('#from').val(data[x].name+'.D');
                        $('#fromcity').val(data[x].name);
                    }
                }
                else if(data[x].type == 'city'){
//                if(data[x].type == 'city'){
//                    alert(data[x].name);
                    $('#from').val(data[x].name+'.C');
                    $('#fromcity').val(data[x].name);
                    c = 1;
                }
                else if(data[x].type == 'terminal'){
//                    alert(data[x].name);
                    if(c == 0){
                        $('#from').val(data[x].name+'.T');
                        $('#fromcity').val(data[x].name);
                    }
                }
                
                if($('#from').val() == ''){
                    $('#fromcity').click();
                    $('#fromcity').focus();
//                    $(elem).removeAttr('onblur');
//                    $(elem).parent().parent().removeClass('active');
//                    break;
                }
                else{
//                  $('#fromcity').val(data[x].name);
//                  $('#from').val(data[x].name+'.'+data[x].type);
                    $('#tocity').click();
//                    $(elem).removeAttr('onblur');
//                    $(elem).parent().parent().removeClass('active');
//                    break;
                }
            }
            if(elem.id == 'tocity'){
                if(data[x].type == 'province'){
                    if(c == 0){
                        $('#to').val(data[x].name+'.P');
                        $('#tocity').val(data[x].name);
                    }
                }
                else if(data[x].type == 'district'){
                    if(c == 0){
                        $('#to').val(data[x].name+'.D');
                        $('#tocity').val(data[x].name);
                    }
                }
                else if(data[x].type == 'city'){
//                if(data[x].type == 'city'){
                    $('#to').val(data[x].name+'.C');
                    $('#tocity').val(data[x].name);
                    c = 1;
                    
                }
                else if(data[x].type == 'terminal'){
                    if(c == 0){
                        $('#to').val(data[x].name+'.T');
                        $('#tocity').val(data[x].name);
                    }
                }
                
                if($('#to').val() == ''){
                    $('#tocity').click();
                    $('#tocity').focus();
//                    $(elem).removeAttr('onblur');
//                    $(elem).parent().parent().removeClass('active');
//                    break;
                }
                else{
//                $('#tocity').val(data[x].name);
//                $('#to').val(data[x].name+'.'+data[x].type);
                    $('#date').focus();
//                    $(elem).removeAttr('onblur');
//                    $(elem).parent().parent().removeClass('active');
//                    break;
                }
            }
            
            $(elem).removeAttr('onblur');
            $(elem).parent().parent().removeClass('active');
            
        }
    }
}
function setLocPari(elem){
    
    for(x in data){
        if((elem.value.toLowerCase()) == (data[x].name.toLowerCase())){
            if(elem.id == 'from_city'){
                //                if(data[x].type == 'province'){
//                    $('#from').val(data[x].name+'.P');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'district'){
//                    $('#from').val(data[x].name+'.D');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'city'){
                if(data[x].type == 'city'){
                    $('#fromC').val(data[x].name+'.C');
                    $('#from_city').val(data[x].name);
                }
//                else if(data[x].type == 'terminal'){
//                    $('#fromC').val(data[x].name+'.T');
//                    $('#from_city').val(data[x].name);
//                }
                
//                $('#from_city').val(data[x].name);
//                $('#fromC').val(data[x].name+'-'+data[x].id+'-'+data[x].type);
            }
            
            $(elem).removeAttr('onblur');
            $(elem).parent().parent().removeClass('active');
        }
    }
}
function msetLocPari(elem){
    var c = 0;
    for(x in data){
        if((elem.value.toLowerCase()) == (data[x].name.toLowerCase())){
            if(elem.id == 'mfrom_city'){
                //                if(data[x].type == 'province'){
//                    $('#from').val(data[x].name+'.P');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'district'){
//                    $('#from').val(data[x].name+'.D');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'city'){
                if(data[x].type == 'city'){
                    $('#mfromP').val(data[x].name+'.C');
                    $('#mfrom_city').val(data[x].name);
                }
//                else if(data[x].type == 'terminal'){
//                    $('#mfromP').val(data[x].name+'.T');
//                    $('#mfrom_city').val(data[x].name);
//                }
                
//                $('#mfrom_city').val(data[x].name);
//                $('#mfromP').val(data[x].name+'-'+data[x].id+'-'+data[x].type);
            }
            
            $(elem).removeAttr('onblur');
            $(elem).parent().parent().removeClass('active');
        }
    }
}
function msetLoc(elem){
    var c = 0;
    for(x in data){
        if((elem.value.toLowerCase()) == (data[x].name.toLowerCase())){
            if(elem.id == 'mfromcity'){
//                if(data[x].type == 'province'){
//                    $('#from').val(data[x].name+'.P');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'district'){
//                    $('#from').val(data[x].name+'.D');
//                    $('#fromcity').val(data[x].name);
//                }
//                else if(data[x].type == 'city'){
                if(data[x].type == 'city'){
                    $('#mfrom').val(data[x].name+'.C');
                    $('#mfromcity').val(data[x].name);
                    c = 1;
                }
                else if(data[x].type == 'terminal'){
                    if(c == 0){
                        $('#mfrom').val(data[x].name+'.T');
                        $('#mfromcity').val(data[x].name);
                    }
                    
                }
                
//                $('#mfromcity').val(data[x].name);
//                $('#mfrom').val(data[x].name+'-'+data[x].id+'-'+data[x].type);
                $('#mtocity').click();
            }
            if(elem.id == 'mtocity'){
                //                if(data[x].type == 'province'){
//                    $('#to').val(data[x].name+'.P');
//                    $('#tocity').val(data[x].name);
//                }
//                else if(data[x].type == 'district'){
//                    $('#to').val(data[x].name+'.D');
//                    $('#tocity').val(data[x].name);
//                }
//                else if(data[x].type == 'city'){
                if(data[x].type == 'city'){
                    $('#mto').val(data[x].name+'.C');
                    $('#mtocity').val(data[x].name);
                    c = 1;
                }
                else if(data[x].type == 'terminal'){
                    if(c == 0){
                        $('#mto').val(data[x].name+'.T');
                        $('#mtocity').val(data[x].name);
                    }
                }
                
//                $('#mtocity').val(data[x].name);
//                $('#mto').val(data[x].name+'-'+data[x].id+'-'+data[x].type);
            }
            $(elem).removeAttr('onblur');
        }
    }
}

function swap(){
    var temp=$('#fromcity').val();
    $('#fromcity').val($('#tocity').val());
    $('#tocity').val(temp);
    
    temp=$('#from').val();
    $('#from').val($('#to').val());
    $('#to').val(temp);
}

function couponDisc(){
        var code = $('#couponcode').val();
        var b_id = $('#bus_id').val();
//        var amount = parseFloat($('#total_amnt').html());
        var amount = parseFloat($('#total_amount').val());
        var url=getBaseURL()+"payment/coupon-discount";
        var data={ccode:code,busid:b_id,amount:amount};
        var f = 1;
        if(f == 1){
            postAjax(url,data,function(res){
                f = 0;
                console.log(res);
                if(res.flag == 1){
                    $('#cpnmsg').html('* '+ res.msg);
                    $('#cpnmsg').css('display','block');
                    $('#cpnmsg').css('color','green');
                    $('#discount_amount').html(parseFloat(res.discount/1000).toFixed(3));
//                    $('#total_amnt').html((amount-res.discount/1000).toFixed(3));
                    $('#total_amnt').html(((amount-res.discount)/1000).toFixed(3));
//                    $('#internet_banking_total').html(((amount-res.discount)/1000).toFixed(3));
                    $('#internet_banking_total').html(((amount-res.discount)/1000).toFixed(3));
//                    $('#total_amount').val(((amount - res.discount)) * 1000);
                    console.log('amount : '+amount + ' , discount : '+res.discount);
                    $('#total_amount').val(((amount - res.discount)) );
//                    $('#coupon_discount').val((res.discount.toFixed(3))*1000);
                    $('#coupon_discount').val((res.discount));
                    $('#coupon_code').val(code);

                    $('#cpnbtn').removeAttr('onclick');
                }
                else{
                    $('#cpnmsg').html('* '+ res.msg);
                    $('#cpnmsg').css('display','block');
                    $('#cpnmsg').css('color','red');
                }
            });
        }
        
    }
    
    function setDetails(val){
        for(var x in blist){
            if(blist[x].bank_short_name === val){
                $('#bname').html(blist[x].bank_name+' - '+blist[x].bank_address);
                $('#acname').html('Nama Rekening: '+blist[x].account_name);
                $('#acno').html('Nomor Rekening: '+blist[x].account_no);
            }
        }
    }
    
    function downloadAsPdf(ticket_id){
        
        var url=getBaseURL()+"payment/download-as-pdf";
        var data={ticket_id:ticket_id,};
        postAjax(url,data,function(res){
            console.log(res);
        });
    }
    
    function pad(d) {
        return (d < 10) ? '0' + d.toString() : d.toString();
    }
    function matchCpass(){
        if($('#new_pass').val() != $('#cnew_pass').val()){
            alert('New Password & Confirm New Password must be same.');
            $('#new_pass').val('');
            $('#cnew_pass').val('');
            $('#new_pass').focus();
            return false;
        }
        return true;
    }

$(document).ready(function(){

});