var buses = [];
var osrt = 'asc';
var psrt = 'asc';
$(document).ready(function () {
    var initial = $("body").width();
//    console.log('initial : '+initial);
    $(window).resize(function(){
//        location.reload();
        var w = $(this).width();
        if(w < initial){
            initial = w;
            if(w <= 768){
                location.reload();
            }
        }else if (w > initial){
            initial = w;
            if(w > 768){
                location.reload();
            }
        }
    });
    
    var param = $('#param').val();
//    console.log(param);
    param = JSON.parse(param);
    var url = getBaseURL() + 'services/search-pariwisata';
    postAjax(url, param, function (res) {
//        console.log(res);
        if(typeof res.flag !== 'undefined'){
            $('#ptitle').html('0 Bus Found');
            $('#ptitle1').html('0 Bus Found');
            var msg = '<center>'+res.msg+'</center>';
            $('.route-search-result-items').html(msg);
            return;
        }
        $('.route-search-result-items').html(res);
//        $('.result').html(res);
        $('#altmsg').css({display: 'none'});
    });
    
    $(document).on('click','.pariwisata_facelities',function(){
        var cur_show = $(this).parent().next().attr('data-show');
//        console.log(cur_show);
        if(cur_show == 0){
            $(this).parent().next().attr('data-show',1);
            $(this).parent().next().css({display:'block'});
        }else{
            $(this).parent().next().attr('data-show',0);
            $(this).parent().next().css({display:'none'});
        }
    });
    
    var total_bus = 0;
    
    $(document).on('click','.list_harga',function(){
//        console.log('clicked');
        var price = JSON.parse($(this).prev().val());
        var l = price.length;
        if(l>0){
            var tr = '';
            for(var i in price){
                var pr = price[i]['price'];
//                pr = pr.toFixed(3);
//                console.log(pr);
//                 pr = (pr/1000).toFixed(3);
                tr += '<tr><td>'+price[i]['direction']+'</td><td>'+price[i]['type']+'<br>'+price[i]['seat_count']+' Seats</td><td>'+pr+'</td></tr>';
            }
        }else{
            tr = '<tr><td colspan="3">No pricing found.</td></tr>'
        }
        $('#append_price').html(tr);
    });
    
    $(document).on('click','.select_bus',function(){
        
        var id = $(this).attr('data-id');
        var sp = $(this).attr('data-sp');
        var type = $(this).attr('data-type');
        var ac = $(this).attr('data-ac');
        ac = ac == 1 ? 'AC' : 'Non AC';
        var book = '<button class="booking-box-item-close" data-id="'+id+'" id="close_'+id+'"><span class="sprite icon-close"></span></button>'+
                    '<div class="booking-box-item-content">'+
                        '<h4 class="booking-box-item-content-title">'+sp+'</h4>'+

                        '<span class="label label-default label-smallest">'+
                            '<span class="sprite icon-bus-smallest"></span> '+type+
                        '</span>'+

                        '<span class="label label-default label-smallest">'+
                            '<span class="sprite icon-check-smallest"></span> '+ac+
                        '</span>'+
                    '</div>';
        var cnt = 0;
        var parent = $('#list_'+id);
        var total_bus = parseInt($('#total_bus').val());
        if($(this).hasClass('btn-orange')){
            $(this).removeClass('btn-orange').addClass('btn-green');
            $('.booking-box-item').each(function(){
                var l = $(this).html().length;

                if(l== 0){
                    cnt = 1;
                    $('#total_bus').val(total_bus + 1);
                    $(this).html(book);
                    $(this).addClass('booked');
                    parent.find('.common').css({display:'block'});
                    var b = {
                        id:id,
                        sp_name:sp,
                        ac:ac,
                        type:type
                    };
                    buses.push(b);
//                    console.log(buses);
                    return false;
                }

            });    
            if(cnt == 0){
                alert('You can not add more bus.');
            }
        }else{
            $('#total_bus').val(total_bus - 1);
            $(this).removeClass('btn-green').addClass('btn-orange');
            var par = $('.booking-box-item').find('#close_'+id).parent();
            par.html('');
            par.removeClass('booked');
            parent.find('.common').css({display:'none'});
            removeBus(id);
//            console.log(buses);
        }
        
        
    });
    $(document).on('click','.booking-box-item-close',function(){
        var total_bus = parseInt($('#total_bus').val());
        $('#total_bus').val(total_bus - 1);
        var id = $(this).attr('data-id');
        $(this).parent().removeClass('booked');
        $(this).parent().html('');
        $('#btn_'+id).removeClass('btn-green').addClass('btn-orange');
        var parent = $('#list_'+id);
        parent.find('.common').css({display:'none'});
        removeBus(id);
//        console.log(buses);
    })
    
    $(document).on('click','#book_bus',function(){
        var total = $('#total_bus').val();
        if(total == 0){
            alert('please select at least one bus');
            return false;
        }
        $('#bus_detail').val(JSON.stringify(buses));
    });
});
function removeBus(id){
    for(var i in buses){
        if(buses[i]['id'] == id){
            buses.splice(i, 1);
            return;
        }
    }
}



$(document).ready(function(){
    
    
    //----------------- For Sorting -----------------------------------------//
    
    $('#opsort,#m_opsort').click(function(){
        if(osrt == 'asc'){
            
            $('.sp_name').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('up');
            $(this).removeClass('down');
            osrt = 'dsc';
        }else if(osrt == 'dsc'){
            $('.sp_name').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.sp_name').text() > $(b).children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('down');
            $(this).removeClass('up');
            osrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    
    
    $('#pricesort,#m_pricesort').click(function(){
//        console.log('Called');
        if(psrt == 'asc'){
            console.log(psrt);
            var a = $('.fare').parent().sort(function(a, b) {
                var aPr = $(a).children('.fare').val();
                var bPr = $(b).children('.fare').val();
//                console.log(aPr+' < '+bPr);
                if (aPr < bPr) {
                    return -1;
                } else {
                    return 1;
                }
                return aPr < bPr ? -1 : bPr < aPr ? 1:0;
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            psrt = 'dsc';
        }else if(psrt == 'dsc'){
            console.log(psrt);
            $('.fare').parent().sort(function(a, b) {
                var aPr = $(a).children('.fare').val();
                var bPr = $(b).children('.fare').val();
//                console.log(aPr+' < '+bPr);
//                if (aPr > bPr) {
//                    return -1;
//                } else {
//                    return 1;
//                }
                return aPr > bPr ? -1 : bPr > aPr ? 1:0;
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            psrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    
    $('#seatsort,#m_seatsort').click(function(){
//        console.log('Called');
        if(psrt == 'asc'){
            console.log(psrt);
            var a = $('.tseat').parent().sort(function(a, b) {
                var aPr = $(a).children('.tseat').val();
                var bPr = $(b).children('.tseat').val();
//                console.log(aPr+' < '+bPr);
                if (aPr < bPr) {
                    return -1;
                } else {
                    return 1;
                }
                return aPr < bPr ? -1 : bPr < aPr ? 1:0;
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            psrt = 'dsc';
        }else if(psrt == 'dsc'){
            console.log(psrt);
            $('.tseat').parent().sort(function(a, b) {
                var aPr = $(a).children('.tseat').val();
                var bPr = $(b).children('.tseat').val();
//                console.log(aPr+' < '+bPr);
//                if (aPr > bPr) {
//                    return -1;
//                } else {
//                    return 1;
//                }
                return aPr > bPr ? -1 : bPr > aPr ? 1:0;
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            psrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    
    
    
    //------------------- For Filtering ----------------------------------//
    
    $( '#splist a,#splist1 a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = sp_filter.indexOf( val ) ) > -1 ) {
                    sp_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    sp_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

            return false;
    });
    
    $( '#typelist a,#typelist1 a' ).on( 'click', function( event ) {
//            console.log( type_filter );
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = type_filter.indexOf( val ) ) > -1 ) {
                    type_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    type_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( type_filter );
            if($('div.showin-995#searchresult').css('display') !='undefined' && $('div.showin-995#searchresult').css('display') == 'block'){
                m_getFilter();
                return false;
            }
            getFilter();
            return false;
    });
    $( '#fitur a,#fitur1 a' ).on( 'click', function( event ) {
//            console.log( fitur_filter );
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = fitur_filter.indexOf( val ) ) > -1 ) {
                    fitur_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    fitur_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( type_filter );
            if($('div.showin-995#searchresult').css('display') !='undefined' && $('div.showin-995#searchresult').css('display') == 'block'){
                m_getFilter();
                return false;
            }
            getFilter();
            return false;
    });
    
});

function getFilter(){
    var n=$('#searchresult').find('div.pariwisata-item.clearfix');

    for(i=0;i<n.length;i++){
        var sp = $(n[i]).find('h3.sp_name')[0].textContent;
        var tp = $(n[i]).find('input.bustype')[0].value;
        var ft = $(n[i]).find('input.fitur')[0].value;
        
        var sp_flag=0;
        var tp_flag=0;
        var ft_flag=0;
        

        for(y=0;y<sp_filter.length;y++){
//                    console.log( sp_filter );
//                    console.log(sp+' : '+sp_filter[y]);
//                    console.log(sp_filter[y]);
            if(sp == sp_filter[y]){
//                        console.log('In SP If');
                sp_flag=1;
                break;
            }
            
        }

        for(y=0;y<type_filter.length;y++){
//                    console.log( type_filter );
//                    console.log(tp+" : "+type_filter[y]);
            if(tp == type_filter[y]){
//                        console.log('In TP If');
                tp_flag=1;
                break;
            }
            
        }
        for(y=0;y<fitur_filter.length;y++){
//                    console.log( type_filter );
//                    console.log(ft+" : "+fitur_filter[y]);
            if(ft.trim() == fitur_filter[y].trim()){
//                        console.log('In TP If');
                ft_flag=1;
                break;
            }
            
        }
        
//        console.log(n[i]);
//                console.log("sp_flag : "+sp_flag+", tp_flag : "+tp_flag+", ft_flag : "+ft_flag);
        if(sp_flag==1 && tp_flag==1 && ft_flag==1){
            $(n[i]).css('display','block');
        }
        else{
            $(n[i]).css('display','none');
        }
    }
}