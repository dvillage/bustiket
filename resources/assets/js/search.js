var osrt = 'asc';
var bsrt = 'asc';
var dsrt = 'asc';
var rsrt = 'asc';
var asrt = 'asc';
var psrt = 'asc';

$(document).ready(function () {
    var initial = $("body").width();
//    console.log('initial : '+initial);
    $(window).resize(function(){
//        location.reload();
        var w = $(this).width();
        if(w < initial){
            initial = w;
            if(w <= 768){
                location.reload();
            }
        }else if (w > initial){
            initial = w;
            if(w > 768){
                location.reload();
            }
        }
    });
    var nop = $('#nop').val();
    var mnop = $('#mhnop').val();
    if(nop > 4 || mnop > 4){
        alert('maximum 4 passengers allowed.');
        return false;
    }
    var param = $('#param').val();
//    console.log(param);
//    param = JSON.parse(param);
    param = {id:param};
    var url = getBaseURL() + 'services/search-bus';
    postAjax(url, param, function (res) {
        $('.route-search-result-items').html(res);
        $('.rwd-route-search-result').html(res);
        $('#altmsg').css({display: 'none'});
        $('#pricesort').trigger('click');
        $('#m_pricesort').trigger('click');
        setPrice15(param);
    });
    
    
    
    $(document).on("click",".tab_click",function() {
        var bus_id = $(this).data("bus_id");
        $('#rdetail_'+bus_id).css({display:'block'});
    });
    
    $(document).on("click",".view-seats",function() {
        var obj = $(this);
        var bus_id = $(this).data("bus_id");
        $('.view-seats').each(function(){
            var o_bus = $(this).data("bus_id");
            if($(this).attr("data-map") != 0 && bus_id != o_bus){
                $(this).attr("data-map",2);
            }
        });
        
        var is_opened = $(this).attr("data-map");
        $('.route-item-detail').children().css({display:'none'});
        $('#rdetail_'+bus_id).css({display:'none'});
        $('.tab-content').css({display:'none'});
        $('.route-item').removeClass('active');
        if(is_opened == 2)
        {
            obj.parent().parent().next().children().css({display:'block'});
            obj.parent().parent().addClass('active');
//            $('#rdetail_'+bus_id).css({display:'block'});
            $(this).attr('data-map',1);
            return false;
        }else if(is_opened == 1){
            obj.parent().parent().next().children().css({display:'none'});
            $('#rdetail_'+bus_id).css({display:'none'});
            obj.parent().parent().removeClass('active');
            $(this).attr('data-map',2);
            return false;
        }
        obj.css({display:'none'});
        $('#loader_'+bus_id).css({display:'block'});
        var nop = $('#total_person').val();
        var date = $('#date').val();
        var url = getBaseURL() + 'services/get-seat-map';
        var param = {"bus_id" : bus_id,date:date,nop:nop};
        postAjax(url, param, function (res) {
            if(typeof res.flag !== 'undefined' && res.flag != 1){
                alert(res.msg);
                window.location = $('#base_url').val();
            }
            obj.css({display:'block'});
            $('#loader_'+bus_id).css({display:'none'});
            obj.attr('data-map',1);
//            $('.route-search-result-items').html(res);
            obj.parent().parent().addClass('active');
            obj.parent().parent().next('.route-item-detail').html(res);
            $('#sp_id_s_'+bus_id).val($('#sp_id_'+bus_id).val());
            $('#sp_name_s_'+bus_id).val($('#sp_name_'+bus_id).val());
            $('#bus_name_s_'+bus_id).val($('#bus_name_'+bus_id).val());
            var per_head_f = $('#fare_format_'+bus_id).val();
            $('#per_head_'+bus_id).html('Rp '+per_head_f);
            $('#fare_total_f_'+bus_id).html('Rp '+per_head_f);
            $('#altmsg').css({display: 'none'});
            $('#sess_id_'+bus_id).val($('#session_id').val());
            var board = JSON.parse($('#board_points_'+bus_id).val());
            var drop = JSON.parse($('#drop_points_'+bus_id).val());
            var bop = '';
            var dop = '';
            for(var i in board){
                bop += '<option value="'+board[i]['b_terminal_id']+'">'+board[i]['b_name']+'</option>';
            }
            for(var i in drop){
                dop += '<option value="'+drop[i]['d_terminal_id']+'">'+drop[i]['d_name']+'</option>';
            }
            $('#board_'+bus_id).append(bop);
            $('#drop_'+bus_id).append(dop);
        });
    });
    
    $(document).on("click",".select_seat",function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($('.total_seat_'+bus_id).html());
        var nop = $('#total_person').val();
        var old_lbl = $('.total_seat_lable_'+bus_id).html().trim();
        var label = $(this).data("seat_label");
        var idx = $(this).data("seat_index");
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var total_fare_f;
        var total_fare;
        var total_seat;
        
        $('#per_head_fare_'+bus_id).val(per_head);
        if($(this).hasClass('icon-seat-green')){
            var new_lbl = old_lbl.replace(','+label,'');
            var new_lbl = new_lbl.replace(label+',','');
            var new_lbl = new_lbl.replace(label,'');
            var new_lbl = new_lbl.replace(',,','');
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $(this).removeClass('icon-seat-green').addClass('icon-seat-white');
            $('.total_seat_'+bus_id).html((cnt)-1);
            var nc = parseInt(cnt)-1;
            
            if(nc == 0){
                var new_lbl = new_lbl.replace(',','');
                $('.total_seat_lable_'+bus_id).html(new_lbl);
                $('#form_'+bus_id).css({display:'none'});
                $('#form_'+bus_id).parsley().reset();
                $('.bus_'+bus_id).attr('id','add_pass_'+bus_id+'_'+1);
                $('.bus_'+bus_id).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(1)});
                return;
            }
            total_fare_f = (per_head * nc/1000).toFixed(3);
            total_fare = (per_head * nc);
            total_seat = nc;
            $('.seat_label').each(function(){
                var lbl =  $(this).val();
                console.log(lbl);
                if(lbl == label){
                    var rm_id = $(this).parent().parent('div').attr('id');
                    console.log(rm_id);
                    $('#'+rm_id).remove();
                }
            });
            var jc = 1;
            $('.bus_'+bus_id).each(function(){
                $(this).attr('id','add_pass_'+bus_id+'_'+jc);
                $('#add_pass_'+bus_id+'_'+jc).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(jc)});
                jc++;
            });
//            $('#add_pass_'+bus_id+'_'+cnt).remove();
        }else{
            var new_lbl = old_lbl =='' ?  label : old_lbl + ','+ label;
            
            if(cnt == 4 || (cnt+1) > nop){
                alert('you can not select more seats.');
                return false;
            }
            total_fare_f = (per_head * (cnt+1)/1000).toFixed(3);
            total_fare = (per_head * (cnt+1));
            total_seat = cnt+1;
            $(this).removeClass('icon-seat-white').addClass('icon-seat-green');
            $('.total_seat_'+bus_id).html((cnt)+1);
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $('#form_'+bus_id).css({display:'block'});
            $('#add_pass_'+bus_id+'_'+cnt).clone().attr({'id':'add_pass_'+bus_id+'_'+(cnt+1)}).insertAfter($('#add_pass_'+bus_id+'_'+cnt));
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(cnt+1)});
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_label').val(label);
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_index').val(idx);
            $('#form_'+bus_id).parsley();
            $('.dob').inputmask();
        }
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
    });
    $(document).on('change','.select_seat_drop',function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($(this).val());
        $('#form_'+bus_id).css({display:'block'});
        $('.total_seat_'+bus_id).html((cnt));
        $('.total_seat_'+bus_id).css({display:'block'});
        $('#form_'+bus_id).parsley().reset();
        $('#form_'+bus_id).parsley();
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var total_fare_f;
        var total_fare;
        var total_seat;
        total_fare_f = (per_head * cnt/1000).toFixed(3);
        total_fare = (per_head * cnt);
        total_seat = cnt;
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#per_head_fare_'+bus_id).val(per_head);
        if(cnt == 1){
            $('#add_pass_'+bus_id+'_'+2).remove();
            $('#add_pass_'+bus_id+'_'+3).remove();
            $('#add_pass_'+bus_id+'_'+4).remove();
        }
       
        for(var i=1;i<=cnt;i++){
            var ex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+i)).length;
            if(ex == 0){
                $('#add_pass_'+bus_id+'_'+(i-1)).clone().attr({'id':'add_pass_'+bus_id+'_'+(i)}).insertAfter($('#add_pass_'+bus_id+'_'+(i-1)));
                $('#add_pass_'+bus_id+'_'+(i)).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(i)});
            }
            var rex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+(cnt+i))).length;
            if(rex){
                $('#add_pass_'+bus_id+'_'+(cnt+i)).remove();
            }
        }
        $('.dob').inputmask();
//        $(".dob").datepicker();
    });
    
    $(document).on('change keyup','.copy_to_pass',function(){
        var bus_id = $(this).data('bus_id');
        if($('.copy_'+bus_id).is(':checked')){
            var main = $('#add_pass_'+bus_id+'_1');
            var val = $(this).val();
            if($(this).attr('id') == 'booker_name'){
                main.find('#pass_name').val(val);
            }
            if($(this).attr('id') == 'booker_dob'){
                main.find('#pass_dob').val(val);
            }
            if($(this).attr('id') == 'booker_mo'){
                main.find('#pass_mo').val(val);
            }
        }
    });
    
    
    /*var toDay = new Date();
        var d = new Date();
        d.setDate(toDay.getDate()+3);
	$(".dob").datepicker({
//		dateFormat: 'dd MM, yy'
		dateFormat: 'yy-mm-dd',
                minDate:    d,
	}).datepicker( "setDate", d);*/
    
    
});
function setPrice15(param){
    var url15 = getBaseURL() + 'services/price-for15';
    postAjax(url15, param, function (res) {
        console.log(res)
//        console.log(window.location.href);
//        console.log(window.location.href.replace(/\d{4}-\d{2}-\d{2}/g,'2017-09-12'));
        
        if(res.flag == 1){
            for(x in res.data){
                var redir_url = window.location.href.replace(/\d{4}-\d{2}-\d{2}/g , res.data[x].date);
                var dt = x ;
                var pr = res.data[x].price;
                console.log(pr);
                if(pr == '-'){
//                    x = '';
                }else{
                    pr = 'Rp '+pr;
                }
                $('#price_15').append('<div class="cari-tanggal-item" style="padding:10px;"><a href="'+redir_url+'" style="color:black;"><span class="cari-tanggal-title">'+x+'</span><span class="cari-tanggal-price">'+pr+'</span></a></div>');
    
            }
            $("#price_15").addClass('route-filter-cari-tanggal-slider');
            $("#price_15").addClass('owl-carousel');
            $(".route-filter-cari-tanggal-slider").owlCarousel({
                    mouseDrag: true,
                    margin: 0,
//                    loop: true,
                    loop: false,
//                    autoplay: true,
                    autoplay: false,
                    autoplayHoverPaus: true,
                    autoplaySpeed: 500,
                    autoplayTimeout: 5000,
                    touchDrag: true,
                    responsiveClass: true,
                    responsive: {
                            0:{
                                    items: 1
                            },
                            400:{
                                    items: 1
                            },
                            600: {
                                    items: 2
                            },
                            1000: {
                                    items: 7
                            }
                            },
                    nav: true,
                    navText: [
                            "<span class='sprite icon-chevron-left'></span>",
                            "<span class='sprite icon-chevron-right'></span>"
                    ],
            });
            $("#price_15").addClass('owl-hidden');
        }
        else if(res.flag == 0){
            $('#price_15').append('<h2>'+res.msg+'</h2>');
        }
        
        $('#altmsg').css({display: 'none'});
        $('#more_date').text('Cari Tanggal lain');
    });
}
function setBoard(obj){
    var bus_id = $(obj).data('bus_id');
    var id = $(obj).val();
    var board = JSON.parse($('#board_points_'+bus_id).val());
    for(var i in board){
        if(board[i]['b_terminal_id'] == id){
            $('#f_terminal_'+bus_id).html(board[i]['b_name']+'('+board[i]['boarding_datetime']+')');
            $('#f_city_'+bus_id).html(board[i]['b_city_name']+',');
            $('#f_dist_'+bus_id).html(board[i]['b_district_name']);
            $('#board_point_'+bus_id).val(JSON.stringify(board[i]));
        }
    }
    $('#drop_'+bus_id).val('');
    
}
function setDrop(obj){
    var bus_id = $(obj).data('bus_id');
    var id = $(obj).val();
    var drop = JSON.parse($('#drop_points_'+bus_id).val());
    for(var i in drop){
        if(drop[i]['d_terminal_id'] == id){
            $('#t_terminal_'+bus_id).html(drop[i]['d_name']+'('+drop[i]['droping_datetime']+'),<br>'+drop[i]['d_city_name']+',');
            $('#t_dist_'+bus_id).html(drop[i]['d_district_name']);
            $('#drop_point_'+bus_id).val(JSON.stringify(drop[i]));
        }
    }
    var board_id = $('#board_'+bus_id+' :selected').val();
    var nop = $('#total_person').val();
    var date = $('#date').val();
    var data = {from_terminal_id:board_id,to_terminal_id:id,nop:nop,bus_id:bus_id,date:date};
    var url = getBaseURL() + 'services/get-price-by-terminal';
    if(board_id > 0){
        postAjax(url,data,function(res){
//            console.log(res);
            if(res.flag == 1){
                var pr = parseFloat(res.data);
                $('#pricing_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                $('#fare_'+bus_id).val(res.data);
                $('#per_head_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                if($('.select_seat_drop :selected').val()){
                    $('.select_seat_drop').trigger('change');
                }
                var ts = $('#total_seat_'+bus_id).val();
                if(ts != ''){
                    ts = parseInt(ts);
                    $('#per_head_fare_'+bus_id).val(pr);
                    $('#total_amount_'+bus_id).val(pr*ts);
                    $('#fare_total_f_'+bus_id).html('Rp '+(pr*ts/1000).toFixed(3));
                }else{
                    $('#fare_total_f_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                }
            }
        });
    }
}
function validate(bus_id){
    console.log(bus_id);
    
    var b = $('#board_'+bus_id+' :selected').val();
    var d = $('#drop_'+bus_id+' :selected').val();
    if(b == ''){
        alert('select boarding point');
        return false;
    }
    if(d == ''){
        alert('select droping point');
        return false;
    }
//    var cnt = 0;
//    $('#form_'+bus_id).find('.mobile_val').each(function(){
//        var con = $(this).val();
//        if(con != ''){
//            var two = con.substring(0, 2);
//            var three = con.substring(0, 3);
//            if(two == '62'){
//                $(this).val('+'+con);
//            }else if(three != '+62'){
//                cnt++;
//            }
//        }
//    });
//    if(cnt){
//        alert('mobile number must start with +62');
//        return false;
//    }
    return true;
}



/*** responsive booking  ***/

$(document).ready(function(){
    $(document).on("click",".view_seat_res",function(){
        var obj = $(this);
        
        var bus_id = $(this).data("bus_id");
        
        $('.view_seat_res').each(function(){
            var o_bus = $(this).data("bus_id");
            if($(this).attr("data-map") != 0 && bus_id != o_bus){
                $(this).attr("data-map",2);
            }
        });
        var is_opened = $(this).attr("data-map");
        $('.route-item-detail').children().css({display:'none'});
        $('.route_amenity').css({display:'none'});
        $('#route_amenity_'+bus_id).css({display:'none'});
        if(is_opened == 2)
        {
            obj.parent().prev().children().css({display:'block'});
            $('#route_amenity_'+bus_id).css({display:'block'});
            $(this).attr('data-map',1);
            return false;
        }else if(is_opened == 1){
            obj.parent().prev().children().css({display:'none'});
            $('#route_amenity_'+bus_id).css({display:'none'});
            $(this).attr('data-map',2);
            return false;
        }
        
        $('#route_amenity_'+bus_id).css({display:'block'});
        var date = $('#date').val();
        var nop = $('#mhnop').val();
        var url = getBaseURL() + 'services/get-seat-map';
        var param = {"bus_id" : bus_id,date:date,nop:nop};
        postAjax(url, param, function (res) {
            if(typeof res.flag !== 'undefined' && res.flag != 1){
                alert(res.msg);
                window.location = $('#base_url').val();
            }
            obj.attr('data-map',1);
            obj.parent().prev('.route-item-detail').html(res);
            $('#sp_id_s_'+bus_id).val($('#sp_id_'+bus_id).val());
            $('#sp_name_s_'+bus_id).val($('#sp_name_'+bus_id).val());
            $('#bus_name_s_'+bus_id).val($('#bus_name_'+bus_id).val());
            var per_head_f = $('#fare_format_'+bus_id).val();
            $('#per_head_'+bus_id).html('Rp '+per_head_f);
            $('#fare_total_f_'+bus_id).html('Rp '+per_head_f);
            $('#altmsg').css({display: 'none'});
            $('#sess_id_'+bus_id).val($('#session_id').val());
            var board = JSON.parse($('#board_points_'+bus_id).val());
            var drop = JSON.parse($('#drop_points_'+bus_id).val());
            var bop = '';
            var dop = '';
            for(var i in board){
                bop += '<option value="'+board[i]['b_terminal_id']+'">'+board[i]['b_name']+'</option>';
            }
            for(var i in drop){
                dop += '<option value="'+drop[i]['d_terminal_id']+'">'+drop[i]['d_name']+'</option>';
            }
            $('#board_'+bus_id).append(bop);
            $('#drop_'+bus_id).append(dop);
        });
    });
    
    $(document).on("click",".select_seat_res",function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($('.total_seat_'+bus_id).html());
        var old_lbl = $('.total_seat_lable_'+bus_id).html().trim();
        var label = $(this).data("seat_label");
        var idx = $(this).data("seat_index");
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var nop = $('#mhnop').val();
        var total_fare_f;
        var total_fare;
        var total_seat;
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#per_head_fare_'+bus_id).val(per_head);
        if($(this).hasClass('icon-rwd-seat-green')){
            var new_lbl = old_lbl.replace(','+label,'');
            var new_lbl = new_lbl.replace(label+',','');
            var new_lbl = new_lbl.replace(label,'');
            var new_lbl = new_lbl.replace(',,','');
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $(this).removeClass('icon-rwd-seat-green').addClass('icon-rwd-seat-white');
            $('.total_seat_'+bus_id).html((cnt)-1);
            var nc = parseInt(cnt)-1;
            
            if(nc == 0){
                $('#route_detail_'+bus_id).css({display:'none'});
                var new_lbl = new_lbl.replace(',','');
                $('.total_seat_lable_'+bus_id).html(new_lbl);
                $('#form_'+bus_id).css({display:'none'});
                $('#form_'+bus_id).parsley().reset();
                $('.bus_'+bus_id).attr('id','add_pass_'+bus_id+'_'+1);
                $('.bus_'+bus_id).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(1)});
                return;
            }
            total_fare_f = (per_head * nc/1000).toFixed(3);
            total_fare = (per_head * nc);
            total_seat = nc;
            $('.seat_label').each(function(){
                var lbl =  $(this).val();
                console.log(lbl);
                if(lbl == label){
                    var rm_id = $(this).parent().parent('div').attr('id');
                    console.log(rm_id);
                    $('#'+rm_id).remove();
                }
            });
            var jc = 1;
            $('.bus_'+bus_id).each(function(){
                $(this).attr('id','add_pass_'+bus_id+'_'+jc);
                $('#add_pass_'+bus_id+'_'+jc).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(jc)});
                jc++;
            });
//            $('#add_pass_'+bus_id+'_'+cnt).remove();
        }else{
            $('#route_detail_'+bus_id).css({display:'block'});
            var new_lbl = old_lbl =='' ?  label : old_lbl + ','+ label;
            
            if(cnt == 4 || (cnt+1) > nop){
                alert('you can not select more seats.');
                return false;
            }
            total_fare_f = (per_head * (cnt+1)/1000).toFixed(3);
            total_fare = (per_head * (cnt+1));
            total_seat = cnt+1;
            $(this).removeClass('icon-rwd-seat-white').addClass('icon-rwd-seat-green');
            $('.total_seat_'+bus_id).html((cnt)+1);
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $('#form_'+bus_id).css({display:'block'});
            $('#add_pass_'+bus_id+'_'+cnt).clone().attr({'id':'add_pass_'+bus_id+'_'+(cnt+1)}).insertAfter($('#add_pass_'+bus_id+'_'+cnt));
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(cnt+1)});
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_label').val(label);
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_index').val(idx);
            $('#form_'+bus_id).parsley();
            $('.dob').inputmask();
        }
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
        $('select').dropdown();
    });
    $(document).on('change','.select_seat_drop_res',function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($(this).val());
        $('#route_detail_'+bus_id).css({display:'block'});
        $('#form_'+bus_id).css({display:'block'});
        $('.total_seat_'+bus_id).html((cnt));
        $('.total_seat_label_'+bus_id).html((cnt));
        $('#form_'+bus_id).parsley().reset();
        $('#form_'+bus_id).parsley();
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var total_fare_f;
        var total_fare;
        var total_seat;
        total_fare_f = (per_head * cnt/1000).toFixed(3);
        total_fare = (per_head * cnt);
        total_seat = cnt;
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#per_head_fare_'+bus_id).val(per_head);
        if(cnt == 1){
            $('#add_pass_'+bus_id+'_'+2).remove();
            $('#add_pass_'+bus_id+'_'+3).remove();
            $('#add_pass_'+bus_id+'_'+4).remove();
        }
       
        for(var i=1;i<=cnt;i++){
            var ex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+i)).length;
            if(ex == 0){
                $('#add_pass_'+bus_id+'_'+(i-1)).clone().attr({'id':'add_pass_'+bus_id+'_'+(i)}).insertAfter($('#add_pass_'+bus_id+'_'+(i-1)));
                $('#add_pass_'+bus_id+'_'+(i)).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(i)});
            }
            var rex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+(cnt+i))).length;
            if(rex){
                $('#add_pass_'+bus_id+'_'+(cnt+i)).remove();
            }
        }
        $('.dob').inputmask();
//        $(".dob").datepicker();
    });
    
    $('#mdate').change(function(){
        var from = $('#mhfrom').val();
        var to = $('#mhto').val();
        var nop = $('#mhnop').val();
        var date = $(this).val();
        var url = getBaseURL() + 'search-bus/'+from+'.'+to+'.'+date+'.'+nop;
        window.location = url;
    });
    $('#next_date').click(function(){
        var cur = $('#date').val();
        var arr = cur.split('-')
        var dat = new Date(arr[0],(arr[1]-1),arr[2]);
//        console.log(arr);
        var d = new Date(arr[0],(arr[1]-1),arr[2]);
        d.setDate(dat.getDate()+1);
        var newd = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        console.log(newd);
        var from = $('#mhfrom').val();
        var to = $('#mhto').val();
        var nop = $('#mhnop').val();
        var url = getBaseURL() + 'search-bus/'+from+'.'+to+'.'+newd+'.'+nop;
        window.location = url;
    });
    $('#prev_date').click(function(){
        var from = $('#mhfrom').val();
        var to = $('#mhto').val();
        var nop = $('#mhnop').val();
        
        var cur = $('#date').val();
        var arr = cur.split('-')
        var dat = new Date(arr[0],(arr[1]-1),arr[2]);
//        console.log(arr);
        var d = new Date(arr[0],(arr[1]-1),arr[2]-1);
//        d.setDate(dat.getDate()-1);
        
        var today = new Date();
        var afterThreeD = new Date(today.getFullYear(),today.getMonth(),today.getDate()+3);

        if(d < afterThreeD){
            alert('please select after three day date');
            var url = getBaseURL() + 'search-bus/'+from+'.'+to+'.'+cur+'.'+nop;
//            window.location = url;
            return false;
        }
        
        var newd = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
//        console.log(newd);
       
        var url = getBaseURL() + 'search-bus/'+from+'.'+to+'.'+newd+'.'+nop;
        window.location = url;
    });
    
    $('#more_date').on('click', function(event){
        if($(this).parent().hasClass('active')){
            $(this).parent().removeClass('active');
            $('#moredateprice').css('display','none');
        }
        else{
            $(this).parent().addClass('active');
            $('#moredateprice').css('display','block');
        }
    });
    
    //----------------- For Sorting -----------------------------------------//
    
    $('#opsort').click(function(){
        if(osrt == 'asc'){
            
            $('.sp_name').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.sp_name').text() < $(b).children().children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('up');
            $(this).removeClass('down');
            osrt = 'dsc';
        }else if(osrt == 'dsc'){
            $('.sp_name').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.sp_name').text() > $(b).children().children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('down');
            $(this).removeClass('up');
            osrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#boardsort').click(function(){
        if(bsrt == 'asc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() < $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            bsrt = 'dsc';
        }else if(bsrt == 'dsc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() > $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            bsrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#dropsort').click(function(){
        if(dsrt == 'asc'){
            
            $('.drop_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.drop_time').text() < $(b).children().children().children().children().children('.drop_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            dsrt = 'dsc';
        }else if(dsrt == 'dsc'){
            
            $('.drop_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.drop_time').text() > $(b).children().children().children().children().children('.drop_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            dsrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#ratesort').click(function(){
        if(rsrt == 'asc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() < $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            rsrt = 'dsc';
        }else if(rsrt == 'dsc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() > $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            rsrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#availsort').click(function(){
        if(asrt == 'asc'){
            
            $('.avail_seat').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.avail_seat').text() < $(b).children().children().children().children().children('.avail_seat').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            asrt = 'dsc';
        }else if(asrt == 'dsc'){
            
            $('.avail_seat').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.avail_seat').text() > $(b).children().children().children().children().children('.avail_seat').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            asrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#pricesort').click(function(){
//        console.log('Called');
        if(psrt == 'asc'){
            console.log(psrt);
            $('.fare').parent().parent().parent().sort(function(a, b) {
                console.log($(a).children().children().children('.fare').val()+' < '+$(b).children().children().children('.fare').val());
                if ($(a).children().children().children('.fare').val() < $(b).children().children().children('.fare').val()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            psrt = 'dsc';
        }else if(psrt == 'dsc'){
            console.log(psrt);
            $('.fare').parent().parent().parent().sort(function(a, b) {
                console.log($(a).children().children().children('.fare').val()+' < '+$(b).children().children().children('.fare').val());
                if ($(a).children().children().children('.fare').val() > $(b).children().children().children('.fare').val()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            psrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });

    //----------------- For Sorting Over -------------------------------------//
    
    
    //----------------- For Responsive Sorting -------------------------------//
    
    $('#m_opsort').click(function(){
        $('.route-filter-search-result-item').removeClass('up');
        $('.route-filter-search-result-item').removeClass('down');
        if(osrt == 'asc'){
            
            $('.sp_name').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children('.sp_name').text() < $(b).children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('up');
            $(this).removeClass('down');
            osrt = 'dsc';
        }else if(osrt == 'dsc'){
            $('.sp_name').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.sp_name').text() > $(b).children().children().children().children('.sp_name').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            $(this).addClass('down');
            $(this).removeClass('up');
            osrt = 'asc';
        }
//        $(this).siblings().removeClass('up');
//        $(this).siblings().removeClass('down');
    });
    $('#m_boardsort').click(function(){
        $('.route-filter-search-result-item').removeClass('up');
        $('.route-filter-search-result-item').removeClass('down');
        if(bsrt == 'asc'){
            
            $('.board_time').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.board_time').text() < $(b).children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            bsrt = 'dsc';
        }else if(bsrt == 'dsc'){
            
            $('.board_time').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.board_time').text() > $(b).children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            bsrt = 'asc';
        }
//        $(this).siblings().removeClass('up');
//        $(this).siblings().removeClass('down');
    });
    $('#m_dropsort').click(function(){
        $('.route-filter-search-result-item').removeClass('up');
        $('.route-filter-search-result-item').removeClass('down');
        if(dsrt == 'asc'){
            
            $('.drop_time').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.drop_time').text() < $(b).children().children().children().children('.drop_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            dsrt = 'dsc';
        }else if(dsrt == 'dsc'){
            
            $('.drop_time').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.drop_time').text() > $(b).children().children().children().children('.drop_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            dsrt = 'asc';
        }
//        $(this).siblings().removeClass('up');
//        $(this).siblings().removeClass('down');
    });
    $('#m_ratesort').click(function(){
        if(rsrt == 'asc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() < $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            rsrt = 'dsc';
        }else if(rsrt == 'dsc'){
            
            $('.board_time').parent().parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children().children('.board_time').text() > $(b).children().children().children().children().children('.board_time').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            rsrt = 'asc';
        }
        $(this).siblings().removeClass('up');
        $(this).siblings().removeClass('down');
    });
    $('#m_availsort').click(function(){
        $('.route-filter-search-result-item').removeClass('up');
        $('.route-filter-search-result-item').removeClass('down');
        if(asrt == 'asc'){
            
            $('.avail_seat').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.avail_seat').text() < $(b).children().children().children().children('.avail_seat').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            asrt = 'dsc';
        }else if(asrt == 'dsc'){
            
            $('.avail_seat').parent().parent().parent().parent().sort(function(a, b) {
//                console.log($(a).children().children().children().children('.sp_name').text() < $(b).children().children().children().children('.sp_name').text());
                if ($(a).children().children().children().children('.avail_seat').text() > $(b).children().children().children().children('.avail_seat').text()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            asrt = 'asc';
        }
//        $(this).siblings().removeClass('up');
//        $(this).siblings().removeClass('down');
    });
    $('#m_pricesort').click(function(){
//        console.log('Called');
        $('.route-filter-search-result-item').removeClass('up');
        $('.route-filter-search-result-item').removeClass('down');
        if(psrt == 'asc'){
//            console.log(psrt);
            $('.fare').parent().parent().sort(function(a, b) {
//                console.log($(a).children().children('.fare').val()+' < '+$(b).children().children('.fare').val());
                if ($(a).children().children('.fare').val() < $(b).children().children('.fare').val()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('up');
            $(this).removeClass('down');
            psrt = 'dsc';
        }else if(psrt == 'dsc'){
//            console.log(psrt);
            $('.fare').parent().parent().sort(function(a, b) {
//                console.log($(a).children().children('.fare').val()+' < '+$(b).children().children('.fare').val());
                if ($(a).children().children('.fare').val() > $(b).children().children('.fare').val()) {
                    return -1;
                } else {
                    return 1;
                }
            }).appendTo('#searchresult');
            
            $(this).addClass('down');
            $(this).removeClass('up');
            psrt = 'asc';
        }
//        $(this).siblings().removeClass('up');
//        $(this).siblings().removeClass('down');
    });

    //----------------- For Responsive Sorting Over --------------------------//
    
    
    //------------------- For Filtering ----------------------------------//

    $( '#splist a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = sp_filter.indexOf( val ) ) > -1 ) {
                    sp_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    sp_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( sp_filter );
            
            return false;
    });

    
    
    $( '#typelist a' ).on( 'click', function( event ) {
            console.log( type_filter );
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = type_filter.indexOf( val ) ) > -1 ) {
                    type_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    type_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( type_filter );
            if($('div.showin-995#searchresult').css('display') !='undefined' && $('div.showin-995#searchresult').css('display') == 'block'){
                m_getFilter();
                return false;
            }
            getFilter();
            return false;
    });
    
    $( '#boardlist a' ).on( 'click', function( event ) {

            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = board_filter.indexOf( val ) ) > -1 ) {
                    board_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    board_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( board_filter );
            if($('div.showin-995#searchresult').css('display') !='undefined' && $('div.showin-995#searchresult').css('display') == 'block'){
                m_getFilter();
                return false;
            }
            getFilter();
            return false;
    });
    
    $( '#droplist a' ).on( 'click', function( event ) {

            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = drop_filter.indexOf( val ) ) > -1 ) {
                    drop_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    drop_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( drop_filter );
            if($('div.showin-995#searchresult').css('display') !='undefined' && $('div.showin-995#searchresult').css('display') == 'block'){
                m_getFilter();
                return false;
            }
            getFilter();
            return false;
    });
    
    //------------------- For Filtering Over -----------------------------//
});

function getFilter(){
    var n=$('#searchresult').find('div.route-item.clearfix');

    for(i=0;i<n.length;i++){
        var sp = $(n[i]).find('span.sp_name')[0].textContent;
        var tp = $(n[i]).find('input.bustype')[0].value;
        var bt = $(n[i]).find('span.board_time')[0].textContent;
        var dt = $(n[i]).find('span.drop_time')[0].textContent;

        var sp_flag=0;
        var tp_flag=0;
        var bt_flag=0;
        var dt_flag=0;

        for(y=0;y<sp_filter.length;y++){
//                    console.log( sp_filter );
//                    console.log(sp+' : '+sp_filter[y]);
//                    console.log(sp_filter[y]);
            if(sp == sp_filter[y]){
//                        console.log('In SP If');
                sp_flag=1;
                break;
            }
            else{
//                        console.log('In SP Else');
                sp_flag=0;
            }
        }

        for(y=0;y<type_filter.length;y++){
//                    console.log( type_filter );
//                    console.log(tn+" : "+type_filter[y]);
            if(tp == type_filter[y]){
//                        console.log('In TP If');
                tp_flag=1;
                break;
            }
            else{
//                        console.log('In TP Else');
                tp_flag=0;
            }
        }
        for(y=0;y<board_filter.length;y++){
//                    console.log( board_filter );
            if(board_filter[y] == 'Pagi'){
//                        console.log("04:00 <= "+bt+" <= 11:00");
                if(bt >= '04:00' && bt <= '11:00'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Siang'){
//                        console.log("11:00 <= "+bt+" <= 15:00");
                if(bt >= '11:00' && bt <= '15:00'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Sore'){
//                        console.log("15:00 <= "+bt+" <= 18:30");
                if(bt >= '15:00' && bt <= '18:30'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Malam'){
//                        console.log("18:30 <= "+bt+" <= 04:00");
                if((bt >= '18:30' && bt <= '24:59') || (bt >= '00:00' && bt <= '04:00')){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }

        }

        for(y=0;y<drop_filter.length;y++){
//                    console.log( drop_filter );
//                    console.log(st+" : "+board_filter[y]);
            if(drop_filter[y] == 'Pagi'){
//                        console.log("04:00 <= "+dt+" <= 11:00");
                if(dt >= '04:00' && dt <= '11:00'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Siang'){
//                        console.log("11:00 <= "+dt+" <= 15:00");
                if(dt >= '11:00' && dt <= '15:00'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Sore'){
//                        console.log("15:00 <= "+dt+" <= 18:30");
                if(dt >= '15:00' && dt <= '18:30'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Malam'){
//                        console.log("18:30 <= "+dt+" <= 04:00");
                if((dt >= '18:30' && dt <= '24:59') || (dt >= '00:00' && dt <= '04:00')){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }

        }

//                console.log("sp_flag : "+sp_flag+", tp_flag : "+tp_flag+", bt_flag : "+bt_flag+", dt_flag : "+dt_flag);
        if(sp_flag==1 && tp_flag==1 && bt_flag==1 && dt_flag==1){
            $(n[i]).parent().css('display','block');
        }
        else{
            $(n[i]).parent().css('display','none');
        }
    }
}

function m_getFilter(){
//    alert('Called');
    var n=$('#searchresult').find('div.rwd-booking-kursi.clearfix');

    for(i=0;i<n.length;i++){
        var sp = $(n[i]).find('h3.sp_name')[0].textContent;
        var tp = $(n[i]).find('input.bustype')[0].value;
        var bt = $(n[i]).find('span.board_time')[0].textContent;
        var dt = $(n[i]).find('span.drop_time')[0].textContent;

        var sp_flag=0;
        var tp_flag=0;
        var bt_flag=0;
        var dt_flag=0;

        for(y=0;y<sp_filter.length;y++){
//                    console.log( sp_filter );
//                    console.log(sp+' : '+sp_filter[y]);
//                    console.log(sp_filter[y]);
            if(sp == sp_filter[y]){
//                        console.log('In SP If');
                sp_flag=1;
                break;
            }
            else{
//                        console.log('In SP Else');
                sp_flag=0;
            }
        }

        for(y=0;y<type_filter.length;y++){
//                    console.log( type_filter );
//                    console.log(tn+" : "+type_filter[y]);
            if(tp == type_filter[y]){
//                        console.log('In TP If');
                tp_flag=1;
                break;
            }
            else{
//                        console.log('In TP Else');
                tp_flag=0;
            }
        }
        for(y=0;y<board_filter.length;y++){
//                    console.log( board_filter );
            if(board_filter[y] == 'Pagi'){
//                        console.log("04:00 <= "+bt+" <= 11:00");
                if(bt >= '04:00' && bt <= '11:00'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Siang'){
//                        console.log("11:00 <= "+bt+" <= 15:00");
                if(bt >= '11:00' && bt <= '15:00'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Sore'){
//                        console.log("15:00 <= "+bt+" <= 18:30");
                if(bt >= '15:00' && bt <= '18:30'){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }
            if(board_filter[y] == 'Malam'){
//                        console.log("18:30 <= "+bt+" <= 04:00");
                if((bt >= '18:30' && bt <= '24:59') || (bt >= '00:00' && bt <= '04:00')){
                    bt_flag=1;
                    break;
                }
                else{
                    bt_flag=0;
                }
            }

        }

        for(y=0;y<drop_filter.length;y++){
//                    console.log( drop_filter );
//                    console.log(st+" : "+board_filter[y]);
            if(drop_filter[y] == 'Pagi'){
//                        console.log("04:00 <= "+dt+" <= 11:00");
                if(dt >= '04:00' && dt <= '11:00'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Siang'){
//                        console.log("11:00 <= "+dt+" <= 15:00");
                if(dt >= '11:00' && dt <= '15:00'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Sore'){
//                        console.log("15:00 <= "+dt+" <= 18:30");
                if(dt >= '15:00' && dt <= '18:30'){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }
            if(drop_filter[y] == 'Malam'){
//                        console.log("18:30 <= "+dt+" <= 04:00");
                if((dt >= '18:30' && dt <= '24:59') || (dt >= '00:00' && dt <= '04:00')){
                    dt_flag=1;
                    break;
                }
                else{
                    dt_flag=0;
                }
            }

        }

                console.log("sp_flag : "+sp_flag+", tp_flag : "+tp_flag+", bt_flag : "+bt_flag+", dt_flag : "+dt_flag);
        if(sp_flag==1 && tp_flag==1 && bt_flag==1 && dt_flag==1){
            $(n[i]).css('display','block');
        }
        else{
            $(n[i]).css('display','none');
        }
    }
}