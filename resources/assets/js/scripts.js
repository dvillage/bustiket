// JavaScript Document
(function($) {
	"use strict";

	// rwd toogle navigation
	$('.menu-rwd').on('show.bs.collapse', function() {
		$("body").on( 'click', function() {
			$('.menu-rwd').collapse("hide");
		});
	})
	// var btnRwdToggle = $('.site-header-rwd .btn-rwd-show');
	// btnRwdToggle.on( 'click', function() {
		// var openMenu = ( 'active' );
		// var parent = $(this).parent('.site-header-rwd');
		// var parentMenu = parent.find( '.menu-rwd');
		
		// parentMenu.toggleClass( 'active' );

	// });

	// rwd toogle remove navigation
	// $('html, body').on( 'click', function(e) {
	// 	if ( $(e.target).hasClass('menu-rwd') ) {
	// 		console.log('ini di click didalam menu');
	// 		return false;
	// 	} else if ( $(e.target).hasClass('menu-rwd-header') ){
	// 		console.log('ini di click didalam menu');
	// 		return false;
	// 	} else if ( $(e.target).hasClass('menu-rwd-nav') ){
	// 		console.log('ini di click didalam menu');
	// 		return false;
	// 	} else if ( $(e.target).hasClass('btn-rwd-show') ){
	// 		console.log('ini di click didalam menu');
	// 		return false;
	// 	} 
	// 	var menuActive = $('.site-header-rwd .menu-rwd');

	// 	if ( menuActive.hasClass( 'active' ) ){
			// console.log('eek');
	// 		menuActive.removeClass( 'active' );
	// 	}

	// });

	// site-header sticky
	$(window).scroll(function() {
		if ($(this).scrollTop() > 200){
			$(".site-header-sticky").addClass("sticky");
		} else {
			$(".site-header-sticky").removeClass("sticky");
		}
	});

	$(window).load(function() {
		// hero-carousel
		$('.hero-carousel').flexslider({
			animation: 'fade',

			// primary controls
			controlNav: false,
			directionNav: false,
		});
	});

	// hero-carousel
	// $('.hero-carousel').owlCarousel({
	// 	animateIn: 'fadeIn',
	// 	animateOut: 'fadeOut',
	// 	items: 1,
	// 	smartSpeed: 450,
	// 	autoplay: false,
	// 	autoplayTimeout: 3000,
	// 	loop: true,
	// 	nav: false
	// });

	// cari-tiket
//	var hitCariTiket = $('.cari-tiket-field');
//	hitCariTiket.on( 'click', function() {
//		$(this)
//			.toggleClass('active');
//	})

        var hitCariTiket = $(".cari-tiket-field .cari-tiket-field-content input[type='text']");
	hitCariTiket.on( 'focus', function() {
		$(this).parents('.cari-tiket-field').toggleClass('active');
	});
        
        var hitCariTiket = $(".cari-tiket-field .cari-tiket-field-content span"); /* for pariwisata bus type dropdown do not remove it. */
	hitCariTiket.on( 'focus blur click', function() {
		$(this).parents('.cari-tiket-field').toggleClass('active');
	});


	// tiketFieldContent.on('focus', function() {
	// 	$(this).parent().addClass('hastext');
	// });

	// tiketFieldContent.on('blur', function() {
	// 	if ( $(this).val() == '') {
	// 		$(this).parent().removeClass('hastext');
	// 	}
	// });

	// setValue from list in BUS TAB-CONTENT
	$('#from-bus').on( 'focus blur', function(e) {

		$(this).parents('.field-content-bus').toggleClass('is-select', (e.type === 'focus' || this.value.length > 0));
	});

	$('#cari-tiket-bus li a').on( 'click', function(e) {
		e.preventDefault();
		$( '#from-bus').val( $(this).text() );	

		$(".floating-label input").each(function() {
			checkForInput(this);
		});

	});
	
	// setValue from list in PARIWISATA TAB-CONTENT
	$('#from-pariwisata').on( 'focus blur', function(e) {
		$(this).parents('.field-content-pariwisata').toggleClass('is-select', (e.type === 'focus' || this.value.length > 0));
	});
	$('#cari-tiket-pariwisata li a').on( 'click', function(e) {
		e.preventDefault();
		$( '#from-pariwisata').val( $(this).text() );

		$(".floating-label input").each(function() {
			checkForInput(this);
		});
	});


	// swap
	var btnSwapBus = $( '#bus-swap' );
	btnSwapBus.on( 'click', function(){
		$(this).toggleClass('rotate');

		var fromBus = $( '#from-bus' ).val();
		$( '#from-bus' ).val( $( '#to-bus' ).val());
		$( '#to-bus' ).val(fromBus);
	});

	var btnSwapPariwisata = $( '#pariwisata-swap' );
	btnSwapPariwisata.on( 'click', function(){
		$(this).toggleClass('rotate');

		var fromPariwisata = $( '#from-pariwisata' ).val();
		$( '#from-pariwisata' ).val( $( '#to-pariwisata' ).val());
		$( '#to-pariwisata' ).val(fromPariwisata);
	});

	// setValue from list in BUS TAB-CONTENT
	$('#cari-tiket-bus li a').on( 'click', function(e) {
		e.preventDefault();
		$( '#from-bus').val( $(this).text() );
	});
	
	// setValue from list in PARIWISATA TAB-CONTENT
	$('#cari-tiket-pariwisata li a').on( 'click', function(e) {
		e.preventDefault();
		$( '#from-pariwisata').val( $(this).text() );
	});


	// favorite-carousel
	$(".favorite-carousel").owlCarousel({
		mouseDrag: true,
		margin: 10,
		loop: true,
		autoplay: true,
		autoplayHoverPaus: true,
		autoplaySpeed: 500,
		autoplayTimeout: 5000,
		touchDrag: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 1
			},
			600: {
				items: 2
			},
			1000:{
				items: 4
			},
		},
		nav: true,
		navText: [
				"<span class='sprite icon-arrow-circle-left'></span>",
	        	"<span class='sprite icon-arrow-circle-right'></span>"
		],
	});

	// route-filter-cari-tanggal-slider
	$(".route-filter-cari-tanggal-slider").owlCarousel({
		mouseDrag: true,
		margin: 0,
		loop: true,
		autoplay: false,
		autoplayHoverPaus: true,
		autoplaySpeed: 500,
		autoplayTimeout: 5000,
		touchDrag: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 7
			}
			},
		nav: true,
		navText: [
				"<span class='sprite icon-chevron-left'></span>",
	        	"<span class='sprite icon-chevron-right'></span>"
		],
	});

	// foto-slider
	$(".foto-slider").owlCarousel({
		mouseDrag: true,
		margin: 15,
		loop: true,
		autoplay: true,
		autoplayHoverPaus: true,
		autoplaySpeed: 500,
		autoplayTimeout: 5000,
		touchDrag: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 3
			}
		},
		nav: true,
		navText: [
				"<span class='sprite icon-arrow-circle-left-small'></span>",
	        	"<span class='sprite icon-arrow-circle-right-small'></span>"
		],
	});

	// cta-carousel
	$('.cta-carousel').owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		items: 1,
		smartSpeed: 450,
		autoplay: false,
		autoplayTimeout: 3000,
		loop: true,
		nav: true,
		dots: true,
		dotsEach: true,
		navText: [
				"<span class='sprite icon-arrow-circle-left-small'></span>",
	        	"<span class='sprite icon-arrow-circle-right-small'></span>"
		],
	});

	// calendar
        $.datepicker.regional['ina'] = {
        monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        dayNamesShort: ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
        dayNamesMin: ['Mi', 'Se', 'Sa', 'Ra', 'Ka', 'Ju', 'Su'],
        dateFormat: 'dd MM, yy', firstDate: 0,
    }

	 $.datepicker.setDefaults($.datepicker.regional['ina']);  
        var toDay = new Date();
        var d = new Date();
//        d.setDate(toDay.getDate()+3);
        d.setDate(toDay.getDate());
	$(".cari-tiket-date").datepicker({
//		dateFormat: 'dd MM, yy'
		dateFormat: 'yy-mm-dd',
                minDate:    d,
	}).datepicker( "setDate", d);
	$(".datepicker-berangkat").datepicker({
//		dateFormat: 'dd MM, yy'
		dateFormat: 'yy-mm-dd',
                minDate:    d,
//	}).datepicker( "setDate", d);
	});

//	$(".datepicker").datepicker({
	$(".cari-tiket-date").datepicker({
//		dateFormat: 'D M dd yy',
                dateFormat: 'yy-mm-dd',
        onSelect: function(datetext){
            var d = new Date(); // for now
            datetext=datetext+" "+d.getHours()+": "+d.getMinutes()+": "+d.getSeconds();
            $('.datepicker_home').val(datetext);
        },
	})

	$(".datepicker-two").datepicker({
		dateFormat: "dd-mm-yy"
	});

	$(".select-2").select2();

	$(".select-department").select2({
		placeholder: "All Departments"
	});

	$(".select-location").select2({
		placeholder: "All Location"
	});

	// custom tabs for route-item-box
	// $('.route-items-detail-cta .detail-cta-box').hide(); // initial hide all content
	// $(".route-subitem-content .cta a").on( 'click', function(e){
	// 	e.preventDefault();
	// 	if ($(this).attr( 'class' ) == 'active' ){

	// 	} else {
	// 		$('.route-items-detail-cta .detail-cta-box').hide();
	// 		$('.route-subitem-content .cta a').parent().removeClass("active");
	// 		$(this).parent().addClass('active');
	// 		$( $(this).attr('href')).fadeIn();
	// 	}
	// });

	// var btnRwdToggle = $('.site-header-rwd .btn-rwd-show');
	// btnRwdToggle.on( 'click', function() {
		// var openMenu = ( 'active' );
		// var parent = $(this).parent('.site-header-rwd');
		// var parentMenu = parent.find( '.menu-rwd');
		
		// parentMenu.toggleClass( 'active' );

	// });

})(jQuery);
