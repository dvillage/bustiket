    var smd = JSON.parse($('#smaindata').val());
    var sad = JSON.parse($('#sadata').val());
    var sbd = JSON.parse($('#sbdata').val());
    var testdata2 = [
        {key: "One", y: 5},
        {key: "Two", y: 2},
        {key: "Three", y: 9},
        {key: "Four", y: 7},
        {key: "Five", y: 4},
        {key: "Six", y: 3},
        {key: "Seven", y: 0.5}
    ];
    var box_height = 250;
    var height = 350;
    var width = 350;
    nv.addGraph(function() {
        var chart = nv.models.pieChart()
            .x(function(d) { return d.key })
            .y(function(d) { return d.y })
            .width(width)
            .height(height)
            .showTooltipPercent(true);
        d3.select("#ssmain")
            .datum(smd)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);
        d3.select("#ssa")
            .datum(sad)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);
        d3.select("#ssb")
            .datum(sbd)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);

        return chart;
    });
    
    
    historicalBarChart = [
        {
            key: "Today",
            values: [
                {
                    "label" : "A" ,
                    "value" : 29.765957771107
                },
                {
                    "label" : "B" ,
                    "value" : 29.765957771107
                },
                {
                    "label" : "C" ,
                    "value" : 29.765957771107
                }
            ]
        },
        {
            key: "Month",
            values: [
                
            ]
        },
        {
            key: "Year",
            values: [
                
            ]
        }
    ];
    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(true)
            .staggerLabels(historicalBarChart[0].values.length > 8)
//            .showValues(true)
            .duration(250)
            .showLegend(true)
            .showYAxis(false)
//            .showXAxis(false)
//            .height(box_height)
            .wrapLabels(true)
            ;
        d3.select('#totalsale svg')
            .datum(historicalBarChart)
            .attr('height', height)
            .call(chart);
        d3.select('#commss svg')
            .datum(historicalBarChart)
            .attr('height', height)
            .call(chart);
        d3.select('#comab svg')
            .datum(historicalBarChart)
            .attr('height', height)
            .call(chart);
        nv.utils.windowResize(chart.update);
        return chart;
    });