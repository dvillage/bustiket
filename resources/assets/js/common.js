$(document).ajaxStart(function () {
    
        //ajax request went so show the loading image
//        var imga="<div id='loader' style='hight:100%;width:100%;top:275px;padding-left:200px;text-align:center;position:absolute;' ><img src='{{URL::to('assets/img/loader.gif')}}' height='30px' width='30px'></div>";
//        $("body").append(imga);
//        $('#sptable').css('visibility','hidden');
//        document.getElementsByTagName( "html" )[0].classList.add( "loading" );

        // All browsers
//        document.getElementsByTagName( "html" )[0].className.replace( "", /loading/  );

        // Or with jQuery
//        $( "html" ).addClass( "loading" );
    });
     
    $(document).ajaxComplete(function () {

    //got response so hide the loading image
//        $("#loader").remove();
//        $('#sptable').css('visibility','visible');
//        $('#cntrlbtn').css('Visibility','visible');
//        document.getElementsByTagName( "html" )[0].classList.remove( "loading" );

        // All browsers
//        document.getElementsByTagName( "html" )[0].className.replace( /loading/, "" );

        // Or with jQuery
//        $( "html" ).removeClass( "loading" );
    });

function modalOpen(mdlname,title,msg,onsuccess,id,onabort){
    $('#'+mdlname+'').find('#mdltitle').html(title);
    $('#'+mdlname+'').find('#mdlmsg').html(msg);
    $('#'+mdlname+'').find('#mdlyes').attr('onclick',onsuccess+'('+id+')');
   
    if(onabort!=='')
        $('#'+mdlname+'').find('#mdlabort').attr('onclick',onabort+'()');

    $('#'+mdlname+'').openModal();

}

function getBaseURL(){
    var url = $("#base_url").val();
    return url;
}

function getData(urlto,crnt,len,type,opr,search,extra){
    
    var url=getBaseURL()+urlto;
    var data={crnt:crnt,len:len,type:type,opr:opr,search:search};
    if(typeof extra !=='undefined'){
        for(var i in extra){
            data[i] = extra[i];
        }
    }
    postAjax(url,data,function(res){
        if(res.flag==0){
            
            $('#alerterror').html('<h3 style="color:red;">No Data Found !!</h3>');
            $('#alerterror').css('display','block');
            $('#vtable').css('visibility','hidden');
            $('#cntrlbtn').css('display','none');
            Materialize.toast(res.msg+' No Change Occured.', 2000,'rounded')
            return false;
        }
        else{
            $("#vtable").html(res);
            $('#alerterror').css('display','none');
            $('#vtable').css('visibility','visible');
            $('#cntrlbtn').css('display','block');
            $("#crnt").val($("#current").val());
            $('#crnt').attr('max',$("#total_page").val());
            $('#crnt').attr('min',1);
            $("#len").val($("#len").val());
            $("#total").html($("#total_page").val());
        }
    });

}

function delData(urlto,elem,urlto1){
    var url=getBaseURL()+urlto;
    var data={id:elem};
    postAjax(url,data,function(data){
        if(typeof data.flag !== 'undefined' && data.flag != 1){
            Materialize.toast(data.msg, 2000,'rounded');
            return false;
        }
        Materialize.toast(data.msg, 2000,'rounded');
        filterData(urlto1);
    });
}


function filterDataWith(urlto,elem,extra){
    var crnt = $('#crnt').val();
    var len = $('#len').val();
    var type = $('#type').html();
    var opr = elem.id;
    var search = $('#searchname').val();
    
    if(typeof extra === 'undefined'){
        extra = {};
    }
    
    getData(urlto,crnt,len,type,opr,search,extra);
}

function filterData(urlto,len,extra){
//    alert(len);
    if(typeof len === 'undefined' || len ==''){
        len = $('#len').val();
    }
    if(typeof extra === 'undefined'){
        extra = {};
    }
//    alert(len);
    var crnt = $('#crnt').val();
    var type = $('#type').html();
    var search = $('#searchname').val();
    
    if(crnt > $("#total").html() && $("#total").html() != 0){
        return false;
    }
    
    if(getData(urlto,crnt,len,type,'',search,extra)==false){
        return false;
    }
}

function pagination(v,search_str,display) {
           // $(document).ready(function(){
           var url=getBaseURL()+v;
                function loadData(page,len){
                    var data= "page="+page+"&len="+len+"&"+search_str;
                        postAjax(url,data,function(msg)
                            {
                                $("#"+display).html(msg);
                        });
                }
                var len = parseInt($('#len :selected').val());
                loadData(1,len);  // For first time page load default results
                $('#first').on('click',function(){
                    var page = 1;
                    loadData(page,len);                    
                });           
                $('#last').on('click',function(){
                    var page = parseInt($('#total').val());
                    loadData(page,len);                    
                });           
                $('#prev').on('click',function(){
                    var page = parseInt($('#crnt').val());
                    if(page >0){
                        page = page -1 ;
                    }else{
                        return false;
                    }
                    loadData(page,len);                    
                });           
                $('#next').on('click',function(){
                    var page = parseInt($('#crnt').val());
                    var last = parseInt($('#total').val());
                    if(page < last){
                        page = page + 1 ;
                    }else{
                        return false;
                    }
                    loadData(page,len);                    
                });           
                $('#crnt').on('change',function(){
                    var page = parseInt($('#crnt').val());
                    var no_of_pages = parseInt($('#total').val());
                    if(page != 0 && page <= no_of_pages){
                        loadData(page,len);
                    }else{
                        alert('Enter a PAGE between 1 and '+no_of_pages);
                        $('#crnt').val("").focus();
                        return false;
                    }
                    
                });
}

 $.fn.autocomplete = function (options) {
      // Defaults
//      console.log('autocomplete called..');
      
      var defaults = {
        data: {},
        limit: Infinity,
        onAutocomplete: null,
        minLength: 1
      };

      options = $.extend(defaults, options);

      return this.each(function () {
        var $input = $(this);
        var data = options.data,
            count = 0,
            activeIndex = -1,
            oldVal,
            $inputDiv = $input.closest('.input-field'); // Div to append on

        // Check if data isn't empty
        if (!$.isEmptyObject(data)) {
          var $autocomplete = $('<ul class="autocomplete-content dropdown-content"></ul>');
          var $oldAutocomplete;

          // Append autocomplete element.
          // Prevent double structure init.
          if ($inputDiv.length) {
            $oldAutocomplete = $inputDiv.children('.autocomplete-content.dropdown-content').first();
            if (!$oldAutocomplete.length) {
              $inputDiv.append($autocomplete); // Set ul in body
            }
          } else {
            $oldAutocomplete = $input.next('.autocomplete-content.dropdown-content');
            if (!$oldAutocomplete.length) {
              $input.after($autocomplete);
            }
          }
          if ($oldAutocomplete.length) {
            $autocomplete = $oldAutocomplete;
          }

          // Highlight partial match.
          var highlight = function (string, $el) {
            var img = $el.find('img');
            var matchStart = $el.text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                matchEnd = matchStart + string.length - 1,
                beforeMatch = $el.text().slice(0, matchStart),
                matchText = $el.text().slice(matchStart, matchEnd + 1),
                afterMatch = $el.text().slice(matchEnd + 1);
            $el.html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
            if (img.length) {
              $el.prepend(img);
            }
          };

          // Reset current element position
          var resetCurrentElement = function () {
            activeIndex = -1;
            $autocomplete.find('.active').removeClass('active');
          };

          // Remove autocomplete elements
          var removeAutocomplete = function () {
            $autocomplete.empty();
            resetCurrentElement();
            oldVal = undefined;
          };

          $input.off('blur.autocomplete').on('blur.autocomplete', function () {
            removeAutocomplete();
          });

          // Perform search
          $input.off('keyup.autocomplete focus.autocomplete').on('keyup.autocomplete focus.autocomplete', function (e) {
            // Reset count.
            count = 0;
            var val = $input.val().toLowerCase();

            // Don't capture enter or arrow key usage.
            if (e.which === 13 || e.which === 38 || e.which === 40) {
              return;
            }
            
            // Check if the input isn't empty
            if (oldVal !== val) {
              removeAutocomplete();
              
              if (val.length >= options.minLength) {
                  
                for (var key in data) {
                  if (data.hasOwnProperty(key) && key.toLowerCase().indexOf(val) !== -1) {
                  
                    // Break if past limit
                    if (count >= options.limit) {
                      break;
                    }

                    var autocompleteOption = $('<li id="'+data[key]+'"></li>');
                    /*if (!!data[key]) {
                        autocompleteOption.append('<span>' + data[key] + '</span>');
//                      autocompleteOption.append('<img src="' + data[key] + '" class="right circle"><span>' + key + '</span>');
                    } else {
                      autocompleteOption.append('<span>' + key + '</span>');
                      autocompleteOption.append('<span>' + data[key] + '</span>');
                    }*/
                    
                    autocompleteOption.append('<span>' + key + '</span>');
//                    autocompleteOption.append('<span>' + data[key] + '</span>');
                    

                    $autocomplete.append(autocompleteOption);
                    highlight(val, autocompleteOption);
                    count++;
                  }
                }
              }
            }

            // Update oldVal
            oldVal = val;
            
          });

          $input.off('keydown.autocomplete').on('keydown.autocomplete', function (e) {
            // Arrow keys and enter key usage
            var keyCode = e.which,
                liElement,
                numItems = $autocomplete.children('li').length,
                $active = $autocomplete.children('.active').first();
                
            // select element on Enter
            if (keyCode === 13 && activeIndex >= 0) {
              liElement = $autocomplete.children('li').eq(activeIndex);
              if (liElement.length) {
                liElement.trigger('mousedown.autocomplete');
                e.preventDefault();
              }
              return;
            }
            if(keyCode ===13){
//                console.log('old val : '+oldVal);
//                console.log('num items : '+numItems);
                if(numItems == 0 || activeIndex < 0){
                    options.onAutocomplete.call('', -1,-1);
                    e.preventDefault();
                }
            }
//            if(keyCode === 9){
//                if(activeIndex >= 0){
//                    liElement = $autocomplete.children('li').eq(activeIndex);
//                    if (liElement.length) {
//                      liElement.trigger('mousedown.autocomplete');
//                      e.preventDefault();
//                    }
//                    return;
//                }else{
//                    e.preventDefault();
//                }
//                if(numItems == 0){
//                    e.preventDefault();
//                }
//            }
            // Capture up and down key
            if (keyCode === 38 || keyCode === 40) {
              e.preventDefault();

              if (keyCode === 38 && activeIndex > 0) {
                activeIndex--;
              }

              if (keyCode === 40 && activeIndex < numItems - 1) {
                activeIndex++;
              }
              $active.removeClass('active');
              if (activeIndex >= 0) {
                $autocomplete.children('li').eq(activeIndex).addClass('active');
              }
            }
          });

          // Set input value
          $autocomplete.off('mousedown.autocomplete touchstart.autocomplete').on('mousedown.autocomplete touchstart.autocomplete', 'li', function () {
            var text = $(this).text().trim();
//            console.log(this);
//            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            $input.val(text);
            $input.trigger('change');
            removeAutocomplete();
//            console.log('text in autocomlete : '+text);
            // Handle onAutocomplete callback.
            if (typeof options.onAutocomplete === "function") {
                
              options.onAutocomplete.call(this, text,id);
            }
          });

          // Empty data
        } else {
          $input.off('keyup.autocomplete focus.autocomplete');
        }
      });
    };
    
function chk(theElement){
    var form=document.form;
 var theForm = theElement.form, z = 0;
     for(z=0; z<theForm.length;z++){
  if(theForm[z].type == 'checkbox' && theForm[z].name != 'selectall'){
      theForm[z].checked = theElement.checked;

      }
 }
if(document.getElementById('selectall').checked == true){
    document.getElementById('hc').value='1';
}
else
document.getElementById('hc').value='0';
}

function fun(id){
if(document.getElementById(id).checked == true){
    document.getElementById('hc').value='1';
}
else
document.getElementById('hc').value='0';
}

