var base_url;
$(document).ready(function () {
    base_url = getBaseURL();
    var flag = 0;
    var city_list = $('#city_list').val();
    city_list = JSON.parse(city_list);
    $(document).on('keyup','#from_city,#to_city',function(e){
        var obj = $(this);
        var s = $(this).val();
        if(s.length < 2){
            flag = 1;
            obj.next().val('');
//            return false;
        }
        var keyCode = e.which;
//        console.log(keyCode);
        if(keyCode === 8){
            obj.next().val('');
        }
        if(flag == 1){
            flag = 0;
           var url = base_url+"services/city-name";
           var data = {name:s};
//           postAjax(url,data,function(res){
//               if(res.flag == 1){
                   $(obj).autocomplete({
//                    data: res.data,
                    data: city_list,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            obj.next().val(val+'.C');
                        }else if(obj.next().val() == ''){
                            obj.next().val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
//               }
//           });
        }
    });
    $(document).on('click','#search_bus',function(){
        var from = $('#form_search').find('#from_id').val();
        var to = $('#form_search').find('#to_id').val();
        var date = $('#form_search').find('#journey_date').val();
        var nop = $('#form_search').find('#id_nop').val();
//            console.log(from,to,date,nop);
        var f = $('#form_search').attr('action');
//            alert(f+'/'+from+'.'+to+'.'+date+'.'+nop);
//            return false;
        $('#id_set').val(from+'.'+to+'.'+date+'.'+nop);
        $('#form_search').ajaxForm(function(res) {
//            console.log(res);
            if(typeof res.flag === 'undefined'){
                $('#result_search').html(res);
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    });
    window.Parsley.addValidator('dateDiff', {
        validateString: function(value, id) {
//            console.log(value);
//            console.log(id);
    //                return 0 === value % requirement;
          var from_date = value;
          var to_date = id;
          from_date = from_date.split('-');
          new_date = new Date(from_date[0], from_date[1], from_date[2]);
          date1 = to_date.split('-');
          new_date1 = new Date(date1[0], date1[1], date1[2]);
          if(new_date1 > new_date){
              return false;
          }
        },
        messages: {
          en: 'the date must be today or after today\'s date. ' 
        }
      });
      
      $(document).on("click",".view-seats",function() {
        var obj = $(this);
        var bus_id = $(this).data("bus_id");
        $('.view-seats').each(function(){
            var o_bus = $(this).data("bus_id");
            if($(this).attr("data-map") != 0 && bus_id != o_bus){
                $(this).attr("data-map",2);
            }
        });
        var is_opened = $(this).attr("data-map");
        $('.route-item-detail').children().css({display:'none'});
        $('.route-item').removeClass('active');
        if(is_opened == 2)
        {
            obj.parent().parent().next().children().css({display:'flex'});
            obj.parent().parent().addClass('active');
            $(this).attr('data-map',1);
            return false;
        }else if(is_opened == 1){
            obj.parent().parent().next().children().css({display:'none'});
            obj.parent().parent().removeClass('active');
            $(this).attr('data-map',2);
            return false;
        }
        obj.css({display:'none'});
        $('#loader_'+bus_id).css({display:'block'});
        var nop = $('#id_nop').val();
        var date = $('#journey_date').val();
        var url = getBaseURL() + 'services/get-seat-map';
        var param = {"bus_id" : bus_id,date:date,backend:1,nop:nop};
        postAjax(url, param, function (res) {
            if(typeof res.flag !== 'undefined' && res.flag != 1){
                alert(res.msg);
                window.location = $('#base_url').val();
            }
            obj.css({display:'block'});
            $('#loader_'+bus_id).css({display:'none'});
            obj.attr('data-map',1);
//            $('.route-search-result-items').html(res);
            obj.parent().parent().addClass('active');
            obj.parent().parent().next('.route-item-detail').html(res);
            $('#sp_id_s_'+bus_id).val($('#sp_id_'+bus_id).val());
            $('#sp_name_s_'+bus_id).val($('#sp_name_'+bus_id).val());
            $('#bus_name_s_'+bus_id).val($('#bus_name_'+bus_id).val());
            var per_head_f = $('#fare_format_'+bus_id).val();
            $('#per_head_'+bus_id).html('Rp '+per_head_f);
            $('#fare_total_f_'+bus_id).html('Rp '+per_head_f);
            $('#altmsg').css({display: 'none'});
            $('#sess_id_'+bus_id).val($('#session_id').val());
            $('#journey_date_'+bus_id).val(date);
            var board = JSON.parse($('#board_points_'+bus_id).val());
            var drop = JSON.parse($('#drop_points_'+bus_id).val());
            var bop = '';
            var dop = '';
            for(var i in board){
                bop += '<option value="'+board[i]['b_terminal_id']+'">'+board[i]['b_name']+'</option>';
            }
            for(var i in drop){
                dop += '<option value="'+drop[i]['d_terminal_id']+'">'+drop[i]['d_name']+'</option>';
            }
            $('#board_'+bus_id).append(bop);
            $('#drop_'+bus_id).append(dop);
        });
    });
    $(document).on("click",".select_seat",function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($('.total_seat_'+bus_id).html());
        var old_lbl = $('.total_seat_lable_'+bus_id).html().trim();
        var label = $(this).data("seat_label");
        var idx = $(this).data("seat_index");
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var total_fare_f;
        var total_fare;
        var total_seat;
        var nop = $('#total_person').val();
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#per_head_fare_'+bus_id).val(per_head);
        if($(this).hasClass('icon-seat-green')){
            var new_lbl = old_lbl.replace(','+label,'');
            var new_lbl = new_lbl.replace(label+',','');
            var new_lbl = new_lbl.replace(label,'');
            var new_lbl = new_lbl.replace(',,','');
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $(this).removeClass('icon-seat-green').addClass('icon-seat-white');
            $('.total_seat_'+bus_id).html((cnt)-1);
            var nc = parseInt(cnt)-1;
            
            if(nc == 0){
                var new_lbl = new_lbl.replace(',','');
                $('.total_seat_lable_'+bus_id).html(new_lbl);
                $('#form_'+bus_id).css({display:'none'});
                $('#form_'+bus_id).parsley().reset();
                $('.bus_'+bus_id).attr('id','add_pass_'+bus_id+'_'+1);
                $('.bus_'+bus_id).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(1)});
                return;
            }
            total_fare_f = (per_head * nc/1000).toFixed(3);
            total_fare = (per_head * nc);
            total_seat = nc;
            $('.seat_label').each(function(){
                var lbl =  $(this).val();
                console.log(lbl);
                if(lbl == label){
                    var rm_id = $(this).parent().parent('div').attr('id');
                    console.log(rm_id);
                    $('#'+rm_id).remove();
                }
            });
            var jc = 1;
            $('.bus_'+bus_id).each(function(){
                $(this).attr('id','add_pass_'+bus_id+'_'+jc);
                $('#add_pass_'+bus_id+'_'+jc).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(jc)});
                jc++;
            });
//            $('#add_pass_'+bus_id+'_'+cnt).remove();
        }else{
            var new_lbl = old_lbl =='' ?  label : old_lbl + ','+ label;
            
            if(cnt == 4 || (cnt+1) > nop){
                alert('you can not select more seats.');
                return false;
            }
            total_fare_f = (per_head * (cnt+1)/1000).toFixed(3);
            total_fare = (per_head * (cnt+1));
            total_seat = cnt+1;
            $(this).removeClass('icon-seat-white').addClass('icon-seat-green');
            $('.total_seat_'+bus_id).html((cnt)+1);
            $('.total_seat_lable_'+bus_id).html(new_lbl);
            $('#form_'+bus_id).css({display:'block'});
            $('#add_pass_'+bus_id+'_'+cnt).clone().attr({'id':'add_pass_'+bus_id+'_'+(cnt+1)}).insertAfter($('#add_pass_'+bus_id+'_'+cnt));
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.pass_name').attr({'placeholder':'Detail penumpang '+(cnt+1)});
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_label').val(label);
            $('#add_pass_'+bus_id+'_'+(cnt+1)).children().children('.seat_index').val(idx);
            $('#form_'+bus_id).parsley();
            $('.dob').inputmask();
        }
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
    });
    $(document).on('change','.select_seat_drop',function(){
        var bus_id = $(this).data("bus_id");
        var cnt = parseInt($(this).val());
        $('#form_'+bus_id).css({display:'block'});
        $('.total_seat_'+bus_id).html((cnt));
        $('#form_'+bus_id).parsley().reset();
        $('#form_'+bus_id).parsley();
        var per_head_f = $('#fare_format_'+bus_id).val();
        var per_head = $('#fare_'+bus_id).val();
        var total_fare_f;
        var total_fare;
        var total_seat;
        total_fare_f = (per_head * cnt/1000).toFixed(3);
        total_fare = (per_head * cnt);
        total_seat = cnt;
        $('#fare_total_f_'+bus_id).html('Rp '+total_fare_f);
        $('#total_amount_'+bus_id).val(total_fare);
        $('#total_seat_'+bus_id).val(total_seat);
//        $('#per_head_'+bus_id).html('Rp '+per_head_f);
        $('#per_head_fare_'+bus_id).val(per_head);
        if(cnt == 1){
            $('#add_pass_'+bus_id+'_'+2).remove();
            $('#add_pass_'+bus_id+'_'+3).remove();
            $('#add_pass_'+bus_id+'_'+4).remove();
        }
       
        for(var i=1;i<=cnt;i++){
            var ex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+i)).length;
            if(ex == 0){
                $('#add_pass_'+bus_id+'_'+(i-1)).clone().attr({'id':'add_pass_'+bus_id+'_'+(i)}).insertAfter($('#add_pass_'+bus_id+'_'+(i-1)));
                $('#add_pass_'+bus_id+'_'+(i)).children().children('.pass_name').attr({'placeholder':'Detil penumpang '+(i)});
            }
            var rex = $('#form_'+bus_id).find($('#add_pass_'+bus_id+'_'+(cnt+i))).length;
            if(rex){
                $('#add_pass_'+bus_id+'_'+(cnt+i)).remove();
            }
        }
        $('.dob').inputmask();
//        $(".dob").datepicker();
    });
    
    $(document).on('change keyup','.copy_to_pass',function(){
        var bus_id = $(this).data('bus_id');
        if($('.copy_'+bus_id).is(':checked')){
            var main = $('#add_pass_'+bus_id+'_1');
            var val = $(this).val();
            if($(this).attr('id') == 'booker_name'){
                main.find('#pass_name').val(val);
            }
            if($(this).attr('id') == 'booker_dob'){
                main.find('#pass_dob').val(val);
            }
            if($(this).attr('id') == 'booker_mo'){
                main.find('#pass_mo').val(val);
            }
        }
    });
      
});

function setBoard(obj){
    var bus_id = $(obj).data('bus_id');
    var id = $(obj).val();
    var board = JSON.parse($('#board_points_'+bus_id).val());
    for(var i in board){
        if(board[i]['b_terminal_id'] == id){
            $('#f_terminal_'+bus_id).html(board[i]['b_name']+'('+board[i]['boarding_time']+')');
            $('#f_city_'+bus_id).html(board[i]['b_city_name']+',');
            $('#f_dist_'+bus_id).html(board[i]['b_district_name']);
            $('#board_point_'+bus_id).val(JSON.stringify(board[i]));
        }
    }
    $('#drop_'+bus_id).val('');
}
function setDrop(obj){
    var bus_id = $(obj).data('bus_id');
    var id = $(obj).val();
    var drop = JSON.parse($('#drop_points_'+bus_id).val());
    for(var i in drop){
        if(drop[i]['d_terminal_id'] == id){
            $('#t_terminal_'+bus_id).html(drop[i]['d_name']+','+drop[i]['d_city_name']+',');
            $('#t_dist_'+bus_id).html(drop[i]['d_district_name']);
            $('#drop_point_'+bus_id).val(JSON.stringify(drop[i]));
        }
    }
    var board_id = $('#board_'+bus_id+' :selected').val();
    var nop = $('#total_person').val();
    var date = $('#journey_date').val();
    var data = {from_terminal_id:board_id,to_terminal_id:id,nop:nop,bus_id:bus_id,date:date};
    var url = getBaseURL() + 'services/get-price-by-terminal';
    if(board_id > 0){
        postAjax(url,data,function(res){
//            console.log(res);
            if(res.flag == 1){
                var pr = parseFloat(res.data);
                $('#pricing_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                $('#fare_'+bus_id).val(res.data);
                $('#per_head_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                if($('.select_seat_drop :selected').val()){
                    $('.select_seat_drop').trigger('change');
                }
                var ts = $('#total_seat_'+bus_id).val();
                if(ts != ''){
                    ts = parseInt(ts);
                    $('#per_head_fare_'+bus_id).val(pr);
                    $('#total_amount_'+bus_id).val(pr*ts);
                    $('#fare_total_f_'+bus_id).html('Rp '+(pr*ts/1000).toFixed(3));
                }else{
                    $('#fare_total_f_'+bus_id).html('Rp '+(pr/1000).toFixed(3));
                }
            }
        });
    }
}
function validate(bus_id){
    console.log(bus_id);
    
    var b = $('#board_'+bus_id+' :selected').val();
    var d = $('#drop_'+bus_id+' :selected').val();
    if(b == ''){
        alert('select boarding point');
        return false;
    }
    if(d == ''){
        alert('select droping point');
        return false;
    }
//    var cnt = 0;
//    $('#form_'+bus_id).find('.mobile_val').each(function(){
//        var con = $(this).val();
//        if(con != ''){
//            var two = con.substring(0, 2);
//            var three = con.substring(0, 3);
//            if(two == '62'){
//                $(this).val('+'+con);
//            }else if(three != '+62'){
//                cnt++;
//            }
//        }
//    });
//    if(cnt){
//        alert('mobile number must start with +62');
//        return false;
//    }
    return true;
}