<html><head><title></title></head>
<body yahoo="yahoo">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td align="center"><table cellspacing="0" cellpadding="0" class="responsiveTable" width="640">
          <tbody>
            <tr valign="top">
              <td height="13" align="center" valign="top"><img class="responsiveImage" src="{{URL::to("/../images/topLine.png")}}" width="624" height="13" style="vertical-align:top" alt=""/></td>
            </tr>
            <tr>
              <td align="center"><table border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                  <tbody>
                    <tr>
                      <td width="15" height="15"></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td valign="middle" align="center"><a href="#"><img src="{{URL::to("/../images/".$imglogo)}}" width="79" height="43" alt=" Bus Ticket"/></a></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td width="15" height="15"></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td height="17" bgcolor="#3e9143"></td>
                    </tr>
                    <tr>
                      <td height="4"></td>
                    </tr>
                    <tr>
                      <td height="5" bgcolor="#fcb912"></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
              <td height="20"></td>
            </tr>
            <tr>
              <td valign="top"><table width="100%" class="responsiveDataTbl" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="30" height="30"><img src="{{URL::to("/../images/textconLT.png")}}" width="30" height="30" alt=""/></td>
                      <td style="background-image:url({{URL::to("/../images/textconT.png")}}); background-repeat:repeat-x; background-position:center top" ></td>
                      <td><img src="{{URL::to("/../images/textconRT.png")}}" width="30" height="30" alt=""/></td>
                    </tr>
                    <tr>
                      <td style="background-image:url({{URL::to("/../images/textconL.png")}}); background-repeat:repeat-Y; background-position:left center"></td>
                      <td valign="top"><div style="min-height:325px">
                          <div style="font-size:18px; color:#414042; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:5px">E - tiket</div>
                          <div style="font-size:14px; color:#3e9143; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:10px">Perjalanan Pergi</div>
                          <div style="text-align:center;font-size:12px; color:#222; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> Booking ID:
                            <span style="font-size:10px; color:#fcb912; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> {{$ticket_id}} </span>
                          </div>
                          <table width="100%" border="0" cellspacing="5" cellpadding="0">
                            <tbody>
                              <tr>
                                <td valign="middle"><div style="font-size:12px; color:#222; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px">{{$sp_name}}<br>
                                    <?php echo ucfirst($parent_bus['par_name']); ?></div></td>
                                <td bgcolor="#e7e8e9" width="2"></td>
                                <td><div style="font-size:12px; color:#222; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:5px; margin-left:5px"> <?php echo date("D, j F, Y",strtotime($date));?> </div>
                                  <table width="100%" border="0" cellspacing="5" cellpadding="3">
                                    <tbody>
                                      <tr>
                                          <td><div style="font-size:14px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> <?php echo explode("--", $bording_time)[1];?> </div>
                                          <div style="font-size:12px; color:#222; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> {{$from_city['city_name']}} </div>
                                          <div style="font-size:11px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> <?php echo explode("--", $bording_time)[0];?><br/><?php echo $boading_address;?></div></td>
                                          <td valign="middle" align="center"><img src="{{URL::to("/../images/right-arrow.png")}}" width="12" height="23" alt=""/></td>
                                          <td bgcolor="#e7e8e9" width="2"></td>
                                        <td><div style="font-size:14px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> <?php echo explode("--", $departure_time)[1];?> </div>
                                          <div style="font-size:12px; color:#222; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> {{$to_city['city_name']}} </div>

                                          
                                          <div style="font-size:11px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px"> <?php  echo explode("--", $departure_time)[0];?><br/><?php echo $drop_address;?></div></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                                
                                <!--<td></td>-->
                              </tr>
                            </tbody>
                          </table>
                          <div style="height:20px; clear:both; width:100%; display:block"></div>
                          <div style="border:1px solid #a6a6a6">
                            <table style="font-size:12px" width="100%" border="0" cellspacing="0" cellpadding="5">
                              <tbody>
                                <tr>
                                  <th bgcolor="#f1f2f2">No.</th>
                                  <th bgcolor="#f1f2f2">Nama Penumpang</th>
                                  <th align="center" valign="middle" bgcolor="#f1f2f2">Nomor Tiket</th>
                                  <th align="center" valign="middle" bgcolor="#f1f2f2">Nomor Kursi</th>
                                  <th align="center" valign="middle" bgcolor="#f1f2f2">Gender</th>
                                  <th align="center" valign="middle" bgcolor="#f1f2f2">Umur</th>
                                </tr>
                                
                                <?php 
                                foreach ($passenger as $key => $row)
                                {
                                  $seat = "";
                                  if($layout){ $seat = "Saat Keberangkatan "; } else { $seat = isset($row["passenger_seatlabel"]) ? $row["passenger_seatlabel"] : $row["passenger_seatNo"]; }
                                  ?>
                                  <tr>
                                    <td><?php echo $key+1;?></td>
                                    <td><?php echo $row["passenger_Name"];?></td>
                                    <td align="" valign="middle"><div style="color:#3e9143"><?php echo $row["unique_id"];?></div></td>
                                    <td align="" valign="middle"><?php echo $seat;?></td>
                                    <td align="center" valign="middle"><?php echo $row["passenger_Gender"];?></td>
                                    <td align="center" valign="middle"><?php echo $row["passenger_Age"];?></td>
                                  </tr>
                                  <?php
                                } 
                                ?>
                              </tbody>
                            </table>
                          </div>
                          <div style="height:20px; clear:both; width:100%; display:block"></div>
                          <div style="font-size:14px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:0px; line-height:1.5"> <strong>CATATAN:</strong>
                            <ol style="list-style-type:decimal; margin:0; color:#414042; font-size:13px; padding-left:15px">
<li style="margin-bottom:5px">Harap print e-Tiket ini dibawa untuk ditukarkan dengan tiket bus resmi.</li>

<li style="margin-bottom:5px">Waktu yang tercantum pada tiket adalah waktu setempat.</li>

<li style="margin-bottom:5px">Harap tiba di terminal keberangkatan 60 menit sebelum waktu keberangkatan yang tercantum pada e-Tiket.</li>

<li style="margin-bottom:5px">Nomor kursi dapat berubah sesuai kebijakan operator pada saat keberangkatan.</li>

<li style="margin-bottom:5px">Harga dapat berubah sewaktu-waktu pada saat keberangkatan sesuai dengan kebijakan masing-masing operator dikarenakan high season, liburan panjang, lebaran dan lain-lain yang berpotensi terhadap kenaikan tiket. Tambahan biaya tiket tersebut akan dibebankan kepada penumpang.</li>

<li style="margin-bottom:5px">BUSTIKET tidak bertanggung jawab atas keterlambatan atau penjadwalan ulang bus. Hal tersebut berada 
     dibawah kendali masing-masing manajemen bus.</li>

<li style="margin-bottom:5px">Jika Anda menghadapi masalah, atau memiliki pertanyaan silahkan hubungi kami di 0812-8000-3919</li>

 

</ol>
                          </div>
                        </div></td>
                      <td style="background-image:url({{URL::to("/../images/textconR.png")}}); background-repeat:repeat-Y; background-position:right center"></td>
                    </tr>
                    <tr>
                      <td><img src="{{URL::to("/../images/textconLB.png")}}" width="30" height="30" alt=""/></td>
                      <td style="background-image:url({{URL::to("/../images/textconB.png")}}); background-repeat:repeat-x; background-position:top center" ></td>
                      <td width="30" height="30"><img src="{{URL::to("/../images/textconRB.png")}}" width="30" height="30" alt=""/></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
              <td height="20"></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td height="5" bgcolor="#fcb912"></td>
                    </tr>
                    <tr>
                      <td height="4"></td>
                    </tr>
                    <tr>
                      <td height="17" bgcolor="#3e9143"></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>