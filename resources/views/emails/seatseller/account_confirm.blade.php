<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{$mail_subject}}</title>
        <style type="text/css">
            body { margin: 0; }
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham,  Helvetica, Arial, sans-serif; font-style: normal; font-weight: 400; }
            .responsiveDataTbl { width: 100%!important; margin: 0 auto!important; font-size: 0; }

            @media screen and (max-width:640px) {
                body,
                table,
                td,
                p,
                a,
                li,
                blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham,  Helvetica, Arial, sans-serif; }
                table { width: 100%; }
                .responsiveTable { width: 100%!important; }
                .responsiveImage { width: 100%!important; height: auto }
                .responsiveDataTbl { width: 95%!important; margin: 0 auto!important }
            }

            @media screen and (max-width:480px) {
                body,
                table,
                td,
                p,
                a,
                li,
                blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham,  Helvetica, Arial, sans-serif; }
                table { width: 100% !important; border-style: none !important; }
                .responsiveTable { width: 100%!important; }
                .responsiveImage { width: 100%!important; height: auto }
                .responsiveDataTbl { width: 95%!important; margin: 0 auto!important }
            }
        </style>
    </head>
    <body yahoo="yahoo">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td align="center"><table cellspacing="0" cellpadding="0" class="responsiveTable" width="640">
                            <tbody>
                                <tr valign="top">
                                    <td height="13" align="center" valign="top"><img class="responsiveImage" src="{{URL::to("/../images/topLine.png")}}" width="624" height="13" style="vertical-align:top" alt=""/></td>
                                </tr>
                                <tr>
                                    <td align="center"><table border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td width="15" height="15"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td valign="middle" align="center"><a href="#"><img src="{{URL::to("/../images/logoBusTicket.png")}}" width="79" height="43" alt=" Bus Ticket"/></a></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td width="15" height="15"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="17" bgcolor="#3e9143"></td>
                                                </tr>
                                                <tr>
                                                    <td height="4"></td>
                                                </tr>
                                                <tr>
                                                    <td height="5" bgcolor="#fcb912"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td valign="top"><table width="100%" class="responsiveDataTbl" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="30" height="30"><img src="{{URL::to("/../images/textconLT.png")}}" width="30" height="30" alt=""/></td>
                                                    <td style="background-image:url({{URL::to("/../images/textconT.png")}}); background-repeat:repeat-x; background-position:center top" ></td>
                                                    <td><img src="{{URL::to("/../images/textconRT.png")}}" width="30" height="30" alt=""/></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-image:url({{URL::to("/../images/textconL.png")}}); background-repeat:repeat-Y; background-position:left center"></td>
                                                    <td valign="top"><div style="min-height:325px">
                                                            <div style="font-size:18px; color:#000; font-family:Gotham,   Helvetica, Arial, sans-serif; margin-bottom:20px">Welcome, <?php echo isset($user_firstname) ? $user_firstname : "";?></div>
                                                            <div style="font-size:14px; color:#000; font-family:Gotham,  Helvetica, Arial, sans-serif; margin-bottom:25px; line-height:1.5"> Your login details:<br>
                                                                Email: {{$user_email}}<br>
                                                                Password: {{$user_password}}</div>
                                                            <div style="font-size:14px; color:#000; font-family:Gotham,  Helvetica, Arial, sans-serif; margin-bottom:40px">Click Here to <a style="color:#3e9143; text-decoration:underline" href='{{URL::to("/user_welcome.php?uid=")}}{{$user_id}}'>Login</a>. We recommend you to change your password soon. </div>
                                                            <div style="font-size:14px; color:#000; font-family:Gotham,  Helvetica, Arial, sans-serif; margin-bottom:5px">Regards,</div>
                                                            <div style="font-size:16px; color:#000; font-family:Gotham,  Helvetica, Arial, sans-serif">BUSTIKET</div>
                                                        </div></td>
                                                    <td style="background-image:url({{URL::to("/../images/textconR.png")}}); background-repeat:repeat-Y; background-position:right center"></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="{{URL::to("/../images/textconLB.png")}}" width="30" height="30" alt=""/></td>
                                                    <td style="background-image:url({{URL::to("/../images/textconB.png")}}); background-repeat:repeat-x; background-position:top center" ></td>
                                                    <td width="30" height="30"><img src="{{URL::to("/../images/textconRB.png")}}" width="30" height="30" alt=""/></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="5" bgcolor="#fcb912"></td>
                                                </tr>
                                                <tr>
                                                    <td height="4"></td>
                                                </tr>
                                                <tr>
                                                    <td height="17" bgcolor="#3e9143"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>