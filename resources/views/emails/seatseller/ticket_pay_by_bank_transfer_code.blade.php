<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{$mail_subject}}</title>
        <style type="text/css">
            body { margin: 0; }
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; font-style: normal; font-weight: 400; }
            .responsiveDataTbl { width: 100%!important; margin: 0 auto!important; font-size: 0; }
            .responsiveTableMobile { float: left; font-size: 13px; }

            @media screen and (max-width:640px) {
                body,
                table,
                td,
                p,
                a,
                li,
                blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; }
                table { width: 100%; }
                .responsiveTable { width: 100%!important; }
                .responsiveImage { width: 100%!important; height: auto }
                .responsiveDataTbl { width: 95%!important; margin: 0 auto!important }
            }

            @media screen and (max-width:480px) {
                body,
                table,
                td,
                p,
                a,
                li,
                blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; }
                table { width: 100% !important; border-style: none !important; }
                .responsiveTable { width: 100%!important; }
                .responsiveImage { width: 100%!important; height: auto }
                .responsiveDataTbl { width: 95%!important; margin: 0 auto!important }
                .responsiveTableMobile { width: 100%!important; float: none }
                .responsiveTableMobile.dataTable { color: #000!important }
            }
        </style>
    </head>
    <body yahoo="yahoo">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" class="responsiveTable" width="640">
                            <tbody>
                                <tr valign="top">
                                    <td height="13" align="center" valign="top"><img class="responsiveImage" src="{{URL::to("/../images/topLine.png")}}" width="624" height="13" style="vertical-align:top" alt=""/></td>
                                </tr>
                                <tr>
                                    <td align="center"><table border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td width="15" height="15"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td valign="middle" align="center"><a href="#"><img src="{{URL::to("/../images/logoBusTicket.png")}}" width="79" height="43" alt=" Bus Ticket"/></a></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td width="15" height="15"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="17" bgcolor="#3e9143"></td>
                                                </tr>
                                                <tr>
                                                    <td height="4"></td>
                                                </tr>
                                                <tr>
                                                    <td height="5" bgcolor="#fcb912"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr >
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="5" width="5"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><div style="font-size:12px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; padding-left:30px">
                                                             <div style=" margin-left:-25px; margin-top:5px ; float:left"><img src="{{URL::to("/../images/alertSign1.png")}}" width="17" height="17" alt=""/></div>
                                                            Mohon Segera Selesaikan Pembayaran Melalui transfer ( ATM /Internet Banking, M-Banking ) Dalam Waktu 50 Menit 
                                                            kedepan. Setelah pembayaran kami terima, E-tiket akan kami kirimkan ke email anda dalam waktu 60 menit.</div></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td height="5" width="5"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div style="height:1px; width:100%; background:#ADADAD; display:block"></div></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="5" width="5"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><div style="font-size:12px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;  padding-left:30px">
                                                             <div style=" margin-left:-25px; margin-top:5px ; float:left"><img src="{{URL::to("/../images/alertSign.png")}}" width="17" height="17" alt=""/></div>
                                                            Penting: Kami mohon pastikan hanya transfer ke salah satu rekening Bank BUSTIKET. BUSTIKET dan semua karyawan 
                                                            tidak pernah meminta anda untuk transfer ke rekening lain seperti disebut diatas. Hati-hati terhadap penipuan.</div></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td height="5" width="5"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffbb00">
                                            <tbody bgcolor="#ffbb00">
                                                <tr>
                                                    <td width="10" height="10"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><table class="responsiveTableMobile" width="50%" border="0" cellspacing="0" cellpadding="5">
                                                            <tbody>
                                                                <tr>
                                                                    <td><div style="font-size:18px">Kode Transfer</div></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table class="responsiveTableMobile" width="50%" border="0" cellspacing="0" cellpadding="5">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="right"><div style="font-size:18px"><?php echo strtoupper($uniqid);?></div></td>
                                                            </tbody>
                                                        </table></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td width="10" height="10"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#3e9143">
                                            <tbody bgcolor="#3e9143">
                                                <tr>
                                                    <td width="10" height="10"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td align="center"><div style="font-size:16px; color:#fff; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:5px; line-height:1.5"> Waktu Anda untuk melakukan pembayaran adalah</div>
                                                        <div style="font-size:24px; color:#fff; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom:5px; line-height:1.5">50 menit</div>
                                                        <div id="templateEndTime" style="font-size:16px; color:#fff; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5">Sampai jam {{$expire_time}}</div></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td width="10" height="10"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="15"></td>
                                </tr>
                                <tr>
                                    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e5e5e5">
                                            <tbody>
                                                <tr>
                                                    <td width="5" height="5"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><div style="font-size:16px; color:#000; font-family:Gotham, \"Helvetica Neue\", Helvetica, Arial, sans-serif; line-height:1.5">Panduan pengiriman dengan Bank Transfer</div></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td width="5" height="5"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr><tr>'
                                    <td><div style="font-size:16px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-left:5px; margin-top:10px; margin-bottom:5px"> Bank Central Asia (BCA) &ndash; KCP Blok A Cipete </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="10">
                                            <tbody>
                                                <tr>
                                                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="3" style="font-size:14px">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Nama Rekening</td>
                                                                    <td>: {{$bank_account_name}} </td>
                                                                <tr>
                                                                    <td> Nomor Rekening </td>
                                                                    <td>: {{$bank_account_no}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td ><div style="font-size:14px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-bottom:5px"> Pastikan Anda mengikuti langkah-langkah berikut ini: </div>
                                        <div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5;" >
                                             <ol style="list-style-type:decimal; margin:0">
                                                <li>Mohon untuk verifikasi Rincian Perjalanan & Informasi pembelian tiket Anda.</li>
                                                <li>Pastikan Anda telah memasukkan detil rekening bank pengirim pembayaran dengan benar dan akurat. 
                                                    Kami akan gunakan ini untuk identifikasi pembayaran yang masuk di rekening kami.</li>
                                                <li>Mohon transfer sesuai harga total yang tertera di "Detail Pemesanan"</li>
                                                <li>Batas pembayaran adalah 50 menit.</li>
                                                <li>Setelah pembayaran dilakukan silakan kunjungi halaman "Konfirmasi Pembayaran".</li>
                                                <li>Masukkan data mengenai detail rekening pengirim transfer:
                                                    <ol style="list-style-type:lower-alpha">
                                                        <li>Kode Transfer</li>
                                                        <li>Nama Bank</li>
                                                        <li>Nama Pemilik Rekening</li>
                                                        <li>Jumlah Yang Ditransfer</li>
                                                    </ol>
                                                </li>
                                                <li>Klik tombol submit.</li>
                                                <li>Kami akan lakukan verifikasi manual dan akan mengirim e-tiket ke email Anda 
                                                    selambat-lambatnya dalam 60 menit.</li>
                                            </ol>
                                        </div></td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td ><div style="border: 1px solid #999">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="10" height="10"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td><div style="font-size:14px; color:#000; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-bottom:5px"> Konfirmasi Transfer Anda </div>
                                                            <div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-bottom:5px"> Silahkan melakukan pembayaran sejumlah Rp. {{$final_payable_amount}} ke bank yang tercantum diatas. <br>
                                                                Setelah itu Anda dapat melakukan Konfirmasi dengan mengunjungi halaman konfirmasi pembayaran dimana linknya tersedia di bagian menu atas.</div></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td width="10" height="10"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div></td>
                                </tr>
                                <tr>
                                    <td height="5"></td>
                                </tr>
                                <tr>
                                    <td><div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.2; margin-left:5px; margin-top:10px; margin-bottom:5px;"> <strong>Rincian Harga</strong></div>
                                        <div style="margin-left:10px; margin-right:10px; font-size:14px">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#6d6e71" >
                                                <tbody>
                                                    <tr bgcolor="#fff">
                                                        <td><table border="0" cellspacing="0" cellpadding="5">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Harga Tiket : </td>
                                                                        <td>Rp. {{$total_amt}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Potongan Kode Voucher  : </td>
                                                                        <td>Rp. {{$discount_price}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Biaya Layanan : </td>
                                                                        <td>Rp. {{$handling_fee}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="10"></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td ><strong>Harga Total :</strong></td>
                                                                        <td><strong>Rp. {{$final_payable_amount}}</strong></td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table></td>
                                                    </tr>
                                                    <tr bgcolor="#fff">
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr bgcolor="#fff">
                                                        <td><table class="responsiveTableMobile" width="70%" border="0" cellspacing="0" cellpadding="5">
                                                            </table></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div></td>
                                </tr>
                                <tr>
                                    <td height="10" valign="middle" align="center"><div style="height:1px; width:100%; background:#ADADAD; display:block"></div></td>
                                </tr>
                                <tr>
                                    <td><div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.2; margin-left:5px; margin-top:10px; margin-bottom:5px;"> <strong>Rincian Perjalanan</strong></div>
                                        <div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-left:5px; margin-top:10px; margin-bottom:5px;"> <?php echo date("D, j M Y",strtotime($date)); ?><br>
                                            <span style="color:#3e9143">Berangkat Dari : </span><br>
                                            {{$bording_time}}
                                            <br>
                                            <span style="color:#3e9143">Tiba Di :</span><br>
                                            {{$departure_time}}
                                            <br>
                                            <span style="color:#f7901e">Rute {{$rute}}</span> </div></td>
                                </tr>
                                <tr>
                                    <td height="10" valign="middle" align="center"><div style="height:1px; width:100%; background:#ADADAD; display:block"></div></td>
                                </tr>

                                <tr>
                                    <td><div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.2; margin-left:5px; margin-top:10px; margin-bottom:5px;"> <strong>Daftar Penumpang</strong></div>


                                        <?php 
                                        for($i=0;$i<count($passenger);$i++)
                                        {
                                            
                                        ?>

                                        <div style="font-size:14px; color:#6d6e71; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height:1.5; margin-left:5px; margin-top:10px; margin-bottom:5px;"> <?php echo $passenger[$i]['name']; ?><br>
                                            <span style="color:#3e9143">Nomor Kursi:</span><br> <?php echo $passenger[$i]['seat_no'];?> </div>
                                        <?php } ?>

                                    </td>
                                </tr><tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="5" bgcolor="#fcb912"></td>
                                                </tr>
                                                <tr>
                                                    <td height="4"></td>
                                                </tr>
                                                <tr>
                                                    <td height="17" bgcolor="#3e9143"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>