<html><head>
        <title>{{$mail_subject}}</title>
    </head>
    <body>
        <div>
            <div style="width:650px; border:1px solid #eeeeee; background:#ffffff; font-family:helvetica; margin:0 auto;">
                <div style="padding:10;background-color: #474658;">
                    <div style="width:300px; margin:auto;">
                        <a style="outline:none; text-decoration:none; color:#000" title="{{config("constant.PLATFORM_NAME")}}" href="{{\URL::to("/")}}" target="_blank">
                            <img style="border:none; margin-top:5px" src="{{\URL::to(config("constant.LOGO_URL"))}}" alt="{{config("constant.PLATFORM_NAME")}} logo" height="75">
                        </a>
                    </div>
                </div>
                
                <div style="clear:both"></div>
                <div style="background:url('{{\URL::to("assets/img/welcome.jpg")}}') no-repeat">
                    <div style="width:410px;max-width:410px;min-width:410px;min-height:597px;min-height:597px;max-height:597px;font-size:12px;padding:0 0 0 120px">
                        <p style="color:#ed934f;font-weight:bold;margin:70px 0 5px;float:left;width:100%;text-align:left;text-align:center;font-size:14px">
                            {{config("constant.PLATFORM_NAME")}} Welcomes you!
                        </p>
                        <p style="color:#ed934f;font-weight:bold;margin:5px 0 0;float:left;width:100%;text-align:left">Dear User,</p>
                        <p style="color:#ed934f;font-weight:bold;margin:5px 0 0;float:left;width:100%;text-align:left">Welcome to {{config("constant.PLATFORM_NAME")}}!</p>
                        <p style="color:#919191;font-weight:bold;margin:5px 0 0;float:left;width:100%;text-align:left">You've been part of {{Config::get("constant.PLATFORM_NAME")}} by your this effort. We appreciate and warmly welcomes you.</p>

                        <p style="color:#919191;font-weight:normal;margin:20px 0 0;float:left;width:100%;text-align:left">
                            We'll make sure you're the first to know about our special offers. Be a {{config("constant.PLATFORM_NAME")}} insider! Explore.
                        </p>
                        <div style="clear:both"></div>
                        <p style="color:#919191;font-weight:normal;margin:40px 0 0;float:left;width:100%;text-align:center">------- Stay updated, socially -------</p>
                        <div style="clear:both"></div>
                        <div style="text-align: center;width:250px;max-width:250px;min-width:250px;min-height:30px;min-height:30px;max-height:30px;border:none;margin:15px 0 0 85px">
                            <a style="outline:none;text-decoration:none;color:#000" title="Share {{config("constant.PLATFORM_NAME")}} on Facebook" href="{{config("constant.FACEBOOK_PAGE_URL")}}" target="_blank">
                                <img style="border:none" src="{{\URL::to("assets/img/fb.png")}}" alt="FB">
                            </a>
                            <a style="outline:none;text-decoration:none;color:#000" title="Share {{config("constant.PLATFORM_NAME")}} on Google+" href="{{config("constant.GOOGLE_PLUS_PAGE_URL")}}" target="_blank">
                                <img style="border:none;margin-left:10px" src="{{\URL::to("assets/img/gp.png")}}" alt="G">
                            </a>
                        </div>
                        <!--<table style="margin:140px 0 0 80px;width:60%;font-weight:normal">
                            <tbody>
                                <tr>
                                    <td style="color:#a1a1a9;text-align:center;width:100px">Twitter Followers</td>
                                    <td style="color:#a1a1a9;text-align:center;width:100px">Facebook Fans</td>
                                    <td style="color:#a1a1a9;text-align:center;width:100px">Franchise outlets</td>
                                </tr>
                                <tr>
                                    <td style="color:#575757;text-align:center">850+</td>
                                    <td style="color:#575757;text-align:center">101854</td>
                                    <td style="color:#575757;text-align:center">6000</td>
                                </tr>
                            </tbody>
                        </table>-->
                    </div>
                </div>
                <div style="clear:both"></div>
                <div style="width:600px;max-width:600px;min-width:600px;min-height:180px;min-height:180px;max-height:180px;background:#ffffff;font-size:12px;font-weight:normal;padding:35px 0 0 30px">
                    <div style="width:600px;max-width:600px;min-width:600px;min-height:50px;min-height:50px;max-height:50px;background:#ffffff">
                        <div style="float:left;padding:0 30px;border-right:1px solid #919191">
                            <a style="outline:none;text-decoration:none;color:#464646" title="About Us" href="{{\URL::to("about-us")}}" target="_blank">About Us</a>
                        </div>
                        <div style="float:left;padding:0 30px;border-right:1px solid #919191">
                            <a style="outline:none;text-decoration:none;color:#464646" title="Terms and Condition" href="{{\URL::to("terms-and-condition")}}" target="_blank">Terms and Condition</a>
                        </div>
                        <div style="float:left;padding:0 30px;color:#464646;border-right:1px solid #919191">
                            <a href="#" target="_blank">{{config("constant.CARE_NUMBER")}}</a>
                        </div>
                        <div style="float:left;padding:0 30px">
                            <a style="outline:none;text-decoration:none;color:#464646" href="mailto:{{config("constant.INFO_MAIL")}}" target="_blank">{{config("constant.INFO_MAIL")}}</a>
                        </div>
                    </div>
                    
                    <div style="margin: auto;width: 96;hieght: 48;">
                        <a title="Download {{config("constant.PLATFORM_NAME")}} Android App" href="{{config("constant.USER_APP_PLAY_STORE_URL")}}" target="_blank">
                            <img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/android-128.png" height="46" width="46">
                        </a>
                        <a title="Download {{config("constant.PLATFORM_NAME")}} Tableupp iOs App" href="{{\URL::to("")}}" target="_blank">
                            <img src="https://cdn3.iconfinder.com/data/icons/picons-social/57/56-apple-128.png" height="46" width="46">
                        </a>
                    </div>
                    
                    <!--<div style="width:480px;max-width:480px;min-width:480px;min-height:120px;min-height:120px;max-height:120px;background:#ffffff;border-top:1px dotted #464646;font-size:14px;margin:0 0 0 55px">
                        <p style="color:#4e4e4e;font-weight:normal;margin:20px 0 0;float:left;width:100%;text-align:center">DOWNLOAD APP</p>
                        <div style="clear:both"></div>
                        <div style="text-align: center;width:140px;max-width:140px;min-width:140px;min-height:46px;min-height:46px;max-height:46px;border:none;padding:15px 0 0 0;margin:0 auto">
                            <div style="background:url('{{\URL::to("assets/img/downloadapp.png")}}') no-repeat 0px 0px;float:left;width:46px;min-height:46px">
                                <a style="outline:none;text-decoration:none;color:#000;width:46px;min-height:46px;float:left" title="Download {{config("constant.PLATFORM_NAME")}} Android App" href="{{config("constant.USER_APP_PLAY_STORE_URL")}}" target="_blank"></a>
                            </div>
                            <div style="background:url('{{\URL::to("assets/img/downloadapp.png")}}') no-repeat -48px 0px;float:left;width:46px;min-height:46px">
                                <a style="outline:none;text-decoration:none;color:#000;width:46px;min-height:46px;float:left" title="Download {{config("constant.PLATFORM_NAME")}} iOs App" href="#" target="_blank"></a>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </body>
</html>