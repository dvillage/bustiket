<html><head>
        <title>New  Request</title>
    </head>
    <body>
        <div>
            <div style="width:650px; border:1px solid #eeeeee; background:#ffffff; font-family:helvetica; margin:0 auto;">
                <div style="padding:10;background-color: #474658;">
                    <div style="width:300px; margin:auto;">
                        <a style="outline:none; text-decoration:none; color:#000" title="{{config("constant.PLATFORM_NAME")}}" href="" target="_blank">
                            <img style="border:none; margin-top:5px;margin-left: 50px" src="{{URL::asset('assets/images/logo-footer.png')}}" alt="{{URL::asset('assets/images/logo-site.png')}} logo" height="75">
                        </a>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div  no-repeat">
                    <div style="width:410px;max-width:410px;min-width:410px;min-height:597px;min-height:597px;max-height:597px;font-size:12px;padding:0 0 0 120px">
                        <p style="color:#ed934f;font-weight:bold;margin:70px 0 5px;float:left;width:100%;text-align:left;text-align:center;font-size:14px">
                            Bustiket Payment information
                        </p>
                         <p style="color:#919191;font-weight:normal;margin:40px 0 0;float:left;width:100%;text-align:center"></p>
                        <div style="clear:both"></div>
                    
                        <div style="text-align: center;width:250px;max-width:250px;min-width:250px;min-height:30px;min-height:30px;max-height:30px;border:none;">
                            <table border="2px solid Blue" cellspacing="0" cellpadding="0"  width="400px" > 
                                 <tr>
                                        <td>Transaction Code:</td>
                                        <td>{{$bankData[0]['value']}}</td>
                                </tr>
                                 <tr>
                                        <td>Bank Address</td>
                                        <td>{{$bankData[1]['value']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Account Number:</td>
                                        <td>{{$bankData[2]['value']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Bank Name:</td>
                                        <td>{{$bankData[3]['value']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Transfer Amount:</td>
                                        <td>{{$bankData[4]['value']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Passenger Name:</td>
                                        <td>{{$bankData[5]['value']}}</td>
                                    </tr>
                                     <tr>
                                            <td align='center' valign='top'>

                                              <h2>Please confirm if the payment has been made.

                                              If Successful then Go to Admin Panel on "{{URL::to('admin/login/scottiger')}}" and Approve this Ticket.</h2>
                                            </td>
                                        </tr>

                            </table>

                        </div>
               </div>
                </div>
                <div style="clear:both"></div>
                <div style="width:600px;max-width:600px;min-width:600px;min-height:180px;min-height:180px;max-height:180px;background:#ffffff;font-size:12px;font-weight:normal;padding:35px 0 0 30px">
                    <div style="width:600px;max-width:600px;min-width:600px;min-height:50px;min-height:50px;max-height:50px;background:#ffffff">
                        
                    </div>
                    
                    <div style="margin: auto;width: 96;hieght: 48;">
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
