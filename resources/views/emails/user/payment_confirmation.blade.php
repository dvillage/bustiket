<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{$mail_subject}}</title>
    </head>
    <body yahoo="yahoo">
        <table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable' >
            <tr>
                <td align='center' valign='top'>
                    <table border='0' cellpadding='20' cellspacing='0' width='600' id='emailContainer' >
                        <tr> 
                            <td align='center' valign='top'>
                                <h1> Payment Confirmation</h1>
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Booking Code : {{$transfer_code}}
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Account Name Bank : {{$account_name}}
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Account number : {{$account_number}}
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Bank name : {{$bank_name}}
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Number of Transfers : {{$amount}}
                            </td>
                        </tr>
                        <tr>
                            <td  valign='top'>
                                Record : {{$note}}
                            </td>
                        </tr>
                        <tr>
                            <td align='center' valign='top'>
                                <h2>Please confirm if the payment has been made.
                                    <?php  $site_url = str_replace("/api", "", \URL::to("/")); ?>
                                    If Successful then Go to Admin Panel on <?php echo $site_url;?> and Approve this Ticket.</h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>