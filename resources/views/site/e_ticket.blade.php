<?php
$dropDate = date("j F Y,H:i", strtotime($data['dropping_date']));
$boardDate = date("j F Y,H:i", strtotime($data['pickup_date']));

$dropDate = \App\Models\General::getIndoMonths($dropDate);
$boardDate = \App\Models\General::getIndoMonths($boardDate);

$formated_hf = $data['handling_fee'] ? \General::number_format($data['handling_fee'],3) : 0;
$formated_dis = $data['coupon_discount'] ? \General::number_format($data['coupon_discount'],3) : 0;
?>
        <html><head><title></title>
        </head>
        <body yahoo="yahoo">
            
                <style>
                    body { margin: 0; }
                     body, table, td,p, a,li,blockquote { -webkit-text-size-adjust: none!important; font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; font-style: normal; font-weight: 400; }

                        .responsiveDataTbl { width: 100%!important; margin: 0 auto!important; font-size: 0; }
                      
                        .heghit{
                            height:auto;
                            color:rgb(102,102,102);
                            font-size:12px;
                            margin:0;
                            font-family: "Arial";
                        }
                        
                </style>
                
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <!--<tbody>-->
            <tr>
              <td align="center"><table cellspacing="0" cellpadding="0" class="responsiveTable" width="640px">
                  <!--<tbody>-->
				  <tr>
                        <td height="17" bgcolor="#3e9143"></td>
                  </tr>
                    <tr valign="top">
                      <td height="13" align="center" valign="top"><img class="responsiveImage" src="{{URL::to('assets/images/topLine.png')}}" width="100%" height="13" style="" alt=""/></td>
                    </tr>
                    <tr>
                      <td align="center"><table border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                          <!--<tbody>-->
                            <tr>
                              <td width="15" height="15"></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td valign="middle" align="center"><a href="#"><img src="{{URL::to('assets/images/logo-header-2-new.png')}}" width="130" height="80" alt=" Bus Ticket"/></a></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td></td>
                              <td width="15" height="15"></td>
                            </tr>
                          <!--</tbody>-->
                        </table></td>
                    </tr>
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <!--<tbody>-->
                            <tr>
                              <td height="4"></td>
                            </tr>
                          <!--</tbody>-->
                        </table></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                      <td valign="top"><table width="100%" class="responsiveDataTbl" border="0" cellspacing="0" cellpadding="0">
                          <!--<tbody>-->
                            <tr>
                            </tr>
                            <tr>
                                <td valign="top"><div id="body_div">
                                        <div id="book_route_div" style="border:1px solid #ccc; display:inline-block;width:95%;margin-left: 16px;">
                                            <div id="booking_id" style="text-align:center;font-size:19px; color:#414042;padding:5px; font-family:Gotham,'Helvetica Neue',  Helvetica, Arial, sans-serif; margin-bottom:-5px" >Booking ID </div>
                                            <div id="ticket_id" style="font-size:18px;border-bottom: 1px solid #ccc;border-bottom:1px solid #ccc;
                                    padding: 0 5px 5px;text-align:center;font-weight:bold; color:#3e9143; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; margin-bottom:0px"> <?php echo  $data['booking_id'] ?> </div> 
                                
                     <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <!--<tbody>-->
                    <tr>
                        <td class="from_to" style="width:330px;text-align:center;padding:10px;">
                                                <div class="berangkat" style="font-size:12px;color:rgb(102,102,102);font-family: 'Arial';">Berangkat</div>
                                                <h3 class="city_name" style="font-weight:bold;color:#000;font-size: 20px;margin-bottom: 5px;text-transform:uppercase;"><?php echo strtoupper($data['from_terminal']['loc_citi']['name']) ?></h3>
                                                <p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';"><?php echo  $data['from_terminal']['name'] ?></p>
                                                <p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';" ><?php echo  $data['from_terminal']['name'].','.$data['from_terminal']['loc_citi']['name'].','.$data['from_terminal']['loc_district']['name'] ?></p>
						<p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';" > <?php echo  $boardDate ?></p>
                                            </td>
                                            <td id="arrow" style="padding:10px;"><img src="{{URL::to('assets/images/right-arrow.png')}}" width="17px" height="35px"></td>
                                            <td class="from_to" style="width:330px;text-align:center;padding:10px;">
                                                <div  class="berangkat" style="font-size:12px;color:rgb(102,102,102);font-family: 'Arial';">Tiba</div>
                                                <h3  class="city_name" style="font-weight:bold;color:#000;font-size: 20px;margin-bottom: 5px;text-transform:uppercase;"><?php echo  strtoupper($data['to_terminal']['loc_citi']['name']) ?></h3>
                                                <p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';" ><?php echo  $data['to_terminal']['name'] ?></p>
                                                <p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';" ><?php echo  $data['to_terminal']['name'].','.$data['to_terminal']['loc_citi']['name'].','.$data['to_terminal']['loc_district']['name'] ?></p>
						<p class="heghit" style="height:auto;color:rgb(102,102,102);font-size:12px;margin:0;font-family: 'Arial';" > <?php echo  $dropDate ?></p>
                                  </td>
                                  </tr>
                                    <!--</tbody>-->
                                  </table>
                                  </div>
                                        <div class="empty_div" style="height:20px; clear:both; width:100%; display:block"></div>
                                        <div id="amount_div" style="border:1px solid #ccc;border-radius:10px;">
                                            <div id="pembayaran" style="color:#625c5c;border-bottom:1px solid #ccc;text-align:center;font-size:20px;padding:6px;font-weight:bold;">Rincian Pembayaran</div>
                                            <table id="table_ticket" style="margin:10px;">
                                                <tr><th id="total_amount" style="float:left;">Harga Tiket :</th></tr>
                                            <tr>
                                                <td class="td_width" style="width:800px;">
                                                    <span class="font_size" style="font-size:15px;">Total Tiket</span>
                                                    <span  class="font_size" style="font-size:15px;">(<span><?php echo  $data['nos'] ?></span> x <span id="per_ticket" style="color:#390;font-weight:bold;">Rp. <?php echo  \General::number_format($data['booking_seats'][0]['amount'],3) ?> </span>)</span>
                                                </td>
                                                <td class="result" style="width: 140px;padding: 0 20px;">
                                                	<span  class="font_size" style="font-size:15px;">Rp. </span>
                                                        <span class="font_size_right" style="font-size:15px;float:right;"> <?php echo  \General::number_format($data['booking_seats'][0]['amount']*$data['nos'],3) ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  class="td_width" style="width:800px;">
                                                	<span  class="font_size" style="font-size:15px;">Biaya Layanan</span>
                                                 </td>
                                                 <td class="result" style="width: 140px;padding: 0 20px;">
    
                                                	<span class="font_size" style="font-size:15px;">Rp. </span>
                                                        <span class="font_size_right" style="font-size:15px;float:right;"> <?php echo $formated_hf ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_width" style="width:800px;">
                                                	<span  class="font_size" style="font-size:15px;">Diskon</span>
                                                 </td>
                                                 <td class="result" style="width: 140px;padding: 0 20px;">
    
                                                	<span  class="font_size" style="font-size:15px;">Rp. </span>
                                                        <span class="font_size_right" style="font-size:15px;float:right;"> <?php echo $formated_dis ?></span>
                                                </td>
                                            </tr>
                                            <tr id="tr_margin" style="margin-top:30px;">
                                                <td id="td_width" style="width:800px;padding-top:30px;">
                                                    <span id="harga_ticket" style="font-size:18px;font-weight:bold;">Total Harga Tiket</span>
                                                 </td>
                                                 <td id="total_harga" style="width: 170px;padding:30px 20px 0;">
    
                                                     <span id="span_rp" style="font-size:18px;color:#390;font-weight:bold;">Rp. </span>
                                                     <span id="spna_amount" style="font-size:18px;float:right;color:#390;font-weight:bold;"> <?php echo  \General::number_format((($data['booking_seats'][0]['amount']*$data['nos']) + $data['handling_fee'] - $data['coupon_discount']),3); ?></span>
                                                </td>
                                            </tr>
                                        </table>
                                 </div>
                                        <div class="empty_div" style="height:20px; clear:both; width:100%; display:block"></div>
                                        <div id="mid_image" style="margin: 0 auto; text-align: center;"><img src="{{URL::to('assets/images/image-mid.jpg')}}"></div>
                                        <div  class="empty_div" style="height:20px; clear:both; width:100%; display:block"></div>
                                  <div id="descrption" style="font-size:14px;padding-bottom:5px; color:#000; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; margin-bottom:0px; line-height:1.5"> <strong>CATATAN:</strong>
                                      <ol id="ol" style="list-style-type:decimal; margin:5px 0 0; color:#414042; font-size:13px; padding-left:15px">
                                          <li class="li_margin" style="margin-bottom:5px">Harap print e-Tiket ini dibawa untuk ditukarkan dengan tiket bus resmi.</li>
        <li class="li_margin" style="margin-bottom:5px">Waktu yang tercantum pada tiket adalah waktu setempat.</li>
        <li class="li_margin" style="margin-bottom:5px">Harap tiba di terminal keberangkatan 60 menit sebelum waktu keberangkatan yang tercantum pada e-Tiket.</li>
        <li class="li_margin" style="margin-bottom:5px">Nomor kursi dapat berubah sesuai kebijakan operator pada saat keberangkatan.</li>
        <li class="li_margin" style="margin-bottom:5px">Harga dapat berubah sewaktu-waktu pada saat keberangkatan sesuai dengan kebijakan masing-masing operator dikarenakan high season, liburan panjang, lebaran dan lain-lain yang berpotensi terhadap kenaikan tiket. Tambahan biaya tiket tersebut akan dibebankan kepada penumpang.</li>
        <li class="li_margin">BUSTIKET tidak bertanggung jawab atas keterlambatan atau penjadwalan ulang bus. Hal tersebut berada 
             dibawah kendali masing-masing manajemen bus.</li>
        <li class="li_margin" style="margin-bottom:5px">Jika Anda menghadapi masalah, atau memiliki pertanyaan silahkan hubungi kami di 0812-8000-3919</li>
                                    </ol>
                                  </div>
                                </div></td>
                            </tr>
                            <tr>
                            </tr>
                          <!--</tbody>-->
                        </table></td>
                    </tr>
                    <tr>
                      <td height="20"></td>
                    </tr>
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <!--<tbody>-->
                            <tr>
                              <td height="5" bgcolor="#fcb912"></td>
                            </tr>
                            <tr>
                              <td height="4"></td>
                            </tr>
                            <tr>
                              <td height="50" bgcolor="#3e9143">
                                  <span id="phon_img" style="float:left;padding:10px;" ><img src="{{URL::to('assets/images/phon.png')}}" style="height: 25px;width: 25px;"></span>
                                  <span id="phon_num" style="color:#fff;display: inline-block; margin: 10px 0 0;font-size: 20px;font-weight: bold;">0812-8000-3919</span>
                                  <span id="email_id" style="color:#fff;float:right; margin: 10px 10px 0 0;font-size: 20px;font-weight: bold;">cs@bustiket.com</span>
                                  <span id="email_img" style="float:right;padding:10px;margin-top:3px;" ><img src="{{URL::to('assets/images/email.png')}}" style="height: 25px;width: 25px;"></span>
                              </td>
                            </tr>
                          <!--</tbody>-->
                        </table></td>
                    </tr>
                  <!--</tbody>-->
                </table></td>
            </tr>
          <!--</tbody>-->
        </table>
        </body>
        </html>

