@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Pendaftaran Perusahaan Jasa Transportasi</h1>
                            </header>

                            <div class="item-company">
                                <h2 class="item-company-title">Keuntungan Perusahaan Bus menjadi mitra BUSTIKET :</h2>

                                <div class="item-company-content">
                                    <ul class="item-company-list">
                                        <li>
                                            <h3 class="item-company-list-title">Meningkatkan Efisiensi Kerja</h3>
                                            Sistem BUSTIKET juga akan meningkatkan efisiensi kerja perusahaan bus karena proses pemesanan tiket sudah secara otomatis dan didukung oleh sistem teknologi canggih, yang memudahkan semua data laporan penjualan dapat dilihat kapanpun dan dimanapun.
                                        </li>

                                        <li>
                                            <h3 class="item-company-list-title">Mengurangi Biaya Operasional</h3>
                                            Dengan menggunakan sistem BUSTIKET juga akan mengurangi biaya operasional. Staff dan agen dapat melakukan pemesanan tiket secara mandiri tanpa memerlukan panggilan telepon ke staff tiketing.
                                        </li>
                                        <li>
                                            <h3 class="item-company-list-title">Meningkatkan Penjualan</h3>
                                            Perusahaan bus tidak hanya mengandalkan agen saja dalam meningkatkan penjualan. Dengan menggunakan sistem BUSTIKET, penumpang bus dapat langsung melakukan pemesanan tiket melalui situs BUSTIKET.COM dimanapun dan kapanpun. Hal ini akan memberikan peningkatan penjualan tiket secara signifikan.
                                        </li>

                                        <li>
                                            <h3 class="item-company-list-title">Meningkatkan Citra Perusahaan</h3>
                                            Dikarenakan harga software yang amat mahal, maka tak ayal hanya sedikit perusahaan bus yang telah menjalani sistem tiketing. BUSTIKET memberikan solusi bagi perusahaan bus dalam meningkatkan eksklusifitas dan nilai tambah terhadap citra perusahaan.
                                        </li>

                                        <li>
                                            <h3 class="item-company-list-title">Promosi yang Kuat</h3>
                                            Melalui situs BUSTIKET.COM dan memaksimalkan jaringan pemasaran yang sangat luas, akan lebih banyak masyarakat yang mengetahui perusahaan bus terkait yang diyakini dapat memberikan kontribusi yang tinggi terhadap volume penjualan. 
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- / .item-company -->

                            <div class="item-company">
                                <h2 class="item-company-title">Daftar Sekarang!</h2>

                                <div class="item-company-content">
                                    <div class="form-wrapper row">
                                        <form action="#" class="form-company" id="frmBus">
                                            <div class="col-md-6">
                                                <h3 class="item-company-content-title">Profil Operator</h3>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nama Operator</span>
                                                    <input type="text" id="oprName" placeholder="example" >
                                                </p>

                                                <div class="form-field style-2">
                                                    <span class="form-field-label">Tipe Operator</span>
                                                    
                                                    <span class="clearfix space space-15"></span>
                                                    <fieldset>
                                                        <input type="radio"  name="operator" value="bus" id="bus">
                                                        <label for="bus">Bus</label>

                                                        <input type="radio" name="operator" id="shuttle" value="shuttle" checked>
                                                        <label for="shuttle">Shuttle</label>

                                                        <input type="radio" name="operator" value="travel" id="travel">
                                                        <label for="travel">Travel</label>
                                                    </fieldset>
                                                    <small class="form-field-desc">*dapat memilih lebih dari satu</small>
                                                </div>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Standar Harga Tiket</span>
                                                    <input type="text" id="stdTicketPrice" placeholder="Rp 300.000" >
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Alamat</span>
                                                    <input type="text" id="addressP1" placeholder="Jl. xyz RT 01 RW 01" >
                                                    <input type="text" id="addressP2" name="" placeholder="South Jakarta, Jakarta, Indonesia" >
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">No Telpon</span>
                                                    <input type="text" id="phoneNo" name="" placeholder="0812 1234751">
                                                </p>
                                            </div>

                                            <div class="col-md-6">
                                                <h3 class="item-company-content-title">Profil Pimpinan</h3>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nama Pimpinan</span>
                                                    <input type="text" id="loprName" placeholder="John Doe" >
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Alamat</span>
                                                    <input type="text" id="lAddressP1" placeholder="Jl. xyz RT 01 RW 01" >
                                                    <input type="text" id="lAddressP2" name="" placeholder="South Jakarta, Jakarta, Indonesia">
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">No. Telpon</span>
                                                    <input type="text" id="lPhoneNo" placeholder="0812 1234751" >
                                                </p>

                                                <p class="form-field form-submit text-right">
                                                    <input type="button" id="btnReg" name=""  class="btn btn-radius btn-green" value="DAFTAR">
                                                    <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                                                </p>
                                                <div class="alert alert-success" id="onmail" style="display: none">
                                                    <strong></strong> 
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
             $(function(){
                 
                $("#btnReg").click(function(){
                    $("#btnReg").hide();
                    $("#btnS").show();
                    
                    var oprName=$("#oprName").val();
                    var oprType=$("input[name=operator]:checked").val();
                    var stdTicketPrice=$("#stdTicketPrice").val();
                    var add1=$("#addressP1").val();
                    var add2=$("#addressP2").val();
                    var phoneNo=$("#phoneNo").val();
                    
                    var loprName=$("#loprName").val();
                    var ladd1=$("#lAddressP1").val();
                    var ladd2=$("#lAddressP2").val();
                    var lphoneNo=$("#lPhoneNo").val();
                    
                    
                 var flag=1;   
                
                  if(oprName==''){
                       $("#oprName").focus();
                       $("#oprName").css('border-color','red');
                       flag=0;
                   }else{
                          $("#oprName").css('border-color','black');
                   }
                   
                   if(stdTicketPrice==''){
                       $("#stdTicketPrice").focus();
                       $("#stdTicketPrice").css('border-color','red');
                       flag=0;
                   }
                   else if(!stdTicketPrice.match(/^\d+$/))
                   {
                       $("#stdTicketPrice").focus();
                       $("#stdTicketPrice").css('border-color','red');
                       flag=0;
                       alert('Harga Tiket Harus Digandakan');
                       
                   }
                   else{
                       $("#stdTicketPrice").css('border-color','black');
                   }
                   
                   if(add1==''){
                       $("#addressP1").focus();
                       $("#addressP1").css('border-color','red');
                       flag=0;
                   }else{
                       $("#addressP1").css('border-color','black');
                   }
                   
                   if(add2==''){
                       $("#addressP2").focus();
                       $("#addressP2").css('border-color','red');
                       flag=0;
                   }else{
                       $("#addressP2").css('border-color','black');
                   }
                   
                   if(phoneNo==''){
                       $("#phoneNo").focus();
                       $("#phoneNo").css('border-color','red');
                       flag=0;
                   }
                   else if(!phoneNo.match(/^\d+$/))
                   {
                       $("#phoneNo").focus();
                       $("#phoneNo").css('border-color','red');
                       flag=0;
                       alert('Telepon Tidak Harus Digandakan');
                   }
                   else if((phoneNo.length)<10)
                   {
                       $("#phoneNo").css('border-color','red');
                       alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                       flag=0;
                   }else{
                       $("#phoneNo").css('border-color','black');
                   }
                   
                   if(loprName==''){
                       $("#loprName").focus();
                       $("#loprName").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#loprName").css('border-color','black');
                   }
                   
                   if(ladd1==''){
                       $("#lAddressP1").focus();
                       $("#lAddressP1").css('border-color','red');
                       flag=0;
                   }
                   else{
                      $("#lAddressP1").css('border-color','black'); 
                   }
                   
                   if(ladd2==''){
                       $("#lAddressP2").focus();
                       $("#lAddressP2").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#lAddressP2").css('border-color','black');
                   }
                   
                   if(lphoneNo==''){
                       $("#lPhoneNo").focus();
                       $("#lPhoneNo").css('border-color','red');
                       flag=0;
                   }
                   else if(!lphoneNo.match(/^\d+$/))
                   {
                       $("#lPhoneNo").focus();
                       $("#lPhoneNo").css('border-color','red');
                       alert('Telepon Tidak Harus Digandakan');
                       
                       flag=0;
                      
                   }
                   else if((lphoneNo.length)<10)
                   {
                       alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                       flag=0;
                   }
                   else
                   {
                       $("#lPhoneNo").css('border-color','black');
                   }   
                   
     
                   if(flag==1){
                       var token = $("[name=csrf-token]").attr("content");
                       var oprData={oprName:oprName,oprType:oprType,price:stdTicketPrice,add1:add1,add2:add2,phoneNo:phoneNo,loprName:loprName,ladd1:ladd1,ladd2:ladd2,lphoneNo:lphoneNo}
                      
                        $.ajax({
                           url:'{{URL::to("company-bus")}}',
                           type:'post',
                           dataType:'json',
                           data:{_token:token,oprData:oprData},
                           success:function(data){
                               var jdata=JSON.parse(JSON.stringify(data));
                               if(jdata.flag==1){
                                   $("#onmail").show();
                                   $("#onmail").text(jdata.msg);
                                   $("#frmBus")[0].reset();
                                   $("#btnReg").show();
                                   $("#btnS").hide();
                               }
                               else if(jdata.flag==0){
                                   $("#btnReg").show();
                                   $("#btnS").hide();
                               }
                           }
                       });
                   }
                   else{
                       $(this).show();
                       $("#btnS").hide();
                   }  
                });                
    });    
        </script>
@stop
