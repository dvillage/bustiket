@extends('layouts.site_layout')
@section('content')
<main class="site-main site-main-login">
    <div class="site-main-login-mask"></div>
    <section class="section section-login">
        <div class="container">

            <div class="section-login-wrapper section-register-wrapper">
                <header class="section-header">
                    <h1 class="section-header-title">Register</h1>        
                </header>

                <div class="section-content">
                    <div class="form-wrapper">
                        <form action="" class="row form-login form-register" method="POST" action="{{url("signup")}}">
                            <div class="col-sm-6 clearfix">
                                <p class="form-field">
                                    <span class="form-field-label">Nama Lengkap</span>
                                    <input type="text" name="user_firstname" value="{{ old('user_firstname') }}">
                                </p>
                            </div>

                            <div class="col-sm-6 clearfix">
                                <p class="form-field">
                                    <span class="form-field-label">Alamat Email</span>
                                    <input type="email" name="user_email">
                                </p>
                            </div>

                            <div class="col-sm-6 clearfix">
                                <p class="form-field">
                                    <span class="form-field-label">Kata Sandi</span>
                                    <input type="password" name="user_password">
                                </p>
                            </div>

                            <div class="col-sm-6 clearfix">
                                <p class="form-field">
                                    <span class="form-field-label">Ulangi kata Sandi</span>
                                    <input type="password" id="confirm-password">
                                </p>
                            </div>

                            <div class="col-sm-12 clearfix text-center">
                                <p class="form-field form-submit">
                                    <button class="btn btn-green btn-radius btn-minwidth">Daftar</button>
                                </p>
                            </div>
                            <?php 
                                    if(isset($body['new_signup']) && !empty($body['new_signup']))
                                    { 
                                        $color = $body['new_signup']['flag'] == 1 ? "green" : "red";
                                        ?> 
                            <div class="col-sm-12 clearfix text-center" style="color: <?php echo $color;?>">
                                <p class="form-field form-submit">
                                  <?php 
                                        echo $body['new_signup']['msg'];
                                    ?>
                                </p>
                            </div>
                            <?php } ?>

                            <span class="clearfix space space-30"></span> 

                            <p class="form-field text-center info clearfix">
                                Atau Masuk dengan:
                                <span class="clearfix space space-15"></span>
                            </p>

                            <div class="col-sm-12 clearfix">
                                <p class="form-field login-with clearfix">
                                    <a href="{{url('auth/facebook')}}" class="btn btn-line btn-line-fb pull-left">
                                        <span class="sprite icon-facebook-square"></span> Facebook
                                    </a>
                                    <a href="{{url("auth/google")}}" class="btn btn-line btn-line-google pull-right">
                                        <span class="sprite icon-google-square"></span> Google
                                    </a>
                                </p>
                            </div>
                            <input type="hidden" name="type" value="normal" />
                            <input type="hidden" name="device_token" value="{{csrf_token()}}" />
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
@stop