@extends('layouts.site_layout')

@section('content')
<?php 
$banners = $body['banners'];
?>
<div class="date-holder opened" style="display:none">
    <div class="onward gtm-open-cal" id="onward_cal">
        <label >Tanggal Perjalanan</label>
<i class="fa fa-times" aria-hidden="true" style="font-size:1.5em;float: right"></i>
        <div>
            <?php $today = date("Y-m-d");?>
            <div class="date" id="cal_date"><?php echo date("d",  strtotime($today.'+ 3 days'));?></div>
            <div class="day-holder">
                <span class="dd-mm-yy" id="cal_do_week"><?php echo date("l",  strtotime($today.'+ 3 days'));?></span>
                <br>
                <span class="dd-mm-yy" id="cal_mnth"><?php echo date("M",  strtotime($today.'+ 3 days'));?> </span>
                <span class="dd-mm-yy" id="cal_yr"><?php echo date("y",  strtotime($today.'+ 3 days'));?></span>
            </div>
        </div>
    </div>
    <div class="cal-popup" id="cal_container" >
    </div>
</div>
<div id="banner" class="page-header">
<div class="container_banner_home">
        <header id="myCarousel" class="carousel slide"> 
            <ol class="carousel-indicators">
                <?php for($i=0;$i<count($banners);$i++) { ?>
                <li data-target="#myCarousel" data-slide-to="{{$i}}" class=""></li>
                <?php } ?>
            </ol>
            <div class="carousel-inner">
                <?php foreach($banners as $ban) { ?>
                <?php 
                $height = $ban['height'] > 0 ? $ban['height'].$ban['height_type'] : '';
                $width = $ban['width'] > 0 ? $ban['width'].$ban['width_type'] : '';
                $hw = $height ? 'height:'.$height.';' : '';
                $hw .= $width? 'width:'.$width.';' : '';
                ?>
                <div class="item"> <a href="{{ $ban['redirect_to'] }}"><img src="{{URL::to('assets/images/banner').'/'.$ban['file']}}" style="{{$hw}}" alt="{{$ban['alt']}}"> </a></div>
                <?php } ?>
            </div>
               <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev"></span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="icon-next"></span> </a> 
        </header>
        <div class="row-1 " id="enjoy_search">
        <div class="left3 img-responsive">
        <div class="left2">
            <div class="container">
                 <div class="journey_tab">
                    <div id="tiket" class="col-xs-6 col-sm-3 ctab active">
                        <img src="{{URL::to('assets/images/index/bussel.png')}}" id="bus1-img1" alt="" class="pull-left">
                        <img src="{{URL::to('assets/images/index/busunsel.png')}}" id="bus1-img2" alt="" class="pull-left">
                       Tiket Bus
                    </div>
                    <div id="pariwisata" class="col-xs-6 col-sm-3 ctab">
                        <img src="{{URL::to('assets/images/index/bussel.png')}}" id="bus2-img1" alt="" class="pull-left ">    
                        <img src="{{URL::to('assets/images/index/busunsel.png')}}" id="bus2-img2" alt="" class="pull-left">
                        Pariwisata
                    </div>
                </div>
            </div>
            <div class="container ">
                <div class="left-session">
                    <div class="tab-content" id="myTabContent">
                        <div class="err_msg" id="errmsg"></div>
                        <div id="bus-malam" class="tab-pane fade active in">
                        <img src="{{URL::to('assets/images/index/spinner-loop.gif')}}" class="preload" style="display: none;">
                            <form role="form" name="frm_search" id="frm_search" action="" method="post">
                                <div class="city-zone" style="display: block;">
                                    <ul>
                                       <li style="width: 303.333px;">
                                        <div class="pading_w">
                                                <h3 class="city-icon"><font style="font-size:20px;font-family:arial;">1</font>&nbsp;Kota Asal</h3>
                                                <input name="ter_from" id="ter_from" class="form-control ac_input input-group pull-left" placeholder="Pilih Kota Keberangkatan" autocomplete="off">  
                                                <img src="{{URL::to('assets/images/index/in-out.png')}}" onclick="switchTag()" alt="" style="width:20px;float: right;margin-top:7px;cursor:pointer;" class="hidden-xs hidden-sm  pull-right1">    
                                            </div>
                                        </li>
                                        <li style="width: 303.333px;">
                                           <div class="pading_w">
                                                <h3 class="city-icon"><font style="font-size:20px;font-family:arial;">2</font>&nbsp;Kota Tujuan</h3>
                                                <div class="input-group2">
                                                    <input type="text" name="tag" placeholder="Pilih Kota Tujuan..." id="tag" onchange="return dat_val();" onfocus="get_val_points();" class="form-control2">
                                                    </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="date-zone" style="display: block;">
                                    <div class="cover-dataxx">
                                        <ul>
                                            <li style="width: 303.333px;">
                                                <div class="pading_w">
                                                    <h3 class="calender-icon hidden-sm hidden-md hidden-lg">
                                                        <i class="fa fa-calendar" style="color:#000;margin-left:5px"></i>&nbsp;<font style="font-size:20px;font-family:arial;">3</font>
                                                        Tanggal Keberangkatan
                                                    </h3>
                                                    <h3 class="calender-icon hidden-xs"><font style="font-size:20px;font-family:arial;">3</font>&nbsp;Tanggal Keberangkatan</h3>
                                                    
                                                    <div class="input-group date hidden-xs" id="datetimepicker1">
                                                        <input type="text" id="datepicker" name="datepicker" class="form-control datepicker hasDatepicker" placeholder="Tanggal Berangkat">
                                                        <span class="input-group-addon "> <img src="{{URL::to('assets/images/index/eamil.png')}}" class="img-responsive" width="21" height="21" alt=""></span> 
                                                    </div>
                                                    <div class="input-group date hidden-sm hidden-md hidden-lg " id="datetimepicker1">
                                                        <input type="text" id="departure_date" class="form-control" placeholder="Tanggal Berangkat">
                                                        <span class="input-group-addon "> </span> 
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="datetimepicker2" style="width: 303.333px;">
                                                <div class="pading_w">
                                                    <h3>Tanggal Pulang</h3>
                                                    <div class="input-group date dclass" id="for-date1">
                                                        <input type="text" id="datepicker1" name="datepicker1" class="form-control datepicker1 hasDatepicker" placeholder="Tanggal Pulang" disabled="disabled">
                                                        <span class="input-group-addon "> <img src="{{URL::to('assets/images/index/eamil.png')}}" width="21" height="21" alt=""> </span> </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="custom-button">
                                        <div>
                                            <label>
                                                <input type="radio" name="triptype" id="oneway" value="1" checked="checked" onclick="showreturn(this.value);">
                                                <label for="thing"></label>
                                                <span class="lbl padding-8">Sekali jalan</span>
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input type="radio" name="triptype" id="round" value="2" onclick="showreturn(this.value);">
                                                <label for="thing"></label>
                                                <span class="lbl padding-8">Bolak balik</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="action-zone" style="display: block;">
                                    <div class="pading_w">
                                        <h3 style="visibility:hidden">submit</h3>
                                        <div class="ticket-btn">
                                            <input type="hidden" name="search" value="PESAN TIKET">
                                            <input type="submit" value="PESAN TIKET" onclick="return validate();" name="search" alt="Cari Tiket">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                        <div id="bus-malam2" class="tab-pane fade in" style="display:none">
                            <form role="form" name="frm_search" id="frm_search" action="" method="post">
                                <div class="city-zone" style="display: block;">
                                    <ul>
                                        <li style="width: 303.333px;">
                                            <div class="pading_w">
                                                <h3 class="city-icon"><font style="font-size:20px;font-family:arial;">1</font>&nbsp;Kota Asal</h3>
                                                <div class="input-group pull-left">
                                                    <input name="ter_from_pa" id="ter_from_pa" class="form-control ac_input" placeholder="Masukan Kota..." autocomplete="off">
                                                     </div>
                                                  <img src="{{URL::to('assets/images/index/in-out.png')}}" alt="" style="width:20px;float: right;margin-top:-30px" class="hidden-xs hidden-sm  pull-right1">
                                            </div>
                                        </li>
                                        <li style="width: 303.333px;">
                                            <div class="pading_w">
                                                <h3 class="bus-icon hidden-sm hidden-md hidden-lg">
                                                    <i class="fa fa-bus" aria-hidden="true" style="margin-left: 5px;color:#000"></i>&nbsp;<font style="font-size:20px;font-family:arial;">2</font>
                                                    Bus Type
                                                </h3>
                                                <h3 class="bus-icon hidden-xs"><font style="font-size:20px;font-family:arial;">2</font>&nbsp;Bus Type</h3>
                                                <div class="input-group">
                                                    <input type="hidden" name="ter_bustype_pa" id="ter_bustype_pa">
                                                    <!--dropdown-->
                                                    <div class="">
                                                        <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" style="width: 100%;border:1px #000 solid; border-radius: 5px;height: 37px;" aria-expanded="true"><span class="filter-option pull-left" id="dropdown-button">Select Bus Type</span>&nbsp;</button>
                                                            <ul class="dropdown-menu inner" id="bus_type_block_ul" role="menu" style="max-height: 249px; overflow-y: auto; min-height: 80px;border-radius: 0px;">
                                                                <li class="selected" onclick="selectBusType('Select All')" style="width: 303.333px;"><a><span class="text">Select All</span></a></li>
                                                                <li class="selected" onclick="selectBusType('AC')" style="width: 303.333px;"><a><span class="text">AC</span><i id="bus_type_ac_check" class="fa fa-check" style="margin-right: 15px;float: right;display: none;"></i></a></li>
                                                                <li class="selected" onclick="selectBusType('Non AC')" style="width: 303.333px;"><a><span class="text">Non AC</span><i id="bus_type_nonac_check" class="fa fa-check" style="margin-right: 15px;float: right;display: none;"></i></a></li>
                                                            </ul>
                                                    </div>
                                                    <!--drop down-->
                                                    
                                                    </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="date-zone" style="display: block;">
                                    <div class="cover-dataxx">
                                        <ul>
                                            <li style="width: 303.333px;">
                                                <div class="pading_w">
                                                    <h3 class="calender-icon hidden-sm hidden-md hidden-lg"><i class="fa fa-calendar" style="color:#000;margin-left:5px"></i>
                                                       <font style="font-size:20px;font-family:arial;">3</font>
                                                        Tanggal Keberangkatan
                                                    </h3>
                                                    <h3 class="calender-icon hidden-xs"><font style="font-size:20px;font-family:arial;">3</font>&nbsp;Tanggal Keberangkatan</h3>
                                                    <div class="input-group date hidden-xs" id="datetimepicker1">
                                                        <input type="text" id="datepicker11" name="datepicker11" class="form-control datepicker hasDatepicker" placeholder="Tanggal Berangkat">
                                                        <span class="input-group-addon "> <img src="{{URL::to('assets/images/index/eamil.png')}}" width="21" height="21" alt=""></span> 
                                                    </div>
                                                    <div class="input-group date hidden-sm hidden-md hidden-lg" id="datetimepicker1">
                                                        <input type="text" id="departure_date11" class="form-control" placeholder="Tanggal Berangkat">
                                                        <span class="input-group-addon "> </span> 
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="datetimepicker2" style="width: 303.333px;">
                                                <div class="pading_w">
                                                    <h3>Tanggal Pulang</h3>
                                                    <div class="input-group date dclass" id="for-date1">
                                                        <input type="text" id="datepicker11" name="datepicker11" class="form-control datepicker11" placeholder="Tanggal Pulang" disabled="disabled">
                                                        <span class="input-group-addon "> <img src="{{URL::to('assets/images/index/calendar_icon.png')}}" width="21" height="21" alt=""> </span> </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="custom-button">
                                        <div>
                                            <label>
                                                <input type="radio" name="triptype" id="oneway" value="1" checked="checked" onclick="showreturn(this.value);">
                                                <label for="thing"></label>
                                                <span class="lbl padding-8">Sekali jalan</span>
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input type="radio" name="triptype" id="round" value="2" onclick="showreturn(this.value);">
                                                <label for="thing"></label>
                                                <span class="lbl padding-8">Bolak balik</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="action-zone" style="display: block;">
                                    <div class="pading_w">
                                        <h3 style="visibility:hidden">submit</h3>
                                        <div class="ticket-btn2">
                                            <input type="hidden" name="pariwisata_search" value="PESAN BUS  ">
                                            <input type="submit" value="PESAN BUS" onclick="return pariwisataValidate();" name="pariwisata_search" alt="Cari Tiket">
                                        </div>
                                    </div>
                                </div>
                            </form>
							</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
	 <div class="clearfix"></div>
<div class="home_slider_sec">
	<div class="container bx-wrapper">
		<div class="tile_box hidden-xs">
			<font color="#ffffff">
      <!--<h1>Tiket Bus Online, Pesan Tiket Bus Murah, Tiket Online, Tiket Bus Malam, Sewa bus pariwisata, tiket bus Lorena, tiket bus Pahala kencana, tiket bus Haryanto, tiket bus Agra Mas, tiket bus Shantika, tiket bus Damri, tiket bus Nusantara, tiket bus Hiba Utama, tiket bus ALS, tiket bus Prima Jasa, tiket bus Sinar Jaya, tiket bus Harapan Jaya, Harga Tiket Bus, Tiket Kereta Api Online, Bus Indonesia, Tiket Mudik Lebaran</h1>-->
      </font>
	  <h2>TUJUAN FAVORIT</h2>
		</div>
		<div class="fav img-responsive">
            <h3><span>icon </span> SILAHKAN PILIH TUJUAN FAVORIT ANDA</h3>
		</div>
		<div id="owl-demo" class="owl-carousel owl-theme inner_fav_slider" style="opacity: 1; display: block;">
			<div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 9720px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/fav_img-8.jpg')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>BANYUWANGI<span>Jakarta - Banyuwangi</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Jakarta', 'Banyuwangi', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/fav_img-7.jpg')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>PALEMBANG<span>Jakarta - Palembang</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Jakarta', 'Palembang', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/secr2_img3.png')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>PRAMBANAN<span>Tangerang - Prambanan</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Tangerang', 'Prambanan', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/fav_img-5.jpg')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>SUMENEP<span>Jakarta - </span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Jakarta', '', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/fav_img-7.jpg')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>BLITAR<span>Bandung - Blitar</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Bandung', 'Blitar', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="http://192.168.0.104/bustiket/images/secr2_img5.png" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>YOGYAKARTA<span>Cilegon - Yogyakarta</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Cilegon', 'Yogyakarta', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="http://192.168.0.104/bustiket/images/secr2_img4.png" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>SURABAYA<span>Tangerang - Surabaya</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Tangerang', 'Surabaya', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/fav_img-6.jpg')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>SEMARANG<span>Bogor - Semarang</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Bogor', 'Semarang', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div><div class="owl-item" style="width: 270px;"><div class="col-sm-3_f owl-item" style="width: 270px;">                                           
                        <div class="favorite_sl_item item">
                            <div class="thumbnail"><img src="{{URL::to('assets/images/index/secr2_img3.png')}}" width="450" height="301" alt="" style="height:131px"></div>
                            <div class="favorite_sl_data">
                                <h4>JAKARTA<span>Bandung - Jakarta</span> </h4>
                                <p>Harga Tiket</p>
                                <div class="amo_favorite"> <em>Rp</em> </div>
                                <div class="action_favorite"> <a onclick="favRoutes('Bandung', 'Jakarta', '30-07-2017')" class="btn btn-primary">PESAN TIKET</a></div>
                            </div>
                        </div>
                    </div></div></div></div>
					<div class="owl-controls clickable">
					<div class="owl-pagination">
					<div class="owl-page active"><span class=""></span></div>
					<div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div>
					<div class="customNavigation">
                <a class="btn prev"><img src="http://192.168.0.104/bustiket/image/img/pre.png"></a>
                <a class="btn next"><img src="http://192.168.0.104/bustiket/image/img/right.png"></a>
              </div>
		</div>
	</div>
	<div class="container">
		<div class="eamil-2 img-responsive hidden-xs hidden-md hidden-sm">
             <span>Dapatkan Promo Eksklusif <br>Langsung ke Email Anda</span>
<!--             <form name="email_sub" method="post" action="//bustiket.us12.list-manage.com/subscribe/post?u=e33113f8322f7802deb7b1a90&id=d10f505c08" target="blank">-->
                <input type="text" name="text" id="sname" placeholder="Masukan Nama" class="eamil-3">
              <input type="eamil" name="eamil" id="semail" placeholder="Alamat Email" class="eamil-4">
              <button class="eamil-5" type="submit" name="subscribe" id="subscribe-email" style="font-size: 16px;color: white;font-family: Arial;text-transform: uppercase;">subscribe</button>
             <!--</form>-->
            </div>
			<div class="col-xs-12 col-md-4 col-sm-4 eamil-71">
			<div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/BUSTIKET" data-action="like" data-layout="standard" data-show-faces="true" data-share="true" data-size="small" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=108503602893618&amp;container_width=315&amp;href=https%3A%2F%2Fwww.facebook.com%2FBUSTIKET&amp;layout=standard&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=small">
			<span style="vertical-align: bottom; width: 450px; height: 56px;">
			<iframe name="f3f6d32f96480a4" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.8/plugins/like.php?action=like&amp;app_id=108503602893618&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Dffca6f9a7e5268%26domain%3D192.168.0.104%26origin%3Dhttp%253A%252F%252F192.168.0.104%252Ff2002fd79e159d8%26relation%3Dparent.parent&amp;container_width=315&amp;href=https%3A%2F%2Fwww.facebook.com%2FBUSTIKET&amp;layout=standard&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=small" style="border: none; visibility: visible; width: 450px; height: 56px;" class=""></iframe>
			</span>
			</div>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-4 eamil-71" style="padding-left: 9%">
			<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="http://platform.twitter.com/widgets/tweet_button.b4d0882c4750b56021097fae9667fe03.en-gb.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en-gb&amp;original_referer=http%3A%2F%2F192.168.0.104%2Fbustiket%2F&amp;size=m&amp;text=Tiket%20Bus%20Online%20Murah%2C%20Lengkap%20dan%20Aman%20di%20Indonesia%20%E2%80%93%20Pesan%20Tiket%20Bus%20Online%20di%20BUSTIKET.COM&amp;time=1501143475572&amp;type=share&amp;url=http%3A%2F%2F192.168.0.104%2Fbustiket%2F" style="position: static; visibility: visible; width: 54px; height: 20px;"></iframe>
			<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
			<div class="col-xs-12 col-md-4 col-sm-4 eamil-71">
			<div id="___follow_0" style="position: absolute; width: 450px; left: -10000px;">
			<iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position:absolute;top:-10000px;width:450px;margin:0px;border-style:none" tabindex="0" vspace="0" width="100%" id="I0_1501143475266" name="I0_1501143475266" src="https://apis.google.com/_/widget/render/follow?usegapi=1&amp;rel=%7BrelationshipType%7D&amp;origin=http%3A%2F%2F192.168.0.104&amp;url=https%3A%2F%2Fplus.google.com%2F1111237514689795844&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.gHutCA9gF8g.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCNmsCuKHxAlQ4M37AVnVdWqLJWU7Q#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh&amp;id=I0_1501143475266&amp;parent=http%3A%2F%2F192.168.0.104&amp;pfname=&amp;rpctoken=23888592" data-gapiattached="true">
			</iframe></div><g:follow href="https://plus.google.com/1111237514689795844" rel="{relationshipType}" data-gapiscan="true" data-onload="true" data-gapistub="true"></g:follow>
			</div>
	</div>
	<div class="container-fluid section_management  hidden-xs">
		<div class="container">
			<div class="tile_box">
				<h2>MENGAPA BUSTIKET ?</h2>
				<h5>Apa Saja Keuntungan Menggunakan BUSTIKET.COM ?</h5>
			</div>
			<div class="inner_section">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <div class="col-lg-3 col-sm-6 col-xs-12 block first payment-math">
                        <div class="detail icon_payment">
                            <img src="http://192.168.0.104/bustiket/img/jrm.png">
                            <p>SMART SEARCH</p>
                        </div>
                        <div class="hover_div">
                            <span>
                                Mencari dan membandingkan berbagai jenis bus Antar Kota Antar Propinsi di seluruh Indonesia mulai dari segi harga, rute, lama perjalanan, fasilitas dan lainnya dengan teknologi algoritma search terbaru.
                                <button class="btn btn-primary active" onclick="gotoTop()">PESAN Tiket</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 block second payment-math">
                        <div class="detail icon_payment">
                            <img src="http://192.168.0.104/bustiket/img/rp13.png">
                            <p>HARGA TERJAMIN</p>
                        </div>
                        <div class="hover_div">
                            <span>
                                Harga tiket bus yang ditampilkan di BUSTIKET.COM adalah harga resmi dan terbaik, sudah termasuk biaya-biaya seperti pajak, handling fee dan lainnya.
                                <button class="btn btn-primary active" onclick="gotoTop()">Pesan Tiket</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 block third payment-math">
                        <div class="detail icon_payment">
                            <img src="http://192.168.0.104/bustiket/img/lock'.png">
                            <p>TRANSAKSI AMAN</p>
                        </div>
                        <div class="hover_div">
                            <span>
                                Teknologi SSL dari RapidSSL dengan Sertifikat yang terotentikasi menjamin privasi dan keamanan transaksi online Anda. Konfirmasi instan dan e-tiket dikirim ke email Anda.
                                <button class="btn btn-primary active" onclick="gotoTop()">Pesan Tiket</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 block last payment-math">
                        <div class="detail icon_payment">
                            <img src="http://192.168.0.104/bustiket/img/care'.png">
                            <p>PEMBAYARAN MUDAH</p>
                        </div>
                        <div class="hover_div">
                            <span>
                                Pembelian tiket menjadi semakin fleksibel dengan berbagai pilihan pembayaran dari Transfer ATM, Kartu Kredit, Internet Banking dan melalui  berbagai gerai minimarket yang tersebar di seluruh Indonesia.
                                <button class="btn btn-primary active" onclick="gotoTop()">Pesan Tiket</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 bus_icon img-responsive">
                    <img src="http://192.168.0.104/bustiket/img/bus.png" alt="">
                </div>
            </div>
        </div>
		</div>
	</div>
	<div class="main_yellow_div  hidden-xs">
		<div class="container">
			<div class="inner_container">
				<div class="container">
					<h1>PESAN TIKET BUS HANYA 4 LANGKAH PRAKTIS:</h1>
					<div class="col-md-6 col-sm-6 col-xs-12 mobile_div">
						<img class="left_arrow" src="http://192.168.0.104/bustiket/images/new_home/secr4_img2.png">
						<!--img class="center_img" src="img/slider-2.png"-->
						<img id="1" class="center_img" src="http://192.168.0.104/bustiket/img/slider.png">
						<img id="2" class="center_img" src="http://192.168.0.104/bustiket/img/slider-3.png">
						<img id="3" class="center_img" src="http://192.168.0.104/bustiket/img/slider-5.png">
						<img id="4" class="center_img active" src="http://192.168.0.104/bustiket/img/slider-4.png">
						<img class="right_arrow" src="http://192.168.0.104/bustiket/images/new_home/secr4_img1.png">
                  </div>
				  <div class="col-md-4 col-sm-6 col-xs-12 detail_div">
                    <div class="col-lg-12 col-sm-12 col-xs-12 slize active">
                        <a id="1"><img src="http://192.168.0.104/bustiket/images/bus_image/hand.png">
                        <label>Pilih Tanggal dan Kota Tujuan Anda</label></a>
                    </div>		
                    <div class="col-lg-12 col-sm-12 col-xs-12 slize">
                        <a id="2"><img src="http://192.168.0.104/bustiket/images/bus_image/hand2.png">
                        <label>Pilih Kursi dan Isi Data Penumpang</label></a>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 slize pull-right">
                        <a id="3"><img src="http://192.168.0.104/bustiket/images/bus_image/hand3.png">
                        <label>Lakukan Pembayaran</label></a>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 slize">
                        <a id="4"><img src="http://192.168.0.104/bustiket/images/bus_image/hand4.png">
                        <label>E-Tiket Dikirimkan ke Email Anda</label></a>
                    </div>
                </div>
				</div>
			</div>
		</div>
	</div>

	<div id="main_2nd_div" class="main_2nd_div  hidden-xs">   
		<div class="container">
			<div class="inner_container">
				<div class="container">
					<div class="tile_box">
						<h2>bergabunglah bersama kami</h2>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="col-xs-4 col-centered">
							<img src="http://192.168.0.104/bustiket/img/bus-1.png">
						</div>
						<div class="clearfix"></div>
						<div class="text_content">
							<div class="main_caption">Perusahaan Bus</div>
							<div class="main_decp">Tingkatkan penjualan tiket Anda <br>secara Efisien, Modern dan Murah<br>dengan bergabung menjadi mitra kami!</div>
							<div class="action_favorite"> <a class="btn btn-primary" href="register-operator.php">GABUNG DISINI</a></div>
						</div>
                 </div>
				 <div class="hidden-sm hidden-lg clearfix"><br>&nbsp;</div>
				 <div class="col-md-6 col-sm-6">
                    <div class="col-xs-4  col-centered">
                        <img src="http://192.168.0.104/bustiket/img/tiket.png">
                    </div>
                    <div class="clearfix"></div>
                    <div class="text_content">
                        <div class="main_caption">MITRA BUSTIKET</div>
                        <div class="main_decp">Ingin memiliki bisnis sendiri <br>yang Mudah, Cepat dan Aman?<br>Segeralah bergabung menjadi mitra kami! </div>
                        <div class="action_favorite"> <a class="btn btn-primary" href="spot.php">GABUNG DISINI</a></div>
                    </div>
                </div>
				</div>
			</div>
		</div>
	</div>
	<div class="main_3rd_div  hidden-xs">
		<div class="blank_layer">
			<div class="container">
				<div class="inner_container">
					<div class="container">
						<div class="col-md-6 col-sm-6 margin_right">
							<img src="http://192.168.0.104/bustiket/images/new_home/secr6_img1.png">
						</div>
						<div class="col-md-6 col-sm-6">
                        <div class="text_content margin_left">
                            <div class="main_caption">Request Operator Bus &amp;  <br> Rute Favorite Anda</div>
                            <div class="main_decp margin_left">Belum menemukan operator bus atau tujuan yang Anda butuhkan?<br>
								Jangan kuatir, silahkan menghubungi tim kami yang akan berupaya untuk segera memenuhi kebutuhan Anda.
							</div>
                            <div class="action_favorite pull-left"> <a href="mailto:info@bustiket.com" class="btn btn-primary">REQUEST</a></div>
                        </div>
                    </div>
					</div>
				</div>	
			</div>		
		</div>			
	</div>
	<div class="section_blog  hidden-xs">
		<div class="container">
			<div class="tile_box">
				<h2>BLOG &amp; PROMO</h2>
			</div>
			<div class="inner_section_payment">
            <div class="row">
                 <div class="hidden-sm hidden-lg clearfix"><br>&nbsp;</div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                             <div class="payment-math blog_promo">
                               <div class="icon_payment">
                                <span class="mybtn">NEW</span>
                                <img src="http://192.168.0.104/bustiket/images/new_home/secr7_img2.png" alt="">
									   </div>
								<div class="disc_payment">
									<span>Dec 03,2016</span>
									<br>
									<h5>Pesona Alam Medan Yang Eksotis </h5>
									<p>Pulau medan yang terkenal akan alam dan makanan yangbegitu khas, sangat menarik perhatian wisatawan manca negara, untuk menikmati alam dan makanan... <br><a href="http://google.com"><font color="#ccc">Read More &gt;&gt; </font></a></p>
									
								</div>
                        </div>
                    </div>
                             <div class="hidden-sm hidden-lg clearfix"><br>&nbsp;</div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                             <div class="payment-math blog_promo">
                               <div class="icon_payment">
                                <span class="mybtn">PROMO</span>
                                <img src="http://192.168.0.104/bustiket/images/new_home/secr7_img3.png" alt="">
                               </div>
                        <div class="disc_payment">
                            <span>Dec 03,2016</span>
                            <br>
                            <h5>Pesona Alam Medan Yang Eksotis </h5>
                            <p>Pulau medan yang terkenal akan alam dan makanan yangbegitu khas, sangat menarik perhatian wisatawan manca negara, untuk menikmati alam dan makanan... <br><a href="http://bustiket.com"><font color="#ccc">Read More &gt;&gt; </font></a></p>
                        </div>
                        </div>
                    </div>
                             <div class="hidden-sm hidden-lg clearfix"><br>&nbsp;</div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                             <div class="payment-math blog_promo">
                               <div class="icon_payment">
                                <span class="mybtn">NEW</span>
                                <img src="http://192.168.0.104/bustiket/images/new_home/secr7_img4.png" alt="">
                               </div>
                        <div class="disc_payment">
                            <span>Dec 05,2016</span>
                            <br>
                            <h5>Pesona Alam Medan Yang Eksotis </h5>
                            <p>Pulau medan yang terkenal akan alam dan makanan yangbegitu khas, sangat menarik perhatian wisatawan manca negara, untuk menikmati alam dan makanan... <br><a href="http://www.bitmatrixinfotech.com"><font color="#ccc">Read More &gt;&gt; </font></a></p>
                        </div>
                        </div>
                    </div>
                           <div class="hidden-sm hidden-lg clearfix"><br>&nbsp;</div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                             <div class="payment-math blog_promo">
                               <div class="icon_payment">
                                <span class="mybtn">test</span>
                                <img src="http://192.168.0.104/bustiket/images/new_home/logo-busticket.jpg" alt="">
                               </div>
                        <div class="disc_payment">
                            <span>Apr 22,2017</span>
                            <br>
                            <h5>Pesona Alam Medan Yang Eksotis </h5>
                            <p>This is testig blog.
								this blog is posted only for testing purpose.
								there is no similarity between this blog and real life .
								this blog should have... <br><a href="http://phpfiddle.org"><font color="#ccc">Read More &gt;&gt; </font></a></p>
                       </div>
                        </div>
                    </div>
        
            </div>
        </div>
		</div>
	</div>
	<div class="main_4nd_div  hidden-xs">   
		<div class="inner_container white">
			<div class="container ">
				<div class="tile_box">
					<h2>kata mereka</h2>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12 inner_section_payment vertical-center">
					<div class="col-md-1 col-sm-12">
						<img class="left_arrow previous" src="http://192.168.0.104/bustiket/images/new_home/secr8_img1.png">
					</div>
						<div class="col-md-10 col-sm-12 col-xs-12 review_box vertical-center">
											  
							<div class="review active">
								<div class="col-md-6 col-sm-12 col-xs-12 ">
									<img class="center_img client" src="http://192.168.0.104/bustiket/feedback/feedback_image/image23.png">
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">
									<p style="font-size: 16px;color:#666666;font-size:18px; text-align:left;font-family:Arial; min-height: 10vh;" class="vertical-center">
										 suresh<br>
										suresh@gmail.com<br>
										sdsdss                            </p>
								</div>
							</div>
						   <div class="review ">
								<div class="col-md-6 col-sm-12 col-xs-12 ">
									<img class="center_img client" src="http://192.168.0.104/bustiket/feedback/feedback_image/user-testimonial-1.jpg">
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">
									<p style="font-size: 16px;color:#666666;font-size:18px; text-align:left;font-family:Arial; min-height: 10vh;" class="vertical-center">
										 sds<br>
										raj@gmail.com<br>
										sdsd                            </p>
								</div>
							</div>
					</div>
                <div class="col-md-1 col-sm-12 col-xs-12">
                    <img class="right_arrow next" style="right:0;position:relative" src="http://192.168.0.104/bustiket/images/new_home/secr8_img2.png">
                </div>
            </div>
			<div class="col-md-4 col-sm-12 col-xs-12">
                              <div class="reviewvid">
									<video widh="320" height="240" controls="" id="feedback_video">
										<source src="http://192.168.0.104/bustiket/feedback/feedback_video/Short%20video%20clip-nature.mp4.mp4" type="video/mp4">                
										Your browser does not support the video tag.
									</video>
                            </div>    
                                <div class="reviewvid active">
									<video widh="320" height="240" controls="" id="feedback_video">
										<source src="http://192.168.0.104/bustiket/feedback/feedback_video/Animal%20Life%20Short%20Clip.mp4" type="video/mp4">                
										Your browser does not support the video tag.
									</video>
                           </div>    
           </div>
			</div>
			<div class="container gray" style="margin-top: 20px" ;="">
				<p style="text-align:left;font-weight:lighter;font-family:Arial;color:#333333;font-size:11px; padding:10px 0;margin:0">
					BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Kota Dalam Propinsi (AKDP) yang pertama dan terpercaya  di Indonesia. Dirilis sejak awal 2015, BUSTIKET.COM memberikan layanan rute perjalanan ke berbagai kota di pulau Jawa, Sumatera, Bali, Madura, dan Kalimantan dalam waktu dekat. Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.COM menyediakan berbagai informasi dalam hal jadwal keberangkatan semua rute bus perharinya, beragam pilihan bus, waktu tempuh, fasilitas, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini.  <br><br>
					Melalui BUSTIKET.COM, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer, tiket bus sudah bisa Anda dapatkan. Karena kepuasan pelanggan adalah prioritas utama kami, maka BUSTIKET.COM tidak hanya dapat diakses melalui website, tetapi juga tersedia dalam bentuk aplikasi Android untuk smartphone Anda. <br>Dengan aplikasi terbaru kami, pemesanan tiket bus dapat dilakukan kapanpun dan dimanapun. Gak capek, ga antri, hanya di BUSTIKET.COM!
				</p>
            </div>
		</div>
	</div>

</div>
@endsection
