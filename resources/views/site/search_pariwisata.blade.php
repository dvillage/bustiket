@extends('layouts.site_layout')

@section('content')
<?php 
//dd
$param = json_decode($body['param'],true);

$from = $param['from_city'];
$type = $param['bus_type'];
$date = date("l, d F Y",  strtotime($param['date']));
$ctype = $param['type'];

//if(isset($param['Error'])){ 
//    echo '<script>alert()<script>';
// }

?>
<script>
var data = JSON.parse('<?php echo $body['locations']; ?>');
//console.log(data);
$(document).ready(function(){
    var md=[];
    for(x in data){
        var d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
        if(data[x].type == 'city'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:'\xa0\xa0\xa0\xa0\xa0\xa0'+data[x].name};
        }
        md.push(d);
    }
    $( "#mfrom_city" ).autocomplete({
      source: md,
      select: function(event, ui){
          $('#m_from').val(ui.item.value+'-'+ui.item.id)
      }
    });
    $( "#mfrom_city_p" ).autocomplete({
      source: md,
      select: function(event, ui){
          $('#mfromP').val(ui.item.value+'-'+ui.item.id)
      }
    });
    var type = '{{$type}}';
//    if(type == 1){
    if(type == 'ac'){
        $('#ac').click();
//    }else if(type == 0){
    }else if(type == 'nonac'){
        $('#non_ac').click();
    }else{
        $('#all_type').click();
    }
    
    $('#search_form').on('submit', function(event){
        var flag = 0;
        if(flag == 0){
            event.preventDefault();
            var from = $('#search_form').find('#s_city_from').val();
            var date = $('#search_form').find('#sdate').val();
            var type = $('#search_form').find('#b_type_h').val();
            type = type == '-1'? 'all' : (type == '1'?'ac':'nonac');
//            console.log(from,to,date,nop);
            var f = $('#search_form').attr('action');
//            alert(f+'/'+from+'.'+type+'.'+date);
//            return false;
            $('#search_form').attr('action',f+'/'+from+'.'+type+'.'+date);
            flag = 1;
            $('#search_form')[0].submit();
        }
        
    });
    
});
</script>
<input type="hidden" id="param" value='<?php echo $body['param'];?>' />
<!--<input type="hidden" id="session_id" value='<?php // echo session()->getId();?>' />-->
<input type="hidden" id="session_id" value='<?php echo session('session_id');?>' />
<!--<input type="hidden" id="session_id" value='<?php // echo csrf_token();?>' />-->
<input type="hidden" id="date" value='<?php echo $param['date'];?>' />
    <main class="site-main">
        <section class="section section-hero-page have-content  hidden-995" style="background-image: url('{{URL::to('assets/images/heropages/2.jpg')}}') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h3 class="section-hero-page-title">Pesan Tiket Bus Kapan saja dan dari mana saja</h3>
                    <p class="section-hero-page-subtitle">Semuanya bisa Anda lakukan dengan mudah melalui BUSTIKET.com</p>
                </div>
            </div>
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <!--<button class="btn btn-radius btn-green btn-big showin-995" data-toggle="modal" data-target="#id-modal">showing sample modal-rwd</button>-->
                        </div>
                        
                        <div class="clearfix space space-30 hidden-995"></div>

                        <div class="route-content cari-ulang clearfix hidden-995">
                            <form action="{{URL::to('search-pariwisata')}}" method="post" id="search_form" class="" data-parsley-validate>
                            <div class="route-content-description clearfix">
                                <div class="cari-ulang-item">
                                    <h3 class="cari-ulang-item-title">Kota asal</h3>
                                    <div class="cari-ulang-item-box clearfix" style="position: relative">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-bus-right"></span>
                                        </span>

                                        <div class="cari-ulang-item-content">
                                            <span class="cari-ulang-item-content-title">Dari:</span>
                                            <input type="text" class="cari-ulang-item-value" id="s_from_city" value="{{$from}}" autocomplete="off" onkeyup="sfilterFrom();" onfocus="sfilterFrom();"  onblur="return ssetLoc(this);" onselect="$(this).attr('onblur','ssetLoc(this);');" required="">
                                            <input type="hidden" id='s_city_from' name='from_city' value="{{$param['from_city'].'.'.$ctype}}" />
                                            <input type="hidden" name='_token' value="{{csrf_token()}}" />
                                        </div>
                                        <ul class="autocomplete_filter" id='sfromfilter'>
                                        </ul>
                                    </div>
                                </div><!-- / .cari-ulang-item -->

                                <div class="cari-ulang-item">
                                    <h3 class="cari-ulang-item-title">Tipe Bus</h3>
                                    <div class="cari-ulang-item-box clearfix" style="position: relative;">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-bus-left"></span>
                                        </span>

                                        <div class="cari-tiket-field-content">
                                            <label for="" class="cari-tiket-field-title">Tipe Bus</label>
                                            <span class="cari-tiket-field-value" id="b_type" style="cursor: pointer">Select bus type</span>
                                            <input type="hidden" id="b_type_h" name="bus_type" value="{{$type}}" >
                                        </div>
                                        <ul class="autocomplete_filter" id='bus_type'>
                                            <li><a onclick="sselectType(this);" id="all_type" data-ac="-1">Select All</a></li>
                                            <li><a onclick="sselectType(this);" id="non_ac" data-ac="0">Non Ac <i id="bus_type_ac_check" class="" style="float: right; display: block;"></i></a></li>
                                            <li><a onclick="sselectType(this);" id="ac" data-ac="1" >Ac <i id="bus_type_ac_check" class="" style="margin-right: 15px; float: right; display: block;"></i></a></li>
                                        </ul>
                                    </div>
                                </div><!-- / .cari-ulang-item -->

                                <div class="cari-ulang-item berangkat">
                                    <h3 class="cari-ulang-item-title">Waktu Keberangkatan</h3>
                                    <div class="cari-ulang-item-box clearfix">
                                        <span class="cari-ulang-item-icon">
                                            <span class="sprite icon-calendar"></span>
                                        </span>

                                        <div class="cari-ulang-item-content ">
                                            <span class="cari-ulang-item-content-title">Waktu Keberangkatan:</span>
                                            <!--<input type="text" class="cari-ulang-item-value" value="Terminal Kampung Rambutan, Jakarta">-->
                                            <input type="text" id="sdate" name="date" class="cari-ulang-item-value datepicker-berangkat" value="{{$param['date']}}" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="route-content-refind">
                                <button class="btn btn-orange btn-radius">Cari Ulang</button>
                            </div>
                            </form>
                        </div><!-- / .route-content .cari-ulang -->

                        <div class="route-search-result">
                            
                            
                            <div class="rwd-route-content clearfix showin-995">
                                <div class="route-content-description">
                                    <h2 class="route-content-title pariwisata" id="ptitle1">No Bus Found</h2>
                                </div>

                                <div class="route-content-rwd-sorted">
                                    <ul class="clearfix">
                                        <li><a data-toggle="modal" data-target="#sort-modal">
                                            <span class="sprite-wrapper">
                                                <span class="sprite icon-sorting-down"></span>
                                            </span>
                                            Urutkan
                                            <span class="sprite-wrapper">
                                                <span class="sprite icon-arrow-small-updown"></span>
                                            </span>
                                        </a></li>

                                        <li><a data-toggle="modal" data-target="#filter-modal">
                                            <span class="sprite-wrapper">
                                                <span class="sprite icon-forma"></span>
                                            </span>
                                            Filter
                                            <span class="sprite-wrapper">
                                                <span class="sprite icon-arrow-small-updown"></span>
                                            </span>
                                        </a></li>
                                    </ul>
                                </div><!-- / .route-content-rwd-sorted -->

                                <div class="route-content-rwd-sorted result">
                                    
                                </div><!-- / .route-content-rwd-sorted -->
                            </div><!-- / .rwd-route-content -->
                            
                            
                            
                            
                            <div class="route-filter route-filter-pariwisata clearfix hidden-995">
                                <div class="route-filter-content">
                                    <div class="col-md-4">
                                        <span class="route-filter-content-header" style="margin-right: 140px;">Filter :</span>
                                    </div>
                                    <div class="col-md-3" style="margin-left: 15px;">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Operator <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='splist'></ul>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Tipe <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='typelist'>
                                                <li><a class="large" data-value="Mini Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Mini Bus</a></li>
                                                <li><a class="large" data-value="Big Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Big Bus</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="route-filter-content-item dropdown-toggle" data-toggle="dropdown" style='margin: 0px;'>
                                            Fitur <span class="arrow-down"></span>
                                        </span>
                                        <ul class="dropdown-menu" id='fitur'>
                                                <li><a class="large" data-value="AC" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;AC</a></li>
                                                <li><a class="large" data-value="Non AC" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Non AC</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="route-filter-refind">
                                    <button class="btn btn-line btn-line-gray btn-radius">Cari Tanggal lain</button>
                                </div>
                            </div><!-- / .route-filter -->
                            <div class="route-filter-search-result route-filter-search-result-pariwisata clearfix hidden-995">
                                <span class="route-filter-search-result-pariwisata-title" id="ptitle">No Bus Found</span>

                                <a class="route-filter-search-result-item" id='opsort' style="cursor: pointer">Operator <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item up berangkat" id="seatsort" style="cursor: pointer">Kursi <span class="arrow-updown"></span></a>

                                <a class="route-filter-search-result-item down" id="pricesort" style="cursor: pointer">Harga <span class="arrow-updown"></span></a>
                            </div><!-- / .route-filter-search-result .route-filter-search-result-pariwisata -->
                            <div class="route-search-result-items route-search-result-items-pariwisata row">
                                
                            </div><!-- / route-search-result-items div.route-search-result-items-pariwisata -->
                        </div><!-- / .route-search-result -->
                        
                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->
        
        <!-- MODAL BEGIN -->
<div class="modal modal-flat fade" id="filter-modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-body-title">Filter</h3>

        <div class="route-filter-search-result clearfix table-responsive table-harga" >
            <table class="table">
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Operator <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='splist1'></ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Tipe <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='typelist1'>
                            <li><a class="large" data-value="Mini Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Mini Bus</a></li>
                            <li><a class="large" data-value="Big Bus" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Big Bus</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="route-filter-content-item" style='margin: 0px;'>
                            Fitur <span class="arrow-down"></span>
                        </span>
                    </th>
                </tr>
                <tr>
                    <td style="text-align: left!important;">
                        <ul class="dropdown-menu1" id='fitur1'>
                            <li><a class="large" data-value="AC" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;AC</a></li>
                            <li><a class="large" data-value="Non AC" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;Non AC</a></li>
                        </ul>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <button type="button" class="btn btn-orange" data-dismiss="modal">Filter Search</button>
                    </td>
                </tr>
            </table>
        </div><!-- / .route-filter-search-result -->
        
      </div>
        
            
    </div><!-- / .modal-content -->
  </div>
</div>
<!-- MODAL END -->

<!-- MODAL BEGIN -->
<div class="modal modal-flat fade" id="sort-modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-body-title">Sort</h3>

        <div class="route-filter-search-result clearfix table-responsive table-harga">
            <table class="table">
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_opsort' style="cursor: pointer;margin-left: 0px;">Operator <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="route-filter-search-result-item berangkat" data-dismiss="modal" id='m_seatsort' style="cursor: pointer;margin-left: 0px;">Kursi <span  class="arrow-updown"></span></a>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <a class="route-filter-search-result-item" data-dismiss="modal" id='m_pricesort' style="cursor: pointer;margin-left: 0px;">Harga <span class="arrow-updown"></span></a>
                    </td>
                </tr>
                
            </table>
        </div><!-- / .route-filter-search-result -->
        
      </div>
        
            
    </div><!-- / .modal-content -->
  </div>
</div>
<!-- MODAL END --> 

    @endsection
