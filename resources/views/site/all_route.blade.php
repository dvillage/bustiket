@extends('layouts.site_layout')
@section('content')
<style>
    a{
        color:black;
    }
</style>
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/1.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

<!--        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Rute lanniya</h1>
                            </header>
                           <h2 class="item-company-title">KOTA-KOTA KEBERANGKATAN TERPOPULER</h2>
                           <table class="table   table-responsive table-striped">
                               <?php
                               
                                    $terminal=array();
                                    $cityList=$body[0];
                                    $cityBtn=array();
                                    $localCity=array();
                                    
                                    for($i=0;$i<count($cityList);$i++){
                                        
                                        for($j=0;$j<count($cityList);$j++){
                                            
                                            $localCity['to']=$cityList[$i];
                                            $localCity['from']=$cityList[$j];
       
                                             array_push($cityBtn,$localCity);
                                            
                                        }
                                    }
                                 
                               ?>

                               <?php
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                   
                               ?>
                               <?php
                               
                                    echo "<tr>";
                                    for($i=0;$i<count($cityBtn);$i++)
                                    {    
                                        $terminalTo=$cityBtn[$i]['to'];
                                        $terminalFrom=$cityBtn[$i]['from'];
                                        if($terminalTo==$terminalFrom){
                                            continue;
                                        }
                             
                                        $strurl0="search-bus/".$terminalTo.".C.".$terminalFrom.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                                ?>
                              
                                <td><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{URL::to($strurl0)}}">{{$terminalTo}}-{{$terminalFrom}}</a></td>
                                <?php
                                        if(($cnt%4)==0){
                                            echo "</tr><tr>";
                                        }
                                        $cnt++;
                                    }
                               ?>                                       
                           </table>  
                        </article>

                    </div>
                </div>
            </div>
        </div>-->
<div class="site-main-content of-x">
            <div class="container container-1170">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single article-rute clearfix">
                            <header class="article-header">
                                <h1>Rute Perjalanan Bustiket</h1>
                            </header>

                            <ul class="list-rute rute-lainnya clearfix">
                                
                                <?php
                               
                                    $terminal=array();
                                    $cityList=$body[0];
                                    $cityBtn=array();
                                    $localCity=array();
                                    
                                    for($i=0;$i<count($cityList);$i++){
                                        
                                        for($j=0;$j<count($cityList);$j++){
                                            
                                            $localCity['to']=$cityList[$i];
                                            $localCity['from']=$cityList[$j];
       
                                             array_push($cityBtn,$localCity);
                                            
                                        }
                                    }
                                 
                               ?>

                               <?php
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                   
                               ?>
                               <?php
                               
                                    
                                    for($i=0;$i<count($cityBtn);$i++)
                                    {    
                                        $terminalTo=$cityBtn[$i]['to'];
                                        $terminalFrom=$cityBtn[$i]['from'];
                                        if($terminalTo==$terminalFrom){
                                            continue;
                                        }
                             
                                        $strurl0="search-bus/".$terminalTo.".C.".$terminalFrom.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                                ?>
                              
                                
                                <li class="">
                                    <span class="sprite icon-placeholder-small"></span> <a href="{{URL::to($strurl0)}}">{{$terminalTo}} - {{$terminalFrom}}</a>
                                </li>
                                <?php
                                        
                                    }
                               ?>                  
                            </ul>

                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div>
        <script>
            
        </script>
@stop
