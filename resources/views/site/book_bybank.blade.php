@extends('layouts.site_layout')

@section('content')

<div class="greenStrip noBorder">
  <div class="greenStripInner">
    <div class="container">
      <div class="alertTicket"><i class="fa fa-exclamation-circle"></i>
        <p>Mohon Segera Selesaikan Pembayaran Melalui transfer ( ATM /Internet Banking, M-Banking ) Dalam Waktu 50 Menit kedepan.
          Setelah pembayaran kami terima, E-tiket akan kami kirimkan ke email anda dalam waktu 60 menit.</p>
      </div>
    </div>
  </div>
</div>

<div class="grayStrip">
  <div class="grayStripInner">
    <div class="container">
      <div class="infoTicket"><i class="fa fa-exclamation-triangle"></i>
        <p>Penting: Kami mohon pastikan hanya transfer ke salah satu rekening Bank BUSTIKET. BUSTIKET dan semua karyawan tidak pernah 
          meminta anda untuk transfer ke rekening lain seperti disebut diatas. Hati-hati terhadap penipuan.</p>
      </div>
    </div>
  </div>
</div>
<div class="divider40"></div>

<div class="container">
	<div class="row">
		<div class=" hidden-lg hidden-md">
		
      <div class="codeTransfer">
        <div class="row"><h3 class="col-sm-4">Kode Transfer</h3>
        <div class="code col-sm-4 text-center">ja553</div>
        <div class="clearfix"></div>
        </div>
        
      </div>
      <div class="paymentTime">
        <h4>Waktu Anda untuk melakukan pembayaran adalah</h4>
        <div class="leftMinute"><div id="timer-res">49:00</div></div>
        <div id="endTime_res" class="leftTime">Sampai jam 12:02</div>
      </div>
      <h2 class="detailHeader">Panduan pengiriman dengan Bank Transfer</h2>
      <div class="clearfix"></div>
    </div>
		<div class="col-md-8 col-lg-9 col-lg-push-3 col-md-push-4">
      <div class="ticketDetailInfo">
        <h2 class="detailHeader hidden-sm hidden-xs ">Panduan pengiriman dengan Bank Transfer</h2>

                <div class="detailPart1">
          <h3>Bank Central Asia (BCA) – KCP Blok A Cipete</h3>
          <dl class="dl-horizontal">
            <dt>Nama Rekening</dt>
            <dd>: CV Mitra Indo Visi Group </dd>
            <dt>Nomor Rekening</dt>
            <dd>: 218-008-8809</dd>
            <div class="clearfix"></div>
          </dl>
        </div>        <div class="detailPart2">
          <h4> Pastikan Anda mengikuti langkah-langkah berikut ini:</h4>
          <ol class="listNum">
            <li>Mohon untuk verifikasi Rincian Perjalanan &amp; Informasi pembelian tiket Anda.</li>
            <li>Pastikan Anda telah memasukkan detil rekening bank pengirim pembayaran dengan benar dan akurat.  Kami akan gunakan ini untuk identifikasi pembayaran yang masuk di rekening kami.</li>
            <li>Mohon transfer sesuai harga total yang tertera di "Detail Pemesanan"</li>
            <li>Batas pembayaran adalah 50 menit.</li>
            <li>Setelah pembayaran dilakukan silakan kunjungi halaman "Konfirmasi Pembayaran".</li>
            <li>Masukkan data mengenai detail rekening pengirim transfer:
              <ul class="listAlpha">
                <li>Kode Transfer</li>
                <li>Nama Bank</li>
                <li>Nama Pemilik Rekening</li>
                <li>Jumlah Yang Ditransfer</li>
              </ul>
            </li>
            <li>Klik tombol submit.</li>
            <li>Kami akan lakukan verifikasi manual dan akan mengirim e-tiket ke email Anda selambat-lambatnya 
              dalam 60 menit.</li>
          </ol>
        </div>
        <div class="detailPart3">
          <div class="well passengerInfo">
            <h3>Konfirmasi Transfer Anda</h3>
            <p>Silahkan melakukan pembayaran sejumlah Rp. 11.500 ke bank yang tercantum diatas.</p>
            <p>Setelah itu Anda dapat melakukan Konfirmasi dengan mengunjungi halaman konfirmasi pembayaran dimana
              linknya tersedia di bagian menu atas.</p>
          </div>
        </div>
      </div>
    </div>
	<div class="col-md-4 col-lg-3 col-lg-pull-9 col-md-pull-8">
      <div class="ticketSidebar">
        <div class="codeTransfer hidden-sm hidden-xs">
          <h3>Kode Transfer</h3>
          <div class="code">ja553</div>
          <div class="clearfix"></div>
        </div>
        <div class="paymentTime hidden-sm hidden-xs">
          <h4>Waktu Anda untuk melakukan pembayaran adalah</h4>
          <div class="leftMinute"><div id="timer">44:14</div></div>
          <div id="endTime" class="leftTime">Sampai jam 12:02</div>
        </div>
        <div class="paymentDetail">
          <h3>Rincian Harga</h3>
          <div class="ticketDTable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                              <tr>
                  <td>Harga Tiket : </td>
                  <td>Rp.11.500</td>
                </tr>
                <tr>
                  <td>Potongan Kode Voucher	: </td>
                  <td>Rp.0</td>
                </tr>
                <tr>
                  <td>Biaya Layanan	: </td>
                  <td>Rp. 1.150 </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Harga Total : </td>
                  <!--<td>Rp.11.500</td>-->
                  <td>Rp.12.650</td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
                <div class="ticketDetail">
          <h3>Rincian Perjalanan</h3>
          <p>Wed, 2 Aug 2017</p>
          <h5 class="txtGreen">Berangkat Dari :</h5>
          <p>JAKARTA TIMUR. DKI. TERM. PULOGEBANG, Jakarta (18:00)</p>
          <p></p>
          <h5 class="txtGreen">Tiba Di:</h5>
          <p>MENDOLO, Wonosobo (03:59)</p>
          <p class="txtYellow">Rute :
              Jakarta - Bekasi - Purwakarta - Cirebon - Brebes - Brebes. jawa tengah - Banyumas - Purbalingga - Banjarnegara - Banjarnegara. jawa tengah. terminal - Wonosobo - Bekasi - Purwakarta - Cirebon - Brebes - Brebes. jawa tengah - Banyumas - Purbalingga - Banjarnegara - Banjarnegara. jawa tengah. terminal - Wonosobo          </p>
        </div>
        <div class="wayBill">
          <h3>Daftar Penumpang</h3>
          <div class="passengerInfo"><p>sagar patel</p><h5 class="txtGreen">Nomor Kursi:</h5><p>7A</p></div>          
     </div>
      </div>
    </div>
	</div>
</div>
<iframe src="http://hadiahme.go2cloud.org/aff_l?offer_id=152&amp;adv_sub=07PK29UR11B6U4&amp;adv_sub2=ja553" scrolling="no" frameborder="0" width="1" height="1"></iframe>


@endsection

