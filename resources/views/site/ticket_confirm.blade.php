@extends('layouts.pay_layout')
@section('content')
    
        <div class="site-main-content content-payment">

            <article class="article-content-payment done">
                <div class="container">


                    <div class="row">
                        <div class="col-md-12">
                           <p class="text-uppercase">e-tiket telah terkirim melalui email anda</p>
                           <p class="text-uppercase">terimakasih telah menggunakan tikebus.com</p>
                        </div>
                    </div>
                </div>
            </article><!-- / .article-content-payment -->

        </div><!-- / .site-main-content .content-payment -->
        <section class="section">
            <div class="container">
                <div class="row site-section-widget">
                    <div class="col-md-12">
                        <div class="widget widget-download">
                            <h3 class="widget-title">DOWNLOAD BUSTIKET APP</h3>

                            <figure class="aligncenter">
                                <a href="#">
                                    <span class="sprite icon-google-play-big"></span>
                                </a>
                            </figure>
                            <div class="clearfix"></div>

                            <ul class="list-socmed">
                                <li><a href="#"><span class="sprite icon-facebook-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-twitter-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-instagram-circle"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                
               
                
                <div class="row">
                    
                    
                    <?php
                        if(isset($body['ticket']) && $body['ticket'] != ''){
                        ?>
<!--                     <div class="col-sm-12 col-md-3 col-lg-3" style="margin-bottom: 15px;position: fixed;bottom: 50px;left: 20px;">
                        <a href="{{URL::to('payment/download-as-pdf?ticket_id=1234')}}" class="btn btn-green">Download as PDF</a>
                        <a href="{{URL::to('assets/uploads/ticket/bustiket_'.$body['ticket_id'].'.pdf')}}" class="btn btn-green" download>Download as PDF</a>
                        <input type="button" class="btn btn-green" style="border: none" onclick="downloadAsPdf({{$body['ticket_id']}});" value="Download as PDF">
                    </div>-->
                        
                    <?php
//                    echo $body['ticket'];
                    }
                    ?>
                </div>
            </div>
        </section>
        
        
@endsection