@extends('layouts.site_layout')

@section('content')
<div class="round_way_search">
<div id="z_black_coer">
<div class="container">
<div id="panel-dark" class="bus-xx">
    <div class="col-xs-12 text-center" id="success_message" style="margin-top: 120px;margin-bottom: 200px;">
        <h1>Terima Kasih</h1><br><br>
        <img src="{{URL::to('assets/images/image.png')}}" /><br>
        <font color="gray">Permintaan order(inquiry) Anda telah kami terima, dan email<br>
        Konfirmasi order telah kami kirim ke alamat<br>
        email <b><?php echo $body['email'];?></b></font><br><br>
        <font color="gray">Tim sales kami akan segera menghubungl Anda dalam waktu maks 3 jam pada hari kerja dan<br>
        maks 1x24 jam pada hari libur</font>  
        <br>
        <a href="{{URL::to('')}}" class="btn btn-primary" value="" style="margin-top: 20px;">Go to Home Page</a>
    </div>
</div>
</div>
</div>
</div>
@endsection