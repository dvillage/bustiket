@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Payment Confirmation</h1>
                            </header>

                            <div class="item-company">
                                <h2 class="item-company-title">
                                  </h2>
                                <p>  Silahkan lengkapi form dibawah untuk konfirmasi pembayaran anda.
                                        Penting! Mohon konfirmasi ini hanya dilakukan setelah Anda melakukan pembayaran. 
                                        Isi data dengan benar untuk memudahkan kami verifikasi pembayaran Anda.</p>
                                
                            </div><!-- / .item-company -->

                            <div class="item-company">
                                <h2 class="item-company-title">Informasi Pembayaran</h2>

                                <div class="item-company-content">
                                    <div class="form-wrapper row">
                                        <form action="#" class="form-company" id="frmBank" method="post">
                                            <div class="col-md-6">
                                              

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Kode Transfer</span>
                                                    <input type="text" id="transferCode" name="transferCode" placeholder="" >
                                                </p>

                                              

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nama Pemilik Rekening Bank Tujuan</span>
                                                    <input type="text" id="bankNameAdd" name="bankNameAdd" placeholder="" >
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nomor Rekening</span>
                                                    <input type="text" id="accNumber" name="accNumber" placeholder="" >
                                                  
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nama Bank</span>
                                                    <input type="text" id="bankName" name="bankName"  placeholder="">
                                                </p>
                                            </div>

                                            <div class="col-md-6">
                                             

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Jumlah Transfer</span>
                                                    <input type="text" id="transferAmt" name="transferAmt" placeholder="" >
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Catatan (tuliskan nama para Penumpang Bus)</span>
                                                    <textarea cols="53" rows="5" type="text" id="passName" name="passName" placeholder="" ></textarea>
                                                </p>

                                                <p class="form-field style-2">
                                                    <div class="input-group">
                                                        <?php 
                                                                              $a=rand(10,99);
                                                                              $b=rand(10,99);
                                                        ?>
                                                        <div class="input-group-addon">
                                                          <?=$a?>+<?=$b?>
                                                        </div>
                                                        <input type="hidden" id="ques" name="ques" value="<?php echo $a + $b; ?>">
                                                        <input type="text" placeholder="Tulis Jawaban Anda" id="answer" name="answer" class="form-control">
                                                      </div>
                                                    
                                                </p>

                                                <p class="form-field form-submit text-right">
                                                    <input type="button" id="btnReg" name=""  class="btn btn-radius btn-green" value="Submit">
                                                    <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                                                </p>
                                                <div class="alert alert-success" id="onmail" style="display: none">
                                                    <strong></strong> 
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
             $(function(){
                 
                $("#btnReg").click(function(){
                    $("#btnReg").hide();
                    $("#btnS").show();
                    
                    var transferCode=$("#transferCode").val();
                    var bankNameAdd=$("#bankNameAdd").val();
                    var accNumber=$("#accNumber").val();
                    var bankName=$("#bankName").val();
                    var transferAmt=$("#transferAmt").val();
                    var passName=$("#passName").val();
                    var answer=$("#answer").val();
                    var ques=$("#ques").val();
                     
                     
                    
                  var flag=1;   
                  if(transferCode==''){
                       $("#transferCode").focus();
                       $("#transferCode").css('border-color','red');
                       flag=0;
                   }else{
                          $("#transferCode").css('border-color','black');
                   }
                   
                   if(bankNameAdd==''){
                       $("#bankNameAdd").focus();
                       $("#bankNameAdd").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#bankNameAdd").css('border-color','black');
                   }
                   
                   if(accNumber==''){
                       $("#accNumber").focus();
                       $("#accNumber").css('border-color','red');
                       flag=0;
                   }else{
                       $("#accNumber").css('border-color','black');
                   }
                   
                   if(bankName==''){
                       $("#bankName").focus();
                       $("#bankName").css('border-color','red');
                       flag=0;
                   }else{
                       $("#bankName").css('border-color','black');
                   }
                   
                   if(transferAmt==''){
                       $("#transferAmt").focus();
                       $("#transferAmt").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#transferAmt").css('border-color','black');
                   }
                   
                   if(passName==''){
                       $("#passName").focus();
                       $("#passName").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#passName").css('border-color','black');
                   }
                   if(answer==''){
                       $("#answer").focus();
                       $("#answer").css('border-color','red');
                       flag=0;
                   }
                   else{
                       $("#answer").css('border-color','black');
                   }
                                         
     if(answer==ques){
                   if(flag==1){
                       var token = $("[name=csrf-token]").attr("content");
                       var bankData=$("#frmBank").serializeArray(); 
                        $.ajax({
                           url:'{{URL::to("payment-confirmation")}}',
                           type:'post',
                           dataType:'json',
                           data:{_token:token,bankData:bankData},
                           success:function(data){
                               var jdata=JSON.parse(JSON.stringify(data));
                               if(jdata.flag==1){
                                   $("#onmail").show();
                                   $("#onmail").text(jdata.msg);
                                   $("#frmBank")[0].reset();
                                   $("#btnReg").show();
                                   $("#btnS").hide();
                               }
                               else if(jdata.flag==0){
                                   
                                   $("#onmail").show();
                                   $("#onmail").text(jdata.msg);
                                   $("#btnReg").show();
                                   $("#btnS").hide();
                               }
                           }
                       });
                   }
                   else{
                       $(this).show();
                       $("#btnS").hide();
                   }
               }else{
                   alert('Verify you are human being');
                   $(this).show();
                       $("#btnS").hide();
               }
                });                
    });    
        </script>
@stop
