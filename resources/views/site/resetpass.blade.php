@extends('layouts.site_layout')
@section('content')
<script>
</script>
    <main class="site-main site-main-login">
        <div class="site-main-login-mask"></div>
        <section class="section section-login">
            <div class="container">

                <div class="section-login-wrapper">
                    <header class="section-header">
                        <h1 class="section-header-title">Reset Password</h1>        
                    </header>
                    
                    <div class="section-content">
                        <div class="form-wrapper">
                            <?php if(!isset($body['forgorttoken'])){ ?>
                                <div class="alert alert-danger" style="text-align: center;">
                                    <strong>This Link is Expired.</strong>
                                </div>
                            <?php }else{
//                            dd($errors);
                            if(isset($errors) && $errors->first()!=''){ ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $errors->first(); ?></strong>
                                    </div>
                            <?php }
                            $session = \Session::get("msg");
//                            dd($session);
                            if($session != "" && \General::is_json($session)){ 
                                $session = json_decode($session,true);
                                \Session::forget("msg");
                                ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $session['msg']; ?></strong>
                                    </div>
                            <?php }
                            
                            ?>
                            <form action="{{URL::to('user/reset-pass')}}" method="POST" class="form-login">
                                <p class="form-field">
                                    <span class="form-field-label">New Password</span>
                                    <input type="password" name="new_pass" id="new_pass" autocomplete="off">
                                </p>

                                <p class="form-field">
                                    <span class="form-field-label">Confirm New Password</span>
                                    <input type="password" name="cnew_pass" id="cnew_pass" autocomplete="off">
                                    <input type="hidden" name="forgottoken" id="forgottoken" value='{{$body['forgorttoken']}}'>
                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                </p>


                                <p class="form-field form-submit">
                                    <button type="submit" class="btn btn-green btn-radius btn-fullwidth" onclick="return matchCpass();">Reset Password</button>
                                </p>
                                
                                <span class="clearfix space space-30"></span> 

                                

                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </section>

@stop