@extends('layouts.site_layout')

@section('content')

<?php // dd($body['locations']); ?>

<script>
var data = JSON.parse('<?php echo $body['locations']; ?>');
//console.log(data);
$(document).ready(function(){
    var flag = 0;
    var md=[];
    var cities = [];
    for(x in data){
//        var d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
        var d;
        if(data[x].type == 'province'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
            md.push(d);
//            cities.push(d);
        }
        if(data[x].type == 'district'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
            md.push(d);
//            cities.push(d);
        }
        if(data[x].type == 'city'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:data[x].name};
            md.push(d);
            cities.push(d);
        }
        else if(data[x].type == 'terminal'){
            d = { id:data[x].id+'-'+data[x].type, value:data[x].name, label:'\xa0\xa0\xa0\xa0\xa0\xa0'+data[x].name};
            md.push(d);
        }
        
    }
    $( "#mfromcity" ).autocomplete({
      source: md,
      select: function(event, ui){
            var f = ui.item.id.split('-');
            if(f[1] == 'province'){
                $('#mfrom').val(ui.item.value+'.P');
            }
            else if(f[1] == 'district'){
                $('#mfrom').val(ui.item.value+'.D');
            }
            else if(f[1] == 'city'){
                $('#mfrom').val(ui.item.value+'.C');
            }
            else if(f[1] == 'terminal'){
                $('#mfrom').val(ui.item.value+'.T');
            }
//          $('#mfrom').val(ui.item.value+'.'+f[1]);
//          console.log($('#mfrom').val());
//          $('#mfrom').val(ui.item.value+'-'+ui.item.id)
      }
    });
    $( "#mfrom_city" ).autocomplete({
      source: cities,
      select: function(event, ui){
            var f = ui.item.id.split('-');
            if(f[1] == 'province'){
                $('#mfromP').val(ui.item.value+'.P');
            }
            else if(f[1] == 'district'){
                $('#mfromP').val(ui.item.value+'.D');
            }
            else if(f[1] == 'city'){
                $('#mfromP').val(ui.item.value+'.C');
            }
            else if(f[1] == 'terminal'){
                $('#mfromP').val(ui.item.value+'.T');
            }
//          $('#mfromP').val(ui.item.value+'-'+ui.item.id);
      }
    });
    $( "#mtocity" ).autocomplete({
      source: md,
      select: function(event, ui){
            var f = ui.item.id.split('-');
            if(f[1] == 'province'){
                $('#mto').val(ui.item.value+'.C');
            }
            else if(f[1] == 'district'){
                $('#mto').val(ui.item.value+'.C');
            }
            else if(f[1] == 'city'){
                $('#mto').val(ui.item.value+'.C');
            }
            else if(f[1] == 'terminal'){
                $('#mto').val(ui.item.value+'.T');
            }
//          $('#mto').val(ui.item.value+'.'+f[1]);
//          $('#mto').val(ui.item.value+'-'+ui.item.id);
      }
    });
    
    $('#cont-from').on('click',function(){
        $('#fromcity').focus();
    });
    $('#cont-to').on('click',function(){
        $('#tocity').focus();
    });
    
    $('#ticket-bus').on('submit', function(event){
        
        if(flag == 0){
            event.preventDefault();
            var from = $('#ticket-bus').find('#from').val();
            var to = $('#ticket-bus').find('#to').val();
            var date = $('#ticket-bus').find('#datepick').val();
            var nop = $('#ticket-bus').find('#nop').val();
//            console.log(from,to,date,nop);
            var f = $('#ticket-bus').attr('action');
            $('#ticket-bus').attr('action',f+'/'+from+'.'+to+'.'+date+'.'+nop);
            flag = 1;
            var url = f+'/'+from+'.'+to+'.'+date+'.'+nop;
            window.location = url;
//            $('#ticket-bus').submit();
        }
        
    });
    
    $('#mticket-bus').on('submit', function(event){
        
        if(flag == 0){
            event.preventDefault();
            var from = $('#mticket-bus').find('#mfrom').val();
            var to = $('#mticket-bus').find('#mto').val();
            var date = $('#mticket-bus').find('#date').val();
            var nop = $('#mticket-bus').find('#nop').val();
//            console.log(from,to,date,nop);
            var f = $('#mticket-bus').attr('action');
            $('#mticket-bus').attr('action',f+'/'+from+'.'+to+'.'+date+'.'+nop);
            flag = 1;
            var url = f+'/'+from+'.'+to+'.'+date+'.'+nop;
            window.location = url;
//            $('#mticket-bus').submit();
        }
        
    });
    
    $('#pari_wisata').on('submit', function(event){
        
        if(flag == 0){
            event.preventDefault();
            var from = $('#pari_wisata').find('#fromC').val();
            var type = $('#pari_wisata').find('#b_type_h').val();
            var date = $('#pari_wisata').find('#datepickp').val();
            type = type == '-1'? 'all' : (type == '1'?'ac':'nonac');
//            console.log(from,to,date,nop);
            var f = $('#pari_wisata').attr('action');
            $('#pari_wisata').attr('action',f+'/'+from+'.'+type+'.'+date);
            flag = 1;
            var url = f+'/'+from+'.'+type+'.'+date;
            window.location = url;
//            $('#pari_wisata').submit();
        }
        
    });
    
    $('#mpariwisata').on('submit', function(event){
        
        if(flag == 0){
            event.preventDefault();
            var from = $('#mpariwisata').find('#mfromP').val();
            var type = $('#mpariwisata').find('#bus_type').val();
            var date = $('#mpariwisata').find('#pdate').val();
            type = type == '-1'? 'all' : (type == '1'?'ac':'nonac');
            var f = $('#mpariwisata').attr('action');
            $('#mpariwisata').attr('action',f+'/'+from+'.'+type+'.'+date);
            flag = 1;
            var url = f+'/'+from+'.'+type+'.'+date;
            window.location = url;
//            $('#mpariwisata').submit();
        }
        
    });
});

</script>
<style>
    
    
</style>
<main class="site-main">
    <section class="section section-cari-tiket-rwd showin-995">
        <div class="tab-cari-tiket">
            <ul class="list-tab clearfix" role="tablist">
                <li class="active">
                    <a href="#rwd-bus-tiket" role="tab" data-toggle="tab" aria-controls="rwd-bus-tiket">
                        <span class="sprite icon-bus"></span>
                            <span>Tiket Bus</span>
                    </a>
                </li>
                <li>
                    <a href="#rwd-bus-pariwisata" role="tab" data-toggle="tab" aria-controls="rwd-bus-pariwisata">
                            <span class="sprite icon-bus-pariwisata"></span>
                        <span class="">Pariwisata</span>
                    </a>
                </li>
            </ul><!-- / .list-tab -->

            <div class="tab-content">
                <div class="tab-pane fade in active" role="tabpanel" id="rwd-bus-tiket">
                    <form action="{{URL::to('search-bus')}}" id="mticket-bus" method="GET" class="form-cari-tiket-rwd form-field-with-label-big form-field-with-field-radius" data-parsley-validate>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <ul class="form-cari-tiket-content bus">
                            <li class="form-field ">
                                <span class="form-field-label">
                                    <span class="sprite-wrapper"><span class="sprite icon-city"></span></span>Kota Asal</span>
                                <input type="text" class='locations' id='mfromcity' title='Please Enter Valid From City..' placeholder="" onblur="msetLoc(this);">
                                <input type="hidden" id='mfrom' />
                            </li>

                            <li class="form-field">
                                <span class="form-field-label">
                                    <span class="sprite-wrapper"><span class="sprite icon-city"></span></span>Kota Tujuan</span>
                                <input type="text" class='locations' id='mtocity' title='Please Enter Valid Destination City..' placeholder="" onblur="msetLoc(this);">
                                <input type="hidden" id='mto' />
                            </li>

                            <li class="form-field">
                                <span class="form-field-label">
                                    <span class="sprite-wrapper"><span class="sprite icon-calendar-smallest"></span></span>Waktu Keberangkatan</span>
                                <input type="text" id="date"  class="datepicker-berangkat">
                            </li>

                            <li class="form-field">
                                <span class="form-field-label">
                                    <span class="sprite-wrapper"><span class="sprite icon-person-smallest"></span></span>Jumlah Penumpang</span>
                                <input type="number" id="nop" value="1">
                            </li>

                            <li class="form-field form-submit">
                                <div class="alert alert-danger maltmsg" style="display: none;margin-top: 40px;">
                                    <strong>Required !</strong> <span id="mmsg"></span>
                                </div>
                                <input type="submit" class="btn btn-yellow btn-fullwidth" onclick="return validate('m');" value="Pesan Tiket">
                            </li>
                        </ul>
                    </form>
                </div><!-- / #rwd-bus-tiket -->

                <div class="tab-pane fade" role="tabpanel" id="rwd-bus-pariwisata">
                    <form action="{{URL::to('search-pariwisata')}}" method="GET" id="mpariwisata" class="form-cari-tiket-rwd form-field-with-label-big form-field-with-field-radius">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <ul class="form-cari-tiket-content pariwisata">
                            <li class="form-field">
                                <span class="form-field-label"><span class="sprite-wrapper"><span class="sprite icon-city"></span></span>Kota Asal</span>
                                <input type="text" id="mfrom_city"  placeholder="" onblur="filterFromPariwisata(this);msetLocPari(this);">
                                <input type="hidden" id='mfromP' name='from_city'/>
                                <input type="hidden" id="session_id" value='<?php echo session()->getId();?>' />
                            </li>

                            <li class="form-field">
                                <span class="form-field-label"><span class="sprite-wrapper"><span class="sprite icon-tipe-bus"></span></span>
                                    Tipe Bus</span>
                                <select name="bus_type" id="bus_type" placeholder="">
                                    <option value="-1">Select All</option>
                                    <option value="0">Non AC</option>
                                    <option value="1">AC</option>
                                </select>
                            </li>

                            <li class="form-field">
                                <span class="form-field-label"><span class="sprite-wrapper"><span class="sprite icon-calendar-smallest"></span></span>Waktu Keberangkatan</span>
                                <input type="text" name="date" id="pdate" class="datepicker-berangkat">
                            </li>

                            <li class="form-field form-submit">
                                <input type="submit" class="btn btn-yellow btn-fullwidth" value="Pesan Tiket">
                            </li>
                        </ul>
                    </form>
                </div><!-- / #rwd-bus-pariwisata -->
            </div><!-- / .tab-content -->
        </div><!-- / .tab-cari-tiket -->
    </section><!-- / .section-cari-tiket-rwd -->

    <section class="section section-cta-download-rwd showin-995">
        <!-- <a href="#" class="fullwidth"> -->
            <!--<img src="assets/images/banner-mobile.jpg" alt="">-->
            <!-- <img src="{{URL::to('assets/images/banner-mobile.jpg')}}"   alt=""> -->
            <!-- <span style="background-image: url('assets/images/banner-images.jpg');"></span> -->
        <!-- </a> -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

       <?php $flg = 0;
       foreach($body['mobile_footer_banners'] as $banner){
            $url = $banner['redirect_to'];
            if(strpos($url,'http://') === false && strpos($url,'https://') === false){
                $url = 'http://'.$url;
            }else{
                
            }
            ?>
            <?php if ($flg == 0) { ?>
            <div class="item active">
            <a href="{{$url}}" target="_blank" class="fullwidth"><img src="{{URL::to('assets/images/banner').'/'.$banner['file']}}" alt="" ></a>
            </div>
            <?php }else{ ?>
                <div class="item">
                <a href="{{$url}}" target="_blank" class="fullwidth"><img src="{{URL::to('assets/images/banner').'/'.$banner['file']}}" alt="" ></a>
                </div>
            <?php }$flg++;} ?>
         </div>
        </div>
    </section>

    <section class="section section-hero-slider hidden-995">
        <div class="hero-carousel flexslider">
            <ul class="slides">
                <?php foreach($body['banners'] as $banner){
                    $url = $banner['redirect_to'];
                    if(strpos($url,'http://') === false && strpos($url,'https://') === false){
                        $url = 'http://'.$url;
                    }else{
                        
                    }
                    ?>
                <li><a href="{{$url}}" target="_blank"><img src="{{URL::to('assets/images/banner').'/'.$banner['file']}}" alt=""></a></li>
                <?php } ?>
                
<!--                <li><img src="assets/images/heroslider/2.jpg" alt=""></li>
                <li><img src="assets/images/heroslider/3.jpg" alt=""></li>-->
            </ul>
<!--            <ul class="slides">
                <li>&nbsp;</li>
                 <li><img src="assets/images/heropages/idulfitri.jpg" alt="" height="582"></li>
                <li><img src="assets/images/heropages/idulfitri.jpg" alt="" height="582"></li>
                <li><img src="assets/images/heropages/idulfitri.jpg" alt="" height="582"></li> 
            </ul>-->
        </div>
        </section><!-- / .section-hero-slider -->

    <section class="section section-cari-tiket section-offset hidden-995">
        <div class="container">
            <div class="row section-content tab-cari-tiket">
            <div class="col-md-12">
                <ul class="list-tab clearfix" role="tablist">
                    <li class="active">
                        <a href="#tiket_bus" role="tab" data-toggle="tab" aria-controls="tiket_bus">
                            <span class="sprite icon-bus-front-two"></span>
                            <span>Tiket Bus</span>
                        </a>
                    </li>
                    <li>
                        <a href="#pariwisata" role="tab" data-toggle="tab" aria-controls="pariwisata">
                            <span class="sprite icon-bus-pariwisata"></span>
                            <span>Pariwisata</span>
                        </a>
                    </li>
                </ul><!-- / .list-tab -->
                <div class="clearfix"></div>

                <div class="tab-content">
                    <div class="tab-pane fade in active" role="tabpanel" id="tiket_bus">
                        <header>
                            <h2>Cari tiket bus murah dan cepat disini!</h2>
                        </header>

                        <div class="content">
                            <form action="{{URL::to('search-bus')}}" method="GET" id="ticket-bus" class="form-cari-tiket clearfix" data-parsley-validate>
                                <!--<input type="hidden" name="_token" value="{{csrf_token()}}">-->
                                <ul class="list-step-cari-tiket">

                                    <li class="item-list-step-cari-tiket clearfix">
                                        <h2 class="item-list-step-cari-tiket-title">Destinasi</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix" id="cont-from">
                                                <span class="cari-tiket-field-icon">
                                                    <span class="sprite icon-bus-right"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label class="cari-tiket-field-title">Kota Asal</label>
                                                    <!--<span class="cari-tiket-field-value">Terminal Kampung Rambutan, Jakarta</span>-->
                                                    <input type="text" class='cari-tiket-field-value' id='fromcity' autocomplete="off" onkeyup="filterFrom();"  onblur="setLoc(this);" onselect="$(this).attr('onblur','setLoc(this);');" placeholder="" />
                                                    <!--<input type="hidden" id='from' name='from'/>-->
                                                    <input type="hidden" id='from' />
                                                </div>

                                                <ul class="cari-tiket-field-child" id='fromfilter' style="z-index: 1001;">
                                                </ul>
                                            </div><!-- / .cari-tiket-field -->

                                            <div class="cari-tiket-field cari-tiket-sep text-center clearfix">
                                                <a  id="bus-swap" onclick="swap();"></a>
                                            </div>

                                            <div class="cari-tiket-field clearfix" id="cont-to">
                                                <span class="cari-tiket-field-icon">
                                                    <span class="sprite icon-bus-left"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label for="" class="cari-tiket-field-title">Kota Tujuan</label>
<!--                                                    <span class="cari-tiket-field-value">Kota Surabaya, Surabaya</span>-->
                                                    <input type="text" class='cari-tiket-field-value' id='tocity' autocomplete="off" onkeyup="filterTo();" onfocus="filterTo();" onblur="setLoc(this);" onselect="$(this).attr('onblur','setLoc(this);');" placeholder="" />
                                                    <!--<input type="hidden" id='to' name='to'/>-->
                                                    <input type="hidden" id='to' />
                                                </div>
                                                
                                                <ul class="cari-tiket-field-child" id='tofilter' style="z-index: 1000;">
                                                </ul>
                                            </div>
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>

                                    <li class="item-list-step-cari-tiket clearfix">
                                        <h2 class="item-list-step-cari-tiket-title">Waktu Keberangkatan</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix">
                                                <span class="cari-tiket-field-icon small">
                                                    <span class="sprite icon-calendar mt-5"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label class="cari-tiket-field-title">Tanggal Berangkat</label>
                                                    <!--<input type="text" class="cari-tiket-field-value cari-tiket-date datepicker_home" id='datepick' name="date" value="">-->
                                                    <input type="text" class="cari-tiket-field-value cari-tiket-date datepicker_home" id='datepick' value="">
                                                    <!--<input type="hidden" id='date' name='date' />-->
                                                </div>
                                            </div>
                                            <!--<div class="cari-tiket-field clearfix">-->
                                                
                                                <div class="alert alert-danger altmsg" style="display: none;margin-top: 40px;">
                                                    <strong>Required !</strong> <span id="msg"></span>
                                                </div>
                                            <!--</div>-->

<!--                                            <div class="cari-tiket-field cari-tiket-sep cari-tiket-check clearfix">
                                                <input type="checkbox" name="" id="sekali-jalan">
                                                <label for="sekali-jalan">Sekali Jalan</label>
                                            </div>-->
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>

                                    <li class="item-list-step-cari-tiket clearfix">
                                        <h2 class="item-list-step-cari-tiket-title">Jumlah Penumpang</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix">
                                                <span class="cari-tiket-field-icon small">
                                                    <span class="sprite icon-person-opa mt-5"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label class="cari-tiket-field-title">Orang</label>
                                                    <!--<span class="cari-tiket-field-value">01</span>-->
                                                    <!--<input type='text' name='nop' id='nop' class="cari-tiket-field-value" value="1"/>-->
                                                    <input type='text' id='nop' class="cari-tiket-field-value" value="1"/>
                                                </div>
                                            </div> 

                                            <div class="cari-tiket-field cari-tiket-sep text-center clearfix">
                                                        <span class="space space-15"></span>
                                                        <span class="space space-10"></span>
                                            </div>

                                            <div class="cari-tiket-field cari-tiket-field-submit clearfix">
                                                <input type="submit" class="btn btn-orange" onclick="return validate('w');" value="Cari Bus">
                                                <!--<button type="submit" class="btn btn-orange" onclick="return validate('w');">Cari Bus</button>-->
                                                <!--<a href="#" class="btn btn-orange">Cari Bus</a>-->
                                            </div>
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>
                                </ul>
                            </form><!-- / .form-cari-tiket -->
                        </div>
                    </div><!-- / .tab-pane -->

                    <div class="tab-pane fade" role="tabpanel" id="pariwisata">
                        <header>
                            <h2>Cari tiket pariwisata murah dan cepat disini!</h2>
                        </header>

                        <div class="content">
                            <form  action="{{URL::to('search-pariwisata')}}" id="pari_wisata" method="GET"  class="form-cari-tiket clearfix">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <ul class="list-step-cari-tiket">

                                    <li class="item-list-step-cari-tiket clearfix">
                                        <h2 class="item-list-step-cari-tiket-title">Destinasi</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix">
                                                <span class="cari-tiket-field-icon">
                                                    <span class="sprite icon-bus-right"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label class="cari-tiket-field-title">Kota Asal</label>
                                                    <input type="text" class='cari-tiket-field-value' id='from_city' autocomplete="off" onkeyup="filterFromPariwisata();"  onblur="setLocPari(this);" onselect="$(this).attr('onblur','setLocPari(this);');" placeholder="" />
                                                    <input type="hidden" id='fromC' name='from_city'/>
                                                </div>
                                                <ul class="cari-tiket-field-child" id='from_filter_pariwisata'>
                                                </ul>
<!--                                                <ul class="cari-tiket-field-child">
                                                    <li>
                                                        <a href="#">Jakarta (Semua Lokasi)</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jakarta Selatan</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jakarta Timur</a>
                                                        <ul>
                                                            <li><a href="#">Terminal Kampung Rambutan</a></li>
                                                            <li><a href="#">Terminal Pasar Rebo</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="">Jakarta Barat</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jakarta Pusat</a>
                                                    </li>
                                                </ul>-->
                                            </div><!-- / .cari-tiket-field -->
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>

                                            <li class="item-list-step-cari-tiket clearfix">
                                                <h2 class="item-list-step-cari-tiket-title">Tipe Bus</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix">
                                                <span class="cari-tiket-field-icon">
                                                    <span class="sprite icon-bus-right"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label for="" class="cari-tiket-field-title">Pilih Tipe Bus</label>
                                                    <span class="cari-tiket-field-value" id="b_type">Select bus type</span>
                                                    <input type="hidden" id="b_type_h" name="bus_type" >
                                                </div>
                                                <ul class="cari-tiket-field-child" id='bus_type'>
                                                    <li><a onclick="selectType(this);" id="all_type" data-ac="-1">Select All</a></li>
                                                    <li><a onclick="selectType(this);" id="non_ac" data-ac="0">Non Ac <i id="bus_type_ac_check" class="" style="float: right; display: block;"></i></a></li>
                                                    <li><a onclick="selectType(this);" id="ac" data-ac="1" >Ac <i id="bus_type_ac_check" class="" style="margin-right: 15px; float: right; display: block;"></i></a></li>
                                                </ul>
                                            </div>
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>

                                    <li class="item-list-step-cari-tiket clearfix">
                                        <h2 class="item-list-step-cari-tiket-title">Waktu Keberangkatan</h2>

                                        <div class="item-list-step-cari-tiket-content">
                                            <div class="cari-tiket-field clearfix">
                                                        <span class="cari-tiket-field-icon">
                                                    <span class="sprite icon-calendar mt-5"></span>
                                                </span>

                                                <div class="cari-tiket-field-content">
                                                    <label class="cari-tiket-field-title">Tanggal Berangkat</label>
                                                    <input type="text" class="cari-tiket-field-value cari-tiket-date datepicker_home" id='datepickp' name="date" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="alert alert-danger altmsg" style="display: none;margin-top: 40px;">
                                                <strong>Required !</strong> <span id="msg1"></span>
                                            </div>

                                            <div class="cari-tiket-field cari-tiket-sep text-center clearfix">
                                                <span class="space space-15"></span>
                                                <span class="space space-10"></span>
                                            </div>
                                            <div class="cari-tiket-field cari-tiket-field-submit clearfix">
                                                <button type="submit" class="btn btn-orange" onclick="return validate('p');">Cari Bus</button>
                                            </div>
                                        </div><!-- / .item-list-step-cari-tiket-content -->
                                    </li>
                                </ul>
                            </form><!-- / .form-cari-tiket -->
                        </div>
                    </div><!-- / .tab-pane -->
                </div><!-- / .tab-content -->
            </div>
        </div>
    </section>

    <section class="section section-tujuan-favorite hidden-995">
        <div class="container">

            <header class="section-header">
                <h2>Tujuan Favorit</h2>
                <p>Silahkan Pilih Tujuan Anda</p>
                    <span class="sprite icon-placeholder"></span>
            </header>

            <div class="row section-content">
                    <div class="col-md-12">               
                <div class="favorite-carousel owl-carousel">
                    <?php foreach($body['fav_routes'] as $routes){ ?>
                    
                        <div class="item-favorite">
                            <figure class="item-favorite-image">
                                <img src="{{URL::to('assets/uploads/fav_routes')}}/{{$routes['image']}}" alt="">
                            </figure>

                            <div class="item-favorite-content">
                                <h3>{{$routes['from_city']['name']}} - {{$routes['to_city']['name']}}</h3>
                                <span class="price">Rp {{\General::number_format($routes['price'] , 3)}}</span>
                                <a href="{{URL::to('search-bus')}}/{{$routes['from_city']['name']}}.C.{{$routes['to_city']['name']}}.C.{{date('Y-m-d',strtotime('+3 day'))}}.1" class="btn btn-orange btn-radius">PESAN</a>
                            </div>
                        </div>
                    
                    <?php } ?>
<!--                    <div class="item-favorite">
                        <figure class="item-favorite-image">
                            <img src="assets/images/tujuan/1.jpg" alt="">
                        </figure>

                        <div class="item-favorite-content">
                            <h3>Jakarta - Banyuwangi</h3>
                            <span class="price">Rp 380.000</span>
                            <a href="#" class="btn btn-orange btn-radius">PESAN</a>
                        </div>
                    </div>-->

<!--                    <div class="item-favorite">
                        <figure class="item-favorite-image">
                            <img src="assets/images/tujuan/2.jpg" alt="">
                        </figure>

                        <div class="item-favorite-content">
                            <h3>Jakarta - Banyuwangi</h3>
                            <span class="price">Rp 380.000</span>
                            <a href="#" class="btn btn-orange btn-radius">PESAN</a>
                        </div>
                    </div>

                    <div class="item-favorite">
                        <figure class="item-favorite-image">
                            <img src="assets/images/tujuan/3.jpg" alt="">
                        </figure>

                        <div class="item-favorite-content">
                            <h3>Jakarta - Banyuwangi</h3>
                            <span class="price">Rp 380.000</span>
                            <a href="#" class="btn btn-orange btn-radius">PESAN</a>
                        </div>
                    </div>

                    <div class="item-favorite">
                        <figure class="item-favorite-image">
                            <img src="assets/images/tujuan/4.jpg" alt="">
                        </figure>

                        <div class="item-favorite-content">
                            <h3>Jakarta - Banyuwangi</h3>
                            <span class="price">Rp 380.000</span>
                            <a href="#" class="btn btn-orange btn-radius">PESAN</a>
                        </div>
                    </div>-->

                

                        </div><!-- / .favorite-carousel -->
            </div>
        </div>
            </div>
    </section>

    <section class="section section-why-bustiket bt-gray hidden-995">
        <div class="container">

            <header class="section-header">
                <h2>Mengapa BUSTIKET?</h2>
                <p>Apa Saja Keuntungan Menggunakan BUSTIKET?</p>
            </header>

            <div class="row section-content">

                <div class="col-md-3 item item-why">
                    <div class="subitem-item-why">
                        <span class="item-why-wrapper">
                            <span class="sprite icon-smart-search"></span>
                        </span>

                        <h3>Smart Search</h3>

                        <div class="item item-why-caption">
                            <div class="item item-why-caption-content">
                                <p>Mencari dan membandingkan berbagai jenis bus Antar Kota Antar Provinsi di seluruh Indonesia mulai dari segi harga, rute, lama perjalanan, fasilitas dan lainnya dengan teknologi algoritma search terbaru.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- / .item item-why -->

                <div class="col-md-3 item item-why">
                    <div class="subitem-item-why">
                        <span class="item-why-wrapper">
                                <span class="sprite icon-price"></span>
                        </span>

                        <h3>Harga Terjamin</h3>

                        <div class="item item-why-caption">
                            <div class="item item-why-caption-content">
                                <p>Harga tiket bus yang ditampilkan di BUSTIKET.COM adalah harga resmi dan terbaik, sudah termasuk biaya-biaya seperti pajak, handling fee dan lainnya</p>
                            </div>
                        </div>
                    </div>
                </div><!-- / .item item-why -->

                <div class="col-md-3 item item-why">
                    <div class="subitem-item-why">
                        <span class="item-why-wrapper">
                                <span class="sprite icon-lock"></span>
                        </span>

                        <h3>Transaksi Aman</h3>

                        <div class="item item-why-caption">
                            <div class="item item-why-caption-content">
                                <p>Teknologi SSL dari RapidSSL dengan Sertifikat yang terotentikasi menjamin privasi dan keamanan transaksi online Anda. Konfirmasi instan dan e-tiket dikirm ke email Anda.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- / .item item-why -->

                <div class="col-md-3 item item-why">
                    <div class="subitem-item-why">
                        <span class="item-why-wrapper">
                                <span class="sprite icon-credit-card"></span>
                        </span>

                        <h3>Pembayaran Mudah</h3>

                        <div class="item item-why-caption">
                            <div class="item item-why-caption-content">
                                <p>Pembelian tiket menjadi semakin fleksibel dengan berbagai pilihan pembayaran dari Transfer ATM, Kartu Kredit, Inter Banking dan melalui berbagai gerai minimarket yang tersebar di seluruh Indonesia</p>
                            </div>
                        </div>
                    </div>
                </div><!-- / .item item-why -->


            </div>
        </div>
    </section>

    <section class="section section-cta section-smallpad bg-gray hidden-995">
        <div class="container">
            <div class="row section-content">

                <div class="col-md-6 cta-1">
                    <header class="section-header">
                        <h2>4 Langkah Praktis Pesan Tiket Bus</h2>
                    </header>

                    <div class="section-content">
                        <div class="cta-carousel owl-carousel">
                            <div class="item-cta">
                                    <img src="assets/images/langkah-praktis/1.png" alt="" width="276">    
                            </div>
                            <div class="item-cta">
                                    <img src="assets/images/langkah-praktis/2.png" alt="" width="276">    
                            </div>
                            <div class="item-cta">
                                    <img src="assets/images/langkah-praktis/3.png" alt="" width="276">    
                            </div>
                            <div class="item-cta">
                                    <img src="assets/images/langkah-praktis/4.png" alt="" width="276">    
                            </div>
                        </div>                            
                    </div>
                </div>

                <div class="col-md-6 cta-2">
                    <header class="section-header">
                        <h2>Unduh Aplikasi BUSTIKET.COM</h2>
                    </header>

                    <div class="section-content">
                        <div class="text-center">  
                            <div class="clearfix space space-30"></div>
                            <div class="clearfix space space-30"></div>
                            <a href='https://play.google.com/store/apps/details?id=com.bustiket.app'><span class="sprite icon-google-play"></span></a>
                            <div class="clearfix space space-30"></div> 
                            <div class="clearfix space space-30"></div>
                                <p class="text-green text-left font-16">Kami akan mengirimkan tautan untuk mengunduh aplikasi langsung ke ponsel atau tablet anda</p>
                        </div>

                        <div class="row cta-wrapper">
                            <div class="col-md-6">
                                <form action="" class="form-cta">
                                    <p class="form-field">
                                        <input type="tel" name="" placeholder="No. Telpon">
                                    </p>
                                    <p class="form-field form-submit">
                                        <input type="submit" class="btn btn-green btn-radius" value="Kirim Via SMS">
                                    </p>
                                </form>
                            </div>

                            <div class="col-md-6">
                                <form action="" class="form-cta" >
                                    <p class="form-field">
                                        <input type="tel" name=""  placeholder="Alamat Email">
                                    </p>
                                    <p class="form-field form-submit">
                                        <input type="button"  class="btn btn-green btn-radius" value="Kirim Via Email">
                                        
                                    </p>
                                    

                                </form>
                            </div>
                        </div>
                    </div><!-- / .section-content -->
                </div>

            </div>
        </div>
    </section>

    <section class="section section-join-us hidden-995" id="joinUS" >
        <div class="container" >
            <header class="section-header">
                <h2>Bergabung Bersama Kami</h2>
            </header>

            <div class="row section-content">
                <div class="col-md-6 item-join-us">
                    <div class="subitem-join-us">
                        <span class="item-join-us-icon">
                                <span class="sprite icon-service-bus"></span>
                        </span>

                        <h3 class="item-join-us-title">Perusahaan Bus</h3>
                            <p class="">Tingkatkan Penjualan tiket Anda secar Efisien, Modern, dan Murah dengan bergabung menjadi mitra kami!</p>

                        <a href="{{URL::to('company-bus')}}" class="btn btn-yellow btn-radius btn-big">GABUNG DISINI</a>
                    </div>
                </div>

                <div class="col-md-6 item-join-us">
                    <div class="subitem-join-us">
                        <span class="item-join-us-icon">
                                <span class="sprite icon-service-gear"></span>
                        </span>

                        <h3 class="item-join-us-title">Mitra BUSTIKET</h3>
                            <p class="">Ingin memiliki bisnis sendiri yang Mudah, Cepat, dan Aman? Segeralah bergabung menjadi mitra kami!</p>

                        <a href="{{URL::to('partner')}}" class="btn btn-green btn-radius btn-big">GABUNG DISINI</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-request-operator bg-gray hidden-995">
        <div class="container">
            <div class="row section-content">

                <div class="col-md-5">
                        <div class="clearfix space space-15"></div>
                    <figure>
                            <img src="assets/images/map-pin-new.png" alt="">
                    </figure>
                </div>

                    <div class="col-md-7">
                    <header class="section-header text-left">
                        <h2>Request Operator Bus</h2>
                    </header>

                    <div class="section-content">
                            <p class="font-16 text-gray-placeholder">Belum memiliki operator bus atau tujuan yang Anda butuhkan?, Jangan kuatir, silahkan menghubungi tim kami yang akan berupaya untuk segera memenuhi kebutuhan Anda.</p>    
                        <span class="clearfix space space-30"></span>
                        <a href="mailto:info@bustiket.com" class="btn btn-yellow btn-radius btn-big btn-minwidth">REQUEST</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="section section-testimonial hidden-995">
        <div class="container">
            <header class="section-header">
                <h2>Testimonial</h2>
            </header>                

            <div class="section-content row">
                <?php $test = ($body['testimonial']);
                for($i= 0; $i < 3; $i++){ ?>
                    <div class="col-md-4 item-testimonial">

                        <div class="subitem-testimonial">
                            <figure>
                                <img src="{{URL::to('assets/uploads/testimonial')}}/{{$test[$i]['image']}}" alt="" width="120">
                            </figure>
                            <h3 class="item-testimonial-title">{{$test[$i]['name']}}</h3>

                            <p>{{$test[$i]['message']}}</p>
                        </div>

                    </div><!-- / .item-testimonial -->
                <?php }?>
<!--                <div class="col-md-4 item-testimonial">

                    <div class="subitem-testimonial">
                        <figure>
                            <img src="assets/images/people-1.jpg" alt="" width="120">
                        </figure>
                        <h3 class="item-testimonial-title">Novri</h3>

                        <p>Cara Booking mudah, call center sangat membantu untuk booking</p>
                    </div>

                </div> / .item-testimonial 

                <div class="col-md-4 item-testimonial">

                    <div class="subitem-testimonial">
                        <figure>
                            <img src="assets/images/people-2.jpg" alt="" width="120">
                        </figure>
                        <h3 class="item-testimonial-title">Tari</h3>

                        <p>Bagus. Pelayanan bagus banget, customer service welcome banget</p>
                    </div>

                </div> / .item-testimonial 

                <div class="col-md-4 item-testimonial">

                    <div class="subitem-testimonial">
                        <figure>
                            <img src="assets/images/people-1.jpg" alt="" width="120">
                        </figure>
                        <h3 class="item-testimonial-title">Novri</h3>

                        <p>Cara Booking mudah, call center sangat membantu untuk booking</p>
                    </div>

                </div> / .item-testimonial -->
            </div>
        </div>
    </section>

    <section class="section section-subscribe section-smallestpad hidden-995">
        <div class="container">
            <form action="" id="frmSub" class="form-subscriber clearfix" method="post">
                <h2 class="form-subscriber-title">Dapatkan Promo Ekslusif Kami</h2>
                <p class="form-field-inline">
                    <input type="email" name="" placeholder="Alamat Email" id="semail">
                    
                </p>
                <p class="form-field-inline form-submit">
                    <input type="button" id="btnSubscribe" name="" value="SUBSCRIBE" class="btn btn-green btn-radius">
                    <img  id="btnS"  style="display:none" src="{{URL::asset('assets/img/loading.gif')}}" height="50px" width="50px" />
                </p>
                
            </form>
            <span class="col-sm-offset-5" id="onmail" ></span>
            
          
        </div>
    </section>

    <section class="section section-smallestpad hidden-995">
        <div class="container">
            <div class="row section-content">

                <div class="col-md-3 item-related">
                    <figure>
                        <img src="assets/images/media/tia.png" alt="">    
                    </figure>

                </div>

                <div class="col-md-3 item-related">
                    <figure>
                        <img src="assets/images/media/IDNTimes.png" alt="">    
                    </figure>

                </div>

                <div class="col-md-3 item-related">
                    <figure>
                        <img src="assets/images/media/dailysocial.png" alt="">    
                    </figure>
                </div>

                <div class="col-md-3 item-related">
                    <figure>
                        <img src="assets/images/media/kumparan.png" alt="">    
                    </figure>
                </div>

            </div>
        </div>
    </section>
        
<script>
    
    function validateEmail(sEmail) {   
        var filter =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if (filter.test(sEmail)) {
            return true;
            
        }else {
            return false;
            
        }
    }
             $(function(){

                
                 
                $("#btnSubscribe").click(function(){
                    $(this).hide();
                    $("#btnS").show();
                   
                var semail=$("#semail").val();

                var stoken=$("#stoken").val();
                    
                var flag=1;   
                
                  if(validateEmail(semail)){
                           $("#semail").css('border-color','black');
                  }else{
                           
                          $("#semail").focus();
                          $("#onmail").css('color','red');
                          $("#onmail").text('Enter Valid Email');
                          flag=0;
                }
                   
                   
     
                   if(flag==1){
                       var token = $("[name=csrf-token]").attr("content");
                       var oprData={semail:semail};
                      
                        $.ajax({
                           url:'{{URL::to("send-mail-new-subscriber")}}',
                           type:'post',
                           dataType:'json',
                           data:{_token:token,oprData:oprData},
                           success:function(data){
                               var jdata=JSON.parse(JSON.stringify(data));
                               if(jdata.flag==1){
                                   $("#onmail").show();
                                   $("#onmail").css('color','green');
                                   $("#onmail").text(jdata.msg);
                                   $("#frmSub")[0].reset();
                                   $("#btnSubscribe").show();
                                   $("#btnS").hide();
                               }
                               else if(jdata.flag==0){
                                   $("#btnSubscribe").show();
                                   $("#btnS").hide();
                               }
                              
                           }
                       });
                   }
                   else{
                       $(this).show();
                       $("#btnS").hide();
                   }
                    
                    
                });  
                
               $('#frmSub').submit(function(e){
                   e.preventDefault();
                   $("#btnSubscribe").trigger('click');
               });
    });    
        </script>



@endsection
