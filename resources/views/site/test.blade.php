@extends('layouts.site_layout')

@section('content')

<style>
    table td{
        text-align: center;
        border: 1px solid white;
        background-color: grey;
        color: white;
        font-weight: bolder
    }
    table td.CS
    {
        background-color: green;
    }
    table td.DS
    {
        background-color: blue;
    }
    table td.T
    {
        background-color: yellow;
        color: black
    }
    table td.D
    {
        background-color: white;
        color: black
    }
    table td.gap
    {
        /*        background-color: white;
                color: white*/
    }

    .box{
        width: 100px;
    }
</style>

<script>
    $(function () {
        var $container = $("#my_box"),
                gridWidth = 196,
                gridHeight = 100,
                gridRows = 6,
                gridColumns = 5,
                i, x, y;

        //loop through and create the grid (a div for each cell). Feel free to tweak the variables above
        for (i = 0; i < gridRows * gridColumns; i++) {
            y = Math.floor(i / gridColumns) * gridHeight;
            x = (i * gridWidth) % (gridColumns * gridWidth);
            $("<div/>").css({position: "absolute", border: "1px solid #454545", width: gridWidth - 1, height: gridHeight - 1, top: y, left: x}).prependTo($container);
        }

        //set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
        TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
        TweenLite.set(".box", {width: gridWidth, height: gridHeight, lineHeight: gridHeight + "px"});

        Draggable.create(".box", {
            bounds: $container,
            edgeResistance: 0.65,
            type: "x,y",
            throwProps: true,
            autoScroll: true,
            liveSnap : true
        });
    });
</script>

<div class="container">

    <div class="row">
        <div class="col-lg-offset-3 col-lg-9">

            <div id="my_box">
                <div class="box" id="box1">Drag and throw me 1</div>
                <div class="box" id="box2">Drag and throw me 2</div>
            </div>

            <br><br><br><br><br><br><br><br>


            <?php $layout = $layouts["1"]; ?>
            <table>
                <?php for ($row = 1; $row < $layout["row"] + 1; $row++) { ?>
                    <tr>
                        <?php
                        for ($col = 1; $col < $layout["col"] + 1; $col++) {
                            $found = 0;
                            ?>
                            <?php for ($i = 0; $i < count($layout['map']); $i++) { ?>
                                <?php if ($layout['map'][$i]['row'] == $row && $layout['map'][$i]['col'] == $col) { ?>
                                    <td class="<?php echo $layout['map'][$i]['object_type']; ?>" rowspan="<?php $layout['map'][$i]['height']; ?>" colspan="<?php $layout['map'][$i]['length']; ?>">
                                        <?php echo $layout['map'][$i]['seat_lbl']; ?>
                                    </td>
                                    <?php
                                    $found = 1;
                                    $rowspan = $layout['map'][$i]['height'] != 1 ? 1 : 0;
                                    break;
                                }
                                ?>
                                <?php
                            }
                            if (!$found) {
                                $rowspan = 0;
                                ?>  <td><?php echo "D - " . $row . "-" . $col; ?></td> <?php
                            }
                            ?>
                        <?php }
                        ?>
                    </tr>
                <?php } ?>
            </table>
            <br><br><br><br><br><br><br><br>
            <table>
                <?php for ($row = 0; $row < count($layout['seats']); $row++) { ?>
                    <tr>
                        <?php
                        for ($col = 0; $col < count($layout['seats'][$row]); $col++) {
                            if (!is_null($layout['seats'][$row][$col])) {
                                ?>
                                <td class="<?php echo $layout['seats'][$row][$col]['object_type']; ?>" rowspan="<?php echo $layout['seats'][$row][$col]['height']; ?>" colspan="<?php echo $layout['seats'][$row][$col]['length']; ?>">
                                    <?php echo $layout['seats'][$row][$col]['seat_lbl']; ?>
                                </td>

                                <?php
                            } else {
                                ?>  <td class="gap"><?php echo "D - " . $row . "-" . $col; ?></td> <?php
                            }
                        }
                        ?>
                    </tr>
                <?php } ?>
            </table>

        </div>
    </div>
</div>
@endsection
