@extends('layouts.site_layout')
@section('content')
<style>
    a{
        color:black;
    }
</style>
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url({{URL::to('assets/images/heropages/1.jpg')}});">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

<!--        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Kota lanniya</h1>
                            </header>
                           <h2 class="item-company-title">PERJALANAN DARI {{strtoupper($body[1])}}</h2>
                           <table class="table   table-responsive table-striped">
                               <?php
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                    $cityname=$body[1];
                                   
                               ?>
                                <?php
                                    echo "<tr>";
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        if($city==$cityname){
                                            continue;
                                        }
                                        $strurl0="search-bus/".$cityname.".C.".$city.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                                    ?>
                                    <td><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{URL::to($strurl0)}}">Ticket bus dari {{$cityname}} ke {{$city}}</a></td>                                
                                
                              <?php
                                        if(($cnt%4)==0){
                                            echo "</tr><tr>";
                                        }
                                        $cnt++;
                                    }
                             ?>                          
                                
                           </table>  
                           <h2 class="item-company-title">PERJALANAN KE {{strtoupper($cityname)}}</h2>
                            <table class="table   table-responsive table-striped">
                               <?php
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                    $cityname=$body[1];
                                   
                               ?>
                                <?php
                                    echo "<tr>";
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        if($city==$cityname){
                                            continue;
                                        }
                                        $city=$citylist[$i];
                                        $strurl0="search-bus/".$city.".C.".$cityname.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                               ?>
                                <td><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{URL::to($strurl0)}}">Ticket bus dari {{$city}} ke {{$cityname}} </a></td>
                               <?php
                                        if(($cnt%4)==0){
                                            echo "</tr><tr>";
                                        }
                                        $cnt++;
                                    }
                                ?>                                      
                           </table> 
                        </article> / .article-single 

                    </div>
                </div>
            </div>
        </div> / .site-main-content site-main-content-offset -->

        <div class="site-main-content of-x">
            <div class="container container-1170">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single article-rute clearfix">
                            <header class="article-header">
                                <h1>Kota-kota keberangkatan terpopuler</h1>
                            </header>

                            <ul class="list-rute rute-lainnya kota-populer clearfix">
                                <?php
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                    $cityname=$body[1];
                                   
                               ?>
                                <?php
                                    
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        if($city==$cityname){
                                            continue;
                                        }
                                        $strurl0="search-bus/".$cityname.".C.".$city.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                                    ?>
                                    <li class="">
                                        <span class="sprite icon-placeholder-small"></span>  <a href="{{URL::to($strurl0)}}">Ticket bus dari {{$cityname}} ke {{$city}}</a>
                                    </li>
                                
                              <?php
                                        
                                    }
                             ?>
                                

                            </ul>
                        </article><!-- / .article-single -->

                        <article class="article-single article-rute clearfix">
                            <header class="article-header">
                                <h1>Kota-kota tujuan terpopuler</h1>
                            </header>

                            <ul class="list-rute rute-lainnya clearfix">
                                <?php
                                    echo "<tr>";
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        if($city==$cityname){
                                            continue;
                                        }
                                        $city=$citylist[$i];
                                        $strurl0="search-bus/".$city.".C.".$cityname.".C.".date('Y-m-d',strtotime('+3 day')).'.2';
                               ?>
                                <li class="">
                                    <span class="sprite icon-placeholder-small"></span>  <a href="{{URL::to($strurl0)}}">Ticket bus dari {{$city}} ke {{$cityname}} </a>
                                </li>
                               <?php
                                        
                                    }
                                ?>       
                                
                            </ul>

                        </article>

                    </div>
                </div>
            </div>
        </div>
        <script>

        </script>
@stop
