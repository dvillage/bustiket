
<h2>Header</h2>
<table>
    <thead>
        <tr>
            <th style="width: 30%">Name</th>
            <th style="width: 70%">Value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Accept</td>
            <td>application/json</td>
        </tr>
        <?php if($type == "request_token") { ?>
        <tr>
            <td>RequestToken</td>
            <td>Get it from Get Requst Token API</td>
        </tr>
        <?php } ?>
        <?php if($type == "auth_token") { ?>
        <tr>
            <td>AuthToken</td>
            <td>Get it from Seatseller Login API</td>
        </tr>
        <?php } ?>
        <tr>
            <td>User Agent</td>
            <td>android bustiket.com XXXXXXXXX</td>
        </tr>
    </tbody>
</table>