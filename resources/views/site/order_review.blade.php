@extends('layouts.site_layout')

@section('content')

<div class="greenStrip">
  <div class="greenStripInner">
    <div class="container">
      <h2>Periksa tiket Anda sebelum melakukan pembayaran</h2>
    </div>
  </div>
</div>

<div class="container">
	<div class="row">
		<div id="view" class="mid-toggle passenger-info">
			<div id="table-tab3" class="tab-pane  fade my_tab active in ">
        		<form action="inter.php" method="post" id="from" name="from">
					<div id="table-tab3" class="pembayaran tab-pane my_tab fade in dfv">
						<div class="pem-box">
							<div class="col-md-12">
								<div class="right-bg">
									<div class="bus-ticket">
										<div class="head-toggle clearfix">
											<div class="row">
												<div class="col-xs-6">
													<h1 class="txtGreen">Pahala Kencana</h1>
													<h3>
														Executive - 531              </h3>
													<p class="txtGreen hidden-md no-br hidden-lg">Rute Jakarta - Bekasi - Purwakarta - Cirebon - Brebes - Brebes. jawa tengah - Banyumas - Purbalingga - Banjarnegara - Banjarnegara. jawa tengah. terminal - Wonosobo - Bekasi - Purwakarta - Cirebon - Brebes - Brebes. jawa tengah - Banyumas - Purbalingga - Banjarnegara - Banjarnegara. jawa tengah. terminal - Wonosobo </p>

												</div>
												<div class="col-xs-6">
													<div class="info-masi text-right">
													<!-- <p>Your session will be out with in <span style="font-size: 17px; color: #f00;" id="countTime"></span> Minutes.</p> -->
													</div>
												</div>
											</div>
										</div>
											
										<div class="well passengerInfo passengerTicket">
											<div class="hidden-xs hidden-sm">
												<table class="table tableWhite noMargin">
												<thead>
												<tr>
												<td><h5 class="txtYellow">Berangkat Dari</h5></td>
												  <td></td>
												  <td></td>
												  <td></td>
												  <td></td>
												  <td><h5 class="txtYellow">Tiba Di</h5></td>
												  <td></td>
												</tr>
											</thead>
											<tbody>
										<tr>
										<td><h4 class="txtGray">
											JAKARTA TIMUR. DKI. TERM. PULOGEBANG                    </h4>
										<h3>JAKARTA</h3>
                                        <p class="no-br txtGreen"> JAKARTA TIMUR. DKI. TERM. PULOGEBANG </p></td>
								<td class="text-center"><h4 class="txtGray">1 August, 2017</h4>
								<h3>18:00</h3></td>
							<td class="text-center"><i class="fa fa-angle-right fa-3x"></i></td>
							<td class="text-center "><p>Rute</p>
							<p class="no-br txtGreen"> Jakarta - Bekasi - Purwakarta - Cirebon - Brebes -<br> 
								Brebes. jawa tengah - Banyumas - Purbalingga -<br> 
								Banjarnegara - Banjarnegara. jawa tengah. terminal<br> 
								- Wonosobo - Bekasi - Purwakarta - Cirebon -<br> 
								Brebes - Brebes. jawa tengah - Banyumas -<br> 
								Purbalingga - Banjarnegara - Banjarnegara. jawa<br> 
								tengah. terminal - Wonosobo </p>
							</td>
						<td class="text-center"><i class="fa fa-angle-right fa-3x"></i></td>
						<td><h4 class="txtGray">MENDOLO</h4>
						<h3>WONOSOBO</h3></td>
						<td class="text-center"><h4 class="txtGray">
                                          </h4>
							<h3>03:59</h3></td>
						</tr>
					</tbody>
					</table>
					</div>
				<div class="hidden-lg hidden-md">
					<table class="table tableWhite noMargin">
					<tbody>
					<tr>
					<td colspan="2"><h5 class="txtYellow">Berangkat Dari</h5></td>
					</tr>
					<tr>
					<td><h4 class="txtGray">
                      JAKARTA TIMUR. DKI. TERM. PULOGEBANG                    </h4>
                    <h3>JAKARTA</h3></td>
                  <td class="text-left"><h4 class="txtGray">1 August, 2017</h4>
                    <h3>18:00</h3></td>
                </tr>
                <tr>
                  <td colspan="2"><h5 class="txtYellow">Tiba Di</h5></td>
                </tr>
                <tr>
                  <td><h4 class="txtGray">MENDOLO</h4>
                    <h3>WONOSOBO</h3></td>
                  <td class="text-left"><h4 class="txtGray">1 August, 2017</h4>
                    <h3>03:59</h3></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>	
		
		<div id="passengerData">
                    <div class="well passengerInfo passengerHeading">
            <table class="table tableWhite noMargin" id="passenger_info">
              <colgroup>
              <col width="15%">
              <col width="35%">
              <col width="25%">
              <col width="25%">
              </colgroup>
              <thead>
                <tr>
                  <th class="txtGray">No</th>
                  <th class="txtGray">Nama Penumpang</th>
                  <th class="txtGray text-center">Jenis Kelamin</th>
                  <th class="txtGray text-center">No. Kursi</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="passengerData well passengerInfo">
            <table class="table noMargin tableWhite" id="passenger_info">
              <colgroup>
              <col width="15%">
              <col width="35%">
              <col width="25%">
              <col width="25%">
              </colgroup>
              <tbody>
                                <tr>
                  <td>1</td>
                  <td>sagar kumar</td>
                  <td align="center">P</td>
                  <td align="center" class="">
                  9A                  </td>
                </tr>
                              </tbody>
            </table>
          </div>
        </div>
		
		<div class="psgTicketPrice">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-8">
              <div class="row">
                <div class="col-xs-6">
                  <h3>Harga Tiket</h3>
                </div>
                <div class="col-xs-6 text-right">
                  <div class="ticketPrice h2">Rp.11.500</div>
                </div>
              </div>
            </div>
          </div>
        </div>
		
		<div class="headingCheckbox">
          <label>
          <div>
            <input type="checkbox" onclick="toggle_visibility('promoCodewin');" value="1" id="" name="promocode">
            <label for="thing"></label>
          </div>
                    <h3>Saya punya kode promo</h3>
          </label>
        </div>
		
		<script type="text/javascript">
 
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
 
        </script>
		
		<div id="promoCodewin" class="well passengerInfo" style="display: none;">
          <div class="form-group noMargin">
            <table class="table tableWhite noMargin">
              <tbody>
                <tr>
                  <td><label for="name" class="font10 black8">MASUKKAN KODE PROMO:</label></td>
                  <!-- onblur="checkcode(this.value,'167','PK-403 ','2017-08-01')" ,discountcheck(document.getElementById('from').promo_code.value,'167','PK-403 ','10000','2017-08-01')-->
                  <td><input type="text" name="promo_code" id="promo_code" class="form-control font12" placeholder="D17EA9DBEE2F" value=""></td>
                  <td><a class="btn btn-success" href="javascript:void(0)" onclick="checkcode(document.getElementById('from').promo_code.value,'167','PK-403 ','2017-08-01','10000');">Apply Code</a></td>
                </tr>
              </tbody>
            </table>
            <div id="check_res"></div>
           <span id="discount_res"></span>
          </div>
        </div>
		
		<div class="row">
                    <div class="col-sm-12">
            <div class="final-detail-ticket">
              <ul>
                 <li>
                  <div class="row">
                    <div class="col-md-10 col-sm-8">Biaya Layanan</div>
                    <div class="col-md-2 col-sm-4">Rp. <span>
                      10.000                      </span></div>
                  </div>
                </li>
                <li class="last">
                  <div class="row">
                    <div class="col-md-10 col-sm-8">
                      <div class="h3">Harga Total</div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div id="gross_amount" class="h1 txtGreen ">Rp. <span>21.500</span></div>
                    </div>
                  </div>
                </li>
              </ul>
              <input name="gross_amount" type="hidden" class="" value="11500">
            </div>
          </div>
        </div>
									</div>
							</div>
						</div>
					</div>
					
					<div class="divider40"></div>
					<div id="pembox1-sep">
						<div class="col-md-12">
							<div class=" pembayaran">
								<div class="pem-box2">
									<div class="right-bg-tab">
										<div class="info-masi"> </div>
										<div class="well  passengerInfo">
											<h2>Info Pembayaran</h2>
											<div id="info-tab" class="info-tab-pem hidden-sm hidden-xs">
											<div class="row">
											<div class="col-sm-4 col-md-3 col-lg-2 ">
											  <ul role="tablist" class="nav nav-pills nav-stacked" id="myTab">
												<li tab="1" role="presentation" class=" active"> <a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#info-tab1">Bank Transfer </a> </li>
												<li tab="2" class="" role="presentation"> <a aria-controls="" data-toggle="tab" id="" role="tab" href="#info-tab2" aria-expanded="true">Kartu Kredit</a> </li>
												<li tab="3" class="" role="presentation"><a aria-controls="" data-toggle="tab" id="" role="tab" href="#info-tab6" aria-expanded="true">Internet Banking</a></li>
												<li tab="5" class="" role="presentation"><a aria-controls="" data-toggle="tab" id="" role="tab" href="#info-tab5" aria-expanded="true">Minimarket</a></li>
												<li tab="4" class="" role="presentation"><a aria-controls="" data-toggle="tab" id="" role="tab" href="#info-tab4" aria-expanded="true">Doku Wallet</a></li>
											  </ul>
											</div>
										<div class=" col-sm-8 col-md-9 col-lg-10 ">
											<div class="tab-content" id="infoTabContent">
												<div aria-labelledby="home-tab" id="info-tab1" class="tab-pane fade tab-box2 active in">
														<div class="right-detail pull-left">
															<h3> Kami menerima pembayaran dengan transfer (melalui ATM/Internet Banking/SMS Banking) :</h3>
															<p>Mohon lakukan pembayaran transfer dari dan untuk rekening Bank yang sama. Jika Anda tidak 
															  memiliki rekening Bank yang sama dengan kami, lakukan transfer HANYA melalui ATM agar 
															  transaksi berlangsung real time. Verifikasi pembayaran tidak bisa kami lakukan jika Anda 
															  tidak mengikuti ketentuan diatas.</p>
														<div id="bankDetail">
															<div class="row">
															<div class="col-sm-6">
														  <h3>Rekening Bank</h3>
														  <p>Pilih salah satu rekening kami yang akan dituju</p>
														</div>
        <script type="text/javascript">


          $(document).ready(function () {

          $( "#selectMe" ).change(function() {
              if ($(this).val() == "option1"){
                  $("#selectMe1").val("option11");
              }
              if ($(this).val() == "option2"){
                  $("#selectMe1").val("option22");
              }
           });

          $('.group').hide();
          $('#option1').show();
          $('#selectMe').change(function () {
          $('.group').hide();
          $('#'+$(this).val()).show();
          })
          });
        </script>
													<div class="col-sm-6">
													  <select name="bankTransfer" id="selectMe" class="form-control">
														<option value="option1">Bank BCA</option>
														<option value="option2">Bank Mandiri</option>
													  </select>
													<div id="option1" class="group addressBank" style="display: block;"> Bank Central Asia (BCA) – KCP Blok A Cipete<br>
														Nama Rekening: CV. Mitra Indo Visi Group<br>
														Nomor Rekening: 218-­‐008-­‐8809<br>
																<br>
															  </div>
													  <div id="option2" class="group addressBank" style="display: none;">Bank Mandiri – Pondok Indah Office Tower 3 <br>
														Nama Rekening: CV. Mitra Indo Visi Group<br>
														Nomor Rekening: 101-­‐00017320813<br>
														<br>
													  </div>
														</div>
      </div>
    </div>
  </div>
									  <div class="clearfix"></div>
									  <div class="btn-box">
										<button name="book" class="btn btn-primary" id="book" type="submit">LAKUKAN PEMBAYARAN</button>
									  </div>
									  <p>Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="term_condition.php">Syarat dan Ketentuan</a> pelayanan BUSTIKET.</p>
									</div>
									<div id="info-tab20" class="tab-pane fade">
										<h3>SYARAT DAN KETENTUAN KARTU KREDIT </h3>
									<ol>
									  <li>Kami menerima pembayaran dengan kartu kredit berlogo Visa dan Master Card </li>
									  <li>Pastikan saldo limit kartu kredit Anda cukup untuk bertransaksi. </li>
									  <li>Masukkan nomor kartu kredit, expiration date dan CW.</li>
									  <li>Pastikan detail pembayaran Anda benar dan lanjutkan ke step 3D Secure </li>
									  <li>One Time Password (OTP) akan dikirimkan ke nomor ponsel Anda yang telah diregistrasikan dengan kartu kredit Anda</li>
									  <li>Masukkan OTP yang Anda telah terima di halaman 3D Secure </li>
									  <li>Pembayaran Anda dengan kartu kredit 3D Secure selesai8 8. Setelah pembayaran dianggap berhasil, Anda akan menerima E-Tiket secara otomatis ke email Anda dalam waktu beberapa menit kedepan.</li>
																</ol>
							  <input type="submit" name="book" class="btn btn-primary" id="book" value="Pay with VT-Web">

							</div>
							<div id="info-tab2" class="tab-pane fade">
							  <div class="col-xs-12" style="padding-top:10px;padding-bottom:30px">
								 Pastikan bahwa Anda memiliki ID dan Password pengguna untuk Doku Wallet dan telah terdaftar 
								 <b>SYARAT DAN KETENTUAN KARTU KREDIT</b>
								 <div>1. Kami menerima pembayaran dengan kartu kredit berlogo Visa dan Master Card</div>
								<div>2. Pastikan saldo limit kartu kredit Anda cukup untuk bertransaksi.</div>
								<div>3. Masukkan nomor kartu kredit, expiration date dan CCW.</div>
								<div>4. Pastikan detail pembayaran Anda benar dan lanjutkan ke step 3D Secure</div>
								<div>5. One Time Password (OTP) akan dikirimkan ke nomor ponsel Anda yang telah diregistrasikan dengan kartu kredit Anda</div>
								<div>6. Masukkan OTP yang Anda telah terima di halaman 3D Secure</div>
								<div>7. Pembayaran Anda dengan kartu kredit 3D Secure selesai. Setelah pembayaran dianggap berhasil, Anda akan menerima E-Tiket secara otomatis ke email Anda dalam waktu beberapa menit kedepan.</div>
							  </div>
							  <div class="col-md-3" style="padding-top:10px;padding-bottom:10px">
								  <input type="hidden" name="paymentchannelc" value="15">
								  <input type="submit" name="book" class="btn btn-primary" id="book" value="LAKUKAN PEMBAYARAAN">
							  </div>
								 <div class="col-md-12"> 
									 <p class="small">Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="term_condition.php" target="_blank">Syarat &amp; Ketentuan</a> pelayanan BUSTIKET.</p>
								 </div> 
								<!-- Doku end Here -->
								</div>
								<div id="info-tab3" class="tab-pane fade ">
								  <div class="col-md-offset-5 col-md-7">
									<div class="info-tab-pem">
								</div>
  </div>
</div>
						<div id="info-tab6" class="tab-pane fade ">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="info-tab-pem">
									<h3>Kami menerima pembayaran melalui berbagai macam internet banking.</h3>
										<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
										<table>
								<tbody><tr><td>
								  <input type="radio" value="24" name="paymentchannel">
								  <label for="thing"></label>
									</td>
										<td><img src="http://192.168.0.104/bustiket/image/payment_img/buy.png" class="alfamart minimarket"></td>
										<td>
										<input type="radio" value="02" name="paymentchannel">
										<label for="thing"></label> 
										</td> 
										<td>
									<img src="http://192.168.0.104/bustiket/image/payment_img/mandire.png" class="alfamart minimarket"> 
									</td>    
									<td>
								<input type="radio" value="18" name="paymentchannel">
								<label for="thing"></label> 
									</td> 
									<td>
								<img src="http://192.168.0.104/bustiket/image/payment_img/e-pay.jpg" class="alfamart minimarket">
								</td>    
							</tr></tbody></table>  
							</div>
							</div>
							  <button type="submit" name="interbank" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
						  </div>
</div>
						<div id="info-tab4" class="tab-pane fade ">    
						   <div class="col-md-12" style="padding-top: 10px">
							   <img src="http://192.168.0.108/bustiket_new/public/assets/img/dokuwallet-logo.png" style="margin-left: 0px;width: 20%">
						   </div>    
						  <div class="col-xs-12" style="padding-top:10px;padding-bottom:30px">
							 Pastikan bahwa Anda memiliki ID dan Password pengguna untuk Doku Wallet dan telah terdaftar 
						  </div>
						  <div class="col-md-3" style="padding-top:10px;padding-bottom:10px">
							  <input type="hidden" name="paymentchanneld" value="04"> 
							<button type="submit" name="dokuwallet" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
						  </div>
						   <div class="col-md-12"> 
							 <p class="small">Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="term_condition.php" target="_blank">Syarat &amp; Ketentuan</a> pelayanan BUSTIKET.</p>
						   </div> 
</div>
						<div id="info-tab5" class="tab-pane fade ">
							<div class="col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid #eee">
							<div class="info-tab-pem">
							  <h3>Kami menerima pembayaran melalui berbagai macam mini market.</h3>
							  <div class="col-md-1" style="padding-top:10px;">
									  <input type="radio" value="14" name="paymentchannel">
									<label for="thing"></label>
      </div>
						  <div class="col-md-11 col-sm-11 col-xs-11">
							  <img src="http://192.168.0.104/bustiket/image/payment_img/alfamix.png" class="col-md-4 alfamart minimarket">
							  <img src="http://192.168.0.104/bustiket/image/payment_img/dandan.png" class="col-md-4 alfamart minimarket">
							  <img src="http://192.168.0.104/bustiket/image/payment_img/lawson.png" class="col-md-4 alfamart minimarket">
							  <img src="http://192.168.0.104/bustiket/image/payment_img/alfaexpress.jpg" class="col-md-4 alfamart minimarket">
						  </div>    
					</div>
					</div>
						  <div class="col-md-12 col-sm-12 col-xs-12">
							  <div class="col-md-1" style="padding-top:20px;">
									  <!--<input type="radio" value="31" name="paymentchannel">-->
									  <input type="radio" value="20" name="paymentchannel">
									<label for="thing"></label>
							  </div>
							  <div class="col-md-11 col-sm-11 col-xs-11" style="padding-top:20px;">
								  <img src="http://192.168.0.104/bustiket/image/payment_img/Indomaret.png" class="col-md-4 alfamart minimarket">
							  </div>
							<button type="submit" name="minimart" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
  </div>    
</div>    
</div>    
</div>
</div>
</div>
</div>

<div id="info-tab" class="info-tab-pem hidden-lg hidden-md">
  <div id="accordion" class="panel-group">
    <div class="panel panel-default in">
      <div class="panel-heading">
        <h4 class="panel-title"> <a acc="1" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Bank Transfer</a> </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse  in">
        <div class="panel-body">
          <div class="right-detail pull-left">
            <h3> Kami menerima pembayaran dengan transfer (melalui ATM/Internet Banking/SMS Banking) :</h3>
            <p>Mohon lakukan pembayaran transfer dari dan untuk rekening Bank yang sama. Jika Anda tidak 
              memiliki rekening Bank yang sama dengan kami, lakukan transfer HANYA melalui ATM agar 
              transaksi berlangsung real time. Verifikasi pembayaran tidak bisa kami lakukan jika Anda 
              tidak mengikuti ketentuan diatas.</p>
            <div id="bankDetail">
              <div class="row">
                <div class="col-sm-6">
                  <h3>Rekening Bank</h3>
                  <p>Pilih salah satu rekening kami yang akan dituju</p>
                </div>
                <script type="text/javascript">

          $(document).ready(function () {
          $('.group1').hide();
          $('#option11').show();
          $('#selectMe1').change(function () {
          $('.group1').hide();
          $('#'+$(this).val()).show();
          })
          });
        </script>
                <div class="col-sm-6">
                  <select name="bankTransfer" id="selectMe1" class="form-control">
                    <option value="option11">Bank BCA</option>
                    <option value="option22">Bank Mandiri</option>
                  </select>
                  <div id="option11" class="group1 addressBank" style="display: block;">Bank Central Asia (BCA) – KCP Blok A Cipete<br>
                    Nama Rekening: CV. Mitra Indo Visi Group<br>
                    Nomor Rekening: 218-­‐008-­‐8809<br>
                    <br>
                  </div>
                  <div id="option22" class="group1 addressBank" style="display: none;">Bank Mandiri – Pondok Indah Office Tower 3 <br>
                    Nama Rekening: CV. Mitra Indo Visi Group<br>
                    Nomor Rekening: 1010007320813<br>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="btn-box">
            <button name="book" class="btn btn-primary" id="book" type="submit">LAKUKAN PEMBAYARAN</button>
          </div>
          <p>Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan Syarat dan Ketentuan pelayanan BUSTIKET.</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title"> <a acc="2" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Kartu Kredit</a> </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse">
        <div class="panel-body">
            
          <h3>SYARAT DAN KETENTUAN KARTU KREDIT </h3>
          <ol>
            <li>Kami menerima pembayaran dengan kartu kredit berlogo Visa dan Master Card </li>
            <li>Pastikan saldo limit kartu kredit Anda cukup untuk bertransaksi. </li>
            <li>Masukkan nomor kartu kredit, expiration date dan CW.</li>
            <li>Pastikan detail pembayaran Anda benar dan lanjutkan ke step 3D Secure </li>
            <li>One Time Password (OTP) akan dikirimkan ke nomor ponsel Anda yang telah diregistrasikan dengan kartu kredit Anda</li>
            <li>Masukkan OTP yang Anda telah terima di halaman 3D Secure </li>
            <li>Pembayaran Anda dengan kartu kredit 3D Secure selesai8 8. Setelah pembayaran dianggap berhasil, Anda akan menerima E-Tiket secara otomatis ke email Anda dalam waktu beberapa menit kedepan.</li>
          </ol>
<!--          <div class="col-md-12" style="padding-top: 10px">
       <img src="image/busway/dokuwallet-logo.png" style="margin-left: 0px;width: 20%">
   </div>    
  <div class="col-xs-12" style="padding-top:10px;padding-bottom:30px">
     Pastikan bahwa Anda memiliki ID dan Password pengguna untuk Doku Wallet dan telah terdaftar 
  </div>-->
  <div class="col-md-3" style="padding-top:10px;padding-bottom:10px">
      <input type="hidden" name="paymentchannelc" value="15">
      <input type="submit" name="book" class="btn btn-primary" id="book" onclick="document.getElementById('form').submit();" value="LAKUKAN PEMBAYARAAN">
  </div>
<!-- <div class='col-md-12'> 
     <p class="small">Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="term_condition.php" target="_blank">Syarat &amp; Ketentuan</a> pelayanan BUSTIKET.</p>
 </div>
          <input type="submit" name="book"  class="btn btn-primary" id="book" onclick="document.getElementById('form').submit();"  value="Pay with VT-Web">-->
        </div>
      </div>
    </div>
    <div class="panel panel-default">  
      <div class="panel-heading">
        <h4 class="panel-title"> <a acc="6" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Internet Banking</a> </h4>
      </div>
      <div id="collapseSix" class="panel-collapse collapse">
        <div class="panel-body">
         <div class="col-md-12 col-sm-12 col-xs-12">   
         <p>Kami menerima pembayaran melalui berbagai macam internet banking.</p>
         </div>
      <div class="col-md-12 col-sm-12 col-xs-12">
          <table><tbody><tr>
              <td>
                  <input type="radio" value="24" name="paymentchannel">
                  <label for="thing"></label>
              </td>
              <td><img src="https://www.testing.bustiket.com/image/payment_img/buy.png" class="alfamart minimarket"></td></tr><tr>
              <td>
                 <input type="radio" value="02" name="paymentchannel">
            <label for="thing"></label> 
              </td> 
              <td>
                 <img src="https://www.testing.bustiket.com/image/payment_img/mandire.png" class="alfamart minimarket"> 
              </td></tr><tr>    
              <td>
                 <input type="radio" value="18" name="paymentchannel">
            <label for="thing"></label> 
              </td> 
              <td>
                 <img src="https://www.testing.bustiket.com/image/payment_img/e-pay.jpg" class="alfamart minimarket">
              </td>    
          </tr></tbody></table>
          
          </div>
        <div class="col-md-12 col-sm-12 col-xs-12">    
            <button type="submit" name="interbank" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
        </div>    
      </div>    
    </div>    
    </div>
    <div class="panel panel-default">  
    <div class="panel-heading">
        <h4 class="panel-title"> <a acc="5" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Mini Market</a> </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">  
      <div class="panel-body">
         <div class="col-md-12 col-sm-12 col-xs-12">   
         <p>Kami menerima pembayaran melalui berbagai macam mini market.</p>
         </div>
          <div class="col-sm-12 col-md-12 col-xs-12" style="border-bottom: 1px solid #eee">
            <div class="col-md-1" style="padding-top:10px;">
              <input type="radio" value="14" name="paymentchannel">
              <label for="thing"></label>
            </div>
        <div class="col-md-11 col-sm-11 col-xs-12">
            <img src="https://www.testing.bustiket.com/image/payment_img/alfamix.png" class="col-md-3 alfamart minimarket">
            <img src="https://www.testing.bustiket.com/image/payment_img/dandan.png" class="col-md-3 alfamart minimarket">
            <img src="https://www.testing.bustiket.com/image/payment_img/lawson.png" class="col-md-3 alfamart minimarket">
            <img src="https://www.testing.bustiket.com/image/payment_img/alfaexpress.jpg" class="col-md-3 alfamart minimarket">
        </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="col-md-1" style="padding-top:20px;">
<!--              <input type="radio" value="31" name="paymentchannel">-->
          <input type="radio" value="20" name="paymentchannel">
            <label for="thing"></label>
      </div>
      <div class="col-md-11 col-sm-11 col-xs-11">
		  <img src="https://www.testing.bustiket.com/image/payment_img/Indomaret.png" class="col-md-4 alfamart minimarket">
      </div>
      <button type="submit" name="minimart" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
  </div>  
      </div>
     </div>    
    </div>    
      
    <div class="panel panel-default">  
    <div class="panel-heading">
        <h4 class="panel-title"> <a acc="4" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Doku Wallet</a> </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">  
      <div class="panel-body">
        <div class="col-md-12" style="padding-top: 10px">
            <img src="http://192.168.0.108/bustiket_new/public/assets/img/dokuwallet-logo.png" style="margin-left: 0px;width: 20%">
        </div>
        <div class="col-xs-12" style="padding-top:10px;padding-bottom:30px">
            Pastikan bahwa Anda memiliki ID dan Password pengguna untuk Doku Wallet dan telah terdaftar 
          
        </div>
        <div class="col-md-3" style="padding-top:10px;padding-bottom:10px">
            <button type="submit" name="dokuwallet" class="btn btn-primary">LAKUKAN PEMBAYARAN</button>
        </div>
        <div class="col-md-12"> 
          <p class="small">Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="term_condition.php" target="_blank">Syarat &amp; Ketentuan</a> pelayanan BUSTIKET.</p>
        </div>  
      </div>      
    </div>      
    </div>      
      

  </div>
</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
					</div>
				</form>
		</div>
	</div>
</div>
</div>


@endsection