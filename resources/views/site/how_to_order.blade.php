@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                             <article class="article-single clearfix">
                            <header class="article-header">
                                 <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#bookTicket"><h1>Cara Pesan Tiket Bus</h1></a></li>
                                    <li><a data-toggle="tab" href="#paymentTicket"><h1>Cara Pambayaran</h1></a></li>
                                </ul>
                            </header>
                            <div class="tab-content">
                                 <div id="bookTicket" class="tab-pane fade in active">
                                        
                                          <div class="thumbnail"><img src="{{URL::asset('assets/images/howtoorder/messages1.png')}}" alt=""/></div>
                                          <div class="thumbnail"><img src="{{URL::asset('assets/images/howtoorder/messages2.png')}}" alt=""/></div>
                                          <div class="thumbnail"><img src="{{URL::asset('assets/images/howtoorder/messages3.png')}}" alt=""/></div>
                                          <div class="thumbnail"><img src="{{URL::asset('assets/images/howtoorder/messages4.png')}}" alt=""/></div>
                                          <div class="thumbnail"><img src="{{URL::asset('assets/images/howtoorder/messages5.png')}}" alt=""/></div>
                                        
                                 </div>
                                 
                             <div    id="paymentTicket" class="tab-pane fade in ">
                                 <div class="panel karir-item">
                                    <div class="container">
                                        <div class="panel-heading karir-item-header" role="tab" id="heading1">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h2>Transfer</h2>
                                                   
                                                </div>
                                                
                                            </div>
                                        </div><!-- / karir-item-header #headingOne -->        

                                        <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading1" id="collapse1">
                                           <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Bank Transfer</h3>    
                                                    <p>
                                                        Pilih metode pembayaran dengan Bank Transfer, lalu pastikan Anda dapat melakukan transfer ke salah satu rekening kami yang ditampilkan pada halaman pembayaran. Pastikan Anda melakukan pembayaran sesuai nominal yang tertera dengan batas waktu maksimal 50 menit. Setelah pembayaran dilakukan, Anda dapat melakukan konfirmasi pembayaran di www.bustiket.com/konfirmasi-pembayaran dengan memasukkan "Kode Transfer", Info Bank yang dituju dan jumlah transfer. Kami akan lakukan verifikasi secara manual dan e-tiket akan dikirimkan ke alamat email Anda dalam waktu 60 menit.
                                                    </p>  
                                                </div>
                                            </div>

                                        </div><!-- / .karir-item-content -->

                                        <footer class="karir-item-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" class="content-collapse">
                                                        <span class="sprite icon-chevron-down"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </footer><!-- / .karir-item-footer -->
                                    </div>
                                </div><!-- / .panel .karir-item -->

                               <div class="panel karir-item">
                                    <div class="container">
                                        <div class="panel-heading karir-item-header" role="tab" id="heading2">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h2>Kartu Kredit</h2>
                                                   
                                                </div>
                                                
                                            </div>
                                        </div><!-- / karir-item-header #headingOne -->        

                                        <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" id="collapse2">
                                           <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Kartu Kredit</h3>    
                                                    <p>
                                                        Kami dapat menerima pembayaran via Kartu Kredit berlogo Visa atau Mastercard. Semua transaksi kartu kredit dijamin aman dengan teknologi Secure Socket Layer (SSL). Setelah pembayaran selesai, e-tiket akan dikirimkan ke alamat email Anda dalam waktu 60 menit.


                                                    </p>  
                                                </div>
                                            </div>


                                        </div><!-- / .karir-item-content -->

                                        <footer class="karir-item-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2" class="content-collapse">
                                                        <span class="sprite icon-chevron-down"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </footer><!-- / .karir-item-footer -->
                                    </div>
                                </div><!-- / .panel .karir-item -->

                                
                                                                 <div class="panel karir-item">
                                    <div class="container">
                                        <div class="panel-heading karir-item-header" role="tab" id="headingThree">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h2>Metode lain</h2>
                                                   
                                                </div>
                                                
                                            </div>
                                        </div><!-- / karir-item-header #headingOne -->        

                                        <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" id="collapseThree">
                                           <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Metode lainya</h3> 
                                                    <p>
                                                    Kami juga dapat menerima pembayaran via Internet Banking. Anda dapat langsung mengisi form dari Internet Banking dengan menyiapkan token PIN Anda. Transaksi dilakukan secara realtime dan aman, dengan teknologi Secure Socket Layer (SSL).
                                                    </p>
  
                                                </div>
                                            </div>

                                        </div><!-- / .karir-item-content -->

                                        <footer class="karir-item-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="content-collapse">
                                                        <span class="sprite icon-chevron-down"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </footer><!-- / .karir-item-footer -->
                                    </div>
                                </div><!-- / .panel .karir-item -->
                            </div>
                    </div>
                </div>
            </div>
        </div>
  

     </main>
          
@stop
