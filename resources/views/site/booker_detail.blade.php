@extends('layouts.site_layout')

@section('content')

<div class="greenStrip">
  <div class="greenStripInner">
    <div class="container">
      <div class="passenger-info">
        <div class="info-masi">
          <h2 class="inlineHead">Informasi penumpang atau</h2>
            <div class="btn-box">
				<button type="button" class="btn btn-success y-btn font14">SIGN IN</button>
            </div>
			<p> untuk mendapatkan mengikuti berita <br>
				dari BusTiket dan mendapatkan penawaran khusus </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container">
	<div class="row">
		<div id="view" class="mid-toggle passenger-info">
			<div id="table-tab2" class="tab-pane fade my_tab active in ">
				<div class="col-md-4 col-lg-3">
					<div class="left-bg">
						<h5 class="supperTitle">KETERANGAN PESANAN</h5>
						<div class="filter clearfix">
							<h3 class="title_filter">Perjalanan Pergi</h3>
							<div class="filter_data">
								<div class="mar-b14">
							<h5>OPERATOR BUS :</h5>
							<h3>
								Pahala Kencana          </h3>
							<p class="txtGreen">
								Executive - 531          </p>
							</div>
				<div class="mar-b14">
						<h5>BERANGKAT:</h5>
						<div class="sea_res_b"> <span> <strong style="color:#0099CC;"> 
						<!--Departures--> 
							</strong>
							<p>Tue, 1 Aug 2017</p><h3>18:00</h3><p>Jakarta • JAKARTA TIMUR. DKI. TERM. PULOGEBANG</p><p>JAKARTA TIMUR. DKI. TERM. PULOGEBANG</p>            </span> </div>
				</div>
   				<div class="mar-b14">
					  <h5>TIBA:</h5>
					  <div class="sea_res_b"> <span>
						<h3>03:59</h3><p>Wonosobo • MENDOLO</p>            </span> </div>
				</div>
			<div class="mar-b14">
						  <h5>RUTE :</h5>
						  <div class="sea_res_b journeyCx"> <span>
							<p class="txtYellow">
							  Jakarta - Bekasi - Purwakarta - Cirebon - Brebes -<br>
				Brebes. jawa tengah - Banyumas - Purbalingga -<br>
				Banjarnegara - Banjarnegara. jawa tengah. terminal<br>
				- Wonosobo - Bekasi - Purwakarta - Cirebon -<br>
				Brebes - Brebes. jawa tengah - Banyumas -<br>
				Purbalingga - Banjarnegara - Banjarnegara. jawa<br>
				tengah. terminal - Wonosobo            </p>
					</span> </div>
        </div>
        <div class="rp">
          <h5>TOTAL HARGA</h5>
          <h2 class="totalAmount"> <span class="sub">Rp</span> 11.500 </h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-8 col-lg-9">
	<div id="table-tab">
		<div class="tab-content" id="view-tab">
			<div class="row">
				<div class="col-md-12">
					<form method="post" action="booker_details_step3.php">
						<div class="well bs-component clearfix">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-2 col-md-3">
										<label>Penumpang  1</label>
									</div>
									<div class="col-lg-6 col-md-8">
										<div style="display:none" class="form-group flag_class">
										<input type="text" readonly="" class="form-control" value="Seat No. 9A">
										<input type="hidden" name="seatno_1" id="seatno_1" value="9A" class="">
									   </div>
									<div class="form-group">
									<div class="row row_sm">
									<div class="col-xs-6">
									<input type="text" placeholder="Nama Depan" name="passname_1" id="passname_1" class="form-control" onkeypress="" required="required" oncopy="return false" onpaste="return false">
									</div>
									<div class="col-xs-6">
									<input type="text" placeholder="Nama Belakang" name="l[1]" id="passname_" class="form-control" onkeypress="" required="required" oncopy="return false" onpaste="return false">
									</div>
								</div>
							</div>
								<div class="form-group">
									<div class="row">
									<div class="col-sm-3">
										<input type="number" name="age_1" id="age_1" size="5" maxlength="2" placeholder="Umur" class="form-control width50" required="required" onkeypress="return isNumberKey(event);" oncopy="return false" onpaste="return false">
									</div>
								<div class="col-sm-9">
									<div class="custom-button">
									<ul>
										<li>
										<label>
											<input type="radio" name="gender_1" id="1gender_1" value="L" checked="checked">
										<label for="thing"></label>
										<span class="lbl padding-8 black9">Laki-laki</span>
									</label>
	                              </li>
                              <li>
                                <label>
                                <input type="radio" name="gender_1" id="gender_1" value="P">
                                <label for="thing"></label>
                                <label for="thing"></label>
                                <span class="lbl padding-8 black9">Perempuan</span>
                                </label>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-2 col-md-3"> <label>Nama pemesan</label> </div>
							<div class="col-lg-6 col-md-8">
								<div class="form-group">
									<div class="row row_sm">
										<div class="col-xs-6">
											<input type="text" value="" required="required" placeholder="Nama Depan" name="booker_name" id="booker_name" onkeypress="" class="form-control" oncopy="return false" onpaste="return false">
										</div>
										<div class="col-xs-6">
							                          <input type="text" value="" required="required" placeholder="Nama Belakang" name="booker_lastname" id="booker_lastname" onkeypress="" class="form-control" oncopy="return false" onpaste="return false">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-3"><input type="number" name="age_2" id="age_2" size="5" maxlength="2" placeholder="Umur" class="form-control width50" required="required" onkeypress="return isNumberKey(event);" oncopy="return false" onpaste="return false"></div>
								<div class="col-sm-9">                      <div class="custom-button">
									<ul>
										<li>
											<label>
										<input type="radio" name="gender_2" id="1gender_2" value="M" checked="checked">
											<label for="thing"></label>
											<span class="lbl padding-8 black9">Laki-laki</span>
										</label>
										</li>
									<li>
									<label>
										<input type="radio" name="gender_2" id="gender_2" value="F">
											<label for="thing"></label>
										<label for="thing"></label>
										<span class="lbl padding-8 black9">Perempuan</span>
									</label>
									</li>
								</ul>
							</div></div></div>
							</div>
						</div>
					</div>
						<div class="row">
							<div class="col-lg-2 col-md-3">
								<label>Email</label>
						</div>
						<div class="col-lg-6 col-md-8">
							<div class="form-group">
								<input type="email" value="" required="required" placeholder="Masukan Email anda, Tiket akan dikirim via Email" name="email" id="email" class="form-control" oncopy="return false" onpaste="return false">
							</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-3">
							<label>Nomor Telepon </label>
								</div>
					<div class="col-lg-6 col-md-8">
                    <div class="row row_sm">
                          <div class="col-xs-3"><input type="text" required="required" readonly="" value="+62" placeholder="+62" id="phone" class="form-control width50"></div>
                          <div class="col-xs-9"><input name="mobile" type="number" required="required" maxlength="15" onkeypress="return isNumberKey(event);" placeholder="Nomor Telepon" id="mobile" class="form-control" oncopy="return false" onpaste="return false"></div>
                    </div>
                    <div class="btn-box text-center">
                      <input type="hidden" name="processSecond" value="resultSecond">
                      <button type="submit" value="resultSecond" class=" btn btn-primary btn-lg">LANJUT</button>
                    </div>
                  </div>
                </div>
              </div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
			</div>
		</div>
	</div>
</div>

@endsection
