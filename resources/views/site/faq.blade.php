@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                             <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Frequently Asked Question (FAQ)</h1>
                            </header>
                                 <div class="item-company">
                                     <h2 class="item-company-title">FAQ List</h2>
                                 </div>
                                 <h4>Bagaimana cara saya memesan tiket lewat BUSTIKET.COM?</h4>
                                    <p class="lst-txt text-muted">Untuk cara pemesanan tiket, Anda dapat melihatnya pada halaman WWW.BUSTIKET.COM/howto.php. BUSTIKET juga dapat melayani penjualan melalui customer service untuk tiket dengan jadwal keberangkatan H-2, H-1 sebelum tanggal keberangkatan.</p>
                                 <hr>    
                                 <h4>Kenapa saya tidak bisa memesan tiket secara mendadak?</h4>
                    <p class="lst-txt text-muted">Untuk saat ini kami hanya menerima pemesanan tiket secara online sampai dengan 3 hari sebelum keberangkatan. Cara alternatifnya Anda bisa membeli tiket pada saat H-2, H-1 sebelum keberangkatan dengan menghubungi customer service kami di 0812-8000-3919atau layanan Live Chat di website WWW.BUSTIKET.COM.</p>
                    <hr>
                    <!--------------------- Q 3 ------------------->

                    <h4>Saya sudah memesan tiket lewat BUSTIKET.COM, bagaimana saya melakukan pembayaran?</h4>
                    <p class="lst-txt text-muted"> Untuk saat ini kami dapat menerima pembayaran melalui bank transfer, kartu kredit dan online banking.</p>
                    <hr>
                    <!--------------------- Q 4 ------------------->

                    <h4>Bagaimana jaminan keamanan untuk transaksi menggunakan kartu kredit?</h4>
                    <p class="lst-txt text-muted"> Semua transaksi kartu kredit dijamin aman dengan teknologi Secure Socket Layer (SSL) enskripsi data danjuga 3D Secure untuk fraud protection.</p>
                    <hr>
                    <!--------------------- Q 5 ------------------->

                    <h4>Jika belum menerima E-Tiket setelah melakukan pembayaran, apakah yang harus saya lakukan?</h4>
                    <p class="lst-txt text-muted"> Silahkan hubungi customer service kami dengan menelpon ke 0812-8000-3919 atau dengan layanan Live Chat di website kami, customer service kami siap untuk membantu Anda.</p>
                    <hr>
                    <!--------------------- Q 6 ------------------->

                    <h4>Apakah saya dapat memesan tiket untuk pihak lain?</h4>
                    <p class="lst-txt text-muted"> Anda dapat memesan tiket untuk pihak lain, karena kami membedakan data pemesan dan data penumpang.</p>
                    <hr>
                     <!--------------------- Q 7 ------------------->

                    <h4>Apakah tiket saya dapat digunakan oleh pihak lain?</h4>
                    <p class="lst-txt text-muted"> Tiket tidak dapat digunakanoleh pihak lain. Operator berhak menolak penumpang yang tidak tercantum dalam data manifest penumpang.</p>
                    <hr>
                     <!--------------------- Q 8 ------------------->

                    <h4>Apakah saya dapat membatalkan tiket jika saya tertinggal armada?</h4>
                    <p class="lst-txt text-muted"> Tiket tidak dapat dibatalkan jika penumpang yangtertinggal armadadikarenakan karena keterlambatan atau hal lainnya yang merupakan kelalaian penumpang,dan hal tersebut bukan merupakan tanggung jawab kami maupun penyedia jasa angkutan.</p>
                    <hr>
                    <!--------------------- Q 9 ------------------->

                    <h4>Bagaimana syarat pembatalan tiket di BUSTIKET?</h4>
                    <p class="lst-txt text-muted"> Berikut ini ketentuan pembatalan tiket:</p>
                            <li>Tiket tidak bisa dibatalkan untuk pemesanan pada hari libur nasional, long weekend, dan hari besar lainnya.</li>
                            <li>Pembatalan tiket oleh penumpang yang dilakukan sebelum 7 (tujuh) hari sebelum keberangkatan akan dikenakan biaya administrasi sebesar 50% (Lima puluh persen) dari harga tiket.</li>
                            <li>Pembatalan tiket oleh penumpang yang dilakukan antara 7 (enam) sampai dengan 3 (tiga) hari sebelum keberangkatan akan dikembalikan dalam bentuk Voucher (BUSTIKET Coupon) yang dapat digunakan pada transaksi anda berikutnya, baik pembayaran yang dilakukan melalui metode Bank Transfer /Kartu Kredit/Debit. Voucher ini berlaku 3 bulan sejak tanggal pembatalan. </li>
                            <li>Pembatalan tiket oleh penumpang yang dilakukan antara 24 jam sampai 3 hari sebelum waktu keberangkatan tidak dapat dikembalikan.</li>
                    <hr>
                    <!--------------------- Q 10 ------------------->

                    <h4>Bagaimana cara membatalkan tiket perjalanan saya?</h4>
                    <p class="lst-txt text-muted">Anda dapat klik “Cancel” pada halaman utama, lalu masukkan nomor tiket dan alamat email yang digunakan saat pemesanan. Diwajibkan untuk membaca “Informasi Penting” pada halaman pembatalan tiket. Setelah itu, tunggu konfirmasi dari pihak kami.</p>
                    <hr>
                    <!--------------------- Q 11 ------------------->

                    <h4>Berapa lama saya mendapatkan uang pengembalian tiket?</h4>
                    <p class="lst-txt text-muted">Kami akan memproses pembatalan tiketdan mengembalikan uang tiket kedalam rekening pemesan dalam jangka waktu 30 hari.</p>
                    <hr>
                    <!--------------------- Q 12 ------------------->

                    <h4>Apakah saya akan dikenakan biaya transfer untuk uang pengembalian tiket?</h4>
                    <p class="lst-txt text-muted">Jika rekening bank Anda berbeda dari rekening bank BUSTIKET maka biaya transfer bank ditanggung sepenuhnya oleh pemilik rekening penerima dan akan dipotong langsung dari jumlah uang pengembalian tiket. Jumlah uang pengembalian tiket sesuai total harga tiket tidak berikut biaya admin.</p>
                    <hr>
                    <!--------------------- Q 13 ------------------->

                    <h4>Bagaimana dengan uang pengembalian tiket yang pembayaran tiket dilakukan melalui kartu kredit?</h4>
                    <p class="lst-txt text-muted">Pembayaran tiket yang dilakukan melalui kartu kredit tidak dapat dikembalikan secara tunai. Pengembalian dana akan dilakukan dalam bentuk pengembalian limit kartu dan mengikuti syarat & ketentuan pihak penerbit kartu. Jumlah uang pengembalian tiket sesuai total harga tiket tidak berikut biaya admin.</p>
                    <hr>
                    <!--------------------- Q 14 ------------------->

                    <h4>Apakah saya bisa membatalkan proses pembatalan tiket?</h4>
                    <p class="lst-txt text-muted">Anda tidak dapat membatalkan kembali proses pembatalan tiket dengan alasan apapun.</p>
                    <hr>
                    <!--------------------- Q 15 ------------------->

                    <h4>Dapatkah saya menikmati 2 promo sekaligus untuk 1 pembelian tiket?</h4>
                    <p class="lst-txt text-muted">Promo kami umumnya tidak dapat digunakan bersamaan dengan promo lain.</p>
                     <hr>
                    <!--------------------- Q 16 ------------------->

                    <h4>Bagaimana cara mendapatkan informasi seputar diskon dan promo?</h4>
                    <p class="lst-txt text-muted">Anda dapat mengetahuisetiap promo dan diskon BUSTIKET dengan berlangganan newsletter kami.</p>
                    <hr>
                    <!--------------------- Q 17 ------------------->

                    <h4>Bagaimana cara menggunakan promo dari BUSTIKET?</h4>
                    <p class="lst-txt text-muted">Anda hanya perlu memasukkan kode voucher pada bagian pembayaran. System kami secara otomatis akan mendeteksi apakah promo masih berlaku atau tidak.</p>
                    <hr>
                    <!--------------------- Q 18 ------------------->

                    <h4>Apakah saya akan dikenakan biaya untuk berlangganan newsletter BUSTIKET?</h4>
                    <p class="lst-txt text-muted">Berlangganan Newsletter BUSTIKET tidak dikenakan biaya apapun.</p>
                    <hr>
                    <!--------------------- Q 19 ------------------->

                    <h4>Apakah tiket promo bisa dibatalkan?</h4>
                    <p class="lst-txt text-muted">Tiket promo dapat dibatalkan sesuai dengan ketentuan yang tercantum diatas.</p>
                    <hr>

                    </div>
                </div>
            </div>
        </div>
     </main>
@stop
