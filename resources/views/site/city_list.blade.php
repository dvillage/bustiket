@extends('layouts.site_layout')
@section('content')
<style>
    textarea:focus, input:focus{
    outline: none;
}
a{
    color: black;
}
</style>
<?php // dd($body[0]);?>
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/1.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

<!--        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Kota lanniya</h1>
                            </header>
                           <p class="form-field style-2 fa fa-search" style="float:right">
                                        <input type="text"  id="searchbox" placeholder="Cari Kota keberangkatan Anda" >
                           </p>
                            <div class="item-company-title">
                                    <h2>KOTA-KOTA KEBERANGKATAN TERPOPULER
                                    
                                    </h2>
                                
                            </div>
                           
                           <table class="table   table-responsive table-striped" id="cityList1">
                               <?php
                                
                               
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                                   
                               ?>
                               <?php
                                    echo "<tr>";
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        
                                    ?>
                               <td class="city" data-city="{{strtolower($city)}}"><i class="fa fa-map-marker " aria-hidden="true"></i><a href="{{URL::to('city-list-from\\'.$city)}}">Ticket bus dari {{$city}}</a></td>
                                    <?php
                                        if(($cnt%4)==0){
                                            echo "</tr><tr>";
                                        }
                                        $cnt++;
                                    }
                                    ?>                                                          
                           </table>  
                           <h2 class="item-company-title">KOTA-KOTA TUJUAN TERPOPULAR</h2>
                           <table class="table   table-responsive table-striped" id="cityList2">
                               <?php
                                    $cnt=1;
                                    $city=array();
                                    $citylist=$body[0];
                               ?>
                               <?php
                                    echo "<tr>";
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                        
                                    ?>
                                        <td class="city"  data-city="{{strtolower($city)}}"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{URL::to('city-list-from\\'.$city)}}">Ticket bus ke {{$city}}</a></td>
                                    <?php
                                        if(($cnt%4)==0){
                                            echo "</tr><tr>";
                                        }
                                        $cnt++;
                                    }
                                    ?>
                                                                
                           </table> 
                           <div id="sdata"></div>
                           <div class="jumbotron">
                               <p>
                                   BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus ke berbagai tujuan di wilayah Indonesia dalam satu portal situs booking online.
                                   Dirilis sejak awal 2016,
                                   BUSTIKET.COM memberikan layanan rute perjalanan ke kota besar maupun kota kecil di Pulau Jawa, mernyusul untuk tujuan ke Sumatera,
                                   Bali dan Kalimantan dalam waktu dekat.
                                   Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), 
                                   shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.
                                   COM menyediakan banyak informasi dalam hal jadwal keberangkatan semua rute bus perharinya,
                                   beragam pilihan bus, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini.
                                   Dengan situs booking online ini, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer,
                                   tiket sudah bisa Anda dapatkan.Karena kepuasan pelanggan adalah prioritas utama BUSTIKET.COM. 
                                   Bukan hanya dapat diakses melalui website, BUSTIKET.COM juga akan segera tersedia dalam bentuk aplikasi mobile untuk smartphone Anda. Dengan aplikasi terbaru dan fungsional, lakukan pemesanan tiket kapanpun dan dimanapun akan lebih mudah tentunya melalui BUSTIKET.COM

                               </p></div>
                        </article> / .article-single 

                    </div>
                </div>
            </div>
        </div> / .site-main-content site-main-content-offset -->
        <div class="site-main-content of-x">
            <div class="container container-1170">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single article-rute clearfix">
                            <header class="article-header">
                                <h1>Kota-kota keberangkatan terpopuler</h1>
                            </header>

                            <ul class="list-rute rute-lainnya kota-populer clearfix">
                                <?php
                                    $citylist=$body[0];
                                    for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                    ?>
                                    <li class="">
                                        <span class="sprite icon-placeholder-small"></span> <a href="{{URL::to('city-list-from\\'.$city)}}"> Tiket bus dari {{$city}} </a>
                                    </li>
                                <?php
                                    }
                                    ?>
                                

                            </ul>
                        </article><!-- / .article-single -->

                        <article class="article-single article-rute clearfix">
                            <header class="article-header">
                                <h1>Kota-kota tujuan terpopuler</h1>
                            </header>

                            <ul class="list-rute rute-lainnya clearfix">
                                <?php
                                for($i=0;$i<count($citylist);$i++)
                                    {    
                                        $city=$citylist[$i];
                                    ?>
                                    <li class="">
                                        <span class="sprite icon-placeholder-small"></span>  <a href="{{URL::to('city-list-from\\'.$city)}}">Tiket bus ke {{$city}} </a>
                                    </li>
                                <?php
                                    }
                                ?>
                                
                            </ul>

                        </article>

                    </div>
                </div>
            </div>
        </div>
        <script>
                $(function(){    
                        $("#searchbox").keyup(function(){
                            $(this).css({"border-left":"none"})
                                var sval=$(this).val();
                                if(sval==''){
                                    $(".city").each(function(){
                                        $(this).show(); 
                                    });
                                }
                                
                                console.log('sval='+sval);
                                $(".city").each(function(){
                                    var x=$(this).data('city');
                                    if(!x.includes(sval)){
                                         $(this).hide();
                                     }else{
                                         
                                     }
                                 });               
                        }); 
                });
        </script>
@stop
