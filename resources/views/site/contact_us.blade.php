@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Hubungi Kami</h1>
                            </header>

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 text-center hubungi-kami-cta">
                                    <p>Jika terdapat pertanyaan silakan hubungi kami di: 0812-8000-3919</p>
                                    <p>Atau email ke: info@bustiket.com</p>

                                    <div class="clearfix space space-30"></div>
                                    <div class="clearfix space space-30"></div>
                                </div>
                            </div>

                            <div class="form-wrapper row">
                                <div class="col-md-6">
                                    <form action="#" class="form-pembatalan form-hubungi-kami" id="frmContact" method="post">
                                        <p class="form-field style-2">
                                            <span class="form-field-label">Nama Lengkap</span>
                                            <input type="text" id="fullname" name="fullname" placeholder="Masukan Nomor Tiket">
                                        </p>

                                        <p class="form-field style-2">
                                            <span class="form-field-label">Alamat Email</span>
                                            <input type="email" id="email" name="email" placeholder="Masukan Alamat email">
                                        </p>

                                        <p class="form-field style-2">
                                            <span class="form-field-label">No Telp / No. Handphone</span>
                                            <input type="text"  id="contactNo" name="contactNo" placeholder="No Telp / No. Handphone">
                                        </p>

                                        <p class="form-field style-2">
                                            <span class="form-field-label">Pesan</span>
                                            <input type="text" id="message" name="message" placeholder="Tulis Pesan">
                                        </p>

                                        <p class="form-field form-submit style-2">
                                            <input type="submit" id="btnReg" value="Kirim" class="btn btn-green btn-radius" style="display:none" >
                                            <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                                        </p>
                                    </form>
                                    <p class="form-field form-submit style-2">
                                               <input type="button" id="btnSubmit" name=""  class="btn btn-green btn-radius" value="kirim Pesan">                
                                    </p>
                                    <div class="alert alert-success" id="onmail" style="display: none">
                                            <strong></strong> 
                                   </div>
                                </div>
                            </div>
                            
                        </article><!-- / .article-single -->


                       

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
             $(function(){                 
               $("#btnSubmit").click(function(){
                $("#btnReg").hide();
                $("#btnS").show();

                var flag=1;
                var phoneNo=$("#contactNo").val();
                
                function checkEmpty(param)
                {
                  for(x in param[0]){
                     var idn=param[0][x]['name'];
                     var idv=param[0][x]['value'];
                      if(idv==''){
                            $("#"+idn).focus();
                            $("#"+idn).css('border-color','red');
                            flag=0;
                        }else{
                            $("#"+idn).css('border-color','black');
                        }  
                    }
                }
                
                function validateEmail(sEmail) {   
                    var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
                    if (filter.test(sEmail)) {
                        return true;

                    }else {
                        return false;

                    }
                }
                    
                if(validateEmail($("#email").val())){
                           $("#email").css('border-color','black');
                }else{
                            
                          $("#email").focus();
                          $("#email").css('border-color','red');
                          flag=0;
                }
                
                
                    if(!phoneNo.match(/^\d+$/))
                    {
                        $("#phoneNo").focus();
                        $("#phoneNo").css('border-color','red');
                        flag=0;
                        alert('Telepon Tidak Harus Digandakan');
                    }
                    else if((phoneNo.length)<10)
                    {
                        $("#phoneNo").css('border-color','red');
                        alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                        flag=0;
                    }else{
                        $("#phoneNo").css('border-color','black');
                    }
                
                
                 
              checkEmpty([$("#frmContact").serializeArray()]);
              
              if($("#ques").val()==$("#answer").val()){
                   if(flag==1){
                       var token = $("[name=csrf-token]").attr("content");
                       var contactData=$("#frmContact").serializeArray(); 
                        $.ajax({
                           url:'{{URL::to("contact-us")}}',
                           type:'post',
                           dataType:'json',
                           data:{_token:token,contactData:contactData},
                           success:function(data){
                               var jdata=JSON.parse(JSON.stringify(data));
                               if(jdata.flag==1){
                                   $("#onmail").show();
                                   $("#onmail").text(jdata.msg);
                                   $("#frmContact")[0].reset();
                                   $("#btnSubmit").show();
                                   $("#btnS").hide();
                               }
                               else if(jdata.flag==0){
                                   
                                   $("#onmail").show();
                                   $("#onmail").text(jdata.msg);
                                   $("#btnSubmit").show();
                                   $("#btnS").hide();
                               }
                           }
                       });
                   }
                   else{
                       $(this).show();
                       $("#btnS").hide();
                   }
               }else{
                   alert('Verify you are human being');
                   $(this).show();
                       $("#btnS").hide();
               }
                });                
    });    
        </script>
@stop
