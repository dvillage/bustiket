@extends('layouts.pay_layout')
@section('content')
    
        <div class="site-main-content content-payment">

            <article class="article-content-payment done">
                <div class="container">


                    <div class="row">
                        <div class="col-md-12">
                           <p class="text-uppercase">{{$body['msg']}}</p>
                        </div>
                    </div>
                </div>
            </article><!-- / .article-content-payment -->

        </div><!-- / .site-main-content .content-payment -->
        <section class="section">
            <div class="container">
                <div class="row site-section-widget">
                    <div class="col-md-12">
                        <div class="widget widget-download">
                            <h3 class="widget-title">DOWNLOAD BUSTIKET APP</h3>

                            <figure class="aligncenter">
                                <a href="#">
                                    <span class="sprite icon-google-play-big"></span>
                                </a>
                            </figure>
                            <div class="clearfix"></div>

                            <ul class="list-socmed">
                                <li><a href="#"><span class="sprite icon-facebook-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-twitter-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-instagram-circle"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        
        
@endsection