
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<!DOCTYPE html>
<!-- saved from url=(0045)http://apidocjs.com/example/#api-User-PutUser -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>BUSTIKET | Seat seller API Documentation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  
  <?php
        if (isset($header['css']) && count($header['css'])>0)
            for ($i = 0; $i < count($header['css']); $i++)
                if (strpos($header['css'][$i], "http://") !== FALSE)
                    echo '<link rel="stylesheet" type="text/css" href="' .$header['css'][$i] . '"/>';
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . url("/")."/" .  $header['css'][$i] . '"/>';
                
                
                echo '<script type="text/javascript" src="'.URL::to('assets/js/app.min.js').'"></script>';
                
        if (isset($header['js']) && count($header['js'])>0)
            for ($i = 0; $i < count($header['js']); $i++)
            {
                if (strpos($header['js'][$i], "http://") !== FALSE)
                    echo '<script type="text/javascript" src="' . $header['js'][$i] . '"></script>';
                else
                    echo '<script type="text/javascript" src="' . url("/")."/" . $header['js'][$i] . '"></script>';
            }
        ?>
  
   <style>
  body {
      position: relative;
  }
  </style>
    </head>
<body data-spy="scroll" data-target="#scrollingNav" data-offset="20">
<!--    <div class="container">-->
<div class="container-fluid">
  <div class="row-fluid">
    <div id="sidenav" class="span2">
<nav id="scrollingNav">
  <ul class="nav sidenav nav-list">
          <li class="nav-fixed nav-header active" data-group="SeatSeller"><a href="#">SeatSeller (API Doc)</a></li>
          <li class="" data-group="SeatSeller" data-name="GetRequestToken" ><a href="#Get-Request-Token">Get Request Token</a></li>
          <li class="" data-group="SeatSeller" data-name="SeatSellerLogin" ><a href="#Seat-Seller-Login">Seat Seller Login</a></li>
          <li class="" data-group="SeatSeller" data-name="SeatSellerForgotPass" ><a href="#SeatSeller-Forget-Password">SeatSeller Forget Password</a></li>
          <li class="" data-group="SeatSeller" data-name="GetFromCity" ><a href="#Get-From-City-List">Get From City List</a></li>
          <li class="" data-group="SeatSeller" data-name="GetToCity" ><a href="#Get-To-City-List">Get To City List</a></li>
          <li class="" data-group="SeatSeller" data-name="SearchBus" ><a href="#SeatSeller-Search-Bus">SeatSeller Search Bus</a></li>
          <li class="" data-group="SeatSeller" data-name="ApplyCouponCode" ><a href="#Apply-Coupon-Code">Apply Coupon Code</a></li>
          <li class="" data-group="SeatSeller" data-name="ReserveTicketBooking" ><a href="#Reserve-Ticket-Booking">Reserve Ticket Booking</a></li>
          <li class="" data-group="SeatSeller" data-name="GetTicketByTransferCode" ><a href="#Get-Ticket-By-Transfer-Code">Get Ticket By Transfer Code</a></li>
          <li class="" data-group="SeatSeller" data-name="ApproveBooking" ><a href="#Approve-Booking">Approve Booking</a></li>
          <li class="" data-group="SeatSeller" data-name="DatewiseReport" ><a href="#Datewise-Report">Datewise Report</a></li>
          <li class="" data-group="SeatSeller" data-name="BookingCancel" ><a href="#Booking-Cancel">Booking Cancel</a></li>
          <li class="" data-group="SeatSeller" data-name="SeatCancel" ><a href="#Seat-Cancel">Seat Cancel</a></li>
          <li class="" data-group="SeatSeller" data-name="ProfileUpdate" ><a href="#Profile-Update">Profile Update</a></li>
          <li class="" data-group="SeatSeller" data-name="ChangePassword" ><a href="#Change-Password">Change Password</a></li>
  </ul>
  
</nav>
</div>
    <div id="content">
      <div id="project">
  <div class="pull-left">
      <img src="{{URL::to('assets/img/logo.png')}}" style="width: 200px"  />
  </div>
  <div class="clearfix"></div>
</div>
      <div id="header">
          <br>
          <br>
    <div id="api-_">
        <h1>Overview</h1>
    <p>
    <stron style="font-weight: bold">Production Base URL: </stron> <a href="https://www.bustiket.com/api/" target="__balnk">https://www.bustiket.com/api/</a>
    </p>
    <p>
    <stron style="font-weight: bold">Development Base URL: </stron> <a href="http://redesign.bustiket.com/api/" target="__balnk">http://redesign.bustiket.com/api/</a>
    </p>
    <p>
        <stron style="font-weight: bold">Response Type: </stron> JSON
    </p>
    <p>In every API call it need to pass either RequestToken or AuthToken except Get Request Token API call. And it should be pass in Header. AuthToken use for identification of user and it can be obtain by login API response. In every API it is neccessary to pass Accept and User-agent in Header. Example of API call already attached in this document for every API.</p>
    <p>There are four coman response flag which will be attached in every API response</p>
    <ul>
        <li>0 - Error which indidate operation fail</li>
        <li>1 - Success</li>
        <li>2 - Validation Error</li>
        <li>7 - Request token expired or invalid</li>
        <li>8 - Authtoken expired or invalid</li>
    </ul>
    <p>To view proper response json . Copy sample JSON data from this page and past it here <a href="http://json.parser.online.fr/" target="__blank">http://json.parser.online.fr/</a> </p>
</div>
</div>
      <div id="sections">
  <section id="">
    
      <div id="Get-Request-Token">
        
  <article id="" data-group="SeatSeller" data-name="GetRequestToken" data-version="0.3.0">
    <div class="pull-left">
      <h1>Get Request Token</h1>
    </div>
    <div class="clearfix"></div>

      <p></p><p>No Need to pass any header data. request_token in response will be use in next API call</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="get"><code><span class="pln">services/get-request-token</span></code></pre>

      
      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "services/get-request-token";
                $url = $base_url.$action;
                $headers = array();

                $fields = array();

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
    
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code"></td>
              <td>
                
              </td>
            <td>
            <p></p> 

            </td>
          </tr>
        </tbody>
      </table>
    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Object
              </td>
            <td>
            <p>{"request_token":"TzlbMNYxzkcWGCcS88IuUVzTVC4kptVX4pN38UMb"}</p> 
            </td>
          </tr>
        </tbody>
      </table>


      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <!--<a href="<?php echo URL::to('docs/')?>#error-examples-User-GetUser-0_3_0-0">Response (example):</a>-->
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-GetUser-0_3_0-0">
        <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">1,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"success",</span><span class="pln"></span>
            <span class="str">"data"</span><span class="pun">:</span><span class="pln"> </span><span class="str"><span class="pun">{</span>"request_token":"kwpxmgJrysa45ktjdg"<span class="pun">}</span></span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "success",
               "data": {
                  "request_token": "TzlbMNYxzkcWGCcS88IuUVzTVC4kptVX4pN38UMb"
               }
            }
        </code></pre>
        </div>
      </div>


  </article>

      </div>
      
      
      <div id="Seat-Seller-Login">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="SeatSellerLogin" data-version="0.3.0">
    <div class="pull-left">
      <h1>Seat Seller Login</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>User name and password for account will be provided from Admin. And it will different for production and development environment. One can not use same login and password for production and development. There is no registration API for seatseller account.
          If login success, Server return auth_token in this API response. It will use in blow some APIs.
      </p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/login</span></code></pre>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/login";
                $url = $base_url.$action;
                $RequestToken = "GET_REQUEST_TOKEN_FROM_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $RequestToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'agent_email' => 'test@email.com',
                    'agent_password' => 'dummypassword',
                    'device_token'=>'sdfsjhdfhsfhasghfsdfsdfj',
                    '_token'=>'XK6F2C0uoJuJGUoc5jZUQloluaqN5zNiOC0qCPYg'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
    

      @include('site.seatseller_api_header',['type' => 'request_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">agent_email</td>
              <td>
                String
              </td>
            <td>
            <p>registered email id</p> 
            </td>
          </tr>
          <tr>
            <td class="code">agent_password</td>
              <td>
                String
              </td>
            <td>
            <p>password of registered email</p> 
            </td>
          </tr>
          <tr>
            <td class="code">device_token</td>
              <td>
                String
              </td>
            <td>
            <p>Device token if login by facebook or google</p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Login Success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Object
              </td>
            <td>
            <p>{"agent_id":19,"agent_name":"Seat Seller-A","agent_email":"aa@gmail.com","agent_mobile":"+629979998798","agent_address":"","agent_city":"Surat","agent_commission":"1000.00","agent_joindate":"2017-09-28 11:48:00","agent_status":1,"agnt_amt":"0.00","agent_type":"A","tax":0,"paypal_email":"","auth_token":"vYQ9P5oefmwpy5G"}}</p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Account does not exists</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code class="prettyprint">
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Account does not exists"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            
            {
                "flag": 1,
                "msg": "Login Success",
                "data": {

                    {
                    "agent_id":19,
                    "agent_name":"Seat Seller-A",
                    "agent_email":"aa@gmail.com",
                    "agent_mobile":"+629979998798",
                    "agent_address":"",
                    "agent_city":"Surat",
                    "agent_commission":"1000.00",
                    "agent_joindate":"2017-09-28 11:48:00",
                    "agent_status":1,
                    "agnt_amt":"0.00",
                    "agent_type":"A",
                    "tax":0,
                    "paypal_email":"",
                    "auth_token":"vYQ9P5oefmwpy5G"
                }
             }
        }

                         
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="SeatSeller-Forget-Password">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="SeatSellerForgotPass" data-version="0.3.0">
    <div class="pull-left">
      <h1>SeatSeller Forget Password</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/forget-password</span></code></pre>

      
      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/forget-password";
                $url = $base_url.$action;
                $RequestToken = "GET_REQUEST_TOKEN_FROM_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $RequestToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'agent_email' => "test@email.com",
                    "_token"=>"XK6F2C0uoJuJGUoc5jZUQloluaqN5zNiOC0qCPYg",
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
    
        @include('site.seatseller_api_header',['type' => 'auth_token'])
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">agent_email</td>
              <td>
                String
              </td>
            <td>
            <p>registered email id</p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Password mail sent to your email address. Also check in spam folder</p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Email address not found in our record</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Email address not found in our record"</span><span class="pln"></span>
            <span class="pun">}</span>-->
                {
                    "flag": 1,
                    "msg": "Password mail sent to your email address. Also check in spam folder"
                 }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="Get-From-City-List">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="GetFromCity" data-version="0.3.0">
    <div class="pull-left">
      <h1>Get From City List</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>This API from city list based on key field.</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">services/get-from-city-list</span></code></pre>

      
       <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "services/get-from-city-list";
                $url = $base_url.$action;
                $RequestToken = "GET_REQUEST_TOKEN_FROM_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $RequestToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'key' => "jak"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
    
        @include('site.seatseller_api_header',['type' => 'request_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">key</td>
              <td>
                String
              </td>
            <td>
            <p> City name (Ex. jak) </p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
            <p>
                [
                    {
                      "city_name": "Jakarta",
                      "id": 417
                    },

                    {
                      "city_name": "Majalengka",
                      "id": 470
                    },
                    {
                      "city_name": "Purworejo",
                      "id": 448
                    }
                  ]
            </p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Something might wrong</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Something might wrong"</span><span class="pln"></span>
            <span class="pun">}</span>-->
                {
                    "flag": 1,
                    "msg": "success",
                    "data": [
                       {
                          "city_name": "Jakarta",
                          "id": 417
                       },
                       {
                          "city_name": "Majalengka",
                          "id": 470
                       },
                       {
                          "city_name": "Purworejo",
                          "id": 448
                       }
                    ]
                 }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="Get-To-City-List">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="GetToCity" data-version="0.3.0">
    <div class="pull-left">
      <h1>Get To City List</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>This Api search to city which is depend on from city API. It is necessary to pass from_city field</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">services/get-to-city-list</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "services/get-from-city-list";
                $url = $base_url.$action;
                $RequestToken = "GET_REQUEST_TOKEN_FROM_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $RequestToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'key' => "solo",
                    'from_city' => "jakarta"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'request_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">key</td>
              <td>
                String
              </td>
            <td>
            <p> City name (Ex. sol) </p> 
            </td>
          </tr>
          <tr>
            <td class="code">from_city</td>
              <td>
                String
              </td>
            <td>
            <p> City name (Ex. jakarta) which got from "Get From City List" </p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
            <p>[{"city_name":"Solo","id":418},{"city_name":"Wonosobo","id":480}]</p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Something might wrong</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Something might wrong"</span><span class="pln"></span>
            <span class="pun">}</span>-->
                {
                    "flag": 1,
                    "msg": "success",
                    "data": [
                       {
                          "city_name": "Solo",
                          "id": 418
                       },
                       {
                          "city_name": "Wonosobo",
                          "id": 480
                       }
                    ]
                 }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="SeatSeller-Search-Bus">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="SearchBus" data-version="0.3.0">
    <div class="pull-left">
      <h1>SeatSeller Search Bus</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>Search All Bus</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/search-bus</span></code></pre>

      
      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/search-bus";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'to_city' => 57,
                    'from_city' => 1,
                    'date' => "2017-10-20",
                    "operator_id"=>99,
                    "type_id"=>1,
                    "feature_id"=>6,
                    "nop" => 2
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
    
        @include('site.seatseller_api_header',['type' => 'auth_token'])
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">to_city</td>
              <td>
                Integer
              </td>
            <td>
            <p> City Id (Ex. 57) which got from "Get To City List"</p> 
            </td>
          </tr>
          <tr>
            <td class="code">from_city</td>
              <td>
                Integer
              </td>
            <td>
            <p> City Id (Ex. 1) which got from "Get From City List" </p> 
            </td>
          </tr>
          <tr>
            <td class="code">date</td>
              <td>
                date
              </td>
            <td>
            <p> date in yyyy-mm-dd format </p> 
            </td>
          </tr>
          <tr>
            <td class="code">operator_id</td>
              <td>
                String
              </td>
            <td>
            <p> 99,157 (Optional) </p> 
            </td>
          </tr>
          <tr>
            <td class="code">type_id</td>
              <td>
                String
              </td>
            <td>
            <p> 1,2 (Optional) </p> 
            </td>
          </tr>
          <tr>
            <td class="code">feature_id</td>
              <td>
                String
              </td>
            <td>
            <p> 6,7,8 (Optional) </p> 
            </td>
          </tr>
          <tr>
            <td class="code">nop</td>
              <td>
                Integer
              </td>
            <td>
            <p> 1,2,.. etc. (No. of Passenger) </p> 
            </td>
          </tr>
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
                <p>
 [
      {
	"bus":[
			{
				"Bus_id":399,
				"service_provider_name":"Pahala Kencana ",
				"luxitem":"Makan",
				"rute":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
				"parent_busname":"PK - VIP 40",
				"available_seats":30,
				"bus_fare":"135.000",
				"time":"18:00 - 03:45",
				"duration":"10 Jam",
				"routes":[
					{
						"r_id":39898,
						"r_route":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
						"r_from_id":687,
						"r_from_name":"TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR",
						"r_to_id":924,
						"r_to_name":"TERMINAL SAWANGAN",
						"r_board_time":"18:00",
						"r_drop_time":"03:45",
						"r_price":"135.000"
					},
					{
						"r_id":39899,
						"r_route":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL MENDOLO -Wonosobo",
						"r_from_id":687,
						"r_from_name":"TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR",
						"r_to_id":923,
						"r_to_name":"TERMINAL MENDOLO",
						"r_board_time":"18:00",
						"r_drop_time":"04:11",
						"r_price":"135.000"
					}
				]
			},
			{
				"Bus_id":403,
				"service_provider_name":"Pahala Kencana ",
				"luxitem":"Makan",
				"rute":"Jakarta-TERMINAL PULOGEBANG.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
				"parent_busname":"PK - Exe 32",
				"available_seats":32,
				"bus_fare":"185.000",
				"time":"18:00 - 03:33",
				"duration":"10 Jam",
				"routes":[
					{
						"r_id":40404,
						"r_route":"Jakarta-TERMINAL PULOGEBANG.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
						"r_from_id":584,
						"r_from_name":"TERMINAL PULOGEBANG.JAKARTA TIMUR",
						"r_to_id":924,
						"r_to_name":"TERMINAL SAWANGAN",
						"r_board_time":"18:00",
						"r_drop_time":"03:33",
						"r_price":"185.000"
					}
				]
			}
		],
		"from_id":1,
		"to_id":57,
		"filter":{
			"operator":[
				{
					"SP_name":"Pahala Kencana ",
					"SP_id":54
				}
			],
			"type":[
				{
					"typeID":"bus",
					"typeName":"Bus"
				},
				{
					"typeID":"shuttle",
					"typeName":"Shuttle"
				},
				{
					"typeID":"travel",
					"typeName":"Travel"
				}
			],
			"feature":[
				{
				"lux_id":2,
				"lux_name":"Wi-Fi"
				},
				{
				"lux_id":3,
				"lux_name":"Movie"
				},
				{
				"lux_id":4,
				"lux_name":"News Paper"
				},
				{
					"lux_id":6,
					"lux_name":"Water Bottle"
				}
			]
		}
	}
          </p>    
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Something might wrong</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                   
    {
	"flag":1,
	"msg":"success",
	"data":{
	"bus":[
			{
				"Bus_id":399,
				"service_provider_name":"Pahala Kencana ",
				"luxitem":"Makan",
				"rute":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
				"parent_busname":"PK - VIP 40",
				"available_seats":30,
				"bus_fare":"135.000",
				"time":"18:00 - 03:45",
				"duration":"10 Jam",
				"routes":[
					{
						"r_id":39898,
						"r_route":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
						"r_from_id":687,
						"r_from_name":"TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR",
						"r_to_id":924,
						"r_to_name":"TERMINAL SAWANGAN",
						"r_board_time":"18:00",
						"r_drop_time":"03:45",
						"r_price":"135.000"
					},
					{
						"r_id":39899,
						"r_route":"Jakarta-TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR -TERMINAL MENDOLO -Wonosobo",
						"r_from_id":687,
						"r_from_name":"TERMINAL KAMPUNG RAMBUTAN.JAKARTA TIMUR",
						"r_to_id":923,
						"r_to_name":"TERMINAL MENDOLO",
						"r_board_time":"18:00",
						"r_drop_time":"04:11",
						"r_price":"135.000"
					}
				]
			},
			{
				"Bus_id":403,
				"service_provider_name":"Pahala Kencana ",
				"luxitem":"Makan",
				"rute":"Jakarta-TERMINAL PULOGEBANG.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
				"parent_busname":"PK - Exe 32",
				"available_seats":32,
				"bus_fare":"185.000",
				"time":"18:00 - 03:33",
				"duration":"10 Jam",
				"routes":[
					{
						"r_id":40404,
						"r_route":"Jakarta-TERMINAL PULOGEBANG.JAKARTA TIMUR -TERMINAL SAWANGAN-Wonosobo",
						"r_from_id":584,
						"r_from_name":"TERMINAL PULOGEBANG.JAKARTA TIMUR",
						"r_to_id":924,
						"r_to_name":"TERMINAL SAWANGAN",
						"r_board_time":"18:00",
						"r_drop_time":"03:33",
						"r_price":"185.000"
					}
				]
			}
		],
		"from_id":1,
		"to_id":57,
		"filter":{
			"operator":[
				{
					"SP_name":"Pahala Kencana ",
					"SP_id":54
				}
			],
			"type":[
				{
					"typeID":"bus",
					"typeName":"Bus"
				},
				{
					"typeID":"shuttle",
					"typeName":"Shuttle"
				},
				{
					"typeID":"travel",
					"typeName":"Travel"
				}
			],
			"feature":[
				{
					"lux_id":2,
					"lux_name":"Wi-Fi"
				},
				{
					"lux_id":3,
					"lux_name":"Movie"
				},
				{
					"lux_id":4,
					"lux_name":"News Paper"
				},
				{
					"lux_id":6,
					"lux_name":"Water Bottle"
				}
			]
		}
	}
    }
        </code></pre>
        </div>
      </div>
    </article>
</div>
      
 
 <div id="Apply-Coupon-Code">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="ApplyCouponCode" data-version="0.3.0">
    <div class="pull-left">
      <h1>Apply Coupon Code</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>This API will not actually apply coupon code. this will just give estimation about final payable amount.</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">services/apply-coupon-code</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "services/apply-coupon-code";
                $url = $base_url.$action;
                $RequestToken = "GET_REQUEST_TOKEN_FROM_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $RequestToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'date' => "2017-05-23",
                    'bus_id' => "219",
                    'coupon_code' => "COUpenCode",
                    'total_seat_count' => "1",
                    'total_seat_payment' => "180000"
                    
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">date</td>
              <td>
                date
              </td>
            <td>
            <p> date in yyyy-mm-dd format </p> 
            </td>
          </tr>
          <tr>
            <td class="code">bus_id</td>
              <td>
                String
              </td>
            <td>
            <p> bus id which is got from SeatSeller Search Bus </p> 
            </td>
          </tr>
          <tr>
            <td class="code">coupon_code</td>
              <td>
                String
              </td>
            <td>
            <p> Coupon Code on which On which Discount available. </p> 
            </td>
          </tr>
          <tr>
            <td class="code">total_seat_count</td>
              <td>
                Integer
              </td>
            <td>
            <p> Number of seats Selected</p> 
            </td>
          </tr>
          <tr>
            <td class="code">total_seat_payment</td>
              <td>
                String
              </td>
            <td>
            <p> Total amount of selected seats.</p> 
            </td>
          </tr>
                    
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
                <p>
                     {
                        "total_discounted_price": "334.000",
                        "discount_amount": "36.000",
                        "discount_percent": "10"
                    }

                </p>
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Invalid Promocode</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Something might wrong"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "success",
               "data": {
                  "total_discounted_price": "334.000",
                  "discount_amount": "36.000",
                  "discount_percent": "10"
               }
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="Reserve-Ticket-Booking">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="ReserveTicketBooking" data-version="0.3.0">
    <div class="pull-left">
      <h1>Reserve Ticket Booking</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>This API will just reserve seat for few minutes (~ 50 minutes). After calling this API seatseller balance will not deduct. One must have to call approve ticket for final booking.</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/reserve-ticket-booking</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/reserve-ticket-booking";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                                
                                  "_token"          => "4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r",
                                  "date"            => "2017-10-10",
                                  "bus_id"          => 1,
                                  "coupon_code"     => "code5",
                                  "total_seats"     => 2,
                                  "from_id"         => 2,
                                  "to_id"           => 1,
                                  "from_term_id"    => 11,
                                  "to_term_id"      => 1,
                                  "user_firstname"  => "Seat Seller-A",
                                  "user_email"      => "aa@gmail.com",
                                  "user_mobileno"   => "84646446464343",
                                  "user_age"        => 29,
                                  "user_gender"     => "L",
                                  "boarding_time"   => "13:05",
                                  "departure_time"  => "14:10",   
                 'passenger' => array(

                                        array(
                                                "passenger_age"=>"29\",
                                                "passenger_name"=>"Seat Seller-A",
                                                "passenger_gender"=>"M",
                                                "passenger_mobile":"84646446464343",
                                                 
                                        ),
                                        array(
                                                "passenger_age"=>"29",
                                                "passenger_name"=>"Seat Seller-A",
                                                "passenger_gender"=>"M",
                                                "passenger_mobile"=>"84646446464343",
                                                
                                        )
                                   )
                    
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">date</td>
              <td>
                date
              </td>
            <td>
            <p> date in yyyy-mm-dd format </p> 
            </td>
          </tr>
          <tr>
            <td class="code">bus_id</td>
              <td>
                String
              </td>
            <td>
            <p> bus id which is got from SeatSeller Search Bus </p> 
            </td>
          </tr>
          <tr>
            <td class="code">coupon_code</td>
              <td>
                String
              </td>
            <td>
            <p> Coupon Code on which On which Discount available. </p> 
            </td>
          </tr>
          <tr>
            <td class="code">total_seats</td>
              <td>
                Integer
              </td>
            <td>
            <p> Number of seats Selected</p> 
            </td>
          </tr>
          <tr>
            <td class="code">from_id</td>
              <td>
                Integer
              </td>
            <td>
            <p> From City Id.</p> 
            </td>
          </tr>
          <tr>
            <td class="code">to_id</td>
              <td>
                Integer
              </td>
            <td>
            <p> To City Id.</p> 
            </td>
          </tr>
          <tr>
            <td class="code">from_term_id</td>
              <td>
                Integer
              </td>
            <td>
            <p> From Terminal Id.</p> 
            </td>
          </tr>
          <tr>
            <td class="code">to_term_id</td>
              <td>
                Integer
              </td>
            <td>
            <p> To Terminal Id.</p> 
            </td>
          </tr>
          <tr>
            <td class="code">user_firstname</td>
              <td>
                String
              </td>
            <td>
            <p> Booker First Name</p> 
            </td>
          </tr>
          <tr>
            <td class="code">user_email</td>
              <td>
                Email
              </td>
            <td>
            <p> Booker Valid Email Address</p> 
            </td>
          </tr>
          <tr>
            <td class="code">user_mobileno</td>
              <td>
                Integer
              </td>
            <td>
            <p> Booker Mobile Number</p> 
            </td>
          </tr>
          <tr>
            <td class="code">user_age</td>
              <td>
                Integer
              </td>
            <td>
            <p> Booker Age</p> 
            </td>
          </tr>
          <tr>
            <td class="code">user_gender</td>
              <td>
                String
              </td>
            <td>
                <p> Booker Gender which must be either <b>L </b> or <b>P </b></p> 
            </td>
          </tr>
          <tr>
            <td class="code">boarding_time</td>
              <td>
                String
              </td>
            <td>
                <p> Boarding Time Which is Got from Search Bus result 'r_board_time' of selected route</p> 
            </td>
          </tr>
          <tr>
            <td class="code">departure_time</td>
              <td>
                String
              </td>
            <td>
                <p> Departure Time Which is Got from Search Bus result 'r_drop_time' of selected route</p> 
            </td>
          </tr>
          <tr>
            <td class="code">passenger</td>
              <td>
                Array
              </td>
            <td>
                <p> Passenger detail array
                    like, <br>[{'firstname':'xyz','gender':'P','age':'20'},
                    {'firstname':'abc','gender':'L','age':'21'}]
                    <br>
                    Here Gender P for Male and L for Female.
                </p> 
            </td>
          </tr>
                    
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket Reservation Success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
                <p>
                    
                    
   {
    "ticket_id": "09TK29UR18U7R1",
    "pt_id": -1,
    "transfer_code": "SE166",
    "bank_detail": [
      {
        "id": 1,
        "bank_name": "Bank Central Asia (BCA)",
        "bank_short_name": "BCA",
        "bank_address": "KCP Blok A Cipete",
        "logo": "bca.png",
        "account_name": "PT.BUSTIKET Global Technology",
        "account_no": "218-1888-280"
      },
      {
        "id": 2,
        "bank_name": "Bank Mandiri",
        "bank_short_name": "Mandiri",
        "bank_address": "Pondok Indah Office Tower 3",
        "logo": "mandiri.png",
        "account_name": "CV. Mitra Indo Visi Group",
        "account_no": "1010007320813"
      }
    ],
    "cc_url": " http://redesign.bustiket.com/api/services/ticket-checkout/ALaC7ZJiTo7qqkivgdnKo5xVwJsNiMNj2OZsTrmxvmo",
    "payable_amount": "20.400"
  }

                    
                    
                </p>
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Passenger data missing</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Passenger data missing"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
                "flag": 1,
                "msg": "Ticket Reservation Success",
                "data": {
                  "ticket_id": "09TK29UR18U7R1",
                  "pt_id": -1,
                  "transfer_code": "SE166",
                  "bank_detail": [
                    {
                      "id": 1,
                      "bank_name": "Bank Central Asia (BCA)",
                      "bank_short_name": "BCA",
                      "bank_address": "KCP Blok A Cipete",
                      "logo": "bca.png",
                      "account_name": "PT.BUSTIKET Global Technology",
                      "account_no": "218-1888-280"
                    },
                    {
                      "id": 2,
                      "bank_name": "Bank Mandiri",
                      "bank_short_name": "Mandiri",
                      "bank_address": "Pondok Indah Office Tower 3",
                      "logo": "mandiri.png",
                      "account_name": "CV. Mitra Indo Visi Group",
                      "account_no": "1010007320813"
                    }
                  ],
                  "cc_url": " http://redesign.bustiket.com/api/services/ticket-checkout/ALaC7ZJiTo7qqkivgdnKo5xVwJsNiMNj2OZsTrmxvmo",
                  "payable_amount": "20.400"
                }
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="Get-Ticket-By-Transfer-Code">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="GetTicketByTransferCode" data-version="0.3.0">
    <div class="pull-left">
      <h1>Get Ticket By Transfer Code</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>Fetch full detail of reserve ticket</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/get-ticket-by-trn</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/get-ticket-by-trn";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    "_token"=>"4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r",
                    "transfer_code"=>"SE166"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

     @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">transfer_code</td>
              <td>
                String
              </td>
            <td>
            <p> transfer code got from reserve ticket booking. </p> 
            </td>
          </tr>
          
                    
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
                <p>
{
    "date": "2017-10-10",
    "date_formated": "Tue, 10 October, 2017",
    "user_name": "Seat Seller-A",
    "user_email": "aa@gmail.com",
    "user_mobile": "84646446464343",
    "service_provider_name": "test Kencana",
    "parent_bus_name": "A Travelus 2 4",
    "final_payable_amount": "20.400",
    "total_amt": "24.000",
    "discount_price": "6.000",
    "handling_fee": "2.400",
    "unit_handling_fee": "1.200",
    "time_from": "13:05 PM",
    "time_to": "14:10 PM",
    "boading_address": "Terminal jakarta",
    "boading_full_address": "Jakarta",
    "drop_address": "Koja Utara",
    "drop_full_address": "",
    "rute": "Jakarta-North Jakarta-Terminal jakarta-Koja Utara-South Jakarta-Jakarta",
    "passenger": [
      {
        "name": "Seat Seller-A",
        "seat": "D3",
        "gender": "F",
        "age": 29,
        "unique_id": "09TK29UR18U7R1"
      },
      {
        "name": "Seat Seller-A",
        "seat": "B3",
        "gender": "F",
        "age": 29,
        "unique_id": "09TK29UR18U7R1"
      }
    ],
    "from_city_id": 11,
    "to_city_id": 1,
    "from_city": "Jakarta",
    "to_city": "Jakarta",
    "ticket_id": "09TK29UR18U7R1",
    "layout": 0,
    "bus_name": "A Travelus 2 4",
    "coupon_code": "code5",
    "coupon_code_discount": "6.000"
  }
</p>
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Invalid code</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Invalid code"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
                  "flag": 1,
                  "msg": "success",
                  "data": {
                    "date": "2017-10-10",
                    "date_formated": "Tue, 10 October, 2017",
                    "user_name": "Seat Seller-A",
                    "user_email": "aa@gmail.com",
                    "user_mobile": "84646446464343",
                    "service_provider_name": "test Kencana",
                    "parent_bus_name": "A Travelus 2 4",
                    "final_payable_amount": "20.400",
                    "total_amt": "24.000",
                    "discount_price": "6.000",
                    "handling_fee": "2.400",
                    "unit_handling_fee": "1.200",
                    "time_from": "13:05 PM",
                    "time_to": "14:10 PM",
                    "boading_address": "Terminal jakarta",
                    "boading_full_address": "Jakarta",
                    "drop_address": "Koja Utara",
                    "drop_full_address": "",
                    "rute": "Jakarta-North Jakarta-Terminal jakarta-Koja Utara-South Jakarta-Jakarta",
                    "passenger": [
                      {
                        "name": "Seat Seller-A",
                        "seat": "D3",
                        "gender": "F",
                        "age": 29,
                        "unique_id": "09TK29UR18U7R1"
                      },
                      {
                        "name": "Seat Seller-A",
                        "seat": "B3",
                        "gender": "F",
                        "age": 29,
                        "unique_id": "09TK29UR18U7R1"
                      }
                    ],
                    "from_city_id": 11,
                    "to_city_id": 1,
                    "from_city": "Jakarta",
                    "to_city": "Jakarta",
                    "ticket_id": "09TK29UR18U7R1",
                    "layout": 0,
                    "bus_name": "A Travelus 2 4",
                    "coupon_code": "code5",
                    "coupon_code_discount": "6.000"
                  }
                }
        </code></pre>
        </div>
      </div>
    </article>
</div>
 <div id="Approve-Booking">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="ApproveBooking" data-version="0.3.0">
    <div class="pull-left">
      <h1>Approve Booking</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>Seatseller balance will deduct. Passenger will get email of ticket detail.</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/approve-ticket-by-trn</span></code></pre>

      
    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/approve-ticket-by-trn";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                   "_token"=>"4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r",
                    "transfer_code"=>"SE166"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">transfer_code</td>
              <td>
                String
              </td>
            <td>
            <p> transfer code got from reserve ticket booking. </p> 
            </td>
          </tr>
          
                    
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket Approved successfully</p> 
            </td>
          </tr>
          
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>no tickets found or ticket already approved</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"no tickets found or ticket already approved"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "Ticket Approved successfully"
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>
    <div id="Datewise-Report">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="DatewiseReport" data-version="0.3.0">
    <div class="pull-left">
      <h1>Datewise Report</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p>List of all tickets between two dates</p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/datewise-report</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/datewise-report";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    '_token'=> '4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r',
                    'from_date' => '2017-05-21',
                    'to_date' => '2017-05-23'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">from_date</td>
              <td>
                date
              </td>
            <td>
            <p> date in yyyy-mm-dd format </p> 
            </td>
          </tr>
          <tr>
            <td class="code">to_date</td>
              <td>
                date
              </td>
            <td>
            <p>  date in yyyy-mm-dd format </p> 
            </td>
          </tr>
          
                    
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>success</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
            <td>
                <p>{"ticket_count":6,"total_amount":"220.500"}</p>
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Account does not exists</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Account does not exists"</span><span class="pln"></span>
            <span class="pun">}</span>-->
                    {
                     "flag": 1,
                     "msg": "success",
                     "data": {
                                "ticket_count": 6,
                                "total_amount": "220.500"
                     }
                }
        </code></pre>
        </div>
      </div>
    </article>
</div>
    
    <div id="Booking-Cancel">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="BookingCancel" data-version="0.3.0">
    <div class="pull-left">
      <h1>Booking Cancel</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p></p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/ticket-cancel</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/ticket-cancel";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    "_token"=>"4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r",
                    'ticket_id' => "160521GMS0414638"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">ticket_id</td>
              <td>
                String
              </td>
            <td>
            <p> Ticket id generated from reserve ticket booking api. </p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket cancelled</p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket not found</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Ticket not found"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "Ticket cancelled"
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>

<div id="Seat-Cancel">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="SeatCancel" data-version="0.3.0">
    <div class="pull-left">
      <h1>Seat Cancel</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p></p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/ticket-cancel-by-unique-id</span></code></pre>

      

    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/ticket-cancel";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    "_token"=>"eEw3MhqXguGfnZrImtaZpAQO7RrcaIoTXV7q2ZOZ",
                    'ticket_id' => "09TK29SS318"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
    
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">ticket_id</td>
              <td>
                String
              </td>
            <td>
            <p> Ticket id generated from reserve ticket booking api. </p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket cancelled</p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Ticket not found</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Ticket not found"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "Ticket cancelled"
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>
      
<div id="Profile-Update">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="ProfileUpdate" data-version="0.3.0">
    <div class="pull-left">
      <h1>Profile Update</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p></p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/update-profile</span></code></pre>

      
    
      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/update-profile";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    "_token"=> "4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r",
                    'agent_name' => "xyz",
                    'agent_id' => "25",
                    'agent_mobile' => "1234567890"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>
        @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">agent_name</td>
              <td>
                String
              </td>
            <td>
            <p> Name of seat seller. </p> 
            </td>
          </tr>
          <tr>
            <td class="code">agent_id</td>
              <td>
                Integer
              </td>
            <td>
            <p> Agent ID got from login api. </p> 
            </td>
          </tr>
          <tr>
            <td class="code">agent_mobile</td>
              <td>
                Integer
              </td>
            <td>
            <p> Valid mobile number </p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Profile updated successful</p> 
            </td>
          </tr>
          <tr>
            <td class="code">data</td>
              <td>
                Array
              </td>
              <td><p>
  {
    "agent_id": 19,
    "agent_name": "SS A",
    "agent_email": "aa@gmail.com",
    "agent_mobile": "+626541230789",
    "agent_address": "",
    "agent_city": "Surat",
    "agent_commission": "1000.00",
    "agent_joindate": "2017-09-28 11:48:00",
    "agent_status": 1,
    "agnt_amt": "9876345710.00",
    "agent_type": "A",
    "tax": 0,
    "paypal_email": "",
    "auth_token": "Fo7c6Jh841n3gIT"
  }
                  </p>
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Update fail</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Update fail"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
                "flag": 1,
                "msg": "Profile updated successful",
                "data": {
                        "agent_id": 19,
                        "agent_name": "SS A",
                        "agent_email": "aa@gmail.com",
                        "agent_mobile": "+626541230789",
                        "agent_address": "",
                        "agent_city": "Surat",
                        "agent_commission": "1000.00",
                        "agent_joindate": "2017-09-28 11:48:00",
                        "agent_status": 1,
                        "agnt_amt": "9876345710.00",
                        "agent_type": "A",
                        "tax": 0,
                        "paypal_email": "",
                        "auth_token": "Fo7c6Jh841n3gIT"
                 }
          }
        </code></pre>
        </div>
      </div>
    </article>
</div>
    <div id="Change-Password">
        
  <article id="api-User-PostUser-0.3.0" data-group="SeatSeller" data-name="ChangePassword" data-version="0.3.0">
    <div class="pull-left">
      <h1>Change Password</h1>
    </div>
    
    <div class="clearfix"></div>

      <p></p><p></p> <p></p>

    <pre class="prettyprint language-html prettyprinted" data-type="post"><code><span class="pln">seatseller/change-password</span></code></pre>

      
    <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">PHP</a>
          </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="examples-User-GetUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
                $base_url = "http://redesign.bustiket.com/api/";
                $action = "seatseller/change-password";
                $url = $base_url.$action;
                $AuthToken = "GET_AUTH_TOKEN_FROM_LOGIN_API";
                $headers = array(
                    'X-CSRF-TOKEN:' . $AuthToken,
                    'Accept:application/json',
                    'User-Agent:android bustiket.com .' . time()
                );

                $fields = array(
                    'old_password' => "old password",
                    'new_password' => "new password",
                     "_token"=> "4sq5WDrrGAFCSunRPNviF45GAc3clHZxxHnbJV6r"
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
                curl_setopt( $ch, CURLOPT_COOKIEJAR, "cookie.txt" );
                curl_setopt( $ch, CURLOPT_COOKIEFILE, "cookie.txt" );
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
</code></pre>
        </div>
      </div>

        @include('site.seatseller_api_header',['type' => 'auth_token'])
      <h2>Post Parameter</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">old_password</td>
              <td>
                String
              </td>
            <td>
            <p> Old password </p> 
            </td>
          </tr>
          <tr>
            <td class="code">new_password</td>
              <td>
                String
              </td>
            <td>
            <p> New password </p> 
            </td>
          </tr>
          
        </tbody>
      </table>



    
      <h2>Success 200</h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>1</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Password changed successfully</p> 
            </td>
          </tr>
          
          
        </tbody>
      </table>



    
      <h2>Error </h2>
      <table>
        <thead>
          <tr>
            <th style="width: 30%">Field</th>
            <th style="width: 10%">Type</th>
            <th style="width: 70%">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="code">flag</td>
              <td>
                Integer
              </td>
            <td>
            <p>0</p> 
            </td>
          </tr>
          <tr>
            <td class="code">msg</td>
              <td>
                String
              </td>
            <td>
            <p>Invalid Old password</p> 
            </td>
          </tr>
          
        </tbody>
      </table>

      <ul class="nav nav-tabs nav-tabs-examples">
          <li class="active">
            <a href="#">Response (example):</a>
          </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="error-examples-User-PostUser-0_3_0-0">
            <pre class="prettyprint language-json prettyprinted" data-type="json"><code>
<!--            <span class="pun">{</span><span class="pln"></span>
            <span class="str">"flag"</span><span class="pun">:</span><span class="pln"> </span><span class="str">0,</span><span class="pln"></span>
            <span class="str">"msg"</span><span class="pun">:</span><span class="pln"> </span><span class="str">"Invalid Old password"</span><span class="pln"></span>
            <span class="pun">}</span>-->
            {
               "flag": 1,
               "msg": "Password changed successfully"
            }
        </code></pre>
        </div>
      </div>
    </article>
</div>
      
  </section>
</div>
      <div id="footer">
    
</div>
      <div id="generator">
      <div class="content">
        Generated and Maintain By <a href="http://www.bustiket.com/">Bustiket Team</a>
      </div>
</div>
    </div>
  </div>
</div>



<!--<script data-main="main.js" src="<?php echo URL::to('assets/doc/require.min.js')?>"></script>-->

    <!--</div>-->
</body></html>