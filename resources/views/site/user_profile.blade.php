@extends('layouts.site_layout')
@section('content')
 <main class="site-main">
     
    <section class="section section-hero-page hidden-995" style="background-image: url('{{URL::to('assets/images/heropages/1.jpg')}}') ;">  
     </section>
   
        <div class="site-main-content site-main-content-offset site-main-content-profile normal-offset-995">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single article-tabs article-login clearfix">
                            <ul class="list-tab" role="tablist">
                                <li class="active">
                                    <a href="#profil-info" role="tab" data-toggle="tab" aria-controls="profil-info">
                                        <span class="icon-wrapper">
                                            <span class="sprite icon-person"></span>    
                                        </span>
                                        <span>Profil</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tiket-saya" role="tab" data-toggle="tab" aria-controls="tiket-saya">
                                        <span class="icon-wrapper">
                                            <span class="sprite icon-ticket"></span>    
                                        </span>
                                        <span>Tiket Saya</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#booking" role="tab" data-toggle="tab" aria-controls="booking">
                                        <span class="icon-wrapper">
                                            <span class="sprite icon-cek-pemesanan"></span>    
                                        </span>
                                        <span>Cek Pemesanan</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#message" role="tab" data-toggle="tab" aria-controls="message">
                                        <span class="icon-wrapper">
                                            <span class="sprite icon-langganan-info"></span>    
                                        </span>
                                        <span>Langganan Info</span>
                                    </a>
                                </li>
                            </ul><!-- / .list-tab -->

<div class="tab-content tab-content-user">
    <div class="tab-pane fade in active" role="tabpanel" id="profil-info">
        <header class="clearfix">
            <h2 class="tab-content-user-title">Info</h2>
        </header>

        <form action="" class="form-tab-content-user" id="frmUser">
            <p class="form-field style-2 clearfix">
                <span class="form-field-label">Nama</span>
                <input type="text" class="form-field-one-in-eight" id="name" name="name" value="{{$body[0]['name']}}" placeholder="John Doe">
            </p>

            <p class="form-field style-2 clearfix">
                <span class="form-field-label">Telepon</span>
                <input type="tel" class="form-field-one-in-eight" id="phoneNo" name="phoneNo" value="{{$body[0]['mobile']}}" placeholder="62 812 345678">
            </p>

            <p class="form-field style-2 clearfix">
                <span class="form-field-label">Email</span>
                <input type="email" class="form-field-one-in-eight" id="email" name="email" value="{{$body[0]['email']}}" placeholder="johndoe@gmail.com">
            </p>

            <p class="form-field style-2 clearfix">
                <span class="form-field-label">Log in ID</span>
                <input type="text" class="form-field-one-in-eight" id="logId" name="logId" placeholder="johndoe.bustiket">
            </p>

            <p class="form-field style-2 clearfix">
                <span class="form-field-label">Kata Sandi <a  id="lnkChangePass" class="form-field-label-link">Ubah Kata Sandi</a>
                </span>
                <input type="password" class="form-field-one-in-eight" id="password" name="password" placeholder="John Doe">
            </p>
            <p class="form-field form-submit style-2">
                <input type="submit" id="btnReg" value="Kirim" class="btn btn-green btn-radius" style="display:none" >
                
                <div class="alert alert-success" id="onmail" style="display: none">
                <strong></strong> 
            </div>
            </p>
            
            <p class="form-field form-submit text-left">
                <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                <input type="button" id="btnSubmit" name=""  class="btn btn-green btn-radius" value="kirim Pesan">                
            </p>
    </form>
        
       

        
    </div><!-- / #profil-info -->

    <div class="tab-pane fade" role="tabpanel" id="tiket-saya">
        <header class="clearfix hidden-995">
            <div class="tab-content-symbol">
                <ul>
                    <li class="green">Perjalanan Sebelumnya</li>
                    <li class="orange">Perjalanan Mendatang</li>
                    <li class="red">Perjalanan Batal</li>
                </ul>
            </div>
            <h2 class="tab-content-user-title">Semua Perjalanan</h2>
        </header>

        <div class="table-responsive tab-content-user-table hidden-995">
            <table class="table">
               
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Tiket</th>
                        <th>Tanggal Perjalanan</th>
                        <th>Tanggal Booking</th>
                        <th>Jumlah Kursi</th>
                        <th>Asal-Tujuan</th>
                        <th>Jenis Pembayaran</th>
                    </tr>
                </thead>
                <tbody>
                
                     @for($i=0;$i< count($body[1]);$i++)
                      <?php 
                            if ($body[1][$i]['status']==2) {
                                $cl='red';
                            }else{
                                if($body[1][$i]['journey_date']< date('Y-m-d')){
                                    $cl='green'; 
                                }
                                else if($body[1][$i]['journey_date']> date('Y-m-d')){
                                    $cl='orange';
                                }
                            }
                            
                            switch($body[1][$i]['book_by']){
                                case 0:$bookby='User';
                                    break;
                                case 1:$bookby='Admin';
                                    break;
                                case 2:$bookby='Service Provider A';
                                    break;
                                case 3:$bookby='Service Provider A';
                                    break;
                                case 4:$bookby='Service Provider B';
                                    break;
                                case 5:$bookby='Seat seller';
                                    break;
                                case 6:$bookby='Seat seller A';
                                    break;
                                case 7:$bookby='Seat seller B';
                                    break;
                            }
                            
                     ?>
                            <tr class="{{$cl}}">
                                <td>{{$i+1}}</td>
                                <td>{{$body[1][$i]['ticket_id']}}</td>
                                <td>{{$body[1][$i]['journey_date']}}</td>
                                <td>{{$body[1][$i]['created_at']}}</td>
                                <td>{{$body[1][$i]['amount']}}</td>
                                <td>{{$body[1][$i]['from_city']}}-{{$body[1][0]['to_city']}}</td>
                                <td>{{$bookby}}</td>

                            </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div><!-- / #tiket-saya -->

    <div class="tab-pane fade" role="tabpanel" id="booking">
        <header class="clearfix">
            <h2 class="tab-content-user-title">Print/ SMS/ Email Tiket</h2>
        </header>

        <form action="" class="form-tab-content-user form-check-pemesanan" id="frmPrint">
            <p class="form-field-inline clearfix">
                <span class="form-field-label">Nomor Tiket</span>
                <input type="text" id="ticketNumber" name="ticketNumber" placeholder="Masukan No Tiket">
            </p>
            
            <p class="form-field-inline clearfix type-select" id="em" style="display: block;">
                <span class="form-field-label">Email</span>
                <input type="text"  id="emailp" name="emailp" placeholder="Masukan email">
            </p>

            <p class="form-field-inline clearfix type-select" id="mn">
                <span class="form-field-label">Nomer Telpon</span>
                <input type="text" id="phoneNop" name="phoneNop" placeholder="Masukan No Telpon">
            </p>
            
            <div class="form-field-inline clearfix">
                <div class="clearfix space space-30 hidden-995"></div>
                <span class="form-field-label extra">Silahkan Pilih</span>
                <fieldset class="select-type-print">
                    <div class="clearfix space space-15 hidden-995"></div>
                    <input type="radio"  id="cetak-tiket" name="ticketType" value="print">
                    <label for="cetak-tiket">Cetak Tiket</label>

                    <input type="radio" name="ticketType" id="dapatkan-tiket-melalui-sms" name="dapatkan-tiket-melalui-sms" value="sms">
                    <label for="dapatkan-tiket-melalui-sms">Dapatkan Tiket Melalui SMS</label>

                    <input type="radio" name="ticketType" id="dapatkan-tiket-melalui-email" name="dapatkan-tiket-melalui-email" value="emailr">
                    <label for="dapatkan-tiket-melalui-email">Dapatkan Tiket Melalui Email</label>
                </fieldset>
            </div>
            <p class="form-field-inline clearfix">
                <img  id="btnS2"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                <input type="button" id="btnPrint" class="btn btn-green btn-radius btn-minwidth" value="Print Tiket">
            </p>
        </form>
        <div class="alert alert-success" id="onmail2" style="display: none">
                    <strong></strong> 
        </div>
    </div><!-- / #booking -->

    <div class="tab-pane fade" role="tabpanel" id="message">
        <header class="clearfix">
            <h2 class="tab-content-user-title">Ubah Setting Info Promo</h2>
        </header>

        <form action="" class="form-tab-content-user form-check-pemesanan">
            <p class="form-field-inline">Email Info Promo dan Diskon Ekslusif</p>
            
            <span class="clearfix space space-15"></span>
            <p class="form-field-inline clearfix nospace-top checkbox-check">
                <input type="checkbox" name="" id="settingPromo" checked="checked">
                <label for="settingPromo">Ya! Kirim Saya Info Terbaru Tiket Promo dan Diskon Ekslusif</label>
            </p>

            <p class="form-field-inline clearfix form-submit-inline">
                <img  id="btnS1"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                <input type="button" id="btnSave" class="btn btn-yellow btn-radius btn-minwidth" value="Simpan">
                <a href="{{URL::to('')}}"  class="btn btn-yellow btn-radius btn-minwidth" >Batal</a>
            </p>
        </form>
            <div class="alert alert-success" id="onmail1" style="display: none">
                <strong></strong> 
            </div>
    </div><!-- / #message -->
    
</div><!-- / .tab-content tab-content-user -->
                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->




  <script>
             $(function(){
                 var flags=0;
                 var userid={{$body[0]['id']}};
                 $("input:radio[name=ticketType]").change(function(){
                        if(this.value=="sms"){
                            $("#mn").show();
                            $("#em").hide();
                            $("#btnPrint").val('kirim Pesan');
                            flags=1;
                        }else{                           
                            $("#em").show();
                            $("#mn").hide();
                            $("#btnPrint").val('kirim Email');
                            flags=2;
                        }
                    });
                    
                    $("#btnPrint").click(function(){
                       // $(this).hide();
                        $("#btnS2").show();
                        var ticketType=$("input:radio[name=ticketType]:checked").val();
                        var ticketNumber=$("#ticketNumber").val();
                        var email=$("#emailp").val();
                        var phoneNo=$("#phoneNop").val();
                        var flag=1;

                        function validateEmail(email) {   
                             var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
                             if (filter.test(email)) {
                                 return true;
                             }else {
                                 return false;
                             }
                         }
                         
                         function validateMobileNo(argMobile){
                                 var filter=/^\+\d{10,}$/;
                                 if(filter.test(argMobile)){
                                     return true;

                                 }else{
                                     return false;
                                 }
                         }
                             
                            if(ticketNumber==''){
                                     $("#ticketNumber").focus();
                                     $("#ticketNumber").css('border-color','red');
                                     flag=0;
                                 }else{
                                        $("#ticketNumber").css('border-color','black');
                            }

                            if(flags==1){  
                             
                                if(validateMobileNo(phoneNo)){
                                    $("#phoneNo").css('border-color','black'); 
                                }
                                else{
                                    $("#phoneNo").focus();
                                    $("#phoneNo").css('border-color','red');
                                    flag=0;
                                    alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                                } 
                          }

                          if(flags==2 || flags==0){

                              if(validateEmail($("#email").val())){
                                         $("#email").css('border-color','black');
                              }else{

                                        $("#email").focus();
                                        $("#email").css('border-color','red');
                                        flag=0;
                              }
                          }


                                 if(flag==1){
                                     var token = $("[name=csrf-token]").attr("content");
                                     postAjax('{{URL::to("user/check-booking")}}',{"ticketNumber":ticketNumber,"email":email,"phoneNo":phoneNo,"flags":flags},function(data){
                                            var jdata=JSON.parse(JSON.stringify(data));
                            
                                            if(jdata.flag==1){
                                                 $("#onmail2").show();
                                                 $("#onmail2").text(jdata.msg);
                                                 $("#onmail2").css({backgroundColor:'#d4edda'});
                                                 $("#frmPrint")[0].reset();
                                                 $("input:radio[name=ticketType]").trigger('change');
                                                 $("#btnPrint").show();
                                                 $("#btnS2").hide();
                                             }
                                             else if(jdata.flag==0){
                                                 $("#onmail2").show();
                                                 $("#onmail2").text(jdata.msg);
                                                 $("#onmail2").css({backgroundColor:'pink'});
                                                 $("#btnPrint").show();
                                                 $("#btnS2").hide();
                                             }else if(jdata.flag==3){
                                                 
                                                 var url = '{{URL::to("check-booking")}}?'+'ticket_id='+ticketNumber+'&email='+email;
                                                 window.location = url;
                                                 $("#btnPrint").show();
                                                 $("#btnS2").hide();
                                             }
                                        });
                                 }
                                 else{
                                     $("#btnPrint").show();
                                     $("#btnS2").hide();
                                 }
               
                              });
                    
                    
                    
                        var po={{$body[0]['promo_offer']}};                   
                        $("#settingPromo").prop("checked",value=po);
                        
                        
                        $("#btnSave").click(function(){
                        $("#btnSave").hide();
                        $("#btnS1").show();

                        var token = $("[name=csrf-token]").attr("content");
                        var userid={{$body[0]['id']}};
                        var chkval;
                        if($("#settingPromo").is(":checked"))
                        {
                            chkval=1;
                        }else{
                            chkval=0;
                        }

                        postAjax('{{URL::to("user/update-promo-status")}}',{chkval:chkval},function(data){
                                var jdata=JSON.parse(JSON.stringify(data));
                                if(jdata.flag==1){
                                    $("#onmail1").show();
                                    $("#onmail1").text(jdata.msg);
                                    $("#btnSave").show();
                                    $("#btnS1").hide();
                                }
                                else if(jdata.flag==0){
                                    $("#onmail1").show();
                                    $("#onmail1").text(jdata.msg);
                                    $("#btnSave").show();
                                    $("#btnS1").hide();
                                }
                            });

                    });  
                 
                        
                        $("#lnkChangePass").click(function(){
                            var token = $("[name=csrf-token]").attr("content");
                            var pass=$("#password").val();
                            if(pass.length<6 ){
                                alert("Password must greater than 6 character");
                            }else{
                                postAjax('{{URL::to("user/update-user-password")}}',{password:pass},function(data){
                                        var jdata=JSON.parse(JSON.stringify(data));
                                        if(jdata.flag==1){
                                            $("#onmail").show();
                                            $("#onmail").text(jdata.msg);
                                            $("#frmUser")[0].reset();
                                        }
                                        else if(jdata.flag==0){
                                            $("#offmail").show();
                                            $("#offmail").text(jdata.msg);
                                        }
                                    });
                            }    
                        });  
                        

                    $("#btnSubmit").click(function(){
                            $("#btnSubmit").hide();
                            $("#btnS").show();

                             var flag=1;
                             var phoneNo=$("#phoneNo").val();
                             var email=$("#email").val();
                             var name=$("#name").val();

                             if(name==''){
                                 $("#name").focus();
                                       $("#name").css('border-color','red');
                                       flag=0;
                                       
                             }else{            
                                       $("#name").css('border-color','black');
                             }

                            function validateEmail(argEmail) {   
                                 var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
                                 if (filter.test(argEmail)) {
                                     return true;

                                 }else {
                                     return false;
                                 }
                             }

                             if(validateEmail(email)){
                                        $("#email").css('border-color','black');
                             }else{            
                                       $("#email").focus();
                                       $("#email").css('border-color','red');
                                       flag=0;
                             }


                             function validateMobileNo(argMobile){
                                 var filter=/^\+\d{10,}$/;
                                 if(filter.test(argMobile)){
                                     return true;

                                 }else{
                                     return false;
                                 }
                             }


                             if(validateMobileNo(phoneNo)){
                                 $("#phoneNo").css('border-color','black'); 
                             }
                             else{
                                 $("#phoneNo").focus();
                                 $("#phoneNo").css('border-color','red');
                                 flag=0;
                                 alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                             }                  

                                if(flag==1){
                                    var token = $("[name=csrf-token]").attr("content");
                                    var userData=$("#frmUser").serializeArray();
                                    postAjax('{{URL::to("user/profile")}}',{userdata:userData},function(data){
                                        var jdata=JSON.parse(JSON.stringify(data));
                                        if(jdata.flag==1){
                                            $("#onmail").show();
                                            $("#onmail").text(jdata.msg);
                                            $("#frmUser")[0].reset();
                                             $("#btnSubmit").show();
                                             $("#btnS").hide();
                                        }
                                        else if(jdata.flag==0){
                                            $("#onmail").show();
                                            $("#onmail").text(jdata.msg);
                                            $("#btnSubmit").show();
                                            $("#btnS").hide();
                                        }
                                    });       
                        }else{
                            $("#btnSubmit").show();
                            $("#btnS").hide();

                        }
                    });                
    });    
        </script>
@stop
