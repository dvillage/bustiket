</main><!-- / .site-main .site-main-login -->
<?php if(isset($footer['flag']) && $footer['flag']==0){?>
        <footer class="site-footer">
            <div class="container">
                <div class="row site-footer-widget">
                    <div class="col-md-5">

                        <div class="widget widget-list widget-list-grid">
                            <h3 class="widget-title">BANTUAN</h3>    

                            <div class="row">
                                <div class="col-sm-6">
                                    <ul>
                                        <li><a href="#">Cara Pesan</a></li>
                                        <li><a href="#">Cek Pemesanan</a></li>
                                        <li><a href="#">Konfirmasi Pembayaran</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul>
                                        <li><a href="#">Cancel Tiket</a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Hubungi Kami</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- / .widget-list-grid -->

                    </div>
                
                    <div class="col-md-4">
                        <div class="widget widget-list">
                            <h3 class="widget-title">Hubungi Customer Service</h3>

                            <ul>
                                <li><span class="text-orange"><b>0812-8000-3919</b></span></li>
                                <li><a href="#" class="link-orange"><b>cs@bustiket.com</b></a></li>
                            </ul>
                        </div><!-- / .widget-list -->
                    </div>

                    <div class="col-md-3">
                        <div class="widget widget-download">
                            <h3 class="widget-title">DOWNLOAD BUSTIKET APP</h3>

                            <figure class="alignleft">
                                <a href="#">
                                    <span class="sprite icon-google-play-small"></span>
                                </a>
                            </figure>

                            <ul class="list-socmed">
                                <li><a href="#"><span class="sprite icon-facebook-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-twitter-circle"></span></a></li>
                                <li><a href="#"><span class="sprite icon-instagram-circle"></span></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-4 col-md-offset-4">
                        <div class="widget widget-text">
                            <div class="text-center">
                                <img src="assets/images/logo-footer-new.png" alt="">
                                
                                <div class="clearfix space space-30"></div>
                                <p>&copy; Copyright 2017 <a href="#">BUSTIKET.COM</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div><!-- / .site-footer-widget -->

                <div class="row site-footer-content">
                    <p class="copyright-proudly">
                        <span class="sprite icon-indonesia-big"></span>
                        Proudly Made In Indonesia
                    </p>
                </div>
            </div>
        </footer>

    <?php
}
        if (isset($footer['css'])  && count($footer['css']) > 0)
            for ($i = 0; $i < count($footer['css']); $i++)
                if (strpos($footer['css'][$i], "http://") !== FALSE)
                    echo '<link rel="stylesheet" type="text/css" href="' .$footer['css'][$i] . '"/>';
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . url('/')."/" .  $footer['css'][$i] . '"/>';
                
        if (isset($footer['js']) && count($footer['js']) > 0)
            for ($i = 0; $i < count($footer['js']); $i++)
            {
                if (strpos($footer['js'][$i], "http://") !== FALSE)
                    echo '<script type="text/javascript" src="' . $footer['js'][$i] . '"></script>';
                else
                    echo '<script type="text/javascript" src="' . url('/')."/" .  $footer['js'][$i] . '"></script>';
            }
        ?>
    <script type="text/javascript" src="{{URL::to('assets/js/scripts.js')}}"></script>
</body>
</html>