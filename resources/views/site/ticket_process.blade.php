@extends('layouts.pay_layout')
@section('content')
<?php 

// dd($body); 
$t = explode(':', $body['data']['time_limit']);
$tn = explode(':', $body['data']['time_now']);
$total = \General::number_format(($body['data']['total_amount']),3);

//dd($tn,$t);
?>

<script>
// Set the date we're counting down to

var d = new Date(<?php echo $t[0];?>,<?php echo $t[1]-1;?>,<?php echo $t[2];?>,<?php echo $t[3];?>,<?php echo $t[4]; ?>);
var s = new Date(<?php echo $tn[0];?>,<?php echo $tn[1]-1;?>,<?php echo $tn[2];?>,<?php echo $tn[3];?>,<?php echo $tn[4]; ?>);

var countDownDate = d.getTime() ;
var startTime = s.getTime() - new Date().getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time for jakarta timezone
    var d = new Date();
    var localTime = d.getTime();
    var localOffset = d.getTimezoneOffset() * 60000;
    var utc = localTime + localOffset;
    var offset = 7;   
    var jakarta = utc + (3600000*offset);
    var now = new Date(jakarta).getTime(); 
    
    
    // Get todays date and time
//    var now = new Date().getTime();
//    console.log(new Date());
//    var now = d1.getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - (now);
    
    // Time calculations for days, hours, minutes and seconds
    
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    $('.timer').html( pad( minutes ) + ":" + pad( seconds ) );
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        $('.timer').html("EXPIRED");
        alert("Your Session has been expired. You'll be redirected to Homepage.");
        window.location.href = '{{URL::to("")}}';
    }
}, 1000);

<?php if($body['bank']['bank_short_name'] == 'BNI'){ ?>
    $(document).ready(function(){
        chkBook();
        setInterval(function () {
            chkBook();
        },5*1000);
    });
    function chkBook(){
        var b_id = "{{$body['data']['booking_id']}}";
        var url=getBaseURL()+"payment/chk-book-confirm";
        var data={booking_id:b_id,};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                alert(res.msg);
                window.location.href = '{{URL::to("/")}}';
            }
        });
    }
<?php }?>

</script>
<?php if($body['param']['payment'] == 1){ ?>
    
        <div class="site-main-content content-payment">

            <article class="article-content-payment">
                <div class="container">
                    <header class="article-header">
                        <h1>Panduan Pengiriman dengan Bank Transfer</h1>
                    </header>

                    <div class="row">
                        <div class="col-xs-12 showin-990">
                            <div class="item-payment">
                                <div class="item-payment-content text-center">
                                    <h3 class="content-proses-title">Waktu Anda untuk melakukan pembayaran adalah</h3>
                            
                                    <span class="content-proses-counter-time timer" ></span>

                                    <p>Sampai jam {{$t[3]}}:{{$t[4]}}</p>
                                </div>
                            </div>   

                            <div class="clearfix space space-30"></div>                         
                        </div><!-- / .col-xs-12 -->


                        <div class="col-xs-7 showin-990">
                            <div class="item-payment">                                
                                <div class="item-payment-content content-transfer-bank">
                                    <h3 class="content-proses-title ">Please Transfer Ke:</h3>

                                    <ul class="list-content-proses-transfer">
                                        <li class="bank">
                                            {{$body['bank']['bank_name']}} 
                                            <span>
                                                <img src="{{URL::to('assets/images/bank')}}/{{$body['bank']['logo']}}" style="height:40px;width:80px;">
                                            </span>
                                        </li>
                                        <li>
                                            Nomor Rekening:
                                            <span>{{$body['bank']['account_no']}}</span>
                                        </li>
                                        <li>
                                            Nama Rekening:
                                            <span>{{$body['bank']['account_name']}}</span>
                                        </li>
                                        <li class="total">Jumlah Transfer <span>Rp {{$total}}</span></li>
                                    </ul>
<!--                                    <ul class="list-content-proses-transfer">
                                        <li class="bank">
                                            Bank Central Asia (BCA) <span><span class="sprite icon-bca-naked"></span></span>
                                        </li>
                                        <li>
                                            Nomor Rekening:
                                            <span>218--008--8809</span>
                                        </li>
                                        <li>
                                            Nama Rekening:
                                            <span>CV. Mitra Indo Visi Group</span>
                                        </li>
                                        <li class="total">Jumlah Transfer <span>Rp 800.000</span></li>
                                    </ul>-->
                                </div>
                            </div><!-- / .item-payment -->

                            <div class="item-payment">
                                <div class="item-payment-content  content-ket">
                                    <h3 class="content-proses-title">Please Transfer Ke:</h3>

                                    <ol class="list-flow-transfer list-number">
                                        <li>Mohon untuk verifikasi Rincian Perjalanan & Informasi pembelian tiket Anda.</li>
                                        <li>Pastikan Anda telah memasukkan detil rekening bank pengirim pembayaran dengan benar dan akurat. <br>Kami akan gunakan ini untuk identifikasi pembayaran yang masuk di rekening kami.</li>
                                        <li>Mohon transfer sesuai harga total yang tertera di "Detail Pemesanan"</li>
                                        <li>Batas pembayaran adalah 50 menit.</li>
                                        <li>Setelah pembayaran dilakukan silakan kunjungi halaman "Konfirmasi Pembayaran".</li>
                                        <li>Masukkan data mengenai detail rekening pengirim transfer:
                                        <ol class="list-alpa">
                                            <li>Kode Transfer</li>
                                            <li>Nama Bank</li>
                                            <li>Nama Pemilik Rekening</li>
                                            <li>Jumlah Yang Ditransfer</li>
                                        </ol>
                                        </li>
                                        <li>Klik tombol submit.</li>
                                        <li>Kami akan lakukan verifikasi manual dan akan mengirim e-tiket ke email Anda selambat-lambatnya dalam 60 menit.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="clearfix space space-30"></div>
                        </div>

                        <div class="col-xs-5 showin-990">
                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <ul class="list-content-proses-data">
                                        <li>Booking ID <span class="block-content"><b>{{$body['data']['booking_id']}}</b></span></li>
                                        <li>Detail Perjalanan <span class="block-content"><b>{{$body['param']['b_city']}} - {{$body['param']['d_city']}}</b></span></li>

                                        <li class="space-top">
                                            <span class="block-content">{{$body['data']['indo_p_date']}}</span> 
                                            <!--<span class="block-content">{{$body['data']['indo_p_time']}}</span>-->
                                            <span class="block-content">-</span>
                                            <span class="block-content">{{$body['data']['indo_d_date']}}</span>
                                            <!--<span class="block-content">{{$body['data']['indo_d_time']}}</span>-->
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <ul class="list-content-proses-data">
                                        <?php for($i=0;$i<$body['data']['nos'];$i++){ ?>
                                            <li>Daftar Penumpang <span class="block-content"><b>{{$i+1}}. {{$body['data']['pass_data']['p_name'][$i]}} (<?php echo $body['data']['pass_data']['p_gen'][$i] == 'm' ? 'Pria':'Wanita' ?>)</b></span></li>
                                            <li>Nomor Kursi <span class="block-content"><b>{{$body['data']['pass_data']['s_lbl'][$i]}}</b></span></li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>

                            <div class="clearfix space space-30"></div>
                        </div>

                        <div class="col-xs-12 showin-990">
                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <h3 class="content-proses-title text-left">Setelah melakukan pembayaran kami akan mengirimkan e-tiket ke email Anda Selambat-lambatnya dalam 60 menit.</h3>
                                    <p class="text-center"><a href="{{url("payment-confirmation")}}" class="link-green-dark"><b>Saya Telah Melakukan Pembayaran</b></a></p>
                                </div>
                            </div>                            

                            <div class="clearfix space space-30"></div>
                        </div>

                        <div class="col-md-8 hidden-990">
                            
                            <div class="item-payment">
                                <div class="item-payment-content text-center">
                                    <h3 class="content-proses-title">Waktu Anda untuk melakukan pembayaran adalah</h3>

                                    <span class="content-proses-counter-time timer"></span>

                                    <p>Sampai jam {{$t[3]}}:{{$t[4]}}</p>
                                </div>
                            </div>

                            <div class="item-payment">
                                <div class="item-payment-content content-transfer-bank">
                                    <h3 class="content-proses-title ">Please Transfer Ke:</h3>

                                    <ul class="list-content-proses-transfer">
                                        <li class="bank">
                                            {{$body['bank']['bank_name']}} 
                                                <span>
                                                    <img src="{{URL::to('assets/images/bank')}}/{{$body['bank']['logo']}}" style="height:40px;width:80px;">
                                                </span>
                                        </li>
                                        <li>
                                            Nomor Rekening:
                                            <span>{{$body['bank']['account_no']}}</span>
                                        </li>
                                        <li>
                                            Nama Rekening:
                                            <span>{{$body['bank']['account_name']}}</span>
                                        </li>
                                        <li class="total">Jumlah Transfer <span>Rp {{$total}}</span></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="item-payment">
                                <div class="item-payment-content  content-ket">
                                    <h3 class="content-proses-title">Please Transfer Ke:</h3>

                                    <ol class="list-flow-transfer list-number">
                                        <li>Mohon untuk verifikasi Rincian Perjalanan & Informasi pembelian tiket Anda.</li>
                                        <li>Pastikan Anda telah memasukkan detil rekening bank pengirim pembayaran dengan benar dan akurat. <br>Kami akan gunakan ini untuk identifikasi pembayaran yang masuk di rekening kami.</li>
                                        <li>Mohon transfer sesuai harga total yang tertera di "Detail Pemesanan"</li>
                                        <li>Batas pembayaran adalah 50 menit.</li>
                                        <li>Setelah pembayaran dilakukan silakan kunjungi halaman "Konfirmasi Pembayaran".</li>
                                        <li>Masukkan data mengenai detail rekening pengirim transfer:
                                        <ol class="list-alpa">
                                            <li>Kode Transfer</li>
                                            <li>Nama Bank</li>
                                            <li>Nama Pemilik Rekening</li>
                                            <li>Jumlah Yang Ditransfer</li>
                                        </ol>
                                        </li>
                                        <li>Klik tombol submit.</li>
                                        <li>Kami akan lakukan verifikasi manual dan akan mengirim e-tiket ke email Anda selambat-lambatnya dalam 60 menit.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <h3 class="content-proses-title text-left">Setelah melakukan pembayaran kami akan mengirimkan e-tiket ke email Anda Selambat-lambatnya dalam 60 menit.</h3>
                                    <p class="text-center"><a href="{{url("payment-confirmation")}}" class="link-green-dark"><b>Saya Telah Melakukan Pembayaran</b></a></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 hidden-990">
                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <ul class="list-content-proses-data">
                                        <li>Booking ID <span class="block-content"><b>{{$body['data']['booking_id']}}</b></span></li>
                                        <li>Detail Perjalanan <span class="block-content"><b>{{$body['param']['b_city']}} - {{$body['param']['d_city']}}</b></span></li>

                                        <li class="space-top">
                                            <span class="block-content">{{$body['data']['indo_p_date']}}</span> 
                                            <!--<span class="block-content">{{$body['data']['indo_p_time']}}</span>-->
                                            <span class="block-content">-</span>
                                            <span class="block-content">{{$body['data']['indo_d_date']}}</span>
                                            <!--<span class="block-content">{{$body['data']['indo_d_time']}}</span>-->
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="item-payment">
                                <div class="item-payment-content ">
                                    <ul class="list-content-proses-data">
                                        <?php for($i=0;$i<$body['data']['nos'];$i++){ ?>
                                            <li>Daftar Penumpang <span class="block-content"><b>{{$i+1}}. {{$body['data']['pass_data']['p_name'][$i]}} (<?php echo $body['data']['pass_data']['p_gen'][$i] == 'm' ? 'Pria':'Wanita' ?>)</b></span></li>
                                            <li>Nomor Kursi <span class="block-content"><b>{{ $body['data']['pass_data']['s_lbl'][$i] != '' ? $body['data']['pass_data']['s_lbl'][$i] : 'Saat Keberangkatan' }}</b></span></li>
                                        <?php } ?>
<!--                                        <li class="space-top">Daftar Penumpang <span class="block-content"><b>2. Jane Rose (Wanita)</b></span></li>
                                        <li>Nomor Kursi <span class="block-content"><b>7B / Eksekutif</b></span></li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="clearfix space space-60"></div>
                            <div class="clearfix space space-30"></div>
                        </div>
                    </div>
                </div>
            </article><!-- / .article-content-payment -->

        </div><!-- / .site-main-content .content-payment -->


<?php }

//else if($_REQUEST['payment'] == 20 || $_REQUEST['payment'] == 60)
//        {
//      veritranse :
//            $gross_amount = mysql_real_escape_string($_SESSION['book_var']['gross_amount_with_handelling_fee']);
//            require_once('one-veritrans-php-master/Veritrans.php');
//            Veritrans_Config::$isProduction = is_production;
////            Veritrans_Config::$serverKey = veritranse_serverkey_live;
//            Veritrans_Config::$serverKey = veritranse_serverkey;
//            
//            $order_id = 'tik_'.$ticket_id;
//            $params                      = array(
//                    'transaction_details' => array(
//                            'order_id' => $order_id,
//                            'gross_amount' => $gross_amount
//                    )
//            );
//
//            try {
//                    // Redirect to Veritrans VTWeb page
//                    write_log('veritranse.log', 'veritranse payment page called ');
//                    header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($params));
//            }
//            catch (Exception $e) {
//                    echo $e->getMessage();
//                    if (strpos($e->getMessage(), "Access denied due to unauthorized")) {
//                            echo "<code>";
//                            echo "<h4>Please set real server key from sandbox</h4>";
//                            echo "In file: " . __FILE__;
//                            echo "<br>";
//                            echo "<br>";
//                            echo htmlspecialchars('Veritrans_Config::$serverKey = \'VT-server-jCWUNdJT7gEldUWsX7lO7PjR\';');
//                            die();
//                    }
//            }
//        }

else{ 
    if($_REQUEST['payment']==2){
        $paymentchannle=$_REQUEST['paymentchannelc'];
    } else if($_REQUEST['payment']==5){
        $paymentchannle=$_REQUEST['paymentchanneld'];
    } else {
        $paymentchannle=$_REQUEST['paymentchannel'];
    }
   // $gross_amount = $bus_fare;
//    $gross_amount = mysql_real_escape_string($_SESSION['book_var']['gross_amount_with_handelling_fee']);
    $TRANSIDMERCHANT='tik_'.$body['data']['booking_id'];   

//    if($paymentchannle == '60' || $paymentchannle == '20'){
//        goto  veritranse;
//    }
//    dd(config('constant.CURRENCY'));
    
    ?>
    <form method="post" id="dokuwallet_form"       action="<?php echo env('doku_url').'Receive';?>">
        <input type="hidden" name="MALLID"          value="<?php echo env('MALLID');?>"> 
        <input type="hidden" name="CHAINMERCHANT"   value="<?php echo env('CHAINMERCHANT');?>"> 
        <input type="hidden" name="AMOUNT"          value="<?php echo $AMOUNT=$body['data']['total_amount'].'.00';?>"> 
        <input type="hidden" name="PURCHASEAMOUNT"  value="<?php echo $AMOUNT;?>"> 
        <input type="hidden" name="TRANSIDMERCHANT" value="<?php echo $TRANSIDMERCHANT;?>"> 
        <input type="hidden" name="WORDS"           value="<?php echo sha1($AMOUNT.env('MALLID').env('SHAREDKEY').$TRANSIDMERCHANT);?>"> 
        <input type="hidden" name="REQUESTDATETIME" value="<?php echo date('Ymdhis');?>"> 
        <input type="hidden" name="CURRENCY"        value="<?php echo env('CURRENCY');?>"> 
       <input type="hidden" name="PURCHASECURRENCY" value="<?php echo env('CURRENCY');?>"> 
        <input type="hidden" name="SESSIONID"       value="<?php echo $body['param']['sessionid'];?>"> 
        <input type="hidden" name="NAME"            value="<?php echo $body['data']['booker_name'];?>"> 
        <input type="hidden" name="EMAIL"           value="<?php echo $body['data']['booker_email'];?>"> 
        <input type="hidden" name="BASKET"          value="<?php echo $TRANSIDMERCHANT.','.$AMOUNT.',1,'.$AMOUNT;?>"> 
        <input type="hidden" name="PAYMENTCHANNEL"  value="<?php echo $paymentchannle; ?>"> 
    </form>
    <script>
            $(document).ready(function(){
//                    console.log($('#dokuwallet_form').serialize());
               $("#dokuwallet_form").submit(); 
            });
    </script>
<?php }?>
        

@endsection