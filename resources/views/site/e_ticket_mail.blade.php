<?php
$dropDate = date("j F Y,H:i", strtotime($data['dropping_date']));
$boardDate = date("j F Y,H:i", strtotime($data['pickup_date']));

$dropDate = \App\Models\General::getIndoMonths($dropDate);
$boardDate = \App\Models\General::getIndoMonths($boardDate);

$formated_hf = $data['handling_fee'] ? \General::number_format($data['handling_fee'],3) : 0;
$formated_dis = $data['coupon_discount'] ? \General::number_format($data['coupon_discount'],3) : 0;
//print_r($data['sp']['avatar']);
$avtar = $data['sp']['avatar'];
if($avtar){
    $filepath = config('constant.UPLOAD_SP_LOGO_DIR_PATH');
    if(file_exists($filepath.$avtar)){
        $avtar = URL::to('assets/images/sp').'/'.$avtar;
    }else{
        $avtar = URL::to('assets/images/paha.png');
    }
}else{
    $avtar = URL::to('assets/images/paha.png');
}
$spLogo = $avtar;
$busName = $data['bus']['name'];
$Booker_phone = $data['booker_mo'];
$layout = $data['bus']['layout'];
$route_code = '';
if(isset($data['route_code'])){
    $route_code = $data['route_code'];
}
?>
<style>
    @page { margin: 0px; }
    .header{
        position: relative; left: 0px; top: -50px; right: 0px;border: 1px solid; 
    }
     .footer { position: fixed; left: 0px; bottom: -180px; right: 0px;  }
     .page-break {
        page-break-after: always;
    }

</style>

    <div class="container">
        
        <div class="row" >
            
            <div class="col-lg-12 col-lg-12" style="width:100%;">
                <div class="" style="background-color:#3e9143;width: 850px;height: 17px;margin-left: -45px;">
                    <img class=" col-lg-12 col-lg-12"  src="{{URL::to('assets/images/green_line.png')}}" width="100%" height="17" style="" alt=""/>
                </div>
                <div style="margin-top:2px;width: 860px;position: relative;left: -45px;">
                        <img class=" col-lg-12 col-lg-12"  src="{{URL::to('assets/images/topLine.png')}}" width="100%" height="13"  alt=""/>
                </div> 
            </div>
            <!--<div class="">-->
                
            <!--</div>-->
            <div class="col-lg-12 col-lg-12" align="center" >
                <img src="{{URL::to('assets/images/logo-busticket.jpg')}}" style="padding-top: 15px;padding-bottom: 15px;" width="130" height="80" alt=" Bus Ticket"/>
            </div>
        </div>
        <div id="content_div" style="">
        <div class="row"   style="text-align: center;">
            
            <div class='col-lg-12 col-md-12'>
                <table align="center" style="border: 1px;border-color: gray;border-color: #ccc;border-style: solid;width: 650px;">
                    <tr>
                        <td colspan="3" style='border-bottom: 1px solid #ccc;text-align: center;'>
                            <div style="text-align:center;font-size:18px; color:#414042; 
                                  font-family:Gotham,'Helvetica Neue',  Helvetica, Arial, sans-serif;">
                                Booking ID 
                            </div>
                            <div style="font-size:17px;text-align:center;font-weight:bold; color:#3e9143; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif;"> <?php echo  $data['booking_id'] ?> </div> 
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <div style="font-size:12px;color:rgb(102,102,102);font-family: 'Arial';margin-top: 5px;">Berangkat</div>
                            <h3 style="font-weight:bold;color:#000;font-size: 20px;text-transform:uppercase;"><?php echo strtoupper($data['from_terminal']['loc_citi']['name']) ?></h3>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';" ><?php echo  $data['from_terminal']['name'] ?></p>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';" ><?php echo  $data['from_terminal']['name'].','.$data['from_terminal']['loc_citi']['name'].','.$data['from_terminal']['loc_district']['name'] ?></p>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';margin-bottom: 15px;" > <?php echo  $boardDate ?></p>
                        </td>
                        <td style=""><img src="{{URL::to('assets/images/right-arrow.png')}}" width="17px" height="35px"></td>
                        <td style="text-align:center;">
                            <div style="font-size:12px;color:rgb(102,102,102);font-family: 'Arial';margin-top: 5px;">Tiba</div>
                            <h3 style="font-weight:bold;color:#000;font-size: 20px;text-transform:uppercase;"><?php echo  strtoupper($data['to_terminal']['loc_citi']['name']) ?></h3>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';" ><?php echo  $data['to_terminal']['name'] ?></p>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';" ><?php echo  $data['to_terminal']['name'].','.$data['to_terminal']['loc_citi']['name'].','.$data['to_terminal']['loc_district']['name'] ?></p>
                            <p style="height:auto; color:rgb(102,102,102);font-size:12px; margin:0;font-family: 'Arial';margin-bottom: 15px;" > <?php echo  $dropDate ?></p>
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>
        
        <div class="row" style="padding-top:15px;width: 690px;padding-left: 65px;">
            <div style="border:1px solid #ccc;border-radius:10px;">
                <div style="color:#625c5c;border-bottom:1px solid #ccc;text-align:center;font-size:20px;padding:6px;font-weight:bold;">Rincian Pembayaran</div>
                <table style="width: 100%;padding: 5px;">
                    <tr><td style="float:left;"><b><p style="font-size:15px;margin: 0px;font-weight:bold;">Harga Tiket :</p></b></td><td></td><td></td></tr>
                    <tr>
                        <td style="font-size:20px;">
                            <p style="font-size:16px;margin: 0px;">Total Tiket
                            <span style="font-size:15px;">(<span><?php echo  $data['nos'] ?></span> x <span style="color:#390;font-weight:bold;font-size:16px;">Rp. <?php echo  \General::number_format($data['booking_seats'][0]['amount'],3) ?> </span>)</span>
                        </td>
                        <td style="">
                            <span style="font-size:15px;float: right;">Rp. </span>
                        </td>
                        <td style="text-align: right;width: 20%;">
                            <span style="font-size:15px;text-align: right;padding-right: 15px;"> <?php echo  \General::number_format($data['booking_seats'][0]['amount']*$data['nos'],3) ?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="">
                            <span style="font-size:15px;">Biaya Layanan</span>
                        </td>
                        <td style="">
                            <span style="font-size:15px;float: right;">Rp. </span>
                        </td>
                        <td style="text-align: right;">
                            <span style="font-size:15px;text-align: right;padding-right: 15px;"> <?php echo $formated_hf ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="">
                            <span style="font-size:15px;">Diskon</span>
                        </td>
                        <td style="">
                            <span style="font-size:15px;float: right;">Rp. </span>
                        </td>
                        <td style="text-align: right;">
                            <span style="font-size:15px;text-align: right;padding-right: 15px;"> <?php echo $formated_dis ?></span>
                        </td>
                    </tr>
                    <tr style="">
                        <td style="padding-top: 20px;">
                                <span style="font-size:15px;font-weight:bold;">Total Harga Tiket</span>
                        </td>
                        <td style="padding-top: 20px;">
                            <span style="font-size:15px;color:#390;font-weight:bold;float: right;">Rp. </span>
                        </td>
                        <td style="text-align: right;padding-top: 20px;">
                            <span style="font-size:15px;color:#390;font-weight:bold;padding-right: 15px;"> <?php echo  \General::number_format((($data['booking_seats'][0]['amount']*$data['nos']) + $data['handling_fee'] - $data['coupon_discount']),3); ?></span>
                        </td>
                    </tr>
                </table>
        </div>
        </div>
        <div class="row" align="center" style="margin-top: 20px">
            <div class="col-lg-12 col-md-12" style="text-align:center;">
                <img src="{{URL::to('assets/images/image-mid.jpg')}}" width="80%">
            </div>
        </div>
        <div class="row" style="padding-left: 45px;padding-right: 35px;">
            <div style="font-size:14px;padding-bottom:5px; color:#000; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; margin-bottom:0px; line-height:1.5"> <strong>CATATAN:</strong>
                <ol style="list-style-type:decimal; margin:5px 0 0; color:#414042; font-size:13px; padding-left:15px">
                    <li style="margin-bottom:0px">Harap print e-Tiket ini dibawa untuk ditukarkan dengan tiket bus resmi.</li>
                    <li style="margin-bottom:0px">Waktu yang tercantum pada tiket adalah waktu setempat.</li>
                    <li style="margin-bottom:0px">Harap tiba di terminal keberangkatan 60 menit sebelum waktu keberangkatan yang tercantum pada e-Tiket.</li>
                    <li style="margin-bottom:0px">Nomor kursi dapat berubah sesuai kebijakan operator pada saat keberangkatan.</li>
                    <li style="margin-bottom:0px">Harga dapat berubah sewaktu-waktu pada saat keberangkatan sesuai dengan kebijakan masing-masing operator dikarenakan high season, liburan panjang, lebaran dan lain-lain yang berpotensi terhadap kenaikan tiket. Tambahan biaya tiket tersebut akan dibebankan kepada penumpang.</li>
                    <li style="margin-bottom:0px">BUSTIKET tidak bertanggung jawab atas keterlambatan atau penjadwalan ulang bus. Hal tersebut berada 
                         dibawah kendali masing-masing manajemen bus.</li>
                    <li style="margin-bottom:0px">Jika Anda menghadapi masalah, atau memiliki pertanyaan silahkan hubungi kami di 0812-8000-3919</li>
                </ol>
            </div>
        </div>
            
        </div>
        <div style="margin-top: 5px;margin-bottom: 5px;height: 5px;background-color: #fcb912;position: relative;width: 835px;"><img class=" col-lg-12 col-lg-12"  src="{{URL::to('assets/images/yellow_line.png')}}" width="100%" height="5" style="" alt=""/></div>
        <div style="position: relative;width: 835px;"><img style="width: 835px;" src="{{URL::to('assets/images/footer_email_phone.png')}}"  height="40" alt=""/></div>
    </div>

    
<!--<div class="footer"></div>-->
   
<?php
$i = 0;
//print_r($data['booking_seats']);

    foreach($data['booking_seats'] as $row){
//    for($i = 0;$i<count($data['booking_seats']);$i++){
//        $row = $data['booking_seats'][$i];
//        print_r($data['booking_seats'][1]);
        $seatLable = $layout ? $row['seat_lbl'] : 'Saat <br>Keberangkatan';
        
            ?>
    <div class="page-break"></div>
    <br><br><br>
    <div class='container' style="width: 600px;height: 329px;text-align: center;margin: 20px auto;">
                    <div class='row'>
                            <div class='row'>
                                <img src='{{URL::to('assets/images/topLine.png')}}' height='60px' alt='Logo' class='col-md-12 col-sm-12 col-xs-12'>
                        </div>
                        <div class='row' style="">
                            <div class='col-md-2 col-sm-3 col-xs-3' style="width: 20%;float: left;" >
                                <img src='{{URL::to('assets/images/logo-header-2-new.png')}}' height='160px' width='250' alt='Logo' style='margin-top: 15px;height: 160px;width: 250px;padding-left: 10px;'>
                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6' style='/*margin-top:10px;*/text-align:center;height: 60px;width: 65%;float: left;' >
                                <?php if($spLogo != ''){ ?>
                                <img src='<?php echo $spLogo; ?>' alt='Logo' style='margin-top: 15px;padding-left:0px;height: 60px;width: 300px;'/>
                                <?php } ?>
                            </div>

                            <div class='col-md-2 col-sm-2 col-xs-2'style='width : 20%; float: right;background-color: #0D9E0D;font-weight:bold;padding: 5px;padding-left: 5px;padding-right: 15px;margin-top: 15px;padding-left:5px;text-align:center;color:white'>

                                    <span  style='font-size:17px'><strong>E-Tiket</strong></span>
                            </div>
                        </div>
                            <div class='row'>

                                <div class='col-md-12 col-sm-12 col-xs-12' align='center' style="padding-left: 11px;padding-right: 11px;" >
                                <table width='100%' style="border-collapse: collapse;" >
                                    <tr>
                                        <td colspan='3'></td>
                                        <td colspan='2' style="text-align:center;width: 140px;height: 17px;border-top: 1px;border-left: 1px;border-right: 1px;border-color: gray;border-style: solid;"><div style=''><?php echo $busName; ?></div> </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='2' style='border: 1px;border-bottom: 0px;border-color: gray;border-style: solid;'>
                                            <div style="width: 200px;height: 200px;" ><img src='{{URL::to('welcome/qr-code/'.$row['ticket_id'])}}' alt='Logo' style='padding:10px;height: 200px;width: 200px;margin-top: 30px;' /></div>
                                        </td>
                                        <td colspan='4' style='border-top: 1px;border-right: 1px;border-color: gray;border-style: solid;text-align:center'>

                                            <table style="width: 445px;height: 105px;">
                                                <tr>
                                                    <td style='text-align:center;padding:10px;'><p style='text-align:center;color:grey;'>Berangkat</p></td>
                                                    <td></td>
                                                    <td style='text-align:center;padding:10px;'><p style='color:grey;'>Tiba</p></td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align:center;'> 
                                                        
                                                        <div style='padding:20px;'>
                                                            <h3 style='font-weight: bold;'><?php echo strtoupper($data['from_terminal']['loc_citi']['name']) ?></h3>
                                                            <p style='color:grey;'>
                                                                
                                                                <?php echo  $data['from_terminal']['name'] ?><br>
                                                                <?php echo  $data['from_terminal']['name'].','.$data['from_terminal']['loc_citi']['name'].','.$data['from_terminal']['loc_district']['name'] ?><br>
                                                                <?php echo  $boardDate ?>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <img src='{{URL::to('assets/images/right-arrow.png')}}' height='30px' alt='Logo' style='padding:20px' >
                                                    </td>
                                                    <td style='text-align:center;'>
                                                        <div style='margin-left:5px;'>

                                                            <h3 style='font-weight: bold;'><?php echo  strtoupper(($data['to_terminal']['loc_citi']['name'])) ?></h3>
                                                            <p style='color:grey;'>
                                                                <?php echo  $data['to_terminal']['name'] ?><br>
                                                                <?php echo  $data['to_terminal']['name'].','.$data['to_terminal']['loc_citi']['name'].','.$data['to_terminal']['loc_district']['name'] ?><br>
                                                                <?php echo  $dropDate ?>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='border-top: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;width: 148px;height: 19px;'>Penumpang</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-color: gray;border-style: solid;text-align:center;width: 150px;height: 19px;'>Booking ID</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-color: gray;border-style: solid;text-align:center;width: 150px;height: 19px;white-space:nowrap;'>Nomor Tiket</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-right: 1px;border-color: gray;border-style: solid;text-align:center;width: 94px;height: 19px;'>Nomor Kursi</td>

                                    </tr>
                                    <tr>
                                        <td style='border-right: 1px;border-left: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;height: 20px;'></td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;height: 55px;'>
                                            <div style='text-align:center;padding:10px;' >
                                                <p style='text-align:center;'>
                                                    <?php echo substr($row['passenger_name'],0,16); ?><br>
                                                    ( <?php echo strtolower($row['passenger_gender']) == 'm' ? 'L' :'P'; ?> / <?php echo $row['passenger_age']; ?> )<br>
                                                    <?php echo $row['passenger_mobile']; ?>
                                                </p>
                                            </div>
                                        </td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;text-align:center;border-style: solid;height: 55px;'>
                                            <p style='color: #0D9E0D;font-weight:bold;padding: 5px;'>
                                                <?php
                                                echo $row['booking_id']; 
//                                                echo 56897457210135; 
                                                ?>
                                            </p>
                                        </td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;text-align:center;border-style: solid;height: 55px;'>
                                            <p style='color: #0D9E0D;font-weight:bold;padding: 5px;'>
                                                <?php 
                                                echo $row['ticket_id']; 
//                                                echo 3261457842157; 
                                                
                                                ?>
                                            </p>
                                        </td>
                                        <td rowspan="2" style='background-color: black;text-align:center;color: white;border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;height: 55px;'>
                                            <?php if($layout){ ?>
                                            <p style='font-weight: bold;padding:2px;font-size: 24px;'>
                                                <?php
                                                echo $seatLable;
                                                
                                                ?>
                                            </p>
                                            <?php }else{
                                                ?>
                                            <p style='font-weight: bold;padding:2px;font-size: 12px;'>
                                                <?php
                                                echo $seatLable;
                                                
                                                ?>
                                            </p>
                                                <?php
                                            } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='border-left: 1px;border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;width: 133px;height: 23px;'>
                                            <p> <span style="font-size: 10px;">Harga : </span> <span style='color: #0D9E0D;font-weight:bold;font-size:14px'><strong>Rp. <?php echo \General::number_format($row['amount'],3); ?></strong></span></p>
                                        </td>
                                    </tr>
                                    </table>
                            </div>
                        </div>
                        <div class='row' style='background-color: #3e9143;margin-top:15px;color: white;width: 100%;height: 27px;' >
                            
                            <div style="width: 70%;float: left;">
                                <div style="color:#fff;width: 42%;float: left;">
                                    <div style="width:25%;float: left;"><img src="{{URL::to('assets/images/phon.png')}}" style="margin-left:20px;margin-top:3px;height: 20px;width: 20px;">  </div>
                                   <div style="margin-top: 4px;width: 75%;float: left;"><b>0812-8000-3919</b></div>
                                </div>
                                <div style="color:#fff;text-align: left;width: 48%;">
                                    <div style="width:14%;float: left;padding-right: 5px;padding-top: 1px "><img src="{{URL::to('assets/images/email.png')}}" style="margin-left:0px;margin-top:2px;height: 20px;width: 20px;">  </div>
                                    <div style="margin-top: 2px;width: 70%;float: left;"><b>cs@bustiket.com</b></div>
                                </div>

                            </div>
                           <?php if($route_code != ''){ ?>
                            <div style="width: 28%;float: right;margin-top: 2px;">
                                <span style='text-align:  right;'> Kode Rute (<?php echo $route_code; ?>)</span>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
 
<?php
}
?>

