@extends('layouts.site_layout')
@section('content')
    <main class="site-main">
        <section class="section section-hero-page" style="background-image: url('assets/images/heropages/1.jpg') ;">  
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Syarat & Ketentuan</h1>
                            </header>

                            <ol class="list-syaratketentuan">
                                <li>
                                    <h3 class="list-syaratketentuan-title">Syarat & Ketentuan</h3>

                                    <ol>
                                        <li>Terima kasih atas kunjungan Anda ke website kami, WWW.BUSTIKET.COM. Kami berharap agar kunjungan Anda dapat bermanfaat dan memberi kenyamanan dalam mengakses dan menggunakan seluruh Layanan yang tersedia di website kami. Kami terus menerus berupaya memperbaiki dan menin gkatan mutu pelayanan kami, dan sangat menghargai segala kritik, saran dan masukan dari Anda; silakan Anda menyampaikannya ke kami melalu cs@bustiket.com atau telepon di 0812-8000-3919.</li>

                                        <li>Website ini dimiliki, dioperasikan dan diselenggarakan oleh CV. Mitra Indo Visi Group (BUSTIKET.COM) website dan layanan kami tersedia secara online melalui website: WWW.BUSTIKET.COM atau berbagai akses, media, perangkat dan platform lainnya, baik yang sudah atau akan tersedia dikemudian hari.</li>
                                    </ol>    
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Informasi Penting</h3>

                                    <ol>
                                        <li>
                                            <span class="block-content">BUSTIKET bertanggung jawab pada :</span>
                                            <ul>
                                                <li>Mengeluarkan tiket valid yang diterima oleh operator.</li>
                                                <li>Menyediakan bantuan untuk pembatalan dan refund sesuai dengan syarat dan ketentuan masing-masing kebijakan operator.</li>
                                                <li>Menyediakan customer service dan bantuan dalam komunikasi dengan operator.</li>
                                            </ul>

                                            <span class="block-content">BUSTIKET tidak bertanggung jawab pada:</span>
                                            <ul>
                                                <li>Keterlambatan/tidak berangkatnya armada akibat kerusakan mesin ataupun hal lainnya yang menyangkut operasional dari operator.</li>
                                                <li>Crew operator yang tidak ramah.</li>
                                                <li>Armada dan fasilitas armada yang tidak sesuai dengan harapan penumpang.</li>
                                                <li>Penundaan keberangkatan maupun percepatan keberangkatan dikarenakan hal yang tidak dapat dihindari (misal: kemacetan, kecelakaan, kerusakan armada dll).</li>
                                                <li>Kehilangan barang bawaan/ bagasi maupun rusaknya barang bawaan selama perjalanan</li>
                                                <li>Pemindahan kursi tempat duduk pada menit akhir keberangkatan dikarenakan kebijakan dari operator</li>
                                                <li>Perpindahan titik lokasi keberangkatan dikarenakan kendala kemacetan dan percepatan waktu perjalanan. Hubungi operator sebelum hari keberangkatan untuk mengetahui lokasi. Nomor telpon (dan alamat email apabila ada) operator tersedia di e-tiket.</li>
                                                <li>Tertinggal karena Anda menunggu di tempat keberangkatan yang salah / tidak sesuai. Pastikan untuk menghubungi operator apabila Anda kesulitan menemukan lokasi keberangkatan.
                                                <span class="block-content">Sehubungan dengan kendala operasional, operator bisa melakukan:</span>
                                                <ul>
                                                    <li>Pemindahan armada.</li>
                                                    <li>Penggunaan angkutan feeder sebagai penjemput penumpang.</li>
                                                    <li>Pemindahan armada juga mungkin dilakukan oleh operator antar kelas armada berbeda (Misal: Eksekutif dipindahkan ke Bisnis).</li>
                                                </ul>
                                                <span class="block-content">Pengalihan jalur perjalanan oleh crew operator akibat satu dan lain hal yang mengakibatkan Anda tidak dapat turun pada tujuan akhir Anda.</span>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>Pada beberapa operator, penumpang diwajibkan untuk mencetak e-tiket BUSTIKET dan membawa kartu identitas yang masih berlaku (KTP/ SIM/ PASPOR). Jika Anda tidak membawa dokumen tersebut, kegagalan pengangkutan mungkin dapat terjadi.</li>
                                        <li>Tiket yang di beli di BUSTIKET dapat dibatalkan. Mohon untuk selalu membaca kebijakan pembatalan masing masing operator yang tersedia di E-Ticket. Tiket yang dibeli melalui credit card, tidak akan kembali dalam bentuk limit credit, melainkan di transfer menggunakan Account Bank BCA, Mandiri, ataupun BRI. 
                                        <small class="block-content">*Biaya transfer antar bank maupun biaya payment gateway karena penggunaan credit card, convenient store, dll tidak dapat di refund.</small>
                                        </li>
                                        <li>Dalam hal SMS dan email e-ticket tidak berhasil dikirimkan dikarenakan penulisan nomor maupun alamat email yang keliru, kursi akan tetap ter-booked selama transaksi telah berhasil dilakukan. BUSTIKET dan juga operator tidak bertanggung jawab apabila ternyata terjadi perubahan jadwal, armada, kursi maupun tempat keberangkatan dan informasi ini tidak diterima oleh penumpang.</li>
                                        <li>BUSTIKET tidak memberikan jaminan terhadap pemenuhan seluruh informasi yang tercantum di dalam website meliputi: rute, fasilitas, gambar armada, tempat keberangakatan, jam keberangkatan dan kedatangan, semua hal tersebut hanyalah sebatas informasi yang diberikan oleh operator kepada BUSTIKET sebagai partner penjualan tiket online.</li>
                                        <li>Tambahan biaya tiket dikarenakan terjadi kenaikan harga BBM setelah tanggal pemesanan. Hal ini sepenuhnya merupakan wewenang operator bersangkutan. Tambahan biaya mungkin akan diminta petugas operator pada saat keberangkatan.

                                        <span class="clearfix space space-15"></span>
                                        <small class="block-content nospace-bottom">Catatan Penting untuk Keberangkatan High Season/ Musim Liburan.</small>
                                        Untuk hari tertentu yang berpotensi terhadap kenaikan harga tiket dikarenakan high season, liburan panjang, lebaran, dan hari khusus lainnya, biaya tambahan mungkin akan dikenakan kepada penumpang pada saat keberangkatan. Tambahan biaya tiket ini akan disampaikan oleh petugas operator diberlakukan kepada seluruh penumpang, baik yang membeli online maupun membeli langsung. Jam keberangkatan yang tertera di e-ticket adalah jam estimasi keberangkatan normal. Jam keberangkatan dapat berubah dikarenakan high season, liburan sekolah, long weekend, dll. Jika lokasi keberangkatan Anda adalah rute lintasan (bukan pool/ origin bus), kendala kemacetan mungkin akan membuat keterlambatan keberangkatan. Pada musim liburan, beberapa tempat keberangkatan mungkin akan ditutup oleh operator guna percepatan durasi perjalanan dan menghindari kemacetan. Hubungi costumer services 1 hari sebelum hari keberangkatan untuk kepastian lokasi tempat keberangkatan. Nomor telpon (dan alamat email apabila ada) costumer services tersedia di e-tiket. Percepatan jam keberangkatan mungkin dilakukan oleh operator untuk menghindari kemacetan, selalu aktifkan nomor Handphone Anda agar operator bisa kontak Anda mengenai perubahan tersebut. Hubungi costumer services 1 hari sebelum hari keberangkatan untuk kepastian lokasi tempat keberangkatan. Nomor telpon (dan alamat email apabila ada) costumer services tersedia di e-tiket.
                                        </li>

                                    </ol>
                                </li>
                                <li>
                                    <h3 class="list-syaratketentuan-title">Umum</h3>
                                    <ol>
                                        <li>Dengan mengakses dan menggunakan website dan layanan kami, Anda menyatakan telah membaca, memahami, menyetujui dan menyatakan tunduk pada Syarat dan Ketentuan Penggunaan BUSTIKET. Jika Anda tidak dapat menyetujui Syarat dan Ketentuan Penggunaan BUSTIKET, baik secara keseluruhan ataupun sebagian, maka Anda tidak diperbolehkan untuk mengakses website ini ataupun menggunakan layanan yang kami sediakan.</li>
                                        <li>Syarat dan Ketentuan Penggunaan BUSTIKET ini terdiri atas (i) syarat dan ketentuan umum yang berlaku untuk setiap akses dan layanan yang tersedia pada website, dan (ii) syarat dan ketentuan khusus yang mengatur lebih lanjut ketentuan penggunaan produk atau layanan tertentu. Dalam hal ditemukan adanya perbedaan atau pertentangan antara syarat dan ketentuan umum dan syarat dan ketentuan khusus, maka yang berlaku adalah syarat dan ketentuan khusus.</li>
                                        <li>Syarat dan Ketentuan Penggunaan BUSTIKET dapat kami ubah, modifikasi, tambah, hapus atau koreksi ("perubahan") setiap saat dan setiap perubahan itu berlaku sejak saat kami nyatakan berlaku atau pada waktu lain yang ditetapkan oleh kami. Anda kami anjurkan untuk mengunjungi website kami secara berkala agar dapat mengetahui adanya perubahan tersebut.</li>
                                    </ol>
                                </li>
                                <li>
                                    <h3 class="list-syaratketentuan-title">Penggunaan</h3>

                                    <ol>
                                        <li>Website ini dan layanan yang tersedia didalamnya dapat digunakan oleh Anda hanya untuk penggunaan pribadi dan secara non-komersial dan setiap saat tunduk pada dan berlaku syarat dan ketentuan yang saat itu berlaku dalam Syarat dan Ketentuan Penggunaan BUSTIKET.</li>
                                        <li>Website ini dan produk-produk, teknologi dan proses yang terdapat atau terkandung dalam website, dimiliki oleh Kami atau pihak ketiga yang memberi hak kepada Kami. Kecuali untuk penggunaan yang secara tegas diijinkan dan diperbolehkan dalam Syarat dan Ketentuan Penggunaan BUSTIKET, Anda tidak memiliki ataupun menerima dan BUSTIKET tidak memberikan hak lain apapun ke Anda atas website ini, berikut dengan segala data, informasi dan konten didalamnya. </li>
                                        <li>Dengan menggunakan website ini atau Layanan yang tersedia didalamnya, maka Anda menyatakan setuju tidak akan men-download, menayangkan atau mentransmisi dengan cara apa pun, dan atau membuat konten apa pun tersedia untuk umum yang tidak konsisten dengan penggunaan yang diijinkan dalam Syarat dan Ketentuan Penggunaan BUSTIKET.</li>
                                        <li>Dalam website ini mungkin terdapat link (tautan) ke website yang dikelola oleh pihak ketiga ("Situs Eksternal"). Situs Eksternal disediakan hanya untuk referensi dan kenyamanan saja. BUSTIKET tidak mengoperasikan, mengendalikan atau mendukung dalam bentuk apa pun Situs Eksternal yang bersangkutan ataupun konten/isinya. Anda bertanggung jawab penuh atas penggunaan Situs Eksternal tersebut dan dianjurkan untuk mempelajari syarat dan ketentuan dari Situs Eksternal itu secara seksama.</li>
                                        <li>Layanan yang tersedia dalam website ini secara umum menggunakan sistem re-marketing dan sistem cookies yang memungkinkan pihak ketiga (termasuk dan tidak terbatas pada Google) mengakses dan menggunakan data kunjungan dalam sistem Cookies website ini untuk menampilkan dan menayangkan kembali tiap iklan BUSTIKET melalui internet.</li>
                                        <li>Anda tidak boleh membuat link, melakukan screen capture atau data crawling ke website tanpa adanya persetujuan tertulis sebelumnya dari BUSTIKET. Hal-hal tersebut dianggap sebagai pelanggaran hak milik intelektual BUSTIKET.
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <h3 class="list-syaratketentuan-title">Layanan BUSTIKET>COM</h3>

                                    <ol>
                                        <li>BUSTIKET menyediakan dan menyelenggarakan sistem dan fasilitas pemesanan online secara terpadu ("Layanan"), yang dapat melayani pemesanan : Transportasi Darat ("Produk") yang memungkinkan Anda untuk mencari informasi atas produk yang Anda inginkan, serta melakukan pemesanan dan pembelian dan sekaligus melakukan pembayaran secara online dan aman melalui berbagai sistem dan fasilitas pembayaran yang tersedia.</li>

                                        <li>Layanan kami secara umum dapat tersedia secara online selama dua puluh empat jam sehari dan tujuh hari dalam seminggu; kecuali dalam hal adanya perbaikan, peningkatan atau pemeliharaan pada website kami. </li>

                                        <li>Produk disediakan, disuplai dan diselenggarakan oleh pihak ketiga ("Mitra") yang telah mengadakan kerjasama dan telah mengadakan ikatan, baik secara langsung ataupun tidak langsung, dengan kami. Anda memahami dan mengakui bahwa:
                                        <ul>
                                            <li>Pemesanan dan pembelian yang Anda lakukan melalui BUSTIKET merupakan hubungan hukum dan kontrak yang mengikat antara Anda dan mitra kami. Dalam hal ini, BUSTIKET bertindak sebagai agen atau perantara yang bertugas untuk memfasilitasi transaksi antara Anda dan mitra kami.</li>

                                            <li>Data dan informasi terkait dengan produk tertentu yang kami cantumkan pada website merupakan data dan informasi yang Kami terima dari mitra, dan kami mempublikasikan data dan informasi tersebut dengan itikad baik sesuai dengan data dan informasi yang kami terima.</li>

                                            <li>Kami tidak memiliki kendali atas data dan informasi yang diberikan oleh mitra kami, dan kami tidak menjamin bahwa data dan informasi yang disajikan adalah akurat, lengkap, atau benar, dan terbebas dari kesalahan.</li>
                                        </ul>
                                        </li>

                                        <li>Anda tidak diperbolehkan untuk menjual kembali produk kami, menggunakan, menyalin, mengawasi, menampilkan, men-download, atau mereproduksi konten atau informasi, piranti lunak, atau layanan apa pun yang tersedia di website kami untuk kegiatan atau tujuan komersial apapun, tanpa persetujuan tertulis dari kami sebelumnya.
                                        </li>

                                        <li>Anda dapat menggunakan website dan layanan yang tersedia untuk membuat pemesanan/pemesanan yang sah. Anda tidak diperbolehkan untuk membuat pemesanan untuk tujuan spekulasi, tidak benar atau melanggar hukum. Jika kami menemukan atau sewajarnya menduga bahwa pemesanan/pemesanan yang Anda buat ternyata tidak sah, maka kami mencadangkan hak untuk membatalkan pemesanan Anda.</li>

                                        <li>Anda juga menjamin bahwa data dan informasi yang Anda berikan ke kami, baik sehubungan dengan pemesanan ataupun pendaftaran pada BUSTIKET, adalah data dan informasi yang akurat, terkini dan lengkap. Untuk ketentuan penggunaan data dan informasi yang Anda berikan, silakan untuk merujuk pada Kebijakan Penggunaan Data.</li>

                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Layanan/Pembelian Produk</h3>

                                    <ol>
                                        <li>Pemesanan/pembelian produk dianggap berhasil atau selesai setelah Anda melakukan pelunasan pembayaran dan BUSTIKET menerbitkan dan mengirim ke Anda, Surat konfirmasi pemesanan/pembelian. Apabila terjadi perselisihan atau permasalahan, maka data yang terdapat pada BUSTIKET akan menjadi acuan utama dan diangap sah.</li>

                                        <li>Dengan menyelesaikan pemesanan/pembelian, maka Anda dianggap setuju untuk menerima: (i) email yang akan kami kirim tidak lama sebelum tanggal pelayanan yang Anda pesan, memberikan Anda informasi tentang produk yang Anda beli, dan menyediakan Anda informasi dan penawaran tertentu (termasuk penawaran pihak ketiga yang telah Anda pilih sendiri) yang terkait dengan pemesanan dan tujuan Anda, dan (ii) email yang akan kami kirim tidak lama setelah tanggal pelayanan untuk mengundang Anda untuk melengkapi formulir ulasan pengguna produk kami. Selain dari konfirmasi email yang menyediakan konfirmasi pemesanan dan email-email yang telah Anda pilih sendiri, kami tidak akan mengirimi Anda pemberitahuan (yang diinginkan maupun yang tidak), email, korespondensi lebih lanjut, kecuali jika diminta secara khusus oleh Anda.</li>
                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Harga Produk</h3>

                                    <ol>
                                        <li>Kami selalu berupaya untuk menyediakan harga terbaik atas produk untuk dapat dipesan oleh Anda. Harga yang tertera mungkin memiliki syarat dan ketentuan khusus, jadi Anda harus memeriksa sendiri dan memahami syarat dan ketentuan khusus yang berlaku terhadap suatu harga atau tarif tertentu sebelum Anda melakukan pemesanan. Anda juga perlu memeriksa dan memahami ketentuan mengenai pembatalan dan pengembalian dana yang secara khusus berlaku untuk produk dan/atau harga tertentu.</li>

                                        <li>Harga yang tercantum sudah termasuk pajak, pungutan, biaya dan ongkos lainnya yang akan Kami uraian secara tegas pada website atau surat konfirmasi dari kami.</li>

                                        <li>Untuk produk-produk tertentu kami juga memberikan jaminan harga terbaik bagi Anda. Harap Anda dapat mempelajari kebijaksanaan kami atas jaminan harga terbaik kami. Dalam kaitannya dengan kebijakan kami atas jaminan harga terbaik itu, jika Anda menemukan ada harga yang lebih rendah di layanan online lain di internet, harap Anda dapat memberitahu ke kami (cs@bustiket.com) dan kami akan berupaya untuk menyamakan harga/tarif Kami dengan harga/tarif lebih rendah yang Anda temukan.</li>

                                        <li>BUSTIKET berhak untuk mengubah harga suatu produk setiap saat tanpa pemberitahuan sebelumnya, namun tetapi produk yang sudah dibeli oleh Anda dan yang untuk produk mana Anda sudah mendapat surat konfirmasi tidak akan berubah.</li>
                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Pembayaran</h3>

                                    <ol>
                                        <li>Pelunasan atas harga pembelian merupakan syarat untuk melakukan pembelian. Kami menerima pembayaran dengan sistem pembayaran menggunakan kartu kredit (VISA, Master Card), transfer antar rekening serta antar bank ke rekening BUSTIKET di bank-bank yang tercantum di website.
                                        <ol class="lower-latin">
                                            <li>Untuk melindungi dan mengenskripsi informasi kartu kredit Anda, kami menggunakan fasilitas teknologi "Secure Socket Layer (SSL)";</li>
                                            <li>Dalam hal terjadi kasus penipuan kartu kredit atau penyalah-gunaan sistem pembayaran oleh pihak-pihak ketiga manapun, maka kejadian tersebut harus segera dilaporkan ke kami dan perusahaan / bank penerbit kartu kredit Anda, untuk Anda memperoleh penanganan lebih lanjut sesuai dengan prosedur dan aturan yang berlaku.</li>
                                        </ol>
                                        </li>
                                        <li>Atas setiap pemesanan yang dapat kami konfirmasi, kami akan mengirim Anda surat konfirmasi via email yang berisi uraian produk dan pemesanan yang Anda buat serta konfirmasi pembayaran. Anda bertanggung-jawab untuk mencetak dan menjaga informasi yang tertera pada surat konfirmasi yang kami kirim. Surat konfirmasi ini merupakan dokumen yang sangat penting dan Anda wajib membawa cetakan dari surat konfirmasi ini pada saat Anda akan menggunakan atau mengambil produk yang Anda beli. Kami atau mitra kami berhak untuk menolak memberikan Produk atau pelayanan, jika Anda tidak dapat membuktikan bahwa Anda telah secara sah melakukan pemesanan dan pelunasan, dan Anda membebaskan BUSTIKET dari segala tanggung-jawab dan kerugian Anda dalam bentuk apapun.</li>
                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Perubahan dan Pembataln</h3>

                                    <ol>
                                        <li>Kecuali secara tegas dinyatakan lain dalam Syarat dan Ketentuan Penggunaan BUSTIKET, semua pembelian Produk di BUSTIKET tidak dapat diubah, dibatalkan, dikembalikan uang, ditukar atau dialihkan ke orang/pihak lain.</li>

                                        <li>Dengan melakukan pemesanan atau pembelian produk di BUSTIKET, Anda dianggap telah memahami, menerima dan menyetujui kebijakan dan ketentuan pembatalan, serta segala syarat dan ketentuan tambahan yang diberlakukan oleh mitra. Kami akan mencantumkan kebijakan dan ketentuan pembatalan tersebut di setiap surat konfirmasi yang kami kirim ke Anda. Harap dicatat bahwa tarif atau penawaran tertentu tidak memenuhi syarat untuk pembatalan atau pengubahan. Anda bertanggung-jawab untuk memeriksa dan memahami sendiri kebijakan dan ketentuan pembatalan tersebut sebelumnya.</li>
                                        <li>Jika Anda ingin melihat ulang, melakukan perubahan, atau membatalkan pesanan Anda, harap merujuk pada surat konfirmasi dan ikuti instruksi di dalamnya. Harap dicatat bahwa Anda mungkin saja dikenakan biaya tambahan atas pembatalan sesuai dengan kebijakan dan ketentuan pembatalan.</li>
                                        <li>Walaupun sangat kecil kemungkinan kami membatalkan atau mengubah pemesanan yang sudah kami konfirm dalam surat konfirmasi, namun jika diperlukan kami akan memberitahu Anda secepat mungkin.
                                        <ol class="lower-latin">
                                            <li>Kami akan bertanggung-jawab terhadap perubahan yang secara signifikan berpengaruh pada ketentuan produk dan layanan yang telah kami konfirmasikan sebelumnya, yaitu:
                                            <ul class="list-circle">
                                                <li>Pembatalan pemesanan;</li>
                                                <li>Perubahan tanggal, atau produk; <br>Dalam hal ini kami melakukan perubahan signifikan, maka Anda memiliki pilihan untuk:
                                                    <ul>
                                                        <li>Menerima perubahan yang Kami tawarkan untuk tanggal atau produk lain; atau</li>
                                                        <li>Menerima pengembalian pembayaran dalam bentuk kredit yang dapat Anda gunakan untuk pembelian produk dikemudian hari dari BUSTIKET. dan untuk itu Anda harus memberitahu kami dalam waktu 7 (tujuh) hari kalender mengenai pilihan Anda, dan jika kami tidak mendapat konfirmasi dari Anda, maka kami menganggap Anda memilih pilihan (a).</li>
                                                    </ul>
                                                </li>

                                            </ul>
                                            </li>
                                        </ol>
                                        </li>
                                        <li>Kami tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal kami tidak dapat menyerahkan produk atau memberi layanan kepada Anda, akibat dari hal-hal yang terjadi akibat keadaan memaksa atau yang diluar kekuasaan kami atau mitra kami untuk mengendalikan, seperti, tapi tidak terbatas pada: perang, kerusuhan, teroris, perselisihan industrial, tindakan pemerintah, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya.</li>
                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Keamanan</h3>
                                    <ol>
                                        <li>Pada saat Anda membuat pemesanan atau mengakses informasi akun Anda, Anda akan menggunakan akses Secure Server Layer (SSL) akan mengenkripsi informasi yang Anda kirimkan melalui website ini.</li>

                                        <li>Walaupun BUSTIKET akan menggunakan upaya terbaik untuk memastikan keamanannya, BUSTIKET tidak bisa menjamin seberapa kuat atau efektifnya enkripsi ini dan BUSTIKET tidak dan tidak akan bertanggung jawab atas masalah yang terjadi akibat pengaksesan tanpa ijin dari informasi yang Anda sediakan.</li>
                                    </ol>
                                </li>

                                <li>
                                    <h3 class="list-syaratketentuan-title">Kebijakan Penggunaan Data</h3>
                                    <ol>
                                        <li>Kami menganggap privasi Anda sebagai hal yang penting.</li>
                                        <li>Pada saat Anda membuat pemesanan di BUSTIKET, kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Pada prinsipnya, data Anda akan kami gunakan untuk menyediakan produk dan memberi layanan kepada Anda. Kami akan menyimpan setiap data yang Anda berikan, dari waktu ke waktu, atau yang kami kumpulkan dari penggunaan produk dan layanan kami. Data pribadi Anda yang ada pada kami, dapat kami gunakan untuk keperluan akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, serta keperluan security, administrasi dan legal, reward points atau bentuk sejenisnya, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi, dan membantu kami dikemudian hari dalam memberi pelayanan kepada Anda. Sehubungan dengan itu, kami dapat mengungkapkan data Anda kepada group perusahaan di mana BUSTIKET tergabung didalamnya, mitra penyedia produk, perusahaan lain yang merupakan rekanan dari BUSTIKET, perusahaan memproses data yang terikat kontrak dengan kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang, di jurisdiksi manapun.</li>
                                    </ol>
                                </li>
                                <li>
                                    <h3 class="list-syaratketentuan-title">Penolakan Tanggung-Jawab</h3>
                                    <ol class="">
                                        <li>Seluruh data, informasi, atau konten dalam bentuk apapun yang tersedia pada website ini disediakan seperti apa adanya dan tanpa ada jaminan.</li>
                                        <li>Anda mengakui, setuju dan sepakat bahwa Anda menanggung sendiri segala bentuk resiko atas penggunaan website dan layanan. Lebih lanjut, Anda mengakui, setuju dan sepakat bahwa BUSTIKET, termasuk setiap direktur, pejabat, pegawai, mitra dan pihak lain manapun yang bekerjasama dengan BUSTIKET, tidak bertanggung-jawab atas dan tidak memberi jaminan terhadap:
                                        <ol class="lower-latin">
                                            <li>Segala hal yang berkaitan dengan website ini termasuk tapi tidak terbatas pada pengoperasian atau keakurasian data, kelayakan, kelengkapan data.a. Segala hal yang berkaitan dengan website ini termasuk tapi tidak terbatas pada pengoperasian atau keakurasian data, kelayakan, kelengkapan data.</li>
                                            <li>baik yang tersirat maupun tersurat, termasuk jaminan yang tersirat dari pembelian atau kepatutan dari tujuan tertentu atau kelayakan untuk diperdagangkan;</li>
                                            <li>Kehilangan atau kerusakan, baik langsung, tidak langsung, khusus, yang bersifat konsekuensial atau termasuk kehilangan keuntungan, reputasi, kerusakan/kehilangan data, kerusakan koneksi yang tak dapat diperbaiki akibat penggunaan atau ketidakmampuan menggunakan website ini baik yang berdasarkan hukum atau hal lain bahkan ketika kita diinformasikan mengenai kemungkinan-kemungkinan tersebut;</li>
                                            <li>Akses Anda terhadap website, berikut dengan kerusakan yang mungkin timbul akibat akses Anda ke website atau situs eksternal. Akses tersebut merupakan tanggung-jawab Anda sendiri dan Anda sendiri yang harus memastikan bahwa Anda terbebas dan terlindungi dari virus atau hal lain yang mungkin mengganggu atau merusak pengoperasian sistem komputer Anda.</li>
                                        </ol>
                                        </li>
                                        <li>Sejauh yang dimungkinkan secara hukum, tanggung-jawab dan kerugian yang dapat ditanggung oleh BUSTIKET, baik untuk satu kejadian ataupun serangkaian kejadian yang saling terhubung, yang timbul dari kerugian yang secara langsung diderita oleh Anda, sebagai akibat dari kesalahan BUSTIKET, adalah terbatas sampai dengan jumlah total biaya yang telah Anda bayar lunas sebagaimana tercantum dalam Surat Konfirmasi.</li>
                                    </ol>
                                </li>

                            </ol>
                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->

        <section class="section bg-gray section-seo hidden-995">
            <div class="container container-two">

                <div class="row section-content">
                    <div class="col-md-12">
                        <h5 class="text-gray-placeholder nospace-top">BUSTIKET - Penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Dalam Kota Antar Propinsi (AKDP)</h5>

                        <p class="text-gray-placeholder small-ln ">BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Kota Dalam Propinsi (AKDP) yang pertama dan terpercaya di Indonesia. Dirilis sejak awal 2015, BUSTIKET.COM memberikan layanan rute perjalanan ke berbagai kota di pulau Jawa, Sumatera, Bali, Madura, dan Kalimantan dalam waktu dekat. Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.COM menyediakan berbagai informasi dalam hal jadwal keberangkatan semua rute bus perharinya, beragam pilihan bus, waktu tempuh, fasilitas, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini. Melalui BUSTIKET.COM, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer, tiket bus sudah bisa Anda dapatkan. Karena kepuasan pelanggan adalah prioritas utama kami, maka BUSTIKET.COM tidak hanya dapat diakses melalui website, tetapi juga tersedia dalam bentuk aplikasi Android untuk smartphone Anda. Dengan aplikasi terbaru kami, pemesanan tiket bus dapat dilakukan kapanpun dan dimanapun. Gak capek, ga antri, hanya di BUSTIKET.COM!</p>
                    </div>
                </div><!-- / .section-content -->

                <div class="row section-content">
                    <div class="col-md-12">
                        <div class="clearfix space space-30"></div>
                        <h5 class="text-gray-placeholder nospace-top">RUTE BUS, TRAVEL, DAN SHUTTLE TERPOPULER</h5>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder ">Jakarta - Solo</span>
                        <span class="block-content text-gray-placeholder ">Cilegon - Wonogiri</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Wonosobo</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pekalongan</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Prabumulih</span>
                        <span class="block-content text-gray-placeholder  ">Bogor - Wonogiri</span>
                        
                        <span class="clearfix space space-30"></span>
                        <a href="#" class="link-orange font-21">Kota Lainnya >></span>
                        </a>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bandung - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Solo</span>
                        <span class="block-content text-gray-placeholder  ">Bekasi - Bojonegoro</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pamekasan</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Pasuruan</span>
                        <span class="block-content text-gray-placeholder  ">Bekasi - Cepu</span>
                        
                        <span class="clearfix space space-30"></span>
                        <a href="#" class="link-orange font-21">Kota Lainnya >></a>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bekasi - Bangkalan</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Palembang</span>
                        <span class="block-content text-gray-placeholder  ">Depok - Wonosari</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang - Klaten</span>
                        <span class="block-content text-gray-placeholder  ">Bogor - Ponorogo</span>
                        <span class="block-content text-gray-placeholder  ">Bandung - Bandar jaya</span>
                    </div>

                    <div class="col-md-3 item-ref-kota">
                        <span class="block-content text-gray-placeholder  ">Bogor - Semarang</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang Selatan - Bangkalan</span>
                        <span class="block-content text-gray-placeholder  ">Cilegon - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Jakarta - Blora</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang Selatan - Yogyakarta</span>
                        <span class="block-content text-gray-placeholder  ">Tangerang - Purwodadi</span>
                    </div>
                </div>

            </div>
        </section>

    </main><!-- / .site-main .site-main-login -->


@stop