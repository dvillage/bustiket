<?php
$dropDate = date("j F Y,H:i", strtotime($data['dropping_date']));
$boardDate = date("j F Y,H:i", strtotime($data['pickup_date']));

$dropDate = \App\Models\General::getIndoMonths($dropDate);
$boardDate = \App\Models\General::getIndoMonths($boardDate);

$formated_hf = $data['handling_fee'] ? number_format($data['handling_fee'],3,'.','.') : 0;
$formated_dis = $data['coupon_discount'] ? number_format($data['coupon_discount'],3,'.','.') : 0;
$spLogo = '';
$busName = $data['bus']['name'];
$Booker_phone = $data['booker_mo'];
$layout = $data['bus']['layout'];
$seatLable = $layout ? $row['seat_lbl'] : 'Saat <br>Keberangkatan';
$route_code = '';
?>
    <br><br>
<div class='container' style="width: 600px;height: 329px;text-align: center;margin: 20px auto;">
                    <div class='row'>
                            <div class='row'>
                                <img src='{{URL::to('assets/images/topLine.png')}}' height='60px' alt='Logo' class='col-md-12 col-sm-12 col-xs-12'>
                        </div>
                        <div class='row' style="">
                            <div class='col-md-2 col-sm-3 col-xs-3' style="width: 20%;float: left;" >
                                <img src='{{URL::to('assets/images/logo-header-2-new.png')}}' height='160px' width='250' alt='Logo' style='margin-top: 15px;height: 160px;width: 250px;padding-left: 10px;'>
                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6' style='/*margin-top:10px;*/text-align:center;height: 60px;width: 65%;float: left;' >
                                <?php if($spLogo != ''){ ?>
                                <img src='<?php echo $spLogo; ?>' alt='Logo' style='margin-top: 15px;padding-left:0px;height: 60px;width: 300px;'/>
                                <?php } ?>
                            </div>

                            <div class='col-md-2 col-sm-2 col-xs-2'style='width : 20%; float: right;background-color: #0D9E0D;font-weight:bold;padding: 5px;padding-left: 5px;padding-right: 15px;margin-top: 15px;padding-left:5px;text-align:center;color:white'>

                                    <span  style='font-size:17px'><strong>E-Tiket</strong></span>
                            </div>
                        </div>
                            <div class='row'>

                                <div class='col-md-12 col-sm-12 col-xs-12' align='center' style="padding-left: 11px;padding-right: 11px;" >
                                <table width='100%' style="border-collapse: collapse;" >
                                    <tr>
                                        <td colspan='3'></td>
                                        <td colspan='2' style="text-align:center;width: 140px;height: 17px;border-top: 1px;border-left: 1px;border-right: 1px;border-color: gray;border-style: solid;"><div style=''><?php echo $busName; ?></div> </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='2' style='border: 1px;border-bottom: 0px;border-color: gray;border-style: solid;'>
                                            <div style="width: 200px;height: 200px;" ><img src='{{URL::to('welcome/qr-code/<?php echo $row['ticket_id']; ?>')}}' alt='Logo' style='padding:10px;height: 200px;width: 200px;margin-top: 30px;' /></div>
                                            
                                        </td>
                                        <td colspan='4' style='border-top: 1px;border-right: 1px;border-color: gray;border-style: solid;text-align:center'>

                                            <table style="width: 445px;height: 105px;">
                                                <tr>
                                                    <td style='text-align:center;padding:10px;'><p style='text-align:center;color:grey;'>Berangkat</p></td>
                                                    <td></td>
                                                    <td style='text-align:center;padding:10px;'><p style='color:grey;'>Tiba</p></td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align:center;'> 
                                                        
                                                        <div style='padding:20px;'>
                                                            <h3 style='font-weight: bold;'><?php echo strtoupper($data['from_terminal']['loc_citi']['name']) ?></h3>
                                                            <p style='color:grey;'>
                                                                
                                                                <?php echo  $data['from_terminal']['name'] ?><br>
                                                                <?php echo  $data['from_terminal']['name'].','.$data['from_terminal']['loc_citi']['name'].','.$data['from_terminal']['loc_district']['name'] ?><br>
                                                                <?php echo  $boardDate ?>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <img src='{{URL::to('assets/images/right-arrow.png')}}' height='30px' alt='Logo' style='padding:20px' >
                                                    </td>
                                                    <td style='text-align:center;'>
                                                        <div style='margin-left:5px;'>

                                                            <h3 style='font-weight: bold;'><?php echo  strtoupper(($data['to_terminal']['loc_citi']['name'])) ?></h3>
                                                            <p style='color:grey;'>
                                                                <?php echo  $data['to_terminal']['name'] ?><br>
                                                                <?php echo  $data['to_terminal']['name'].','.$data['to_terminal']['loc_citi']['name'].','.$data['to_terminal']['loc_district']['name'] ?><br>
                                                                <?php echo  $dropDate ?>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='border-top: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;width: 148px;height: 19px;'>Penumpang</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-color: gray;border-style: solid;text-align:center;width: 150px;height: 19px;'>Booking ID</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-color: gray;border-style: solid;text-align:center;width: 150px;height: 19px;white-space:nowrap;'>Nomor Tiket</td>
                                        <td style='border-top: 1px;border-bottom: 1px;border-left: 1px;border-right: 1px;border-color: gray;border-style: solid;text-align:center;width: 94px;height: 19px;'>Nomor Kursi</td>

                                    </tr>
                                    <tr>
                                        <td style='border-right: 1px;border-left: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;height: 20px;'></td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;height: 55px;'>
                                            <div style='text-align:center;padding:10px;' >
                                                <p style='text-align:center;'>
                                                    <?php echo substr($row['passenger_name'],0,16); ?><br>
                                                    ( <?php echo $row['passenger_gender']; ?> / <?php echo $row['passenger_age']; ?> )<br>
                                                    <?php echo $Booker_phone; ?>
                                                </p>
                                            </div>
                                        </td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;text-align:center;border-style: solid;height: 55px;'>
                                            <p style='color: #0D9E0D;font-weight:bold;padding: 5px;'>
                                                <?php
                                                echo $row['booking_id']; 
//                                                echo 56897457210135; 
                                                ?>
                                            </p>
                                        </td>
                                        <td rowspan="2" style='border-right: 1px;border-bottom: 1px;border-color: gray;text-align:center;border-style: solid;height: 55px;'>
                                            <p style='color: #0D9E0D;font-weight:bold;padding: 5px;'>
                                                <?php 
                                                echo $row['ticket_id']; 
//                                                echo 3261457842157; 
                                                
                                                ?>
                                            </p>
                                        </td>
                                        <td rowspan="2" style='background-color: black;text-align:center;color: white;border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;height: 55px;'>
                                            <?php if($layout == 0){ ?>
                                            <p style='font-weight: bold;padding:2px;font-size: 24px;'>
                                                <?php
                                                echo $seatLable;
                                                
                                                ?>
                                            </p>
                                            <?php }else{
                                                ?>
                                            <p style='font-weight: bold;padding:2px;font-size: 12px;'>
                                                <?php
                                                echo $seatLable;
                                                
                                                ?>
                                            </p>
                                                <?php
                                            } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='border-left: 1px;border-right: 1px;border-bottom: 1px;border-color: gray;border-style: solid;text-align:center;width: 133px;height: 23px;'>
                                            <p> <span style="font-size: 10px;">Harga : </span> <span style='color: #0D9E0D;font-weight:bold;font-size:14px'><strong>Rp. <?php echo number_format($row['amount'],3,'.','.'); ?></strong></span></p>
                                        </td>
                                    </tr>
                                    </table>
                            </div>
                        </div>
                        <div class='row' style='background-color: #3e9143;margin-top:15px;color: white;width: 100%;height: 27px;' >
                            
                            <div style="width: 70%;float: left;">
                                <div style="color:#fff;width: 42%;float: left;">
                                    <div style="width:25%;float: left;"><img src="{{URL::to('assets/images/phon.png')}}" style="margin-left:20px;margin-top:3px;height: 20px;width: 20px;">  </div>
                                   <div style="margin-top: 4px;width: 75%;float: left;"><b>0812-8000-3919</b></div>
                                </div>
                                <div style="color:#fff;text-align: left;width: 48%;">
                                    <div style="width:14%;float: left;padding-right: 5px;padding-top: 1px "><img src="{{URL::to('assets/images/email.png')}}" style="margin-left:0px;margin-top:2px;height: 20px;width: 20px;">  </div>
                                    <div style="margin-top: 2px;width: 70%;float: left;"><b>cs@bustiket.com</b></div>
                                </div>

                            </div>
                           <?php if($route_code != ''){ ?>
                            <div style="width: 28%;float: right;margin-top: 2px;">
                                <span style='text-align:  right;'> Kode Rute (<?php echo $route_code; ?>)</span>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
 
                
