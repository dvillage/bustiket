@extends('layouts.site_layout')
@section('content')
        <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/1.jpg') ;">  
            <div class="section-hero-page-mask"></div>
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">Ajukan Operator Sobat Untuk Mempermudah Akses ke <br>Tempat Tujuan Anda</h1>
                    <p class="section-hero-page-subtitle">Ajukan operator atau tujuan yang dibutuhkan</p>

                    <div class="custom-position hidden-995" style="right: 0; top: 50%;">
                        <img src="assets/images/map-pin-new.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Request Operator & Tujuan</h1>
                            </header>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-request-operator-wrapper">
                                        <h2 class="form-request-operator-wrapper-title">Formulir Pengajuan Operator</h2>

                                        <form action="" class="form-request-operator">
                                            <div class="form-request-operator-box">
                                                <h3 class="form-request-operator-title">Detil Pemesanan Order</h3>

                                                <div class="row">
                                                    <p class="form-field style-2 clearfix col-md-6">
                                                        <span class="form-field-label">Nama Lengkap</span>
                                                        <input type="text" value="John Doe" onblur="if(this.value=='')this.value='John Doe'" onfocus="if(this.value=='John Doe')this.value=''" />
                                                    </p>
                                                    <p class="col-md-12">
                                                        <span class="clearfix space space-15 hidden-995"></span>
                                                    </p>

                                                    <p class="form-field style-2 clearfix col-md-6">
                                                        <span class="form-field-label">Email</span>
                                                        <input type="email" value="johndoe@email.com" onblur="if(this.value=='')this.value='johndoe@email.com'" onfocus="if(this.value=='johndoe@email.com')this.value=''" />
                                                    </p>
                                                </div>

                                                <div class="form-field  clearfix">
                                                    <span class="form-field-label">Masukan nama operator dan tujuan yang Anda ajukan</span>
                                                    <textarea name="" id="" rows="4"></textarea>
                                                    <small class="form-field-desc">Tuliskan detail perjalanan anda disini. Contoh: berangkat dari Jakarta tgl 1/6/2016. Kedatangan di Jogja tgl 2/6/2016. Kunjungan ke Prambanan dan Borobudur tgl 3/6/2016. Berangkat balik dari Jogja tgl 4/6/2016. Kedatangan Jakarta tgl 5/6/2016.</small>
                                                </div>
                                            </div>

                                            <p class="form-field form-submit">
                                                <input type="submit" name="" value="Submit" class="btn btn-green btn-radius">
                                            </p>
                                        </form>
                                    </div><!-- / .form-request-operator-wrapper -->

                                </div>
                            </div>
                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->

@stop
