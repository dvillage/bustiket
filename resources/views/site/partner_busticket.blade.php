@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">Bergabung dengan 1000+ anggota penjualan tiket online</h1>
                    <p class="section-hero-page-subtitle">Sistem keagenan penjualan Tiket Bus Online</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Pendaftaran Mitra BUSTIKET</h1>
                            </header>

                            <p>Anda dapat meningkatkan penghasilan dengan menjual tiket bus, travel, atau shuttle secara online. Melalui fitur MITRA BUSTIKET, siapa saja dapat menjualkan tiket bus, travel, atau shuttle kapanpun dan dimanapun.</p>

                            <p>BUSTIKET.COM membuka peluang usaha ini untuk siapapun khususnya bagi Anda yang menginginkan model bisnis yang mudah, cepat dan aman.BUSTIKET.COM membuka peluang usaha ini untuk siapapun khususnya bagi Anda yang menginginkan model bisnis yang mudah, cepat dan aman.</p>
                            
                            <div class="clearfix space space-60"></div>
                            <div class="item-company">
                                <h2 class="item-company-title">Pendaftaran Mitra!</h2>

                                <div class="item-company-content">
                                    <div class="form-wrapper row">
                                        <form action="" method="post" class="form-company">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <div class="col-md-6">
                                                <h3 class="item-company-content-title">Data Keanggotaan</h3>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Alamat Email</span>
                                                    <input type="email" name="email" placeholder="example" required="">
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Password</span>
                                                    <input type="password" name="password" placeholder="example" required="">
                                                </p>
                                                
                                                <h3 class="item-company-content-title">Data Pribadi</h3>

                                                <div class="row">
                                                    <p class="form-field style-2 col-md-6">
                                                        <span class="form-field-label">Nama</span>
                                                        <input type="text" name="first_name" placeholder="Nama Depan" required="">    
                                                    </p>

                                                    <p class="form-field style-2 col-md-6">
                                                        <span class="form-field-label hidden-995">&nbsp;</span>
                                                        <input type="text" name="last_name" placeholder="Nama Belakang" required="">    
                                                    </p>

                                                    <p class="form-field style-2 col-md-6">
                                                        <span class="form-field-label">Tanggal Lahir</span>
                                                        <input type="text" name="date" class="datepicker" placeholder="Tanggal Lahir eg. 25/01/1980" required="">  
                                                    </p>

                                                    <p class="form-field style-2 col-md-6">
                                                        <span class="form-field-label">Jenis kelamin</span>
                                                        <select name="gender" id="" class="select-border">
                                                            <option value="pria">Pria</option>

                                                            <option value="wanita">Wanita</option>
                                                        </select>
                                                    </p>
                                                    <span class="clearfix"></span>

                                                    <p class="form-field style-2 col-md-4">
                                                        <span class="form-field-label">Nomor Identitas</span>

                                                        <select name="proof_type" id="" class="form-field-one-third">
                                                            <option value="ktp">KTP</option>
                                                            <option value="sim">SIM</option>
                                                        </select>
                                                    </p>

                                                    <p class="form-field style-2 col-md-8">
                                                        <span class="form-field-label hidden-995">&nbsp;</span>

                                                        <input type="number" name="proof_number" placeholder="No Identitas" required="">
                                                    </p>
                                                </div>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">No. Telpon</span>

                                                    <input type="tel" name="mobile" placeholder="0812 1234751" pattern="(\+?[0-9]).{9,}" title="minimum 10 digits required" required="">
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Alamat</span>
                                                    <input type="text" name="address1" placeholder="Jl. xyz RT 01 RW 01" required="">
                                                    <input type="text" name="address2" placeholder="South Jakarta, Jakarta, Indonesia">
                                                </p>

                                                <p class="form-field style-2 clearfix">
                                                    <span class="form-field-label">Lokasi</span>
                                                    <select name="location" id="" class="form-field-half">
                                                        <option value="jakarta">Jakarta</option>
                                                        <option value="jawa">Jawa</option>
                                                        <option value="bali">Bali</option>
                                                        <option value="madura">Madura</option>
                                                        <option value="sumatera">Sumatera</option>
                                                        <option value="kalimantan">Kalimantan</option>
                                                        <option value="sulawesi">Sulawesi</option>
                                                    </select>
                                                </p>
                                            </div>
                                                    
                                            <div class="col-md-6">
                                                <div class="clearfix space space-30 showin-995"></div>
                                                <h3 class="item-company-content-title">Data Perusahaan / Badan Usaha Anda</h3>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nama Perusahaan</span>
                                                    <input type="text" name="company_name" placeholder="John Doe" required="">
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Alamat Perusahaan</span>
                                                    <input type="text" name="company_address1" placeholder="Jl. xyz RT 01 RW 01" required="">
                                                    <input type="text" name="company_address2" placeholder="South Jakarta, Jakarta, Indonesia">
                                                </p>

                                                <p class="form-field style-2">
                                                    <span class="form-field-label">No. Telpon Perusahaan</span>
                                                    <input type="tel" name="company_mobile" placeholder="0812 1234751" pattern="(\+?[0-9]).{9,}" title="minimum 10 digits required" required="">
                                                </p>

                                                <p class="form-field form-submit text-right">
                                                    <input type="submit" name="submit" class="btn btn-radius btn-green" value="DAFTAR">
                                                </p>
                                                <div class="alert alert-success" id="onmail" >
                                                    <strong>{{ $body['msg'] }}</strong> 
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
            <?php 
//            dd(\Session::get('msg'));
            \Session::forget('msg');
            ?>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
        var date = $('.datepicker').datepicker({
             dateFormat:'dd/mm/yy'
        });
        </script>
        <style>
            #onmail{
                background-color: white;
                color: green;
            }
        </style>
@stop