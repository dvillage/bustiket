
Dear {{$data['subscriber_name']}}
<br><br>Terima Kasih telah melakukan pemesanan penyewaan bus. Detail pemesanan Anda adalah sebagai berikut
<br>Nama Pemesan       :   {{$data['subscriber_name']}}
<br>Telp / Hp          :   {{$data['mobile']}}
<br>Email              :   {{$data['email']}}
<br>Nama Perusahaan    :   {{$data['company_name']}}
<br>
<br>Alamat Penjemputan  :   {{$data['pickup_address']}}
<br>Waktu Keberangkatan :   {{$data['order_date']}}
<br>Jenis Layanan       :   {{$data['order_service_type']}}
<br>Tujuan       :   {{$data['comment']}}
<br>Catatan       :   {{$data['order']}}
<br>Jumlah Bus       :   {{$data['total_bus']}}
<br>
<br>Bus pilihan anda :
<br>
<?php 
$i=1;
foreach ($data['buses'] as $key => $val)
{
   echo '<br>'.$i.' '.$val['bus']['sp']['first_name'].' '.$val['bus']['sp']['last_name'].' '.$val['bus']['type'].', '.($val['bus']['ac'] ? 'AC' : 'Non AC');
   $i++;
}

?>

<br><br>Tim sales Kami akan segera menghubungi Anda dalam waktu maksimal 3 jam pada hari kerja dan maks 1X24 jam pada hari libur
<br>Apabila Anda membutuhkan informasi lebih lanjut silakan hubungi customer service kami dengan tel : 0812-8000-3919
<br> via livechat di website WWW.BUSTIKET.COM.
<br><br>Terima Kasih
<br><br>WWW.BUSTIKET.COM