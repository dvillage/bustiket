@extends('layouts.site_layout')
@section('content')
<main class="site-main">
        <section class="section section-hero-page section-hero-karir have-content hidden-995" style="background-image: url('assets/images/heropages/karir.jpg') ;">  
            <div class="section-hero-page-content ">
                <div class="container">
                    <h3 class="section-hero-page-title">#GoFurtherFaster</h3>
                    <p class="section-hero-page-subtitle">Explore Opportunities</p>
                </div>
            </div>
        </section>

        <section class="section section-why-join-us clearfix">
            <div class="container">
                
                <header class="section-header">
                    <h2>Why Join Us</h2>
                </header>

                <div class="row section-content">
                    
                    <div class="col-sm-4">
                        <div class="item-why-join-us">
                            <span class="icon-wrapper">
                                <span class="sprite icon-top-talent"></span>
                            </span>
                            <h3>Top Talent, High Standard</h3>
                            <p>Be part of a travel ecommerce company for buses who helps shaping the future of the bus industry in Indonesia for the better.</p>
                        </div>
                    </div><!-- / .item-why-join-us -->


                    <div class="col-sm-4">
                        <div class="item-why-join-us">
                            <span class="icon-wrapper">
                                <span class="sprite icon-take-bold"></span>
                            </span>
                            <h3>Take Bold Steps</h3>
                            <p>We move fast to do things that have never been done. Because you have to take risks if you want to build  the next big idea.</p>
                        </div>
                    </div><!-- / .item-why-join-us -->

                    <div class="col-sm-4">
                        <div class="item-why-join-us">
                            <span class="icon-wrapper">
                                <span class="sprite icon-growth-oprtunity"></span>
                            </span>
                            <h3>Growth Opportunity</h3>
                            <p>Be a part of a startup experience in cross-functional teams and work alongside the best in your field - grow with the company into the role you wan to have</p>
                        </div>
                    </div><!-- / .item-why-join-us -->

                </div><!-- / .row .section-content -->
            </div>
        </section><!-- / .section-why-join-us -->

        <section class="section section-karir clearfix">
            <form action="" class="form-section-karir">
                <div class="container">
                    <div class="row">
                        
                        <div class="col-md-4 form-field form-field-karir clearfix">
                            <input type="text" id="search">
                            <span class="sprite icon-search"></span>
                        </div><!-- / .form-field-karir -->

                        <div class="col-md-4 form-field form-field-karir clearfix">
                            <select name="" id="departments" class="select-2 select-department" >
                                
                            </select>  
                        </div><!-- / .form-field-karir -->

                        <div class="col-md-4 form-field form-field-karir clearfix">
                            <select name="" id="location" class="select-2 select-location" >
                            </select>
                        </div><!-- / .form-field-karir -->

                    </div>
                </div>
            </form><!-- / .form-section-karir -->

<div class="panel-group karir-list" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingOne">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Web / Front End Developer</h2>
                        <p> <span class="field"> Engineering </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="DEV">Apply</button>
                    </div>
                </div>
            </div><!-- / #headingOne -->            

            <div class="karir-item-content panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne" id="collapseOne">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <p>Front-end Developer at BUSTIKET take the central roles in utilizing the full potential of web technologies to build highly usable, inter-operable, and performant web applications. You will help us build reliable, ross-platform web applications with great user experience and usability. You may also be responsible in designing, developing, and tooling of our cutting-edge web presentation frameworks, always at the forefront at what is possible with web applications.</p>

                        <p>You will work in cross-functional teams and meet great people regularly from top tier technology, consulting, product, or academic background. We work in open environment where there are no boundaries or power distance. Everyone is encouraged to speak their mind, propose ideas, influence others, and continuously grow themselves. Get the exposure to multi-aspect, collaborative, intensive startup experience and exploration of new products.</p>
                        
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                            <li>Passion in software engineering, especially in building rich applications</li>
                            <li>Experience and passion in web technologies (HTML5/CSS3/Javascript), knowledge on browser compatibility and multi-platform development</li>
                            <li>Comfortable working in server and client side of our front-end stack</li>
                            <li>Curiosity to explore creative solutions and try new things</li>
                            <li>Strong attention to detail and constant drive to learn</li>
                            <li>At least Bachelor's degree degree in Computer Science or equivalent from a reputable university</li>
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: DEV)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content #collapseOne -->

            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->

        </div><!-- / .container -->
    </div><!-- / .panel .karir-item -->

    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingTwo">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Software Engineer - Machine Learning</h2>
                        <p><span class="field"> Engineering </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="ENGML">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <p>With skill sets in artificial intelligence, machine learning, and other statistical topics, you will be placed in teams or projects with greater 
			needs for intelligent systems, such as classification, search ranking and relevance in some product teams, 
                        malicious bot detection, and fraud detection. You will collaborate with Data Analysts in designing, refining, and implementing algorithms and in system development. </p>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                                <li>Passion in software engineering with special interest in artificial intelligence, machine learning, or other statistical topics</li>
				<li>Excellent understanding of software engineering concepts, design patterns, and algorithms</li>
				<li>Excellent analytical skills and algorithmic or statistical intuition</li>
				<li>Curiosity to explore creative solutions and try new things</li>
				<li>Bachelors' degree in Computer Science or equivalent from a reputable university</li>
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE:  ENGML)</span>  
                    </div>
                </div>

            </div><!-- / .karir-item-content -->

            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
            
        </div>
    </div><!-- / .panel .karir-item -->

    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingThree">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Graphic and UI/UX Designer</h2>
                        <p> <span class="field"> Engineering </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="DSGN">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" id="collapseThree">
               <div class="row">
                    <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <ul>
                            <li>Responsible for design & development of web page, graphic, multimedia & user interface.</li>
                            <li>Creating sites contents, graphical images, icons, banner & color schemes, logos & branding.</li>
                        </ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                             <li>Minimum 1 years experience (fresh graduates are welcome).</li>
				<li>In love with anything related to movies or video.</li>
				<li>Have minimum 4 years experience in advertising agency / ecommerce as graphic design/ web design.</li>
				<li>Familiar and expert in using adobe creative suites, especially adobe photoshop and adobe illustrator.</li>
				<li>Creative and have good sense of art.</li>
				<li>Passionate in graphics, poster design, UI/UX design and thypography</li>
				<li>Smart working, self motivated & team player.</li> 
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: DSGN)</span>  
                    </div>
                </div>

            </div><!-- / .karir-item-content -->

            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
        </div>
    </div><!-- / .panel .karir-item -->
    
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingFour">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Business Analysts</h2>
                        <p> <span class="field"> Business </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="BA">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingFour -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" id="collapseFour">
                <div class="col-md-12">
                    
                    <h3>Job Description:</h3>
                        <ul>
                              <li>Able to analyze bus business, Otobus Company needs and popular destination.</li>
                              <li>Able to view and analyze passengers behavior ( what passengers need and the expectations of passengers ).</li>
                              <li>Create recommendations for internal improvement of business processes.</li>   
                              <li>Create recommendations for features to be incorporated into the system as required by the bus company, travel, shuttle and its agents.</li>
                              <li>Define configuration specifications and business analysis requirements.</li>
                              <li>Perform quality assurance.</li>
                              <li>Define reporting and alerting requirements.</li>
                              <li>Own and develop relationship with partners, working with them to optimize and enhance our integration.</li>
                              <li>Help design, document and maintain system processes.</li>
                              <li>Report on common sources of technical issues or questions. </li>
                              <li>Communicate key insights .</li>
                            
                        </ul>
                       
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                          <li>Bachelor's degree in Any Majors from top local or overseas universities ( business background, science, statistic, math, fisikawill be preferable ) .</li>
                          <li>Fresh graduate or have experiences in the same area are welcome to apply .</li>
                          <li>Minimum GPA 3.00</li>
                           <li>Competent in their field.</li>
                           <li>Have extensive knowledge.</li>
                           <li>Can use their ability in analytical to business issue with high investigation ability.</li>
                           <li>Expert in modeling or mapping about business process.</li>
                            <li>A strong track record of exceeding targets and personal goals.</li>
                            <li>Strong quantitative and qualitative analysis skills.</li>
                            <li>Strong commercial awareness and a passion for business.</li>
                            <li>A self-starter with a solution-oriented approach to challenges.</li>
                            <li>Excellent interpersonal, communication skills and can influence people at the right level.</li>
                            <li>Strong familiarity with Transportation, Service and Online Travel Agent businesses are preferred.</li>
                            

                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE:  BA)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->

            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
        </div><!-- / .panel .karir-item -->

    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingFour">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Business Devlopement</h2>
                        <p> <span class="field"> Business </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="BDM">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingFour -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" id="collapseFour">
                <div class="col-md-12">
                        <h3>Job Description</h3>    
                                                    <p>As a Business Development at BUSTIKET, you will be taking part in delivering valuable strategic initiatives and partner with key persons in the organization. </p>

                        <div class="clearfix space space-30"></div>
                        <h3>Roles & responsibilities:</h3>
                        <ul>
                              <li>Passion in system analysis, or business analysis</li>
				<li>Strong analytical skills and having the drive to be well-organized</li>
				<li>Strong sense of responsibility and good teamwork and communication skills</li>
				<li>Collaborate with various department heads to identify drivers for achieving financial performance targets from both revenue and cost perspectives.</li>
				<li>Construct financial models to continuously track results against targets</li>
				<li>Partner with Finance team to understand fully the breakdown for revenue and each cost components and ensure that relevant incentives are accounted for in the financial </li>
				<li>Develop and manage relationship with bus operators, agents and other stakeholders to ensure our pricing and inventory competitiveness as well as commission are favorable</li>
				<li>Identify key potential partners for joint promotion</li>
				<li>Develop and manage relationship with external parties for delivering joint promo and fuel growth</li>
				<li>Negotiate for exclusive deals with partners</li>
				<li>Conduct analysis required to support our strategic initiatives such as market projection, historical performance, or competitor analysis. You will also be taking part in creating analysis and reports required by investor or potential investor</li> 
                        </ul>
                        <h2>Qualifications:</h2>
			
			<ul>
				<li>Bachelor's degree in Industrial Engineering, Mathematics, Finance,  Information Management or Engineering from top local or overseas universities with min GPA 3.5 on a 4.0 scale</li>
				<li>2+ years of relevant experience is required for this role</li>
				<li>A strong track record of exceeding targets and personal goals</li>
				<li>Strong quantitative and qualitative analysis skills</li>
				<li>Strong commercial awareness and a passion for business</li>
				<li>A self-starter with a solution-oriented approach to challenges</li>
				<li>Excellent interpersonal, communication skills and can influence people at the right level</li>
				<li>Having a network of relevant partners contact in web domain and internet business or travel-related industry is a plus</li>

			</ul>
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: BDM)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->

            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
        </div><!-- / .panel .karir-item -->


    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingSix">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Advertising Promo Manager</h2>
                        <p> <span class="field"> Marketing </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="MKTG">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix" id="collapseSix">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                         <p>Generating sales across all of our online and offline platforms such as video ads, banner ads, advertising on 
							outside and inside of buses, pay tv channel, cinemas, etc. Selling face to face. Using web analytics and digital 
							programmatic platforms. Driving high-level external meetings. Building and maintaining profitable relationships. 
							Delivering pitches and generating audience led solutions to ad agencies, clients and content partners. These are just some of the things you’ll be responsible for when you join us as an 
							Advertising Sales Manager. And, you’ll do it all using your operational excellence and flair for providing first class customer service.</p>

                        <div class="clearfix space space-30"></div>
                      <h2>Qualifications:</h2>
				<p>Confident, energetic and proactive, you know how to generate business aligned to audience, current 
				and future trends, seasonality and tactical opportunities. You’re also great at utilising a market leading 
				portfolio to deliver annual revenue targets. A network of key contacts within direct clients and media agencies is a must too. So is an understanding of the advertising sales market in terms of pricing, 
				ad serving and yield management and the ability to thrive in a constantly changing environment. And, if you have digital and TV VOD/TV media sales experience, even better.</p>

                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: MKTG)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
        </div>
        <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingSeven">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>SEO & Social Media Associate</h2>
                        <p> <span class="field"> Marketing </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="SM">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" id="collapseSeven">
                <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <p>Social Media plays a vital role to increase brand awareness, expand reach and establish meaningful relationships with our customers. In this role, you need to have a passion to connect 
							with audiences and keep up with social media trends. You strongly believe in the power of social media to create impact or cause so others can follow suit.</p>
                        <span class="express">As a Social Media Associate, you will be responsible to:</span></p>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                               <li>Defining and managing the roadmap to BUSTIKET social media channels success in growth and engagement</li>
							<li>Deliver solid content and promotion strategy for social media channels</li>
							<li>Have a thorough knowledge of search ranking factors and critical updates</li>
							<li>Familiar with industry-standard bid transportation and travel</li>
							<li>Have personally built and optimized per month pay-per-click account that met or exceeded specific business rules (such as CPA goals)</li>
							<li>Have managed a team of content writers, link builders, and social media marketers</li>
							<li>Comfortable working with API’s, advanced and integrated reporting</li>
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: SM)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
        </div>
        <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingEight">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Content Writer</h2>
                        <p> <span class="field"> Internships </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="CW">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight" id="collapseEight">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                         <ul>
								<li>Support marketing & PR activities.</li>
								<li>Market research.</li>
								<li>Create useful and creative article related with Transportaion, Traveling, Bus, and Travel.</li>

							</ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                           <li>Passion in data-driven marketing and creating growth with proven track record.</li>
				<li>Bachelor's degree/ Diploma/ final year student, major in Communication or Public Relations</li>
				<li>Have strong communication skills – both written and verbal</li>
				<li>Self-driven, love challenges, have enterpreneurial spirit and get things done.</li>
				<li>Excellent communication, passion, dedication and energy</li>
				<li>Strong sense of ownership and accountability, with good attention to details.</li>
				<li>Good interpersonal skills and ability to work well in an international team environment</li>    
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: CW)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingNine">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Field Coordinator</h2>
                        <p> <span class="field"> Operations </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="FC">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine" id="collapseNine">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <ul>
								<li>Bertanggung jawab untuk dealing bersama perusahaan otobus, travel, dan shuttle serta melakukan negosiasi kontrak kerjasama penjualan online untuk mengembangkan jaringan layanan BUSTIKET di seluruh Indonesia.</li>
								<li>Membangun hubungan baik bersama perusahaan otobus, travel, dan shuttle serta melakukan fungsi support dan kegiatan strategis lainnya.</li>
								<li>Mengumpulkan data yang berkaitan dengan perjalanan perusahaan otobus, travel, dan shuttle serta data penunjang yang dibutuhkan</li>
								<li>Melakukan kegiatan marketing strategis untuk meningkatkan penjualan tiket operator BUSTIKET.</li>

							</ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                              <li>Pendidikan minimum D3/ Sederajat dan lebih diutamakan yang memiliki pengalaman di bidang sales. Fresh Graduate may apply!</li>
				<li>Menyukai dunia teknologi digital.</li>
				<li>Memahami pasar dan kondisi lapangan perusahaan otobus, travel, dan shuttle serta memahami geografis daerah sekitar.</li>
				<li>Menyukai tantangan dan mampu bekerja di dalam kondisi yang dinamis.</li>
				<li>Memiliki kemampuan komunikasi yang baik, berkomitmen, jujur, berintegritas, dan memiliki rasa tanggung jawab terhadap pekerjaan.</li>
				<li>Memiliki SIM A dan bersedia melakukan perjalanan luar kota.</li> 
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: FC)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="headingTen">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Customer Service</h2>
                        <p> <span class="field"> Operations </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="CS">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen" id="collapseTen">
              <div class="col-md-12">
                        <h3>Job Description</h3>    
                          <ul>
								<li>Melakukan daily communication dengan operator Bus, Travel, dan shuttle terkait penjualan, pembatalan, harga tiket, dll</li>
								<li>Bertanggung jawab untuk menyediakan layanan dan online support serta tanya jawab seputar BUSTIKET kepada pengunjung.</li>
								<li>Berkoordinasi dengan data entry department terkait schedule dan availability dari tiap operator</li>
								<li>Membangun hubungan baik dengan key person dari tiap operator.</li>

							</ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                             <li>Wanita</li>
				<li>Pendidikan minimum D3/ Sederajat dan lebih diutamakan yang memiliki pengalaman di bidang customer service dan atau account manager. Fresh Graduate may apply!</li>
				<li>Memiliki kemampuan komunikasi secara verbal dan tulisan yang baik, fluently in english is additional point!</li>
				<li>Cepat belajar dan memahami pola kerja perusahaan otobus, travel, dan shuttle.</li>
				<li>Bersedia bekerja dengan sistem shift</li>
				<li>Memiliki tanggung jawab dan cakap dalam memecahkan masalah-masalah yang berkaitan dengan tugas sehari hari.</li>
				<li>Menyukai dunia teknologi digital.</li>  
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: CS)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="heading11">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Sales Freelancer</h2>
                        <p> <span class="field"> Operations </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="SF">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading11" id="collapse11">
                <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <ul>
								<li>- Mengajak dan meyakinkan agen bus, travel, dan shuttle untuk memulai penjualan online bersama BUSTIKET.COM (Gratis!)</li>
								<li>- Berkoordinasi dengan Kantor pusat BUSTIKET terkait data operator yang telah diperoleh.</li>
								<li>- Mendampingi Sales BUSTIKET untuk verifikasi data operator di daerah setempat.</li>

							</ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                            	<li>- Berusia minimal 18 tahun</li>
				<li>- Memiliki relasi dan atau koneksi dengan agen maupun operator bus, travel, dan shuttle di daerah setempat.</li>
				<li>- Bersedia  mengikuti  interview & briefing dengan datang ke kantor operasional BUSTIKET (untuk area Jabodetabek) atau melalui telepon dan atau video call untuk area luar Jabodetabek. Product knowledge akan disampaikan pada waktu briefing.</li>
				<li>- Memiliki rekening BCA untuk pembayaran komisi.</li>
			<span class="express">Daerah prioritas: </span>
					<li>-	JABODETABEK, Karawang, Bandung, Sukabumi, Tasikmalaya</li>
					<li>-	Jawa Tengah: Cirebon, Cilacap, , Blora, Brebes, Tegal, Pekalongan, Semarang, Salatiga, Jepara, Pati, Kudus, Lasem, Karanganyar, Purwodadi, Purbalingga, Rembang,Yogyakarta, Gunung Kidul, Gorontalo, Klaten, Solo, Magelang, Wonogiri, Wonosobo, Banyuwangi, Bojonegoro</li>
					<li>-	Jawa Timur: Kediri, Surabaya, Malang, Ponorogo, Sampang, Sumenep, Tuban, Tulungagung, </li>
					<li>-	Bali, Madura, Lampung, Riau</li>
					<li>-	Kalimantan: Pontianak, Banjarmasin, Palangkaraya, Pangkalan Bun, Sampit, Sangatta.</li>
					<li>-	Sulawesi: Makassar, Pare Pare, Palu, Kendari, Manado.</li>
					<li>-	Sumatera: Medan, Bukit Tinggi, Padang, Palembang, Prabumulih</li>   
                        </ul>
                        <p>Terbuka juga untuk area daerah Anda yang menurut Anda berpotensi namun tidak terdaftar di tabel di atas!</p>
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: SF)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="heading12">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Human Resource Manager</h2>
                        <p> <span class="field"> Human Resource </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="HR">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading12" id="collapse12">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                        <p>We are looking for a skilled People Operations Lead to oversee all aspects of Human Resources practices 
							and processes. You will support business needs and ensure the proper implementation of company strategy and objectives. The goal is to promote corporate values and enable business success 
							through job design, recruitment, performance management, training & development, employment cycle changes, talent management, and facilities management services.</p>
							  <li>Develop and implement HR strategies and initiatives  in alignment with the overall business strategy </li>
								<li>Work closely with the management team to develop and implement effective HR policies and processes </li>
								<li>Support current and future business needs through the development, engagement motivation and preservation of human capital </li>
								<li>Develop and monitor overall HR strategies, systems, tactics and procedures across the organization </li>
								<li>Bridge management and employee relations by addressing demands, grievances or other issues </li>
								<li>Nurture a positive working environment - Manage the recruitment and selection process </li>
								<li>Oversee and manage a performance appraisal system that drives high performance </li>
								<li>Report to management and provide decision support through HR metrics </li>
								<li>Ensure legal compliance</li>  
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                           		<li>Male or Female</li>
				<li>At least 3+ years of experience as HR </li>
				<li>Bachelors or higher degree in HR Management, Law, Political Science, General Management, Psychology or other related fields.</li>
				<li>Specialized knowledge in the following areas; Human Resources Management, Human Resources Development, Labor law, Safety.</li>
				<li>Good command both of spoken and written English is a must</li>
				<li>People oriented and results driven </li>
				<li>Demonstrable experience with HR metrics </li>
				<li>Good Knowledge of HR systems and databases </li>
				<li>Ability to architect strategy along with leadership skills </li>
				<li>Excellent active listening, negotiation and presentation skills </li>
				<li>Experience working with a startup or newly established company preferred </li>
				<li>Competence to build and effectively manage interpersonal relationships at all levels of the organization </li>

                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: HR)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="true" aria-controls="collapse12" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="heading13">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Finance Accounting</h2>
                        <p> <span class="field"> Finance </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="FNC">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading13" id="collapse13">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                         <ul>
								<li>Melakukan pengaturan keuangan perusahaan</li>
								<li>Melakukan input data keuangan ke dalam program</li>
								<li>Melakukan transaksi keuangan perusahaan</li>
								<li>Melakukan pembayaran kepada suplier</li>
								<li>Menjalin hubungan internal maupun exsternal terkait dengan aktifitas keuangan perusahaan</li>
								<li>Mengontrol aktivitas keuangan atau transaksi keuangan perusahaan</li>
								<li>Melakukan Penagihan</li>
								<li>Menerima dokumen dari exsternal maupun internal terkait dengan keuangan</li>
								<li>Melakukan verivikasi terhadap keabsahan dokumen</li>
								<li>Menyiapkan dokumen penagihan invoice atau kwitansi beserta kelengkapannya</li>

							</ul>
                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                             <li>Wanita</li>
				<li>Usia Min. 24 - 35 Tahun</li>
				<li>Pendidikan Akuntansi</li>
				<li>Memiliki pengamalan di bidang Accounting minimal 1 tahun</li>
				<li>Mampu mengoperasikan sistem komputer Ms. Office, Excel</li>
				<li>Memahami jurnal dan proses kerja accounting</li>
				<li>Memiliki kemampuan analitikal yang baik, dan Teliti</li>
				<li>Memiliki komunikasi yang aktif</li>
				<li>Tekun dan memiliki motivasi kerja yang tinggi</li>
  
                        </ul>
                        
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: FNC)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="true" aria-controls="collapse13" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    <div class="panel karir-item">
        <div class="container">
            <div class="panel-heading karir-item-header" role="tab" id="heading14">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Marketing & Analytics Internship</h2>
                        <p><span class="field"> Internships </span> <span class="dots"></span> Full-time</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="clearfix space space-15"></div>
                        <span class="place">
                            <span class="sprite icon-map-small"></span>
                            Jakarta, Indonesia
                        </span>
                        <button class="btn btn-radius btn-orange apply" data-jc="INTRN">Apply</button>
                    </div>
                </div>
            </div><!-- / karir-item-header #headingOne -->        

            <div class="karir-item-content panel-collapse collapse" role="tabpanel" aria-labelledby="heading14" id="collapse14">
               <div class="col-md-12">
                        <h3>Job Description</h3>    
                         <p>You will be joining our marketing analytics team and given an exclusive opportunity to learn data-driven marketing including developing growth strategy, marketing optimization and executing it. Our 
							teams consists of analytical, empathic, and data-driven people who boost Traveloka growth through various marketing activities. As an intern, you will be given real work experience.</p>

                        <div class="clearfix space space-30"></div>
                        <h3>Qualifications:</h3>
                        <ul>
                             <li>Currently pursuing a Bachelor Degree in Science, Technology, Engineering, and Mathematics from a reputable university</li>
				<li>Min GPA 3.0 and above (please attach academic transcript)</li>
				<li>Passion in data-driven marketing and creating growth</li>
				<li>Solid analytical and problem-solving skills. High-level proficiency in Excel, SQL, and data analytics is a plus</li>
				<li>Self-driven, love challenges, have entrepreneurial spirit and get things done</li>
				<li>Excellent communication, passion, dedication and energy</li>
				<li>Strong sense of ownership and accountability, with good attention to details</li>  
                        </ul>
                        <span class="express">*Please express your interest for the specific role:</span>
			<ul>
				<li>Product Marketing Intern</li>
				<li>Marketing Technology Intern</li>
				<li>Promotion & Partnership Intern</li>
				<li>Digital Marketing (SEO, SEM, and Display Ads) Intern</li>
				<li>Content Writer</li>
			</ul>
                        <div class="clearfix space space-30"></div>
                        <span class="code-info">(CODE: INTRN)</span>  
                    </div>
                </div>
            </div><!-- / .karir-item-content -->
      
            <footer class="karir-item-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse14" class="content-collapse">
                            <span class="sprite icon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            </footer><!-- / .karir-item-footer -->
    </div><!-- / .panel .karir-item -->
    </div><!-- / .panel .karir-list -->
    
    
        <div class="panel karir-item">
        <div class="container">
        <div class="form-wrapper row">     
       <form action="{{URL::to('career')}}" class="form-company" id="frmApp" method="post" enctype="multipart/form-data" >
            <div class="col-md-6 " id="applyHere">
                    <h3 class="item-company-content-title">Apply for this position</h3>

                        <p class="form-field style-2">
                            <span class="form-field-label">Full name</span>
                            <input type="text" id="appName" name="appName"  placeholder="John Doe" >
                        </p>

                        <p class="form-field style-2">
                            <span class="form-field-label">Email</span>
                            <input type="text" id="email" name="email" placeholder="jhon@gmail.com" > 
                        </p>

                        <p class="form-field style-2">
                            <span class="form-field-label">Please submit your CV and other supporting documents (Maks 5 MB)*</span>
                            <input type="file" id="resume" name="resume" >
                        </p>
                        
                        <p class="form-field style-2">
                            <span class="form-field-label">Job Code</span>
                            <input type="text" id="jobCode" name="jobCode" placeholder="DEV" >
                        </p>
                        
                        <p class="form-field style-2">
                            <span class="form-field-label">Website/ Blog / Protfolio (Write One per line your website/ blog /link protofolio)</span>
                            <textarea cols="53" rows="5" type="text" id="portfolio" name="portfolio" placeholder="" ></textarea>
                        </p>
                        
                        
                            
                        <p class="form-field text-right">
                            <input type="button" id="btnApplyCh"   class="btn btn-radius btn-green" value="Apply">
                            <input type="submit" id="btnApply"  style="display: none" class="btn btn-radius btn-green" value="Apply">
                            <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                        </p>
                        
                        <?php if (isset($body['msg']) && isset($body['msg']['msg']) ) { 
                            if ($body['msg']['flag'] == 1) { 
                                ?>
                                <div class="alert alert-success">
                                    {{ $body['msg']['msg'] }}
                                </div>
                            <?php } 
                            else if ($body['msg']['flag'] == 0) 
                            { ?>
                                <div class="alert alert-danger">
                                    {{ $body['msg']['msg'] }}
                                </div>
                        <?php  } ?>
                        <?php }?>
                        
                        {{csrf_field()}}
                       
         </div>
              </form>
        </div>

        </div>

    </div><!-- / .panel .karir-item -->
    

    <script>
        
        var fields = ['All Departments'];
        var locations = ['All Locations'];
        
        $('.field').each(function(){
            var val = $(this).html().trim();
            var exist = fieldExist(val);
            if(exist == 0){
                fields.push(val);
            }
            
        });
        $('.place').each(function(){
            var val = $(this).text().trim();
            var exist = placeExist(val);
            if(exist == 0){
                locations.push(val);
            }
            
        });
        
        console.log(fields);
        
        function fieldExist(val){
            for(i in fields){
                if(fields[i] == val){
                    return 1;
                }
            }
            return 0;
        }
        function placeExist(val){
            for(i in locations){
                if(locations[i] == val){
                    return 1;
                }
            }
            return 0;
        }
        
        for(var i in fields){
            $('#departments').append('<option value="'+fields[i]+'">'+fields[i]+'</option>');
        }
        for(var i in locations){
            $('#location').append('<option value="'+locations[i]+'">'+locations[i]+'</option>');
        }
        
        
        $('#departments').change(function(){
            var dep = $(this).val();
            var loc = $('#location').val();
            var search = $('#search').val();
            
            filterList(search,dep,loc);
        });
        $('#search').keyup(function(){
            var search = $(this).val();
            var loc = $('#location').val();
            var dep = $('#departments').val();
            
            filterList(search,dep,loc);
        });
        $('#location').change(function(){
            var loc = $(this).val();
            var search = $('#search').val();
            var dep = $('#departments').val();
            
            filterList(search,dep,loc);
        });
        
        
        function filterList(search,dep,loc){
            
            var panel = $('.karir-list .karir-item');
            var l = panel.length;
            console.log(l);
            
            for(i=0;i<l;i++){
                var s_flag =0, d_flag = 0, l_flag = 0;
                
                var dp = $(panel[i]).find('.field').html().trim();
                if(dep == 'All Departments'){
                    d_flag = 1;
                }else if(dp == dep){
                    d_flag = 1;
                }
                
                var head = $(panel[i]).find('.panel-heading h2').text().trim().toLowerCase();
//                console.log(head + ' <--> '+search.toLowerCase());
                if(search == ''){
                    s_flag = 1;
                }else if ( head.indexOf(search.toLowerCase()) > -1 ) {
                    s_flag = 1;
                }
                
                var place = $(panel[i]).find('.place').text().trim();
                
                if(loc == 'All Locations'){
                    l_flag = 1;
                }else if(place == loc){
                    l_flag = 1;
                }
                
//                console.log(place);
                if(s_flag == 1 && d_flag == 1 && l_flag ==1){
                    $(panel[i]).css({display:'block'});
                }else{
                    $(panel[i]).css({display:'none'});
                }
            }
        }
        
        function validateEmail(sEmail) {
           
        var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
        if (filter.test(sEmail)) {
            return true;
            
        }
        else {
            return false;
            
        }
        }
        $(function(){
            var jcode;
            $(".apply").click(function(){
                jcode=$(this).data('jc');
                $("#jobCode").val(jcode);
                $('html, body').animate({
                scrollTop: $("#applyHere").offset().top-100
                }, 1000, 'easeOutExpo');
            });
             
             
                 $("#btnApplyCh").click(function(){
                    $("#btnApplyCh").hide();
                    $("#btnS").show();
 
                    var appName=$("#appName").val();
                    var resume=$("#resume").val();
                    
                    var email=$("#email").val();
                    var jobcode=$("#jobCode").val();
                    var pf=$("#portfolio").val();
                   
                    var flag=1;   

                     if(appName==''){
                          $("#appName").focus();
                          $("#appName").css('border-color','red');
                          flag=0;
                      }else{
                             $("#appName").css('border-color','black');
                      }
                      
                      
                      if(validateEmail(email)){
                           $("#email").css('border-color','black');
                      }else{
                            
                             $("#email").focus();
                              $("#email").css('border-color','red');
                            flag=0;
                      }


                      if(resume==''){
                          $("#resume").focus();
                          $("#resume").css('border-color','red');
                          flag=0;
                      }
                      else{
                             $("#resume").css('border-color','black');
                      }

                      if(email==''){
                          $("#email").focus();
                          $("#email").css('border-color','red');
                          flag=0;
                      }else{
                             $("#email").css('border-color','black');
                      }

                      if(jobcode==''){
                          $("#jobCode").focus();
                          $("#jobCode").css('border-color','red');
                          flag=0;
                      }else{
                             $("#jobCode").css('border-color','black');
                      }

                      if(pf==''){
                          $("#portfolio").focus();
                          $("#portfolio").css('border-color','red');
                          flag=0;
                      }else{
                             $("#portfolio").css('border-color','black');
                      }

                    if(flag==1){
                       
                        $("#btnApply").click();
                        $("#btnS").hide();
                        $(this).show();
                        $("#btnS").hide();
                        
                       
                    }
                    else{
                        $(this).show();
                        $("#btnS").hide();
                       
                    }
                    

              
        });     
                    

       });
       
</script>	

        </section><!-- / .section-karir -->

@stop