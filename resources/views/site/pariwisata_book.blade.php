@extends('layouts.site_layout')

@section('content')
<?php
$buses = $body['data']['bus_detail'];
$total_bus = $body['data']['total_bus'];
?>
<main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">Pesan Tiket Bus Kapan saja dan dari mana saja</h1>
                    <p class="section-hero-page-subtitle">Semuanya bisa Anda lakukan dengan mudah melalui BUSTIKET.com</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-booking">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single article-booking clearfix">
                            <header class="article-header">
                                <h1>Mohon Review Pesanan Anda</h1>
                            </header>

                            <div class="item item-booking clearfix">
                                <h2 class="item-title item-booking-title">Bus Pilihan Anda</h2>

                                <div class="item-booking-content with-box one-in-six">
                                    <ol class="item-booking-list-penumpang">
                                        <?php
                                        foreach($buses as $bus){
                                        ?>
                                        <li>
                                            <h3 class="item-booking-content-title">{{$bus['sp_name']}}</h3>
                                            <ul>
                                                <li>{{$bus['type']}}</li>
                                                <li>{{$bus['ac']}}</li>
                                            </ul>
                                        </li>
                                        <?php } ?>
                                    </ol>
                                </div>
                            </div><!-- / .item-booking -->

                            <div class="item item-booking clearfix">
                                <div class="item-booking-content with-box">
                                    <h3 class="item-booking-content-title bigger">Detail Pemesanan Order</h3>

                                    <form action="" class="item-booking-form row clearfix" >
                                        <input type="hidden" name="all_bus" value="{{json_encode($buses)}}">
                                        
                                        <div class="form-field col-md-6 style-2 clearfix">
                                            <span class="form-field-label">Nama Pemesanan</span>
                                            <!--<input type="text" value="John Doe" onblur="if(this.value=='')this.value='John Doe'" onfocus="if(this.value=='John Doe')this.value=''" required=""/>-->
                                            <input id="booker_name" name="booker_name" type="text" value="" placeholder="John Doe" required=""/>
                                            
                                        </div>

                                        <div class="form-field col-md-6 style-2 nopadd-right clearfix">
                                            <span class="form-field-label">Email</span>
                                            <!--<input type="email" value="johndoe@gmail.com" onblur="if(this.value=='')this.value='johndoe@gmail.com'" onfocus="if(this.value=='johndoe@gmail.com')this.value=''" required/>-->
                                            <input id="booker_email" name="booker_email" type="email" value="" placeholder="johndoe@gmail.com" required/>
                                        </div>

                                        <div class="form-field col-md-6 style-2 clearfix nospace-bottom">
                                            <span class="form-field-label">Nama Perusahaan/Institusi</span>
                                            <!--<input type="text" value="PT. BUSTIKET" onblur="if(this.value=='')this.value='PT. BUSTIKET'" onfocus="if(this.value=='PT. BUSTIKET')this.value=''" />-->
                                            <input type="text" name="institue_name" value="" placeholder="PT. BUSTIKET" />
                                        </div>

                                        <div class="form-field col-md-6 style-2 clearfix nospace-bottom">
                                            <span class="form-field-label">Telepon</span>
                                            <!--<input type="tel" value="+62812 345678" onblur="if(this.value=='')this.value='+62812 345678'" onfocus="if(this.value=='+62812 345678')this.value=''" required/>-->
                                            <input type="tel" id="mobile" name="mobile" value="" placeholder="+62812 345678" required/>
                                        </div>

                                    </form>
                                </div>
                            </div><!-- / .item-booking -->

                            <div class="item item-booking clearfix">
                                <div class="item-booking-content with-box">
                                    <h3 class="item-booking-content-title bigger">Detail Perjalanan</h3>
                                    <form action="" class="item-booking-form row clearfix" >
                                        <div class="form-field col-md-6 style-2 clearfix">
                                            <span class="form-field-label">Alamat Penjemputan</span>
                                            <input type="text" id="pickup_address" name="pickup_address" value="" placeholder="STC Senayan" required>

                                            <fieldset class="clearfix">
                                                <input type="radio" name="service_type" value="Drop/Antar Jemput" id="drop">
                                                <label for="drop" class="pull-left">Drop/Antar Jemput</label>

                                                <input type="radio"  name="service_type" id="pariwisata" value="Pariwisata" checked>
                                                <label for="pariwisata" class="pull-right">Pariwisata</label>
                                            </fieldset>
                                        </div>

                                        <div class="form-field col-md-6 style-2 clearfix">
                                            <span class="form-field-label">Tanggal/Jam</span>
                                            <input type="text" id="date" name="date" value="" placeholder="8 Agustus 2017/14:00" class="datepicker" required>
                                        </div>

                                        <div class="form-field col-md-12 style-2 clearfix">
                                            <span class="form-field-label">Rute Tujuan</span>
                                            <textarea name="destination_route" id="destination_route" rows="4" required></textarea>
                                            <small class="form-field-desc">Tuliskan detail perjalanan anda disini. Contoh: berangkat dari jakarta tgl 1/6/2016. Kedatangan di Jogja tgl 2/6/2016. Kunjungan ke Prambanan dan Borobudur rgl 3/6/2016. Berangkat balik dari jogja tgl 4/6/2016. Kedatangan Jakarta tgl 5/6/2016.</small>
                                        </div>

                                        <div class="form-field col-md-12 style-2 clearfix">
                                            <span class="form-field-label" required>Catatan</span>
                                            <textarea name="notes" id="notes" rows="4"></textarea>
                                            <small class="form-field-desc">Tuliskan Kriteria Kendaraan permintaan anda.</small>
                                        </div>

                                    </form>
                                </div>
                            </div><!-- / .item-booking -->

                            <div class="item item-booking clearfix">
                                <div class="item-booking-content without-box">
                                    <form action="" class="item-booking-form" >
                                        <div class="form-field clearfix">
                                            <span class="form-field-label">Jumlah Bus</span>

                                            <fieldset class="field-merged ">
                                                <input type="number" id="total_bus" name="total_bus" placeholder="0" value="{{$total_bus}}" class="col-xs-9" required>
                                                <span class="input-info col-xs-3">Bus</span>
                                            </fieldset>

                                            <fieldset>
                                                <input type="checkbox" name="check_accept_multiple" id="setuju-hari" class="checkbox-circle">
                                                <label for="setuju-hari">Saya bersedia menggunakan armada dari 2 operator atau lebih</label>
                                            </fieldset>
                                        </div>
                                        <div class="form-field clearfix" style="display: none">
                                            <span class="form-field-label">Tipe Bus</span>
                                                <select id="bus_type" name="bus_type">
                                                    <option value="Mini Bus">Mini Bus</option>
                                                    <option value="Big Bus">Big Bus</option>
                                                </select>
                                        </div>
                                    </form>
                                </div>

                                <div class="item-booking-content with-box">
                                    <h3 class="item-booking-content-title bigger">Syarat dan Ketentuan</h3>

                                    <ul class="list-number">
                                        <li>Booking sewa min. 10 hari kerja (30 Hari sebelumnya Lebih baik)</li>
                                        <li>Booking sewa min. 20 hari kerja khusus bis executive</li>
                                        <li>Booking fee 30% dari harga sewa bis</li>
                                        <li>Pelunasan Min 4 hari sebelum hari H ( keberangkatan )</li>
                                        <li>Pembatalan setelah DP / Konfirmasi dikenakan charge sebesar 50% dari harga sewa</li>
                                        <li>Pembatalan setelah bus terkirim dikenakan charge sebesar 100% dari harga sewa ( Hangus )</li>
                                        <li>Perubahan Tanggal / Hari Setelah DP dikenakan Biaya Administrasi 30 % dari Total Harga Sewa.</li>
                                    </ul>

                                </div>
                            </div>

                            <div class="item item-booking clearfix">
                                <div class="item-booking-content without-box nospace-top">
                                    <form action="{{URL::to('pariwisata-processing')}}" method="post" class="item-booking-form" >
                                        <input type="hidden" id="" name="_token" value="{{csrf_token()}}">
                                        <div class="form-field">
                                            <fieldset>
                                                <input type="checkbox" name="agree" id="setuju-syaratketentuan" class="checkbox-circle" >
                                                <label for="setuju-syaratketentuan">Saya setuju dengan syarat dan ketentuan yang tertera diatas.</label>
                                            </fieldset>
                                        </div>
                                        <div class="clearfix space space-30"></div>
                                        <div class="form-field form-submit">
                                            <input type="submit" id="submit" class="btn btn-green btn-radius btn-minwidth" value="Pesan Bus">
                                        </div>
                                    </form>
                                </div>
                            </div><!-- / .item-booking -->
                        </article>
                        <div class="alert alert-danger altmsg" style="display: none;margin-top: 40px;">
                            <strong>Required !</strong> <span id="msg1"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content <--></-->

    </main><!-- / .site-main .site-main-login -->
    <script>
        /*$.datepicker.regional['ina'] = {
        monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        dayNamesShort: ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
        dayNamesMin: ['Mi', 'Se', 'Sa', 'Ra', 'Ka', 'Ju', 'Su'],
        dateFormat: 'dd MM, yy', firstDate: 0,
    }

	 $.datepicker.setDefaults($.datepicker.regional['ina']);
        	$(".datepicker").datepicker({
	
                    dateFormat: 'D M dd yy',
                    onSelect: function(datetext){
                        var d = new Date(); // for now
                        datetext=datetext+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
                        $('.datepicker').val(datetext);
                    },
                });*/
    var d = new Date();
    var g = new Date();
    var f = d.setDate(d.getDate() + 3);
        g.setDate(g.getDate() + 3);
    $(".datepicker").datetimepicker({
            dayOfWeekStart : 1,
            lang:'en',
            disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
            minDate:f,
            startDate:f,
        });
    $('#submit').click(function(){
        var booker_name = $('#booker_name').val();
        var booker_email = $('#booker_email').val();
        var mobile = $('#mobile').val();
        var pickup_address = $('#pickup_address').val();
        var date = $('#date').val();
        var destination_route = $('#destination_route').val();
        var notes = $('#notes').val();
        var total_bus = $('#total_bus').val();
        var agree = $('#setuju-syaratketentuan');
        var msg = '';
        var id = '';
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var mreg = /^[0-9]+$/;
        var con = mobile;
        var three = con.substring(0, 3);
        if(booker_name == ''){
            msg = 'please enter booker name';
            $('#booker_name').focus();
        }else if(booker_email == ''){
            msg = 'please enter booker email';
            $('#booker_email').focus();
        }else if(!regex.test(booker_email)){
            msg = 'please enter valid booker email';
            $('#booker_email').focus();
        }else if(mobile == ''){
            msg = 'please enter booker mobile';
            $('#mobile').focus();
        }else if(con.length < 10){
                msg = 'mobile number should have minimum 10 digits';
                $('#mobile').focus();
        }else if(!mreg.test(con)){
                msg = 'mobile number should have digits only';
                $('#mobile').focus();
        }else if(pickup_address == ''){
            msg = 'please enter pickup address';
            $('#pickup_address').focus();
        }else if(date == ''){
            msg = 'please enter date';
            $('#date').focus();
        }else if(destination_route == ''){
            msg = 'please enter destination route';
            $('#destination_route').focus();
        }else if(notes == ''){
            msg = 'please enter notes';
            $('#notes').focus();
        }else if(total_bus == ''){
            msg = 'please enter total bus';
            $('#total_bus').focus();
        }else if(!agree.is(':checked')){
            msg = 'please check agree button';
            $('#setuju-syaratketentuan').focus();
        }
        
        if(msg != ''){
            $('.altmsg').css({display:'block'});
            $('#msg1').html(msg);
            return false;
        }else{
//            $('#other_data').val($('.item-booking-form').serialize());
            
            $('.altmsg').css({display:'none'});
            $('#msg1').html(msg);
            
            $.ajax({
                url  :"{{URL::to('pariwisata-processing')}}" ,
                type : 'POST',
                data : $('.item-booking-form').serialize(),
                success : function(res) {
                    if(res.flag == 1){
//                        alert('pariwisata orderd sucessfully !!.');
                        window.location = '{{URL::to("pariwisata-success")}}'+'/'+booker_email;
                    }
                    
//                    window.location.replace(action);
                }
            });
            return false;
            
            return true;
        }
    });
    </script>
    @endsection