@extends('layouts.pay_layout')
@section('content')
<?php // dd($body['data']);
//    $mydate=$body['data']["dep_date"];
    $b_h = (int)(($body['data']['board_point']['boarding_minute'] / 60) % 24);
    $b_m = (int)($body['data']['board_point']['boarding_minute'] % 60);
    
    $d_h = (int)(($body['data']['drop_point']['droping_minute'] / 60) % 24);
    $d_m = (int)($body['data']['drop_point']['droping_minute'] % 60);
//    echo $b_h.':'.$b_m.' , '.$d_h.':'.$d_m;
//    dd(0);
//    $dtime = $b_h.':'.$b_m;
    $dtime = date('H:i',  strtotime($body['data']["board_point"]['boarding_datetime']));
    $ddate = date('d M y',  strtotime($body['data']["board_point"]['boarding_datetime']));
    
//    $atime = $d_h.':'.$d_m;
    $atime = date('H:i',  strtotime($body['data']["drop_point"]['droping_datetime']));
    $adate = date('d M y',  strtotime($body['data']["drop_point"]['droping_datetime']));
//    if($b_h > $d_h){
//        $adate = date('d M y',  strtotime($body['data']["drop_point"]['droping_datetime'].'+1 day'));
//    }
    
    $bdob = date('Y-m-d H:i:s', strtotime($body['data']['booker_dob'])) ;
    $jdt = date('Y-m-d', strtotime($ddate)) ;
//    $pickdt = date('Y-m-d H:i:s', strtotime($ddate.' '.$dtime)) ;
//    $dropdt = date('Y-m-d H:i:s', strtotime($adate.' '.$atime)) ;
    $pickdt =  date('Y-m-d H:i:s', strtotime($body['data']["board_point"]['boarding_datetime'])) ;
    $dropdt =  date('Y-m-d H:i:s', strtotime($body['data']["drop_point"]['droping_datetime'])) ;
//    dd($bdob);
//    dd($dtime,$ddate,$atime,$adate);
    
    $totals=$body['data']['total_amount']+$body['data']['handlefee'];
    $total=\General::number_format(($body['data']['total_amount']+$body['data']['handlefee']),3);
    $amount=\General::number_format(($body['data']['total_amount']),3);
    $handlefee=\General::number_format(($body['data']['handlefee']),3);
    
    $blist = json_decode($body['banklist'],true);
//    dd($blist);
//    $p_details['p_name'] = 
    $p_details['p_name'] = $body['data']['pass_name'];
    $p_details['s_lbl'] = $body['data']['seat_label'];
    $p_details['s_index'] = $body['data']['seat_index'];
    $p_details['p_gen'] = $body['data']['pass_gender'];
    $p_details['p_dob'] = $body['data']['pass_dob'];
    $p_details['p_no'] = $body['data']['pass_mo'];
    $pass_data = json_encode($p_details);
//    dd($pass_data);
    
?>
<script>
    var blist = <?php echo $body['banklist']; ?>;
    
    function setPayment(id){
//        alert('called');
        var p = $('#payment').val(id);
        if(id === 3 ){
           $('#paymentchannel').val('<?php echo config('constant.INTBAK_MANDIRI_PAYMENTCH'); ?>');
        }
        if(id == 1){
            $('#payment_by').val(1);
        }
    }
</script>
        <div class="site-main-content content-payment">

            <article class="article-content-payment">
                <div class="container">
                    <header class="article-header">
                        <h1>Mohon Review Pesanan Anda</h1>
                    </header>

                    <div class="row">
                        <div class="col-md-8">
                            
                            <div class="item-payment">
                                <h2 class="item-payment-title">Daftar Penumpang</h2>

                                <div class="item-payment-content content-penumpang">

                                    <?php for($i=0; $i<$body['data']['total_seat'];$i++){ ?>
                                        <ol class="list-penumpang">
                                            <h3 class="list-penumpang-title">{{$body['data']['pass_name'][$i]}} (<?php echo ($body['data']['pass_gender'][$i]) == 'm' ? 'Pria':'Wanita'; ?>)</h3>
                                            <ul>
                                                <li>Email <span>{{$body['data']["booker_email"]}}</span></li>
                                                <li>Nomor Handphone <span>{{$body['data']['pass_mo'][$i]}}</span></li>
                                                <li>Nomor Kursi <span><?php echo (isset($body['data']['seat_label'][$i]) && $body['data']['seat_label'][$i] != '') ? $body['data']['seat_label'][$i]:'Saat Keberangkatan'; ?> </span></li>
                                            </ul>
                                        </ol>
                                    <?php }?>

                                </div>
                            </div><!-- / .item-payment -->

                            <div class="item-payment">
                                <h2 class="item-payment-title">Detil Perjalanan</h2>

                                <div class="item-payment-content content-perjalanan">
                                    <ol class="list-detil-perjalanan">
                                        <h3 class="list-detil-perjalanan-title">{{$body['data']['sp_name']}}</h3>
                                        <li class="text-gray">{{$body['data']['bus_name']}} / 52 kursi</li>
                                    </ol>

                                    <ol class="list-detil-perjalanan take-in-out">
                                        <h3 class="list-detil-perjalanan-title text-green-dark">Berangkat</h3>
                                        <li><b>{{$body['data']['board_point']['b_name']}} ({{$dtime}})</b></li>
                                        <li>{{$ddate}}</li>
                                        <li>{{$body['data']['board_point']['b_city_name']}}</li>
                                    </ol>

                                    <ol class="list-detil-perjalanan take-in-out">
                                        <h3 class="list-detil-perjalanan-title text-green-dark">Tiba</h3>
                                        <li><b>{{$body['data']['drop_point']['d_name']}} ({{$atime}})</b></li>
                                        <li>{{$adate}}</li>
                                        <li>{{$body['data']['drop_point']['d_city_name']}}</li>
                                    </ol>

                                </div>
                            </div><!-- / .item-payment -->

                            <div class="item-payment">
                                <h2 class="item-payment-title">Rincian Harga</h2>

                                <div class="item-payment-content noborder nospace">
                                    <p>Silahkan periksa kembali pesanana Anda sebelum melanjutkan ke pembayaran</p>
                                    <form action="" class="form-rincianharga-voucher">
                                        <fieldset class="field-merged">
                                            <input type="text" name="couponcode" id="couponcode" placeholder="Kode Voucher" class="col-xs-9">
                                            <input type="button" name="" class="btn btn-green col-xs-3" onclick="couponDisc();" id='cpnbtn' value="Masukkan">
                                            <span id='cpnmsg' style="display: none;"></span>
                                        </fieldset>
                                    </form>
                                </div>

                                <div class="item-payment-content content-rincianharga">
                                    <ul class="list-rincianharga">
                                        <li>Pahala Kencana (dewasa x2) <span><span>Rp</span>{{$amount}}</span></li>
                                        <li>Administrasi <span><span>Rp</span> {{$handlefee}}</span></li>
                                        <li class="diskon">Potongan <span style='text-align: right;float: right;'><span style='text-align: right;'>Rp&nbsp;</span><span style='float: right;text-align: right;' id='discount_amount'>0</span></span></li>
                                        <li class="total">Harga Total <span style='text-align: right;float: right;'><span style='text-align: right;'>Rp&nbsp;</span><span style='float: right;text-align: right;' id='total_amnt'>{{$total}}</span></span></li>
                                    </ul>
                                </div>
                            </div><!-- / .item-payment -->
                        </div>
                        <input type="hidden" name="bus_id" id="bus_id" value='{{$body['data']['bus_id']}}' >

                        <div class="col-md-12">
                            <form action="{{URL::to('ticket-processing')}}" method="Post" >
                                {{csrf_field()}}
                            
                            <div class="clearfix space space-30"></div>
                            <div class="clearfix space space-15"></div>
                            <div class="item-payment">
                                <h2 class="item-payment-title">Info Pembayaran</h2>

                                <div class="item-payment-content content-list nospace clearfix">
                                    <ul class="list-tab" role="tablist">
                                        <li class="active" id="1">
                                            <a href="#bank-transfer" role="tab" data-toggle="tab" onclick='setPayment(1);' aria-controls="bank-transfer">Bank Tranfer</a>
                                        </li >
                                        <li id="2">
                                            <a href="#kartu-kredit" role="tab" data-toggle="tab" onclick='setPayment(2);' aria-controls="kartu-kredit">Kartu Kredit</a>
                                        </li>
                                        <li id="3">
                                            <a href="#internet-banking" role="tab" data-toggle="tab" onclick='setPayment(3);' aria-controls="internet-banking">Internet Banking</a>
                                        </li>
                                        <li id="4">
                                            <a href="#minimarket" role="tab" data-toggle="tab" onclick='setPayment(4);' aria-controls="minimarket">Minimarket</a>
                                        </li>
                                        <li id="5">
                                            <a href="#dokuwallet" role="tab" data-toggle="tab" onclick='setPayment(5);' aria-controls="dokuwallet">Doku wallet</a>
                                        </li>
                                    </ul><!-- / .list-tab -->

                                    <div class="tab-content">
                                        <div class="tab-pane fade tab-content-bank-transfer in active " role="tabpanel" id="bank-transfer">
                                            <h3>Kami menerima pembayaran dengan tranfer (melalui ATM/Internet Banking/SMS Banking) :</h3>

                                            <p>Mohon lakukan pembayaran transfer dari dan untuk rekening Bank yang sama. Jika Anda tidak memiliki rekening Bank yang sama dengan kami, lakukan transfer HANYA melalui ATM agar transaksi berlangsung real time. Verifikasi pembayaran tidak bisa kami lakukan jika Anda tidak mengikuti ketentuan diatas.</p>

                                            <div class="clearfix space space-30"></div>
                                            <h3>Rekening Bank</h3>
                                            <div class="form-pilih-bank">
                                                <p class="form-field">Pilih salah satu rekening kami yang akan dituju</p>
                                                <p class="form-field">
                                                    <select name="bankdetails" id="bankdetails" onchange="setDetails(this.value);">
                                                        <option selected value="{{$blist[0]['bank_short_name']}}">Bank {{$blist[0]['bank_short_name']}}</option>

                                                        <?php for($x=1;$x<count($blist);$x++){ ?>
                                                            <option value="{{$blist[$x]['bank_short_name']}}">Bank {{$blist[$x]['bank_short_name']}}</option>
                                                        <?php }?>

                                                    </select>
                                                </p>

                                                <p class="form-field result">
                                                    <span id='bname' class="block-content">{{$blist[0]['bank_name']}} - {{$blist[0]['bank_address']}}</span>
                                                    <span id='acname' class="block-content">Nama Rekening: {{$blist[0]['account_name']}}</span>
                                                    <span id='acno' class="block-content">Nomor Rekening: {{$blist[0]['account_no']}}</span>
                                                </p>

<!--                                                <p class="form-field form-submit">
                                                    <input type="submit" value="Lakukan Pembayaran" class="btn btn-green btn-radius">
                                                </p>-->

                                                <p class="form-field">Dengan klik tombol "Lakukan Pembayaran", Anda telah setuju dengan <a href="{{URL::to('term-condition')}}">Syarat dan Ketentuan</a> pelayanan BUSTIKET.</p>
                                            </div>
                                        </div><!-- / .tab-pane -->

                                        <div class="tab-pane fade tab-content-kartukredit" role="tabpanel" id="kartu-kredit">
                                            <h3 class="">MASUKAN INFO KARTU KREDIT</h3>

                                            <!--<form action="" class="form-input-kartukredit">-->
<!--                                                <p class="form-field">
                                                    <span class="form-field-label">Nomor Kartu</span>
                                                    <input type="number" name="" placeholder="4011 1111 111 1112">
                                                </p>

                                                <p class="form-field form-field-kartukredit clearfix">
                                                    <span class="form-field-label">Berlaku s.d.</span>
                                                    <span class="form-input-kartukredit-field clearfix">
                                                        <span>Bulan:</span>
                                                        <input type="month" name="">
                                                    </span>
                                                    <span class="form-input-kartukredit-field clearfix">
                                                        <span>Tahun:</span>
                                                        <input type="year" name="">
                                                    </span>
                                                </p>

                                                <p class="form-field form-field-kartukredit clearfix">
                                                    <span class="form-field-label">Nomor CCV</span>

                                                    <input type="number" name="" class="kartu-kredit">
                                                    <span class="form-field-label-desc clearfix">
                                                        <span class="sprite icon-kartu-kredit"></span>
                                                        <span>Nomor CCV adalah 3 angka terakhir yang tertera bagian belakang kartu kredit anda.</span>
                                                    </span>
                                                </p>
                                            </form>-->

                                            <h3>SYARAT DAN KETENTUAN KARTU KREDIT</h3>
                                            <ol class="list-number">
                                                <li>Kami menerima pembayaran dengan kartu kredit berlogo Visa dan Master Card</li>
                                                <li>Pastikan saldo limit kartu kredit Anda cukup untuk bertransaksi.</li>
                                                <li>Masukkan nomor kartu kredit, expiration date dan CCV.</li>
                                                <li>Pastikan detail pembayaran Anda benar dan lanjutkan ke step 3D Secure.</li>
                                                <li>One Time Password (OTP) akan dikirimkan ke nomor ponsel Anda yang telah diregistrasikan dengan kartu kredit Anda</li>
                                                <li>Masukkan OTP yang Anda telah terima di halaman 3D Secure</li>
                                                <li>Pembayaran Anda dengan kartu kredit 3D Secure selesai8</li>
                                                <li>Setelah pembayaran dianggap berhasil, Anda akan menerima E-Tiket secara otomatis ke email Anda dalam waktu beberapa menit kedepan.</li>
                                            </ol>
                                        </div><!-- / kartu-kredit -->

                                        <div class="tab-pane fade tab-content-internet-banking" role="tabpanel" id="internet-banking">
                                            <span class="sprite icon-mandiri-logo"></span>
                                            <div class="clearfix space space-15"></div>
                                            <h3>Mandiri Debit Card Number</h3>
                                            <div class="form-input-debit-card">
                                                <p class="form-field">
                                                    <span class="form-field-label">(Masukan 16 digit kartu Debit Mandiri Anda)</span>
                                                    <!--<input type="number" name="">-->
                                                </p>

                                                <p class="form-field">
                                                    <span class="form-field-label">Input untuk Token Mandiri Anda:</span>

                                                    <span class="form-input-debit-card-label">
                                                        <span class="form-input-debit-card-title">APPLI</span>
                                                        <span class="form-input-debit-card-value">: 3</span>
                                                    </span>

                                                    <span class="form-input-debit-card-label">
                                                        <span class="form-input-debit-card-title">Input 1</span>
                                                        <span class="form-input-debit-card-value">: 10 digit terakhir kartu Anda</span>
                                                    </span>

                                                    <span class="form-input-debit-card-label">
                                                        <span class="form-input-debit-card-title">Total Jumlah Transaksi</span>
                                                        <span class="form-input-debit-card-value" >: Rp <span id="internet_banking_total">{{$total}}</span> (Input 2)</span>
                                                    </span>

                                                    <span class="form-input-debit-card-label">
                                                        <span class="form-input-debit-card-title">No. Transaksi</span>
                                                        <span class="form-input-debit-card-value">: 24680 (input 3)</span>
                                                    </span>
                                                </p>

                                                <p class="form-field">
                                                    <span class="form-field-label">Respon Token Mandiri <br>Silahkan masukkan respon Token Mandiri Anda</span>

                                                    <!--<input type="text">-->
                                                </p>
                                            </div>
                                        </div><!-- / internet-banking -->

                                        <div class="tab-pane fade tab-content-minimarket" role="tabpanel" id="minimarket">
                                            <span class="sprite icon-alfamart-logo"></span>
                                            <div class="clearfix space space-15"></div>

                                            <p>Pembayaran melalui Alfamart Group:</p>
                                            <span class="clearfix space space-15"></span>
                                            <ol class="list-number list-alfa-pattern">
                                                <li>Batas waktu pembayaran adalah 45 menit. Apabila pembayaran belum lunas setelah batas  waktu pembayaran berakhir maka pemesanan tiket akan dibatalkan secara otomatis.</li>
                                                <li>Catat Kode Pembayaran di atas dan datang ke gerai Alfa Group terdekat termasuk Alfa Mart,  Alfa Midi, Alfa Express, Lawson dan Dandan.</li>
                                                <li>Datangi kasir dan sampaikan ke kasir "Ingin membayar DOKU Merchant".</li>
                                                <li>Kasir akan menanyakan kode pembayaran, berikan Kode Pembayaran dari BUSTIKET.</li>
                                                <li>Kasir akan menginformasikan Nama Merchant dan nominal yang harus dibayarkan.</li>
                                                <li>Lakukan pembayaran ke kasir sejumlah nominal harga tiket. Pembayaran dapat menggunakan uang tunai atau Non tunai antara lain Kartu Debit BCA, Kartu Debit BNI, BCA Flazz, BNI Prepaid, dan Mandiri e-money.</li>
                                                <li>Setelah pembayaran, Anda akan menerima E-Tiket secara otomatis ke email Anda dalam waktubeberapa menit kedepan.</li>
                                            </ol>
                                        </div><!-- / minimarket -->

                                        <div class="tab-pane fade tab-content-dokuwallet" role="tabpanel" id="dokuwallet">
                                            <span class="sprite icon-doku-wallet"></span>
                                            <div class="clearfix space space-15"></div>

                                            <p>Pastikan Anda memiliki ID dan password pengguna untuk Doku Wallet dan telah terdaftar</p>

<!--                                            <p>
                                                <a href="#" class="btn btn-green-dark btn-radius">Bayar Sekarang</a>
                                            </p>-->

                                            <p>Dengan klik tombol "Lakukan Pembayaran". Anda telah setuju dengan <a href="#" class="link-blue">Syarat & Ketentuan</a> dan <a href="#" class="link-blue">Kebijakan Privasi.</a></p>
                                        </div><!-- / #dokuwallet -->
                                        
                                        <input type='hidden' name='bus_id' id='bus_id' value='{{$body['data']['bus_id']}}' >
                                        <input type='hidden' name='sp_id' id='sp_id' value='{{$body['data']['sp_id']}}' >
                                        <input type='hidden' name='spname' id='spname' value='{{$body['data']['sp_name']}}' >
                                        <input type='hidden' name='user_id' id='user_id' value='0' >
                                        <input type='hidden' name='book_by' id='book_by' value='0' >
                                        <input type='hidden' name='booking_system' id='booking_system' value='0' >
                                        <input type='hidden' name='status' id='status' value='2' >
                                        <input type='hidden' name='payment_by' id='payment_by' value='1' >
                                        <input type='hidden' name='journey_date' id='journey_date' value='{{$jdt}}' >
                                        <input type='hidden' name='from_term_id' id='from_term_id' value='{{$body['data']['board_point']['b_terminal_id']}}' >
                                        <input type='hidden' name='to_term_id' id='to_term_id' value='{{$body['data']['drop_point']['d_terminal_id']}}' >
                                        <input type='hidden' name='from_term' id='from_term' value='{{$body['data']['board_point']['b_name']}}' >
                                        <input type='hidden' name='to_term' id='to_term' value='{{$body['data']['drop_point']['d_name']}}' >
                                        <input type='hidden' name='from_city' id='from_city' value='{{$body['data']['board_point']['b_city_name']}}' >
                                        <input type='hidden' name='to_city' id='to_city' value='{{$body['data']['drop_point']['d_city_name']}}' >
                                        <input type='hidden' name='from_district' id='from_district' value='{{$body['data']['board_point']['b_district_name']}}' >
                                        <input type='hidden' name='to_district' id='to_district' value='{{$body['data']['drop_point']['d_district_name']}}' >
                                        <input type='hidden' name='pickup_date' id='pickup_date' value='{{$pickdt}}' >
                                        <input type='hidden' name='drop_date' id='drop_date' value='{{$dropdt}}' >
                                        <input type='hidden' name='nos' id='nos' value='{{$body['data']['total_seat']}}' >
                                        <input type='hidden' name='total_amount' id='total_amount' value='{{$totals}}' >
                                        <input type='hidden' name='coupon_discount' id='coupon_discount' value='0' >
                                        <input type='hidden' name='handle_fee' id='handle_fee' value='{{$body['data']['handlefee']}}' >
                                        <input type='hidden' name='base_amount' id='base_amount' value='{{$body['data']['total_amount']}}' >
                                        <input type='hidden' name='coupon_code' id='coupon_code' value='' >
                                        <input type='hidden' name='booker_name' id='booker_name' value='{{$body['data']['booker_name']}}' >
                                        <input type='hidden' name='booker_email' id='booker_email' value='{{$body['data']['booker_email']}}' >
                                        <input type='hidden' name='booker_dob' id='booker_dob' value='{{$bdob}}' >
                                        <input type='hidden' name='booker_no' id='booker_no' value='{{$body['data']['booker_mo']}}' >
                                        
                                        <input type='hidden' name='payment' id='payment' value='1' >
                                        <input type="hidden" name="paymentchannelc" id="paymentchannelc" value="{{config('constant.CREDITCARD_PAYMENTCH')}}">
                                        <input type="hidden" name="paymentchanneld" id="paymentchanneld" value="{{config('constant.DOKU_PAYMENTCH')}}">
                                        <input type="hidden" name="paymentchannel" id="paymentchannel" value="">
                                        <input type="hidden" name="pass_data" id="pass_data" value="{{$pass_data}}">
                                        <input type="hidden" name="sessionid" id="sessionid" value="{{$body['data']['session_id']}}">
                                        <input type="hidden" name="b_city" id="b_city" value="{{$body['data']['board_point']['b_city_name']}}">
                                        <input type="hidden" name="d_city" id="d_city" value="{{$body['data']['drop_point']['d_city_name']}}">
                                        <input type="hidden" name="_token" id="paymentchannel" value="{{csrf_token()}}">
                                        
                                        
                                        <p class="form-field form-submit">
                                            <input type="submit" value="Lakukan Pembayaran" class="btn btn-green btn-radius" style="border: none;margin-top: 25px;">
                                        </p>
                                    </div><!-- / .tab-content -->
                                </div><!-- / .item-payment-content -->
                            </div><!-- / .item-payment -->
                            </form>
                            
                            
                            
                            <div class="clearfix space space-60 hidden-990"></div>
                            <div class="clearfix space space-30"></div>
                        </div>
                    </div>
                </div>
            </article><!-- / .article-content-payment -->

        </div><!-- / .site-main-content .content-payment -->
        
        
        
        
        
@endsection