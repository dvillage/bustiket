@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('assets/images/heropages/2.jpg') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">BUSTIKET hadir untuk menjadi mitra Anda</h1>
                    <p class="section-hero-page-subtitle">Jasa transportasi Perusahaan Otobus, shuttle, maupun travel</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Print/ SMS/ Email Tiket</h1>
                            </header>

                            <div class="item-company">
                                
                            </div><!-- / .item-company -->

                            <div class="item-company">
                               

                                <div class="item-company-content">
                                    <div class="form-wrapper row">
                                        <form action="#"  class="form-company" id="frmContact" method="post">
                                            <div class="col-md-6">
                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nomor Tiket</span>
                                                    <input type="text" id="ticketNumber" name="ticketNumber" placeholder="" >
                                                </p>
                                                <p class="form-field style-2" id="em">
                                                    <span class="form-field-label">Email</span>
                                                    <input type="text" id="email" name="email" placeholder="" >
                                                </p>
                                                <p class="form-field style-2" id="mn" style="display:none">
                                                    <span class="form-field-label">Nomor Handphone </span>
                                                    <input type="text" id="phoneNo" name="phoneNo" placeholder="" >
                                                </p>
                                                
                                                
                                                <div class="form-field style-2">
                                                   <div class="col-md-4">
                                                    <span class="form-field-label">Silikan Pilih</span>
                                                    </div> 
                                                    <div class="form-field style-2">
                                                    <span class="clearfix space space-15"></span>
                                                    <fieldset>
                                                        <input type="radio"  name="ticketType" value="print" id="print" checked="checked">
                                                        <label for="print">Cetak Tiket</label>

                                                        <input type="radio" name="ticketType" id="sms" value="sms" >
                                                        <label for="sms">Dapatkan Tiket Melalui SMS</label>

                                                        <input type="radio" name="ticketType" value="email" id="emailr">
                                                        <label for="emailr">Dapatkan Tiket Melalui Email</label>
                                                    </fieldset>
                                                    </div>
                                                    
                                                </div>
                                                <p class="form-field  text-right">
                                                    <input type="button" id="btnSubmit" name=""  class="btn btn-radius btn-green" value="Cetak Tiket">
                                                    <img  id="btnS"  style="display:none" src="{{URL::asset('assets/images/loading.gif')}}" height="50px" width="50px" />
                                                </p>
                                                <div class="alert alert-success" id="onmail" style="display: none">
                                                    <strong></strong> 
                                                </div> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
             $(function(){ 
                 var flags=0;
                 $("input:radio[name=ticketType]").change(function(){
                        if(this.value=="sms"){
                            $("#mn").show();
                            $("#em").hide();
                            $("#btnSubmit").val('kirim Pesan');
                            flags=1;
                        }else{
                           
                            $("#em").show();
                            $("#mn").hide();
                            $("#btnSubmit").val('kirim Email');
                            flags=2;
                        }
                    });
            
               $("#btnSubmit").click(function(){
                  
                        $("#btnReg").hide();
                        $("#btnS").show();

                        var ticketType=$("input:radio[name=ticketType]:checked").val();
                        var ticketNumber=$("#ticketNumber").val();
                        var email=$("#email").val();
                        var phoneNo=$("#phoneNo").val();
                        var flag=1;

                        function validateEmail(email) {   
                             var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
                             if (filter.test(email)) {
                                 return true;
                             }else {
                                 return false;
                             }
                         }
                         
                         function validateMobileNo(argMobile){
                                 var filter=/^\d{10,}$/;
                                 if(filter.test(argMobile)){
                                     return true;

                                 }else{
                                     return false;
                                 }
                         }
                             
                            if(ticketNumber==''){
                                     $("#ticketNumber").focus();
                                     $("#ticketNumber").css('border-color','red');
                                     flag=0;
                                 }else{
                                        $("#ticketNumber").css('border-color','black');
                            }

                            if(flags==1){  
                             
                                if(validateMobileNo(phoneNo)){
                                    $("#phoneNo").css('border-color','black'); 
                                }
                                else{
                                    $("#phoneNo").focus();
                                    $("#phoneNo").css('border-color','red');
                                    flag=0;
                                    alert('Telepon Tidak Harus lebih besar dari atau sama dengan 10');
                                } 
                          }

                          if(flags==2 || flags==0){
               
                              if(validateEmail($("#email").val())){
                                         $("#email").css('border-color','black');
                              }else{

                                        $("#email").focus();
                                        $("#email").css('border-color','red');
                                        flag=0;
                              }
                          }


                                 if(flag==1){
                                     
                                     var token = $("[name=csrf-token]").attr("content");
                                      $.ajax({
                                         url:'{{URL::to("check-booking")}}',
                                         type:'post',
                                         dataType:'json',
                                         data:{_token:token,bookingData:{"ticketNumber":ticketNumber,"email":email,"phoneNo":phoneNo,"flags":flags}},
                                         success:function(data){
                                             var jdata=JSON.parse(JSON.stringify(data));
                                             if(jdata.flag==1){
                                                 $("#onmail").show();
                                                 $("#onmail").text(jdata.msg);
//                                                 $("#onmail").css({backgroundColor:'#d4edda'});
                                                 $("#onmail").css({backgroundColor:'white'});
                                                 $("#onmail").css({color:'#d4edda'});
                                                 $("#frmContact")[0].reset();
                                                 $("input:radio[name=ticketType]").trigger('change');
                                                 $("#btnSubmit").show();
                                                 $("#btnS").hide();
                                             }
                                             else if(jdata.flag==0){
                                                 $("#onmail").show();
                                                 $("#onmail").text(jdata.msg);
//                                                 $("#onmail").css({backgroundColor:'red'});
                                                 $("#onmail").css({backgroundColor:'white'});
                                                 $("#onmail").css({color:'red'});
                                                 $("#btnSubmit").show();
                                                 $("#btnS").hide();
                                             }else if(jdata.flag==3){
                                                 var url = '{{URL::to("check-booking")}}?'+'ticket_id='+ticketNumber+'&email='+email;
                                                 window.location = url;
                                             }
                                         }
                                     });
                                 }
                                 else{
                                     $(this).show();
                                     $("#btnS").hide();
                                 }
               
             });                
    });    
        </script>
@stop
