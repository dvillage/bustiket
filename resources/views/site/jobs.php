<?php
ob_start();
session_start();
 
require_once("config/config.php");
include_once("includes/functions.php");
include_once("header.php"); 

$msg = '';
if(isset($_SESSION['message'])){
    $msg = $_SESSION['message'];
    unset($_SESSION['message']);
}


?>

<html>

<body>
<style>
	.mimg {
		width:100%;
		position: relative;
	}
	.logocree {
		position:absolute;
		top:0;
		width:100%;	
	}
	.one {
		padding:20px 0;
		float: left;
	}
	.career a{
		color:#fff;
		font-family:Lucida Handwriting;
		font-size:20px;
		margin:35px;
		display:inline-block;
	}
	.two ul {
		padding:0;
		margin:0;
		list-style:none;
		float:right;
	}
	.two ul li {
		display:inline-block;
	}
	.two ul li a {
		color:#fff;
		padding: 30px;
		display: block;
		font-size: 15px;
	}
	.exploer {
		bottom:5%;
		text-align: center;
		position:absolute;
		width:100%;
	}
	.boxp h1 {
		color:#fff;
		margin-bottom: 40px;
		font-size: 30px;
	}
	.boxp a {
		background-color: #f6b519;
		display: inline-block;
		padding: 6px 30px;
		color: #000;
		font-size: 14px;	
	}
	.exploer i {
		color:#fff;
		font-size:50px;
	}
	.join {
		padding:60px 0px;
	}
	.join h3 {
		color:#000;
		font-size:25px;
		//margin-bottom:30px;
	}
	.mbox {
		text-align:center;
	}
	.mbox i {
		color:#369f38;
		font-size:50px;
	}
	.mbox h4 {
		font-size:20px;
		color:#339933;
		margin:20px 0;
	}
	.mbox p {
		font-size:15px;
	}
	.greenline {
		background-color:#339933;
		padding:35px 0;
	}
	.serchin input,select {
		background: #FFF none repeat scroll 0 0;
		border: medium none;
		color: #999999;
		font-size: 13px;
		height: 40px;
		padding-left:40px;
		width: 100%;
		-webkit-appearance: none;
	}
	.serchin span {
		border: medium none;
		color: #339933;
		height:40px;
		position: absolute;
		left:15px;
		width: 40px;
		top: 0px;
		font-size: 16px;
		text-align: center;
		line-height: 40px;
	}
	.serchin input,select:focus {
		outline:0;
	}
	.alldepar input {
		padding-left:10px;
	}
	.alldepar span {
		right: 15px !important;
		left: inherit;
		font-size: 25px;
	}
	.no_p {
		padding-bottom:0;
	}
	.btn_ser a, .btn_ser span{
		padding: 10px 22px;
		background-color: #277625;
		color: #fff;
		display: inline-block;
		border-radius: 10px;
		font-size: 15px;
                cursor: pointer;
	}
	.web_font h3 {
		font-size:20px;
		margin-bottom:10px;
		color:#444;
	}
	.web_font h4 {
		color:#339933;
	}
	.web_font h2 {
		margin:15px 0;
		font-weight: bold;
	}
	.web_font p {
		color:#444;
		font-size:15px;
		margin-bottom:20px;
	}
	.web_font ul {
		margin:0;
		padding:0 0 0 20px;
		list-style:inherit;
	}
	.web_font ul li {
		padding:3px 0;
		font-size:15px;
	}
	.jakarta span {
		padding-left: 20px;
		float: left;
	}
	.jakarta span i {
		color:#c8c8c8;
		font-size:40px;
	}
	.jafont {
		font-size: 20px;
		padding-top:7px;
       
	}
	.jakarta span a {
		padding: 12px 18px;
		background-color: #ffa60a;
		color: #fff;
		font-size: 20px;
		font-weight: bold;
		border-radius: 10px;
	}
	.downi i, .upi i {
		color:#ccc;
		font-size:30px;
		display: inline-block;
		padding: 10px;
		cursor: pointer;
	}
	.bode_bo {
		border-bottom:1px solid #c5c5c5;	
	}
	.tex_left {
		display:inline-block;
	}
	.tex_left h4 {
		color:#339933;
	}
	.apply_p h3 {
		font-weight: bolder;
		color: #444;
		font-size: 20px;
		margin-bottom:30px;
	}
	.full_n {
		font-size:16px;
	}
	.apply_p input,textarea {
		background: #FFF none repeat scroll 0 0;
		border: medium none;
		color: #444;
		font-size: 14px;
		height: 40px;
		padding:5px;
		width: 100%;
		border:2px solid #ccc;
		border-radius: 6px;
		outline:0;
	}
	.apply_p button {
		padding: 10px 20px;
		background-color: #ffa60a;
		color: #fff;
		font-size: 20px;
		border: 0;
		border-radius: 10px;
	}
        .code{
            margin: 10px;
            font-size: 18px;
        }
	.green_l {
		width:80px;
		height:3px;
		display:inline-block;
		background-color:#339933;
	}
	.express {
		color: #444;
		margin: 10px 0;
		font-size: 16px;
		display: block;
	}
	
@media (max-width:990px) {
	.two ul {
		float:none;
	}
	.serchin {
		margin-bottom:15px;
	}
} 
</style>

<div class="mimg">
		<img src="img/cree.jpg" class="img-responsive" width="100%">
		<!--<div class="logocree">
			<div class="container">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="one">	
						<a href="#"><img src="img/lohoggg.png" class="img-responsive" width="120px"></a>													
					</div>
					<div class="career">
						<a href="">Career</a>
					</div>
					
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 two">
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">Vacancy</a></li>
					</ul>
				</div>
			</div>
		</div>-->
		
		<div class="exploer">
		<div class="container">
			<div class="boxp">
				<h1>#GoFurtherFaster</h1>
				<a href="#before_job_form">Explore Opportunities</a>
			</div>
			<a href=""><i class="fa fa-angle-down"></i></a>
		</div>
		</div>
</div>
<div class="container join text-center">
		<h3>Why Join Us</h3>
		<div class="green_l"></div>
		<div class="row" style="padding-top:60px;">
			<div class="col-md-4 col-sm-6 col-xs-12 mbox">
				<i class="fa fa-lightbulb-o"></i>
				<h4>Top talent, high standards</h4>
				<p>Be part of a travel ecommerce 
					company for buses who helps shaping 
					the future of the bus industry in 
					Indonesia for the better.</p>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 mbox">
				<i class="fa fa-paper-plane-o"></i>
				<h4>Take bold steps</h4>
				<p>We move fast to do things that have 
					never been done. 
					Because you have to take risks if you want 
					to build the next big idea.</p>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 mbox">
				<i class="fa fa-key"></i>
				<h4>Growth Opportunity</h4>
				<p>Be part of a startup experience in 
					cross-functional teams and work 
					alongside the best in your field - 
					grow with the company into the role 
					you want to have.</p>
			</div>
		</div>
</div>
<div id="before_job_form" class="greenline">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12 no_p">
					<div class="serchin">
						<form>
							<input placeholder="Search Jobs" type="text" id="f_qry">
							<span>
								<i class="fa fa-search"></i>
							</span>
						</form>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 no_p">
					<div class="serchin alldepar">
						<form>
							<select id="f_department">
                                                                <option value="all">All Departments</option>
                                                                <option value="Engineering">Engineering</option>
                                                                <option value="Business">Business</option>
                                                                <option value="Marketing">Marketing</option>
                                                                <option value="Internship">Internship</option>
                                                                <option value="Operations">Operations</option>
                                                                <option value="Human Resource">Human Resource</option>
                                                                <option value="Finance">Finance</option>
							</select>
							<span>
								<i class="fa fa-angle-down"></i>
							</span>
						</form>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 no_p">
					<div class="serchin alldepar">
						<form>
							<select id="f_location">
                                                            <option value="all">All Locations</option>
                                                            <option value="Jakarta">Jakarta</option>
							</select>
							<span>
								<i class="fa fa-angle-down"></i>
							</span>
						</form>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 no_p">
					<div class="btn_ser text-right">
                                            <span href="#">Search</span>
					</div>
				</div>
			</div>
		</div>
</div>
<div class="container job_container" style="padding:50px 0;">
        <div class="row" id="job_1">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Web/Front End Developer</h3>
			<h4>Engineering • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>Front-end Developer at BUSTIKET take the central roles in utilizing the full potential of web technologies to build highly usable, inter-operable, and performant web applications. 
							You will help us build reliable, cross-platform web applications with great user experience and usability. 
							You may also be responsible in designing, developing, and tooling of our cutting-edge web presentation frameworks, always at the forefront at what is possible with web applications. </p>
							
							<p>You will work in cross-functional teams and meet great people regularly from top tier technology, consulting, product, or academic background. 
							We work in open environment where there are no boundaries or power distance. Everyone is encouraged to speak their mind, propose ideas, 
							influence others, and continuously grow themselves. Get the exposure to multi-aspect, collaborative, intensive startup experience and exploration of new products.</p>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Passion in software engineering, especially in building rich applications</li>
				<li>Experience and passion in web technologies (HTML5/CSS3/Javascript), knowledge on browser compatibility and multi-platform development </li>
				<li>Comfortable working in server and client side of our front-end stack</li>
				<li>Curiosity to explore creative solutions and try new things</li>
				<li>Strong attention to detail and constant drive to learn</li>
				<li>At least Bachelor's degree degree in Computer Science or equivalent from a reputable university</li>
				
			</ul>
			           <div class="code">
                            (CODE: DEV)
                        </div>
		
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
                            <span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
                                <span style="margin-top:5px;"><a class="apply" href="#job_form">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			<div class="bode_bo">
			   <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
			</div>
		</div>
		
        </div>
    
        <div class="row" id="job_2">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Software Engineer - Machine Learning</h3>
			<h4>Engineering • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>With skill sets in artificial intelligence, machine learning, and other statistical topics, you will be placed in teams or projects with greater 
							needs for intelligent systems, such as classification, search ranking and relevance in some product teams, 
							malicious bot detection, and fraud detection. You will collaborate with Data Analysts in designing, refining, and implementing algorithms and in system development. </p>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Passion in software engineering with special interest in artificial intelligence, machine learning, or other statistical topics</li>
				<li>Excellent understanding of software engineering concepts, design patterns, and algorithms</li>
				<li>Excellent analytical skills and algorithmic or statistical intuition</li>
				<li>Curiosity to explore creative solutions and try new things</li>
				<li>Bachelors' degree in Computer Science or equivalent from a reputable university</li>
				
			</ul>
			           <div class="code">
                            (CODE: ENGML)
                        </div>
		
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			<div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
			</div>
		</div>
		
        </div>
    
        <div class="row" id="job_3">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Graphic and UI/UX Designer</h3>
			<h4>Engineering • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                             <ul>
                               <li>Responsible for design & development of web page, graphic, multimedia & user interface.</li>
                               <li>Creating sites contents, graphical images, icons, banner & color schemes, logos & branding.</li>
                            </ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Minimum 1 years experience (fresh graduates are welcome).</li>
				<li>In love with anything related to movies or video.</li>
				<li>Have minimum 4 years experience in advertising agency / ecommerce as graphic design/ web design.</li>
				<li>Familiar and expert in using adobe creative suites, especially adobe photoshop and adobe illustrator.</li>
				<li>Creative and have good sense of art.</li>
				<li>Passionate in graphics, poster design, UI/UX design and thypography</li>
				<li>Smart working, self motivated & team player.</li>
			</ul>
		              <div class="code">
                            (CODE:DSGN)
                        </div>
		
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			<div class="bode_bo">
			<div class="text-center downi"><i class="fa fa-angle-down"></i></div>
			</div>
		</div>
		
        </div>
    
        <div class="row" id="job_4">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Business Development</h3>
			<h4>Business  • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>As a Business Development at BUSTIKET, you will be taking part in delivering valuable strategic initiatives and partner with key persons in the organization. </p>
			<h2>Roles & responsibilities:</h2>
			<ul>
				<li>Passion in system analysis, or business analysis</li>
				<li>Strong analytical skills and having the drive to be well-organized</li>
				<li>Strong sense of responsibility and good teamwork and communication skills</li>
				<li>Collaborate with various department heads to identify drivers for achieving financial performance targets from both revenue and cost perspectives.</li>
				<li>Construct financial models to continuously track results against targets</li>
				<li>Partner with Finance team to understand fully the breakdown for revenue and each cost components and ensure that relevant incentives are accounted for in the financial </li>
				<li>Develop and manage relationship with bus operators, agents and other stakeholders to ensure our pricing and inventory competitiveness as well as commission are favorable</li>
				<li>Identify key potential partners for joint promotion</li>
				<li>Develop and manage relationship with external parties for delivering joint promo and fuel growth</li>
				<li>Negotiate for exclusive deals with partners</li>
				<li>Conduct analysis required to support our strategic initiatives such as market projection, historical performance, or competitor analysis. You will also be taking part in creating analysis and reports required by investor or potential investor</li>

			</ul>
			<h2>Qualifications:</h2>
			
			<ul>
				<li>Bachelor's degree in Industrial Engineering, Mathematics, Finance,  Information Management or Engineering from top local or overseas universities with min GPA 3.5 on a 4.0 scale</li>
				<li>2+ years of relevant experience is required for this role</li>
				<li>A strong track record of exceeding targets and personal goals</li>
				<li>Strong quantitative and qualitative analysis skills</li>
				<li>Strong commercial awareness and a passion for business</li>
				<li>A self-starter with a solution-oriented approach to challenges</li>
				<li>Excellent interpersonal, communication skills and can influence people at the right level</li>
				<li>Having a network of relevant partners contact in web domain and internet business or travel-related industry is a plus</li>

			</ul>
			           <div class="code">
                            (CODE: BDM)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_5">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Business Analyst</h3>
			<h4>Business  • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>Business Analyst role is to present Traveloka to hotels and sign them up to register their property for 
							bookings with Traveloka.com. The role is KPI-driven with acquisition targets, requiring substantial time spent visiting with current or potential partners.</p>
			<h2>Main Responsibilities:</h2>
				<ul>
					<li>Identify, prioritize, and ensure pricing and availability competitiveness in assigned market.</li>
					<li>Manage top accounts – own, develop and manage top accounts in assigned destination and ensure the team develops mutually beneficial accommodation property relationships to grow market share.</li>
					<li>Report & analyze – plan with line manager, implement, and monitor agreed KPIs and be able to analyze, visualize and discuss trends, issues and opportunities with team and manager.</li>
					<li>Understand the industry and its developments – very good knowledge of pros and cons of competitor models, market trends &; landscape, accommodation rate structure and yield management, connectivity options and customer booking trends.</li>
					<li>Optimize use of process and systems – Be familiar with full spectrum of business tools and ensure the team makes use of the systems.</li>
					<li>Guide the hotels to enter information into extranet to completion.</li>
					<li>Work with hotels on promotional activities to increase bookings.</li>

				</ul>
			
			<h2>Qualifications:</h2>
			
			<ul>
				<li>Bachelor’s Degree from accredited Universities.</li>
				<li>Passionate in sales and high willingness to visit customers and work outside the office.</li>
				<li>Minimum 3+ years of relevant working experiences.</li>
				<li>Excellent interpersonal and communication skills.</li>
				<li>Strong familiarity with Transportation, Service and Online Travel Agent businesses are preferred.</li>

			</ul>
		                <div class="code">
                            (CODE: BA)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_6">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Advertising Promo Manager</h3>
			<h4>Marketing • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>Generating sales across all of our online and offline platforms such as video ads, banner ads, advertising on 
							outside and inside of buses, pay tv channel, cinemas, etc. Selling face to face. Using web analytics and digital 
							programmatic platforms. Driving high-level external meetings. Building and maintaining profitable relationships. 
							Delivering pitches and generating audience led solutions to ad agencies, clients and content partners. These are just some of the things you’ll be responsible for when you join us as an 
							Advertising Sales Manager. And, you’ll do it all using your operational excellence and flair for providing first class customer service.</p>

			<h2>Qualifications:</h2>
				<p>Confident, energetic and proactive, you know how to generate business aligned to audience, current 
				and future trends, seasonality and tactical opportunities. You’re also great at utilising a market leading 
				portfolio to deliver annual revenue targets. A network of key contacts within direct clients and media agencies is a must too. So is an understanding of the advertising sales market in terms of pricing, 
				ad serving and yield management and the ability to thrive in a constantly changing environment. And, if you have digital and TV VOD/TV media sales experience, even better.</p>
			
                        <div class="code">
                            (CODE: MKTG)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_7">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>SEO & Social Media Associate</h3>
			<h4>Marketing • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>Overview</p>
							
							<p>Social Media plays a vital role to increase brand awareness, expand reach and establish meaningful relationships with our customers. In this role, you need to have a passion to connect 
							with audiences and keep up with social media trends. You strongly believe in the power of social media to create impact or cause so others can follow suit.</p>
							<span class="express">As a Social Media Associate, you will be responsible to:</span>
							<ul>
							<li>Defining and managing the roadmap to BUSTIKET social media channels success in growth and engagement</li>
							<li>Deliver solid content and promotion strategy for social media channels</li>
							<li>Have a thorough knowledge of search ranking factors and critical updates</li>
							<li>Familiar with industry-standard bid transportation and travel</li>
							<li>Have personally built and optimized per month pay-per-click account that met or exceeded specific business rules (such as CPA goals)</li>
							<li>Have managed a team of content writers, link builders, and social media marketers</li>
							<li>Comfortable working with API’s, advanced and integrated reporting</li>
							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Passion in data-driven marketing and creating growth with proven track record.</li>
				<li>Solid analytical and problem solving skills. High level proficiency in Excel, SQL and data analytics is a plus.</li>
				<li>Have an extensive background in SEO with minimum of 3 years experience</li>
				<li>Have a solid understanding of the whole digital marketing mix and how they can work together</li>
				<li>Have a clear vision of how SEO efforts can be improved quickly and efficiently</li>
				<li>Have strong time and people management skills</li>
				<li>Have a good working knowledge of Google Analytics, Google Webmaster Tools, MOZ and other SEO tools</li>
				<li>Have strong communication skills – both written and verbal</li>
				<li>Self-driven, love challenges, have enterpreneurial spirit and get things done.</li>
				<li>Excellent communication, passion, dedication and energy</li>
				<li>Strong sense of ownership and accountability, with good attention to details.</li>
				<li>Good interpersonal skills and ability to work well in an international team environment</li>
				<li>Plus: Experiences in e-commerce, online marketing or with management consultancy</li>

			</ul>
			           <div class="code">
                            (CODE: SM)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_8">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Content Writer</h3>
			<h4>Internship • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <ul>
								<li>Support marketing & PR activities.</li>
								<li>Market research.</li>
								<li>Create useful and creative article related with Transportaion, Traveling, Bus, and Travel.</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Passion in data-driven marketing and creating growth with proven track record.</li>
				<li>Bachelor's degree/ Diploma/ final year student, major in Communication or Public Relations</li>
				<li>Have strong communication skills – both written and verbal</li>
				<li>Self-driven, love challenges, have enterpreneurial spirit and get things done.</li>
				<li>Excellent communication, passion, dedication and energy</li>
				<li>Strong sense of ownership and accountability, with good attention to details.</li>
				<li>Good interpersonal skills and ability to work well in an international team environment</li>

			</ul>
		                 <div class="code">
                            (CODE: CW)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_9">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Field Coordinator</h3>
			<h4>Operations • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <ul>
								<li>Bertanggung jawab untuk dealing bersama perusahaan otobus, travel, dan shuttle serta melakukan negosiasi kontrak kerjasama penjualan online untuk mengembangkan jaringan layanan BUSTIKET di seluruh Indonesia.</li>
								<li>Membangun hubungan baik bersama perusahaan otobus, travel, dan shuttle serta melakukan fungsi support dan kegiatan strategis lainnya.</li>
								<li>Mengumpulkan data yang berkaitan dengan perjalanan perusahaan otobus, travel, dan shuttle serta data penunjang yang dibutuhkan</li>
								<li>Melakukan kegiatan marketing strategis untuk meningkatkan penjualan tiket operator BUSTIKET.</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Pendidikan minimum D3/ Sederajat dan lebih diutamakan yang memiliki pengalaman di bidang sales. Fresh Graduate may apply!</li>
				<li>Menyukai dunia teknologi digital.</li>
				<li>Memahami pasar dan kondisi lapangan perusahaan otobus, travel, dan shuttle serta memahami geografis daerah sekitar.</li>
				<li>Menyukai tantangan dan mampu bekerja di dalam kondisi yang dinamis.</li>
				<li>Memiliki kemampuan komunikasi yang baik, berkomitmen, jujur, berintegritas, dan memiliki rasa tanggung jawab terhadap pekerjaan.</li>
				<li>Memiliki SIM A dan bersedia melakukan perjalanan luar kota.</li>

			</ul>
			         <div class="code">
                            (CODE: FC)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_10">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Customer Service</h3>
			<h4>Operations • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <ul>
								<li>Melakukan daily communication dengan operator Bus, Travel, dan shuttle terkait penjualan, pembatalan, harga tiket, dll</li>
								<li>Bertanggung jawab untuk menyediakan layanan dan online support serta tanya jawab seputar BUSTIKET kepada pengunjung.</li>
								<li>Berkoordinasi dengan data entry department terkait schedule dan availability dari tiap operator</li>
								<li>Membangun hubungan baik dengan key person dari tiap operator.</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Wanita</li>
				<li>Pendidikan minimum D3/ Sederajat dan lebih diutamakan yang memiliki pengalaman di bidang customer service dan atau account manager. Fresh Graduate may apply!</li>
				<li>Memiliki kemampuan komunikasi secara verbal dan tulisan yang baik, fluently in english is additional point!</li>
				<li>Cepat belajar dan memahami pola kerja perusahaan otobus, travel, dan shuttle.</li>
				<li>Bersedia bekerja dengan sistem shift</li>
				<li>Memiliki tanggung jawab dan cakap dalam memecahkan masalah-masalah yang berkaitan dengan tugas sehari hari.</li>
				<li>Menyukai dunia teknologi digital.</li>

			</ul>
			           <div class="code">
                            (CODE: CS)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_11">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Sales Freelancer</h3>
			<h4>Operations • Freelance</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <ul>
								<li>- Mengajak dan meyakinkan agen bus, travel, dan shuttle untuk memulai penjualan online bersama BUSTIKET.COM (Gratis!)</li>
								<li>- Berkoordinasi dengan Kantor pusat BUSTIKET terkait data operator yang telah diperoleh.</li>
								<li>- Mendampingi Sales BUSTIKET untuk verifikasi data operator di daerah setempat.</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>- Berusia minimal 18 tahun</li>
				<li>- Memiliki relasi dan atau koneksi dengan agen maupun operator bus, travel, dan shuttle di daerah setempat.</li>
				<li>- Bersedia  mengikuti  interview & briefing dengan datang ke kantor operasional BUSTIKET (untuk area Jabodetabek) atau melalui telepon dan atau video call untuk area luar Jabodetabek. Product knowledge akan disampaikan pada waktu briefing.</li>
				<li>- Memiliki rekening BCA untuk pembayaran komisi.</li>
			<span class="express">Daerah prioritas: </span>
					<li>-	JABODETABEK, Karawang, Bandung, Sukabumi, Tasikmalaya</li>
					<li>-	Jawa Tengah: Cirebon, Cilacap, , Blora, Brebes, Tegal, Pekalongan, Semarang, Salatiga, Jepara, Pati, Kudus, Lasem, Karanganyar, Purwodadi, Purbalingga, Rembang,Yogyakarta, Gunung Kidul, Gorontalo, Klaten, Solo, Magelang, Wonogiri, Wonosobo, Banyuwangi, Bojonegoro</li>
					<li>-	Jawa Timur: Kediri, Surabaya, Malang, Ponorogo, Sampang, Sumenep, Tuban, Tulungagung, </li>
					<li>-	Bali, Madura, Lampung, Riau</li>
					<li>-	Kalimantan: Pontianak, Banjarmasin, Palangkaraya, Pangkalan Bun, Sampit, Sangatta.</li>
					<li>-	Sulawesi: Makassar, Pare Pare, Palu, Kendari, Manado.</li>
					<li>-	Sumatera: Medan, Bukit Tinggi, Padang, Palembang, Prabumulih</li>

			</ul>
			<p>Terbuka juga untuk area daerah Anda yang menurut Anda berpotensi namun tidak terdaftar di tabel di atas!</p>
			
                        <div class="code">
                            (CODE: SF)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_12">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Human Resource Manager</h3>
			<h4>Human Resource • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>We are looking for a skilled People Operations Lead to oversee all aspects of Human Resources practices 
							and processes. You will support business needs and ensure the proper implementation of company strategy and objectives. The goal is to promote corporate values and enable business success 
							through job design, recruitment, performance management, training & development, employment cycle changes, talent management, and facilities management services.</p>
							
							<ul>
								<li>Develop and implement HR strategies and initiatives  in alignment with the overall business strategy </li>
								<li>Work closely with the management team to develop and implement effective HR policies and processes </li>
								<li>Support current and future business needs through the development, engagement motivation and preservation of human capital </li>
								<li>Develop and monitor overall HR strategies, systems, tactics and procedures across the organization </li>
								<li>Bridge management and employee relations by addressing demands, grievances or other issues </li>
								<li>Nurture a positive working environment - Manage the recruitment and selection process </li>
								<li>Oversee and manage a performance appraisal system that drives high performance </li>
								<li>Report to management and provide decision support through HR metrics </li>
								<li>Ensure legal compliance</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Male or Female</li>
				<li>At least 3+ years of experience as HR </li>
				<li>Bachelors or higher degree in HR Management, Law, Political Science, General Management, Psychology or other related fields.</li>
				<li>Specialized knowledge in the following areas; Human Resources Management, Human Resources Development, Labor law, Safety.</li>
				<li>Good command both of spoken and written English is a must</li>
				<li>People oriented and results driven </li>
				<li>Demonstrable experience with HR metrics </li>
				<li>Good Knowledge of HR systems and databases </li>
				<li>Ability to architect strategy along with leadership skills </li>
				<li>Excellent active listening, negotiation and presentation skills </li>
				<li>Experience working with a startup or newly established company preferred </li>
				<li>Competence to build and effectively manage interpersonal relationships at all levels of the organization </li>

			</ul>
			              <div class="code">
                            (CODE: HR)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_13">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Finance Accounting</h3>
			<h4>Finance • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <ul>
								<li>Melakukan pengaturan keuangan perusahaan</li>
								<li>Melakukan input data keuangan ke dalam program</li>
								<li>Melakukan transaksi keuangan perusahaan</li>
								<li>Melakukan pembayaran kepada suplier</li>
								<li>Menjalin hubungan internal maupun exsternal terkait dengan aktifitas keuangan perusahaan</li>
								<li>Mengontrol aktivitas keuangan atau transaksi keuangan perusahaan</li>
								<li>Melakukan Penagihan</li>
								<li>Menerima dokumen dari exsternal maupun internal terkait dengan keuangan</li>
								<li>Melakukan verivikasi terhadap keabsahan dokumen</li>
								<li>Menyiapkan dokumen penagihan invoice atau kwitansi beserta kelengkapannya</li>

							</ul>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Wanita</li>
				<li>Usia Min. 24 - 35 Tahun</li>
				<li>Pendidikan Akuntansi</li>
				<li>Memiliki pengamalan di bidang Accounting minimal 1 tahun</li>
				<li>Mampu mengoperasikan sistem komputer Ms. Office, Excel</li>
				<li>Memahami jurnal dan proses kerja accounting</li>
				<li>Memiliki kemampuan analitikal yang baik, dan Teliti</li>
				<li>Memiliki komunikasi yang aktif</li>
				<li>Tekun dan memiliki motivasi kerja yang tinggi</li>

				

			</ul>
			             <div class="code">
                            (CODE: FNC)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
		<div class="row" id="job_14">
		<div class="col-md-8 col-sm-12 col-xs-12 web_font no_p">
			<h3>Marketing & Analytics Internship</h3>
			<h4>Internships • Full-time</h4>
                        <div class="description">
                            <h2>Job Description</h2>
                            <p>You will be joining our marketing analytics team and given an exclusive opportunity to learn data-driven marketing including developing growth strategy, marketing optimization and executing it. Our 
							teams consists of analytical, empathic, and data-driven people who boost Traveloka growth through various marketing activities. As an intern, you will be given real work experience.</p>

			<h2>Qualifications:</h2>
			
			<ul>
				<li>Currently pursuing a Bachelor Degree in Science, Technology, Engineering, and Mathematics from a reputable university</li>
				<li>Min GPA 3.0 and above (please attach academic transcript)</li>
				<li>Passion in data-driven marketing and creating growth</li>
				<li>Solid analytical and problem-solving skills. High-level proficiency in Excel, SQL, and data analytics is a plus</li>
				<li>Self-driven, love challenges, have entrepreneurial spirit and get things done</li>
				<li>Excellent communication, passion, dedication and energy</li>
				<li>Strong sense of ownership and accountability, with good attention to details</li>

			</ul>
			<span class="express">*Please express your interest for the specific role:</span>
			<ul>
				<li>Product Marketing Intern</li>
				<li>Marketing Technology Intern</li>
				<li>Promotion & Partnership Intern</li>
				<li>Digital Marketing (SEO, SEM, and Display Ads) Intern</li>
				<li>Content Writer</li>
			</ul>
                        <div class="code">
                            (CODE: INTRN)
                        </div>
                        </div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 text-right no_p">
			<div class="jakarta locality" style="display:inline-block;">
				<span><img src="img/location.png" /></span>
				<span class="jafont">Jakarta,Indonesia</span>
				<span style="margin-top:5px;"><a class="apply">Apply</a></span>
			</div>
		</div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
			
                        <div class="bode_bo">
                            <div class="text-center downi"><i class="fa fa-angle-down"></i></div>
                        </div>
		</div>
		
        </div>
	
</div>
<div class="container" style="padding-top:50px;" id="job_form">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12 apply_p">
			<h3>Apply for this position</h3>
                        <form method="post" action="submit_job_data.php" enctype="multipart/form-data">
				<div style="margin-bottom:20px;">
					<label class="full_n">Full Name*</label>
                                        <input placeholder="Full Name" type="text" name="full_name" id="full_name" required="">
				</div>
				<div style="margin-bottom:20px;">
					<label class="full_n">Email*</label>
                                        <input placeholder="Email" type="email" name="email" id="email" required="">
				</div>
				<div style="margin-bottom:20px;">
					<label class="full_n">Please submit your CV and other supporting documents (Maks 5 MB)*</label>
                                        <input type="file" style="border:0;" name="resume" id="resume" required="">
				</div>
				<div style="margin-bottom:20px;">
					<label class="full_n">Job Code: </label>
                                        <input placeholder="" type="text" name="url" id="url">
				</div>
				<div style="margin-bottom:20px;">
					<label class="full_n">Website/ Blog / Protfolio (Write One per line your website/ blog /link protofolio)</label>
                                        <textarea style="height:100px;" name="detailed" id="detailed"></textarea>
				</div>
				<div style="margin-bottom:20px;">
                                    <label class="full_n">Please answer : <span id="val1"> <?php $v1 = rand(1, 9); echo $v1; ?> </span> + <span id="val2"> <?php $v2 = rand(10, 99); echo $v2; ?> </span></label>
                                    <input type="hidden" name="v1" value="<?php echo $v1; ?>" />
                                    <input type="hidden" name="v2" value="<?php echo $v2; ?>" />
                                    <input placeholder="" type="text" id="total" name="total" required="">
				</div>
                            <div style="margin-bottom:20px;">
					<label class="full_n"> <?php echo $msg; ?></label>
				</div>
				<div style="margin-bottom:20px;">
                                    <button name="submit">Apply for this Position</button>
				</div>
			</form>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12"></div>
	</div>
</div>

</body>

</html>
<script>
    $(document).ready(function(){
         $('.description').hide();      
//         $('#job_1').children().children('.description').show();      
         
         $('.apply').click(function(){
             $("#full_name").focus();
         });
         $('.downi').click(function(){
//             console.log("clicked on down");
//             console.log($(this));
             var visible = $(this).hasClass('upi');
//             console.log(visible);
             if(!visible){
                 $('.description').hide('slow');    
                 $(".fa-angle-up").addClass("fa-angle-down").removeClass("fa-angle-up");
                 $(".upi").removeClass("upi");
                 $(this).parents(".row").find('.description').show('slow');
                 $(this).addClass("upi");
                 $(this).find("i").addClass("fa-angle-up").removeClass("fa-angle-down");
            }
            else{
                $('.description').hide('slow');
                $(this).removeClass("upi");
                $(this).find("i").addClass("fa-angle-down").removeClass("fa-angle-up");
            }
         });
//         $('.upi').click(function(){
//             console.log("clicked on up");
//            $('.description').hide('slow');
//            console.log($(this));
//            $(this).removeClass("upi").addClass("downi");
//            $(this).find("i").addClass("fa-angle-down").removeClass("fa-angle-up");
//         });
         $('#submit').click(function(){
             var val1 = parseInt($('#val1').val());
             var val2 = parseInt($('#val2').val());
             
             var tot = $('#total').val();
             
             if(tot != (val1 + val2)){
                 return false;
             }
         });
         
         
         $(".btn_ser").click(function(){
             var qry = $("#f_qry").val();
             qry = qry.toLowerCase();
             var department = $("#f_department").val();
             department = department.toLowerCase();
             var location = $("#f_location").val();
             location = location.toLowerCase();
//             console.log(qry);
//            console.log(department);
//            console.log(location);
//            console.log("--------------------------");
             var titles = [];
             $(".job_container .row").addClass("hidden");
             $(".job_container .row").each(function(){
                 var post = $(this).find("h3").text();
                 post = post.toLowerCase()
                 var dep = $(this).find("h4").text();
                 dep = dep.toLowerCase();
                 var loc = $(this).find(".locality").find(".jafont").text();
                 loc = loc.toLowerCase();
//                 console.log(post);
//                 console.log(dep);
//                 console.log(loc);
//                 console.log("===========================");
                 
                if(qry != "" && post.search(qry) != -1)
                {
                    if(department != "all" &&  dep.search(department) == -1)
                    {
                        $(this).addClass("hidden");
                    }
                    else if(location != "all" &&  loc.search(location) == -1)
                    {
                        $(this).addClass("hidden");
                    }
                    else
                    {
                        $(this).removeClass("hidden");
//                        console.log("showed");
//                        console.log($(this));
                    }
                }
                else if(department != "all" &&  dep.search(department) != -1)
                {
                    if(location != "all" &&  loc.search(location) == -1)
                    {
                        $(this).addClass("hidden");
                    }
                    else if(qry == "")
                    {
                        $(this).removeClass("hidden");
//                        console.log("showed as qry blank and department matched");
//                        console.log($(this));
                    }
                }
                else if(location != "all" &&  loc.search(location) != -1)
                {
                    if(department == "all" && qry == "" )
                    {
                        $(this).removeClass("hidden");
//                        console.log("showed as qry blank and department blank and locality matched");
//                        console.log($(this));
                    }
                }
                else if(location == "all" &&  department == "all" && qry == "" )
                {
                    $(this).removeClass("hidden");
//                    console.log("showed as qry blank and department blank and locality blank");
//                    console.log($(this));
                }
             });
         });
    });
</script>
 <?php include_once("includes/footer.php"); ?>
<!-- footer end here -->