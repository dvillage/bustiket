@extends('layouts.site_layout')
@section('content')
    <main class="site-main site-main-login">
        <div class="site-main-login-mask"></div>
        <section class="section section-login">
            <div class="container">

                <div class="section-login-wrapper">
                    <header class="section-header">
                        <h1 class="section-header-title">Lupa Password</h1>        
                    </header>

                    <div class="section-content">
                        <div class="form-wrapper">
                            <?php 
//                            dd($errors);
                            if(isset($errors) && $errors->first()!=''){ ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $errors->first(); ?></strong>
                                    </div>
                            <?php }
                            $session = \Session::get("msg");
//                            dd($session);
                            if($session != "" && \General::is_json($session)){ 
                                $session = json_decode($session,true);
                                \Session::forget("msg");
                                ?>
                                    <div class="alert alert-danger" style="text-align: center;">
                                        <strong><?php echo $session['msg']; ?></strong>
                                    </div>
                            <?php }
                            
                            ?>
                            <form action="{{URL::to('forgotpass')}}" method="POST" class="form-login">
                                <p class="form-field">
                                    <span class="form-field-label">Alamat Email</span>
                                    <input type="email" name="user_email" id="user_email">
                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                </p>
                                <span class="clearfix space space-30"></span>
                                <p class="form-field form-submit">
                                    <button type="submit" class="btn btn-green btn-radius btn-fullwidth">Kirim</button>
                                </p>
                                
                                <span class="clearfix space space-30"></span> 
                                <span class="clearfix space space-30"></span> 
                                

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>

@stop