@extends('layouts.site_layout')
@section('content')
        <main class="site-main">
        <section class="section section-hero-page have-content" style="background-image: url('{{URL::to('assets/images/heropages/2.jpg')}}') ;">  
            <div class="section-hero-page-content text-left">
                <div class="container">
                    <h1 class="section-hero-page-title">Bergabung dengan 1000+ anggota penjualan tiket online</h1>
                    <p class="section-hero-page-subtitle">Sistem Keagenan penjualan Tiket Bus Online</p>
                </div>
            </div>
        </section>

        <div class="site-main-content site-main-content-offset offset-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Pembatalan Tiket</h1>
                            </header>
                            
                            <div class="form-wrapper row">
                                <div class="col-md-6 col-md-offset-3">
                                    <form action="#"  class="form-company" id="frmContact" method="post">
                                            <div class="col-md-12">
                                                <p class="form-field style-2">
                                                    <span class="form-field-label">Nomor Tiket</span>
                                                    <input type="text" id="ticketNumber" name="ticketNumber" placeholder="Masukan No Tiket" >
                                                </p>
                                                <p class="form-field style-2" id="em">
                                                    <span class="form-field-label">Email</span>
                                                    <input type="text" id="email" name="email" placeholder="masukan Alamat Email" >
                                                    <span>(Alamat Email yang digunakan saat booking)</span>
                                                </p>
                                                
                                                <p class="form-field  text-right">
                                                    <input type="button" id="btnSubmit" name=""  class="btn btn-radius btn-green" value="TAMPILKAN TIKET">
                                                    <img  id="btnS"  style="display:none" src="{{URL::to('assets/images/loading.gif')}}" height="50px" width="50px" />
                                                </p>
                                                <?php
                                                $d = 'none';
                                                $m = '';
                                                $msg = \Session::get('error');
                                                if($msg){
                                                    $m = $msg;
                                                    $d = 'block';
                                                }
                                                $msgs = \Session::get('success');
                                                if($msgs){
                                                    $m = $msgs;
                                                    $d = 'block';
                                                }
                                                ?>
                                                <div class="alert alert-success" id="onmail" style="display: {{$d}}">
                                                    <strong>{{$m}}</strong> 
                                                </div>
                                                
                                            </div>
                                        </form>
                                </div>
                            </div>
                            
                            <div class="clearfix space space-60 hidden-995"></div>
                            <div class="clearfix space space-15 showin-995"></div>
                            <ol class="list-nomer">
                                <li>Pembatalan tiket oleh Penumpang yang dilakukan untuk keberangkatan pada high season atau musim liburan yang meliputi : libur nasional, libur panjang (long weekend), dan hari besar lainnya akan dikenakan biaya 100% (seratus persen) dari harga tiket atau (full charge).</li>

                                <li>Pembatalan tiket oleh Penumpang yang dilakukan lebih dari 7 (tujuh) hari sebelum keberangkatan, akan dikembalikan 50% (lima puluh persen) dari harga tiket dalam bentuk Coupon atau Voucher perjalanan. Voucher tersebut dapat digunakan untuk transaksi penumpang berikutnya dan berlaku 3 bulan sejak tanggal pembatalan.</li>

                                <li>Pembatalan tiket oleh Penumpang yang dilakukan antara 6 (enam) hari sampai dengan 4 (empat) hari sebelum keberangkatan, akan dikembalikan 75% (tujuh puluh lima persen) dari harga tiket dalam bentuk Coupon atau Voucher perjalanan. Voucher tersebut dapat digunakan untuk transaksi penumpang berikutnya dan berlaku 3 bulan sejak tanggal pembatalan.</li>

                                <li>Pembatalan tiket oleh Penumpang yang dilakukan 3 (tiga) hari sampai dengan 24 (dua puluh empat) Jam sebelum keberangkatan, akan dikenakan biaya 100% (seratus persen) dari harga tiket atau (full charge).</li>
                            </ol>
                            
                        </article><!-- / .article-single -->
                        
                    </div>
                    <div class="col-md-12" id="list_ticket">
                        
                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content site-main-content-offset -->
        <script>
             $(function(){ 
                 
            
               $("#btnSubmit").click(function(){
                        //$("#btnReg").hide();
                        $("#btnS").show();

                        var ticketNumber=$("#ticketNumber").val();
                        var email=$("#email").val();                        
                        var flag=1;

                        function validateEmail(email) {   
//                             var filter = /^[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-z]+$/;
                             var filter = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                             if (filter.test(email)) {
                                 return true;
                             }else {
                                 return false;
                             }
                         }
 
                        if(ticketNumber==''){
                                 $("#ticketNumber").focus();
                                 $("#ticketNumber").css('border-color','red');
                                 flag=0;
                             }else{
                                    $("#ticketNumber").css('border-color','black');
                        }
              
              
                
               
                        if(validateEmail($("#email").val())){
                                   $("#email").css('border-color','black');
                        }else{

                                  $("#email").focus();
                                  $("#email").css('border-color','red');
                                  flag=0;
                        }


                        if(flag==1){
//                            console.log('return');return;
                                   var token = $("[name=csrf-token]").attr("content");
                                    $.ajax({
                                       url:'{{URL::to("cancel-ticket")}}',
                                       type:'post',
//                                       dataType:'json',
                                       data:{_token:token,bookingData:{"ticketNumber":ticketNumber,"email":email}},
                                       success:function(data){
//                                           console.log(data);
                                           if(typeof data.flag === 'undefined'){
                                               $('#list_ticket').html(data);
                                               $("#onmail").hide();
                                               $("#btnS").hide();
                                               $("body").animate({scrollTop: "1250px"}, 2000);
                                               
                                           }else if(data.flag==1){
                                               $("#onmail").show();
                                               $("#onmail").text(data.msg);
                                               $("#onmail").css({backgroundColor:'#d4edda'});
//                                               $("#frmContact")[0].reset();
                                               $("#btnSubmit").show();
                                               $("#btnS").hide();
                                           }
                                           else if(data.flag==0){

                                               $("#onmail").show();
                                               $("#onmail").text(data.msg);
                                               $("#onmail").css({backgroundColor:'red'});
                                               $("#btnSubmit").show();
                                               $("#btnS").hide();
                                           }
                                       }
                                   });
                               }
                               else{
                                   $(this).show();
                                   $("#btnS").hide();
                        }

             });       
             
             $(document).on('click','#submit',function(){
                 var chks = document.getElementsByName('seatval[]');
                var hasChecked = false;
                for (var i = 0; i < chks.length; i++) {
                    if (chks[i].checked) {
                        hasChecked = true;
                        break;
                    }
                }
                if (hasChecked == false) {
                    alert("Please select at least one.");
                    return false;
                }
//                 $('#ticket_cancel').ajaxForm(function(res) {
//                    if(res.flag == 1){
//
//                        location.reload();
//                    }else{
//
//                    }
//                });
             });
    });    
        </script>

@stop