<!-- I am a banner  --><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]--><!--[if IE 8]><html class="ie ie8"><![endif]--><!--[if IE 9]><html class="ie ie9"><![endif]--><!--[if !(IE 7) & !(IE 8) & !(IE 9)]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
    <title><?php echo isset($header['title'])? $header['title']:''?></title>  

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400, 700" rel="stylesheet">
    
    <?php
//        dd($header);
        if (isset($header['css']) && count($header['css'])>0)
            for ($i = 0; $i < count($header['css']); $i++)
                if (strpos($header['css'][$i], "http://") !== FALSE)
                    echo '<link rel="stylesheet" type="text/css" href="' .$header['css'][$i] . '"/>';
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . url("/")."/" .  $header['css'][$i] . '"/>';
                
                
                echo '<script type="text/javascript" src="'.URL::to('assets/js/app.min.js').'"></script>';
                
        if (isset($header['js']) && count($header['js'])>0)
            for ($i = 0; $i < count($header['js']); $i++)
            {
                if (strpos($header['js'][$i], "http://") !== FALSE)
                    echo '<script type="text/javascript" src="' . $header['js'][$i] . '"></script>';
                else
                    echo '<script type="text/javascript" src="' . url("/")."/" . $header['js'][$i] . '"></script>';
            }
        ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<script>
    $(document).ready(function(){
        $('#{{$body['id']}}').addClass('active');    
    });

function postAjax(url,data,cb){

    var token='<?php echo csrf_token(); ?>';
    var jdata={_token:token};
    for(var k in data){
        jdata[k]=data[k];
    }
    $.ajax({
        type:'POST',
        url:url,
        data:jdata,
        success: function(data){
            if(typeof(data)==='object'){
                cb(data);
            }
            else{
                cb(data);
            }
        }
    });
}
</script>
    <input type="hidden" id="base_url" value="{{url('')}}/" />
    <header class="site-header site-header-payment">
        <div class="container">
            <div class="site-header-main pull-left clearfix">
                
                <div class="site-header-logo">
                    <a href="{{URL::to('/')}}">
                        <img src="{{url::to('assets/images/logo-header-2-new.png')}}" alt="">
                    </a>
                </div><!-- / .site-header-logo -->

                <nav class="navigation-header-proses clearfix">
                    <ul>
                        <li id='Pembayaran'><a href="#"><span class="count-proses"></span>Pembayaran</a></li>
                        <li id='Proses'><a href="#"><span class="count-proses"></span>Proses</a></li>
                        <li id='Terbit'><a href="#"><span class="count-proses"></span>E-Ticket Terbit</a></li>
                    </ul>
                </nav><!-- / .navigation-header-proses -->

            </div><!-- / .site-header-main -->
        </div>
    </header><!-- / .site-header .site-header-payment -->

    <main class="site-main">