@extends('layouts.site_layout')
@section('content')
     <main class="site-main">
        <section class="section section-hero-page" style="background-image: url('assets/images/heropages/1.jpg') ;">  
        </section>
   
    <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Tentang BUSTIKET</h1>
                            </header>

                            <p>BUSTIKET.COM adalah penyedia layanan pemesanan tiket bus Antar Kota Antar Propinsi (AKAP) dan Antar Kota Dalam Propinsi (AKDP) yang pertama dan terpercaya di Indonesia. Dirilis sejak awal 2015, BUSTIKET.COM memberikan layanan rute perjalanan ke berbagai kota di pulau Jawa, Sumatera, Bali, Madura, dan Kalimantan dalam waktu dekat. Saat ini sudah tersedia puluhan operator jasa transportasi darat baik Perusahaan Otobus (PO), shuttle dan travel yang siap melayani perjalanan Anda. BUSTIKET.COM menyediakan berbagai informasi dalam hal jadwal keberangkatan semua rute bus perharinya, beragam pilihan bus, waktu tempuh, fasilitas, harga tiket, dan nomor kursi yang dapat di pilih melalui situs ini.</p>

                            <p>Melalui BUSTIKET.COM, Anda tidak perlu capek antri di loket, hanya dengan duduk didepan komputer, tiket bus sudah bisa Anda dapatkan. Karena kepuasan pelanggan adalah prioritas utama kami, maka BUSTIKET.COM tidak hanya dapat diakses melalui website, tetapi juga tersedia dalam bentuk aplikasi Android untuk smartphone Anda.</p>

                            <p>Dengan aplikasi terbaru kami, pemesanan tiket bus dapat dilakukan kapanpun dan dimanapun. Gak capek, ga antri, hanya di BUSTIKET.COM!</p>
                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->

@stop