<?php
$data = $body['data'];

$cur_date=mktime(0, 0, 0, date("m"), date("d"),date("Y"));

     $tra=date('Y-m-d',strtotime($data['journey_date']));
     $str=explode("-",$tra);

     $tra_date=mktime(0, 0, 0, date($str[1])  , date($str[2]), date($str[0]));
    if($cur_date>$tra_date){
        $proc=0;
    }else{
       $proc=1;
    }

?>
<div class="col-sm-12" id="Ticket-div">
<div class="booking-detail" >
  <?php if($proc!=1) {  ?>
  <span class="err_msg">
  <center>
    You cannot Cancel This Ticket. Journey Date already Expired.
  </center>
  </span>
  <?php }   
  
  $boardPoint = $data['from_terminal']['name'];
  $boardTime = date("H:i", strtotime($data['pickup_date'])); 
  ?>
  <table class="table table-striped">
    <tr>
      <td class="title-bookingdata" colspan="9" nowrap="nowrap" align="center"> Rincian Pemesanan </td>
    </tr>
    <tr  >
      <td width="50">&nbsp;</td>
      <td valign="top" width="175" class="heading-data"><b>Booking ID</b></td>
      <td valign="top" width="25">:</td>
      <td valign="top" width="150"><?php echo $data['booking_id']; ?></td>
      <td valign="top" width="50"></td>
      <td valign="top" class="heading-data">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td width="50">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td valign="top" class="heading-data"><b>Operator</b></td>
      <td valign="top">:</td>
      <td valign="top"><?php echo $data['sp']['first_name'].' '.$data['sp']['last_name'] ?></td>
      <td valign="top" width="50"></td>
      <td valign="top" class="heading-data"><b>Bus</b></td>
      <td valign="top">:</td>
      <td valign="top"><?php echo $data['bus']['name']; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="50">&nbsp;</td>
      <td valign="top" width="175" class="heading-data"><b>Kota Asal</b></td>
      <td valign="top" width="25">:</td>
      <td valign="top" width="150"><?php echo strtoupper($data['from_terminal']['loc_citi']['name']); ?></td>
      <td valign="top"></td>
      <td valign="top" width="150" class="heading-data"><b>Kota Tujuan</b></td>
      <td valign="top" width="25">:</td>
      <td valign="top" width="150"><?php echo strtoupper($data['to_terminal']['loc_citi']['name']); ?></td>
      <td width="50">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"></td>
      <td valign="top" class="heading-data"><b>Tanggal Keberangkatan</b></td>
      <td valign="top">:</td>
      <td valign="top"><?php echo $tra; ?></td>
      <td valign="top"></td>
      <td valign="top" class="heading-data"><strong>Jam Keberangkatan</strong></td>
      <td valign="top">:</td>
      <td valign="top"><?php echo $boardTime; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td valign="top" class="heading-data"><strong>Tempat Keberangkatan</strong></td>
      <td valign="top">:</td>
      <td valign="top"><?php echo $boardPoint; ?></td>
      <td valign="top"></td>
      <td valign="top" class="heading-data"><b>Total Harga</b></td>
      <td valign="top">:</td>
      <td valign="top">Rp. <?php echo \General::number_format($data['total_amount'],3) ;?></td>
      <td>&nbsp;</td>
    </tr>
  </table>
</div>
    <form name="form" action="{{URL::to('services/cancel-ticket-by-ticket-id')}}" id="ticket_cancel" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}" />
  <div class="passenger-detail" >
    <?php if(isset($_REQUEST['suc'])) { ?>
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Ticket has been <strong>Cancelled</strong> Succesfully </div>
    <?php } ; ?>
    <input type="hidden" name="suc" id="suc" value="" />
    <?php /*?>	<tr><td colspan="4"></td></tr>	<?php */?>
    <table class="table table-striped">
      <tr >
        <td class="title-bookingdata" colspan='6' align='center'> Rincian Penumpang </td>
      </tr>
      <input type="hidden" name="ticket" id="ticket" value='' />
      <input type="hidden" name="hc" id="hc" value="0" />
      <tr class="heading-bookingdata">
        <td ><?php if($proc==1) {  ?>
          <input type="checkbox" class="chackbox-booking" name="selectall" id="selectall" onclick="chk(this)" title="Select / DeSelect All Seats" />
           
          <?php } ?></td>
        <td><strong>Nama Penumpang</strong></td>
        <td><strong>Nomor Tiket</strong></td>
        <td><strong>Nomor Kursi</strong></td>
        <td><strong>Jenis Kelamin</strong></td>
        <td><strong>Umur</strong></td>
      </tr>
      <?php 
      $num = count($data['booking_seats']);
      $s_no = '';
	foreach($data['booking_seats'] as $row){	 			
				if($row['ticket_id'] == $body['ticket_id']){			 
				?>
      <input type="hidden" name="passenger_id" id="passenger_id" value="<?php echo $row['id']; ?>" >
      <input type="hidden" name="ticket_id" id="uniqueid" value="<?php echo $row['ticket_id']; ?>">
      <input type="hidden" name="booking_id" id="ticket_id" value="<?php echo $row['booking_id']; ?>">
      <input type="hidden" name="count" id="count" value="<?php echo $num; ?>" >
      <tr>
        <td><?php if($proc==1) {  ?>
          <input type="checkbox" name="seatval[]" id="seatval[]" class="case chackbox-booking" value="<?php echo $row['seat_lbl']; ?>" onclick="fun(this.id);" title="<?php echo $row['seat_lbl']; ?>"/>
          <label class="loginrememberme"></label>
          <?php } ?></td>
        <td class="heading-data"><?php echo $row['passenger_name']; ?></td>
        <td><?php echo $row['ticket_id']; ?></td>
        <td><?php echo (!$row['seat_lbl'])?'Saat Keberangkatan':$row['seat_lbl']; ?></td>
        <td><?php echo $row['passenger_gender'] == 'm' ? 'L' : 'P'; ?></td>
        <td><?php echo $row['passenger_age']; ?></td>
      </tr>
      <?php
	$s_no.=$row['seat_lbl'].",";
                                }
		} 
		?>
      <input type="hidden" name="seatno" id="seatno" value="<?php echo $s_no; ?>" >
      <input type="hidden" name="email" id="email" value="<?php echo $data['booker_email'];?>" />
    </table>
  </div>
  <?php if($proc==1) {  ?>
  <div class="new_button action-bookingdata text-center">
    <input type="submit" name="submit" class="btn btn-primary btn-lg " id="submit" value=" Cancel" />
    <!--<input type="button" name="submit" id="submit" value=" Cancel " style="background:none; border:0px; color:#FFFFFF; font-weight:bold;" onclick="alert('This is Demo version !!!');" />-->
    <div class="divider40"></div>
  </div>
  <!--</div>-->
  <?php } ?>
</form>
</div>