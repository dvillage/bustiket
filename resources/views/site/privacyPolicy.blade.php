@extends('layouts.site_layout')
@section('content')
<main class="site-main">
        <section class="section section-hero-page" style="background-image: url('assets/images/heropages/1.jpg') ;">  
        </section>

        <div class="site-main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <article class="article-single clearfix">
                            <header class="article-header">
                                <h1>Kebijakan privasi</h1>
                            </header>

                            <p>BUSTIKET.COM dimiliki dan dioperasikan oleh PT. BUSTIKET Global Technology. Ketentuan Kerahasiaan atau Kebijakan Privasi (“Ketentuan Kerahasiaan”) berikut menguraikan bagaimana kami mengumpulkan, menggunakan, menyingkapkan serta memproses informasi yang dapat diidentifikasi secara pribadi yang kami kumpulkan tentang data pribadi terkait dengan layanan yang tersedia dari BUSTIKET melalui situs web yang beralamatkan di WWW.BUSTIKET.COM. Dengan mengunjungi situs, Anda menyetujui pengumpulan, penggunaan, penyingkapan dan pemrosesan data pribadi Anda sebagaimana diuraikan dalam Ketentuan Kerahasiaan ini.</p>

                            <p>Sewaktu-waktu BUSTIKET mungkin merevisi Ketentuan Kerahasiaan ini untuk mencerminkan perubahan pada hukum, praktik kami dalam pengumpulan dan penggunaan data pribadi, fitur situs kami, atau berbagai kemajuan teknologi. Bila kami membuat revisi yang mengubah cara kami mengumpulkan atau menggunakan data pribadi Anda, perubahan-perubahan tersebut akan dinyatakan pada Ketentuan Kerahasiaan ini dan pada bagian awal Kebijakan Privasi akan tertera tanggal efektif berlaku. Karena itu, Anda harus meninjau Ketentuan Kerahasiaan ini secara berkala, sehingga Anda mendapatkan informasi terbaru mengenai kebijakan dan praktik terkini kami. BUSTIKET juga akan dengan jelas menampilkan perubahan materi semacam itu sebelum mengimplementasikan perubahan.</p>
                            
                            <div class="clearfix space space-15 showin-995"> </div>
                            <div class="clearfix space space-30 hidden-995"></div>
                            <ul class="list-kebijakanprivasi">
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Informasi yang Kami Kumpulkan</h3>
                                    <ul>
                                        <li>Kami mengumpulkan data pribadi mengenai Anda yang Anda berikan kepada kami saat menggunakan situs. Data pribadi bisa termasuk nama, alamat, nomor telepon, nomor kartu kredit, nomor rekening atau alamat e-mail Anda. Kami juga mengumpulkan informasi yang tak dapat diidentifikasi secara pribadi, yang mungkin tertaut pada data pribadi Anda, termasuk nama log-in, alamat IP dan kata sandi Anda, serta preferensi pencarian terkait pada pencarian spesifik.</li>
                                    </ul>
                                </li>
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Melindungi Data Pribadi Anda</h3>
                                    <ul>
                                        <li>Untuk mencegah akses tanpa otorisasi, kami memberlakukan berbagai prosedur fisik, elektronik dan organisasional yang wajar untuk melindungi data pribadi dari penghancuran yang melanggar hukum atau secara tak sengaja, atau kehilangan, pengubahan secara tak sengaja, atau penyingkapan maupun akses tanpa otorisasi. Bagaimana kami menggunakan informasi yang kami kumpulkan kami menggunakan data pribadi dan informasi lainnya yang dikumpulkan melalui situs untuk mendaftarkan Anda ke situs, memberi Anda produk dan layanan yang diminta, membangun berbagai fitur sehingga tersedia layanan yang lebih mudah digunakan di situs, dan menghubungi Anda mengenai semua layanan ini. Hal ini termasuk permohonan pemesanan yang lebih cepat, layanan pelanggan yang lebih baik dan pemberitahuan tepat waktu mengenai layanan baru serta penawaran khusus. Dari waktu ke waktu, BUSTIKET mungkin menghubungi Anda untuk meminta konten review mengenai pengalaman Anda menggunakan situs dan memilih Agen Bus atau Agen Travel (sebagaimana berlaku), membantu kami meningkatkan situs, atau untuk menawarkan promosi spesial pada Anda sebagai pengguna situs, di mana Anda telah menyatakan persetujuan Anda untuk menerima komunikasi semacam ini.</li>
                                    </ul>
                                </li>
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Berbagi Data Pribadi Anda</h3>
                                    <ul>
                                        <li>BUSTIKET dapat membagikan data pribadi Anda dengan pihak ketiga yang kami pekerjakan untuk melakukan layanan atas nama kami, seperti layanan hosting web, analisis data, untuk memasarkan produk dan layanan kami, untuk menangani transaksi kartu kredit atau menyediakan layanan pelanggan untuk Anda. Semua pihak ketiga tersebut memiliki kewajiban untuk menjaga keamanan dan kerahasiaan data pribadi, dan untuk memproses data pribadi hanya sesuai dengan instruksi kami. Dalam beberapa kasus, BUSTIKET mungkin meminta Anda untuk melakukan transaksi secara langsung dengan pihak ketiga untuk mendapatkan layanan, atau saat Anda menggunakan situs untuk memesan tiket bus atau tiket travel, kami mungkin memberikan data pribadi Anda pada Agen Bus atau Agen Travel (sebagaimana berlaku). BUSTIKET dapat menyingkapkan data pribadi untuk melindungi diri Kami terhadap tuntutan, untuk menanggapi panggilan menjadi saksi, proses peradilan, permohonan yang sah dari petugas penegak hukum, atau sebagaimana yang diperlukan untuk mematuhi hukum yang berlaku, atau pada pengguna terkait dengan semua penjualan, penugasan, atau transfer lain dari semua atau sebagian bisnis atau perusahaan kami. Kami juga mungkin menyingkapkan data pribadi untuk menegakkan atau mengaplikasikan Syarat dan Ketentuan yang berlaku bagi produk dan layanan kami, atau untuk melindungi hak, properti atau keselamatan BUSTIKET, pengguna, atau lainnya.</li>
                                    </ul>
                                </li>
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Kebijakan Kami Mengenai Cookie</h3>
                                    <ul>
                                        <li>Untuk melayani Anda dengan lebih cepat dan dengan kualitas lebih baik, kami menggunakan teknologi "cookie" pada situs. Cookie adalah kode berukuran kecil, umumnya disimpan pada hard drive komputer pengguna, yang memampukan situs web untuk "mempersonalisasikan" dirinya bagi setiap pengguna dengan mengingat informasi mengenai kunjungan pengguna ke situs web. Cookie dapat menyimpan berbagai informasi, termasuk alamat IP, data navigasi, informasi server, waktu transfer data, preferensi pengguna, serta alamat e-mail dan sandi yang digunakan untuk mengakses situs. Bagian “Bantuan” pada toolbar sebagian besar browser Internet akan memberitahu Anda cara mencegah browser Anda dari menerima cookie baru atau cara mengonfigurasi browser Anda untuk menolak cookie sekaligus. Bila Anda tidak mengaktifkan cookie, Anda tidak akan dapat mengakses berbagai fungsi atau fitur penting pada situs ini dan Anda mungkin hanya dapat menggunakan situs secara terbatas. Cookie yang digunakan pada situs ini terkait dengan data pribadi pengguna. Bagaimana caranya Anda dapat mengakses atau mengubah informasi yang dapat diidentifikasi secara pribadi. Bila Anda ingin memperbarui data pribadi manapun yang telah Anda berikan sebelumnya, atau menggunakan hak manapun atas akses, pembetulan, penghapusan, atau menentang pemrosesan data pribadi Anda yang mungkin Anda miliki di bawah hukum yang berlaku, silakan hubungi Kami di: 0812-8000-3919. Untuk melindungi privasi dan keamanan Anda, kami akan melakukan verifikasi terhadap identitas Anda sebelum memberi akses atau melakukan perubahan pada data pribadi Anda. Semua permohonan untuk mengakses data pribadi Anda akan dijawab dalam waktu kurang dari 30 hari.</li>
                                    </ul>
                                </li>
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Pendaftaran Keanggotaan</h3>
                                    <ul>
                                        <li>Pengguna situs ini diharapkan membuat akun keanggotaan dengan mendaftar dan memberikan data tertentu. Dalam mendaftar dan memberikan data tertentu. Anda menjamin bahwa:
                                        <ol class="lower-latin">
                                            <li>Pengguna terdaftar berusia minimal 17 tahun;</li>
                                            <li>Informasi tentang diri Anda adalah benar dan akurat, terkini dan lengkap seperti yang dipersyaratkan di dalam formulir pendaftaran di situs (data registrasi) dan</li>
                                            <li>Anda akan memperbaharui data registrasi ini agar selalu benar, tepat dan lengkap.</li>
                                        </ol>
                                        Untuk selanjutnya BUSTIKET tidak bertanggung jawab atas segala kerugian atau bahaya yang mungkin diderita, di mana kerugian atau kerusakan tersebut yang disebabkan oleh informasi yang tidak akurat atau tidak lengkap yang diberikan oleh Anda setelah mendaftar. Anda akan menerima kata sandi dan identitas pengguna. Anda bertanggung jawab untuk menjaga kerahasiaan kata sandi dan identitas pengguna dan Anda bertanggung jawab penuh atas semua kegiatan yang berkaitan dengan sandi atau identitas pengguna. Anda hanya dapat menggunakan satu identitas pengguna (username) dan kata sandi (password) pada satu waktu dan tidak diperbolehkan menggunakan lebih dari satu username. Anda diminta untuk menggunakan password hanya untuk digunakan diri sendiri saja, dan tidak mengizinkan orang lain untuk menggunakan kata sandi Anda tersebut. Anda menyetujui untuk segera memberitahukan kepada BUSTIKET atas penggunaan tidak sah atas sandi atau akun atau semua bentuk pelanggaran keamanan dan memastikan bahwa Anda keluar dari akun setiap kali selesai menggunakan situs ini. BUSTIKET tidak akan bertanggung jawab untuk setiap kehilangan atau kerusakan yang timbul dari kegagalan Anda untuk mematuhi peraturan ini.
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <h3 class="list-kebijakanprivasi-title">Persetujuan mengenai Perubahan Atas Ketentuan Kerahasian</h3>

                                    <ul>
                                        <li>Dengan menggunakan situs atau jasa Kami, maka Anda menyatakan persetujuan dan penundukan diri terhadap pengumpulan dan penggunaan data pribadi sebagaimana tertera pada Ketentuan Kerahasiaan dan Syarat. Selanjutnya, apabila Anda menggunakan jasa kami, maka kami berhak mengumpulkan dan menggunakan informasi data pribadi Anda sesuai dengan syarat yang berlaku. BUSTIKET dapat mengubah Ketentuan Kerahasiaan ini sewaktu-waktu. Kami akan menampilkan perubahan yang ada pada halaman ini agar Anda mengetahui perubahan yang ada mengenai pengumpulan dan penggunaan data pribadi berikut juga keadaan yang memaksa kami untuk memberikan data informasi pada pihak ketiga.</li>
                                    </ul>
                                </li>
                            </ul>
                        </article><!-- / .article-single -->

                    </div>
                </div>
            </div>
        </div><!-- / .site-main-content -->


@stop