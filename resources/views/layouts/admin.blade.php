@include('admin.admin_header')
<?php 
//echo config('constant.LOGGER');
if(config('constant.LOGGER') == 'SS'){
   if(config('constant.SS_TYPE') != 'Main'){
     ?>
@include('admin.ss_ab_sidebar')
<?php
    }else{
    ?>
@include('admin.ss_sidebar')
<?php
    }
}else if(config('constant.LOGGER') == 'SP'){
    if(config('constant.SP_TYPE') != 'Main'){
     ?>
@include('admin.sp_ab_sidebar')
<?php
    }else{
    ?>
@include('admin.sp_sidebar')
<?php
    }
}else{
    ?>
@include('admin.admin_sidebar')
<?php
}
?>

@yield('content')
@include('admin.admin_footer')