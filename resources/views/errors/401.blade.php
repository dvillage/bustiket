
@include('site.header')
<title>Access Denied !</title>
</head>
<body>
<div class="errore">
<div class="container text-center">
	<div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="logo-401">
			<a href="#"><img src="{{URL::to('assets/img/logo404.png')}}" class="img-responsive" alt="" /></a>
		</div>
		<div class="content-404">
			<div class="four">
            <h1 style="color: #363432; font-size:41px; font-weight: 300; font-family: "Roboto",sans-serif;">Access Denied</h1>
            	<h1 style="font-size:100px;">4<span style=" color: #62bc8f; font-weight:300;">0</span>1</h1>
            </div>
			
			<h2><a href="{{URL::to('/')}}">Bring me back Home</a></h2>
		</div>
	</div>
</div>
</div>
</div>
@include('site.footer')

