<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="stylesheet" href="{{URL::to('assets/css/app.min.css')}}">
        <script type="text/javascript" src="{{URL::to('assets/js/app.min.js')}}"></script>
        <title>{{$title}}</title>
        <style>
            .vertical-center {
                min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
                min-height: 100vh; /* These two lines are counted as one :-)       */

                display: flex;
                align-items: center;
            }
        </style>
        <script>
            $(function(){
                try{
                    var param = '{"ticket_id":"<?php echo $ticket_id;?>"}';
                    busTiket.onPaymentSuccess(param);
                }
                catch(e)
                {
                    console.log(e);
                }
                
                setTimeout(function(){
//                    window.location.href = "<?php echo url('ticket-confirmed')."/".$ticket_id; ?>";
                },3000);
            });
        </script>

    </head>
    <body class="">
        <div class="jumbotron vertical-center"> 
            <div class="container">
                <p class="text-center"><img style="width: 100px"  src="{{url("assets/img/loader.gif")}}" /></p>
                <p class="text-center" style="color: #329832;font-size: 25px">Proses Pembayaran</p>
                <p class="text-center" style="font-size: 10px">Mohon Tunggu, pembayaran Anda sedang diproses</p>
            </div>
        </div>
        
        
        <iframe src="http://hadiahme.go2cloud.org/aff_l?offer_id=152&adv_sub=<?php echo $ticket_id;?>&adv_sub2=<?php echo $BT_id;?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
        
    </body>
</html>