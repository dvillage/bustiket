<div>
<div class="route-item-detail-seat clearfix">
    <?php
    if(isset($seats)){
    ?>
    <div class="item-pilih clearfix">
        <div class="item-pilih-seat clearfix">
            <?php 
             
                    for ($row = 0; $row < $rows; $row++) { ?>
                    <div class="item-pilih-seat-row clearfix" style="position: relative;">
                        <?php
                       
                        for ($col = 0; $col < $cols; $col++) {
                            $ex = 0;
                            $type = '';
                            $lbl = '';
                            foreach($seats as $k=>$val){
                                if (($val['row'] == $row && $val['col'] == $col)) {
                                    $ex = 1;
                                    $type = $val['object_type'];
                                    $lbl = $val['seat_lbl'];
                                    $idx = $val['seat_index'];
                                }
                            }
                            if($ex == 1){
                                $st= '';
                                if($type == 'T'){
                                    $lf = 24*$col;
                                    $st = 'position:absolute;left:'.$lf.'px;z-index:1000';
                                    echo '<div class="item-pilih-seat-columns">
                                            <span class="whitespace-res"></span>
                                        </div>';
                                }
                                $cl = '';
                                if(in_array($lbl, $blocked)){
                                    $cl = 'icon-rwd-seat-yellow';
                                }elseif(in_array($lbl, $booked)){
                                    $cl = 'icon-rwd-seat-gray';
                                }else{
                                    $cl = 'icon-rwd-seat-white select_seat_res';
                                }
                                ?>
                        <div class="item-pilih-seat-columns" style="{{$st}}">
                                <?php $class = $type =='DS' ? 'icon-rwd-sear' : ($type =='D' ? 'icon-seat-door_r' : ($type =='T' ? 'icon-rwd-toilet' : $cl)); ?>
                                <span class="sprite {{$class}}" data-bus_id="{{$bus_id}}" id="seat_{{$lbl}}" data-seat_label="{{$lbl}}" data-seat_index="{{$idx}}"></span>
                            </div>
                            <?php
                            }else{
                                ?>
                            <div class="item-pilih-seat-columns">
                                <span class="whitespace-res"></span>
                            </div>
                        <?php
                            }
                            
                        }
                        ?>
                    </div>
                <?php }
                if(count($already_blocked)>0){
                foreach($already_blocked as $al){
                    ?>
                    <script>
                        var lb = '{{$al}}';
                        $('#seat_'+lb).trigger('click');
                    </script>
                    <?php
                }
                }
                    ?>

        </div>

        <div class="item-pilih-seat-ket clearfix">
            <ul class="clearfix">
                <li>
                    <span class="sprite icon-rwd-seat-white"></span> Tersedia
                </li>
                <li>
                    <span class="sprite icon-rwd-seat-yellow"></span> Terbooking
                </li>
                <li>
                    <span class="sprite icon-rwd-seat-gray"></span> Terjual
                </li>

                <li>
                    <span class="sprite icon-rwd-seat-green"></span> Dipilih
                </li>
            </ul>
        </div>
    </div>      
    <?php }else{
        $left_seat = $remain_seat;
        if($left_seat > 4){
            $left_seat = 4;
        }
        if($nop < $left_seat){
            $left_seat = $nop;
        }
        ?>
            <div class="item-pilih clearfix">
                <h3 class="item-pilih-title">Jumlah Kursi</h3>
                <div class="item-pilih-seat clearfix">
<!--                        <div class="col-sm-6">
                            <span class="">Pilih Kursi :</span>
                        </div>-->
                        <!--<div class="col-sm-6">-->
                            <select name="" id="" class="select_seat_drop_res" data-bus_id="{{$bus_id}}">
                                <option value="" disabled selected>Pilih Kursi</option>
                                <?php for($i=1;$i<=$left_seat;$i++){ ?>
                                <option value="{{$i}}">{{$i}}</option>
                                <?php } ?>
<!--                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>-->
                            </select>
                        <!--</div>-->
                </div>
            </div>
    <script>
            var cnt = '{{$block_count}}';
            if(cnt > 0){
                $('.select_seat_drop_res').val(cnt);
                $('.select_seat_drop_res').trigger('change');
            }
            </script>
    <?php
    } ?>
    
</div><!-- / .route-item-detail-seat -->

<div class="route-item-detail-desc clearfix" id="route_detail_{{$bus_id}}" style="display: none;">
    <dib class="box-ket-wrapper">

        <div class="box-ket">
            <span>Kursi Anda:</span>
            <span class="total_seat_lable_{{$bus_id}}"></span>
            <span style="display: none" class="total_seat_{{$bus_id}}">0</span>
        </div>

        <div class="box-ket">
            <span>Total Harga</span>
            <span id="fare_total_f_{{$bus_id}}" class="">Rp 0</span>
        </div>
    </dib> 
    <br>
    <div class="item-seat-ket row clearfix">
    <div class="col-sm-6">
        <h3 class="item-seat-ket-title">Titik Berangkat :</h3>
        <select name="" id="board_{{$bus_id}}" data-bus_id='{{$bus_id}}' onchange="setBoard(this);">
            <option value="" disabled="" selected="">Pilih Tempat</option>
        </select>
    </div>

    <div class="col-sm-6">
        <h3 class="item-seat-ket-title">Titik Tiba :</h3>
        <select name="" id="drop_{{$bus_id}}" data-bus_id='{{$bus_id}}' onchange="setDrop(this);">
            <option value="" disabled="" selected="">Pilih Tempat</option>
        </select>
    </div>
</div><!-- / .item-seat-ket -->
<br>
    <form action="{{URL::to('ticket-checkout')}}" class="route-item-detail-desc-form clearfix row" method="post" id="form_{{$bus_id}}" style="display: block;" data-parsley-validate >
            <div class="form-field-header col-md-12">
                <h3 class="form-field-header-title">Detail Pemesanan</h3>
                <p>Data harus lengkap dan valid</p>
            </div>
            <div>
            <div class="form-field col-md-3 style-2 clearfix">
                <input type="text" placeholder="Nama Lengkap" id="booker_name" class="copy_to_pass" data-bus_id="{{$bus_id}}" name="booker_name" required="">
                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                <input type="hidden" name="bus_id" value="{{$bus_id}}" >
                <input type="hidden" name="session_id" id="sess_id_{{$bus_id}}" value="{{$bus_id}}" >
                <input type="hidden" name="sp_id" id="sp_id_s_{{$bus_id}}" value="" >
                <input type="hidden" name="sp_name" id="sp_name_s_{{$bus_id}}" value="" >
                <input type="hidden" name="bus_name" id="bus_name_s_{{$bus_id}}" value="" >
                <input type="hidden" name="per_head_amount" id="per_head_fare_{{$bus_id}}" value="" >
                <input type="hidden" name="total_amount" id="total_amount_{{$bus_id}}" value="" >
                <input type="hidden" name="total_seat" id="total_seat_{{$bus_id}}" value="" >
                <input type="hidden" name="board_point" id="board_point_{{$bus_id}}" value="" >
                <input type="hidden" name="drop_point" id="drop_point_{{$bus_id}}" value="" >
            </div>

            <div class="form-field col-md-3 style-2 clearfix">
                <input type="email" placeholder="Alamat Email" id="booker_email" class="copy_to_pass" data-bus_id="{{$bus_id}}" name="booker_email" required="">
            </div>

            <div class="form-field col-md-3 style-2 clearfix">
                <input type="text" placeholder="Tanggal Lahir eg. 25-01-1980" id="booker_dob" data-bus_id="{{$bus_id}}" data-parsley-pattern="[0-9]{2}\-[0-9]{2}\-[0-9]{4}" name="booker_dob" data-inputmask="'mask': 'd-m-y'" class="dob copy_to_pass" required="">
            </div>

            <div class="form-field col-md-3 style-2 clearfix">
                <input type="tel" placeholder="No.Handphone +628" id="booker_mo" class="copy_to_pass mobile_val" data-bus_id="{{$bus_id}}" name="booker_mo" data-parsley-minlength="10" data-parsley-pattern="\d+"  required="">
            </div>

            <div class="form-field-header col-md-12">
                <h3 class="form-field-header-title">Detail Penumpang</h3>
                <p>Data penumpang akan kami serahkan ke operator sebagai manifest penumpang. mohon diisi dengan data yang lengkap dan valid.</p>
            </div>

            <div class="form-field clearfix col-md-12">
                <fieldset>
                    <input type="checkbox" name="" id="pemesan-penumpang" class="default-check copy_{{$bus_id}}" checked="">
                    <label for="pemesan-penumpang">Pemesanan adalah penumpang</label>
                </fieldset>
            </div>
            <div id="add_pass_{{$bus_id}}_1" class="form-field clearfix col-md-12 bus_{{$bus_id}}">
                <div class="form-field clearfix style-2 col-md-4">
                    <input type="text" id="pass_name" name="pass_name[]" class="pass_name" placeholder="Detail penumpang 1" required="">
                    <input type="hidden" class="seat_label" name="seat_label[]" >
                    <input type="hidden" class="seat_index" name="seat_index[]" >
                </div>

                <div class="form-field clearfix style-2 col-md-2">
                    <select name="pass_gender[]" id="pass_gender" required="">
                        <option value="" disabled selected>Jenis Kelamin</option>
                        <option value="m">Pria</option>
                        <option value="f">Wanita</option>
                    </select>
                </div>

                <div class="form-field clearfix style-2 col-md-3">
                    <input type="text" name="pass_dob[]" id="pass_dob" data-inputmask="'mask': 'd-m-y'" data-parsley-pattern="[0-9]{2}\-[0-9]{2}\-[0-9]{4}" class="dob" placeholder="Tanggal Lahir eg. 25-01-1980" required="">
                </div>

                <div class="form-field clearfix style-2 col-md-3">
                    <input type="tel" name="pass_mo[]" id="pass_mo" placeholder="No.Handphone +628" data-parsley-pattern="\d+" data-parsley-minlength="10" class="mobile_val"  required="">
                </div>
            </div>
            </div>
            <div class="form-field form-submit clearfix col-md-3">
                <button class="btn btn-radius btn-green btn-minwidth" onclick="return validate('{{$bus_id}}');">Lanjutkan</button>
            </div>
        </form>
</div><!-- / .route-item-detail-desc -->
</div>