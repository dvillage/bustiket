<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" type="image/png" href="{{URL::to('assets/img/favicon.png')}}">

        <script type="text/javascript" charset="utf-8" src="{{URL::to('assets/js/jquery.min.js')}}"></script>
<!--        <script type="text/javascript" charset="utf-8" src="{{URL::to('assets/js/jquery.validate.min.js')}}"></script>

        <link rel="stylesheet" href="{{URL::to('assets/css/bootstrap/bootstrap.css')}}">-->

<!--        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.min.css')}}">
        <link rel="shortcut icon" href="{{URL::to('assets/img/favicon.png')}}" type="image/x-icon">
        <link rel="icon" href="{{URL::to('assets/img/favicon.png')}}" type="image/x-icon">
        <script type="text/javascript" src="{{URL::to('assets/js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>-->
        <title>{{$title}}</title>
        <style>
            body{
                padding: 10px;
            }
            .tc-lst{
                list-style: none;
            }
            .tc-lst.num
            {
                list-style: upper-alpha;
            }
            .lst-txt{
                text-align: justify;
            }
        </style>
    </head>
    <body class="lst-txt">
        <ol class="tc-lst num">

            <li>

                <h4><b>Pendahuluan</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Terima kasih atas kunjungan Anda ke website kami, WWW.BUSTIKET.COM. Kami berharap agar kunjungan Anda dapat bermanfaat dan memberi kenyamanan dalam mengakses dan menggunakan seluruh Layanan yang tersedia di website kami. Kami terus menerus berupaya memperbaiki dan meningkatan mutu pelayanan kami, dan sangat menghargai segala kritik, saran dan masukan dari Anda; silakan Anda menyampaikannya ke kami melalu cs@bustiket.com  atau telepon di 0812-8000-3919. </p>

                    </li>

                    <li>

                        <p>Website ini dimiliki, dioperasikan dan diselenggarakan oleh CV. Mitra Indo Visi Group (BUSTIKET.COM) website dan layanan kami tersedia secara online melalui website: WWW.BUSTIKET.COM atau berbagai akses, media, perangkat dan platform lainnya, baik yang sudah atau akan tersedia dikemudian hari. </p>



                    </li>

                </ol>

            </li> 

            <li>

                <h4><b>INFORMASI PENTING!</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>BUSTIKET bertanggung jawab <b>pada:</b></p>
                        <ul type="1">
                            <li>Mengeluarkan tiket valid yang diterima oleh operator.</li>
                            <li>Menyediakan bantuan untuk pembatalan dan refund sesuai dengan syarat dan ketentuan masing-masing kebijakan operator.</li>
                            <li>Menyediakan customer service dan bantuan dalam komunikasi dengan operator.</li>
                        </ul>
                        <p>BUSTIKET tidak bertanggung jawab <b>pada:</b></p>
                        <ul type="1">
                            <li>Keterlambatan/tidak berangkatnyaarmada akibat kerusakan mesin ataupun hal lainnya yang menyangkut operasional dari operator.</li>
                            <li>Crew operator yang tidak ramah.</li>
                            <li>Armada dan fasilitas armada yang tidak sesuai dengan harapan penumpang.</li>
                            <li>Penundaan keberangkatan maupun percepatan keberangkatan dikarenakan hal yang tidak dapat dihindari (misal: kemacetan, kecelakaan, kerusakan armada dll).</li>
                            <li>Kehilangan barang bawaan/ bagasi maupun rusaknya barang bawaan selama perjalanan</li>
                            <li>Pemindahan kursi tempat duduk pada menit akhir keberangkatan dikarenakan kebijakan dari operator</li>
                            <li>Perpindahan titik lokasi keberangkatan dikarenakan kendala kemacetan dan percepatan waktu perjalanan. Hubungi operator sebelum hari keberangkatan untuk mengetahui lokasi. Nomor telpon (dan alamat email apabila ada) operator tersedia di e-tiket.</li>
                            <li>Tertinggal karena Anda menunggu di tempat keberangkatan yang salah / tidak sesuai. Pastikan untuk menghubungi operator apabila Anda kesulitan menemukan lokasi keberangkatan.</li>
                            <li>Sehubungan dengan kendala operasional, operator bisa melakukan:                                <ul>
                                    <li>Pemindahan armada.</li>
                                    <li>Penggunaan angkutan feeder sebagai penjemput penumpang.</li>
                                    <li>Pemindahan armada juga mungkin dilakukan oleh operator antar kelas armada berbeda (Misal: Eksekutif dipindahkan ke Bisnis).</li>
                                </ul>
                            </li>
                            <li>Pengalihan jalur perjalanan oleh crew operator akibat satu dan lain hal yang mengakibatkan Anda tidak dapat turun pada tujuan akhir Anda.</li>
                        </ul>
                    </li>
                    <li>Pada beberapa operator, penumpang diwajibkan untuk mencetak e-tiket BUSTIKET dan membawa kartu identitas yang masih berlaku (KTP/ SIM/ PASPOR). Jika Anda tidak membawa dokumen tersebut, kegagalan pengangkutan mungkin dapat terjadi.</li>
                    <li>Tiket yang di beli di BUSTIKET dapat dibatalkan. Mohon untuk selalu membaca kebijakan pembatalan masing masing operator yang tersedia di E-Ticket. Tiket yang dibeli melalui credit card, tidak akan kembali dalam bentuk limit credit, melainkan di transfer menggunakan Account Bank BCA, Mandiri, ataupun BRI. <br><small>*Biaya transfer antar bank maupun biaya payment gateway karena penggunaan credit card, convenient store, dll tidak dapat di refund.</small></li>
                    <li>Dalam hal SMS dan email e-ticket tidak berhasil dikirimkan dikarenakan penulisan nomor maupun alamat email yang  keliru, kursi akan tetap ter-booked selama transaksi telah berhasil dilakukan. BUSTIKET dan juga operator tidak bertanggung jawab apabila ternyata terjadi perubahan jadwal, armada, kursi maupun tempat keberangkatan dan informasi ini tidak diterima oleh penumpang.</li>
                    <li>BUSTIKET tidak memberikan jaminan terhadap pemenuhan seluruh informasi yang tercantum di dalam website meliputi: rute, fasilitas, gambar armada, tempat keberangakatan, jam keberangkatan dan kedatangan, semua hal tersebut hanyalah sebatas informasi yang diberikan oleh operator kepada BUSTIKET sebagai partner penjualan tiket online.</li>
                    <li>Tambahan biaya tiket dikarenakan terjadi kenaikan harga BBM setelah tanggal pemesanan. Hal ini sepenuhnya merupakan wewenang  operator bersangkutan. Tambahan biaya mungkin akan diminta petugas operator pada saat keberangkatan.</li>



                    <h4 class="top20 title">Catatan Penting untuk Keberangkatan High Season/ Musim Liburan.</h4>
                    <ul type="1">
                        <li>Untuk hari tertentu yang berpotensi terhadap kenaikan harga tiket dikarenakan high season, liburan panjang, lebaran, dan hari khusus lainnya, biaya tambahan mungkin akan dikenakan kepada penumpang pada saat keberangkatan. Tambahan biaya tiket ini akan disampaikan oleh petugas operator diberlakukan kepada seluruh penumpang, baik yang membeli online maupun membeli langsung.</li>
                        <li>Jam keberangkatan yang tertera di e-ticket adalah jam estimasi <b>keberangkatan normal</b>. Jam keberangkatan dapat berubah dikarenakan high season, liburan sekolah, long weekend, dll. Jika lokasi keberangkatan Anda adalah rute lintasan (bukan pool/ origin bus), kendala kemacetan mungkin akan membuat keterlambatan keberangkatan. </li>
                        <li>Pada musim liburan, beberapa tempat keberangkatan mungkin akan ditutup oleh operator guna percepatan durasi perjalanan dan menghindari kemacetan. Hubungi costumer services 1 hari sebelum hari keberangkatan untuk kepastian lokasi tempat keberangkatan. Nomor telpon (dan alamat email apabila ada) costumer services tersedia di e-tiket.</li>
                        <li>Percepatan jam keberangkatan mungkin dilakukan oleh operator untuk menghindari kemacetan, selalu aktifkan nomor Handphone Anda agar operator bisa kontak Anda mengenai perubahan tersebut. Hubungi costumer services 1 hari sebelum hari keberangkatan untuk kepastian lokasi tempat keberangkatan. Nomor telpon (dan alamat email apabila ada) costumer services tersedia di e-tiket.</li>
                    </ul>


                </ol>

            </li> 

            <li>

                <h4><b>Umum</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Dengan mengakses dan menggunakan website dan layanan kami, Anda menyatakan telah membaca, memahami, menyetujui dan menyatakan tunduk pada Syarat dan Ketentuan Penggunaan BUSTIKET. Jika Anda tidak dapat menyetujui Syarat dan Ketentuan Penggunaan BUSTIKET, baik secara keseluruhan ataupun sebagian, maka Anda tidak diperbolehkan untuk mengakses website ini ataupun menggunakan layanan yang kami sediakan. </p>

                    </li>

                    <li>

                        <p>Syarat dan Ketentuan Penggunaan BUSTIKET ini terdiri atas (i) syarat dan ketentuan umum yang berlaku untuk setiap akses dan layanan yang tersedia pada website, dan (ii) syarat dan ketentuan khusus yang mengatur lebih lanjut ketentuan penggunaan produk atau layanan tertentu. Dalam hal ditemukan adanya perbedaan atau pertentangan antara syarat dan ketentuan umum dan syarat dan ketentuan khusus, maka yang berlaku adalah syarat dan ketentuan khusus. </p>

                    </li>

                    <li>

                        <p>Syarat dan Ketentuan Penggunaan BUSTIKET dapat kami ubah, modifikasi, tambah, hapus atau koreksi ("perubahan") setiap saat dan setiap perubahan itu berlaku sejak saat kami nyatakan berlaku atau pada waktu lain yang ditetapkan oleh kami. Anda kami anjurkan untuk mengunjungi website kami secara berkala agar dapat mengetahui adanya perubahan tersebut.</p>

                    </li>

                </ol>

            </li>

            <li>

                <h4><b>Penggunaan</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Website ini dan layanan yang tersedia didalamnya dapat digunakan oleh Anda hanya untuk penggunaan pribadi dan secara non-komersial dan setiap saat tunduk pada dan berlaku syarat dan ketentuan yang saat itu berlaku dalam Syarat dan Ketentuan Penggunaan BUSTIKET.</p>

                    </li>

                    <li>

                        <p>Website ini dan produk-produk, teknologi dan proses yang terdapat atau terkandung dalam website, dimiliki oleh Kami atau pihak ketiga yang memberi hak kepada Kami. Kecuali untuk penggunaan yang secara tegas diijinkan dan diperbolehkan dalam Syarat dan Ketentuan Penggunaan BUSTIKET, Anda tidak memiliki ataupun menerima dan BUSTIKET tidak memberikan hak lain apapun ke Anda atas website ini, berikut dengan segala data, informasi dan konten didalamnya. </p>

                    </li>

                    <li>

                        <p>Dengan menggunakan website ini atau Layanan yang tersedia didalamnya, maka Anda menyatakan setuju tidak akan men-download, menayangkan atau mentransmisi dengan cara apa pun, dan atau membuat konten apa pun tersedia untuk umum yang tidak konsisten dengan penggunaan yang diijinkan dalam Syarat dan Ketentuan Penggunaan BUSTIKET. </p>

                    </li>

                    <li>

                        <p>Dalam website ini mungkin terdapat link (tautan) ke website yang dikelola oleh pihak ketiga ("Situs Eksternal"). Situs Eksternal disediakan hanya untuk referensi dan kenyamanan saja. BUSTIKET tidak mengoperasikan, mengendalikan atau mendukung dalam bentuk apa pun Situs Eksternal yang bersangkutan ataupun konten/isinya. Anda bertanggung jawab penuh atas penggunaan Situs Eksternal tersebut dan dianjurkan untuk mempelajari syarat dan ketentuan dari Situs Eksternal itu secara seksama. </p>

                    </li>

                    <li>

                        <p>Layanan yang tersedia dalam website ini secara umum menggunakan sistem re-marketing dan sistem cookies yang memungkinkan pihak ketiga (termasuk dan tidak terbatas pada Google) mengakses dan menggunakan data kunjungan dalam sistem Cookies website ini untuk menampilkan dan menayangkan kembali tiap iklan BUSTIKET melalui internet. </p>

                    </li>

                    <li>

                        <p>Anda tidak boleh membuat link, melakukan screen capture atau data crawling ke website tanpa adanya persetujuan tertulis sebelumnya dari BUSTIKET. Hal-hal tersebut dianggap sebagai pelanggaran hak milik intelektual BUSTIKET.</p>

                    </li>

                </ol>

            </li>

            <li>

                <h4><b>Layanan BUSTIKET.COM</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>BUSTIKET menyediakan dan menyelenggarakan sistem dan fasilitas pemesanan online secara terpadu ("Layanan"), yang dapat melayani pemesanan : Transportasi Darat ("Produk") yang memungkinkan Anda untuk mencari informasi atas produk yang Anda inginkan, serta melakukan pemesanan dan pembelian dan sekaligus melakukan pembayaran secara online dan aman melalui berbagai sistem dan fasilitas pembayaran yang tersedia. </p>

                    </li>

                    <li>

                        <p>Layanan kami secara umum dapat tersedia secara online selama dua puluh empat jam sehari dan tujuh hari dalam seminggu; kecuali dalam hal adanya perbaikan, peningkatan atau pemeliharaan pada website kami. </p>

                    </li>

                    <li>

                        <p>Produk disediakan, disuplai dan diselenggarakan oleh pihak ketiga ("Mitra") yang telah mengadakan kerjasama dan telah mengadakan ikatan, baik secara langsung ataupun tidak langsung, dengan kami. Anda memahami dan mengakui bahwa:

                        </p><ul class="ul-disc">

                            <li>

                                <p> Pemesanan dan pembelian yang Anda lakukan melalui BUSTIKET merupakan hubungan hukum dan kontrak yang mengikat antara Anda dan mitra kami. Dalam hal ini, BUSTIKET bertindak sebagai agen atau perantara yang bertugas untuk memfasilitasi transaksi antara Anda dan mitra kami. </p>

                            </li>

                            <li>

                                <p> Data dan informasi terkait dengan produk tertentu yang kami cantumkan pada website merupakan data dan informasi yang Kami terima dari mitra, dan kami mempublikasikan data dan informasi tersebut dengan itikad baik sesuai dengan data dan informasi yang kami terima. </p>

                            </li>

                            <li>

                                <p> Kami tidak memiliki kendali atas data dan informasi yang diberikan oleh mitra kami, dan kami tidak menjamin bahwa data dan informasi yang disajikan adalah akurat, lengkap, atau benar, dan terbebas dari kesalahan. </p>

                            </li>

                        </ul>

                        <p></p>

                    </li>

                    <li>

                        <p>Anda tidak diperbolehkan untuk menjual kembali produk kami, menggunakan, menyalin, mengawasi, menampilkan, men-download, atau mereproduksi konten atau informasi, piranti lunak, atau layanan apa pun yang tersedia di website kami untuk kegiatan atau tujuan komersial apapun, tanpa persetujuan tertulis dari kami sebelumnya. </p>

                    </li>

                    <li>

                        <p>Anda dapat menggunakan website dan layanan yang tersedia untuk membuat pemesanan/pemesanan yang sah. Anda tidak diperbolehkan untuk membuat pemesanan untuk tujuan spekulasi, tidak benar atau melanggar hukum. Jika kami menemukan atau sewajarnya menduga bahwa pemesanan/pemesanan yang Anda buat ternyata tidak sah, maka kami mencadangkan hak untuk membatalkan pemesanan Anda.</p>

                    </li>

                    <li>

                        <p>Anda juga menjamin bahwa data dan informasi yang Anda berikan ke kami, baik sehubungan dengan pemesanan ataupun pendaftaran pada BUSTIKET, adalah data dan informasi yang akurat, terkini dan lengkap. Untuk ketentuan penggunaan data dan informasi yang Anda berikan, silakan untuk merujuk pada Kebijakan Penggunaan Data. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  5th line ------------------------>



            <li>

                <h4><b>Pemesanan/Pembelian Produk</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Pemesanan/pembelian produk dianggap berhasil atau selesai setelah Anda melakukan pelunasan pembayaran dan BUSTIKET menerbitkan dan mengirim ke Anda, Surat konfirmasi pemesanan/pembelian. Apabila terjadi perselisihan atau permasalahan, maka data yang terdapat pada BUSTIKET akan menjadi acuan utama dan diangap sah. </p>

                    </li>

                    <li>

                        <p>Dengan menyelesaikan pemesanan/pembelian, maka Anda dianggap setuju untuk menerima: (i) email yang akan kami kirim tidak lama sebelum tanggal pelayanan yang Anda pesan, memberikan Anda informasi tentang produk yang Anda beli, dan menyediakan Anda informasi dan penawaran tertentu (termasuk penawaran pihak ketiga yang telah Anda pilih sendiri) yang terkait dengan pemesanan dan tujuan Anda, dan (ii) email yang akan kami kirim tidak lama setelah tanggal pelayanan untuk mengundang Anda untuk melengkapi formulir ulasan pengguna produk kami. Selain dari konfirmasi email yang menyediakan konfirmasi pemesanan dan email-email yang telah Anda pilih sendiri, kami tidak akan mengirimi Anda pemberitahuan (yang diinginkan maupun yang tidak), email, korespondensi lebih lanjut, kecuali jika diminta secara khusus oleh Anda. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  6th line ------------------------>



            <li>

                <h4><b>Harga Produk</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Kami selalu berupaya untuk menyediakan harga terbaik atas produk untuk dapat dipesan oleh Anda. Harga yang tertera mungkin memiliki syarat dan ketentuan khusus, jadi Anda harus memeriksa sendiri dan memahami syarat dan ketentuan khusus yang berlaku terhadap suatu harga atau tarif tertentu sebelum Anda melakukan pemesanan. Anda juga perlu memeriksa dan memahami ketentuan mengenai pembatalan dan pengembalian dana yang secara khusus berlaku untuk produk dan/atau harga tertentu. </p>

                    </li>

                    <li>

                        <p>Harga yang tercantum sudah termasuk pajak, pungutan, biaya dan ongkos lainnya yang akan Kami uraian secara tegas pada website atau surat konfirmasi dari kami. </p>

                    </li>

                    <li>

                        <p>Untuk produk-produk tertentu kami juga memberikan jaminan harga terbaik bagi Anda. Harap Anda dapat mempelajari kebijaksanaan kami atas jaminan harga terbaik kami. Dalam kaitannya dengan kebijakan kami atas jaminan harga terbaik itu, jika Anda menemukan ada harga yang lebih rendah di layanan online lain di internet, harap Anda dapat memberitahu ke kami (cs@bustiket.com) dan kami akan berupaya untuk menyamakan harga/tarif Kami dengan harga/tarif lebih rendah yang Anda temukan. </p>

                    </li>

                    <li>

                        <p>BUSTIKET berhak untuk mengubah harga suatu produk setiap saat tanpa pemberitahuan sebelumnya, namun tetapi produk yang sudah dibeli oleh Anda dan yang untuk produk mana Anda sudah mendapat surat konfirmasi tidak akan berubah. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  7th line ------------------------>



            <li>

                <h4><b>Pembayaran</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Pelunasan atas harga pembelian merupakan syarat untuk melakukan pembelian. Kami menerima pembayaran dengan sistem pembayaran menggunakan kartu kredit (VISA, Master Card), transfer antar rekening serta antar bank ke rekening BUSTIKET di bank-bank yang tercantum di website.

                        </p><ol class="ol-alpha">

                            <li>

                                <p> Untuk melindungi dan mengenskripsi informasi kartu kredit Anda, kami menggunakan fasilitas teknologi "Secure Socket Layer (SSL)"; </p>

                            </li>

                            <li>

                                <p> Dalam hal terjadi kasus penipuan kartu kredit atau penyalah-gunaan sistem pembayaran oleh pihak-pihak ketiga manapun, maka kejadian tersebut harus segera dilaporkan ke kami dan perusahaan / bank penerbit kartu kredit Anda, untuk Anda memperoleh penanganan lebih lanjut sesuai dengan prosedur dan aturan yang berlaku. </p>

                            </li>

                        </ol>

                        <p></p>

                    </li>

                    <li>

                        <p>Atas setiap pemesanan yang dapat kami konfirmasi, kami akan mengirim Anda surat konfirmasi via email yang berisi uraian produk dan pemesanan yang Anda buat serta konfirmasi pembayaran. Anda bertanggung-jawab untuk mencetak dan menjaga informasi yang tertera pada surat konfirmasi yang kami kirim. Surat konfirmasi ini merupakan dokumen yang sangat penting dan Anda wajib membawa cetakan dari surat konfirmasi ini pada saat Anda akan menggunakan atau mengambil produk yang Anda beli. Kami atau mitra kami berhak untuk menolak memberikan Produk atau pelayanan, jika Anda tidak dapat membuktikan bahwa Anda telah secara sah melakukan pemesanan dan pelunasan, dan Anda membebaskan BUSTIKET dari segala tanggung-jawab dan kerugian Anda dalam bentuk apapun. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  8th line ------------------------>



            <li>

                <h4><b>Perubahan dan Pembatalan</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Kecuali secara tegas dinyatakan lain dalam Syarat dan Ketentuan Penggunaan BUSTIKET, semua pembelian Produk di BUSTIKET tidak dapat diubah, dibatalkan, dikembalikan uang, ditukar atau dialihkan ke orang/pihak lain </p>

                    </li>

                    <li>

                        <p>Dengan melakukan pemesanan atau pembelian produk di BUSTIKET, Anda dianggap telah memahami, menerima dan menyetujui kebijakan dan ketentuan pembatalan, serta segala syarat dan ketentuan tambahan yang diberlakukan oleh mitra. Kami akan mencantumkan kebijakan dan ketentuan pembatalan tersebut di setiap surat konfirmasi yang kami kirim ke Anda. Harap dicatat bahwa tarif atau penawaran tertentu tidak memenuhi syarat untuk pembatalan atau pengubahan. Anda bertanggung-jawab untuk memeriksa dan memahami sendiri kebijakan dan ketentuan pembatalan tersebut sebelumnya. </p>

                    </li>

                    <li>

                        <p>Jika Anda ingin melihat ulang, melakukan perubahan, atau membatalkan pesanan Anda, harap merujuk pada surat konfirmasi dan ikuti instruksi di dalamnya. Harap dicatat bahwa Anda mungkin saja dikenakan biaya tambahan atas pembatalan sesuai dengan kebijakan dan ketentuan pembatalan. </p>

                    </li>

                    <li>

                        <p>Walaupun sangat kecil kemungkinan kami membatalkan atau mengubah pemesanan yang sudah kami konfirm dalam surat konfirmasi, namun jika diperlukan kami akan memberitahu Anda secepat mungkin.

                        </p><ol class="ol-alpha">

                            <li>
                                <p> Kami akan bertanggung-jawab terhadap perubahan yang secara signifikan berpengaruh pada ketentuan produk dan layanan yang telah kami konfirmasikan sebelumnya, yaitu: </p>

                            </li>

                        </ol>

                        <p></p>

                    </li>

                    <li>

                        <p>Pembatalan pemesanan; </p>

                    </li>

                    <li>

                        <p>Perubahan tanggal, atau produk;

                        </p><ol class="ol-alpha">

                            <li>

                                <p> Dalam hal kami melakukan perubahan signifikan, maka Anda memiliki pilihan untuk:

                                </p><ol class="upper-roman">

                                    <li>

                                        <p>Menerima perubahan yang Kami tawarkan untuk tanggal atau produk lain; atau</p>

                                    </li>

                                    <li>

                                        <p>Menerima pengembalian pembayaran dalam bentuk kredit yang dapat Anda gunakan untuk pembelian produk dikemudian hari dari BUSTIKET.

                                            dan untuk itu Anda harus memberitahu kami dalam waktu 7 (tujuh) hari kalender mengenai pilihan Anda, dan jika kami tidak mendapat konfirmasi dari Anda, maka kami menganggap Anda memilih pilihan a). </p>

                                    </li>

                                </ol>

                                <p></p>

                            </li>

                        </ol>

                        <p></p>

                    </li>

                    <li>

                        <p>Kami tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal kami tidak dapat menyerahkan produk atau memberi layanan kepada Anda, akibat dari hal-hal yang terjadi akibat keadaan memaksa atau yang diluar kekuasaan kami atau mitra kami untuk mengendalikan, seperti, tapi tidak terbatas pada: perang, kerusuhan, teroris, perselisihan industrial, tindakan pemerintah, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  9th line ------------------------>



            <li>

                <h4><b>Keamanan</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Pada saat Anda membuat pemesanan atau mengakses informasi akun Anda, Anda akan menggunakan akses Secure Server Layer (SSL) akan mengenkripsi informasi yang Anda kirimkan melalui website ini. </p>

                    </li>

                    <li>

                        <p>Walaupun BUSTIKET akan menggunakan upaya terbaik untuk memastikan keamanannya, BUSTIKET tidak bisa menjamin seberapa kuat atau efektifnya enkripsi ini dan BUSTIKET tidak dan tidak akan bertanggung jawab atas masalah yang terjadi akibat pengaksesan tanpa ijin dari informasi yang Anda sediakan. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  10th line ------------------------>



            <li>

                <h4><b>Kebijakan Penggunaan Data</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Kami menganggap privasi Anda sebagai hal yang penting. </p>

                    </li>

                    <li>

                        <p>Pada saat Anda membuat pemesanan di BUSTIKET, kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Pada prinsipnya, data Anda akan kami gunakan untuk menyediakan produk dan memberi layanan kepada Anda. Kami akan menyimpan setiap data yang Anda berikan, dari waktu ke waktu, atau yang kami kumpulkan dari penggunaan produk dan layanan kami. Data pribadi Anda yang ada pada kami, dapat kami gunakan untuk keperluan akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, serta keperluan security, administrasi dan legal, reward points atau bentuk sejenisnya, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi, dan membantu kami dikemudian hari dalam memberi pelayanan kepada Anda. Sehubungan dengan itu, kami dapat mengungkapkan data Anda kepada group perusahaan di mana BUSTIKET tergabung didalamnya, mitra penyedia produk, perusahaan lain yang merupakan rekanan dari BUSTIKET, perusahaan memproses data yang terikat kontrak dengan kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang, di jurisdiksi manapun. </p>

                    </li>

                </ol>

            </li>



            <!-----------------  11th line ------------------------>



            <li>

                <h4><b>Penolakan Tanggung-Jawab</b></h4>

                <ol class="upper-roman">

                    <li>

                        <p>Seluruh data, informasi, atau konten dalam bentuk apapun yang tersedia pada website ini disediakan seperti apa adanya dan tanpa ada jaminan. </p>

                    </li>

                    <li>

                        <p>Anda mengakui, setuju dan sepakat bahwa Anda menanggung sendiri segala bentuk resiko atas penggunaan website dan layanan. Lebih lanjut, Anda mengakui, setuju dan sepakat bahwa BUSTIKET, termasuk setiap direktur, pejabat, pegawai, mitra dan pihak lain manapun yang bekerjasama dengan BUSTIKET, tidak bertanggung-jawab atas dan tidak memberi jaminan terhadap:

                        </p><ol class="ol-alpha">

                            <li>

                                <p> Segala hal yang berkaitan dengan website ini termasuk tapi tidak terbatas pada pengoperasian atau keakurasian data, kelayakan, kelengkapan data.a.    Segala hal yang berkaitan dengan website ini termasuk tapi tidak terbatas pada pengoperasian atau keakurasian data, kelayakan, kelengkapan data. </p>

                            </li>

                            <li>

                                <p> baik yang tersirat maupun tersurat, termasuk jaminan yang tersirat dari pembelian atau kepatutan dari tujuan tertentu atau kelayakan untuk diperdagangkan; </p>

                            </li>

                            <li>

                                <p> Kehilangan atau kerusakan, baik langsung, tidak langsung, khusus, yang bersifat konsekuensial atau termasuk kehilangan keuntungan, reputasi, kerusakan/kehilangan data, kerusakan koneksi yang tak dapat diperbaiki akibat penggunaan atau ketidakmampuan menggunakan website ini baik yang berdasarkan hukum atau hal lain bahkan ketika kita diinformasikan mengenai kemungkinan-kemungkinan tersebut; </p>

                            </li>

                            <li>

                                <p> Akses Anda terhadap website, berikut dengan kerusakan yang mungkin timbul akibat akses Anda ke website atau situs eksternal. Akses tersebut merupakan tanggung-jawab Anda sendiri dan Anda sendiri yang harus memastikan bahwa Anda terbebas dan terlindungi dari virus atau hal lain yang mungkin mengganggu atau merusak pengoperasian sistem komputer Anda. </p>

                            </li>

                        </ol>

                        <p></p>

                    </li>

                    <li>

                        <p>Sejauh yang dimungkinkan secara hukum, tanggung-jawab dan kerugian yang dapat ditanggung oleh BUSTIKET, baik untuk satu kejadian ataupun serangkaian kejadian yang saling terhubung, yang timbul dari kerugian yang secara langsung diderita oleh Anda, sebagai akibat dari kesalahan BUSTIKET, adalah terbatas sampai dengan jumlah total biaya yang telah Anda bayar lunas sebagaimana tercantum dalam Surat Konfirmasi. </p>

                    </li>

                </ol>

            </li>

        </ol>
    </body>
</html>