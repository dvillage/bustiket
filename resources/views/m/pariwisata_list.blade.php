                                <div id="searchresult">   
                                    <?php
                                    $splist = [];
                                    foreach($data as $ky=>$par){
                                        $splist[] = $par['sp']['first_name'].' '.$par['sp']['last_name'];
                                        $city=$par['city']['name'];
                                        $ac = $par['ac'] ? 'AC' : 'Non AC';
                                    ?>
                                    <div class="pariwisata-item clearfix active">
                                        <div class="pariwisata-item-content clearfix">
                                            <div class="pariwisata-item-content-desc clearfix">
                                                <figure class="pariwisata-image">
                                                    <div class="busbg common"><span class="sprite icon-check-normal-white"></span></div>
                                                    <?php
                                                    $filepath = config('constant.UPLOAD_PARIWISATA_DIR_PATH');
                                                    $url = URL::to('assets/uploads/pariwisata/').'/'.$par['image'];
                                                    if(file_exists($filepath.$par['image'])){
                                                        echo  '<img src="'.$url.'" alt="" >';
                                                    }
                                                    ?>
                                                    <p class="kisaran-harga">
                                                        <button class="btn-radius btn btn-orange select_bus"  id="btn_{{$par['id']}}" data-id="{{$par['id']}}" data-type="{{$par['type']}}" data-ac="{{$par['ac']}}" data-sp="{{$par['sp']['first_name'].' '.$par['sp']['last_name']}}">
                                                            <span class="sprite icon-check-normal-yellow"></span> Pilih Bus
                                                        </button><input type="hidden" id="price_list" value="{{json_encode($par['prices'])}}"/>
                                                        <button class="btn-radius btn btn-dark list_harga" data-toggle="modal" data-target="#id-modal">
                                                            <span class="sprite icon-list-harga " ></span> List Harga
                                                        </button>
                                                    </p>
                                                </figure>

                                                <div class="pariwisata-ket">
                                                    <h3 class="pariwisata-ket-title sp_name">{{$par['sp']['first_name'].' '.$par['sp']['last_name']}}</h3>
                                                    <span class="pariwisata-ket-subtitle">Kisaran harga sewa harian</span>
                                                    <span class="pariwisata-ket-price">Rp. {{\General::number_format($par['price'],3)}}</span>

                                                    <ul class="pariwisata-ket-list">
                                                        <li>
                                                            <span class="icon-wrapper">
                                                                <span class="sprite icon-pariwisata-bbm"></span>
                                                            </span>Petrol
                                                        </li>
                                                        <li>
                                                            <span class="icon-wrapper">
                                                                <span class="sprite icon-pariwisata-tolparkir"></span>
                                                            </span>Termasuk Tol & Parkir
                                                        </li>
                                                        <li>
                                                            <span class="icon-wrapper">
                                                                <span class="sprite icon-pariwisata-antarjemput"></span>
                                                            </span>Drop / Antar Jemput
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div><!-- / .pariwisata-item-content -->

                                        <div class="pariwisata-item-fasilitas">
                                            <a class="pariwisata_facelities" style="cursor: pointer">Fasilitas</a>
                                        </div>

                                        <div class="pariwisata-item-fasilitas-detail clearfix"  data-show='0' style="display: none;">
                                            <div class="pariwisata-fasilitas-images">
                                                <?php
                                                $other = $par['other_images'];
                                                if($other != ''){
                                                $other = explode(',',$other);
                                                foreach($other as $o){
                                                    $file = URL::to('assets/uploads/pariwisata').'/'.$o;
                                                    $file_check = config('constant.UPLOAD_PARIWISATA_DIR_PATH').$o;
                                                    if(file_exists($file_check) && $o!= ''){
                                                        ?>
                                                <div class="pariwisata-fasilitas-image">
                                                    <span class="sprite icon-check-normal-white common"></span>
                                                    <div class="other_img_bg common"></div>
                                                    <img src="{{$file}}" alt="">
                                                </div>
                                                <?php
                                                    }
                                                }

                                                }
                                                $tseat = $par['type'] == "Big Bus" ? 48 : 32;
                                                ?>

                                            </div>

                                            <div class="pariwisata-fasilitas-desc clearfix">
                                                <div class="col-md-6">
                                                    <h3 class="pariwisata-fasilitas-desc-title">Fasilitas</h3>

                                                    <ul class="list-check-pariwisata clearfix">
                                                         <?php 
                                                        foreach($par['amenities'] as $am){
                                                            ?>
                                                        <li>
                                                            <span class="sprite icon-check-normal-yellow"></span>{{ $am['amenities']['name'] }}
                                                        </li>
                                                        <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>

                                                <div class="col-md-6">
                                                    <h3 class="pariwisata-fasilitas-desc-title">Armada tersedia</h3>
                                                    <?php echo html_entity_decode($par['fleet_detail']);?>
<!--                                                    <ul class="list-armada">
                                                        <li>Big Bus AC 2 - 2 (48 Seats)</li>
                                                        <li>Big Bus AC 2 - 3 (50 Seats)</li>
                                                        <li>Big Bus AC 2 - 2 (48 Seats)</li>
                                                        <li>Big Bus AC 2 - 2 (48 Seats)</li>
                                                    </ul>-->
                                                </div>
                                            </div>
                                        </div>
                                        <input type='hidden' id='type_{{$ky}}' class='bustype' value='{{$par['type']}}' />
                                        <input type='hidden' id='fitur_{{$ky}}' class='fitur' value='{{$ac}}' />
                                        <input type='hidden' id='fare_{{$ky}}' class='fare' value='{{$par['price']}}' />
                                        <input type='hidden' id='tseat_{{$ky}}' class='tseat' value='{{$tseat}}' />
                                    </div><!-- / .pariwisata-item -->
                                    
                                    <?php } ?>
                                </div>
                                <div class="clearfix space space-30"></div>
                                <form method="post" action="{{URL::to('checkout-pariwisata')}}">
                                <div class="col-md-3">
                                    <input type="hidden" name="total_bus" id="total_bus" value="0" />
                                    <input type="hidden" name="bus_detail" id="bus_detail" value="" />
                                    <input type="hidden" name="_token" id="" value="{{csrf_token()}}" />
                                    <div class="booking-box">
                                        <header class="booking-box-header">
                                            <h2>Bus Pilihan Saya</h2>
                                        </header>

                                        <div class="booking-box-content">
                                            <div class="booking-box-item clearfix" id="book_1"></div><!-- / .booked -->

                                            <div class="booking-box-item clearfix" id="book_2"></div><!-- / .booked -->

                                            <div class="booking-box-item clearfix" id="book_3"></div>
                                            <div class="booking-box-item clearfix" id="book_4"></div>
                                            <div class="booking-box-item clearfix" id="book_5"></div>

                                            <div class="booking-box-item-cta">
                                                <button class="btn btn-yellow btn-fullwidth" id="book_bus">Pesan</button>
                                            </div>
                                        </div>
                                    </div><!-- / .booking-box -->
                                    
                                </div>
</form>
<!-- MODAL BEGIN -->
<div class="modal modal-flat fade" id="id-modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
        <h3 class="modal-body-title">Daftar Harga Bus Pariwisata</h3>

        <div class="table-responsive table-harga">
            <table class="table">
                <thead>
                    <tr>
                        <th>Tujuan</th>
                        <th>Kelas Bus & Jumlah Kursi</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody id="append_price">
                    <tr>
                        <td>Medan - Banda Aceh</td>
                        <td>Big Bus 2 - 2 39 <br>39 seats</td>
                        <td>Rp 6.500.000</td>
                    </tr>
                    <tr>
                        <td>Medan - Banda Aceh</td>
                        <td>Big Bus 2 - 2 39 <br>39 seats</td>
                        <td>Rp 7.500.000</td>
                    </tr>
                    <tr>
                        <td>Medan - Banda Aceh</td>
                        <td>Big Bus 2 - 2 39 <br>39 seats</td>
                        <td>Rp 8.500.000</td>
                    </tr>
                </tbody>
            </table>
        </div>
      </div>
            
    </div><!-- / .modal-content -->
  </div>
</div>
<!-- MODAL END -->                                
     <?php
     $splist = array_unique($splist);
     ?>                           
                                
<script>
    <?php 
    if(!isset($count) || $count == 0)
    {
        ?>$('#ptitle1').html(cnt+' bus found at '+city);    <?php
    }
    else
    { ?>
            var city = '{{$city}}';
            var cnt = '{{$count}}';
        $('#ptitle1').html(cnt+' bus found at '+city);    
    <?php } ?>
        
        var spname = <?php echo json_encode($splist); ?>;
    
    var sp_filter = [];
    var type_filter = ['Mini Bus','Big Bus'];
    var fitur_filter = ['AC','Non AC'];
    
    
    for(var x in spname){
        $('#splist1').append('<li><a class="large" data-value="'+spname[x]+'" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;'+spname[x]+'</a></li>');
        sp_filter.push(spname[x]);
    }
    
    $( '#splist1 a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = sp_filter.indexOf( val ) ) > -1 ) {
                    sp_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    sp_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( sp_filter );
            getFilter();
            return false;
    });
</script>