<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="keywords" content="">
        
        <link rel="stylesheet" href="{{URL::to('assets/css/app.min.css')}}">
        <script type="text/javascript" src="{{URL::to('assets/js/app.min.js')}}"></script>
        <title>{{$title}}</title>
        <script>
            $(function(){
                try{
                    var param = '{}';
                    busTiket.onPaymentFail(param);
                }
                catch(e)
                {
                    console.log(e);
                }
                
                setTimeout(function(){
                    window.location.href = "<?php echo url('ticket-failed'); ?>";
                },3000);
            });
        </script>
    </head>
    <body class="">
        
        <body class="">
        <div class="jumbotron vertical-center"> 
            <div class="container">
                <p class="text-center"><img style="width: 100px"  src="{{url("assets/img/loader.gif")}}" /></p>
                <p class="text-center" style="color: #329832;font-size: 25px">{{$msg}}</p>
                <p class="text-center" style="font-size: 10px">Mohon Tunggu, pembayaran Anda sedang diproses</p>
            </div>
        </div>
        
    </body>
        
        <h3 class="text-danger text-center"></h3>
    </body>
</html>