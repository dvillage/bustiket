<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" type="image/png" href="{{URL::to('assets/img/favicon.png')}}">

        <script type="text/javascript" charset="utf-8" src="{{URL::to('assets/js/jquery.min.js')}}"></script>
        <script type="text/javascript" charset="utf-8" src="{{URL::to('assets/js/jquery.validate.min.js')}}"></script>

        <link rel="stylesheet" href="{{URL::to('assets/css/bootstrap/bootstrap.css')}}">

        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.min.css')}}">
        <link rel="shortcut icon" href="{{URL::to('assets/img/favicon.png')}}" type="image/x-icon">
        <link rel="icon" href="{{URL::to('assets/img/favicon.png')}}" type="image/x-icon">
        <script type="text/javascript" src="{{URL::to('assets/js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
        <title>{{$title}}</title>
    </head>
    <body class="">
        <h3 class="text-danger text-center">{{$msg}}</h3>
    </body>
</html>