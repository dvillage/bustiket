<input type="hidden" id="total_person" value="{{$nop}}" />
<?php 
    $splist = [];
foreach ($buses as $key => $bus) { 
//    dd($bus);
    
    if($key !== 'roda' && $key !== 'lorena'){
    ?>
        <div class="rwd-booking-kursi clearfix"  data-bus_id="{{$bus['bus']['id']}}">
            <div class="route-content-rwd-sorted result " >
                <div class="route-rwd-sorted-items clearfix">
                    <div class="item-route-sorted">
                        <h3 class="sp_name">{{$bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']}}</h3>
                        <?php $splist[] = $bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']; ?>
                        <span class="subtitle">{{$bus['bus']['name']}}</span>
                        <p>{{$bus['bus']['total_seats']}} Kursi</p>
                        <span class="avail_seat hidden">{{$bus['bus']['total_seats']}}</span>
                    </div>

                    <div class="item-route-sorted">
                        <h3 id="pricing_{{$bus['bus']['id']}}" class="price">Rp <?php echo \General::number_format($bus['bus']['price'],3)?></h3>
                        <span class="subtitle"><?php echo explode(" ", $bus['routes'][0]['boarding_time'])[1]; ?> - <?php echo explode(" ", $bus['routes'][0]['droping_time'])[1]; ?></span>
                        <span class="board_time hidden"><?php echo explode(" ", $bus['routes'][0]['boarding_time'])[1]; ?></span>
                        <span class="drop_time hidden"><?php echo explode(" ", $bus['routes'][0]['droping_time'])[1]; ?></span>
                        <p>Durasi {{$bus['routes'][0]['duration']}}</p>
                    </div>
                </div>                                
            </div>

            <div class="route-content-rwd-sorted output route_amenity" id="route_amenity_{{$bus['bus']['id']}}" style="display: none;">
                <div class="item-route-output clearfix">
                    <h3 class="item-route-output-title"><span>rute</span></h3>

                    <p>{{$bus['routes'][0]['route']}}</p>
                </div><!-- / .item-route-output -->

                <div class="item-route-output clearfix">
                    <h3 class="item-route-output-title"><span>Fasilitas</span></h3>

                    <p><?php
                    $am = '';
                    
                    if(isset($bus['bus']['amenities'])){
                        foreach($bus['bus']['amenities'] as $a){
                            if($am != ''){
                                $am = $am.',';
                            }
                            $am = $am.$a['aname'];
                        }
                        echo $am;
                    }else{
                        echo 'No Amenity';
                    }
                    ?></p>
                </div><!-- / .item-route-output -->
            </div>
            
            <div class="route-item-detail clearfix">
                    
            </div><!-- / .route-item-detail -->

            <div class="rwd-booking-kursi-cta">
                <input type="hidden" id="sp_id_{{$bus['bus']['id']}}" value="{{$bus['bus']['sp']['id']}}" />
                <input type="hidden" id="sp_name_{{$bus['bus']['id']}}" value="{{$bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']}}" />
                <input type="hidden" id="bus_name_{{$bus['bus']['id']}}" value="{{$bus['bus']['name']}}" />
                <input type="hidden" id="board_points_{{$bus['bus']['id']}}" value="{{json_encode($board_points[$bus['bus']['id']])}}" />
                <input type="hidden" id="drop_points_{{$bus['bus']['id']}}" value="{{json_encode($drop_points[$bus['bus']['id']])}}" />
                <input type="hidden" id="fare_{{$bus['bus']['id']}}" value="{{$bus['bus']['price']}}" />
                <input type="hidden" id="fare_format_{{$bus['bus']['id']}}" class="fare" value="{{\General::number_format($bus['bus']['price'],3)}}" />
                <input type='hidden' id='type_{{$bus['bus']['id']}}' class='bustype' value='Bus' />
                <span class="btn btn-orange btn-fullwidth view_seat_res" id="bus_{{$bus['bus']['id']}}"  data-bus_id="{{$bus['bus']['id']}}"  data-map="0">Pesan Kursi</span>
            </div>
            
        </div><!-- / .rwd-booking-kursi -->

    
<?php 
    }
    else{
//        dd($bus['bus']);
//    echo $key;
        $bus['sp']['avatar'] = $bus['sp']['avatar'] == "" ? url("assets/images/sp/".$bus['sp']['avatar']."default.png") : url("assets/images/sp/".$bus['sp']['avatar']);
        foreach($bus['bus'] as $ky=>$bu){ 
            $keys = array_keys($bu['terminals']);
            ?>
            <div class="rwd-booking-kursi clearfix"  data-bus_id="{{$ky}}">
                <div class="route-content-rwd-sorted result " >
                    <div class="route-rwd-sorted-items clearfix">
                        <div class="item-route-sorted">
                            <h3 class="sp_name">{{$bus['sp']['first_name'].' '.$bus['sp']['last_name']}}</h3>
                            <?php $splist[] = $bus['sp']['first_name'].' '.$bus['sp']['last_name']; ?>
                            <span class="subtitle">{{$bu['bus']}}</span>
                            <p>{{$bu['total_seat']}} Kursi</p>
                            <span class="avail_seat hidden">{{$bu['total_seat']}}</span>
                        </div>

                        <div class="item-route-sorted">
                            <h3 id="pricing_{{$ky}}" class="price">Rp <?php echo \General::number_format($bu['fare'],3)?></h3>
                            <span class="subtitle"><?php echo current($bu['terminals'])[0]['departure_time'];  ?> - <?php echo current($bu['terminals'])[0]['arrival_time']; ?></span>
                            <span class="board_time hidden"><?php echo current($bu['terminals'])[0]['departure_time']; ?></span>
                            <span class="drop_time hidden"><?php echo current($bu['terminals'])[0]['arrival_time']; ?></span>
                            <p>Durasi {{$bu['terminals'][$keys[0]][0]['duration']}}</p>
                        </div>
                    </div>                                
                </div>

                <div class="route-content-rwd-sorted output route_amenity" id="route_amenity_{{$ky}}" style="display: none;">
                    <div class="item-route-output clearfix">
                        <h3 class="item-route-output-title"><span>rute</span></h3>

                        <p>{{$bu['route']}}</p>
                    </div><!-- / .item-route-output -->

                    <div class="item-route-output clearfix">
                        <h3 class="item-route-output-title"><span>Fasilitas</span></h3>

                        <p><?php
//                        $am = '';
//
//                        if(isset($bus['bus']['amenities'])){
//                            foreach($bus['bus']['amenities'] as $a){
//                                if($am != ''){
//                                    $am = $am.',';
//                                }
//                                $am = $am.$a['aname'];
//                            }
//                            echo $am;
//                        }else{
//                            echo 'No Amenity';
//                        }
                        ?></p>
                    </div><!-- / .item-route-output -->
                </div>

                <div class="route-item-detail clearfix">

                </div><!-- / .route-item-detail -->

                <div class="rwd-booking-kursi-cta">
                    <input type="hidden" id="sp_id_{{$ky}}" value="{{$bus['sp']['id']}}" />
                    <input type="hidden" id="sp_name_{{$ky}}" value="{{$bus['sp']['first_name'].' '.$bus['sp']['last_name']}}" />
                    <input type="hidden" id="bus_name_{{$ky}}" value="{{$bu['bus']}}" />
                    <input type="hidden" id="board_points_{{$ky}}" value="{{json_encode($board_points[$ky])}}" />
                    <input type="hidden" id="drop_points_{{$ky}}" value="{{json_encode($drop_points[$ky])}}" />
                    <input type="hidden" id="fare_{{$ky}}" value="{{$bu['fare']}}" />
                    <input type="hidden" id="fare_format_{{$ky}}" class="fare" value="{{\General::number_format($bu['fare'],3)}}" />
                    <input type='hidden' id='type_{{$ky}}' class='bustype' value='Bus' />
                    <span class="btn btn-orange btn-fullwidth view_seat_res" id="bus_{{$ky}}"  data-bus_id="{{$ky}}"  data-map="0">Pesan Kursi</span>
                </div>

            </div><!-- / .rwd-booking-kursi -->
        
<?php    }
    }
}
$splist = array_unique($splist);
?>
<script>
    var spname = <?php echo json_encode($splist); ?>;
    
    var sp_filter = [];
    var type_filter = ['Bus','Travel','Shuttle'];
    var board_filter = ['Pagi','Siang','Sore','Malam'];
    var drop_filter = ['Pagi','Siang','Sore','Malam'];
    
    for(var x in spname){
        $('#splist').append('<li><a class="large" data-value="'+spname[x]+'" tabIndex="-1"><input type="checkbox" checked/>&nbsp;&nbsp;'+spname[x]+'</a></li>');
        sp_filter.push(spname[x]);
    }
    
    $( '#splist a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = sp_filter.indexOf( val ) ) > -1 ) {
                    sp_filter.splice( idx, 1 );
                    setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } 
            else {
                    sp_filter.push( val );
                    setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();

//            console.log( sp_filter );
            m_getFilter();
            return false;
    });
    
    var bus_id = '{{$already_booked_bus_id}}';

    
//    console.log(bus_id);
    $('#bus_'+bus_id).trigger('click');
</script>