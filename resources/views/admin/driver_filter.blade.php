<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';

//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th style="text-align: center;">Driver Name</th>
              <th style="text-align: center;">Route Name</th>
              <th style="text-align: center;">Bus Name</th>
              <th style="text-align: center;">Email</th>
              <th style="text-align: center;">Contact No.</th>
              <th style="text-align: center;">Address</th>
<!--              <th style="text-align: center;">Id Proof</th>
              <th style="text-align: center;">ID proof number</th>-->
              <th style="text-align: center;">Crew</th>
              <th style="text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <input type="hidden" id='ptype-{{ $r['id'] }}' name="{{$r['id_proof']}}" value="{{$r['id_proof']}}" >
                <input type="hidden" id='pnum-{{ $r['id'] }}' value="{{ $r['id_proof_number'] }}" >
                <th><span id='name-{{ $r['id'] }}'>{{ $r['name'] }}</span></th>
                <td><span id='route-{{ $r['id'] }}'>
                        <?php 
                        if(count($r['bus_details']) > 0){
                            foreach($r['bus_details'] as $bus){
                                if(isset($bus['buses']['bus_routes']) && count($bus['buses']['bus_routes'] > 0)){
                                    foreach($bus['buses']['bus_routes'] as $b){
                                        echo $b['name'].', <br>';
                                    }
                                }
                                
                            }
                        }
                            
                        ?>
                    </span>
                </td>
                <td><span id='bname-{{ $r['id'] }}'>
                    <?php    
                    if(count($r['bus_details']) > 0){
                        foreach($r['bus_details'] as $bus){
                            echo $bus['buses']['name'].', <br>';
                        }
                    }
                     ?>
                        
                    </span></td>
                <td><span id='email-{{ $r['id'] }}'>{{ $r['email'] }}</span></td>
                <th><span id='con-{{ $r['id'] }}'>{{ $r['mobile'] }}</span></th>
                <td><span id='addre-{{ $r['id'] }}'>{{ $r['address'] }}</span></td>
<!--                <td><span id='ptype-{{ $r['id'] }}' name="{{$r['id_proof']}}">{{$r['id_proof']}}</span></td>
                <td><span id='pnum-{{ $r['id'] }}'>{{ $r['id_proof_number'] }}</span></td>-->
                <td><span id='crew-{{ $r['id'] }}'>{{ $r['crew'] }}</span></td>
                <th style="text-align: center;">
                    <a class="btn-floating btn-small blue" id="" onclick="editD( {{$r['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delD( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </th>
            </tr>
            <?php } ?>
            
        </tbody>
    </table>
    
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
