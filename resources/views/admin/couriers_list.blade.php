@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('admin/couriers-filter',crnt,len,type,'','');
        
    };
    
    function delCourier(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete This Courier ??';
        var onyes ='delData';
        var param='\'admin/delete-couriers\','+id+' , \'admin/couriers-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addCourier(){
        
        $('#add-name').val('');
        $('#modal-courier').find('#mdlyes').attr('onclick','addNewCourier()');
        $('#modal-courier').find('#mdlyes').html('Add');
        $('#modal-courier').openModal();
        $('#add_courier').parsley().reset();
        var chk = $('#status').is(':checked');
        if(!chk){
            $('#status').trigger('click');
        }
    }
    function upCourier(id){
        $('#add-name').val($('#aname-'+id).html().trim());
        $('#modal-courier').find('#mdlyes').attr('onclick','editCourier('+id+')')
        $('#modal-courier').find('#mdlyes').html('Update');
        $('#modal-courier').openModal();
        $('#add_courier').parsley().reset();
        
        var status = $('#status-'+id).attr('name');
        var chk = $('#status').is(':checked');
        if(status == 1 && !chk){
            $('#status').trigger('click');
        }else if(status == 0 && chk){
            $('#status').trigger('click');
        }
    }
    
    function addNewCourier(){
        var url="{{URL::to('admin/add-couriers')}}";
        $('#add_courier').attr('action',url);
        $('#add_courier').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-courier').closeModal();
                filterData('admin/couriers-filter');
            }
        });
    }
    function editCourier(id){
//        alert('called');
        var url="{{URL::to('admin/edit-couriers')}}";
        $('#c_id').val(id);
        $('#add_courier').attr('action',url);
        $('#add_courier').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-courier').closeModal();
                filterData('admin/couriers-filter');
            }
        });
    }
   
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Couriers</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Couriers</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l2 m12 s12"></div>
        <div class="col l8 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Couriers </h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/couriers-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/couriers-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/couriers-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/couriers-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/couriers-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/couriers-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l2 m12 s12"></div>
        
        <!-- Modal Structure -->
        <form id="add_courier" method="post" action="" data-parsley-validate >
        <div id="modal-courier" class="modal bust-modal" style='width:35%;font-size:100%;' >
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>Courier Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <input type="hidden" name="c_id" id="c_id" value="" />
                    <div class="input-field">
                        <input id="add-name" name="name" type="text" placeholder="" required="">
                        <label for="add-dname">Courier Name</label>
                    </div>
                    
                    <div class="input-field">
                        <p class="switch" style='padding: 10px 0 20px 0;'>
                            <label>
                                <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                <input type="checkbox" id='status' name='status' value="1" />
                                <span class="lever" style="margin-bottom: 8px;"></span>

                            </label>
                        </p>
                    </div>
                    
                </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id='mdlyes' class="modal-action waves-effect waves-green btn-flat ">Add</button>
          </div>
        </div>
        </form>
        
        <!-- Modal Structure Over-->
       
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            
            <a class="btn-floating btn-large red tooltipped" onclick="addCourier();"data-position="top" data-delay="50" data-tooltip="Add Service Provider">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
    <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
    
@endsection