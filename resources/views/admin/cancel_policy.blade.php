@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP' ? 'sp' : 'admin';
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/policy-filter',crnt,len,type,'','');
    };
    
    function delD(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Policy ??';
        var onyes ='delData';
        var param='\'{{$lt}}/delete-policy\','+id+' , \'{{$lt}}/policy-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function addD(){
        $('#sp_filter').val('');
        $('#sp_id').val('');
        $('#time').val('');
        $('#amount').val('');
        
        var chk = $('#status').is(':checked');
        if(!chk){
            $('#status').trigger('click');
        }
        
        $('#add_policy').attr('action',"{{URL::to($lt.'/add-new-policy')}}");
        
        $('#modal-policy').find('#save-d').html('Add');
        $('#modal-policy').openModal();
        $('#add_policy').parsley().reset();
    }
    
    function editD(id){
        $('#sp_filter').val($('#sp-'+id).html());
        $('#sp_id').val($('#sp-'+id).attr('name'));
        $('#duration').val($('#duration-'+id).html());
        $('#time').val($('#time-'+id).html());
        $('#amount').val($('#amount-'+id).html());
        
        $('#did').val(id);
        $('#opr').val('Update');
        
        var status = $('#status-'+id).attr('name');
        var chk = $('#status').is(':checked');
        if(status == 1 && !chk){
            $('#status').trigger('click');
        }else if(status == 0 && chk){
            $('#status').trigger('click');
        }
        
        $('#add_policy').attr('action',"{{URL::to($lt.'/edit-policy')}}");
        
        $('#modal-policy').find('#save-d').html('Update');
        $('#modal-policy').openModal();
        
        $('#add_policy').parsley().reset();
        
    }
    
    $(document).on('click','#save-d',function(){
        $('#add_policy').ajaxForm(function(res) {
           if(res.flag == 1){
               if($('#opr').val()==='Update'){
                   Materialize.toast('Policy Details Updated successfully !!', 2000,'rounded');
               }
               else{
                    Materialize.toast('Policy Added successfully !!', 2000,'rounded');
                }
                $('#modal-policy').closeModal();
               filterData('{{$lt}}/policy-filter');
               
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });
    var flag = 0;
    $(document).on('keyup','#sp_filter',function(){
        var s = $(this).val();
        if(s == ''){
            $('#sp_id').val('');
        }
        if(s.length < 2){
            flag = 1;
            return false;
        }
        if(flag == 1){
            flag = 0;
           var url = '{{URL::to($lt."/sp-name")}}';
           var data = {name:s};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#sp_filter').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#sp_id').val(key);
                        }else if($('#sp_filter').val() == ''){
                            $('#sp_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
        
    });

</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Cancellation Policies</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Cancellation Policies</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" onchange="return filterData('{{$lt}}/policy-filter');">
                        <label for="input_text">Filter By Service provider name</label>
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" onclick="return filterData('{{$lt}}/policy-filter');"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Cancellation Policies</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/policy-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/policy-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/policy-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/policy-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/policy-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/policy-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="add_policy" action="" method="post" enctype="multipart/form-data" data-parsley-validate >
          <div id="modal-policy" class="modal bust-modal" style='width:40%;font-size:100%;overflow: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Cancellation Policy</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="did" id="did" value="" />
                <input type="hidden" name="opr" id='opr' value="Add" />
                    <div class="content">
                        
                        <div class="input-field">

                            <input id="sp_filter" name="sp_filter" type="text" placeholder="" autocomplete="off" required>
                            <input id="sp_id" name="sp_id" type="hidden" >
                            <label for="sp_id">Service Provider</label>
                        </div>
                       
                        <div class="input-field">
                            <div class="input-field">
                                <select id="duration" name="duration" required>
                                    <option value='Before' selected>Before</option>
                                </select>
                                <label for="duration">Duration for cancellation</label>
                            </div>
                        </div>
                        <div class="input-field">
                            <input id="time" name="time" type="number" data-parsley-type="digits" placeholder="Time in hours" required>
                            <label for="height">Time in Hours</label>
                        </div>
                        <div class="input-field">
                            <input id="amount" name="amount" type="number" data-parsley-type="digits"  placeholder="amount in percentage" required>
                            <label for="height">Refundable amount in percentage(%)</label>
                        </div>
                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                    <input type="checkbox" id='status' name='status' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>

                                </label>
                            </p>
                        </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-d" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            
            <a class="btn-floating btn-large red tooltipped" onclick="addD();"data-position="top" data-delay="50" data-tooltip="Add Driver">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>


@endsection