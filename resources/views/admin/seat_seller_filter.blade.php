<?php
$lt = config('constant.LOGGER') == 'SS'?'ss':'admin';
if($type=='Main') {
//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Seat Seller Name</th>
              <th>Email</th>
              <th>City</th>
              <th>Contact No.</th>
              <th>Deposit Balance</th>
              <th>Commission</th>
              <th>Total Tickets</th>
<!--              <th>Total Seat</th>
              <th>Transfer</th>-->
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <input type="hidden" id="status-{{$r['id']}}" value="{{$r['status']}}" />
                <input type="hidden" id="hf-{{$r['id']}}" name='{{$r['handling_fee_type']}}' value="{{ $r['handling_fee']}}" />
                <!--<th><a href="{{URL::to($lt.'/seatseller-details/'.$r['id'])}}"><span id='name-{{ $r['id'] }}'>{{ $r['name'] }}</span></a></th>-->
                <th><a onclick="ssdetails( {{$r['id']}} )" style="cursor: pointer;"><span id='name-{{ $r['id'] }}'>{{ $r['name'] }}</span></a></th>
                <td><span id='email-{{ $r['id'] }}'>{{ $r['email'] }}</span></td>
                <td><span id='city-{{ $r['id'] }}'>{{ $r['city'] }}</span></td>
                <th><span id='con-{{ $r['id'] }}'>{{ $r['mobile'] }}</span></th>
                <td><a><span id='bal-{{ $r['id'] }}'>Rp.{{ $r['balance'] }}</span></a></td>
                <td><a href='{{URL::to($lt.'/commission-management/ss/'.$r['id'])}}'><span id='comm-{{ $r['id'] }}' name="{{$r['comm_type']}}">{{ $r['comm']}}</span></a></td>
                <td><a href="{{URL::to($lt.'/ticket-list/'.$r['id'].'/5')}}">{{ $r['total_ticket']}}</a></td>
<!--                <td><a><span id='totalseat-{{ $r['id'] }}'>0</span></a></td>
                <td><a><span id='trans-{{ $r['id'] }}'>Not Found</span></a></td>-->
                <th>
                    <a class="btn-floating btn-small blue" id="" onclick="editss( {{$r['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delSeatSeller( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    <?php if($r['status']==1){ ?>
                    <i class="mdi-image-brightness-1 " style='color: green;'></i>
                <?php }
                else{ ?> 
                    <i class="mdi-image-brightness-1 " style='color: red;'></i>
                <?php }?>
                </th>
            </tr>
            <?php } ?>
            
        </tbody>
    </table>
    <?php }
    else{ 
//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr >
                <th>Seat Seller Name</th>
                <th>Seat Seller Parent</th>
                <th>Email</th>
                <th>City</th>
                <th>Contact No.</th>
                <th>Deposit Balance</th>
                <th>Commission</th>
                <th>Total Tickets</th>
<!--                <th>Total Seat</th>
                <th>Transfer</th>-->
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($data['result'] as $r) { 
                $type = 6;
                if($r['type'] == 'B'){
                    $type = 7;
                }
                ?>
            <tr>
                <input type="hidden" id="status-{{$r['id']}}" value="{{$r['status']}}" />
                <input type="hidden" id="hf-{{$r['id']}}" name='{{$r['handling_fee_type']}}' value="{{ $r['handling_fee']}}" />
                <th><a href="{{URL::to($lt.'/seatseller-details/'.$r['id'])}}"><span id='name-{{ $r['id'] }}'>{{ $r['name'] }}</span></a></th>
                <td><span id='parent-{{ $r['id'] }}' name="{{$r['parent_info']['id']}}">{{ $r['parent_info']['name'] }}</span></td>
                <td><span id='email-{{ $r['id'] }}'>{{ $r['email'] }}</span></td>
                <td><span id='city-{{ $r['id'] }}'>{{ $r['city'] }}</span></td>
                <th><span id='con-{{ $r['id'] }}'>{{ $r['mobile'] }}</span></th>
                <td><a><span id='bal-{{ $r['id'] }}'>Rp.{{ $r['balance'] }}</span></a></td>
                <td><a><span id='comm-{{ $r['id'] }}' name="{{$r['comm_type']}}">{{$r['comm']}}</span></a></td>
                <td><a href="{{URL::to($lt.'/ticket-list/'.$r['id'].'/'.$type)}}">{{ $r['total_ticket']}}</a></td>
<!--                <td><a><span id='totalseat-{{ $r['id'] }}'>0</span></a></td>
                <td><a><span id='trans-{{ $r['id'] }}'>Not Found</span></a></td>-->
                <th>
                    <a class="btn-floating btn-small blue" id="" onclick="editss( {{$r['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delSeatSeller( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    <?php if($r['status']==1){ ?>
                        <i class="mdi-image-brightness-1 " style='color: green;'></i>
                    <?php }else{ ?> 
                        <i class="mdi-image-brightness-1 " style='color: red;'></i>
                    <?php }?>
                </th>
            </tr>
            <?php } ?>
            
      </tbody>
    </table>
    <?php } ?>
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
