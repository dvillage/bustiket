<?php
$lt = config('constant.LOGGER') == 'SP'?'sp': 'admin' ;
?>

<tr>
    <td class="default_padding">{{$driver['name']}} <input type="hidden" name="drivers[]" value="{{$driver['id']}}" /> </td>
    <td class="default_padding">{{$driver['mobile']}}</td>
    <td class="default_padding">{{$driver['email']}}</td>
    <td class="default_padding">{{$driver['address']}}</td>
    <td class="default_padding center"><i id="remove_d{{$driver['id']}}" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear small" ></td>
</tr>    