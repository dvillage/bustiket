@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SS'?'ss': 'admin' ;
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/seat-seller-deposit',crnt,len,type,'','');
    };
    
    function approve(id,type){
        var m = 'approve';
        if(type == 'R'){
            var m = 'reject';
        }
        var title='<b>Confirmation<b>';
        var msg='Are you sure to '+m+' this deposit request ??';
        var onyes ='approveAjax';
//        var param='\'admin/del-seatseller\','+id+' , \'admin/seat-seller-deposit\'';
        var param=id+',\''+type+'\'';
        var mdlname='confirmApprove';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function approveAjax(id,type){
        var url = '{{URL::to($lt."/approve-ss-deposite")}}';
        var data = {id:id,type:type};
        postAjax(url,data,function(res){
            if(typeof res.flag != 'undefined' && res.flag !=1){
                Materialize.toast(res.msg, 2000,'rounded');
            }
            Materialize.toast(res.msg, 2000,'rounded');
            filterData('{{$lt}}/seat-seller-deposit');
        });
    }
    $(document).on('click','#search',function(){
        var stype = $('#sstype :selected').val();
        var status = $('#status :selected').val();
        var url = '{{$lt}}/seat-seller-deposit';
        var data = 'ss_type='+stype+'&status='+status;
//        pagination(url,data,'vtable');
        var data = {ss_type:stype,status:status};
        
        filterData('{{$lt}}/seat-seller-deposit','',data);
    });
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Seat Seller Deposit Request</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Seat Seller Deposit Request</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <!--<input id="searchname" type="text" class="validate" onchange="return filterData('{{$lt}}/seat-seller-deposit');">-->
                        <input id="searchname" type="text" class="validate" onchange="">
                        <label for="input_text">Filter By Seat Seller Name</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="sstype">
                            <option value="">All</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                        <label for="input_text">Filter By Seat Seller Type</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="status">
                            <option value="">All</option>
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                            <option value="0">Pending</option>
                        </select>
                        <label for="input_text">Filter By Status</label>
                        
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a id="search" class="btn"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Seat Seller Deposit Request</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/seat-seller-deposit',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/seat-seller-deposit',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/seat-seller-deposit');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/seat-seller-deposit',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/seat-seller-deposit',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/seat-seller-deposit',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="confirmApprove" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
    </div>


@endsection