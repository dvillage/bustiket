@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        /*padding: 0px;*/
        margin: 0px;
        /*height: 36px;*/
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/coupon-filter',crnt,len,type,'','');
    };
    
    function delCouponCode(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Coupon Code ??';
        var onyes ='deleteCode';
        var param='\'{{URL::to($lt."/delete-coupon-code")}}\','+id;
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function deleteCode(url,id){
//        url = '{{URL::to('+url+')}}';
        data={id:id};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
                location.reload();
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    }
    
    function addcc(){
        $('#ccode').val('');
        $('#csdate').val('');
        $('#cedate').val('');
        $('#ctype').val('');
        $('#cvalue').val('');
        $('#cminvalue').val('');
        $('#cmaxvalue').val('');
        $('#ctype').material_select();
        $('#opr').val('Add');
        $('#bus_id').val('');
        $('#bus_name').val('');

        $('#add_coupon_code').attr('action',"{{URL::to($lt.'/add-coupon-code')}}");

        $('#add_coupon_code_div').find('#save_ccode').html('Add');
        $('#add_coupon_code_div').openModal();
        $('#status').trigger('click');
        $('#add_coupon_code').parsley().reset();
    }

    function editcc(id){

        $('#ccode').val($('#ccode-'+id).html());
        $('#csdate').val($('#csdate-'+id).html());
        $('#cedate').val($('#cedate-'+id).html());
        $('#ctype').val($('#ctype-'+id).attr('name'));
        $('#bus_id').val($('#bus-'+id).val());
        $('#bus_name').val($('#bus-'+id).attr('name'));
        $('#cvalue').val($('#cvalue-'+id).html());
        $('#cminvalue').val($('#cmin-'+id).html());
        $('#cmaxvalue').val($('#cmax-'+id).html());
        $('#ccid').val(id);
        $('#opr').val('Update');
        $('#ctype').material_select();
        $('#add_coupon_code').attr('action',"{{URL::to($lt.'/edit-coupon-code')}}");

        $('#add_coupon_code_div').find('#save_ccode').html('Update');
        $('#add_coupon_code_div').openModal();

        var status = $('#status-'+id).val();
        var chk = $('#cstatus').is(':checked');
        if(status == 1 && !chk){
            $('#cstatus').trigger('click');
        }else if(status == 0 && chk){
            $('#cstatus').trigger('click');
        }

        $('#add_coupon_code').parsley().reset();
    }
     $(document).on('click','#save_ccode',function(){
         var lt = '{{$lt}}';
         if(lt == 'sp'){
             if($('#bus_id').val() == ''){
                 Materialize.toast('please select bus', 2000,'rounded');
                 return false;
             }
         }
         
         $('#add_coupon_code').ajaxForm(function(res) {
            if(res.flag == 1){
                if($('#opr').val() == 'Update'){
                    Materialize.toast('Coupon Code updated successfully !!', 2000,'rounded');
                }else{
                    Materialize.toast('Coupon Code added successfully !!', 2000,'rounded');
                }
                location.reload();
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
     });
     
     var flag2 = 0;
    $(document).on('keyup','#bus_name',function(){
        if($('#bus_name').val() == ''){
            $('#bus_id').val('');
        }
        
        var s = $(this).val();
        if(s.length < 2){
            flag2 = 1;
            return false;
        }
        if(flag2 == 1){
            flag2 = 0;
            
           var url = '{{URL::to($lt."/bus-name")}}';
           var data = {name:s};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#bus_name').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#bus_id').val(key);
                        }else if($('#bus_name').val() == ''){
                            $('#bus_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
     
     $(document).ready(function(){

        window.Parsley.addValidator('dateDiff', {
            validateString: function(value, id) {
//                return 0 === value % requirement;
                var from_date = $(id).val();
                var to_date = value;
                from_date = from_date.split('-');
                new_date = new Date(from_date[0], from_date[1], from_date[2]);
                date1 = to_date.split('-');
                new_date1 = new Date(date1[0], date1[1], date1[2]);
                if(new_date1 < new_date){
                    return false;
                }
            },
            messages: {
                    en: 'the to date must be after from date.'
            }
        });
        
        $("#upload-csv").click(function(){
            $("#btn-browse-csv").click();
        });
        
        $("#btn-browse-csv").change(function(){
            $("#btn-submit-csv").click();
        });
        
        $("#csv-upload-form").ajaxForm(function(res){
                var jdata=JSON.parse(JSON.stringify(res));
               getData('admin/coupon-filter',1,10,'','','');
        });
    });
    
    
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Coupon Code</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Coupon Code</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
            <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate">
                        <label for="input_text">Filter By Code</label>
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" onclick="return filterData('admin/coupon-filter');"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Coupon Code List
                                <a href="#" class="btn btn-small " id="upload-csv" style="float:right;width:151px;margin-left:10px;"><i class="mdi-file-cloud-upload right " ></i>Upload  CSV</a>
                                <a href="{{URL::to('admin/create-coupon-code-csv')}}" class="btn btn-small blue " download="" style="float:right;width:169px;"><i class="mdi-file-cloud-download right"></i>  Download CSV </a>
                        </h4>
                        <div style="display: none;">
                        <form action="{{URL::to('admin/upload-coupon-code-csv')}}" method="post" enctype="multipart/form-data" id="csv-upload-form">
                            <input type="file" name="csv" id="btn-browse-csv">
                            <input type="submit" id="btn-submit-csv">
                            {{csrf_field()}}
                        </form>
                        </div>
                       
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/coupon-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/coupon-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/coupon-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;padding:initial; '></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/coupon-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="last" onclick="filterDataWith('{{$lt}}/coupon-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('{{$lt}}/coupon-filter',this.value);">
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
        $type = '';
        if($lt == 'sp'){
            $type = config('constant.SP_TYPE');
        }
        if($lt == 'admin' || ($lt == 'sp' && $type == 'Main')){ ?>
        <!-- Modal Structure -->
        <form id="add_coupon_code" action="{{URL::to($lt.'/add-coupon-code')}}" method="post" enctype="multipart/form-data" data-parsley-validate >
        <div id="add_coupon_code_div" class="modal bust-modal" style="width: 35%;font-size: 100%;overflow-x: hidden;">
            <div class="modal-content">
              <h4>Add Coupon Code</h4>
              <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="bus_id" id="bus_id" value="" />
              <input type="hidden" name="ccid" id="ccid" value="" />
              <input type="hidden" name="opr" id="opr" value="" />
              <div class="input-field">
                  <input id="ccode" type="text" name="ccode" class="validate" required="" placeholder="" >
                <label for="input_text">Coupon Code</label>
              </div>
              <?php
              $req = '';
              $opt = '(Optional)';
              if($lt == 'sp'){
                $req = 'required';
                $opt = '';
              }
              ?>
              <div class="input-field">
                  <input id="bus_name" type="text" name="bus_name" class="validate" <?php echo $req; ?> Placeholder='' >
                <label for="input_text">Select Bus {{$opt}} </label>
              </div>
              
              <div class="input-field">
                  <input id="csdate" type="text" name="csdate" data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="" >
                <label for="input_text">Start Date</label>
              </div>
              
              <div class="input-field">
                  <input id="cedate" type="text" name="cedate" data-inputmask="'mask': 'y-m-d'" required data-parsley-date-diff="#csdate" class="validate pikaday" placeholder="" >
                <label for="input_text">End Date</label>
              </div>
              <div class="row">
                  <div class="col s12 m6 l6">
                  <div class="input-field">
                    <input id="cvalue" type="number" name="cvalue" required="" placeholder="" class="validate" >
                  <label for="input_text">Coupon Value</label>
                </div>
              </div>
              
              <div class="col s12 m6 l6">
                  <div class="input-field">
                    <select id="ctype" name="ctype" required="">
                      <option value="" disabled selected>Select Type</option>
                      <option value="P">Percentage</option>
                      <option value="F">Fix</option>
                    </select>
                        <label for="input_text">Coupon Type</label>
              </div>
              </div>
              
              </div>
              <div class="input-field">
                  <input id="cminvalue" name="cminvalue" type="number"  class="validate" placeholder="" required="">
                <label for="input_text">Minimum Value ( Coupon Applied if ticket price is minimum of this amount )</label>
              </div>
              <div class="input-field">
                  <input id="cmaxvalue" name="cmaxvalue" type="number" class="validate" placeholder="" required="">
                <label for="input_text">Maximum Discount amount</label>
              </div>
              <div class="input-field">
                  <p class="switch">
                        <label>
                            Status
                            <input id="cstatus" name="cstatus" type="checkbox" />
                          <span class="lever"></span>
                          
                        </label>
                      </p>
              </div>
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
              <!--<a id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</a>-->
              <button id="save_ccode"  class="waves-effect waves-green btn-flat ">Save</button>
            </div>
        </div>
    </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" href="#add_coupon_code_div" onclick="addcc();"data-position="top" data-delay="50" data-tooltip="Add Coupon Code">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <?php } ?>
    </div>


@endsection