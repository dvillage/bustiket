@extends('layouts.admin')

@section('content')
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
    }
</script>

    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m12 l12">
          <h1>Cached</h1>

         
        </div>
        
      </div>

    </div>
    <!-- /Breadcrumb -->

    <!-- Stats Panels -->
    <div class="row sortable">
        <a href="{{URL::to('admin/cached/?key=all_term')}}">Lorena All Terminals</a><br>
        <a href="{{URL::to('admin/cached/?key=bus_list')}}">Lorena Bus List</a><br>
        <a href="{{URL::to('admin/cached/?key=bus_info')}}">Lorena Bus Info</a><br>
    </div>
    <div class="row">
        <?php
        
//        $book = \App\Models\Lorena::lorena_booking_inquiry('BISLHAJNNC', '11LA04SA17U3W7');
//        $book = \App\Models\Lorena::lorena_payment_inquiry('BISLHAJNNC', '11LA04SA17U3W7');
//        $book = \App\Models\Lorena::lorena_all_city();
//        dd($book);
        
        $key = $body['key'];
        $data = $body['data'];
//        print_r($data);
        if($key == 'all_term'){
            ?>
        <table class="bordered">
            <tr>
                <th>City ID</th>
                <th>City Name</th>
                <th>Points</th>
            </tr>
            <?php
            foreach($data as $d){
                ?>
            <tr>
                <td>{{$d['city_id']}}</td>
                <td>{{$d['city_name']}}</td>
                <td>{{json_encode($d['points'])}}</td>
            </tr>
            <?php
            }
            ?>
        </table>
        <?php
        }elseif($key == 'bus_list'){
//            dd($data);
            $data = array_reverse($data);
            ?>
        <table class="bordered">
            <tr>
                <th>From ID</th>
                <th>To ID</th>
                <th>Traveling Date</th>
                <th>Buses</th>
                <th>Created at</th>
            </tr>
            <?php
            foreach($data as $k=>$d){
                $det = explode('_', $k);
                $det = explode('-', $det[1]);        
                $from_id = $d['from_id'];
                $to_id = $d['to_id'];
                $travel_date = $d['traveling_date'];
                ?>
            <tr>
                <td>{{$from_id}}</td>
                <td>{{$to_id}}</td>
                <td>{{$travel_date}}</td>
                <td style="width: 70%;" > <div style="height: 50px;overflow-y:scroll"> {{json_encode($d['bus'])}} </div></td>
                <td>{{$d['created_at']}}</td>
            </tr>
            <?php
            }
            ?>
        </table>
        <?php
        }elseif($key == 'bus_info'){
            krsort($data);
            
            ?>
        <table class="bordered" style="width: 100%">
            <tr>
                <th>Booking ID</th>
                <th>Route Code</th>
                <th>Bus Name</th>
                <th>Book Code</th>
                <th>Pay Status</th>
                <th>Book Status</th>
                <th>Booking Response</th>
                <th>Payment Response</th>
                <th>Cancel Response</th>
                <th>Routes</th>
                <th>Bus Detail</th>
                <th>Created At</th>
            </tr>
            <?php
            if(count($data)> 0){
            foreach($data as $k=>$d){
                foreach($d as $da){
//                    $bus = \App\Models\Lorena::get_bus_info($da['booking_id']);
//                    dd($bus);
                ?>
            <tr>
                <td>{{$da['booking_id']}}</td>
                <td>{{$da['route_code']}}</td>
                <td>{{$da['bus_name']}}</td>
                <td>{{$da['book_code']}}</td>
                <td>{{$da['pay_status']}}</td>
                <td>{{$da['book_status']}}</td>
                <td style="" > <div style="height: 50px;width: 300px;overflow-y:scroll"> {{$da['booking_response']}} </div></td>
                <td style="" > <div style="height: 50px;width: 200px;overflow-y:scroll"> {{$da['payment_response']}} </div></td>
                <td style="" > <div style="height: 50px;width: 200px;overflow-y:scroll"> {{isset($da['cancel_response'])?$da['cancel_response']:''}} </div></td>
                <td>{{$da['routes']}}</td>
                <td style="width: 70%;" > <div style="height: 50px;width: 200px;overflow-y:scroll"> {{$da['bus_detail']}} </div></td>
                <td>{{isset($da['created_at'])?$da['created_at']:''}}</td>
            </tr>
            <?php
                }
            }
            ?>
        </table>
        <?php
            }
        }
        ?>
    </div>
@endsection
