<?php // dd($data); ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Banner Image</th>
            <th>Group</th>
            <th>Height</th>
            <th>Width</th>
            <th>Alter Text</th>
            <th>Redirect URL</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($data['result'] as $r) { ?>
        <input type="hidden" id="status-{{$r['id']}}" value="{{$r['status']}}" />
        <tr>
            <?php
            $group='';
                switch($r['group']){
                       
                        case 'header-slider':
                                        $group='Web Homepage Header Slider';
                                        break;
                    
                        case 'web-mobile-footer-slider':
                                        $group='Web Mobile Footer Slider';
                                        break;
                        
                        case 'passenger-app-slider':
                                        $group='Passenger App Slider';
                                        break;
                        case 'busway-web-slider':
                                        $group='Busway Web slider ';
                                        break;
                        case 'passenger-app-promo-slider':
                                        $group='Passenger App Promo slider';
                                        break;
                }
            ?>
            
            <td style="text-align:center;"><img src="{{URL::to('assets/images/banner/'.$r['file'])}}" id='img-{{$r['id']}}' name='{{$r['file']}}'style="height: 75px;width: 100px;"/></td>
            <td><span id='grp-{{ $r['id'] }}' name="{{$r['group']}}">{{$group}}</span></td>
            <td><span id='h-{{ $r['id'] }}' name='{{$r['height']}}'>{{ $r['height'].$r['height_type'] }}</span></td>
            <td><span id='w-{{ $r['id'] }}' name='{{$r['width']}}'>{{ $r['width'].$r['width_type'] }}</span></td>
            <td><span id='alt-{{ $r['id'] }}'>{{ $r['alt'] }}</span></td>
            <td><span id='rto-{{ $r['id'] }}'>{{ $r['redirect_to']}}</span></td>
            
            <td>
                <?php if($r['status']==1){ ?>
                    <i class="mdi-image-brightness-1 " style='color: green;'></i>
                <?php }
                else{ ?> 
                    <i class="mdi-image-brightness-1 " style='color: red;'></i>
                <?php }?>
                <a class="btn-floating btn-small blue" id='{{ $r['id'] }}' onclick="updatebnr( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                <a class="btn-floating btn-small btn red modal-trigger" id='{{ $r['id'] }}' onclick="delBanner( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />