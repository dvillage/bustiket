@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
       
    };
    
    
    function saveSettings(){
//        alert('called');
        var url="{{URL::to('admin/save-settings')}}";
        
        $('#add_settings').attr('action',url);
        $('#add_settings').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                location.reload();
            }
        });
    }
   
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Settings</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Settings</a></i>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        <form method="post" id="add_settings" enctype="multipart/form-data" >
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
        <div class="col s12">
<!--        <ul class="tabs">
          <li class="tab col s3"><a href="#general">General</a></li>
          
        </ul>-->
        </div>
        <div class="row" >
            <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">

                <button class="btn-floating btn-large green tooltipped" onclick="saveSettings();"data-position="top" data-delay="50" data-tooltip="Save Settings">
                    save
                </button>
            </div>
        </div>
        
        <div id="general" class="col s12">
            
            <?php
            $s = $body['settings'];
//            echo '<pre>';
//            print_r($s);
//            echo '</pre>';
//            exit;
            
                ?>
            <div class="row">
<!--                <div class="col s12 m2 l2">
                    <div class="input-field">
                        Sanitize Input
                    </div>
                </div>
                <div class="col s12 m4 l4">
                    <input name="sanitize_input" type="text" value="{{$s[0]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                    <div class="input-field">
                        Login Lock Duration
                    </div>
                </div>
                <div class="col s12 m4 l4">
                    <input name="login_lock_duration" type="text" value="{{$s[1]['val']}}">
                </div>-->
                <div class="col s12 m2 l2">
                    Max Login Attempt
                </div>
                <div class="col s12 m4 l4">
                    <input name="max_login_attempt" type="text" value="{{$s[2]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                    Login Url Token
                </div>
                <div class="col s12 m4 l4">
                    <input name="login_url_token" type="text" value="{{$s[4]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                   Website Name
                </div>
                <div class="col s12 m4 l4">
                     <input name="website_name" type="text" value="{{$s[5]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                    Email
                </div>
                <div class="col s12 m4 l4">
                    <input name="email" type="email" value="{{$s[9]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                    Home Page Keyword
                </div>
                <div class="col s12 m4 l4">
                    <input name="home_page_keyword" type="text" value="{{$s[7]['val']}}">
                </div>
                <div class="col s12 m2 l2">
                    Home Page Description
                </div>
                <div class="col s12 m4 l4">
                    <input name="home_page_description" type="text" value="{{$s[8]['val']}}">
                </div>
                
                <div class="col s12 m2 l2">
                    Maintenance Mode
                </div>
                <div class="col s12 m4 l4">
                    <div class="input-field">
                        <p class="switch" style='padding: 10px 0 20px 0;'>
                            <label>
                                <span style='font-size: 20px;padding-bottom: 10px;'></span>
                                <input type="checkbox" id='status' name='maintenance_mode' <?php echo $s[3]['val'] ? 'checked' : '' ?> />
                                <span class="lever" style="margin-bottom: 8px;"></span>

                            </label>
                        </p>
                    </div>
                </div>
                <div class="col s12 m2 l2">
                    Logo
                </div>
                <div class="col s12 m4 l4">
                    <div class="file-field input-field">
                        <div class="btn"  style="width:100px;">
                            <span class="">Logo Image</span>
                            <input type="file" name="logo" id="card_image" accept="image/*" >
                            <!--<label for="bnr_img">Upload Banner Image</label>-->
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    <?php
                        if($s[6]['val'] != ''){
                            $file = URL::to('assets/uploads/logo').'/'.$s[6]['val'];
                            echo '<img src="'.$file.'" style="height:40px;width:40px;" />';
                        }
                       ?> 
                </div>
                
            </div>
            
            
        </div>
        </form>
    </div>
    
    
    
@endsection