<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';

//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th style="text-align: center;">Service provider Name</th>
              <th style="text-align: center;">Duration</th>
              <th style="text-align: center;">Time in hour</th>
              <th style="text-align: center;">Refundable amount in %</th>
              <th style="text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                
                <th><span id='sp-{{ $r['id'] }}' name="{{$r['sp_id']}}">{{ $r['sp']['first_name'].' '.$r['sp']['last_name'] }}</span></th>
                <td><span id='duration-{{ $r['id'] }}'>{{$r['duration']}}</span></td>
                <td><span id='time-{{ $r['id'] }}'>{{$r['time']}}</span></td>
                <td><span id='amount-{{ $r['id'] }}'>{{ $r['amount'] }}</span>
                <span id='status-{{ $r['id'] }}' name= "{{$r['status']}}"></span>
                </td>
                
                <th style="text-align: center;">
                    <a class="btn-floating btn-small blue" id="" onclick="editD( {{$r['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delD( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    <?php if($r['status']==1){ ?>
                    <i class="mdi-image-brightness-1 " title="Active" style='color: green;'></i>
                    <?php }
                    else{ ?> 
                        <i class="mdi-image-brightness-1 "  title="InActive" style='color: red;'></i>
                    <?php }?>
                </th>
            </tr>
            <?php } ?>
            
        </tbody>
    </table>
    
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
