
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Service Provider</th>
              <th>Bus Type</th>
              <th>AC</th>
              <th>Image</th>
              <th>Other Images</th>
              <th>City</th>
              <th>Price</th>
              <th>Engine</th>
              <th>Body Manufacturer</th>
              <th>Fleet Detail</th>
              <th>Note</th>
              <th>Amenities</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='sp-{{ $r['id'] }}' name="{{$r['sp_id']}}">{{ $r['sp']['first_name']. ' '.$r['sp']['last_name'] }}</span></td>
                <td><span id='btype-{{ $r['id'] }}'> {{$r['type']}} </span></td>
                <td><span id='ac-{{ $r['id'] }}' name="{{$r['ac']}}" > {{ $r['ac'] ? 'Yes' : 'No' }}</span></td>
                <td>
                    <span id='bimg-{{ $r['id'] }}'> 
                        <?php
                        if($r['image'] != ''){
                           $file = URL::to('assets/uploads/pariwisata').'/'.$r['image'];
                           $file_check = config('constant.UPLOAD_PARIWISATA_DIR_PATH').$r['image'];
                           if(file_exists($file_check)){
                               echo '<img id="img-'.$r['id'].'" src="'.$file.'" style="height:25px;width:25px"/>';
                           }
                        }
                        ?>
                    </span>
                </td>
                <td>
                    <span id='oimg-{{ $r['id'] }}'> 
                        <?php
                        $other = $r['other_images'];
                        if($other != ''){
                        $other = explode(',',$other);
                        foreach($other as $o){
                            $file = URL::to('assets/uploads/pariwisata').'/'.$o;
                            $file_check = config('constant.UPLOAD_PARIWISATA_DIR_PATH').$o;
                            if(file_exists($file_check) && $o!= ''){
                                echo '<img src="'.$file.'" class="oimg-'.$r['id'].'" name="'.$o.'" style="height:25px;width:25px"/>';
                            }
                        }
                            
                        }
                        ?>
                    </span>
                </td>
                <td><span id='city-{{ $r['id'] }}' name="{{$r['city_id']}}">{{ $r['city']['name']}}</span></td>
                <td><span id='price-{{ $r['id'] }}'> {{$r['price']}} </span></td>
                <td><span id='engine-{{ $r['id'] }}'> {{$r['engine']}} </span></td>
                <td><span id='manufacurer-{{ $r['id'] }}'> {{$r['body_manufacturer']}} </span></td>
                <td><span id='fleet_detail-{{ $r['id'] }}'> <?php echo html_entity_decode($r['fleet_detail']);?> </span></td>
                <td><span id='note-{{ $r['id'] }}'> <?php echo html_entity_decode($r['note']);?> </span></td>
                <td>
                    <?php 
                        $am = $r['amenities'];
                        $alla = '';
                        $allid = '';
                        if(count($am)> 0){
                            foreach($am as $m){
                                $amc = $m['amenities'];
                                    if($alla == ''){
                                        $alla = $amc['name'];
                                        $allid = $amc['id'];
                                    }else{
                                        $alla .= ','.$amc['name'];
                                        $allid .= ','.$amc['id'];
                                    }
                                    $name = preg_replace('/[^A-Za-z0-9]/', '_', $amc['name']);
                                    echo '<input type="hidden" id="'.$name.'_id" value="'.$amc['id'].'" />';
                            }
                            
                        }
                        
                        ?>
                    <span id='amenity-{{ $r['id'] }}'>{{$alla}} </span>
                    <input type="hidden" id="all_amenity-{{$r['id']}}" value="{{$allid}}" />
                    <input type="hidden" id="all_prices-{{$r['id']}}" value="{{json_encode($r['prices'])}}" />
                </td>
                <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upPrw( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delPrw( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />