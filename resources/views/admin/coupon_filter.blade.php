<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
$type = '';
if($lt == 'sp'){
    $type = config('constant.SP_TYPE');
}
?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Code</th>
            <th style="text-align: center;">For ( Bus / Global )</th>
            <th style="text-align: center;">Start Date</th>
            <th style="text-align: center;">End Date</th>
            <th style="text-align: center;">Type</th>
            <th style="text-align: center;">Value</th>
            <th style="text-align: center;">Minimum Amount</th>
            <th style="text-align: center;">Max Amount</th>
            <?php if($lt == 'admin' || ($lt == 'sp' && $type == 'Main')){ ?>
            <th style="text-align: center;">Action</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        
        <?php foreach($data['result'] as $r) { ?>
        <!--<input type="hidden" id="status-{{$r['id']}}" value="{{$r['status']}}" />-->
        <tr>
            <td style="text-align: center;">
                <input type="hidden" id="status-{{$r['id']}}" value="{{$r['status']}}" />
                <input type="hidden" id="bus-{{$r['id']}}" name='{{$r['bus_details']['name']}}' value="{{$r['bus_id']}}" />
                
                <?php echo $r['status'] == 1 ? '<i style="color:green">Active<i>' : '<i style="color:red">Inactive</i>' ; ?>
            </td>
            <td style="text-align: center;"><span id='ccode-{{ $r['id'] }}'>{{$r['code']}}</span></td>
            <td style="text-align: center;"><span id='busname-{{ $r['id'] }}'><?php echo isset($r['bus_id']) && $r['bus_id'] != null && $r['bus_id'] != '' ? $r['bus_details']['name'] : '( Global )'; ?></span></td>
            <td style="text-align: center;"><span id='csdate-{{ $r['id'] }}'>{{date('Y-m-d',strtotime($r['start_date']))}}</span></td>
            <td style="text-align: center;"><span id='cedate-{{ $r['id'] }}'>{{date('Y-m-d',strtotime($r['end_date']))}}</span></td>
            <td style="text-align: center;"><span id='ctype-{{ $r['id'] }}' name="{{$r['type']}}" >{{$r['type'] == 'P'?'Percentage':'Fix'}}</span></td>
            <td style="text-align: right;"><span id='cvalue-{{ $r['id'] }}'>{{(int)$r['value']}}</span></td>
            <td style="text-align: right;"><span id='cmin-{{ $r['id'] }}'>{{(int)$r['min_amount']}}</span></td>
            <td style="text-align: right;"><span id='cmax-{{ $r['id'] }}'>{{(int)$r['max_amount']}}</span></td>
            <?php if($lt == 'admin' || ($lt == 'sp' && $type == 'Main')){ ?>
            <td style="text-align: center;">
                <a class="btn-floating btn-small blue" id="" onclick="editcc( {{$r['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                <a class="btn-floating btn-small red btn " id= "" onclick="delCouponCode( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
            <?php } ?>
        </tr>
        <?php } ?>
    </tbody>
</table>
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />