@extends('layouts.admin')

@section('content')
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
    }
</script>
<?php
    $lt = $body['logger'] == 'SS' ? 'ss' : ($body['logger'] == 'SP' ? 'sp' : 'admin');
    $tag = $lt == 'ss' ? 'Seat Seller' : 'Service Provider';
    $adm = $lt == 'ss' ? 'Seat Seller' : 'Admin';
?>
    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m12 l12">
          <h1>Reporting</h1>

          
        </div>
        
      </div>

    </div>
    <!-- /Breadcrumb -->

    <!-- Stats Panels -->
    <input type="hidden" id="smaindata" value="{{$body['smdata']}}" >
    <input type="hidden" id="sadata" value="{{$body['sadata']}}" >
    <input type="hidden" id="sbdata" value="{{$body['sbdata']}}" >
    <input type="hidden" id="tsale" value="{{$body['tsale']}}" >
    <input type="hidden" id="smcomm" value="{{$body['smcomm']}}" >
    <input type="hidden" id="sabcomm" value="{{$body['sabcomm']}}" >
    <input type="hidden" id="smonth" value="{{$body['smonth']}}" >
    <input type="hidden" id="sdaily" value="{{$body['sdaily']}}" >
    <input type="hidden" id="ssasale" value="{{$body['ssasale']}}" >
    <input type="hidden" id="ssbsale" value="{{$body['ssbsale']}}" >
    <div class="row sortable">
        <div class="row">
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Number of Tickets Sold</b></div>
                        <div class="collapsible-body">
                            <svg id="ssmain" class="mypiechart"></svg>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Number of Tickets Sold By {{$tag}} Type A</b></div>
                        <div class="collapsible-body">
                            <svg id="ssa" class="mypiechart"></svg>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Number of Tickets Sold By {{$tag}} Type B</b></div>
                        <div class="collapsible-body">
                            <svg id="ssb" class="mypiechart"></svg>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Total Sales (Rp.)</b></div>
                        <div class="collapsible-body">
                            <div id="totalsale">
                                <svg></svg>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Net Commission for {{$adm}} (Rp.)</b></div>
                        <div class="collapsible-body">
                            <div id="commss">
                                <svg></svg>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m4 l4">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Net Commission for {{$tag}} type A & B (Rp.)</b></div>
                        <div class="collapsible-body">
                            <div id="comab">
                                <svg></svg>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Monthly Sales</b></div>
                        <div class="collapsible-body">
                            <div id="monthlysale">
                                <svg></svg>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Daily Sales</b></div>
                        <div class="collapsible-body">
                            <div id="dailysale">
                                <svg></svg>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            
        </div>
        <div class="row">
            <div class="col s12 m6 l6">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Individual {{$tag}} Type A Sales</b></div>
                        <div class="collapsible-body">
                            <canvas id="ssasalechart" width="400" ></canvas>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m6 l6">
                <ul class="collapsible" data-collapsible="accordion">
                    <li class="active">
                        <div class="collapsible-header active"><b>Individual {{$tag}} Type B Sales</b></div>
                        <div class="collapsible-body">
                            <canvas id="ssbsalechart" width="400" ></canvas>
                        </div>
                    </li>
                </ul>
            </div>
            
        </div>
        
    </div>

@endsection
