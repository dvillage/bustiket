</section>

<footer align="Center">&copy; 2017 <strong>BusTiket</strong>. All rights reserved. <a href="#"></a></footer>

<!-- jQuery RAF (improved animation performance) -->
<script type="text/javascript" src="{{URL::to('assets/js/jquery.requestAnimationFrame.min.js')}}"></script>

<!-- nanoScroller -->
<script type="text/javascript" src="{{URL::to('assets/js/jquery.nanoscroller.min.js')}}"></script>

<!-- Materialize -->
<script type="text/javascript" src="{{URL::to('assets/js/materialize.min.js')}}"></script>

<!-- d3 -->
<script type="text/javascript" src="{{URL::to('assets/js/d3.min.js')}}"></script>

<!-- nvd3 -->
<script type="text/javascript" src="{{URL::to('assets/js/nv.d3.min.js')}}"></script>
<!-- Sortable -->
<script type="text/javascript" src="{{URL::to('assets/js/Sortable.min.js')}}"></script>

<!-- Main -->
<script type="text/javascript" src="{{URL::to('assets/js/main.min.js')}}"></script>

<!-- Google Prettify -->
<script type="text/javascript" src="{{URL::to('assets/js/prettify.js')}}"></script>
  
<?php 

    if (isset($footer['js'])){
        for ($i = 0; $i < count($footer['js']); $i++) {
            if (strpos($footer['js'][$i], "https://") !== FALSE || strpos($footer['js'][$i], "http://") !== FALSE)
                echo '<script type="text/javascript" src="' . $footer['js'][$i] . '"></script>';
            else
                echo '<script type="text/javascript" src="' . \URL::to('assets/js/' . $footer['js'][$i]) . '"></script>';
        }
    }

?>
@yield('footer')
</body>

</html>