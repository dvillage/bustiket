@extends('layouts.admin')

@section('content')
<?php // dd($body['city']);
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    $(document).ready(function () {
        base_url = getBaseURL();
        var flag = 0;
        var flag1 = 0;
        var flag2 = 0;
        
        $(document).on('keyup','#seller',function(){
            var s = $(this).val();
            if(s.length < 2){
                flag = 1;
                return false;
            }
            if(flag == 1){
                flag = 0;
               var url = '{{URL::to("services/all-ss-name")}}';
               var data = {name:s};
               postAjax(url,data,function(res){
                   if(res.flag == 1){
                       $('#seller').autocomplete({
                        data: res.data,
                        limit: 10, // The max amount of results that can be shown at once. Default: Infinity.
                        onAutocomplete: function(val,key) {
                            console.log(val,key);
                            if(key > 0){
                                $('#ss_id').val(key);
                            }else if($('#seller').val() == ''){
                                $('#ss_id').val('');
                            }

                        },
                        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                    });
                   }
               });
            }
        });
        
        $(document).on('keyup','#busop',function(){
            var s = $(this).val();
            if(s.length < 2){
                flag = 1;
                return false;
            }
            if(flag == 1){
                flag = 0;
               var url = '{{URL::to("services/all-sp-name")}}';
               var data = {name:s};
               postAjax(url,data,function(res){
                   if(res.flag == 1){
                       $('#busop').autocomplete({
                        data: res.data,
                        limit: 10, // The max amount of results that can be shown at once. Default: Infinity.
                        onAutocomplete: function(val,key) {
                            console.log(val,key);
                            if(key > 0){
                                $('#sp_id').val(key);
                            }else if($('#busop').val() == ''){
                                $('#sp_id').val('');
                            }

                        },
                        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                    });
                   }
               });
            }
        });
    });

//        $('.autocomplete').autocomplete({
//            data: <?php // echo $body['city']; ?>,
//            limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
//            onAutocomplete: function(val) {
//              console.log(val);
//            },
//            minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
//        });

    
    
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        var stype = 'ss';
        var sid = '{{$body["uid"]}}';
        filter = {
            ftype : stype
        };
        if(sid != ''){
            filter['id'] = sid;
        }
        $('#cntrlbtn').css('display','none');

//        getData('{{$lt}}/commlist-filter',crnt,len,type,'','');
        filterData('{{$lt}}/commlist-filter',len,filter);
    };
    
    function filterBy(c='',len='',elem=''){
        var crnt = $('#crnt').val();
        var len = len || $('#len').val();
        var filter = {
            'month'   :$('#month').val(),
            'fromdate':$('#fromdate').val(),
            'todate'  :$('#todate').val(),
            'utype'   :$('#u_type').val(),
            'busop'   :$('#sp_id').val(),
            'any'     :$('#any').val(),
            'crnt'     :crnt,
            'opr'     :elem.id || '',
            'len'     :len,
            'ftype'    :'ss',
            'id'        :'{{$body["uid"]}}'
        };
        if(c == 'DL'){
            filter.download = 1;
            filter.by = 'ss';
            var durl = '{{URL::to("services/download-csv")}}';
            downloadCSV(durl,filter,'ss');
            return;
        }
//        console.log(c);
//        console.log(len);
//        console.log(elem);
//        if(c === 'w'){
//            filterDataWith('{{$lt}}/commlist-filter',elem)
//        }
//        else{
            return filterData('{{$lt}}/commlist-filter',len,filter);
//        }
    }
    
    
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Commission Management</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Commission Management</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        <div style='float: right;padding-right: 50px;'>
            <span class="h4"><u><b>Seat Seller :</b></u>&nbsp;&nbsp;&nbsp;{{$body['name']}}&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<u><b>Balance :</b></u>&nbsp;&nbsp;&nbsp;{{$body['balance']}}</span>
        </div>
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="fromdate" name="fromdate" type="text" class="pikaday" class="validate" onchange="return filterBy();">
                        <label for="input_text">Start Booking Date</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="todate" name="todate" class="pikaday" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">End Booking Date</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
<!--                        <input id="to_date" class="pikaday" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">By Monthly</label>-->
                        <select id="month" name="month" onchange="return filterBy();">
                            <option value="" selected>Choose Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <label>By Month</label>
                    </div>

                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
<!--                        <input id="by_month" class="autocomplete" name="filter[fromcity]" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">User Type</label>-->
                        <select id="u_type" name="u_type" onchange="return filterBy();">
                            <option value="">All</option>
                            <option value="5">Seat Seller</option>
                            <option value="6">Seat Seller - A</option>
                            <option value="7">Seat Seller - B</option>
                        </select>
                        <label>User Type</label>
                    </div>

                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="busop" class="autocomplete" name="busop" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">Bus Operator Name</label>
                    </div>
                    
                    <br>
                    <div class="input-field" style='width:50%;display:inline-block;'>
                        <input type="hidden" id="ss_id" name="ss_id" value="">
                        <input type="hidden" id="sp_id" name="sp_id" value="">
                        <!--<a class="btn" style="float:right;width: 80px;" onclick="return filterData('admin/commlist-filter','',$('[name = \"filter\"]'));"><i class="fa fa-search"></i></a>-->
                        
                        <a class="btn" style="float:right;width: 80px;" onclick="return filterBy();"><i class="fa fa-search"></i></a>
                    </div>
                    
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col s12 m10 l9"></div>
        <div class="col s12 m2 l3" style="text-align: right">
            <a class="btn btn-small blue " onclick="return filterBy('DL');" style="width: 140px;height: 23px;padding-right: 10px;"><i class="mdi-file-cloud-download right"></i>  Download CSV</a>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--<h3>No Data Found !!</h3>-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                        </table>
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
<!--                        <a class="btn" id="first" onclick="filterDataWith('admin/commlist-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/commlist-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/commlist-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/commlist-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/commlist-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/commlist-filter',this.value);">-->
                        <a class="btn" id="first" onclick="filterBy('w','',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterBy('w','',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterBy();">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterBy('w','',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterBy('w','',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterBy('',this.value);">
                            <option value="10">10</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal" style="width: 25%;">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
       
    </div>

@endsection