@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp': (config('constant.LOGGER') == 'SS'?'ss':'admin' );
?>
 <!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Profile</h1>
            </div>
            
        </div>

    </div>
 
    <!-- /Breadcrumb -->
    <div class="row" >
        <div class="col l3 m12"></div>
        <div class="col l6 m12">
            <div class="card">
                <div class="title">
                    <h4>Profile</h4>
                </div>
            <div class="content">
                <!-- Name -->
                <form id="profile" method="POST" action="{{URL::to($lt.'/change-admin-password')}}">

                    <?php if(isset($body['msg']) && $body['msg']!=""){ ?>   
                        <div class="alert pb-10 mb-15">
                            <strong><?php echo $body['msg']; ?></strong>
                        </div>   
                    <?php } ?>

                    <div class="input-field">
                      <input id="name" name="name" type="text" value="<?php echo isset($body['name'])? $body['name']: "";?>" required>
                      <label for="name"> {{config('constant.LOGGER') == 'SP'? 'First ' : ''}} Name</label>
                    </div>
                    <?php
                    if(config('constant.LOGGER') == 'SP'){
                        ?>
                    <div class="input-field">
                      <input id="lname" name="lname" type="text" value="<?php echo isset($body['lname'])? $body['lname']: "";?>" required>
                      <label for="name">Last Name</label>
                    </div>
                    <?php
                    }
                    ?>
                    <!-- /Name -->

                    <!-- Email -->
                    <div class="input-field">
                        <input id="email" name="email" type="email" value="<?php echo isset($body['email'])? $body['email']: "";?>" readonly="">
                      <label for="email">Email</label>
                    </div>
                    <!-- /Email -->

                    <!-- Email -->
                    <div class="input-field">
                        <input id="old_password" name="old_password" type="password" required>
                      <label for="old_password">Old Password</label>
                    </div>
                    <!-- /Email -->

                    <!-- Email -->
                    <div class="input-field">
                        <input id="new_password" name="new_password" type="password" required>
                      <label for="new_password">New Password</label>
                    </div>
                    <!-- /Email -->

                    <!-- Email -->
                    <div class="input-field">
                      <input id="confirm_password" name="confirm_password" type="password" required>
                      <label for="confirm-password">Confirm Password</label>
                      <input type="hidden" id='_token' name='_token' value='{{ csrf_token()}}'>
                    </div>
                    <!-- /Email -->

                    <!-- Validation Button -->
                    <div class="row">
                      <div class="col s12">
                        <button class="btn">Update</button>
                      </div>
                    </div>
                    <!-- /Validation Button -->

                </form>   

            </div>
        </div>



      </div>
        <div class="col l3 m12"></div>

    </div>




@endsection

