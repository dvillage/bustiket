
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
//dd($data);
$result = $data['result'];
//dd($result);
?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="text-align: center;">Booking ID</th>
            <th style="text-align: center;">BT ID</th>
            <th style="text-align: center;">Provider Name</th>
            <th style="text-align: center;">Booker Name</th>
            <th style="text-align: center;">Travel Date</th>
            <th style="text-align: center;">Booking Date & Time</th>
            <th style="text-align: center;">From - To</th>
            <th style="text-align: center;">Fare</th>
            <th style="text-align: center;">No. of Seats</th>
            <th style="text-align: center;">User Type</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Cancel</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($result as $r) { 
            $u = '';
            $eu = '';
            $dot = '';
            if($r['booking_system'] > 0){
                
                $u = '<u>';
                $eu = '</u>';
                if($r['booking_system'] == 2){
                    $u = '<u style="text-decoration: underline;text-decoration-style:dashed;">';
                }
                if($r['roda_pay'] == 0){
                    $usr = 'roda';
                    if($r['booking_system'] == 2){
                        $usr = 'lorena';
                    }
                    $dot = '<span style="color:red;" title="'.$usr.' payment failed of this ticket."> * </span>';
                }
                
            }
            ?>
        <tr>
            <td style="text-align: center;"><a href="{{URL::to($lt.'/ticket-details')}}/{{$r['booking_id']}}"><?php echo $dot. $u. $r['booking_id'] .$eu ?></a></td>
            <td style="text-align: center;">{{ $r['bt_id'] }}</td>
            <td style="text-align: center;">{{ $r['sp']['first_name'] .' '. $r['sp']['last_name'] }}</td>
            <td style="text-align: center;">{{ $r['booker_name'] }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['journey_date'])) }}</td>
            <td style="text-align: center;">{{ $r['created_at'] }}</td>
            <td style="text-align: center;">{{ $r['from_terminal']['loc_citi']['name'].' - '.$r['to_terminal']['loc_citi']['name'] }}</td>
            <td style="text-align: right;">{{ $r['total_amount'] }}</td>
            <td style="text-align: center;">{{ $r['nos'] }}</td>
            <td style="text-align: center;">
            <?php 
                switch ($r['book_by']) {
                    case 0:
                        echo 'User';
                        break;
                    case 1:
                        echo 'Admin';
                        break;
                    case 2:
                        echo 'Service Provider';
                        break;
                    case 3:
                        echo 'Service Provider A';
                        break;
                    case 4:
                        echo 'Service Provider B';
                        break;
                    case 5:
                        echo 'Seat Seller';
                        break;
                    case 6:
                        echo 'Seat Seller A';
                        break;
                    case 7:
                        echo 'Seat Seller B';
                        break;
                }
            ?>
            </td>
            
                <?php 
                    switch( $r['status'] ){
                        case 0:
                            echo '<td style="text-align: center;">';
//                            echo '<span style="color:yellow">Pending</span>';
                            if($lt == 'admin')
                                echo '<a style="width: 100px;color:blue;cursor: pointer;" onclick="mdlApprove(\''.$r['booking_id'].'\');">Bank Transfer</a>';
                            else
                                echo '<span style="color:blue">Bank Transfer</span>';
                            echo '</td>';
                            echo '<td></td>';
                            break;
                        case 1:
                            echo '<td style="text-align: center;">';
                            echo '<span style="color:green">Travel</span>';
                            echo '</td>';
                            echo '<td>
                                        <a class="modal-trigger" id="'.$r['id'].'" style="cursor: pointer;" onclick="delTicket(\''.$r['booking_id'].'\');">Cancel</a>
                                    </td>';
                            break;
                        case 2:
                            echo '<td style="text-align: center;">';
//                            echo '<a style="width: 100px;color:blue;cursor: pointer;" onclick="tktApprove(\''.$r['booking_id'].'\');">Bank Transfer</a>';
                            if($lt == 'admin')
                                echo '<a style="width: 100px;color:blue;cursor: pointer;" onclick="mdlApprove(\''.$r['booking_id'].'\');">Bank Transfer</a>';
                            else
                                echo '<span style="color:blue">Bank Transfer</span>';
                            echo '</td>';
                            echo '<td></td>';
                            break;
                        case 3:
                            echo '<td style="text-align: center;">';
                            echo '<span style="color:red">Cancelled</span>';
                            echo '</td>';
                            if($r['booking_seats'][0]['status'] == 2){
                                echo '<td>
                                            <a class="modal-trigger" id="'.$r['id'].'" style="cursor: pointer;" onclick="refTicket(\''.$r["booking_id"] .'\');">Refund</a>
                                        </td>';
                            }else{
                                echo '<td>';
                                echo '<span style="color:green">Refunded</span>';
                                echo '</td>';
                            }
                            break;
                        
                    }
                
                ?>
            
        </tr>
        <?php } ?>
    </tbody>
</table>

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />