
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
//dd($data);
$result = $data['result'];
//dd($result);
if(count($result) > 0 ){ ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="text-align: center;">Booking ID</th>
            <th style="text-align: center;">Bus Operator</th>
            <th style="text-align: center;">Tanggal Berangkat</th>
            <th style="text-align: center;">Tanggal Pesan</th>
            <th style="text-align: center;">Rute Pejalanan</th>
            <th style="text-align: center;">User Type</th>
            <th style="text-align: center;">Nama Penumpang</th>
            <th style="text-align: center;">Seller Name</th>
            <th style="text-align: center;">Tipe Komisi</th>
            <th style="text-align: center;">No. Ticket</th>
            <th style="text-align: center;">Harga Tiket</th>
            <th style="text-align: center;">Komisi ke Admin</th>
            <th style="text-align: center;">Komisi ke Service Provider A/B</th>
            <th style="text-align: center;">Pendapatan Bersih</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $t_ttl_amount = 0;
            $t_from_sp = 0;
            $t_net_price = 0;
            $t_to_ss = 0;
            $t_hf = 0;
            $t_net_income = 0;
            $total_ticket = 0;
//        dd($result);
            foreach($result as $r) { 
//                echo '<pre>';
//                print_r($r['ss_comm']);
//                echo '</pre>';
                $to_ss = 0;
                
                $commtoAB = 0;
                switch ($r['book_by']) {
                    case 2:
                        $netprice = 0;
                        $comFromAdm = 0;
                        if(count($r['admin_comm']) > 0){
                            $netprice = $r['total_amount'] - $r['admin_comm']['com_amount'];
                            $comFromAdm = $r['admin_comm']['com_amount'];
                        }
                        $utype = 'Service Provider';
                        $sname = $r['sp']['first_name'].' '.$r['sp']['last_name'];
                        $comtype = '';
                        $commtoAB = 0;
                        break;
                    case 3:
                        $netprice = 0;
                        $comFromAdm = 0;
                        $commtoAB = 0;
                        if(count($r['admin_comm']) > 0){
                            $netprice = $r['total_amount'] - $r['admin_comm']['com_amount'];
                            $comFromAdm = $r['admin_comm']['com_amount'];
                            $commtoAB = $r['sp_comm']['com_amount'];
                        }
                        $utype =  'Servcie Provider A';
                        $sname = '';
                        $comtype = '';
                        if(count($r['sp_comm'])>0){
                            $netprice = $netprice - $r['sp_comm']['com_amount'];
                            $sname = '<a href="'.URL::to($lt.'/commission-management/spa/'.$r['sp_comm']['sp']['id']).'" title="get commission">'.$r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'].'</a>';
                            $comtype = $r['sp_comm']['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                        }
                        break;
                    case 4:
                        $netprice = 0;
                        $comFromAdm = 0;
                        $commtoAB = 0;
                        if(count($r['admin_comm']) > 0){
                            $netprice = $r['total_amount'] - $r['admin_comm']['com_amount'];
                            $comFromAdm = $r['admin_comm']['com_amount'];
                            $commtoAB = $r['sp_comm']['com_amount'];
                        }
                        $utype =  'Servcie Provider B';
                        $sname = '';
                        $comtype = '';
                        if(count($r['sp_comm'])>0){
                            $netprice = $netprice - $r['sp_comm']['com_amount'];
                            $sname = '<a href="'.URL::to($lt.'/commission-management/spb/'.$r['sp_comm']['sp']['id']).'" title="get commission">'.$r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'].'</a>';
                            $comtype = $r['sp_comm']['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                        }
                        break;
                    default :
                        $netprice = 0;
                        $comFromAdm = 0;
                        if(count($r['admin_comm']) > 0){
                            $netprice = $r['total_amount'] - $r['admin_comm']['com_amount'];
                            $comFromAdm = $r['admin_comm']['com_amount'];
                        }
                        $utype = $r['book_by'] == 0 ? 'User' : ($r['book_by'] == 5 ? 'Seat Seller' : ($r['book_by'] == 6 ? 'Seat Seller A' : ($r['book_by'] == 7 ? 'Seat Seller B' : 'Admin'))) ;
                        $sname = $r['sp']['first_name'].' '.$r['sp']['last_name'];
                        $comtype = '';
                        $commtoAB = 0;
                        break;
                }
                $total_ticket = $total_ticket + $r['nos'];
        ?>
        <tr>
            <td style="text-align: center;"><a href="{{URL::to($lt.'/ticket-details')}}/{{$r['booking_id']}}">{{$r['booking_id']}}</a></td>
            <td style="text-align: center;">{{ $r['sp']['first_name'] .' '. $r['sp']['last_name'] }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['pickup_date'])) }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['dropping_date'])) }}</td>
            <td style="text-align: center;">{{ $r['from_city'].' - '. $r['to_city']}}</td>
            <td style="text-align: center;">{{$utype}}</td>
            <td style="text-align: center;">{{ $r['booker_name'] }}</td>
            <td style="text-align: center;"><?php echo $sname; ?></td>
            <td style="text-align: center;">{{$comtype}}</td>
            <td style="text-align: center;">{{ $r['nos'] }}</td>
            <td style="text-align: right;">{{ round($r['total_amount']) }}</td>
            <td style="text-align: right;">{{ round($comFromAdm) }}</td>
            <td style="text-align: right;">{{ round($commtoAB) }}</td>
            
                <?php 
                    
                    $netincome = $r['total_amount'] - ($comFromAdm + $commtoAB);
                ?>
            
            <td style="text-align: right;">{{ round($netincome) }}</td>
            
            <?php
                $t_ttl_amount += $r['total_amount'] ;
                $t_from_sp += $comFromAdm;
                $t_net_price += $netprice;
                $t_to_ss += $to_ss;
                $t_hf += $commtoAB;
                $t_net_income += $netincome;
            ?>
            
        </tr>
        <?php } ?>
        <tr>
            <td colspan="9" style="text-align: center;"><h4 style="font-weight: bold;">Total</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ $total_ticket }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_ttl_amount,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_from_sp,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_hf,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_net_income,3) }}</h4></td>
            
        </tr>
    </tbody>
</table>

<?php }else{ ?>
    <h2 style='color:red;'>No Data Found</h2>
<?php } ?>

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
<style>
    .get_child_comm{
        cursor: pointer;
        color: blue;
    }
    
</style>