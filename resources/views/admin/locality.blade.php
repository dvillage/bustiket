@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');
        
        if(type == 'Terminal'){
            $('#vtable').parent().parent().parent().parent().removeClass('l8');
        }
        
        getData('admin/locality-filter',crnt,len,type,'','');
        
    };
    
    function delLoc(id){
        var type = $('#type').html().toLowerCase();
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete '+type+' ??';
        var onyes ='delData';
        var param='\'admin/del-locality-'+type+'\','+id+' , \'admin/locality-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addLoc(){
        var type = $('#type').html();
        if(type=='Province'){
            $('#add-pname').val('');
        }
        else if(type=='City'){
            $('#add-cname').val('');
            $('#add-cpro').val(0);
        }
        else if(type=='District'){
            $('#add-dname').val('');
            $('#add-dpro').val(0);
            $('#add-dcity').val(0);
        }
        else if(type == 'Terminal'){
            $('#add-tname').val('');
            $('#add-tpro').val(0);
            $('#add-tdist').val('');
            $('#add-tcity').val(0);
            $('#add-tadd').val('');
            $('#add-tzip').val('');
            $('#add-ta_name').val('');
            $('#add-ta_phone').val('');
            $('#add-ta_email').val('');
            $('#add-googlemap').val('');
        }
        
        $('#modal-'+type+'').find('#mdlyes').attr('onclick','addLocality()');
        $('#modal-'+type+'').find('#mdlyes').html('Add');
        $('#modal-'+type+'').openModal();
    }
    function upLoc(id){
        var type = $('#type').html();
        if(type=='Province'){
            $('#add-pname').val($('#pname-'+id).html());
        }
        else if(type=='City'){
            $('#add-cname').val($('#cname-'+id).html());
            $('#add-cpro').val($('#cpro-'+id).attr('name'));
            var p = $('#cpro-'+id).attr('name');
        }
        else if(type=='District'){
            $('#add-dname').val($('#dname-'+id).html());
            $('#add-dpro').val($('#dpro-'+id).attr('name'));
            $('#add-dcity').val($('#dcity-'+id).attr('name'));
            
            var p = $('#dpro-'+id).attr('name');
            var d = $('#dcity-'+id).attr('name');
            filterCity(p,d);
        }
        else if(type == 'Terminal'){
            $('#add-tname').val($('#tname-'+id).html());
            $('#add-tpro').val($('#tpro-'+id).attr('name'));
            if($('#tpro-'+id).attr('name') != '')
                $('#add-tpro').material_select();
            
            $('#add-tcity').val($('#tcity-'+id).attr('name'));
            $('#add-tdist').val($('#tdist-'+id).html());
            $('#add-tdist_id').val($('#tdist-'+id).attr('name'));
            var p = $('#tpro-'+id).attr('name');
            var d = $('#tdist-'+id).attr('name');
            var c = $('#tcity-'+id).attr('name');
            filterCity(p,c);
            filterDist(c,d);
            
            $('#add-tadd').val($('#tadd-'+id).html());
            $('#add-tzip').val($('#tzip-'+id).html());
            $('#add-ta_name').val($('#ta_name-'+id).html());
            $('#add-ta_phone').val($('#ta_phone-'+id).html());
            $('#add-ta_email').val($('#ta_email-'+id).html());
            $('#add-googlemap').val($('#googlemap-'+id).html());
            
        }
        $('#modal-'+type+'').find('#mdlyes').attr('onclick','editLocality('+id+')')
        $('#modal-'+type+'').find('#mdlyes').html('Update');
        $('#modal-'+type+'').openModal();
    }
    
    function addLocality(){
        var url="{{URL::to('admin/add-locality')}}";
        var data={};
        var type = $('#type').html();
        if(type=='Province'){
            var name=$('#add-pname').val();
            if(name == ''){
                Materialize.toast('Province name should not be blank .', 2000,'rounded');
                return false;
            }
            data={type:type,name:name};
        }
        else if(type=='District'){
            var name=$('#add-dname').val();
            var pro=$('#add-dpro :selected').val();
            var city=$('#add-dcity :selected').val();
            if(name == ''){
                Materialize.toast('District name should not be blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please Select Province .', 2000,'rounded');
                return false;
            }
            if(city == 0){
                Materialize.toast('Please select City .', 2000,'rounded');
                return false;
            }
            data={type:type,name:name,pro:pro,city:city};
        }
        else if(type=='City'){
            var name=$('#add-cname').val();
            var dist=$('#add-cdist :selected').val();
            var pro=$('#add-cpro :selected').val();
            if(name == ''){
                Materialize.toast('City name should not be blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please select Province .', 2000,'rounded');
                return false;
            }
            data={type:type,name:name,pro:pro};
        }
        else if(type == 'Terminal'){
            var name = $('#add-tname').val();
            var pro = $('#add-tpro :selected').val();
            var city = $('#add-tcity').val();
            var dist_name = $('#add-tdist').val();
            var dist = $('#add-tdist_id').val();
            var address = $('#add-tadd').val();
            var zipcode = $('#add-tzip').val();
            var agent_name = $('#add-ta_name').val();
            var agent_phone = $('#add-ta_phone').val();
            var agent_email = $('#add-ta_email').val();
            var googlemap = $('#add-googlemap').val();
            
            if(name == ''){
                Materialize.toast('Terminal Name Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please Select Province .', 2000,'rounded');
                return false;
            }
            if(dist == ''){
                Materialize.toast('Please Select District .', 2000,'rounded');
                return false;
            }
            if(city == 0){
                Materialize.toast('Please Select City .', 2000,'rounded');
                return false;
            }
            if(address == ''){
                Materialize.toast('Address Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(zipcode == ''){
                Materialize.toast('Zipcode Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_name == ''){
                Materialize.toast('Agent Name Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_phone == ''){
                Materialize.toast('Agent Phone No. Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_email == ''){
                Materialize.toast('Agent Email Address Should not be Blank .', 2000,'rounded');
                return false;
            }
            
            data={  type:type,name:name,pro:pro,
                    dist:dist,city:city,distname:dist_name,
                    address:address,zipcode:zipcode,agentname:agent_name,
                    agentphone:agent_phone,agentemail:agent_email,googlemap:googlemap,
                    
                };
        }
        
        postAjax(url,data,function(res){
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('Your Data Saved Successfully !!', 2000,'rounded');
                $('#modal-'+type+'').closeModal();
                filterData('admin/locality-filter');
            }
        });
    }
    function editLocality(id){
        var url="{{URL::to('admin/edit-locality')}}";
        var data={};
        var type = $('#type').html();
        if(type=='Province'){
            var name=$('#add-pname').val();
            if(name == ''){
                Materialize.toast('District Name should not be Blank .', 2000,'rounded');
                return false;
            }
            data={type:type,id:id,name:name};
        }
        else if(type=='District'){
            var name=$('#add-dname').val();
            var pro=$('#add-dpro :selected').val();
            var city=$('#add-dcity :selected').val();
            if(name == ''){
                Materialize.toast('District name should not be blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please Select Province .', 2000,'rounded');
                return false;
            }
            if(city == 0){
                Materialize.toast('Please Select City .', 2000,'rounded');
                return false;
            }
            data={type:type,id:id,name:name,pro:pro,city:city};
        }
        else if(type=='City'){
            var name=$('#add-cname').val();
            var pro=$('#add-cpro :selected').val();
            if(name == ''){
                Materialize.toast('City name should not be Blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please Select Province .', 2000,'rounded');
                return false;
            }
            
            data={type:type,id:id,name:name,pro:pro};
        }
        else if(type == 'Terminal'){
            var name = $('#add-tname').val();
            var pro = $('#add-tpro :selected').val();
            var city = $('#add-tcity').val();
            var dist_name = $('#add-tdist').val();
            var dist = $('#add-tdist_id').val();
            var address = $('#add-tadd').val();
            var zipcode = $('#add-tzip').val();
            var agent_name = $('#add-ta_name').val();
            var agent_phone = $('#add-ta_phone').val();
            var agent_email = $('#add-ta_email').val();
            var googlemap = $('#add-googlemap').val();
            
            if(name == ''){
                Materialize.toast('Terminal name should not be blank .', 2000,'rounded');
                return false;
            }
            if(pro == 0){
                Materialize.toast('Please Select Province .', 2000,'rounded');
                return false;
            }
            if(dist == ''){
                Materialize.toast('Please Select District .', 2000,'rounded');
                return false;
            }
            if(city == 0){
                Materialize.toast('Please Select City .', 2000,'rounded');
                return false;
            }
            if(address == ''){
                Materialize.toast('Address Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(zipcode == ''){
                Materialize.toast('Zipcode Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_name == ''){
                Materialize.toast('Agent Name Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_phone == ''){
                Materialize.toast('Agent Phone No. Should not be Blank .', 2000,'rounded');
                return false;
            }
            if(agent_email == ''){
                Materialize.toast('Agent Email Address Should not be Blank .', 2000,'rounded');
                return false;
            }
            
            data={  type:type,id:id,name:name,pro:pro,
                    dist:dist,city:city,distname:dist_name,
                    address:address,zipcode:zipcode,agentname:agent_name,
                    agentphone:agent_phone,agentemail:agent_email,googlemap:googlemap,
                    
                };
        }
        
        postAjax(url,data,function(res){
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded')
                return false;
            }
            else{
                Materialize.toast('Your Data Saved Successfully !!', 2000,'rounded');
                $('#modal-'+type+'').closeModal();
                filterData('admin/locality-filter');
            }
        });
    }
    function filterDist(cityid,id){
        var type = $('#type').html();
        var url="{{URL::to('admin/filter-dist')}}";
        var data={cityid:cityid};
        postAjax(url,data,function(res){
            if(res.flag==0){
                Materialize.toast('No Change Occured.', 2000,'rounded')
                return false;
            }
            else{
                $('#add-tdist').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        $('#add-tdist_id').val(key); 
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }
        });
    }
    function filterCity(proid,id){
        
        var type = $('#type').html();
        var url="{{URL::to('admin/filter-city')}}";
        var data={proid:proid};
        postAjax(url,data,function(res){
            if(res.flag==0){
                Materialize.toast('No Change Occured.', 2000,'rounded')
                return false;
            }
            else{
                if(type == 'District'){
                    $('#add-dcity').empty().append('<option value="0">Select City</option>');
//                    if(res.data.length > 0){
                        for(var x in res.data){
                            var s = '';
                            if(typeof id !== 'undefined' && id == res.data[x].id){
                                s = 'selected';
                            }
                            $('#add-dcity').append('<option value="' + res.data[x].id + '" '+s+'>' + res.data[x].name + '</option>')
                        }
                        $('#add-dcity').material_select();
//                    }
                }
                else if(type == 'Terminal'){
                    $('#add-tcity').empty().append('<option value="0">Select District</option>');
//                    if(res.data.length > 0){
                        for(var x in res.data){
                            var s = '';
                            if(typeof id !== 'undefined' && id == res.data[x].id){
                                s = 'selected';
                            }
                            $('#add-tcity').append('<option value="' + res.data[x].id + '" '+s+'>' + res.data[x].name + '</option>')
                        }
                        $('#add-tcity').material_select();
//                    }
                }
            }
        });
    }
    $(document).on('click','#search',function(){

        filterData('admin/locality-filter','','');
    });
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1><?php echo $body['type']!=""?$body['type']:'Locality'; ?></h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Locality</a><i class='fa fa-angle-right'></i>
                    </li>
                    <li><a ><?php echo $body['type']!=""?$body['type']:''; ?></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
         <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter by Name</label>
                    </div>
                    
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l2 m12 s12"></div>
        <div class="col l8 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Locality : <span id="type"><?php echo $body['type']!=""?$body['type']:''; ?></span></h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/locality-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/locality-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/locality-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/locality-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/locality-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/locality-filter',this.value);">
<!--                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>-->
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l2 m12 s12"></div>
        
        <!-- Modal Structure -->
        <form id="add_dist" method="post" action="" data-parsley-validate >
        <div id="modal-Province" class="modal bust-modal" style='width:35%;font-size:100%;' >
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>Province Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-pname" name="add-pname" type="text" placeholder="Province Name" required="">
                        <label for="add-pname">Province Name</label>
                    </div>
                </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action waves-effect waves-green btn-flat ">Add</a>
          </div>
        </div>
        </form>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="modal-District" class="modal bust-modal" style='width:35%;font-size:100%;'>
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>District Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-dname" name="add-dname" type="text" placeholder="District Name"required>
                        <label for="add-dname">District Name</label>
                    </div>
                    <div class="input-field">
                        <select id='add-dpro' name='add-dpro' class="browser-default" onchange='filterCity(this.value);' required>
                            <option value="0" selected disabled>Select Province</option>
                            <?php foreach($body['plist'] as $x){?>
                            
                                <option value="{{$x['id']}}">{{$x['name']}}</option>

                            <?php } ?>
                            
                        </select>
                    </div>
                    
                    <div class="input-field">
                        <select id='add-dcity' name='add-dcity' class="browser-default" required>
                            <option value="0" selected disabled>Select City</option>
                        </select>
                        
                    </div>
                    
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action  waves-effect waves-green btn-flat ">Add</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="modal-City" class="modal bust-modal" style='width:35%;font-size:100%;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title">
                    <h5 id='mdltitle'>City Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-cname" name="add-cname" type="text" placeholder="City Name"required>
                        <label for="add-cname">City Name</label>
                    </div>
                    
                    <div class="input-field">
                        <select id='add-cpro' name='add-cpro' class="browser-default" required>
                            <option value="0" selected disabled>Select Province</option>
                            <?php foreach($body['plist'] as $x){?>
                            
                                <option value="{{ $x['id']}}">{{$x['name']}}</option>

                            <?php } ?>
                            
                        </select>
                        
                    </div>
                    
<!--                    <div class="input-field">
                        <select id='add-cdist' name='add-cdist' class="browser-default" required>
                            <option value="0" selected disabled>Select District</option>
                        </select>
                        
                    </div>-->
                    
                </div>
            <!--</div>-->
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action  waves-effect waves-green btn-flat ">Add</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="modal-Terminal" class="modal bust-modal" style='width:35%;font-size:100%;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title">
                    <h5 id='mdltitle'>Terminal Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-tname" name="add-tname" type="text" placeholder="Terminal Name"required>
                        <label for="add-tname">Terminal Name</label>
                    </div>
                    
                    <div class="input-field">
                        <select id='add-tpro' name='add-tpro' class="" onchange='filterCity(this.value);' required>
                            <option value="0" selected disabled>Select Province</option>
                            <?php foreach($body['plist'] as $x){?>
                            
                                <option value="{{ $x['id']}}">{{$x['name']}}</option>

                            <?php } ?>
                            
                        </select>
                        <label for="add-tpro">Select Province</label>
                        
                    </div>
                    
                    <div class="input-field">
<!--                        <select id='add-tdist' name='add-tdist' class="" onchange='filterCity(this.value);' required>
                            <option value="0" selected disabled>Select District</option>
                        </select>-->
                        <select id='add-tcity' name='add-tcity' class="" onchange='filterDist(this.value);' required>
                            <option value="0" selected disabled>Select City</option>
                        </select>
                        <label for="add-tcity">Select City</label>
                        
                    </div>
                    
                    <div class="input-field">
<!--                        <select id='add-tcity' name='add-tcity' class="" required>
                            <option value="0" selected disabled>Select City</option>
                        </select>-->
                        <input id="add-tdist" class="autocomplete" name="add-tdist" type="text" class="validate" placeholder="Select District">
                        <input id="add-tdist_id" name="add-tdist_id" type="hidden" >
                        <label for="add-tdist">Select District</label>
                        
                    </div>
                    
                    <div class="input-field">
                        <input id="add-tadd" name="add-tadd" type="text" placeholder="Terminal Address"required>
                        <label for="add-tadd">Terminal Address</label>
                    </div>
                    
                    <div class="input-field">
                        <input id="add-tzip" name="add-tzip" type="text" placeholder="Zip Code"required>
                        <label for="add-tzip">Zip Code</label>
                    </div>
                    <div class="input-field">
                        <input id="add-ta_name" name="add-ta_name" type="text" placeholder="Agent Name"required>
                        <label for="add-ta_name">Agent Name</label>
                    </div>
                    <div class="input-field">
                        <input id="add-ta_phone" name="add-ta_phone" type="text" placeholder="Agent Phone No."required>
                        <label for="add-ta_phone">Agent Phone No.</label>
                    </div>
                    <div class="input-field">
                        <input id="add-ta_email" name="add-ta_email" type="text" placeholder="Agent Email"required>
                        <label for="add-ta_email">Agent Email</label>
                    </div>
                    <div class="input-field">
                        <textarea class="materialize-textarea" id="add-googlemap" name="add-googlemap" placeholder="Google Map Address"required></textarea>
                        <label for="add-googlemap">Google Map Address</label>
                    </div>
                    
                </div>
            <!--</div>-->
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action  waves-effect waves-green btn-flat ">Add</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <?php $t=$body['type']!=''?$body['type']:''; ?>
            <a class="btn-floating btn-large red tooltipped" onclick="addLoc();"data-position="top" data-delay="50" data-tooltip="Add Locality">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>


@endsection