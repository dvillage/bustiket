@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }

    
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');
        
        getData('admin/testimonial-filter',crnt,len,type,'','');
        
    };
    
    function deleteTestimonial(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete testimonial ??';
        var onyes ='delData';
        var param='\'admin/delete-testimonial\','+id+' , \'admin/testimonial-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addTestimonial(){
        
        $('#uname').val('').attr('placeholder','Enter Name');
        $('#email').val('').attr('placeholder','Enter Email Id');
        $('#image').attr('required',true);
        $('#modal-card').find('#save-banner').attr('onclick','addNewTestimonial()');
        $('#modal-card').find('#save-banner').html('Add');
        $('#modal-card').openModal();
        $('#card_details')[0].reset();
        
    }
    
    function updateTestimonial(id){
        
        $('#tid').val(id);
        $('#uname').val($('#uname-'+id).html().trim());
        $('#email').val($('#email-'+id).html().trim());
        $('#message').val($('#message-'+id).html().trim());
        

        $('#image').removeAttr('required');
        $('#modal-card').find('#save-banner').attr('onclick','updateNewTestimonial('+id+')');
        $('#modal-card').find('#save-banner').html('Update');
        $('#modal-card').openModal();

        

        var status = $('#statusg-'+id).data('st');
        var chk = $('#status').is(':checked');
        if(status == 1 && !chk){
            $('#status').trigger('click');
        }else if(status == 0 && chk){
            $('#status').trigger('click');
        }
    }
    
    function addNewTestimonial(){
        
        var url="{{URL::to('admin/add-testimonial')}}";
        $('#card_details').attr('action',url);
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
              
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/testimonial-filter');
            }
            location.reload();
        });
    }
    
    function updateNewTestimonial(id){
        
        var url="{{URL::to('admin/update-testimonial')}}";
        $('#card_details').attr('action',url);
        
        
        $('#card_details').ajaxForm(function(res) {
            
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/testimonial-filter');
            }
            location.reload();
        });
    }
   
    $(document).on('click','#search',function(){
        
        var keyword = $('#searchname').val();
        var url = 'admin/testimonial-filter';
        var data = {};
        if(keyword!= ''){
            data['keyword'] = keyword;
        }
    
        filterData('admin/testimonial-filter','',data);
    });
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Testimonial</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a>Testimonial</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter by Email</label>
                    </div>
                    
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Testimonial List</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/testimonial-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/testimonial-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/testimonial-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/testimonial-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/testimonial-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/testimonial-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="card_details" action="" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-card" class="modal bust-modal" style='width: 50%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>New Testimonial</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                <input type="hidden" name="tid" id="tid" value="" />
                    <div class="content">
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <input id="uname" name="uname" type="text" placeholder="" autocomplete="off" required>
                                    <label for="uname">Name</label>
                                    {{csrf_field()}}
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <input id="email" name="email" type="text" placeholder="" autocomplete="off" required>
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <label for="message">Feedback Message</label>
                                    <textarea id="message" cols="9" rows="2" name="message" class="materialize-textarea"  autocomplete="off" required></textarea>
          
                                </div>
                            </div>
                           
                            
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field">
                                    <div class="btn"  style="width:100px;">
                                        <span class="">Image</span>
                                        <input type="file" name="image[]" id="image" accept="image/*" >
                                        <!--<label for="bnr_img">Upload Banner Image</label>-->
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field">
                                    <div class="btn"  style="width:100px;">
                                        <span class="">Video</span>
                                        <input type="file" name="video[]" id="video" accept="video/*" >
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-field">
                        <p class="switch" style='padding: 10px 0 20px 0;'>
                            <label>
                                <span style='font-size: 20px;padding-bottom: 10px;'>Status :</span>
                                <input type="checkbox" id='status' name='status' value="1" />
                                <span class="lever" style="margin-bottom: 8px;"></span>
                            </label>
                        </p>
                    </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Testimonial ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="addTestimonial();"data-position="top" data-delay="50" data-tooltip="Add Favorite Route">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
@endsection