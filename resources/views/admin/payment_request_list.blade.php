@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/payment-request-filter',crnt,len,type,'','');
        
    };
    
    function delCourier(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete This request ??';
        var onyes ='delData';
        var param='\'{{$lt}}/delete-payment-request\','+id+' , \'{{$lt}}/payment-request-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addReq(){
        
        $('#add-name').val('');
        $('#modal-Req').find('#mdlyes').attr('onclick','addNewRequest()');
        $('#modal-Req').find('#mdlyes').html('Add');
        $('#modal-Req').openModal();
        $('#amount').val('');
        $('#acc_name').val('');
        $('#acc_number').val('');
        $('#request_payment').parsley().reset();
        var chk = $('#pay_bank').is(':checked');
        if(!chk){
            $('#pay_bank').trigger('click');
        }
    }
    
    $(document).on('change','#pay_bank',function(){
        var c = $(this).is(':checked');
        if(c){
            $('#if_bank').css({display:'block'});
            $('#acc_name').attr('required',true);
            $('#acc_number').attr('required',true);
        }else{
            $('#if_bank').css({display:'none'});
            $('#acc_name').removeAttr('required');
            $('#acc_number').removeAttr('required');
        }
    });
    
    function addNewRequest(){
        var url="{{URL::to($lt.'/new-payment-request')}}";
        $('#request_payment').attr('action',url);
        $('#request_payment').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-Req').closeModal();
                filterData('{{$lt}}/payment-request-filter');
            }
        });
    }
    $(document).on('click','#search',function(){
        var status = $('#status :selected').val();
        var data = {};
        if(status != ''){
            data['status'] = status;
        }
//        pagination(url,data,'vtable');
        
        
        filterData('{{$lt}}/payment-request-filter','',data);
    });
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Payment Management</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Payment Management</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <!--<input id="searchname" type="text" class="validate" onchange="return filterData('{{$lt}}/seat-seller-deposit');">-->
                        <input id="searchname" type="text" class="validate" onchange="">
                        <label for="input_text">Filter by account name or number</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="status">
                            <option value="">All</option>
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                            <option value="0">Pending</option>
                        </select>
                        <label for="input_text">Filter By Status</label>
                        
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a id="search" class="btn"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Payment Management</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/payment-request-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/payment-request-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/seat-seller-deposit');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/payment-request-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/payment-request-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('{{$lt}}/payment-request-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
         <!-- Modal Structure -->
        <div id="modal-Req" class="modal bust-modal" style='width:35%;font-size:100%;'>
            <form id="request_payment" method="post" action="" data-parsley-validate > 
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="id" value="{{config('constant.CURRENT_LOGIN_ID')}}" />
                
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title">
                    <h5 id='mdltitle'>Request Payment</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="amount" name="amount" type="number" required>
                        <label for="add-cname">Amount</label>
                    </div>
                    
                    <div class="input-field">
                        <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Pay by bank : </span>
                                    <input type="checkbox" id='pay_bank' name='pay_bank' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>
                                    
                                </label>
                            </p>
                    </div>
                    <div id="if_bank">
                        <div class="input-field">
                            <input id="acc_name" name="acc_name" type="text" required="">
                            <label for="add">Account name</label>
                        </div>
                        <div class="input-field">
                            <input id="acc_number" name="acc_number" type="text" required="">
                            <label for="add">Account number</label>
                        </div>
                    </div>
                </div>
            <!--</div>-->
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id='mdlyes' class="modal-action  waves-effect waves-green btn-flat ">Add</button>
          </div>
            </form>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="confirmApprove" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            
            <a class="btn-floating btn-large red tooltipped" onclick="addReq();"data-position="top" data-delay="50" data-tooltip="Request Payment">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        
    </div>
    
@endsection