
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
//dd($data);
$result = $data['result'];
//dd($result);
if(count($result) > 0 ){ ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="text-align: center;">Booking ID</th>
            <th style="text-align: center;">Bus Operator</th>
            <th style="text-align: center;">Tanggal Berangkat</th>
            <th style="text-align: center;">Tanggal Pesan</th>
            <th style="text-align: center;">Rute Pejalanan</th>
            <th style="text-align: center;">User Type</th>
            <th style="text-align: center;">Nama Penumpang</th>
            <th style="text-align: center;">Seller Name</th>
            <th style="text-align: center;">Tipe Komisi</th>
            <th style="text-align: center;">No. Ticket</th>
            <th style="text-align: center;">Harga Tiket</th>
            <th style="text-align: center;">Komisi dari Seat Seller</th>
            <th style="text-align: center;">Harga Net</th>
            <th style="text-align: center;">Handling Fee</th>
            <th style="text-align: center;">Pendapatan Bersih</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $t_ttl_amount = 0;
            $t_from_sp = 0;
            $t_net_price = 0;
            $t_to_ss = 0;
            $t_hf = 0;
            $t_net_income = 0;
            $total_ticket = 0;
//        dd($result);
            foreach($result as $r) { 
//                echo '<pre>';
//                print_r($r['ss_comm']);
//                echo '</pre>';
                $to_ss = 0;
                
                $hf = 0;
                $sname = $r['ss_comm'][0]['ss']['name'];
                $netprice = $r['total_amount'] - $r['ss_comm'][0]['com_amount'];
                $comtype = $r['ss_comm'][0]['comm_type'] == 'P' ? 'Percentage' : 'Fixed';
                $comFromAdm = $r['ss_comm'][0]['com_amount'];
                $to_ss = 0;
                $hf = $r['ss_comm'][0]['handle_fee'];
                switch ($r['book_by']) {
                    case 6:
                        $utype =  'Seat Seller A';
                         break;
                    case 7:
                        $utype =  'Seat Seller B';
                        break;
                }
                $total_ticket = $total_ticket + $r['nos'];
        ?>
        <tr>
            <td style="text-align: center;"><a href="{{URL::to($lt.'/ticket-details')}}/{{$r['booking_id']}}">{{$r['booking_id']}}</a></td>
            
            <td style="text-align: center;">{{ $r['ss_comm'][0]['sp']['first_name'] .' '. $r['ss_comm'][0]['sp']['last_name'] }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['pickup_date'])) }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['dropping_date'])) }}</td>
            <td style="text-align: center;">{{ $r['from_city'].' - '. $r['to_city']}}</td>
            <td style="text-align: center;">{{$utype}}</td>
            <td style="text-align: center;">{{ $r['booker_name'] }}</td>
            <td style="text-align: center;"><?php echo $sname; ?></td>
            <td style="text-align: center;">{{$comtype}}</td>
            <td style="text-align: center;">{{ $r['nos'] }}</td>
            <td style="text-align: right;">{{ round($r['total_amount']) }}</td>
            <td style="text-align: right;">{{ round($comFromAdm) }}</td>
            <td style="text-align: right;">{{ round($r['total_amount'] - $comFromAdm) }}</td>
            <td style="text-align: right;">
                <?php 
                    echo $hf;
                    $netincome = $comFromAdm - $to_ss + $hf;
                ?>
            </td>
            <td style="text-align: right;">{{ round($comFromAdm - $to_ss + $hf) }}</td>
            
            <?php
                $t_ttl_amount += $r['total_amount'] ;
                $t_from_sp += $comFromAdm;
                $t_net_price += $netprice;
                $t_to_ss += $to_ss;
                $t_hf += $hf;
                $t_net_income += $netincome;
            ?>
            
        </tr>
        <?php } ?>
        <tr>
            <td colspan="9" style="text-align: center;"><h4 style="font-weight: bold;">Total</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ $total_tiket }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_ttl_amount,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_from_sp,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_net_price,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_hf,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_net_income,3) }}</h4></td>
            
        </tr>
    </tbody>
</table>

<?php }else{ ?>
    <h2 style='color:red;'>No Data Found</h2>
<?php } ?>


<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
<style>
    .get_child_comm{
        cursor: pointer;
        color: blue;
    }
    
</style>