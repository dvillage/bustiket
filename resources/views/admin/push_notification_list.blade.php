@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');
        
        getData('admin/push-notification-filter',crnt,len,type,'','');
        
    };
    
   
    
    function allCheckBox(){
       var mchk=$("#masterCheck").is(":checked");
        if(mchk){
           $(".icheck").each(function(){
           
                var chk=$(this).is(":checked");
                if(!chk){
                    $(this).trigger('click');
                }     
            });
       
       }
       else{
          $(".icheck").each(function(){
              
            var chk=$(this).is(":checked");
                if(chk){
                    $(this).trigger('click');
                }
            });
              
        }
    }
    
    
    function sendNotification(){
       var flag=0;
       var cnt=0;
       $(".icheck").each(function(){
           
                var chk=$(this).is(":checked");
                if(chk){
                   cnt++; 
                }     
        });
        
        if(cnt==0){
            alert("Please select at least 1");
        }else{
                $('#modal-card').find('#save-banner').attr('onclick','sendNewNotification()');
                $("#errorMsg").text('');
                $("#showError").hide();
                $('#card_details')[0].reset();
                $("gtype").val($("#searchname").val());
                $(".icheck").each(function(){

                        var chk=$(this).is(":checked");
                        if(chk){
                            var emailid=$(this).data("email");
                            var em=$("#email").val();
                            $("#email").val(em+emailid+',');
                        }
                });

                $('#modal-card').openModal();
        }
    }
   
    function sendNewNotification(){
        
        var url="{{URL::to('admin/new-push-notification')}}";
        $('#card_details').attr('action',url);
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                $("#errorMsg").text(res.msg);
                $("#showError").show();
                //Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/push-notification-filter');
            }
            location.reload();
        });
    }
    

    
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Push Notification </h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a>Push Notification</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        
                        <select id="searchname" name="searchname" class="validate" onchange="return filterData('admin/push-notification-filter');">
                                <option value="" disabled selected>--Select Type--</option>
                                <option value="all">All</option>
                                <option value="spm">Service Provider Main</option>
                                <option value="spa">Service Provider A</option>
                                <option value="spb">Service Provider B</option>
                                <option value="ssm">Seat Seller Main</option>
                                <option value="ssa">Seat Seller A</option>
                                <option value="ssb">Seat Seller B</option>
                        </select>
                        <label for="searchname">Filter By Group</label>
                    
                    </div>
                   
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Favorite Route</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/push-notification-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/push-notification-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/push-notification-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/push-notification-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/push-notification-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/push-notification-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="card_details" action="" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-card" class="modal bust-modal" style='width: 30%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>New Notification</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
               
                    <div class="content">
                        <div class="row">
                            <div class="col s12 m12 l12 ">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" value="" placeholder="" autocomplete="off" required>
                                        <label for="title">Title</label>
                                </div>
                            </div>  
                        </div>
                        <div class="row" >
                            <div class="col s12 m12 l12 ">
                                <div class="input-field">
                                    <label for="message">Message</label>
                                    <textarea id="message"  name="message" class="materialize-textarea"  autocomplete="off" required></textarea>
          
                                </div>
                            </div> 
                        </div>
                        <input id="email" name="email" type="hidden" value="" placeholder="" autocomplete="off" required>
                        <input id="gtype" name="gtype" type="hidden" value="" placeholder="" autocomplete="off" required>
                    </div>
                <div class="alert alert-danger" style="display:none" id="showError">
                    <strong>Danger</strong><p id="errorMsg"></p>
                    </div>
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Send</button>
          </div>
        </div>
         </form>
        
       
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="sendNotification();"data-position="top" data-delay="50" data-tooltip="Push Notification">
                <i class="mdi-content-send"></i>
            </a>
        </div>
    </div>
    
@endsection