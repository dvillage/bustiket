@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
    };
    $(document).ready(function(){
        $('#paypal').change(function(){
            if($(this).prop("checked") == true){
                $('#paypalblock').css('display','block');
                $('#paypal_id').attr('required',true);
            }
            else if($(this).prop("checked") == false){
                $('#paypalblock').css('display','none');
                $('#paypal_id').removeAttr('required');
            }
        });
    });

</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Add Service Provider</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a href='{{URL::to($lt.'/service-provider')}}'>Service Provider</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a >Add</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
 
    <!-- /Breadcrumb -->
    <div class="row" >
        <div class="col l3 m12"></div>
        <div class="col l6 m12">
            <div class="card">
                <div class="title">
                    <h4>Add Service Provider</h4>
                   <?php
//                   print_r($errors);
                   if(isset($errors) && $errors->first()!=''){ ?>
                    <div class="alert" style="text-align: center;">
                        <strong><?php echo $errors->first(); ?></strong>
                    </div>
                    <?php } ?>
                </div>
            <div class="content">
                <!-- Name -->
                <form id="profile" method="POST" action='{{URL::to($lt.'/add-service-provider')}}' data-parsley-validate >
                <!--<form id="profile" method="POST" action='' >-->

                    <?php if(isset($body['msg']) && $body['msg']!=""){ ?>   
                        <div class="alert pb-10 mb-15">
                            <strong><?php echo $body['msg']; ?></strong>
                        </div>   
                    <?php } ?>
                    
                    <div class="row" style='margin:0;padding-bottom: 30px;' >
                        <div class="col s9 l3 m3" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <label for="cfsp">Status</label>
                            </div>
                        </div>
                        <div class="col s3 l9 m9" style='padding:0px 10px 0 0 ;'>
                            <div class="input-field">
                                <p class="switch">
                                <label>
                                  <input type="checkbox" id='status' name="status"/>
                                  <span class="lever"></span>
                                </label>
                                </p>
                            </div>
                        </div>
                    </div>
                
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input type='hidden' name='type' id='type' value='Main'>
                                <input id="fname" name="fname" type="text" required>
                                <label for="fname">First Name</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="lname" name="lname" type="text" >
                                <label for="lname">Last Name</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-field">
                        <input id="email" name="email" type="email" required>
                      <label for="email">Email</label>
                    </div>
                
                    <div class="input-field">
                      <input id="password" name="password" type="password" required>
                      <label for="passowrd">Password</label>
                    </div>
                    

                    
                    <div class="input-field">
                        <input id="vat" name="vat" type="text" data-parsley-type="number">
                      <label for="vat">VAT</label>
                    </div>
                    

                    
                    <div class="input-field">
                        <input id="address" name="address" type="text" required>
                      <label for="address">Address</label>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="city" name="city" type="text" required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="state" name="state" type="text" required>
                                <label for="state">State</label>
                            </div>
                        </div>
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="pincode" name="pincode" type="text" required data-parsley-type="digits">
                                <label for="pincode">Postal Code</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="landline" name="landline" type="text" required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="landline">Landline</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="fax" name="fax" type="text" >
                                <label for="fax">FAX</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="mobile1" name="mobile1" type="text" required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="mobile1">Mobile 1</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="mobile2" name="mobile2" type="text" required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="mobile2">Mobile 2</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="handlefee" name="handlefee" type="text" required data-parsley-type="number">
                                <label for="handlefee">Handling Fee</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <select id='handletype' name='handletype' required >
                                    <option value="P">Percentage (%)</option>
                                    <option value="F">Flat</option>
                                </select>
                                <label for="handletype">Handling Type</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="cfsp" name="cfsp" type="text" required data-parsley-type="number">
                                <label for="cfsp">Given Commission</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <select id='cfsptype' name='cfsptype' required>
                                    <option value="P">Percentage (%)</option>
                                    <option value="F">Flat</option>
                                </select>
                                <label for="cfsptype">Given Commission Type</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-field">
                      <input id="cpname" name="cpname" type="text" required>
                      <label for="cpname">Contact Person Name</label>
                    </div>
                    
                    <div class="input-field">
                      <input id="cpno" name="cpno" type="text" required data-parsley-type="digits" data-parsley-minlength="10">
                      <label for="cpno">Contact Person No.</label>
                      
                    </div>
                    
                    <div class="input-field">
                        <textarea id="comment" name="comment" class="materialize-textarea"></textarea>
                      <label for="comment">Comment</label>
                      <input type="hidden" id='_token' name='_token' value='{{csrf_token()}}'>
                    </div>
                
                    <div class="file-field input-field">
                        <div class="btn small">
                            <span class="medium">Logo</span>
                            <input type="file" name="sp_logo" id="sp_logo" accept="image/*">
                            <!--<label for="bnr_img">Upload Banner Image</label>-->
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col s9 l6 m6" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <label for="cfsp">Have Paypal Account ?</label>
                            </div>
                        </div>
                        <div class="col s3 l6 m6" style='padding:0px 10px 0 0 ;'>
                            <div class="input-field">
                                <p class="switch">
                                <label>
                                  <input type="checkbox" id='paypal' />
                                  <span class="lever"></span>
                                </label>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-field" id='paypalblock' style='display: none;margin-top: 40px;'>
                      <input id="paypal_id" name="paypal_id" type="text">
                      <label for="paypal_id">Paypal ID </label>
                    </div>
                    
                    
                    

                    <!-- Validation Button -->
                    <div class="row">
                      <div class="col s12">
                          <button class="btn" id="save">Register</button>
                          <a class="btn"  href='{{URL::to($lt.'/service-provider')}}' >Cancel </a>
                        
                      </div>
                    </div>
                    <!-- /Validation Button -->

                </form>   

            </div>
        </div>



      </div>
        <div class="col l3 m12"></div>

    </div>

@endsection