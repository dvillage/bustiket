@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');
        
        getData('admin/bulk-sms-filter',crnt,len,type,'','');
        
    };

    function byKeywordLen(len){
       
        var gender = $('#gender :selected').val();
        var status = $('#status :selected').val();
        
        if(len==''){
            len=$("#len").val();
        }
        
        var url = 'admin/bulk-sms-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        if(gender != ''){
            data['gender'] = gender;
        }
 
       return filterData(url,len,data);       
   }


   function byKeyword(){
         
       var gender=$("#gender :selected").val();          
       var status=$("#status :selected").val();

       
//       console.log('gender=',gender);
//       console.log('status=',status);
//       console.log('search=',$("#searchname").val());
      
       
        var url = 'admin/bulk-sms-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        if(gender != ''){
            data['gender'] = gender;
        }
 
       return filterData(url,'',data);       
   }
   
   function byKeywordWith(opr){
       
        var gender = $('#gender :selected').val();
        var status = $('#status :selected').val();
       
        var url = 'admin/bulk-sms-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        if(gender != ''){
            data['gender'] = gender;
        }
 
       return filterDataWith(url,opr,data);       
   }
   
   $(document).on('click','#search',function(){
        
        var gender = $('#gender :selected').val();
        var status = $('#status :selected').val();
       
        var url = 'admin/bulk-sms-filter';
        var data = {};
        
       console.log('gender=',gender);
       console.log('status=',status);
        
        if(status != ''){
            data['status'] = status;
        }
        if(gender != ''){
            data['gender'] = gender;
        }
        
        return filterData(url,'',data);       
    });
   

    function allCheckBox(){
       var mchk=$("#masterCheck").is(":checked");
        if(mchk){
           $(".icheck").each(function(){
           
                var chk=$(this).is(":checked");
                if(!chk){
                    $(this).trigger('click');
                }     
            });
       
       }
       else{
          $(".icheck").each(function(){
              
            var chk=$(this).is(":checked");
                if(chk){
                    $(this).trigger('click');
                }
            });
              
        }
    }
    
    
    function sendSms(){
       var flag=0;
       var cnt=0;
       $(".icheck").each(function(){
           
                var chk=$(this).is(":checked");
                if(chk){
                   cnt++; 
                }     
        });
        
        if(cnt==0){
            alert("Please select at least 1");
        }else{
                $('#modal-card').find('#save-banner').attr('onclick','sendNewSms()');
                $("#errorMsg").text('');
                $("#showError").hide();
                $('#card_details')[0].reset();
                $("gtype").val($("#searchname").val());
                $(".icheck").each(function(){

                        var chk=$(this).is(":checked");
                        if(chk){
                            var emailid=$(this).data("email");
                            var em=$("#email").val();
                            $("#email").val(em+emailid+',');
                        }
                });

                $('#modal-card').openModal();
        }
    }
   
    function sendNewSms(){
        
        var url="{{URL::to('admin/new-bulk-sms')}}";
        $('#card_details').attr('action',url);
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                $("#errorMsg").text(res.msg);
                $("#showError").show();
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/push-notification-filter');
            }
            location.reload();
        });
    }
    

    
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Bulk SMS </h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a>Bulk SMS</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    
                        
                        <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                            <input id="searchname" type="text" class="validate" placeholder="Enter Name, City, State, Pincode, Contact Nos, Email to search for a Details" onkeyup="byKeyword();">
                                <label for="input_text">Type Any</label>
                               
                        </div>
                        
                        <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;' >
                            <select id="gender" name="gender" class="validate" onchange="byKeyword();">
                                    
                                    <option value="">All</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                   
                            </select>
                        <label for="searchname">Filter By Gender</label>
                        </div>
                         
                        
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="status"  name="status" class="validate" onchange="byKeyword();">
                            <option value="">All</option>
                            <option value="0">InActive</option>
                            <option value="1">Active</option>
                            <option value="2">Suspend</option>
                        </select>
                        <label for="status">Filter By Status</label>
                   </div>
                    <input type="hidden" id="hgender"/>
                    <input type="hidden" id="hstatus"/>
                   <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                   </div>   
                        
                    
                    
                   
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>User List</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="byKeywordWith(this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="byKeywordWith(this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="byKeyword('','');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="byKeywordWith(this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="byKeywordWith(this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" name="" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="byKeywordLen(this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="card_details" action="" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-card" class="modal bust-modal" style='width: 30%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>New Notification</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
               
                    <div class="content">
                        
                        <div class="row" >
                            <div class="col s12 m12 l12 ">
                                <div class="input-field">
                                    <label for="message">Message</label>
                                    <textarea id="message"  name="message" class="materialize-textarea"  autocomplete="off" required></textarea>
          
                                </div>
                            </div> 
                        </div>
                        <input id="email" name="email" type="hidden" value="" placeholder="" autocomplete="off" required>
                        
                    </div>
                <div class="alert alert-danger" style="display:none" id="showError">
                    <p id="errorMsg"></p>
                    </div>
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Send</button>
          </div>
        </div>
         </form>
        
       
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="sendSms();"data-position="top" data-delay="50" data-tooltip="Send SMS">
                <i class="mdi-content-send"></i>
            </a>
        </div>
    </div>
    
@endsection