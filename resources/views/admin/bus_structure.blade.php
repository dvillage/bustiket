@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        
    };
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>{{ $body['bus'] ? $body['bus']['name'] : 'Travelers' }}</h1>
                <ul>
                    <li><a href='{{URL::to($lt.'/service-provider')}}'><i class="fa fa-home"></i> Service Provider</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a href='{{URL::to($lt.'/service-provider/buses/'.$body['bus']['sp_id'])}}'>{{ $body['all_bus'] ? $body['all_bus']['first_name'].' '.$body['all_bus']['last_name'] : 'no service provider' }}</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a >Buses</a>  <i class='fa fa-angle-right'></i></li>
                    </li>
                    <li><a href=''>{{ $body['bus'] ? $body['bus']['name'] : 'no bus' }}</a> 
                </ul>
            </div>
            
        </div>

    </div>

     <!-- /Breadcrumb -->
     <?php if(isset($body['all_bus'])){
         
         if(isset($body['bus'])){
             $bus = $body['bus'];
         ?>
    <div class="row">
        <form id="edit_bus_form" action="{{URL::to($lt.'/edit-bus')}}" method="post" enctype="multipart/form-data" data-parsley-validate >
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="sp_id" value="{{$body['bus']['sp_id']}}" />
              <input type="hidden" name="bus_id" value="{{$body['bus']['id']}}" />
              <input type="hidden" name="seat_array" id="seat_array" value="" />
        <div class="col s12">
            <ul class="tabs" style="overflow: hidden">
            <li class="tab col s3"><a class="active addHash" href="#general">General</a></li>
            <li class="tab col s3"><a class="addHash" href="#routes">Routes</a></li>
            <li class="tab col s3"><a class="addHash" href="#layouts">Layouts</a></li>
            <li class="tab col s3"><a class="addHash" href="#coupon_code">Coupon Code</a></li>
            <li class="tab col s3"><a class="addHash" href="#blocked_date">Blocked Dates</a></li>
          </ul>
        </div>
        <div class="fixed-action-btn" style="">
            <button id="save_bus" class="btn-floating btn-large green">Save</button>
        </div>
        <div id="general" class="col s12">
            <div class="row" style="display:inline;">
                <div class="col s12 m5 l5">
                    <div class="col s12 m2 l2" style="padding: 0px;margin-top: 10px">Bus Name : </div>
                    <div class="col s12 m8 l8">
                        <input id="bus_name" type="text" name="bus_name" value="{{ $bus['name'] }}" class="validate" placeholder="Bus Name" required>
                    </div>
                </div>
                <div class="col s12 m4 l5">
                    <div class="col s12 m2 l2" style="padding: 0px;margin-top: 10px">Bus Number : </div>
                    <div class="col s12 m4 l4">
                        <input id="bus_name" type="text" name="bus_number" value="{{ $bus['bus_number'] }}" class="validate" placeholder="Bus Number" required>
                    </div>
                </div>
                <div class="col s12 m3 l2">
                    <div class="col s12 m4 l4"  style="padding: 0px;margin-top: 18px" >Available </div>
                    <div class="col s12 m1 l1">
                        <p class="switch">
                            <label>
                                <input type="checkbox" name="bus_status" id="bus_status" <?php echo $bus['status'] ? 'checked' : ''; ?> />
                              <span class="lever"></span>
                            </label>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m3 l3">
                    <div class="file-field input-field">
                        <div class="btn">
                          <span>Bus images</span>
                          <input type="file" name="images[]" id="bus_images" multiple="" accept="image/*">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div class="col s12 m9 l9">
                    <?php
                    $other = $bus['images'];
                    if($other != ''){
                        $other = explode(',',$other);
                        foreach($other as $o){
                            $file = URL::to('assets/uploads/bus_images').'/'.$o;
                            $file_check = config('constant.BUS_IMAGE_PATH').$o;
                            if(file_exists($file_check) && $o!= ''){
                                echo '<span style="height:70px;width:70px;position:relative;"><img src="'.$file.'" style="height:40px;width:40px;" /> <span style="position:relative;top:-40px;color:red;cursor:pointer;" name="'.$o.'" onclick="removeImg(this);">x</span> </span>&nbsp;';
                            }
                        }
                            
                        }
                    ?>
                    <input type="hidden" id="removed_images" name="removed_images" />
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12" style="display:inline;">
                    <!--<div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Drivers : </div>-->
                    <div class="col s12 m12 l12">
                        <div class="input-field">
                            <input name="driver" type="text" id="drivers" value="" class="validate" autocomplete="off">
                          <label for="input_text">Drivers</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                <ul class="collapsible"  id="coll-2" data-collapsible="accordion">
                <li class="active">
                    <div class="collapsible-header active"> <b>Driver List </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-2-ar" class="mdi-hardware-keyboard-arrow-up right small" ></i></a>
                    </div>
                    <div class="collapsible-body">
                        <table id="driver_list" class="default_padding table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th class="center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $dr = $body['drivers'];
                                if(count($dr) > 0){
                                    foreach($dr as $d){
                                        ?>
                                <tr>
                                    <td class="default_padding">{{$d['driver']['name']}} <input type="hidden" name="drivers[]" value="{{$d['driver']['id']}}" /> </td>
                                    <td class="default_padding">{{$d['driver']['mobile']}}</td>
                                    <td class="default_padding">{{$d['driver']['email']}}</td>
                                    <td class="default_padding">{{$d['driver']['address']}}</td>
                                    <td class="default_padding center"><i id="remove_d{{$d['driver']['id']}}" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear small" ></td>
                                </tr>    
                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </li>
                </ul>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Amenities : </div>
                    <div class="col s12 m12 l12">
                        <?php 
                        $amenities = $bus['amenities'];
//                        echo '<pre>';
//                        dd($bus);
//                        echo '</pre>';
                        $a = '';
                        if(isset($amenities) && count($amenities)>0){
                            foreach($amenities as $am){
                                $a .=$am['aname'].','; 
                            }
                        }
                        ?>
                        <input id="amenities" type="text" name="amenities" value="{{$a}}" class="validate" >
                    </div>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Terms & Conditions : </div>
                    <div class="col s12 m12 l12">
                        
                        <textarea  id="tnc" name="tnc" class="materialize-textarea">{{$bus['tnc']}}</textarea>
                        <script>
                            CKEDITOR.replace( 'tnc' );
                        </script>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
              <?php 
                $city = $body['city'];
                $dist = $body['dist'];
                $city->prepend('Please select city','');
//                dd($dist);
                
                ?>
        <div id="routes" class="col s12">
            <div class="row" style="display: inline">
                <!--<div class="col s12 m2 l1"></div>-->
                <div class="col s12 m10 l9">
                    <div class="col s12 m2 l2">
                        <label>Select City</label>
                        {{ Form::select('select_city', $city ,'Select city',['id'=>'select_city','onchange' => 'setOption(this,"c")']) }}
                    </div>
                    
                    <div class="col s12 m2 l2" >
                        <label>Select District</label>
                        {{ Form::select('district', array(''=>'Select district'),'',['id'=>'district','onchange' => 'setOption(this,"d")']) }} </div>
                    
                    <div class="col s12 m3 l3">
                        <label>Select Terminal</label>
                        {{ Form::select('select_terminal', array(''=>'Select terminal'),'Select Terminal',['id'=>'select_terminal','onchange' => 'setOption(this,"t")']) }} </div>
                    <div class="col s12 m2 l2">
                        <label>Board Time</label>
                        <input type="text" id="board_time" name="board_time" class="clockpicker" data-inputmask="'mask': 'h:s'" data-autoclose="true" type="text" data-donetext="OK"  data-placement="" data-align="" placeholder="boarding time" >
                    </div>
                    <div class="col s12 m3 l3"  style="padding: 0px;margin-top: 18px;" ><a id="add_route" class="btn btn-small default_color" style="color:white">Add</a> </div>
                </div>
                <?php if($lt == 'admin'){ ?>
                <div class="col s12 m2 l3" style="text-align: right">
                    <a href="{{URL::to('assets/uploads/sample.csv')}}" class="btn btn-small blue " download><i class="mdi-file-cloud-download right" ></i>  Sample</a>
                   <a  class="btn btn-small " id="upload_csv"><i class="mdi-file-cloud-upload right " ></i>  CSV</a> 
                </div>
                <?php } ?>
            </div>
            <div id="all_routes" style="width: 50%">
                <?php 
                $routes = $body['routes'];
                ?>
                <table class="table-bordered default_padding">
                    <thead>
                    <tr>
                        <th>City</th>
                        <th>District</th>
                        <th>Terminal</th>
                        <th>Departure Time</th>
                    </tr>
                    </thead>
                    <tbody id="route_list">
                        <?php 
                        $added = array();
                        $dispf = 0;
                        $prc = 1;
                        if(count($routes) > 0){
                        foreach($routes as $rt){ 
                            $dispf = 1;
                            $prc = count($rt['prices']);
                            $id = $rt['from_district_id'].'-'.$rt['from_city_id'].'-'.$rt['from_terminal_id'];
                            if(!in_array($id, $added)){
                                array_push($added, $id);
                            ?>
                        <tr>
                            <!--var id= 'id_'+dist+'-'+city+'-'+terminal;-->
          
                            <td>{{$rt['fromCity']['name']}}</td> 
                            <td>{{$rt['fromDistrict']['name']}} <input type="hidden" id="r_{{$id}}" value="{{$rt['boarding_time']}}" </td> 
                            <td>{{$rt['fromTerminal']['name']}}</td>
                            <td><span>{{$rt['boarding_time']}} &nbsp;&nbsp;<i style="cursor:pointer" id="id_{{$id}}" onClick="removeRoutes(this,'{{$id}}')" class="mdi-content-clear middle small" ></i></span></td>  
                        </tr>
                        <?php }
                        $id = $rt['to_district_id'].'-'.$rt['to_city_id'].'-'.$rt['to_terminal_id'];
                        if(!in_array($id, $added)){
                                array_push($added, $id);
                            ?>
                        <tr>
                            <!--var id= 'id_'+dist+'-'+city+'-'+terminal;-->
          
                            <td>{{$rt['toCity']['name']}}</td> 
                            <td>{{$rt['toDistrict']['name']}} <input type="hidden" id="r_{{$id}}" value="{{$rt['droping_time']}}" </td> 
                            <td>{{$rt['toTerminal']['name']}}</td>
                            <td><span>{{$rt['droping_time']}} &nbsp;&nbsp;<i style="cursor:pointer" id="id_{{$id}}" onClick="removeRoutes(this,'{{$id}}')" class="mdi-content-clear middle small" ></i></span></td>  
                        </tr>
                        <?php }
                        }
                        }
                        $disp = 'none';
                        if($dispf){
                            $disp = 'block';
                        }
                        ?>
                    </tbody>
                </table>
                <div class="col s12 m2 l2">
                    <a id="set_fare" class="btn btn-small" style="display: {{$disp}};" >Set Fare</a>
                </div>
                
            </div>
            <div id="all_fare">
                <?php
//                $sPrice = $routes[''];
                ?>
                <table class="table-bordered">
                    <thead>
                        <tr>
                            <th  colspan="10"></th>
                            <th colspan="{{$prc}}" style="text-align: center;">Price 
                                <i class="fa fa-minus right remove_price" title="Remove last pricing" style="cursor: pointer;color: red;" aria-hidden="true"></i>
                                <i class="fa fa-plus right add_price" title="Add more pricing" style="cursor: pointer;color: green;" aria-hidden="true"></i>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="4" style="text-align: center">From</th>
                            <th>&nbsp;</th>
                            <th colspan="4" style="text-align: center">To</th>
                            <th>&nbsp;</th>
                            <?php for($i = 0;$i<$prc;$i++){ 
                            ?>
                            <th style="text-align: center;">Price <?php echo App\Models\General::getAlphabate($i); ?>
<!--                                <i class="fa fa-minus right remove_price" title="Remove last pricing" style="cursor: pointer;color: red;" aria-hidden="true"></i>
                                <i class="fa fa-plus right add_price" title="Add more pricing" style="cursor: pointer;color: green;" aria-hidden="true"></i>-->
                            </th>
                            <?php } ?>
                        </tr>
                        <tr id="menu_head">
                            <th>City</th>
                            <th>District</th>
                            <th>Terminal</th>
                            <th>Time</th>
                            <th></td>
                            <th>City</th>
                            <th>District</th>
                            <th>Terminal</th>
                            <th>Time</th>
                            <th></th>
                            <?php for($i = 0;$i<$prc;$i++){
                                $a = App\Models\General::getAlphabate($i);
                                ?>
                            <th style="width: 10%">
                                <div class="col sm12 m6 l6 default_padding">
                                    <input type="number" id="scount" class="seat_count_text" name="seat_count[{{$a}}]" value="<?php  echo count($routes) > 0 ?$routes[0]['prices'][$i]['seat_count']:'' ?>" required placeholder="seat count" >
                                </div>
                            </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody id="set_all_fare">
                        <?php
                        $RouteJs = array();
                        $collect = array();
                        if(count($routes) > 0){
                        foreach($routes as $rt){
                            $id = $rt['from_district_id'].'-'.$rt['from_city_id'].'-'.$rt['from_terminal_id'].'_'.$rt['to_district_id'].'-'.$rt['to_city_id'].'-'.$rt['to_terminal_id'];
                            $fid = $rt['from_district_id'].'-'.$rt['from_city_id'].'-'.$rt['from_terminal_id'];
                            $tid = $rt['to_district_id'].'-'.$rt['to_city_id'].'-'.$rt['to_terminal_id'];
                            $fromR = [
                                    'did' => $rt['from_district_id'],
                                    'cid' => $rt['from_city_id'],
                                    'tid' => $rt['from_terminal_id'],
                                    'dname' => $rt['fromDistrict']['name'],
                                    'cname' => $rt['fromCity']['name'],
                                    'tname' => $rt['fromTerminal']['name'],
                                    'btime' => $rt['boarding_time'],
                                    'rid' => $fid,
                                    'id' =>$rt['id']
                                ];
                            $toR = [
                                    'did' => $rt['to_district_id'],
                                    'cid' => $rt['to_city_id'],
                                    'tid' => $rt['to_terminal_id'],
                                    'dname' => $rt['toDistrict']['name'],
                                    'cname' => $rt['toCity']['name'],
                                    'tname' => $rt['toTerminal']['name'],
                                    'btime' => $rt['droping_time'],
                                    'rid' => $tid,
                                    'id' =>$rt['id']
                                ];
                            if(!in_array($fid, $collect)){
                                array_push($collect, $fid);
                                array_push($RouteJs, $fromR);
                            }
                            if(!in_array($tid, $collect)){
                                array_push($collect,$tid );
                                array_push($RouteJs, $toR);
                            }
                            ?>
                           
            
                            <tr id="tr_{{$id}}">
                                    <td>{{$rt['fromCity']['name']}}
                                        <input type="hidden" name="route[from][]" value="{{json_encode($fromR)}}">
                                        <input type="hidden" name="route[to][]" value="{{json_encode($toR)}}">
                                        <input type="hidden" name="route_id[]" value="{{$rt['id']}}">
                                    </td>
                                    <td>{{$rt['fromDistrict']['name']}}</td>
                                    <td>{{$rt['fromTerminal']['name']}}</td>
                                    <td>{{$rt['boarding_time']}}</td>
                                    <td></td>
                                    <td>{{$rt['toCity']['name']}}</td>
                                    <td>{{$rt['toDistrict']['name']}}</td>
                                    <td>{{$rt['toTerminal']['name']}}</td>
                                    <td>{{$rt['droping_time']}}</td>
                                    <td></td>
                                    <?php foreach($rt['prices'] as $k=>$r){
                                        $a = App\Models\General::getAlphabate($k);
                                        ?>
                                    <td>
                                        <div class="col sm12 m6 l6 default_padding">
                                            <input type="number" name="price[{{$a}}][]" value="{{$r['price']}}" placeholder="enter price" >
                                            <input type="hidden" name="price_ids[{{$a}}][]" value="{{$r['id']}}" >
                                        </div>   
                                    </td>
                                    <?php } ?>
                                <!--for(var k= 0;k<addTd;k++){-->
                                    <!--var chr = getAlphaBats()[k+1];-->
<!--                                    <td>
                                        <div class="col sm12 m6 l6 default_padding"><input type="number" value="0" name="price['+chr+'][]" placeholder="enter price" ></div>    
                                    </td>-->
                                <!--}-->
                            </tr>
                            <?php
                        }
                        }
                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="layouts" class="col s12">
            
            <div class="row" style="display: inline;" >
                <div class="col s12 m34 l4"></div>
                <div class="col s12 m2 l2">
                  <p class="switch">
                                <label>
                                     Layout 
                                    <input type="checkbox" name="is_layout" id="is_layout" <?php echo $bus['layout'] ? 'checked':'' ?> />
                                  <span class="lever"></span>
                                </label>
                            </p>
                            
                </div>
                <?php
                $dis = $bus['layout'] ? 'none' : 'inline';
                $req = $bus['layout'] ? '' : 'required data-parsley-min="4"' ;
                ?>
                <div class="col s12 m2 l2" id="total_seats_div" style="display: {{$dis}}" >
                    <div class="input-field">
                        <input id="total_seats" name="total_seats" type="number" <?php echo $req ?> class="validate" value="{{$bus['seat_capacity']}}" >
                      <label for="input_text">Numbers of Seats</label>
                    </div>
                </div>
            </div>
            
            <?php
            $l = $bus['layout'] ? 'inline' : 'none' ;
            
            ?>
            <div class="row" id="display_layout" style="display: {{$l}}">
                
            <div class="col s12 m12 l2 xl1">
                <ul class="collection">
                    <li class="collection-item "  style="display: inline-block;width: 100% ;text-align: center">
                        <div class="col s12 default_padding seat_chair"  id="seat_chair" style="cursor: pointer;" draggable="true" ondragstart="drag(this)" >
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <span style="position: absolute;top: -5px;left: 35px;color: green" class="slable" onclick="" ></span>
                            <img src="{{URL::to('assets/images/layout_icons/car-seat.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Seat Chair
                        </div>
                    </li>
<!--                    <li class="collection-item " style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <img src="{{URL::to('assets/images/layout_icons/sofa1.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Seat Sleeping
                        </div>
                    </li>-->
                    <li class="collection-item " style="display: inline-block;width: 100%;text-align: center">
                        <div class="col s12 default_padding steering" id="steering" style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/steering.png')}}" style="height:40px;width:40px" />
                        </div>
                        <div class="col s12 default_padding driver" id="driver">
                            Driver
                        </div>
                    </li>
                    <li class="collection-item " style="display: inline-block;width: 100%;text-align: center">
                        <div class="col s12 default_padding toilet" id="toilet"  style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/toilet.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Toilet
                        </div>
                    </li>
                    <li class="collection-item " style="display: inline-block;width: 100%;text-align: center">
                        <div class="col s12 default_padding door " id="door" style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/door.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Door
                        </div>
                    </li>
                </ul>
                
            </div>
            <div id="layout" class="col s12 m12 l8 xl8" style="text-align: center;">
                <div id="layout_in" style="position: relative" ondrop="drop(event)" ondragover="allowDrop(event)">
                    
                </div>
                
            </div>
                <?php
//                print_r($bus['busLayout']);exit;
                ?>
            <div class="col s12 m12 l2 xl2">
                <ul class="collection">
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <div class="input-field">
                                <input id="total_columns" name="total_columns" type="number" value="<?php echo isset($bus['busLayout'][0]) ? $bus['busLayout'][0]['columns'] :'' ?>" class="validate" >
                              <label for="input_text">Columns</label>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <div class="input-field">
                                <input id="total_rows" name="total_rows" type="number" value="<?php echo  isset($bus['busLayout'][0])?$bus['busLayout'][0]['rows']:'' ?>" class="validate" >
                              <label for="input_text">Rows</label>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding" ><a id="generate_layout" class="btn btn-small default_color" style="color:white">Generate</a> </div>
                    </li>
                </ul>
            </div>
            </div>
            
        </div>
     
     <div id="coupon_code" class="col s12">
         <div class="col s12">
             <a href="#add_coupon_code_div" class="btn right" onclick="addcc();">Add Coupon Code</a>
        </div>
         <table class="table-bordered">
             <thead>
                 <tr>
                     <th>Status</th>
                     <th>Code</th>
                     <th>Start Date</th>
                     <th>End Date</th>
                     <th>Type</th>
                     <th>Value</th>
                     <th>Minimum Amount</th>
                     <th>Max Amount</th>
                     <th>Action</th>
                 </tr>
             </thead>
             <tbody>
                 <?php
                 
                 $ccode = $bus['couponCode'];
                 if(count($ccode) > 0){
                     foreach($ccode as $cd){
                         ?>
                 <tr>
                     <td>
                         <input type="hidden" id="status-{{$cd['id']}}" value="{{$cd['status']}}" />
                         <?php echo $cd['status'] == 1 ? '<i style="color:green">Active<i>' : '<i style="color:red">Inactive</i>' ; ?></td>
                     <td><span id='ccode-{{ $cd['id'] }}'>{{$cd['code']}}</span></td>
                     <td><span id='csdate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['start_date']))}}</span></td>
                     <td><span id='cedate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['end_date']))}}</span></td>
                     <td><span id='ctype-{{ $cd['id'] }}' name="{{$cd['type']}}" >{{$cd['type'] == 'P'?'Percentage':'Fix'}}</span></td>
                     <td><span id='cvalue-{{ $cd['id'] }}'>{{(int)$cd['value']}}</span></td>
                     <td><span id='cmin-{{ $cd['id'] }}'>{{(int)$cd['min_amount']}}</span></td>
                     <td><span id='cmax-{{ $cd['id'] }}'>{{(int)$cd['max_amount']}}</span></td>
                     <td>
                         <a class="btn-floating btn-small blue" id="" onclick="editcc( {{$cd['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                        <a class="btn-floating btn-small red btn " id= "" onclick="delCouponCode( {{ $cd['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                     </td>
                 </tr>
                 <?php
                     }
                 }
                 ?>
             </tbody>
         </table>
     </div>
     <div id="blocked_date" class="col s12">
<!--         <ul class="collapsible"  id="coll-2" data-collapsible="accordion" >
                <li class="active">
                    <div class="collapsible-header active"> <b>Blocking Dates </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-2-ar" class="mdi-hardware-keyboard-arrow-up right small" ></i></a>
                    </div>
                    <div class="collapsible-body">-->
                        
                        <table id="block_dates" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="6">Blocking Dates
                                        <!--<a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>-->
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $bdates = $bus['blockDates'];
//                                print_r($pricing);exit;
                                $dcnt = count($bdates);
                                if($dcnt == 0 || $dcnt ==1){
                                ?>
                              <tr>
                                <th class="br bl bb bt">1</th>
                                <td class="br bl bb bt">From : </td>
                                <td class="br bl bb bt">
                                    <input id="date_from_1" type="text" name="block_date[1][from]" value="{{ $dcnt ? date('Y-m-d',strtotime($bdates[0]['from_date'])) : '' }}"  data-inputmask="'mask': 'y-m-d'"  class="validate pikaday" placeholder="from date">
                                    <input type="hidden" name="block_date[1][id]" value="{{ $dcnt ? $bdates[0]['id'] : '' }}" />
                                </td>
                                <td class="br bl bb bt">To :</td>
                                <td class="br bl bb bt">
                                    <input id="date_to_1" type="text" name="block_date[1][to]" value="{{ $dcnt ? date('Y-m-d',strtotime($bdates[0]['to_date'])) : '' }}" date-diff="#date_from_1"  data-inputmask="'mask': 'y-m-d'"  class="validate pikaday" placeholder="date to">
                                </td>
                                <td class="br bl bt bb"><a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a></td>
                              </tr>
                              <?php
                                }else{
                                    $bitr = 1;
                                    foreach($bdates as $bd){
                                    ?>
                              <tr>
                                <th class="br bl bb bt">{{$bitr}}</th>
                                <td class="br bl bb bt">From : </td>
                                <td class="br bl bb bt">
                                    <input id="date_from_{{$bitr}}" type="text" name="block_date[{{$bitr}}][from]" value="{{ date('Y-m-d',strtotime($bd['from_date'])) }}"  data-parsley-data-inputmask="'mask': 'y-m-d'"  class="validate pikaday" placeholder="from date">
                                    <input type="hidden" name="date_from[{{$bitr}}][id]" value="{{$bd['id']}}" />
                                </td>
                                <td class="br bl bb bt">To :</td>
                                <td class="br bl bb bt">
                                    <input id="date_to_{{$bitr}}" type="text" name="block_date[{{$bitr}}][to]" value="{{ date('Y-m-d',strtotime($bd['to_date']))  }}" data-parsley-date-diff="#date_from_{{$bitr}}"  data-inputmask="'mask': 'y-m-d'"  class="validate pikaday" placeholder="date to">
                                </td>
                                <td class="br bl bt bb">
                                    <?php 
                                    if($bitr == 1){
                                    ?>
                                    <a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>
                                    <?php 
                                    }else{
                                        echo '<i id="remove_'.$bitr.'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
                                    }
                                    ?>
                                </td>
                              </tr>
                              <?php
                              $bitr++;
                                    }
                                }
                              ?>
                            </tbody>
                        </table>
<!--                    </div>
                </li>
            </ul>-->
     </div>
        </form>
    </div>
    
    <form id="add_coupon_code" action="{{URL::to($lt.'/add-coupon-code')}}" method="post" enctype="multipart/form-data" data-parsley-validate >
        <div id="add_coupon_code_div" class="modal bust-modal" style="width: 35%;font-size: 100%;overflow: hidden;">
            <div class="modal-content">
              <h4>Add Coupon Code</h4>
              <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="bus_id" value="{{$body['bus']['id']}}" />
              <input type="hidden" name="ccid" id="ccid" value="" />
              <input type="hidden" name="opr" id="opr" value="" />
              <div class="input-field">
                  <input id="ccode" type="text" name="ccode" class="validate" required="" placeholder="" >
                <label for="input_text">Coupon Code</label>
              </div>
              
              <div class="input-field">
                  <input id="csdate" type="text" name="csdate" data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="" >
                <label for="input_text">Start Date</label>
              </div>
              
              <div class="input-field">
                  <input id="cedate" type="text" name="cedate" data-inputmask="'mask': 'y-m-d'" required data-parsley-date-diff="#csdate" class="validate pikaday" placeholder="" >
                <label for="input_text">End Date</label>
              </div>
              <div class="row">
                  <div class="col s12 m6 l6">
                  <div class="input-field">
                    <input id="cvalue" type="number" name="cvalue" required="" placeholder="" class="validate" >
                  <label for="input_text">Coupon Value</label>
                </div>
              </div>
              
              <div class="col s12 m6 l6">
                  <div class="input-field">
                    <select id="ctype" name="ctype" required="">
                      <option value="" disabled selected>Select Type</option>
                      <option value="P">Percentage</option>
                      <option value="F">Fix</option>
                    </select>
                        <label for="input_text">Coupon Type</label>
              </div>
              </div>
              
              </div>
              <div class="input-field">
                  <input id="cminvalue" name="cminvalue" type="number"  placeholder=""class="validate" required="">
                <label for="input_text">Minimum Value ( Coupon Applied if ticket price is minimum of this amount )</label>
              </div>
              <div class="input-field">
                  <input id="cmaxvalue" name="cmaxvalue" type="number" placeholder="" class="validate" required="">
                <label for="input_text">Maximum Discount amount</label>
              </div>
              <div class="input-field">
                  <p class="switch">
                        <label>
                            Status
                            <input id="cstatus" name="cstatus" type="checkbox" />
                          <span class="lever"></span>
                          
                        </label>
                      </p>
              </div>
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
              <!--<a id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</a>-->
              <button id="save_ccode"  class="waves-effect waves-green btn-flat ">Save</button>
            </div>
          </div>
        </form>
    
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
    
    <!-- Modal Structure -->
        <div id="modal-lable" class="modal" style='width:35%;font-size:100%;max-height: 100%;'>
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>Lable Detail</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-lname" name="add-lname" type="text" placeholder="lable Name" required>
                        <label for="add-cname">Lable Name</label>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='save_lable' class="modal-action  waves-effect waves-green btn-flat ">Edit</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        <form id="csv_route" action="{{URL::to($lt.'/csv-route')}}" method="post" enctype="multipart/form-data" >
            <input type="file" name="csv" id="csv" style="display: none;" accept=".csv">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input type="hidden" name="bus_id" value="{{$body['bus']['id']}}" />
        </form>
     <script>
         var Routes = [];
        <?php if(count($RouteJs) > 0){
            $RouteJs = json_encode($RouteJs,true);
            ?>
            Routes = JSON.parse('<?php echo $RouteJs ?>');
//            Routes = ("{{$RouteJs}}");
//            console.log(Routes);
        <?php } ?>
         $('.addHash').click(function(){
             var has = $(this).attr('href');
             location.hash = has;
         });
         removedImg = {};
         function removeImg(obj){
            var img = $(obj).attr('name');
            removedImg[img] = img;
            $(obj).parent().remove();

        }
         
         function delCouponCode(id){
        
            var title='<b>Confirmation<b>';
            var msg='Are you sure to Delete Coupon Code ??';
            var onyes ='deleteCode';
            var param='\'{{URL::to($lt."/delete-coupon-code")}}\','+id;
            var mdlname='confirmDel';
            modalOpen(mdlname,title,msg,onyes,param);
        }
        function deleteCode(url,id){
//            url = '{{URL::to('+url+')}}';
            data={id:id};
            postAjax(url,data,function(res){
                if(res.flag == 1){
                    Materialize.toast(res.msg, 2000,'rounded');
                    location.reload();
                }else{
                    Materialize.toast(res.msg, 2000,'rounded');
                }
            });
        }
        function addcc(){
            $('#ccode').val('');
            $('#csdate').val('');
            $('#cedate').val('');
            $('#ctype').val('');
            $('#cvalue').val('');
            $('#cminvalue').val('');
            $('#cmaxvalue').val('');
            $('#ctype').material_select();
            $('#opr').val('Add');

            $('#add_coupon_code').attr('action',"{{URL::to($lt.'/add-coupon-code')}}");

            $('#add_coupon_code_div').find('#save_ccode').html('Add');
            $('#add_coupon_code_div').openModal();
            $('#status').trigger('click');
            $('#add_coupon_code').parsley().reset();
        }

        function editcc(id){

            $('#ccode').val($('#ccode-'+id).html());
            $('#csdate').val($('#csdate-'+id).html());
            $('#cedate').val($('#cedate-'+id).html());
            $('#ctype').val($('#ctype-'+id).attr('name'));
            $('#cvalue').val($('#cvalue-'+id).html());
            $('#cminvalue').val($('#cmin-'+id).html());
            $('#cmaxvalue').val($('#cmin-'+id).html());
            $('#ccid').val(id);
            $('#opr').val('Update');
            $('#ctype').material_select();
            $('#add_coupon_code').attr('action',"{{URL::to($lt.'/edit-coupon-code')}}");

            $('#add_coupon_code_div').find('#save_ccode').html('Update');
            $('#add_coupon_code_div').openModal();
            
            var status = $('#status-'+id).val();
            var chk = $('#cstatus').is(':checked');
            if(status == 1 && !chk){
                $('#cstatus').trigger('click');
            }else if(status == 0 && chk){
                $('#cstatus').trigger('click');
            }

            $('#add_coupon_code').parsley().reset();
        }
         $(document).on('click','#save_ccode',function(){
             $('#add_coupon_code').ajaxForm(function(res) {
                if(res.flag == 1){
                    if($('#opr').val() == 'Update'){
                        Materialize.toast('Coupon Code updated successfully !!', 2000,'rounded');
                    }else{
                        Materialize.toast('Coupon Code added successfully !!', 2000,'rounded');
                    }
                    location.reload();
                }else{
                    Materialize.toast(res.msg, 2000,'rounded');
                }
            });
         });
         $(document).ready(function(){
             
             window.Parsley.addValidator('dateDiff', {
              validateString: function(value, id) {
//                return 0 === value % requirement;
                var from_date = $(id).val();
                var to_date = value;
                from_date = from_date.split('-');
                new_date = new Date(from_date[0], from_date[1], from_date[2]);
                date1 = to_date.split('-');
                new_date1 = new Date(date1[0], date1[1], date1[2]);
                if(new_date1 < new_date){
                    return false;
                }
              },
              messages: {
                en: 'the to date must be after from date.'
              }
            });
             
             var amenities = {};
             
                 var url ='{{URL::to($lt."/get-amenites")}}';
                 var data = {};
                 postAjax(url,data,function(res){
                     if(typeof res.flag != 'undefined' && res.flag !=1){
                         Materialize.toast('No amenities found', 2000,'rounded');
                     }
                     amenities = res.data;
//                     $('#amenities').autocomplete({
//                        data: amenities,
//                        limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
//                        onAutocomplete: function(val,key) {
//                            console.log('id : '+key);
//                            $('#amenities').tagsInput();
//                          // Callback function when value is autcompleted.
//                        },
//                        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
//                    });
                    
                    $('#amenities').tagsInput({
//                        'autocomplete_url':url,
                        'autocomplete_data':amenities,
//                        'autocomplete_element':'amenities',
                        'height':'auto',
                        'width':'auto',
                        'autocomplete': {selectFirst:false,width:'auto',autoFill:true},
                        'interactive':true,
                        'defaultText':'type here..',
                        'onAddTag':function(){
                            
                        },
                        'onTagExist':function(res){
//                            console.log('exist');
                            Materialize.toast('This amenity '+res+' is already exist', 2000,'rounded');
                        },
                        'onRemoveTag':function(res){
//                            Materialize.toast('The amenity '+res+' is removed', 2000,'rounded');
                        },
//                        'onChange' : callback_function,
//                        'delimiter': [',',';'],   // Or a string with a single delimiter. Ex: ';'
                        'removeWithBackspace' : true,
                        'minChars' : 0,
                        'maxChars' : 0, // if not provided there is no limit
                        'placeholderColor' : '#666666'
                     });
                     $('.tagsinput').css({border:'none'});
                });
             var flag = 0;
            $('#drivers').keyup(function(){
                var str = $(this).val();
                if(str.length < 2){
                    flag = 1;
                    return false;
                }
                var url = '{{URL::to($lt."/drivers")}}';
                var data = {name:str};
                if(flag == 1){
                    postAjax(url,data,function(res){
                        if(res.flag == 1){
                            flag = 0;
                            var d = res.data;
                            $('#drivers').autocomplete({
                                data: d,
                                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                                onAutocomplete: function(val,key) {
                                    var exist = $('#driver_list').find('#remove_d'+key).length;
                                    if(exist){
                                        Materialize.toast('Driver already exist !!', 2000,'rounded');
                                        return false;
                                    }
                                    data = {id:key,all:1};
                                    url = '{{URL::to($lt."/add-driver-to-bus")}}';
                                    if(key > 0){
                                        addDriver(url,data);
                                    }
                                },
                                minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                            });
                        }
                    });
                }
            });
                
                
         });
        
        function addDriver(url,data){
            postAjax(url,data,function(res){
                $('#driver_list tbody').append(res);
            });
        }
        
        $('#coll-1').click(function(){
            if($(this).children().hasClass('active')){
                $('#coll-1-ar').removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-up');
            }else{
                $('#coll-1-ar').removeClass('mdi-hardware-keyboard-arrow-up').addClass('mdi-hardware-keyboard-arrow-down');
            }
        });
        $('#coll-2').click(function(){
            if($(this).children().hasClass('active')){
                $('#coll-2-ar').removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-up');
            }else{
                $('#coll-2-ar').removeClass('mdi-hardware-keyboard-arrow-up').addClass('mdi-hardware-keyboard-arrow-down');
            }
        });
         
         function removeTr(obj){
             $(obj).closest('tr').remove();
         }
        
         $('#add_date_scheme').click(function(){
             var t = $('#block_dates tr:last').children('th').html();
//             var t = $('#block_dates tr').length;
             t = parseInt(t)+1;
             var rm = '<i id="remove_'+t+'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
             console.log($('#block_dates').find('#remove_d').length);
             
             var tr = '<tr>'+
                    '<th class="br bl bb bt">'+t+'</th>'+
                    '<td class="br bl bb bt">From : </td>'+
                    '<td class="br bl bb bt"><input id="date_from_'+t+'" type="text" name="block_date['+t+'][from]" value="" required class="validate pikaday"  data-inputmask="\'mask\': \'y-m-d\'" placeholder="from date"><input type="hidden" name="block_date['+t+'][id]" /></td>'+
                    '<td class="br bl bb bt">To :</td>'+
                    '<td class="br bl bb bt"><input id="date_to_'+t+'" type="text" name="block_date['+t+'][to]" value="" required class="validate pikaday" data-parsley-date-diff="#date_from_'+t+'" data-inputmask="\'mask\': \'y-m-d\'" placeholder="to date"></td>'+
                    '<td class="br bl bt bb">'+rm+'</td>'+
                  '</tr>';
          $('#block_dates tbody').append(tr);
          $('.pikaday').pikaday({ minDate: new Date() });
          $('.pikaday').inputmask();
          $('.pikaday').parsley();
//          var picker = new Pikaday({ field: $('#pikaday'),minDate: new Date() });
//          console.log(picker.setMinDate(new Date()));
//          console.log(new Date());
      });
      $('.pikaday').pikaday({ minDate: new Date() });

      function removeRoute(obj){
          $(obj).closest('ul').remove();
//          $(obj).closest('table').remove();
      }
      
      
      
      $(document).on('click','.clockpicker',function(){
          
//        $(this).clockpicker('remove');
//        $(this).clockpicker();
        $(this).clockpicker();
      });
      
     function setOption(obj,t){
         var id = $(obj).val();
         if(id == ''){
             return false;
         }
//         if(id == ''){
//console.log(t+'-'+t.length);
             $('#board_time').val('');   
             if(t == 'c'){
                 console.log('entered in if');
                $('#district').html('<option value="">Select from list</option>');
                $('#select_terminal').html('<option value="">Select from list</option>');
                $('#district').material_select();
                $('#select_terminal').material_select();
             }else if(t == 'd'){
                $('#select_terminal').html('<option value="">Select from list</option>');
                $('#select_terminal').material_select();
             }
//         }
         
        var url ='{{URL::to($lt."/get-options")}}';
        var data = {id:id,type:t};  
        if(t == 'd' || t == 'c'){
            postAjax(url,data,function(res){
//                console.log(res);
                var repl = t =='c' ? 'district' : 'select_terminal';
                $('#'+repl).html('');
//                $('#'+repl).append('<option value="">Select from list</option>');
                var opt = '<option value="">Select from list</option>';
                if(res.flag == 1){
                    
                    if(res.data.length > 0){
                        for(var i in res.data){
//                            $('#'+repl).append('<option value="'+res.data[i]['id']+'">'+res.data[i]['name']+'</option>');
                            opt += '<option value="'+res.data[i]['id']+'">'+res.data[i]['name']+'</option>';
                        }
//                        $('#'+repl).material_select();
                    }
                }
                $('#'+repl).html(opt);
                $('#'+repl).material_select();
            });
        }
      }
      
      $('#add_route').click(function(){
         var dist = $('#district :selected').val(); 
         var city = $('#select_city :selected').val(); 
         var terminal = $('#select_terminal :selected').val(); 
         var btime = $('#board_time').val();
         if(dist == ''|| city == '' || terminal == '' || btime == ''){
             Materialize.toast('Please fill all the fields !!', 2000,'rounded');
             return false;
         }
//         if(typeof Routes[dist+'-'+city+'-'+terminal] !=='undefined'){
         if($('#r_'+dist+'-'+city+'-'+terminal).length > 0){
             Materialize.toast('This route already exist !!', 2000,'rounded');
             return false;
         }
         var distn = $('#district :selected').text(); 
         var cityn = $('#select_city :selected').text(); 
         var terminaln = $('#select_terminal :selected').text(); 
         var id= 'id_'+dist+'-'+city+'-'+terminal;
          var rm = '<i style="cursor:pointer" id="'+id+'" onClick="removeRoutes(this,\''+dist+'-'+city+'-'+terminal+'\')" class="mdi-content-clear middle small" >';
         //routr_list
         var tr = '<tr>\n\
                        <td>'+cityn+'</td>  \n\
                        <td>'+distn+' <input type="hidden" id="r_'+dist+'-'+city+'-'+terminal+'" value="'+btime+'" </td>  \n\
                        <td>'+terminaln+'</td>  \n\
                        <td><span>'+btime+'&nbsp;&nbsp;'+rm+'</span></td>  \n\
                  </tr>';
        $('#route_list').append(tr);
        var trl = $('#route_list').find('tr').length;
        if(trl >= 2) {
            $('#set_fare').css({display:'block'});
        }else{
            $('#set_fare').css({display:'none'});
        }
        var detail = {
            did : dist,
            cid : city,
            tid : terminal,
            dname : distn,
            cname : cityn,
            tname : terminaln,
            btime : btime,
            rid : dist+'-'+city+'-'+terminal,
            id:0
        };
//        Routes[dist+'-'+city+'-'+terminal] = btime;
        Routes.push(detail);
      });
//      $('.clockpicker').each(function(){
//          console.log('onload each clock picker');
//        $(this).clockpicker('remove');  
//      });
    function removeRoutes(obj,key){
//        obj= JSON.parse(obj);
            var title='<b>Confirmation<b>';
            var msg='Are you sure to Remove this Route ??<br>This will reset all data.';
            var onyes ='routeRemove';
            var param= '"'+key+'"';
            var mdlname='confirmDel';
//            console.log(param);
            modalOpen(mdlname,title,msg,onyes,param);
//            $('#confirmDel').find('#mdltitle').html(title);
//            $('#confirmDel').find('#mdlmsg').html(msg);
//            $('#confirmDel').find('#mdlyes').attr('onclick','routeRemove('+obj+',"'+key+'");');
//            $('#confirmDel').openModal();
        
    }
    function routeRemove(key){
        console.log(key);
//        obj= JSON.stringify(obj);
        $('#id_'+key).parent().parent().parent().remove();
        var trl = $('#route_list').find('tr').length;
        deleteRoute(key);
        if(trl <=1){
            $('#set_fare').css({display:'none'});
        }
        $('#set_all_fare').html('');
        if(trl >=1){
            $('#set_fare').trigger('click');
        }
    }
    function deleteRoute(id){
        for(var i in Routes){
            if(Routes[i]['rid'] == id){
//                delete Routes[i];
                Routes.splice(i,1);
            }
        }
    }
    $('#set_fare').click(function(){
//        var l = objLen(Routes);
        var l = Routes.length;
        console.log('route len '+l);
        var tr = '';
//        $('#set_all_fare').html('');
        var trl = $('#set_all_fare').html().trim().length;
//        console.log(trl);
        var dtd = 11;
        var addTd = 0;
        console.log($('#menu_head > th').length);
//        if(trl > 0){
            var ol = parseInt($('#menu_head > th').length);
            console.log(' td length '+ol);
            if(ol > dtd){
                addTd = ol - dtd;
            }
//        }
        for(var i=0;i<l;i++){
            
//            console.log(Routes[i]);
            for(var j=i+1;j<(l);j++){
                var ex = parseInt($('#set_all_fare').find('#tr_'+Routes[i]['rid']+'_'+Routes[j]['rid']).length);
                if(ex > 0){
                    continue;
                }
                var hdnf = '<input type="hidden" name="route[from][]" value=\''+JSON.stringify(Routes[i])+'\'>';
                var hdnt = '<input type="hidden" name="route[to][]" value=\''+JSON.stringify(Routes[j])+'\'>';
//                console.log(Routes[i]+'=>'+Routes[j]);
                tr += '<tr id="tr_'+Routes[i]['rid']+'_'+Routes[j]['rid']+'">\n\
                        <td>'+Routes[i]['cname']+hdnf+hdnt+'</td>\n\
                        <td>'+Routes[i]['dname']+'</td>\n\
                        <td>'+Routes[i]['tname']+'</td>\n\
                        <td>'+Routes[i]['btime']+'</td>\n\
                        <td></td>\n\
                        <td>'+Routes[j]['cname']+'</td>\n\
                        <td>'+Routes[j]['dname']+'</td>\n\
                        <td>'+Routes[j]['tname']+'</td>\n\
                        <td>'+Routes[j]['btime']+'</td>\n\
                        <td></td>\n\
                        <td>\n\
                            <div class="col sm12 m6 l6 default_padding"><input type="number" name="price[A][]" value="0" placeholder="enter price" ></div>    \n\
                        </td>';
//                    </tr>';
                    for(var k= 0;k<addTd;k++){
                        var chr = getAlphaBats()[k+1];
                        tr += '<td>\n\
                            <div class="col sm12 m6 l6 default_padding"><input type="number" value="0" name="price['+chr+'][]" placeholder="enter price" ></div>    \n\
                        </td>';
                    }
                tr += '</tr>';
            
            }
        }
        $('#set_all_fare').append(tr);
    });
    
    $('.add_price').click(function(){
        var curColSpan = parseInt($(this).parent().attr('colspan'));
        var dfl = 11;
        var ol = parseInt($('#menu_head > th').length);
        var gc = ol - dfl;
//        console.log(gc);
        var chr = getAlphaBats()[gc+1];
        $(this).parent().attr('colspan',curColSpan + 1);
//        console.log(curColSpan);
        $(this).parent().parent().next().append('<th  style="width: 10%;text-align:center;">Price '+chr+'</th>');
        $(this).parent().parent().next().next().append('<th  style="width: 10%;"><div class="col sm12 m6 l6 default_padding"><input type="number" name="seat_count['+chr+']" class="seat_count_text" required placeholder="seat count" ></div></th>');
        
        $('#set_all_fare tr').each(function(){
            $(this).append('<td><div class="col sm12 m6 l6 default_padding"><input type="number"  value="0" name="price['+chr+'][]" placeholder="enter price" ></div></td>');
        });
        
    });
    $('.remove_price').click(function(){
        var curColSpan = parseInt($(this).parent().attr('colspan'));
        var sc = $('.seat_count_text').length;
        if(sc > 1){
            $(this).parent().attr('colspan',curColSpan - 1);
            $(this).parent().parent().next().find('th:last-child').remove();
            $(this).parent().parent().next().next().find('th:last-child').remove();
            $('#set_all_fare tr').each(function(){
                $(this).find('td:last-child').remove();
            });
        }else{
            Materialize.toast('No more pricing to remove !!', 2000,'rounded');
        }
//        console.log(curColSpan);
        
        
    });
    
    function objLen(obj){
        var l = 0;
        for(var i in obj){
            l++;
        }
        return l;
    }
var routes = {};
      $(document).on('click','#save_bus',function(){
        var bus_name = $('#bus_name').val();
        var total_seats = $('#total_seats').val();
//        return false;
//        var bus_type = $('#bus_type :selected').val();
        if(bus_name == ''){
            Materialize.toast('Bus name should not be blank !!', 2000,'rounded');
            return false;
        }
//        console.log(routes);
//        console.log(JSON.stringify(routes));
        $('#seat_array').val(JSON.stringify(routes));
        $('#removed_images').val(JSON.stringify(removedImg));
//        $("#layout_in").append(routes);
//        console.log($('#seat_array').val());
//        console.log($('#seat_array').length);
        if($('#is_layout').is(':checked')){
            if($('#seat_array').val() == ''){
                Materialize.toast('Please add seats in bus layout !!', 2000,'rounded');
                return false;
            }
            
        }
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        
        var seat_price_count = $("#set_all_fare").children().length;
        if(seat_price_count == 0){
            $('.seat_count_text').removeAttr('required');
        }else{
            $('.seat_count_text').attr('required',true);
        }
//        else if(bus_type == ''){
//            Materialize.toast('Please select bus type !!', 2000,'rounded');
//            return false;
//        }
        $('#edit_bus_form').ajaxForm(function(res) {
           console.log(res);
           if(res.flag == 1){
               Materialize.toast('Bus edited successfully !!', 2000,'rounded');
               location.reload();
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });
   
    function removeSeat(obj){
        var ele = $(obj).parent();
        var rc = eleRowCol(ele);
        routes[rc.row+'-'+rc.col] = '';
        console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
        $(obj).parent().remove();
        removeElement(rc.row+'-'+rc.col);
//        $('#generate_layout').trigger('click');
    }
    function chekLayout(){
        var row = $('#total_rows').val();
          var col = $('#total_columns').val();
          
          if(col =='' || col <2){
              Materialize.toast('Please enter minimum 2 total columns', 2000,'rounded');
              return false;
          }
          if(row =='' || row <4){
              Materialize.toast('Please enter minimum 4 total rows', 2000,'rounded');
              return false;
          }
          return true;
    }
    var dclon;
    var id_rem= '';
    
    function allowDrop(ev) {
        ev.preventDefault();
    }
    var s =1 ,d=1,t=1,dr = 1;
    function drag(ev) {
        var id = $(ev).attr('id');
        if(id == 'seat_chair'){
            id = id+'_'+s;
            s++;
        }else if(id == 'steering'){
            id = id+'_'+d;
            d++;
        }else if(id == 'toilet'){
            id = id+'_'+t;
            t++;
        }else if(id == 'door'){
            id = id+'_'+dr;
            dr++;
        }
        id_rem = id;
//        ev.dataTransfer.setData("text", ev);
        dclon = $(ev).clone().attr({'class':'seat_chair_drag','id':id}).css({position: 'absolute',width:100,height:50});
//        console.log(ev);
    }

    function drop(ev) {
//        var element = $(ev._eventTarget);
        ev.preventDefault();
        var x = ev.toElement.offsetLeft;
        var y = ev.toElement.offsetTop;
        co = Math.round(ev.toElement.offsetLeft / ev.toElement.offsetWidth);
        ro = Math.round(ev.toElement.offsetTop / ev.toElement.offsetHeight);
        var lab = '';
//        var data = ev.dataTransfer.getData("text");
//        $(ev).attr('class','seat_chair_drag').appendTo("#layout_in");
        $("#layout_in").append(dclon);
        $('#'+id_rem).children().css({top:'0px'});
        if(dclon.find('.slable')){
            var l = getAlphaBats()[ro];
            dclon.children('.slable').css({top:'10px'});
            dclon.children('.slable').attr('onClick','setLable('+ro+','+co+',this.innerHTML)');
            dclon.find('.slable').html(l+co);
            lab = '-'+l+co;
        }
        TweenLite.to(dclon, 0, {
            left:x,
            top:y,
            delay:0.1,
            ease:Power2.easeInOut
        });
        var type = '';
            if(id_rem.indexOf('steering') !== -1){
                type = 'DS';
            }else if(id_rem.indexOf('door') !== -1){
                type = 'D';
            }else if(id_rem.indexOf('seat_chair') !== -1){
                type = 'CS-'+l+co;
                dclon.children('.slable').attr('id','seat-'+ro+'-'+co);
            }else if(id_rem.indexOf('toilet') !== -1){
                type = 'T';
            }
            routes[ro+'-'+co] = type;
        console.log(ro+'-'+co+':'+routes[ro+'-'+co]);
//        $("#layout_in").find(dclon).children('span').css({top:'0px;'});
        initDraggableComponent(dclon);
		
        dclon = '';
        id_rem = '';
//        ev.appendChild(data);
    }
    
    
    
    function initDraggableComponent(ele,left,top)
    {
        Draggable.create(ele, {
            bounds: $container,
            edgeResistance: 0.65,
            type: "x,y",
            throwProps: false,
            autoScroll: true,
//            liveSnap : true,
            liveSnap:{
                x: function(value) {
//                    console.log('left : '+left);
//                    if(typeof left !== 'undefined'){
//                        return left;
//                    }
                    return Math.round(value / gridWidth) * gridWidth;
                },
                y: function(value) {
//                    console.log('top : '+top);
//                    if(typeof top !== 'undefined'){
//                        return top;
//                    }
                    return Math.round(value / gridHeight) * gridHeight;
                }
            },
            snap: {
                x: function(endValue) {
                    return Math.round(endValue / gridWidth) * gridWidth;
                },
                y: function(endValue) {
                    return Math.round(endValue / gridHeight) * gridHeight;
                }
            },
            onDragStart(e){
                var element = $(this._eventTarget);
                var rc = eleRowCol(element);
                
                routes[rc.row+'-'+rc.col] = '';
                console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
            },
            onDrag:function(e) {
//                    console.log(this);
                highlight(e,this);
            },
            onThrowComplete: function(e) {
//                var element = $(this._eventTarget);
//                eleRowCol(element);
//                highlight(e,this,1);
            },
            onDragEnd:function(e) {
//                    console.log(this);
                var element = $(this._eventTarget);
                
                var rc = eleRowCol(element);
                var l = getAlphaBats()[rc.row];
                var lab = '';
                var ele_id = $(element).attr("id");
                var typ = '';
                if(ele_id.indexOf('steering') !== -1){
                    typ = 'DS';
                }else if(ele_id.indexOf('door') !== -1){
                    typ = 'D';
                }else if(ele_id.indexOf('seat_chair') !== -1){
                    typ = 'CS-'+l+rc.col;
                    element.find('.slable').html(l+rc.col);
                    element.children('.slable').attr('onClick','setLable('+rc.row+','+rc.col+',this.innerHTML)');
                    element.children('.slable').attr('id','seat-'+rc.row+'-'+rc.col);
                }else if(ele_id.indexOf('toilet') !== -1){
                    typ = 'T';
                }
                
                routes[rc.row+'-'+rc.col] = typ;
                var cell = {};
                console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
                highlight(e,this,1);
            },
        });
    }
    function eleRowCol(element){
        var element_indexid = $(element).attr("id");
        var position = $(element).position();
//        console.log(element);
//        console.log(position);

        var row = 0;
        var col = 0;

//                row = Math.round(position['left'] / gridWidth);
//                col = Math.round(position['top'] / gridHeight);
        col = Math.round(position['left'] / gridWidth);
        row = Math.round(position['top'] / gridHeight);

        var cell = {"row" : row,"col" : col};
        console.log("Cell");
        console.log(cell);
        return cell;
    }
    function highlight(e,obj,rem){
        var droppables = $(".seat_chair_drag");
                var l = droppables.length;
                var c = 0;
                while (--l > -1) {

                    if (Draggable.hitTest(droppables[l], e) && droppables[l] !== obj.target) {
                        
                        $(droppables[l]).addClass("highlights");
                        c++;
                        if(typeof rem !== 'undefined' && rem == 1){
                            $(droppables[l]).remove();
                        }
//                        $(droppables[l]).disable();
                    } else {
                        $(droppables[l]).removeClass("highlights");
                    }
                }
                
                return c;
    }
    
    $('#generate_layout').click(function(){
//          
          if(!chekLayout()){
              return false;
          }
            
            var row = $('#total_rows').val();
            var col = $('#total_columns').val();
            $("#layout_in").html('');
            routes ={};
            $container = $("#layout_in");
            gridWidth = 100;
            gridHeight = 50;
            gridRows = row;
            gridColumns = col;
            var i, x, y;

        //loop through and create the grid (a div for each cell). Feel free to tweak the variables above
            for (i = 0; i < gridRows * gridColumns; i++) {
                y = Math.floor(i / gridColumns) * gridHeight;
                x = (i * gridWidth) % (gridColumns * gridWidth);
                $("<div/>").css({position: "absolute", border: "1px solid #454545", width: gridWidth - 1, height: gridHeight - 1, top: y, left: x}).prependTo($container);
            }
            
            if(row > 4){
                if(row == 5){
                    g = 2
                }else{
                    g = 3;
                }
            }else {
                g =2;
            }
//            console.log(g);
            for(ro = 0;ro<row;ro++){
                y = ro * gridHeight;
//                console.log(ro+'-'+y);
                for(co = 0;co<col;co++){
                    x = (co * gridWidth) ;//% (gridColumns * gridWidth);
//                    console.log(ro+' - '+co+' - '+x);
                    if(ro == 1 && co == 0){
                        addElement('steering',ro,co,x,y);
                    }else if(ro == (row - 1) && co == 0){
                        addElement('door',ro,co,x,y);
                    }else if(ro == g ){
//                        console.log('entered');
                        if(co ==(col - 1)){
//                            console.log('in last col');
                            addElement('seat_chair',ro,co,x,y);
                        }
                    }
                    else if(co !=0){
                        addElement('seat_chair',ro,co,x,y);
                    }
                }
            }
            //set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
            TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
            TweenLite.set(".seat_chair_drag", {width: gridWidth, height: gridHeight, lineHeight: gridHeight + "px"});
            
            var overlapThreshold = "50%"; 
            
      });
      
    function addElement(id,ro,co,x,y,lbl){
        var ele = $('#'+id).clone().attr({'class':'seat_chair_drag','id':id+ro+'-'+co}).css({
                position: 'absolute',
                width:gridWidth - 1,
                height:gridHeight - 1
//                top: y, 
//                left: x
            });
            var lab = '';
            if(ele.find('.slable')){
                var l = getAlphaBats()[ro];
                ele.find('.slable').html(l+co);
                ele.children('.slable').attr('onClick','setLable('+ro+','+co+',this.innerHTML)');
                lab = '-'+l+co;
            }
            var type = '';
            if(id == 'steering'){
                type = 'DS';
            }else if(id == 'door'){
                type = 'D';
            }else if(id == 'seat_chair'){
                type = 'CS-'+l+co;
                if(typeof lbl !=='undefined'){
                    type = 'CS-'+lbl;
                    ele.find('.slable').html(lbl);
                }
                ele.children('.slable').attr('id','seat-'+ro+'-'+co);
            }else if(id == 'toilet'){
                type = 'T';
            }
            routes[ro+'-'+co] = type;
//            console.log(routes);
        $("#layout_in").append(ele);
        TweenLite.to(ele, 0, {
            left:x,
            top:y,
            delay:0.1,
            ease:Power2.easeInOut
        });
//                        $(ele).prependTo($container);
         
//        ele = ele.css({position: 'absolute'});
        initDraggableComponent(ele,x,y);
    }
    
    function removeElement(key){
        if(routes.hasOwnProperty(key)){
            delete routes[key];
        }
//        console.log(key);
//        console.log(routes);
    }
    function setLable(r,c,val){
        console.log(r+'-'+c+'-'+val);
        $('#modal-lable').openModal();
        $('#add-lname').val(val);
        $('#save_lable').attr('onClick','saveLable('+r+','+c+')');
    }
    
    function saveLable(r,c){
        var lbl= $('#add-lname').val();
        if(lbl == ''){
            Materialize.toast('Lable should not be blank !!', 2000,'rounded');
            return false;
        }
        $('#seat-'+r+'-'+c).html(lbl);
        routes[r+'-'+c] = 'CS-'+lbl;
        
        $('#modal-lable').closeModal();
    }
    $('#is_layout').click(function(){
        if($(this).is(':checked')){
            $('#display_layout').css({display:'inline'});
            $('#total_seats_div').css({display:'none'});
            $('#total_seats').removeAttr('required');
            $('#total_seats').removeAttr('data-parsley-min');
        }else{
            $('#display_layout').css({display:'none'});
            $('#total_seats_div').css({display:'inline'});
            $('#total_seats').attr('data-parsley-min',4);
        }
    });
    <?php
    if($bus['layout']){
//        dd($bus);
    ?>
    var layout = JSON.parse('<?php echo $body["layout"]?>');
    var cols = '<?php echo $bus["busLayout"][0]['columns']?>';
    var rows = '<?php echo $bus["busLayout"][0]['rows']?>';
//    console.log(layout);
//    console.log(cols);
//    console.log(rows);
    
    $container = $("#layout_in");
    gridWidth = 100;
    gridHeight = 50;
    gridRows = rows;
    gridColumns = cols;
    var i, x, y;

//loop through and create the grid (a div for each cell). Feel free to tweak the variables above
    for (i = 0; i < gridRows * gridColumns; i++) {
        y = Math.floor(i / gridColumns) * gridHeight;
        x = (i * gridWidth) % (gridColumns * gridWidth);
        $("<div/>").css({position: "absolute", border: "1px solid #454545", width: gridWidth - 1, height: gridHeight - 1, top: y, left: x}).prependTo($container);
    }
    
    for(i in layout){
        y = layout[i]['row'] * gridHeight;
        ro = layout[i]['row'];
//                console.log(ro+'-'+y);
//        for(co = 0;co<cols;co++){
        x = (layout[i]['col'] * gridWidth) ;//% (gridColumns * gridWidth);
        co = layout[i]['col'];
        var labl = layout[i]['seat_lbl'];
//                    console.log(ro+' - '+co+' - '+x);
        var ot = layout[i]['object_type'];
        var id_s = ot =='DS' ? 'steering' : ( ot == 'D' ? 'door' : ( ot == 'T' ? 'toilet' : 'seat_chair' ) );
            addElement(id_s,ro,co,x,y,labl);
//        }
    }
    //set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
    TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
    TweenLite.set(".seat_chair_drag", {width: gridWidth, height: gridHeight, lineHeight: gridHeight + "px"});
    
    <?php 
    }
    ?>
        $(document).ready(function(){
            $('#upload_csv').click(function(){
                $('#csv').click();
            });
            $('#csv').change(function(){
                var allow = 1;
                var ext = this.value.match(/\.(.+)$/)[1];
                switch (ext) {
                    case 'csv':
                    break;
                    default:
                        alert('please upload only csv file');
                        this.value = '';
                        allow = 0;
                }
                if(allow){
//                    console.log('file selected');
//                    $('#csv_route').submit();
                    $('#csv_route').ajaxForm(function(res){
//                        console.log(res);
                            Materialize.toast(res.msg, 3000,'rounded');
                        $('#csv').val('');
                        
                        if(typeof res.download !== 'undefined'){
                            var data = res.data;

                            $('<a></a>')
                            .attr('id','downloadFile')
                            .attr('href','data:text/csv;charset=utf8,' + encodeURIComponent(data))
                            .attr('download','routes.csv')
                            .appendTo('body');


                            $('#downloadFile').get(0).click();
                            $('#downloadFile').remove();   
                            return;
                        }
                        if(res.flag == 1){
                            location.reload();
                        }
                        
                    }).submit();
                }
            });
        });
        
     </script>
    <?php 
    
         }else{
         echo '<div class="col l3 m12"> No buses found. </div>';
         }
     }else{
         ?>
     <div class="col l3 m12"> No data found !!! </div>
             <?php
     }
    ?>

@endsection