 <!--
  Yay Sidebar
  Options [you can use all of theme classnames]:
    .yay-hide-to-small         - no hide menu, just set it small with big icons
    .yay-static                - stop using fixed sidebar (will scroll with content)
    .yay-gestures              - to show and hide menu using gesture swipes
    .yay-light                 - light color scheme
    .yay-hide-on-content-click - hide menu on content click

  Effects [you can use one of these classnames]:
    .yay-overlay  - overlay content
    .yay-push     - push content to right
    .yay-shrink   - shrink content width
-->

  <aside class="yaybar yay-shrink yay-hide-to-small yay-gestures">

    <div class="top">
      <div>
        <!-- Sidebar toggle -->
        <a href="#" class="yay-toggle">
          <div class="burg1"></div>
          <div class="burg2"></div>
          <div class="burg3"></div>
        </a>
        <!-- Sidebar toggle -->

        <!-- Logo -->
        <a href="#!" class="brand-logo">
          <img src="{{URL::to('assets/img/bus_image/logo2.png')}}" alt="Con">
        </a>
        <!-- /Logo -->
      </div>
    </div>


    <div class="nano">
        <div class="nano-content">

            <ul>

                <li class="label">Menu</li>

                <li id='Dashboard'>
                    <a href="{{URL::to('sp/dashboard')}}" class="waves-effect waves-blue"><i class="fa fa-dashboard"></i> Dashboards</a>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-ticket"></i> Ticket<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='tkt-book'>
                            <a href="{{URL::to('sp/ticket-book-new')}}" class="waves-effect waves-blue"> Book New</a>
                        </li>
                        <li id='tkt-list'>
                            <a href="{{URL::to('sp/ticket-list')}}" class="waves-effect waves-blue"> List</a>
                        </li>
                    </ul>
                </li>
                <li id='coupon'>
                    <a href="{{URL::to('sp/coupon-code')}}" class="waves-effect waves-blue"><i class="mdi-action-receipt"></i> Coupon Code</a>
                </li>
                
                <li id='admin-comm'>
                    <a href="{{URL::to('sp/commission-management')}}" class="waves-effect waves-blue"> <i class="mdi-action-assignment-ind"></i> Commission Management</a>
                </li>
                
                <li id='pay_req'>
                    <a href="{{URL::to('sp/payment-request')}}" class="waves-effect waves-blue"> <i class="fa fa-dollar"></i>Payment Management </a>
                </li>
<!--                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-device-dvr"></i> Service Provider<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='sp-A'>
                            <a href="{{URL::to('sp/service-provider/A')}}" class="waves-effect waves-blue"> Service Provider - A</a>
                        </li>
                        <li id='sp-B'>
                            <a href="{{URL::to('sp/service-provider/B')}}" class="waves-effect waves-blue"> Service Provider - B</a>
                        </li>
                    </ul>
                </li>-->
            </ul>

        </div>
    </div>
  </aside>
  <!-- /Yay Sidebar -->


  <!-- Main Content -->
  <section class="content-wrap">
