@extends('layouts.admin')
@section('header')
 
@stop
@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('admin/banner-filter',crnt,len,type,'','');
    };
    
    function delBanner(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Banner ??';
        var onyes ='delData';
        var param='\'admin/del-banner\','+id+' , \'admin/banner-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
 
     
    function addbnr(){
        
        $('#bnr_img').val('');
        $('#group').val('');
        $('#height').val('');
        $('#width').val('');
        $('#alt').val('');
        $('#red_url').val('');
        $('#group').material_select();
         $('#bnr_img').attr('required','required');
//        $('#height_type').val(0);
//        $('#width_type').val(0);
        $('#bnr_details').attr('action',"{{URL::to('admin/add-banner')}}");
        $('.file-path').val('');
        var chk = $('#status').is(':checked');
        if(!chk){
            $('#status').trigger('click');
        }
        
        $('#modal-banner').find('#save-banner').html('Add');
        $('#modal-banner').openModal();
        
//        $('#status').trigger('click');
        $('#bnr_details').parsley().reset();
    }
    function updatebnr(id){
        
        
        $('#bid').val(id);
        
        $('#group').val($('#grp-'+id).html());
        $('#height').val($('#h-'+id).attr('name'));
        $('#width').val($('#h-'+id).attr('name'));
        $('#alt').val($('#alt-'+id).html());
        $('#red_url').val($('#rto-'+id).html());
       
        $('#opr').val('Update');
        
        
        $('#group').val($('#grp-'+id).attr('name'));
        console.log($('#grp-'+id).attr('name'));
       $('#group').material_select(); 
        $('#bnr_details').attr('action',"{{URL::to('admin/edit-banner')}}");
        
        $('#bnr_img').removeAttr('required');
        $('#bnr_img').val(null);
        $('.file-path').val('');
        $('#modal-banner').find('#save-banner').html('Update');
        $('#modal-banner').openModal();
        
        var status = $('#status-'+id).val();
        var chk = $('#status').is(':checked');
        if(status == 1 && !chk){
            $('#status').trigger('click');
        }else if(status == 0 && chk){
            $('#status').trigger('click');
        }
        $('#bnr_details').parsley().reset();
    }
    
    $(document).on('click','#save-banner',function(){
        var file = $('#bnr_img').val();
        if($('#opr').val()!=='Update'){
            if(file == ''){
                Materialize.toast('Please select banner image.', 2000,'rounded');
                return false;
            }
        }
        var group = $('#group').val();
        if(group == ''){
            Materialize.toast('Please select banner group.', 2000,'rounded');
            return false;
        }
        $('#bnr_details').ajaxForm(function(res) {
           console.log(res);
           if(res.flag == 1){
               if($('#opr').val()==='Update'){
                   Materialize.toast('Banner Updated successfully !!', 2000,'rounded');
               }
               else{
                    Materialize.toast('Banner Added successfully !!', 2000,'rounded');
                }
                $('#modal-banner').closeModal();    
               filterData('admin/banner-filter');
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Banner</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Banner</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        
                        <select id="searchname" name="searchname" class="validate" onchange="return filterData('admin/banner-filter');">
                                <option value="" disabled selected>--Select Header--</option>
                                <option value="header-slider"> Web Homepage Header Slider</option>
                                <option value="web-mobile-footer-slider">Web Mobile Footer Slider</option>
                                <option value="passenger-app-slider">Passenger App Slider</option>
                                <option value="busway-web-slider">Busway Web slider</option>
                                <option value="passenger-app-promo-slider">Passenger App Promo slider</option>
                            </select>
                        <label for="searchname">Filter By Group</label>
                   </div>
                       
                   
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" onclick="return filterData('admin/banner-filter');"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Banner</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/banner-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/banner-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/banner-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/banner-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/banner-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/banner-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal Structure -->
       
        <div class="modal bust-modal" id="modal-banner">
         <form id="bnr_details" action="{{URL::to('admin/add-banner')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Banner</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                   
                    <div class="content">
                         
                        <div class="file-field input-field">
                            <div class="btn small">
                                <span class="medium">Banner</span>
                                <input type="file" name="bnr_img" id="bnr_img" accept="image/*" required="required">
                                <!--<label for="bnr_img">Upload Banner Image</label>-->
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <div class="input-field">
                            <select id="group" name="group" required>
                                <option value="" disabled selected>--Select Header--</option>
                                <option value="header-slider"> Web Homepage Header Slider</option>
                                <option value="web-mobile-footer-slider">Web Mobile Footer Slider</option>
                                <option value="passenger-app-slider">Passenger App Slider</option>
                                <option value="busway-web-slider">Busway Web slider</option>
                                <option value="passenger-app-promo-slider">Passenger App Promo slider</option>
                            </select>
                            <label for="group">Group</label>
                        </div>
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <input type="hidden" name="bid" id="bid" value="" />
                        <input type="hidden" name="opr" id='opr' value="Add" />
                        
                        <div class="input-field">
                            <input id="height" name="height" placeholder="" type="number" >
                            <label for="height">Height</label>
                        </div>
                        <div class="input-field">
                            <select id="height_type" name="height_type">
                                <!--<option value="" disabled selected>Select Height Type</option>-->
                                <option value="px">px</option>
                                <option value="%">%</option>
                                
                            </select>
                            <label for="height_type">Height Type</label>
                        </div>
                        <div class="input-field">
                            <input id="width" name="width" type="number" >
                            <label for="width">Width</label>
                        </div>
                        <div class="input-field">
                            <select id="width_type" name="width_type">
                                <!--<option value="" disabled selected>Select Width Type</option>-->
                                <option value="px">px</option>
                                <option value="%">%</option>
                            </select>
                            <label for="width_type">Width Type</label>
                        </div>
                        <div class="input-field">
                            <input id="alt" name="alt" type="text" placeholder="" required>
                            <label for="alt">Alter Text</label>
                        </div>
                        
                        <div class="input-field">
                            <input id="red_url" name="red_url" type="text" data-parsley-type="url" placeholder="" required>
                            <label for="red_url">Redirect To...</label>
                        </div>
                        
                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                    <input type="checkbox" id='status' name='status' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>
                                    
                                </label>
                            </p>
                        </div>
                        
                        
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
             </form>
        </div>
       
        
        <!-- Modal Structure Over-->
        
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
       
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="addbnr();"data-position="top" data-delay="50" data-tooltip="Add Service Provider">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>


@endsection
@section('footer')

        
@stop