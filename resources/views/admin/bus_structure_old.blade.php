@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        
    };
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>{{ $body['bus'] ? $body['bus']['name'] : 'Travelers' }}</h1>
                <ul>
                    <li><a href='{{URL::to($lt.'/service-provider')}}'><i class="fa fa-home"></i> Service Provider</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a href='{{URL::to($lt.'/service-provider/buses/'.$body['bus']['sp_id'])}}'>{{ $body['all_bus'] ? $body['all_bus']['first_name'].' '.$body['all_bus']['last_name'] : 'no service provider' }}</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a >Buses</a>  <i class='fa fa-angle-right'></i></li>
                    </li>
                    <li><a href=''>{{ $body['bus'] ? $body['bus']['name'] : 'no bus' }}</a> 
                </ul>
            </div>
            
        </div>

    </div>

     <!-- /Breadcrumb -->
     <?php if(isset($body['all_bus'])){
         
         if(isset($body['bus'])){
             $bus = $body['bus'];
         ?>
    <div class="row">
        <form id="edit_bus_form" action="{{URL::to($lt.'/edit-bus')}}" method="post" enctype="multipart/form-data" data-parsley-validate >
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="sp_id" value="{{$body['bus']['sp_id']}}" />
              <input type="hidden" name="bus_id" value="{{$body['bus']['id']}}" />
              <input type="hidden" name="seat_array" id="seat_array" value="" />
        <div class="col s12">
            <ul class="tabs" style="overflow: hidden">
            <li class="tab col s3"><a class="active addHash" href="#general">General</a></li>
            <li class="tab col s3"><a class="addHash" href="#routes">Routes</a></li>
            <li class="tab col s3"><a class="addHash" href="#layouts">Layouts</a></li>
            <li class="tab col s3"><a class="addHash" href="#coupon_code">Coupon Code</a></li>
          </ul>
        </div>
        <div class="fixed-action-btn" style="">
            <button id="save_bus" class="btn-floating btn-large green">Save</button>
        </div>
        <div id="general" class="col s12">
            <div class="row" style="display:inline;">
                <div class="col s12 m9 l10">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Bus Name : </div>
                    <div class="col s12 m4 l4">
                        <input id="bus_name" type="text" name="bus_name" value="{{ $bus['name'] }}" class="validate" placeholder="Bus Name" required>
                    </div>
                </div>
                <div class="col s12 m3 l2">
                    <div class="col s12 m4 l4"  style="padding: 0px;margin-top: 18px" >Available </div>
                    <div class="col s12 m1 l1">
                        <p class="switch">
                            <label>
                                <input type="checkbox" name="bus_status" id="bus_status" <?php echo $bus['status'] ? 'checked' : ''; ?> />
                              <span class="lever"></span>
                            </label>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12" style="display:inline;">
                    <!--<div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Drivers : </div>-->
                    <div class="col s12 m12 l12">
                        <div class="input-field">
                            <input name="driver" type="text" id="drivers" value="" class="validate" autocomplete="off">
                          <label for="input_text">Drivers</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                <ul class="collapsible"  id="coll-2" data-collapsible="accordion">
                <li class="active">
                    <div class="collapsible-header active"> <b>Driver List </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-2-ar" class="mdi-hardware-keyboard-arrow-up right small" ></i></a>
                    </div>
                    <div class="collapsible-body">
                        <table id="driver_list" class="default_padding table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th class="center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $dr = $body['drivers'];
                                if(count($dr) > 0){
                                    foreach($dr as $d){
                                        ?>
                                <tr>
                                    <td class="default_padding">{{$d['driver']['name']}} <input type="hidden" name="drivers[]" value="{{$d['driver']['id']}}" /> </td>
                                    <td class="default_padding">{{$d['driver']['mobile']}}</td>
                                    <td class="default_padding">{{$d['driver']['email']}}</td>
                                    <td class="default_padding">{{$d['driver']['address']}}</td>
                                    <td class="default_padding center"><i id="remove_d{{$d['driver']['id']}}" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear small" ></td>
                                </tr>    
                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </li>
                </ul>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Amenities : </div>
                    <div class="col s12 m12 l12">
                        <?php 
                        $amenities = $bus['amenities'];
//                        echo '<pre>';
//                        print_r($bus);
//                        echo '</pre>';
                        $a = '';
                        if(isset($amenities) && count($amenities)>0){
                            foreach($amenities as $am){
                                $a .=$am['aname'].','; 
                            }
                        }
                        ?>
                        <input id="amenities" type="text" name="amenities" value="{{$a}}" class="validate" >
                    </div>
                </div>
            </div>
            <div class="row" style="display:inline;">
                <div class="col s12 m12 l12">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Terms & Conditions : </div>
                    <div class="col s12 m12 l12">
                        
                        <textarea  id="tnc" name="tnc" class="materialize-textarea">{{$bus['tnc']}}</textarea>
                        <script>
                            CKEDITOR.replace( 'tnc' );
                        </script>
                    </div>
                </div>
            </div>
            
<!--            <ul class="collapsible"  id="coll-2" data-collapsible="accordion" style="display: none">
                <li class="active">
                    <div class="collapsible-header active"> <b>Blocking Dates </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-2-ar" class="mdi-hardware-keyboard-arrow-up right small" ></i></a>
                    </div>
                    <div class="collapsible-body">
                        
                        <table id="block_dates" class="table">
                            <thead>
                                <tr>
                                    <th colspan="6">Blocking Dates
                                        <a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $bdates = $bus['blockDates'];
//                                print_r($pricing);exit;
                                $dcnt = count($bdates);
                                if($dcnt == 0 || $dcnt ==1){
                                ?>
                              <tr>
                                <th class="br bl bb bt">1</th>
                                <td class="br bl bb bt">From : </td>
                                <td class="br bl bb bt">
                                    <input id="date_from_1" type="text" name="block_date[1][from]" value="{{ $dcnt ? date('Y-m-d',strtotime($bdates[0]['from_date'])) : '' }}"  data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="from date">
                                    <input type="hidden" name="block_date[1][id]" value="{{ $dcnt ? $bdates[0]['id'] : '' }}" />
                                </td>
                                <td class="br bl bb bt">To :</td>
                                <td class="br bl bb bt">
                                    <input id="date_to_1" type="text" name="block_date[1][to]" value="{{ $dcnt ? date('Y-m-d',strtotime($bdates[0]['to_date'])) : '' }}" date-diff="#date_from_1"  data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="date to">
                                </td>
                                <td class="br bl bt bb"><a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a></td>
                              </tr>
                              <?php
                                }else{
                                    $bitr = 1;
                                    foreach($bdates as $bd){
                                    ?>
                              <tr>
                                <th class="br bl bb bt">{{$bitr}}</th>
                                <td class="br bl bb bt">From : </td>
                                <td class="br bl bb bt">
                                    <input id="date_from_{{$bitr}}" type="text" name="block_date[{{$bitr}}][from]" value="{{ date('Y-m-d',strtotime($bd['from_date'])) }}"  data-parsley-data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="from date">
                                    <input type="hidden" name="date_from[{{$bitr}}][id]" value="{{$bd['id']}}" />
                                </td>
                                <td class="br bl bb bt">To :</td>
                                <td class="br bl bb bt">
                                    <input id="date_to_{{$bitr}}" type="text" name="block_date[{{$bitr}}][to]" value="{{ date('Y-m-d',strtotime($bd['to_date']))  }}" data-parsley-date-diff="#date_from_{{$bitr}}"  data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="date to">
                                </td>
                                <td class="br bl bt bb">
                                    <?php 
                                    if($bitr == 1){
                                    ?>
                                    <a id="add_date_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>
                                    <?php 
                                    }else{
                                        echo '<i id="remove_'.$bitr.'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
                                    }
                                    ?>
                                </td>
                              </tr>
                              <?php
                              $bitr++;
                                    }
                                }
                              ?>
                            </tbody>
                        </table>
                    </div>
                </li>
            </ul>-->
            
            
        </div>
        <div id="routes" class="col s12">
            <div class="row" style="display: inline">
                <div class="col s12 m9 l10">
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">From City : </div>
                    <div class="col s12 m4 l4">
                        <?php 
                        $city = $body['city'];
                        ?>
                        {{ Form::select('from_city', $city,'',['id'=>'from_city']) }}
                    </div>
                    <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">To City : </div>
                    <div class="col s12 m4 l4">
                        {{ Form::select('to_city',$city,'',['id'=>'to_city']) }}
                    </div>
                </div>
                <div class="col s12 m3 l2">
                    <div class="col s12 m4 l4"  style="padding: 0px;margin-top: 18px;" ><a id="add_route" class="btn btn-small default_color" style="color:white">Add</a> </div>
                </div>
            </div>
            <div id="all_routes">
                <?php
                    $routes = $body['routes'];
                    
                    $rcnt = count($routes);
                    $cnt = 0;
                    $ij = 0;
                    if($rcnt > 0){
                        $old_from = 0;
                        $old_to = 0;
                        foreach($routes as $r){
                            
                            if($old_from != $r['from_city_id'] || $old_to != $r['to_city_id']){
                                $cnt = 0;
                            }
                            if($cnt == 0){
                                $old_from = $r['from_city_id'];
                                $old_to = $r['to_city_id'];
                                
                                $name = explode(' - ', $r['name']);
                                $fname = $name[0];
                                $tname = $name[1];
                                $ter_from = \App\Models\Locality\Terminal::get_terminal_from_city($r['from_city_id']);
                                $ter_to = \App\Models\Locality\Terminal::get_terminal_from_city($r['to_city_id']);
                                $ft = $ter_from->lists('name','id');
                                $tt = $ter_to->lists('name','id');
                            
                        ?>
                        <ul class="collapsible" data-collapsible="accordion">
                            <li class="">
                                <div class="collapsible-header active"> <b> {{$r['name']}} </b>
                                            <a id="" onClick="removeRoute(this);" style="cursor: pointer" class="default_color"> <i class="mdi-content-clear right small" ></i></a>
                                </div>
                                <div class="collapsible-body">
                                    <table id="{{ $r['from_city_id'].'-'.$r['to_city_id'] }}" class="table">
                        
                                        <tbody>
                                            <tr>
                                                <th class="br bl bb bt"></th>
                                                <td class="br bl bb bt">From Terminal </td>
                                                <td class="br bl bb bt">
                                                    {{  Form::select('ter_from_'.$r['from_city_id'].'-'.$r['to_city_id'],$ft,'',['id'=>'ter_from_'.$r['from_city_id'].'-'.$r['to_city_id'],'class'=>'initialized']) }}
                                                </td>
                                                <td class="br bl bb bt">To Terminal</td>
                                                <td class="br bl bb bt">
                                                    {{  Form::select('ter_to_'.$r['from_city_id'].'-'.$r['to_city_id'],$tt,'',['id'=>'ter_to_'.$r['from_city_id'].'-'.$r['to_city_id'],'class'=>'initialized']) }}
                                                </td>
                                                <td class="br bl bt bb"><a onclick="addTerminal( {{ $r['from_city_id'] }} , {{ $r['to_city_id'] }} ,'{{$fname}}','{{$tname}}')" class="btn btn-small default_color" style="color:white">Add</a></td>
                                            </tr>
                                            <tr>
                                                <td  colspan="6" style="padding: 0px 80px;">
                                                    <table id="term_detail_{{ $r['from_city_id'].'-'.$r['to_city_id'] }}" class="center" >
                                                        <tbody>
                                                            <?php
                            }
//                            echo '<pre>';
//                            print_r($r['fromTerminal']);
//                            echo '</pre>';
                                                            $ter_from_name = $r['fromTerminal']['name'];
                                                           $ter_to_name = $r['toTerminal']['name'];
                                                           $btimem = $r['boarding_time'];
                                                           $dtimem = $r['droping_time'];
                                                           $min = $btimem % 60;
                                                           if(strlen($min) == 1){
                                                               $min = '0'.$min;
                                                           }
                                                           $bth = ($btimem - $min) / 60;
                                                           if(strlen($bth) == 1){
                                                               $bth = '0'.$bth;
                                                           }
                                                           $bt = $bth.':'.$min;
                                                           $dur = $dtimem - $btimem;
                            ?>
                                                        <tr>
                                                            <td>Terminal From :  <input type="hidden" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][main_route]" value="{{$fname}} - {{$tname}}"  />
                                                                <input type="hidden" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][id]" value="{{$r['id']}}"  />
                                                            </td>
                                                            <td class="default_color">{{$ter_from_name}}</td>
                                                            <td>Terminal To : <input type="hidden" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][ter_route]" value="{{$fname}} - {{$ter_from_name}} - {{$ter_to_name}} - {{$tname}}"  /></td>
                                                            <td class="default_color">{{$ter_to_name}} <input type="hidden" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][ter_from]" value="{{$r['from_terminal_id']}}"  /> <input type="hidden" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][ter_to]" value="{{$r['to_terminal_id']}}"  /></td>
                                                            <td>Boarding Time</td>
                                                            <td><input id="bt_{{$r['from_terminal_id']}}-{{$r['to_terminal_id']}}" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][boarding]" required class="clockpicker" data-inputmask="'mask': 'h:s'" data-autoclose="true" type="text" value="{{$bt}}" data-donetext="OK"  data-placement="" data-align=""></td>
                                                            <td>Duration</td>
                                                            <td><input id="dur_{{$r['from_terminal_id']}}-{{$r['to_terminal_id']}}" name="terminal[{{$r['from_city_id']}}-{{$r['to_city_id']}}][{{$cnt+1}}][duration]" required data-parsley-type="integer" class="validation" type="number" value="{{$dur}}" placeholder="Duration in minute" ></td>
                                                            <td><i id="remove_{{$cnt+1}}" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" ></i></td>
                                                        </tr>
                              <?php  
                              if(isset($routes[$ij+1]) && ($routes[$ij+1]['from_city_id'] != $r['from_city_id'] || $routes[$ij+1]['to_city_id'] != $r['to_city_id'])){
                                  ?>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>

                        </ul>
                        <?php
                            }else if(!isset($routes[$ij+1])){
                                ?>
                                                    </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>

                        </ul>
                        <?php
                            }
//                            echo $cnt.'<br>';
                            $ij++;
                        $cnt++;
                            
                        }
                    } 
                ?>
            </div>
        </div>
        <div id="layouts" class="col s12">
            
            <div class="row" style="display: inline;" >
                <div class="col s12 m34 l4"></div>
                <div class="col s12 m2 l2">
                  <p class="switch">
                                <label>
                                     Layout 
                                    <input type="checkbox" name="is_layout" id="is_layout" <?php echo $bus['layout'] ? 'checked':'' ?> />
                                  <span class="lever"></span>
                                </label>
                            </p>
                            
                </div>
                <?php
                $dis = $bus['layout'] ? 'none' : 'inline';
                $req = $bus['layout'] ? '' : 'required data-parsley-min="4"' ;
                ?>
                <div class="col s12 m2 l2" id="total_seats_div" style="display: {{$dis}}" >
                    <div class="input-field">
                        <input id="total_seats" name="total_seats" type="number" <?php echo $req ?> class="validate" value="{{$bus['seat_capacity']}}" >
                      <label for="input_text">Numbers of Seats</label>
                    </div>
                </div>
            </div>
            
            <?php
            $l = $bus['layout'] ? 'inline' : 'none' ;
            
            ?>
            <div class="row" id="display_layout" style="display: {{$l}}">
                
            <div class="col s12 m1 l1">
                <ul class="collection">
                    <li class="collection-item "  style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding seat_chair"  id="seat_chair" style="cursor: pointer;" draggable="true" ondragstart="drag(this)" >
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <span style="position: absolute;top: -5px;left: 35px;color: green" class="slable" onclick="" ></span>
                            <img src="{{URL::to('assets/images/layout_icons/car-seat.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Seat Chair
                        </div>
                    </li>
<!--                    <li class="collection-item " style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <img src="{{URL::to('assets/images/layout_icons/sofa1.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Seat Sleeping
                        </div>
                    </li>-->
                    <li class="collection-item " style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding steering" id="steering" style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/steering.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding driver" id="driver">
                            Driver
                        </div>
                    </li>
                    <li class="collection-item " style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding toilet" id="toilet"  style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/toilet.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Toilet
                        </div>
                    </li>
                    <li class="collection-item " style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding door" id="door" style="cursor: pointer" draggable="true" ondragstart="drag(this)">
                            <span style="position: absolute;top: -15px;left: 80px;color: red" onclick="removeSeat(this);" >x</span>
                            <img src="{{URL::to('assets/images/layout_icons/door.png')}}" style="height: 40px;width: 40px" />
                        </div>
                        <div class="col s12 default_padding">
                            Door
                        </div>
                    </li>
                </ul>
                
            </div>
            <div id="layout" class="col s12 m8 l8" style="text-align: center;">
                <div id="layout_in" style="position: relative" ondrop="drop(event)" ondragover="allowDrop(event)">
                    
                </div>
                
            </div>
                <?php
//                print_r($bus['busLayout']);exit;
                ?>
            <div class="col s12 m2 l2">
                <ul class="collection">
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <div class="input-field">
                                <input id="total_columns" name="total_columns" type="number" value="<?php echo isset($bus['busLayout'][0]) ? $bus['busLayout'][0]['columns'] :'' ?>" class="validate" >
                              <label for="input_text">Columns</label>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding">
                            <div class="input-field">
                                <input id="total_rows" name="total_rows" type="number" value="<?php echo  isset($bus['busLayout'][0])?$bus['busLayout'][0]['rows']:'' ?>" class="validate" >
                              <label for="input_text">Rows</label>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item" style="display: inline-block;width: 100%">
                        <div class="col s12 default_padding" ><a id="generate_layout" class="btn btn-small default_color" style="color:white">Generate</a> </div>
                    </li>
                </ul>
            </div>
            </div>
            
            <ul class="collapsible"  id="coll-1" data-collapsible="accordion">
                <li  class="active">
                    <div class="collapsible-header active"> <b>Price Scheme </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-1-ar" class="mdi-hardware-keyboard-arrow-up right small" ></i></a>
                    </div>
                    <div class="collapsible-body">
                        <table id="pricing" class="table">
<!--                            <thead>
                                <tr>
                                    <th colspan="6">Price Scheme
                                        <a id="add_price_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>
                                    </th>
                                </tr>
                            </thead>-->
                            <tbody>
                                <?php
//                                print_r($bus);
                                $pricing = $bus['busPrice'];
//                                print_r($pricing);exit;
                                $pcnt = count($pricing);
                                if($pcnt == 0 || $pcnt ==1){
                                ?>
                              <tr>
                                <th class="br bl bb bt">1</th>
                                <td class="br bl bb bt">From Seat : </td>
                                <td class="br bl bb bt">
                                    <input id="seat_count_1" type="number" name="price[1][from_seat_count]" value="<?php echo $pcnt ? $pricing[0]['from_seat'] : '' ?>" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                    <input type="hidden" name="price[1][id]" value="<?php echo $pcnt ? $pricing[0]['id'] : '' ?>" />
                                </td>
                                <td class="br bl bb bt">To Seat : </td>
                                <td class="br bl bb bt">
                                    <input id="seat_count_1" type="number" name="price[1][to_seat_count]" value="<?php echo $pcnt ? $pricing[0]['to_seat'] : '' ?>" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                    <input type="hidden" name="price[1][id]" value="<?php echo $pcnt ? $pricing[0]['id'] : '' ?>" />
                                </td>
                                <td class="br bl bb bt">Price :</td>
                                <td class="br bl bb bt">
                                    <input id="price_1" type="text" name="price[1][price]" value="<?php echo $pcnt ? (int)$pricing[0]['price'] : '' ?>" data-parsley-type="integer" required class="validate" placeholder="price">
                                </td>
                                <td class="br bl bt bb"><a id="add_price_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a></td>
                              </tr><?php
                                }else{
                                    $itr = 1;
                                    foreach($pricing as $pr){
                                        ?>
                              <tr>
                                <th class="br bl bb bt">{{$itr}}</th>
                                <td class="br bl bb bt">From Seat : </td>
                                <td class="br bl bb bt">
                                    <input id="seat_count_{{$itr}}" type="number" name="price[{{$itr}}][from_seat_count]" value="{{ $pr['from_seat'] }}" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                    <input type="hidden" name="price[{{$itr}}][id]" value="{{$pr['id'] }}" />
                                </td>
                                <td class="br bl bb bt">To Seat : </td>
                                <td class="br bl bb bt">
                                    <input id="seat_count_{{$itr}}" type="number" name="price[{{$itr}}][to_seat_count]" value="{{ $pr['to_seat'] }}" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                    <input type="hidden" name="price[{{$itr}}][id]" value="{{$pr['id'] }}" />
                                </td>
                                <td class="br bl bb bt">Price :</td>
                                <td class="br bl bb bt">
                                    <input id="price_{{$itr}}" type="text" name="price[{{$itr}}][price]" value="{{ (int)$pr['price']}}" data-parsley-type="integer" class="validate" required placeholder="price">
                               </td>
                                <td class="br bl bt bb">
                                    <?php 
                                    if($itr == 1){
                                    ?>
                                    <a id="add_price_scheme" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a>
                                    <?php 
                                    }else{
                                        echo '<i id="remove_'.$itr.'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
                                    }
                                    ?>
                                </td>
                              </tr>
                              <?php
                              $itr++;
                                    }
                                }
                              ?>
                            </tbody>
                        </table>
                    </div>
                    
                </li>
            </ul>
        </div>
     
     <div id="coupon_code" class="col s12">
         <div class="col s12">
             <a href="#add_coupon_code_div" class="btn right" onclick="addcc();">Add Coupon Code</a>
        </div>
         <table class="table-bordered">
             <thead>
                 <tr>
                     <th>Status</th>
                     <th>Code</th>
                     <th>Start Date</th>
                     <th>End Date</th>
                     <th>Type</th>
                     <th>Value</th>
                     <th>Minimum Amount</th>
                     <th>Max Amount</th>
                     <th>Action</th>
                 </tr>
             </thead>
             <tbody>
                 <?php
                 $ccode = $bus['couponCode'];
                 if(count($ccode) > 0){
                     foreach($ccode as $cd){
                         ?>
                 <tr>
                     <td>
                         <input type="hidden" id="status-{{$cd['id']}}" value="{{$cd['status']}}" />
                         <?php echo $cd['status'] == 1 ? '<i style="color:green">Active<i>' : '<i style="color:red">Inactive</i>' ; ?></td>
                     <td><span id='ccode-{{ $cd['id'] }}'>{{$cd['code']}}</span></td>
                     <td><span id='csdate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['start_date']))}}</span></td>
                     <td><span id='cedate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['end_date']))}}</span></td>
                     <td><span id='ctype-{{ $cd['id'] }}' name="{{$cd['type']}}" >{{$cd['type'] == 'P'?'Percentage':'Fix'}}</span></td>
                     <td><span id='cvalue-{{ $cd['id'] }}'>{{(int)$cd['value']}}</span></td>
                     <td><span id='cmin-{{ $cd['id'] }}'>{{(int)$cd['min_amount']}}</span></td>
                     <td><span id='cmax-{{ $cd['id'] }}'>{{(int)$cd['max_amount']}}</span></td>
                     <td>
                         <a class="btn-floating btn-small blue" id="" onclick="editcc( {{$cd['id']}} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                        <a class="btn-floating btn-small red btn " id= "" onclick="delCouponCode( {{ $cd['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                     </td>
                 </tr>
                 <?php
                     }
                 }
                 ?>
             </tbody>
         </table>
     </div>
        </form>
    </div>
    
    <form id="add_coupon_code" action="{{URL::to($lt.'/add-coupon-code')}}" method="post" enctype="multipart/form-data" data-parsley-validate >
        <div id="add_coupon_code_div" class="modal bust-modal" style="width: 35%;font-size: 100%;overflow: hidden;">
            <div class="modal-content">
              <h4>Add Coupon Code</h4>
              <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="bus_id" value="{{$body['bus']['id']}}" />
              <input type="hidden" name="ccid" id="ccid" value="" />
              <input type="hidden" name="opr" id="opr" value="" />
              <div class="input-field">
                  <input id="ccode" type="text" name="ccode" class="validate" required="" placeholder="" >
                <label for="input_text">Coupon Code</label>
              </div>
              
              <div class="input-field">
                  <input id="csdate" type="text" name="csdate" data-inputmask="'mask': 'y-m-d'" required class="validate pikaday" placeholder="" >
                <label for="input_text">Start Date</label>
              </div>
              
              <div class="input-field">
                  <input id="cedate" type="text" name="cedate" data-inputmask="'mask': 'y-m-d'" required data-parsley-date-diff="#csdate" class="validate pikaday" placeholder="" >
                <label for="input_text">End Date</label>
              </div>
              <div class="row">
                  <div class="col s12 m6 l6">
                  <div class="input-field">
                    <input id="cvalue" type="number" name="cvalue" required="" placeholder="" class="validate" >
                  <label for="input_text">Coupon Value</label>
                </div>
              </div>
              
              <div class="col s12 m6 l6">
                  <div class="input-field">
                    <select id="ctype" name="ctype" required="">
                      <option value="" disabled selected>Select Type</option>
                      <option value="P">Percentage</option>
                      <option value="F">Fix</option>
                    </select>
                        <label for="input_text">Coupon Type</label>
              </div>
              </div>
              
              </div>
              <div class="input-field">
                  <input id="cminvalue" name="cminvalue" type="number"  placeholder=""class="validate" required="">
                <label for="input_text">Minimum Value ( Coupon Applied if ticket price is minimum of this amount )</label>
              </div>
              <div class="input-field">
                  <input id="cmaxvalue" name="cmaxvalue" type="number" placeholder="" class="validate" required="">
                <label for="input_text">Maximum Discount amount</label>
              </div>
              <div class="input-field">
                  <p class="switch">
                        <label>
                            Status
                            <input id="cstatus" name="cstatus" type="checkbox" />
                          <span class="lever"></span>
                          
                        </label>
                      </p>
              </div>
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
              <!--<a id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</a>-->
              <button id="save_ccode"  class="waves-effect waves-green btn-flat ">Save</button>
            </div>
          </div>
        </form>
    
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
    
    <!-- Modal Structure -->
        <div id="modal-lable" class="modal" style='width:35%;font-size:100%;max-height: 100%;'>
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>Lable Detail</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <div class="input-field">
                        <input id="add-lname" name="add-lname" type="text" placeholder="lable Name" required>
                        <label for="add-cname">Lable Name</label>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='save_lable' class="modal-action  waves-effect waves-green btn-flat ">Edit</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
    
     <script>
         $('.addHash').click(function(){
             var has = $(this).attr('href');
             location.hash = has;
         });
         function delCouponCode(id){
        
            var title='<b>Confirmation<b>';
            var msg='Are you sure to Delete Coupon Code ??';
            var onyes ='deleteCode';
            var param='\'{{URL::to($lt."/delete-coupon-code")}}\','+id;
            var mdlname='confirmDel';
            modalOpen(mdlname,title,msg,onyes,param);
        }
        function deleteCode(url,id){
//            url = '{{URL::to('+url+')}}';
            data={id:id};
            postAjax(url,data,function(res){
                if(res.flag == 1){
                    Materialize.toast(res.msg, 2000,'rounded');
                    location.reload();
                }else{
                    Materialize.toast(res.msg, 2000,'rounded');
                }
            });
        }
        function addcc(){
            $('#ccode').val('');
            $('#csdate').val('');
            $('#cedate').val('');
            $('#ctype').val('');
            $('#cvalue').val('');
            $('#cminvalue').val('');
            $('#cmaxvalue').val('');
            $('#ctype').material_select();
            $('#opr').val('Add');

            $('#add_coupon_code').attr('action',"{{URL::to($lt.'/add-coupon-code')}}");

            $('#add_coupon_code_div').find('#save_ccode').html('Add');
            $('#add_coupon_code_div').openModal();
            $('#status').trigger('click');
            $('#add_coupon_code').parsley().reset();
        }

        function editcc(id){

            $('#ccode').val($('#ccode-'+id).html());
            $('#csdate').val($('#csdate-'+id).html());
            $('#cedate').val($('#cedate-'+id).html());
            $('#ctype').val($('#ctype-'+id).attr('name'));
            $('#cvalue').val($('#cvalue-'+id).html());
            $('#cminvalue').val($('#cmin-'+id).html());
            $('#cmaxvalue').val($('#cmin-'+id).html());
            $('#ccid').val(id);
            $('#opr').val('Update');
            $('#ctype').material_select();
            $('#add_coupon_code').attr('action',"{{URL::to($lt.'/edit-coupon-code')}}");

            $('#add_coupon_code_div').find('#save_ccode').html('Update');
            $('#add_coupon_code_div').openModal();
            
            var status = $('#status-'+id).val();
            var chk = $('#cstatus').is(':checked');
            if(status == 1 && !chk){
                $('#cstatus').trigger('click');
            }else if(status == 0 && chk){
                $('#cstatus').trigger('click');
            }

            $('#add_coupon_code').parsley().reset();
        }
         $(document).on('click','#save_ccode',function(){
             $('#add_coupon_code').ajaxForm(function(res) {
                if(res.flag == 1){
                    if($('#opr').val() == 'Update'){
                        Materialize.toast('Coupon Code updated successfully !!', 2000,'rounded');
                    }else{
                        Materialize.toast('Coupon Code added successfully !!', 2000,'rounded');
                    }
                    location.reload();
                }else{
                    Materialize.toast(res.msg, 2000,'rounded');
                }
            });
         });
         $(document).ready(function(){
             
             window.Parsley.addValidator('dateDiff', {
              validateString: function(value, id) {
//                return 0 === value % requirement;
                var from_date = $(id).val();
                var to_date = value;
                from_date = from_date.split('-');
                new_date = new Date(from_date[0], from_date[1], from_date[2]);
                date1 = to_date.split('-');
                new_date1 = new Date(date1[0], date1[1], date1[2]);
                if(new_date1 < new_date){
                    return false;
                }
              },
              messages: {
                en: 'the to date must be after from date.'
              }
            });
             
             var amenities = {};
             
                 var url ='{{URL::to($lt."/get-amenites")}}';
                 var data = {};
                 postAjax(url,data,function(res){
                     if(typeof res.flag != 'undefined' && res.flag !=1){
                         Materialize.toast('No amenities found', 2000,'rounded');
                     }
                     amenities = res.data;
//                     $('#amenities').autocomplete({
//                        data: amenities,
//                        limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
//                        onAutocomplete: function(val,key) {
//                            console.log('id : '+key);
//                            $('#amenities').tagsInput();
//                          // Callback function when value is autcompleted.
//                        },
//                        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
//                    });
                    
                    $('#amenities').tagsInput({
//                        'autocomplete_url':url,
                        'autocomplete_data':amenities,
//                        'autocomplete_element':'amenities',
                        'height':'auto',
                        'width':'auto',
                        'autocomplete': {selectFirst:false,width:'auto',autoFill:true},
                        'interactive':true,
                        'defaultText':'type here..',
                        'onAddTag':function(){
                            
                        },
                        'onTagExist':function(res){
//                            console.log('exist');
                            Materialize.toast('This amenity '+res+' is already exist', 2000,'rounded');
                        },
                        'onRemoveTag':function(res){
//                            Materialize.toast('The amenity '+res+' is removed', 2000,'rounded');
                        },
//                        'onChange' : callback_function,
//                        'delimiter': [',',';'],   // Or a string with a single delimiter. Ex: ';'
                        'removeWithBackspace' : true,
                        'minChars' : 0,
                        'maxChars' : 0, // if not provided there is no limit
                        'placeholderColor' : '#666666'
                     });
                     $('.tagsinput').css({border:'none'});
                });
             var flag = 0;
            $('#drivers').keyup(function(){
                var str = $(this).val();
                if(str.length < 2){
                    flag = 1;
                    return false;
                }
                var url = '{{URL::to($lt."/drivers")}}';
                var data = {name:str};
                if(flag == 1){
                    postAjax(url,data,function(res){
                        if(res.flag == 1){
                            flag = 0;
                            var d = res.data;
                            $('#drivers').autocomplete({
                                data: d,
                                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                                onAutocomplete: function(val,key) {
                                    var exist = $('#driver_list').find('#remove_d'+key).length;
                                    if(exist){
                                        Materialize.toast('Driver already exist !!', 2000,'rounded');
                                        return false;
                                    }
                                    data = {id:key,all:1};
                                    url = '{{URL::to($lt."/add-driver-to-bus")}}';
                                    if(key > 0){
                                        addDriver(url,data);
                                    }
                                },
                                minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                            });
                        }
                    });
                }
            });
                
                
         });
        
        function addDriver(url,data){
            postAjax(url,data,function(res){
                $('#driver_list tbody').append(res);
            });
        }
        
        $('#coll-1').click(function(){
            if($(this).children().hasClass('active')){
                $('#coll-1-ar').removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-up');
            }else{
                $('#coll-1-ar').removeClass('mdi-hardware-keyboard-arrow-up').addClass('mdi-hardware-keyboard-arrow-down');
            }
        });
        $('#coll-2').click(function(){
            if($(this).children().hasClass('active')){
                $('#coll-2-ar').removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-up');
            }else{
                $('#coll-2-ar').removeClass('mdi-hardware-keyboard-arrow-up').addClass('mdi-hardware-keyboard-arrow-down');
            }
        });
         $('#add_price_scheme').click(function(){
             
             var t = $('#pricing tr:last').children('th').html();
//             var t = $('#pricing tr').length;
             t = parseInt(t)+1;
             var rm = '<i id="remove_'+t+'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
             console.log($('#pricing').find('#remove').length);
             
             var tr = '<tr>'+
                    '<th class="br bl bb bt">'+t+'</th>'+
                    '<td class="br bl bb bt">from Seat : </td>'+
                    '<td class="br bl bb bt"><input id="seat_count_'+t+'" type="number" name="price['+t+'][from_seat_count]" value="" required class="validate" data-parsley-type="integer" placeholder="seat count"><input type="hidden" name="price['+t+'][id]" /></td>'+
                    '<td class="br bl bb bt">To Seat : </td>'+
                    '<td class="br bl bb bt"><input id="seat_count_'+t+'" type="number" name="price['+t+'][to_seat_count]" value="" required class="validate" data-parsley-type="integer" placeholder="seat count"><input type="hidden" name="price['+t+'][id]" /></td>'+
                    '<td class="br bl bb bt">Price :</td>'+
                    '<td class="br bl bb bt"><input id="price_'+t+'" type="text" name="price['+t+'][price]" data-parsley-type="integer" value="" required class="validate" placeholder="price"></td>'+
                    '<td class="br bl bt bb">'+rm+'</td>'+
                  '</tr>';
          $('#pricing tbody').append(tr);

         });
         function removeTr(obj){
             $(obj).closest('tr').remove();
         }
        
         $('#add_date_scheme').click(function(){
             var t = $('#block_dates tr:last').children('th').html();
//             var t = $('#block_dates tr').length;
             t = parseInt(t)+1;
             var rm = '<i id="remove_'+t+'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
             console.log($('#block_dates').find('#remove_d').length);
             
             var tr = '<tr>'+
                    '<th class="br bl bb bt">'+t+'</th>'+
                    '<td class="br bl bb bt">From : </td>'+
                    '<td class="br bl bb bt"><input id="date_from_'+t+'" type="text" name="block_date['+t+'][from]" value="" required class="validate pikaday"  data-inputmask="\'mask\': \'y-m-d\'" placeholder="from date"><input type="hidden" name="block_date['+t+'][id]" /></td>'+
                    '<td class="br bl bb bt">To :</td>'+
                    '<td class="br bl bb bt"><input id="date_to_'+t+'" type="text" name="block_date['+t+'][to]" value="" required class="validate pikaday" data-parsley-date-diff="#date_from_'+t+'" data-inputmask="\'mask\': \'y-m-d\'" placeholder="to date"></td>'+
                    '<td class="br bl bt bb">'+rm+'</td>'+
                  '</tr>';
          $('#block_dates tbody').append(tr);
          $('.pikaday').pikaday();
          $('.pikaday').inputmask();
          $('.pikaday').parsley();
      });
      $('#add_route').click(function(){
          var from = $('#from_city :selected').val();
          var to = $('#to_city :selected').val();
          if(from == 0){
              Materialize.toast('please select from city', 2000,'rounded');
              return false;
          }else if(to == 0){
              Materialize.toast('please select to city', 2000,'rounded');
              return false;
          }
          var l = $('#routes').find('#'+from+'-'+to).length;
          if(l > 0){
              Materialize.toast('This route is already set.', 2000,'rounded');
              return false;
          }
          var url = '{{URL::to($lt."/get-terminals")}}';
          var from_name = $('#from_city :selected').text();
          var to_name = $('#to_city :selected').text();
          var data = {from_id : from,to_id:to,from_name:from_name,to_name:to_name};
          postAjax(url,data,function(data){
//              console.log(data);
              if( typeof data.flag != 'undefined' && data.flag != 1){
                  Materialize.toast(data.msg, 2000,'rounded');
                  return false;
              }
              
              $('.collapsible li .collapsible-header').removeClass('active');
              $('#all_routes').append(data);
              $('.collapsible').collapsible();
//              $('select').material_select('destroy');
                console.log('#'+from+'-'+to+' select');
              $('#'+from+'-'+to+' select').material_select();
          });
      });
      /*$(document).on('click','#all_routes .collapsible .collapsible-header',function(){
        var a = 0;
        if($(this).hasClass('active') === 1){
           a = 1; 
        }
        $('#all_routes .collapsible li .collapsible-header').removeClass('active');  
        if(!a){
//            $(this).removeClass('active');
            $(this).addClass('active');
        }else{
            $(this).removeClass('active');
//            $(this).addClass('active');
        }
        $('#all_routes .collapsible').collapsible();
      });*/
      function removeRoute(obj){
          $(obj).closest('ul').remove();
//          $(obj).closest('table').remove();
      }
      
      function addTerminal(from,to,from_name,to_name){
//          var t = $('#term_detail_ tr:last').children('th').html();
             var t = $('#term_detail_'+from+'-'+to+' tr').length;
             t = parseInt(t)+1;
          var rm = '<i id="remove_'+t+'" style="cursor:pointer" onClick="removeTr(this)" class="mdi-content-clear right small" >';
          
          var ter_from = $('#ter_from_'+from+'-'+to+' :selected' ).val();
          var ter_to = $('#ter_to_'+from+'-'+to+' :selected' ).val();
          var ter_from_name = $('#ter_from_'+from+'-'+to+' :selected' ).text();
          var ter_to_name = $('#ter_to_'+from+'-'+to+' :selected' ).text();
          
          var exist  = $('#term_detail_'+from+'-'+to).find('#bt_'+ter_from+'-'+ter_to).length;
          console.log(exist);
          
          if(ter_from == ter_to){
              Materialize.toast('Same Terminals cannot be added.', 2000,'rounded');
              return false;
          }
          
          if(exist > 0){
              Materialize.toast('Terminal already exist', 2000,'rounded');
              return false;
          }
          
          var terminal = '<tr>'+
                            '<td>Terminal From :  <input type="hidden" name="terminal['+from+'-'+to+']['+t+'][main_route]" value="'+from_name+' - '+to_name+'"  /></td>'+
                            '<td class="default_color">'+ter_from_name+'</td>'+
                            '<td>Terminal To : <input type="hidden" name="terminal['+from+'-'+to+']['+t+'][ter_route]" value="'+from_name+' - '+ter_from_name+' - '+ter_to_name+' - '+to_name+'"  /></td>'+
                            '<td class="default_color">'+ter_to_name+' <input type="hidden" name="terminal['+from+'-'+to+']['+t+'][ter_from]" value="'+ter_from+'"  /> <input type="hidden" name="terminal['+from+'-'+to+']['+t+'][ter_to]" value="'+ter_to+'"  /></td>'+
                            '<td>Boarding Time</td>'+
                            '<td><input id="bt_'+ter_from+'-'+ter_to+'" name="terminal['+from+'-'+to+']['+t+'][boarding]" required class="clockpicker" data-inputmask="\'mask\': \'h:s\'" data-autoclose="true" type="text" value="" data-donetext="OK" data-placement="" data-align=""></td>'+
                            '<td>Duration</td>'+
                            '<td><input id="dur_'+ter_from+'-'+ter_to+'" name="terminal['+from+'-'+to+']['+t+'][duration]" required class="validation" type="number" data-parsley-type="integer" value="" placeholder="Duration in minute" ></td>'+
                            '<td>'+rm+'</td>'+
                        '</tr>';
//                '<input type="hidden" name="terminal['+t+'][from_city]" value="'+from+'"  /> <input type="hidden" name="terminal['+t+'][to_city]" value="'+to+'"  />';
                $('#term_detail_'+from+'-'+to+ ' tbody').append(terminal);
                
                $('.clockpicker').clockpicker();
                $('.clockpicker').inputmask();
      }
      
      $(document).on('click','.clockpicker',function(){
          
//        $(this).clockpicker('remove');
//        $(this).clockpicker();
        $(this).clockpicker();
      });
      
//      $('.clockpicker').each(function(){
//          console.log('onload each clock picker');
//        $(this).clockpicker('remove');  
//      });
var routes = {};
      $(document).on('click','#save_bus',function(){
        var bus_name = $('#bus_name').val();
        var total_seats = $('#total_seats').val();
//        return false;
//        var bus_type = $('#bus_type :selected').val();
        if(bus_name == ''){
            Materialize.toast('Bus name should not be blank !!', 2000,'rounded');
            return false;
        }
//        console.log(routes);
//        console.log(JSON.stringify(routes));
        $('#seat_array').val(JSON.stringify(routes));
//        $("#layout_in").append(routes);
//        console.log($('#seat_array').val());
//        console.log($('#seat_array').length);
        if($('#is_layout').is(':checked')){
            if($('#seat_array').val() == ''){
                Materialize.toast('Please add seats in bus layout !!', 2000,'rounded');
                return false;
            }
            
        }
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
//        else if(bus_type == ''){
//            Materialize.toast('Please select bus type !!', 2000,'rounded');
//            return false;
//        }
        $('#edit_bus_form').ajaxForm(function(res) {
           console.log(res);
           if(res.flag == 1){
               Materialize.toast('Bus edited successfully !!', 2000,'rounded');
               location.reload();
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });
   
    function removeSeat(obj){
        var ele = $(obj).parent();
        var rc = eleRowCol(ele);
        routes[rc.row+'-'+rc.col] = '';
        console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
        $(obj).parent().remove();
        removeElement(rc.row+'-'+rc.col);
//        $('#generate_layout').trigger('click');
    }
    function chekLayout(){
        var row = $('#total_rows').val();
          var col = $('#total_columns').val();
          
          if(col =='' || col <2){
              Materialize.toast('Please enter minimum 2 total columns', 2000,'rounded');
              return false;
          }
          if(row =='' || row <4){
              Materialize.toast('Please enter minimum 4 total rows', 2000,'rounded');
              return false;
          }
          return true;
    }
    var dclon;
    var id_rem= '';
    
    function allowDrop(ev) {
        ev.preventDefault();
    }
    var s =1 ,d=1,t=1,dr = 1;
    function drag(ev) {
        var id = $(ev).attr('id');
        if(id == 'seat_chair'){
            id = id+'_'+s;
            s++;
        }else if(id == 'steering'){
            id = id+'_'+d;
            d++;
        }else if(id == 'toilet'){
            id = id+'_'+t;
            t++;
        }else if(id == 'door'){
            id = id+'_'+dr;
            dr++;
        }
        id_rem = id;
//        ev.dataTransfer.setData("text", ev);
        dclon = $(ev).clone().attr({'class':'seat_chair_drag','id':id}).css({position: 'absolute',width:100,height:50});
//        console.log(ev);
    }

    function drop(ev) {
//        var element = $(ev._eventTarget);
        ev.preventDefault();
        var x = ev.toElement.offsetLeft;
        var y = ev.toElement.offsetTop;
        co = Math.round(ev.toElement.offsetLeft / ev.toElement.offsetWidth);
        ro = Math.round(ev.toElement.offsetTop / ev.toElement.offsetHeight);
        var lab = '';
//        var data = ev.dataTransfer.getData("text");
//        $(ev).attr('class','seat_chair_drag').appendTo("#layout_in");
        $("#layout_in").append(dclon);
        $('#'+id_rem).children().css({top:'0px'});
        if(dclon.find('.slable')){
            var l = getAlphaBats()[ro];
            dclon.children('.slable').css({top:'10px'});
            dclon.children('.slable').attr('onClick','setLable('+ro+','+co+',this.innerHTML)');
            dclon.find('.slable').html(l+co);
            lab = '-'+l+co;
        }
        TweenLite.to(dclon, 0, {
            left:x,
            top:y,
            delay:0.1,
            ease:Power2.easeInOut
        });
        var type = '';
            if(id_rem.indexOf('steering') !== -1){
                type = 'DS';
            }else if(id_rem.indexOf('door') !== -1){
                type = 'D';
            }else if(id_rem.indexOf('seat_chair') !== -1){
                type = 'CS-'+l+co;
                dclon.children('.slable').attr('id','seat-'+ro+'-'+co);
            }else if(id_rem.indexOf('toilet') !== -1){
                type = 'T';
            }
            routes[ro+'-'+co] = type;
        console.log(ro+'-'+co+':'+routes[ro+'-'+co]);
//        $("#layout_in").find(dclon).children('span').css({top:'0px;'});
        initDraggableComponent(dclon);
		
        dclon = '';
        id_rem = '';
//        ev.appendChild(data);
    }
    
    
    
    function initDraggableComponent(ele,left,top)
    {
        Draggable.create(ele, {
            bounds: $container,
            edgeResistance: 0.65,
            type: "x,y",
            throwProps: false,
            autoScroll: true,
//            liveSnap : true,
            liveSnap:{
                x: function(value) {
//                    console.log('left : '+left);
//                    if(typeof left !== 'undefined'){
//                        return left;
//                    }
                    return Math.round(value / gridWidth) * gridWidth;
                },
                y: function(value) {
//                    console.log('top : '+top);
//                    if(typeof top !== 'undefined'){
//                        return top;
//                    }
                    return Math.round(value / gridHeight) * gridHeight;
                }
            },
            snap: {
                x: function(endValue) {
                    return Math.round(endValue / gridWidth) * gridWidth;
                },
                y: function(endValue) {
                    return Math.round(endValue / gridHeight) * gridHeight;
                }
            },
            onDragStart(e){
                var element = $(this._eventTarget);
                var rc = eleRowCol(element);
                
                routes[rc.row+'-'+rc.col] = '';
                console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
            },
            onDrag:function(e) {
//                    console.log(this);
                highlight(e,this);
            },
            onThrowComplete: function(e) {
//                var element = $(this._eventTarget);
//                eleRowCol(element);
//                highlight(e,this,1);
            },
            onDragEnd:function(e) {
//                    console.log(this);
                var element = $(this._eventTarget);
                
                var rc = eleRowCol(element);
                var l = getAlphaBats()[rc.row];
                var lab = '';
                var ele_id = $(element).attr("id");
                var typ = '';
                if(ele_id.indexOf('steering') !== -1){
                    typ = 'DS';
                }else if(ele_id.indexOf('door') !== -1){
                    typ = 'D';
                }else if(ele_id.indexOf('seat_chair') !== -1){
                    typ = 'CS-'+l+rc.col;
                    element.find('.slable').html(l+rc.col);
                    element.children('.slable').attr('onClick','setLable('+rc.row+','+rc.col+',this.innerHTML)');
                    element.children('.slable').attr('id','seat-'+rc.row+'-'+rc.col);
                }else if(ele_id.indexOf('toilet') !== -1){
                    typ = 'T';
                }
                
                routes[rc.row+'-'+rc.col] = typ;
                var cell = {};
                console.log(rc.row+'-'+rc.col+':'+routes[rc.row+'-'+rc.col]);
                highlight(e,this,1);
            },
        });
    }
    function eleRowCol(element){
        var element_indexid = $(element).attr("id");
        var position = $(element).position();
//        console.log(element);
//        console.log(position);

        var row = 0;
        var col = 0;

//                row = Math.round(position['left'] / gridWidth);
//                col = Math.round(position['top'] / gridHeight);
        col = Math.round(position['left'] / gridWidth);
        row = Math.round(position['top'] / gridHeight);

        var cell = {"row" : row,"col" : col};
        console.log("Cell");
        console.log(cell);
        return cell;
    }
    function highlight(e,obj,rem){
        var droppables = $(".seat_chair_drag");
                var l = droppables.length;
                var c = 0;
                while (--l > -1) {

                    if (Draggable.hitTest(droppables[l], e) && droppables[l] !== obj.target) {
                        
                        $(droppables[l]).addClass("highlights");
                        c++;
                        if(typeof rem !== 'undefined' && rem == 1){
                            $(droppables[l]).remove();
                        }
//                        $(droppables[l]).disable();
                    } else {
                        $(droppables[l]).removeClass("highlights");
                    }
                }
                
                return c;
    }
    
    $('#generate_layout').click(function(){
//          
          if(!chekLayout()){
              return false;
          }
            
            var row = $('#total_rows').val();
            var col = $('#total_columns').val();
            $("#layout_in").html('');
            routes ={};
            $container = $("#layout_in");
            gridWidth = 100;
            gridHeight = 50;
            gridRows = row;
            gridColumns = col;
            var i, x, y;

        //loop through and create the grid (a div for each cell). Feel free to tweak the variables above
            for (i = 0; i < gridRows * gridColumns; i++) {
                y = Math.floor(i / gridColumns) * gridHeight;
                x = (i * gridWidth) % (gridColumns * gridWidth);
                $("<div/>").css({position: "absolute", border: "1px solid #454545", width: gridWidth - 1, height: gridHeight - 1, top: y, left: x}).prependTo($container);
            }
            
            if(row > 4){
                if(row == 5){
                    g = 2
                }else{
                    g = 3;
                }
            }else {
                g =2;
            }
//            console.log(g);
            for(ro = 0;ro<row;ro++){
                y = ro * gridHeight;
//                console.log(ro+'-'+y);
                for(co = 0;co<col;co++){
                    x = (co * gridWidth) ;//% (gridColumns * gridWidth);
//                    console.log(ro+' - '+co+' - '+x);
                    if(ro == 1 && co == 0){
                        addElement('steering',ro,co,x,y);
                    }else if(ro == (row - 1) && co == 0){
                        addElement('door',ro,co,x,y);
                    }else if(ro == g ){
//                        console.log('entered');
                        if(co ==(col - 1)){
//                            console.log('in last col');
                            addElement('seat_chair',ro,co,x,y);
                        }
                    }
                    else if(co !=0){
                        addElement('seat_chair',ro,co,x,y);
                    }
                }
            }
            //set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
            TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
            TweenLite.set(".seat_chair_drag", {width: gridWidth, height: gridHeight, lineHeight: gridHeight + "px"});
            
            var overlapThreshold = "50%"; 
            
      });
      
    function addElement(id,ro,co,x,y,lbl){
        var ele = $('#'+id).clone().attr({'class':'seat_chair_drag','id':id+ro+'-'+co}).css({
                position: 'absolute',
                width:gridWidth - 1,
                height:gridHeight - 1
//                top: y, 
//                left: x
            });
            var lab = '';
            if(ele.find('.slable')){
                var l = getAlphaBats()[ro];
                ele.find('.slable').html(l+co);
                ele.children('.slable').attr('onClick','setLable('+ro+','+co+',this.innerHTML)');
                lab = '-'+l+co;
            }
            var type = '';
            if(id == 'steering'){
                type = 'DS';
            }else if(id == 'door'){
                type = 'D';
            }else if(id == 'seat_chair'){
                type = 'CS-'+l+co;
                if(typeof lbl !=='undefined'){
                    type = 'CS-'+lbl;
                    ele.find('.slable').html(lbl);
                }
                ele.children('.slable').attr('id','seat-'+ro+'-'+co);
            }else if(id == 'toilet'){
                type = 'T';
            }
            routes[ro+'-'+co] = type;
//            console.log(routes);
        $("#layout_in").append(ele);
        TweenLite.to(ele, 0, {
            left:x,
            top:y,
            delay:0.1,
            ease:Power2.easeInOut
        });
//                        $(ele).prependTo($container);
         
//        ele = ele.css({position: 'absolute'});
        initDraggableComponent(ele,x,y);
    }
    
    function removeElement(key){
        if(routes.hasOwnProperty(key)){
            delete routes[key];
        }
//        console.log(key);
//        console.log(routes);
    }
    function setLable(r,c,val){
        console.log(r+'-'+c+'-'+val);
        $('#modal-lable').openModal();
        $('#add-lname').val(val);
        $('#save_lable').attr('onClick','saveLable('+r+','+c+')');
    }
    
    function saveLable(r,c){
        var lbl= $('#add-lname').val();
        if(lbl == ''){
            Materialize.toast('Lable should not be blank !!', 2000,'rounded');
            return false;
        }
        $('#seat-'+r+'-'+c).html(lbl);
        routes[r+'-'+c] = 'CS-'+lbl;
        
        $('#modal-lable').closeModal();
    }
    $('#is_layout').click(function(){
        if($(this).is(':checked')){
            $('#display_layout').css({display:'inline'});
            $('#total_seats_div').css({display:'none'});
            $('#total_seats').removeAttr('required');
            $('#total_seats').removeAttr('data-parsley-min');
        }else{
            $('#display_layout').css({display:'none'});
            $('#total_seats_div').css({display:'inline'});
            $('#total_seats').attr('data-parsley-min',4);
        }
    });
    <?php
    if($bus['layout']){
    ?>
    var layout = JSON.parse('<?php echo $body["layout"]?>');
    var cols = '<?php echo $bus["busLayout"][0]['columns']?>';
    var rows = '<?php echo $bus["busLayout"][0]['rows']?>';
//    console.log(layout);
//    console.log(cols);
//    console.log(rows);
    
    $container = $("#layout_in");
    gridWidth = 100;
    gridHeight = 50;
    gridRows = rows;
    gridColumns = cols;
    var i, x, y;

//loop through and create the grid (a div for each cell). Feel free to tweak the variables above
    for (i = 0; i < gridRows * gridColumns; i++) {
        y = Math.floor(i / gridColumns) * gridHeight;
        x = (i * gridWidth) % (gridColumns * gridWidth);
        $("<div/>").css({position: "absolute", border: "1px solid #454545", width: gridWidth - 1, height: gridHeight - 1, top: y, left: x}).prependTo($container);
    }
    
    for(i in layout){
        y = layout[i]['row'] * gridHeight;
        ro = layout[i]['row'];
//                console.log(ro+'-'+y);
//        for(co = 0;co<cols;co++){
        x = (layout[i]['col'] * gridWidth) ;//% (gridColumns * gridWidth);
        co = layout[i]['col'];
        var labl = layout[i]['seat_lbl'];
//                    console.log(ro+' - '+co+' - '+x);
        var ot = layout[i]['object_type'];
        var id_s = ot =='DS' ? 'steering' : ( ot == 'D' ? 'door' : ( ot == 'T' ? 'toilet' : 'seat_chair' ) );
            addElement(id_s,ro,co,x,y,labl);
//        }
    }
    //set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
    TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
    TweenLite.set(".seat_chair_drag", {width: gridWidth, height: gridHeight, lineHeight: gridHeight + "px"});
    
    <?php 
    }
    ?>
     </script>
    <?php 
    
         }else{
         echo '<div class="col l3 m12"> No buses found. </div>';
         }
     }else{
         ?>
     <div class="col l3 m12"> No data found !!! </div>
             <?php
     }
    ?>

@endsection