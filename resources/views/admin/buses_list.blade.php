@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        
    };
    $(document).ready(function(){
        
        $('#paypal').change(function(){
            if($(this).prop("checked") == true){
                $('#paypalblock').css('display','block');
            }
            else if($(this).prop("checked") == false){
                $('#paypalblock').css('display','none');
            }
        });
    });
    function  showText(id){
        $('#h-'+id).css({display:'none'});
        $('#ed-'+id).css({display:'none'});
        $('#v-'+id).css({display:'none'});
        $('#t-'+id).css({display:'block'});
        $('#save-'+id).css({display:'block'});
        $('#can-'+id).css({display:'block'});
    }
    function  cancel(id){
        $('#h-'+id).css({display:'block'});
        $('#ed-'+id).css({display:'block'});
        $('#v-'+id).css({display:'block'});
        $('#t-'+id).css({display:'none'});
        $('#save-'+id).css({display:'none'});
        $('#can-'+id).css({display:'none'});
    }
    function saveBusName(id){
        var name = $('#t-'+id).val();
        if(name == ''){
            Materialize.toast('Bus name should not empty .', 2000,'rounded');
            return;
        }else{
            var old = $('#h-'+id).html();
            var new_name = $('#t-'+id).val();
            if(old != new_name){
                var url="{{URL::to($lt.'/update-bus-name')}}";
                var data={id:id,name:new_name};
                postAjax(url,data,function(data){
                    console.log(data);
                    if(data.flag != 1){
                        Materialize.toast(data.msg, 2000,'rounded');
                    }else{
                        $('#h-'+id).html($('#t-'+id).val());
                        Materialize.toast('Bus name updated successfully !!', 2000,'rounded');
                    }
                });
            }
            $('#h-'+id).css({display:'block'});
            $('#ed-'+id).css({display:'block'});
            $('#v-'+id).css({display:'block'});
            $('#t-'+id).css({display:'none'});
            $('#save-'+id).css({display:'none'});
            $('#can-'+id).css({display:'none'});
        }
        
    }
    function getBus(id){
        var url="{{URL::to($lt.'/get-bus')}}";
        var data={id:id};
        postAjax(url,data,function(data){
            
                $('#show_bus #bus_detail').html('');
                $('#show_bus #bus_detail').html(data);
        });
    }
    $(document).on('click','#save_bus',function(){
        var bus_name = $('#bus_name').val();
        var seat_capacity = $('#seat_capacity').val();
        var bus_type = $('#bus_type :selected').val();
        if(bus_name == ''){
            Materialize.toast('Bus name should not be blank !!', 2000,'rounded');
            return false;
        }else if(bus_type == ''){
            Materialize.toast('Please select bus type !!', 2000,'rounded');
            return false;
        }
//        else if(seat_capacity == '' || seat_capacity == 0){
//            Materialize.toast('Please enter seat capacity !!', 2000,'rounded');
//            return false;
//        }
//        $("#add_bus_form").ajaxSubmit({url: '{{URL::to("admin/add-bus")}}', type: 'post',success:function(res){
//                alert('response');
//            console.log(res);
//            }
//        });
        $('#add_bus_form').ajaxForm(function(res) {
           console.log(res);
           if(res.flag == 1){
               Materialize.toast('Bus added successfully !!', 2000,'rounded');
               location.reload();
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });
    
    function mdlOpen(){
        $('#bus_name').val('');
        $('#bus_type').val('0');
        $('#bus_type').material_select();
        $('#add_bus').openModal();
        
    }
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Buses</h1>
                <ul>
                    <li><a href='{{URL::to($lt.'/service-provider')}}'><i class="fa fa-home"></i> Service Provider</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a href='#!'>{{ $body['buses'] ? $body['buses']['first_name'].' '.$body['buses']['last_name'] : 'no service provider' }}</a>  <i class='fa fa-angle-right'></i>
                    </li>
                    <li><a >Buses</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>

     <!-- /Breadcrumb -->
     <?php if(isset($body['buses'])){
         
         if(isset($body['buses']['spBuses'])){
         ?>
    <div class="row" >
        <?php
            foreach($body['buses']['spBuses'] as $bus){
                ?>
        <div class="col l3 m4 s12">
            <div class="card">
                <div class="title">
                    <div class="col l9 m9 s12" style="padding-bottom: 0px;padding-right: 0px;">
                        <a href="{{URL::to($lt.'/bus-structure/'.$bus['id'].'/'.$bus['sp_id'])}}"><h5 id="h-{{ $bus['id'] }}" class="default_color">{{$bus['name']}}</h5></a>
                        <input type="text" id="t-{{ $bus['id'] }}" value="{{$bus['name']}}"  style="display: none;margin: 0px;height: 38px;" />
                    </div>
                    <div class="col l1 m1 s12 " style="padding: 0px;margin-left: 15px;">
                        <a id="ed-{{ $bus['id'] }}" class="right" style="cursor: pointer" onclick="showText( {{ $bus['id'] }} );">
                            <i class="mdi-editor-mode-edit small default_color"></i>
                        </a>
                        <i class="mdi-content-save small default_color" style="display: none;cursor: pointer;"  onclick="saveBusName( {{ $bus['id'] }} );"  id="save-{{ $bus['id'] }}"></i>
                        
                    </div>
                    <div class="col l1 m1 s12 "  style="padding: 0px">
                        <a id="v-{{ $bus['id'] }}" class="right modal-trigger" href="#show_bus" onclick="getBus( {{ $bus['id'] }} )">
                            <i class="mdi-image-remove-red-eye small default_color"></i>
                        </a>
                        <i class="mdi-navigation-close small default_color"  style="display: none;cursor: pointer;" onclick="cancel( {{ $bus['id'] }} );"  id="can-{{ $bus['id'] }}"></i>
                    </div>
                    
                </div>
                <div class="content"> {{count($bus['routeCount'])}} Routes</div>
            </div>
        </div>
        <?php
            }
        ?>
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <!--<a href="#add_bus" class="btn-floating btn-large green modal-trigger"><i class="mdi-content-add"></i></a>-->
            <a class="btn-floating btn-large green " onclick="mdlOpen();"><i class="mdi-content-add"></i></a>
        </div>
        <form id="add_bus_form" action="{{URL::to($lt.'/add-bus')}}" method="post" enctype="multipart/form-data" >
        <div id="add_bus" class="modal bust-modal" style="width: 35%;font-size: 100%;">
            <div class="modal-content">
              <h4>Add Bus</h4>
              <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <input type="hidden" name="sp_id" value="{{$body['buses']['id']}}" />
              <div class="input-field">
                  <input id="bus_name" type="text" name="bus_name" class="validate" >
                <label for="input_text">Bus Name</label>
              </div>
              
              <select id="bus_type" name="bus_type">
                <option value="0" disabled selected>Select Type</option>
                <option value="bus">Bus</option>
                <option value="shuttle">Shuttle</option>
                <option value="travel">Travel</option>
              </select>
<!--              <div class="input-field">
                  <input id="seat_capacity" name="seat_capacity" type="number" class="validate" >
                <label for="input_text">Seat Capacity</label>
              </div>-->
              <div class="file-field input-field">
                <div class="btn">
                  <span>Video</span>
                  <input type="file" name="video" id="video" accept="video/*">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
              <label for="textarea1">Terms And Conditions</label>
              <div class="input-field">
                  
                  <textarea  id="tnc" name="tnc" class="materialize-textarea"></textarea>
                <script>
                CKEDITOR.replace( 'tnc' );
                </script>
              </div>
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
              <!--<a id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</a>-->
              <button id="save_bus"  class="waves-effect waves-green btn-flat ">Save</button>
            </div>
          </div>
        </form>
        
        <div id="show_bus" class="modal bust-modal" style="width: 60%;font-size: 100%;overflow: auto;overflow-x: hidden">
            
            <div id="bus_detail" class="modal-content" style="display: table-cell;width: 40%">
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
              <!--<button id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</button>-->
            </div>
          </div>
        
    </div>
    <?php 
    }else{
         echo '<div class="col l3 m12"> No buses found. </div>';
         }
     }else{
         ?>
     <div class="col l3 m12"> No data found !!! </div>
             <?php
     }
    ?>

@endsection