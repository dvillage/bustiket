<?php
$lt = config('constant.LOGGER') == 'SS'?'ss': 'admin' ;
?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Seat Seller Name</th>
              <th>Type</th>
              <th>Amount</th>
              <th>Payment By</th>
              <th>Bank account name</th>
              <th>Bank account number</th>
              <!--<th>Status</th>-->
<!--              <th>Total Seat</th>
              <th>Transfer</th>-->
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($data['result'] as $r) { 
//                print_r($data['result']);exit;
                ?>
            <tr>
                <th><a href="{{URL::to($lt.'/seatseller-details/'.$r['ss_id'])}}"><span id='name-{{ $r['ss_id'] }}'>{{ $r['seat_seller']['name'] }}</span></a></th>
                <td>{{ $r['seat_seller']['type'] }}</td>
                <td>{{ $r['amount'] }}</td>
                <td>{{ $r['payment_by'] ? 'Bank Transfer' : 'Paypal' }}</td>
                <td>{{ $r['bank_acc_name'] }}</td>
                <td>{{ $r['bank_acc_no'] }}</td>
                <!--<td>{{ $r['status'] == 1 ? 'Approved' : $r['status'] == 0 ? 'Pending' : 'Rejected' }}</td>-->
                <td>
                    <?php if($r['status'] == 0){ ?>
                    <a href="#" title="Approve" onclick="approve( {{ $r['id'] }} ,'A');"><i class="mdi-action-done small default_color modal-trigger"></i></a>
                    <a href="#" title="Reject" onclick="approve( {{ $r['id'] }} ,'R');"><i class="mdi-content-clear small modal-trigger"></i></a>
                    <?php }else{
                       echo $r['status'] == 1 ? '<i style="color:green">Approved<i>' : '<i style="color:red">Rejected</i>' ;
                    } ?>
                </td>
            </tr>
            <?php } ?>
            
        </tbody>
    </table>
    
<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
