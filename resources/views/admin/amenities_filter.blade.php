
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Name</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='aname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td>
                    <span id='aid-{{ $r['id'] }}' name="{{ $r['id'] }}" ></span>
                    <span id='dimg-{{ $r['id'] }}'>
                    <?php
                     $file = URL::to('assets/uploads/amenities').'/'.$r['img'];
                     $file_check = config('constant.UPLOAD_AMENITY_DIR_PATH').'/'.$r['img'];
                     if($r['img'] != ''){
                        if(file_exists($file_check)){
                            echo '<img src="'.$file.'" style="height:50px;width:50px"/>';
                        }
                     }
                    ?>
                    </span></td>
                <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upAmenity( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delAmenity( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />