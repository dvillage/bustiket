
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>
                    <p class="form-field-inline clearfix nospace-top checkbox-check">
                            <input type="checkbox" name="" onClick="allCheckBox();" id='masterCheck'>
                            <label for='masterCheck'></label>
                    </p>
                </th>
              <th>User Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Gender</th>
              <th>City</th>
             
            </tr>
        </thead>
        <tbody>
            
             <?php foreach($data['result'] as $r) {?> 
            <tr>
                 <td width="5%">
                    <p class="form-field-inline clearfix nospace-top checkbox-check">
                        <input type="checkbox" name="" class="icheck" id='p-{{ $r['id'] }}'  data-email="{{$r['email']}}">
                            <label for='p-{{ $r['id'] }}'></label>
                    </p>
                </td>
                <td><span id='name-{{ $r['id'] }}'> {{$r['name']}} </span></td>
                <td><span id='email-{{ $r['id'] }}'> {{$r['email']}} </span></td>
                <td><span id='mobile-{{ $r['id'] }}' > {{$r['mobile']}}</span></td>
                <td><span id='gender-{{ $r['id'] }}' > {{$r['gender'] =='M' ? 'Male':'Female'}}</span></td>
                <td><span id='city-{{ $r['id'] }}'> {{$r['city'] }} </span></td>
            </tr>
            <?php  } ?>
        </tbody>
    </table>
    <?php  ?>

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
