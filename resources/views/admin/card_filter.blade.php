

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Card Name</th>
              <th>Card Price</th>
              <th>Shipping Charge</th>
              <th>Bank Name</th>
              <th>Card Image</th>
              <th>Card Detail</th>
              <th>Functionality</th>
              <th>How To Use Card</th>
              <th>Couriers</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td>
                    <span id='status-{{ $r['id'] }}' name="{{$r['status']}}">
                    <span id='name-{{ $r['id'] }}'> {{$r['name']}} </span></td>
                <td><span id='price-{{ $r['id'] }}'> {{ (int) $r['price']}} </span></td>
                <td><span id='charge-{{ $r['id'] }}' > {{ (int) $r['charge'] }}</span></td>
                <td><span id='bank-{{ $r['id'] }}' >{{ $r['bank_name']}}</span></td>
                <td>
                    <span id='cimg-{{ $r['id'] }}'> 
                        <?php
                        if($r['card_image'] != ''){
                           $file = URL::to('assets/uploads/cards').'/'.$r['card_image'];
                           $file_check = config('constant.UPLOAD_CARD_DIR_PATH').$r['card_image'];
                           if(file_exists($file_check)){
                               echo '<img id="img-'.$r['id'].'" src="'.$file.'" style="height:25px;width:25px"/>';
                           }
                        }
                        ?>
                    </span>
                </td>
                <td><span id='detail-{{ $r['id'] }}'> <?php echo html_entity_decode($r['detail']); ?> </span></td>
                <td><span id='functionality-{{ $r['id'] }}'> <?php echo html_entity_decode($r['functionality']);?> </span></td>
                <td><span id='use-{{ $r['id'] }}'> <?php echo html_entity_decode($r['use']);?> </span></td>
                <td>
                    <?php 
                        $am = $r['courier'];
                        $alla = '';
                        $allid = '';
                        if(count($am)> 0){
                            foreach($am as $m){
                                $amc = $m['courier'];
                                    if($alla == ''){
                                        $alla = $amc['name'];
                                        $allid = $amc['id'];
                                    }else{
                                        $alla .= ','.$amc['name'];
                                        $allid .= ','.$amc['id'];
                                    }
                                    $name = preg_replace('/[^A-Za-z0-9]/', '_', $amc['name']);
                                    echo '<input type="hidden" id="'.$name.'_id" value="'.$amc['id'].'" />';
                            }
                            
                        }
                        
                        ?>
                    <span id='courier-{{ $r['id'] }}'>{{$alla}} </span>
                    <input type="hidden" id="all_courier-{{$r['id']}}" value="{{$allid}}" />
                </td>
                <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upPrw( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delPrw( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    <?php echo $r['status'] == 1 ? '<i class="mdi-image-brightness-1 " style="color: green;" title="Active"></i>' : ($r['status'] == 2 ? '<i class="mdi-image-brightness-1 " style="color: orange;" title="Suspended"></i>' : '<i class="mdi-image-brightness-1 " style="color: red;" title="InActive"></i>' )?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />