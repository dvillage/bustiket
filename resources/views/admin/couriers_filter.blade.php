
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Name</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td>
                    <span id='status-{{ $r['id'] }}' name="{{ $r['status'] }}" >
                    <span id='aname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upCourier( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delCourier( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    <?php echo $r['status'] == 1 ? '<i class="mdi-image-brightness-1 " style="color: green;" title="Active"></i>' : ($r['status'] == 2 ? '<i class="mdi-image-brightness-1 " style="color: orange;" title="Suspended"></i>' : '<i class="mdi-image-brightness-1 " style="color: red;" title="InActive"></i>' )?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />