@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP' ? 'sp' : 'admin';
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/drivers-filter',crnt,len,type,'','');
    };
    
    function delD(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Driver ??';
        var onyes ='delData';
        var param='\'{{$lt}}/delete-driver\','+id+' , \'{{$lt}}/drivers-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function addD(){
        $('#dname').val('');
        $('#demail').val('');
        $('#dcon').val('');
        $('#daddress').val('');
        $('#proof_type').val('');
        $('#proof_number').val('');
        $('#crew').val('');
        
        $('#opr').val('Add');
        
        $('#add_driver').attr('action',"{{URL::to($lt.'/add-new-driver')}}");
        
        $('#modal-driver').find('#save-d').html('Add');
        $('#modal-driver').openModal();
        $('#add_driver').parsley().reset();
    }
    
    function editD(id){
        $('#dname').val($('#name-'+id).html());
        $('#demail').val($('#email-'+id).html());
        $('#dcon').val($('#con-'+id).html());
        $('#daddress').val($('#addre-'+id).html());
        $('#crew').val($('#crew-'+id).html());
        $('#proof_number').val($('#pnum-'+id).val());
        $('#proof_type').val($('#ptype-'+id).attr('name'));
        $('#did').val(id);
        $('#opr').val('Update');
        $('#proof_type').material_select();
        
        $('#add_driver').attr('action',"{{URL::to($lt.'/edit-driver')}}");
        
        $('#modal-driver').find('#save-d').html('Update');
        $('#modal-driver').openModal();
        
        $('#add_driver').parsley().reset();
        
    }
    
    $(document).on('click','#save-d',function(){
        $('#add_driver').ajaxForm(function(res) {
           if(res.flag == 1){
               if($('#opr').val()==='Update'){
                   Materialize.toast('Driver Details Updated successfully !!', 2000,'rounded');
               }
               else{
                    Materialize.toast('Driver Added successfully !!', 2000,'rounded');
                }
                $('#modal-driver').closeModal();
               filterData('{{$lt}}/drivers-filter');
               
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });

</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Drivers</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Drivers</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" onchange="return filterData('{{$lt}}/drivers-filter');">
                        <label for="input_text">Filter By Driver Name or Mobile No.</label>
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" onclick="return filterData('{{$lt}}/drivers-filter');"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Drivers</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/drivers-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/drivers-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/drivers-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/drivers-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/drivers-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/drivers-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="add_driver" action="" method="post" enctype="multipart/form-data" data-parsley-validate >
          <div id="modal-driver" class="modal bust-modal" style='width:40%;font-size:100%;overflow-x: hidden;overflow-y:auto;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Driver</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                    
                    <div class="content">
                        
                        <div class="input-field">
                            <input id="dname" name="name" type="text" placeholder="Driver Name" required>
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="did" id="did" value="" />
                            <input type="hidden" name="opr" id="opr" value="" />
                            <label for="group">Driver Name</label>
                        </div>
                       
                        <div class="input-field">
                            <input id="demail" name="email" type="text" placeholder="Email" >
                            <label for="height">Email</label>
                        </div>
                        <div class="input-field">
                            <input id="dcon" name="mobile" type="number" data-parsley-type="digits" data-parsley-length="[10,11]" placeholder="Contact No." required>
                            <label for="height">Contact No.</label>
                        </div>
                        
                        <div class="row">

                            <div class="col l6 m6 s6">
                                <div class="input-field">
                                <select id="proof_type" name="proof_type" required>
                                    <option value='' disabled selected>Select ID Proof Type</option>
                                    <?php
                                    foreach(config('constant.ID_TYPE') as $key=>$val){
                                        echo '<option value="'.$val.'">'.$val.'</option>';
                                    }
                                    ?>
                                </select>
                                <label for="width_type">ID Proof Type</label>
                                </div>

                            </div>
                            <div class="col l6 m6 s6">
                                <div class="input-field">
                                <input id="proof_number" name="proof_number" type="text" placeholder="Proof Number" required>
                                <label for="width">ID Proof Number</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="input-field">
                            <input id="crew" name="crew" type="text" placeholder="Crew" >
                            <label for="crew">Crew</label>
                        </div>
                        
                        <div class="input-field">
                            <textarea id="daddress" name="address" class="materialize-textarea" ></textarea>
                            <label for="width">Adress</label>
                        </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-d" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            
            <a class="btn-floating btn-large red tooltipped" onclick="addD();"data-position="top" data-delay="50" data-tooltip="Add Driver">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>


@endsection