<?php 
$from_id = $body['param']['from_id'];
$to_id = $body['param']['to_id'];
$from_name = $body['param']['from_name'];
$to_name = $body['param']['to_name'];
$ft = $body['from']->lists('name','id');
$tt = $body['to']->lists('name','id');
//print_r($tt);
//$from_ter = $ft;
//$to_ter = $tt;
?>
<ul class="collapsible" data-collapsible="accordion">
    <li class="">
        <div class="collapsible-header active"> <b> {{ $from_name .'-'.$to_name }} </b>
                    <a id="" onClick="removeRoute(this);" style="cursor: pointer" class="default_color"> <i class="mdi-content-clear right small" ></i></a>
        </div>
        <div class="collapsible-body">
            <table id="{{ $from_id.'-'.$to_id }}" class="table">
<!--                <thead>
                    <tr>
                        <th colspan="6">{{ $from_name .'-'.$to_name }}
                            <a id="" onClick="removeRoute(this);" style="cursor: pointer" class="default_color"> <i class="mdi-content-clear right small" ></i></a>
                        </th>
                    </tr>
                </thead>-->
                <tbody>
                    <tr>
                        <th class="br bl bb bt"></th>
                        <td class="br bl bb bt">From Terminal </td>
                        <td class="br bl bb bt">
                            {{  Form::select('ter_from_'.$from_id.'-'.$to_id,$ft,'',['id'=>'ter_from_'.$from_id.'-'.$to_id,'class'=>'initialized']) }}
                        </td>
                        <td class="br bl bt bb"><a onclick="addBoardPoint( {{ $from_id }} , {{ $to_id }} ,'{{$from_name}}','{{$to_name}}')" class="btn btn-medium default_color" style="color:white">Add Boarding Terminal</a></td>
                        <td class="br bl bb bt">To Terminal</td>
                        <td class="br bl bb bt">
                            {{  Form::select('ter_to_'.$from_id.'-'.$to_id,$tt,'',['id'=>'ter_to_'.$from_id.'-'.$to_id,'class'=>'initialized']) }}
                        </td>
                        <td class="br bl bt bb"><a onclick="addDropPoint( {{ $from_id }} , {{ $to_id }} ,'{{$from_name}}','{{$to_name}}')" class="btn btn-medium default_color" style="color:white">Add Dropping Terminal</a></td>
                    </tr>
                    <tr>
                        <td  colspan="7">
                            
                            <table id="term_detail_{{ $from_id.'-'.$to_id }}" class="center" style="width:100%;" >
                                <tbody>
                                    <tr>
                                        <td style="width:50%;">
                                            <u><h4>Boarding Point List</h4></u>
                                            <table id="term_detail_{{ $from_id}}" class="center" >
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </td >
                                        <td style="border: solid 1px #9ccc65;background-color: #9ccc65;"></td>
                                        <td style="width:50%;">
                                            <u><h4>Droping Point List</h4></u>
                                            <table id="term_detail_{{$to_id }}" class="center" >
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td  colspan="7">
                            <u><h4>Route Pricing</h4></u>
                            <table id="pricing_{{ $from_id.'-'.$to_id }}" class="table">
                                <tbody>
                                    <tr>
                                        <th class="br bl bb bt">1</th>
                                        <td class="br bl bb bt">From Seat : </td>
                                        <td class="br bl bb bt">
                                            <input id="seat_count_1" type="number" name="price[{{ $from_id.'-'.$to_id }}][1][from_seat_count]" value="" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                            <input type="hidden" name="price[{{ $from_id.'-'.$to_id }}][1][id]" value="" />
                                        </td>
                                        <td class="br bl bb bt">To Seat : </td>
                                        <td class="br bl bb bt">
                                            <input id="seat_count_1" type="number" name="price[{{ $from_id.'-'.$to_id }}][1][to_seat_count]" value="" data-parsley-type="integer" required class="validate" placeholder="seat count">
                                            <input type="hidden" name="price[{{ $from_id.'-'.$to_id }}][1][id]" value="" />
                                        </td>
                                        <td class="br bl bb bt">Price :</td>
                                        <td class="br bl bb bt">
                                            <input id="price_1" type="text" name="price[{{ $from_id.'-'.$to_id }}][1][price]" value="" data-parsley-type="integer" required class="validate" placeholder="price">
                                        </td>
                                        <td class="br bl bt bb"><a id="add_price_scheme_n" onclick="addPrice( {{$from_id}} , {{$to_id}} )" style="cursor: pointer" class="default_color"> <i class="mdi-content-add right small" ></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </li>
    
</ul>