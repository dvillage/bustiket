@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
      
        $('#cntrlbtn').css('display','none');

        getData('admin/route-filter',crnt,len,type,'','');
        
    };
    
    function delPrw(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Favorite Route ??';
        var onyes ='delData';
        var param='\'admin/delete-route\','+id+' , \'admin/route-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addprw(){
        
        $('#fcity').val('').attr('placeholder','enter from city name');
        $('#tcity').val('').attr('placeholder','enter to city name');
        $('#bus_name').val('').attr('placeholder','enter bus name');
        $('#from_city_id').val('');
        $('#to_city_id').val('');
        $('#bus_id').val('');
        $('#image').attr('required',true);
        $('#modal-card').find('#save-banner').attr('onclick','addNewRoute()');
        $('#modal-card').find('#save-banner').html('Add');
        $('#modal-card').openModal();
        $('#card_details').parsley().reset();
        
        var chk = $('#in_footerzz').is(':checked');
        if(!chk){
            $('#in_footer').trigger('click');
        }
    }
    
    function upPrw(id){
        $('#cid').val(id);
        $('#fcity').val($('#fcity-'+id).html().trim());
        $('#tcity').val($('#tcity-'+id).html().trim());
        $('#bus_name').val($('#bus-'+id).html().trim());
        $('#price').val($('#price-'+id).attr('name'));
        $('#from_city_id').val($('#fcity-'+id).attr('name'));
        $('#to_city_id').val($('#tcity-'+id).attr('name'));
        $('#bus_id').val($('#bus-'+id).attr('name'));
        
        $('#image').removeAttr('required');
        $('#modal-card').find('#save-banner').attr('onclick','editRoute('+id+')');
        $('#modal-card').find('#save-banner').html('Update');
        $('#modal-card').openModal();
        $('#card_details').parsley().reset();
        

        var status = $('#in_footer-'+id).attr('name');
        var chk = $('#in_footer').is(':checked');
        if(status == 1 && !chk){
            $('#in_footer').trigger('click');
        }else if(status == 0 && chk){
            $('#in_footer').trigger('click');
        }
    }
    
    function addNewRoute(){
        
        var url="{{URL::to('admin/add-route')}}";
        $('#card_details').attr('action',url);
//        console.log(Courier);
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/card-filter');
            }
            location.reload();
        });
    }
    function editRoute(id){
//        alert('called');
        
        var url="{{URL::to('admin/edit-route')}}";
        $('#card_details').attr('action',url);
        
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/route-filter');
            }
            location.reload();
        });
    }
   
    $(document).on('click','#search',function(){
        var status = $('#status :selected').val();
        var to_city = $('#to_city').val();
        var bus = $('#bus').val();
        var url = 'admin/route-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        if(to_city != ''){
            data['to_city'] = to_city;
        }
        if(bus != ''){
            data['bus'] = bus;
        }
//        pagination(url,data,'vtable');
        
        
        filterData('admin/route-filter','',data);
    });
    
    var flag = 0;
    $(document).on('keyup','#fcity',function(){
        if($('#fcity').val() == ''){
            $('#from_city_id').val('');
        }
        var to = $('#to_city_id').val();
        
        var s = $(this).val();
        if(s.length < 2){
            flag = 1;
            return false;
        }
        if(flag == 1){
            flag = 0;
           var url = '{{URL::to("admin/city-name")}}';
           var data = {name:s,id:to};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#fcity').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#from_city_id').val(key);
                        }else if($('#fcity').val() == ''){
                            $('#from_city_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
    var flag1 = 0;
    $(document).on('keyup','#tcity',function(){
        if($('#tcity').val() == ''){
            $('#to_city_id').val('');
        }
        var from = $('#from_city_id').val();
        if(from == ''){
            Materialize.toast('please enter from city', 2000,'rounded');
            return false;
        }
        var s = $(this).val();
        if(s.length < 2){
            flag1 = 1;
            return false;
        }
        if(flag1 == 1){
            flag1 = 0;
            
           var url = '{{URL::to("admin/city-name")}}';
           var data = {name:s,id:from};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#tcity').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#to_city_id').val(key);
                        }else if($('#tcity').val() == ''){
                            $('#to_city_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
    var flag2 = 0;
    $(document).on('keyup','#bus_name',function(){
        if($('#bus_name').val() == ''){
            $('#bus_id').val('');
        }
        
        var s = $(this).val();
        if(s.length < 2){
            flag2 = 1;
            return false;
        }
        if(flag2 == 1){
            flag2 = 0;
            
           var url = '{{URL::to("admin/bus-name")}}';
           var data = {name:s};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#bus_name').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#bus_id').val(key);
                        }else if($('#bus_name').val() == ''){
                            $('#bus_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Favorite Routes </h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Favorite Routes</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter by from city name</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="to_city" type="text" class="validate" >
                        <label for="input_text">Filter by to city name</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="bus" type="text" class="validate" >
                        <label for="input_text">Filter by to bus name</label>
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Favorite Route</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/route-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/route-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/route-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/route-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/route-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/route-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="card_details" action="{{URL::to('admin/route-card')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-card" class="modal bust-modal" style='width: 50%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Favorite Route Detail</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="cid" id="cid" value="" />
                <input type="hidden" name="from_city_id" id="from_city_id" value="" />
                <input type="hidden" name="to_city_id" id="to_city_id" value="" />
                <input type="hidden" name="bus_id" id="bus_id" value="" />
                <input type="hidden" name="opr" id='opr' value="Add" />
                   
                 
                    <div class="content">
                        
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <div class="input-field">

                                    <input id="fcity" name="fcity" type="text" placeholder="" autocomplete="off" required>
                                    <label for="sp_id">From City</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="input-field">

                                    <input id="tcity" name="tcity" type="text" placeholder="" autocomplete="off" required>
                                    <label for="city_id">To City</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <input id="bus_name" name="bus_name" type="text" placeholder="" autocomplete="off" required>
                                    <label for="group">Bus</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <input id="price" name="price" type="text" placeholder="1000" autocomplete="off" required>
                                    <label for="price">Price</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field">
                                    <div class="btn"  style="width:100px;">
                                        <span class="">Image</span>
                                        <input type="file" name="image[]" id="image" accept="image/*" >
                                        <!--<label for="bnr_img">Upload Banner Image</label>-->
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-field">
                        <p class="switch" style='padding: 10px 0 20px 0;'>
                            <label>
                                <span style='font-size: 20px;padding-bottom: 10px;'>Display in Footer : </span>
                                <input type="checkbox" id='in_footer' name='in_footer' value="1" />
                                <span class="lever" style="margin-bottom: 8px;"></span>

                            </label>
                        </p>
                    </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="addprw();"data-position="top" data-delay="50" data-tooltip="Add Favorite Route">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
@endsection