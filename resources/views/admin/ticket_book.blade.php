@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
/*    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }*/
</style>
<script>
    
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
//        var crnt = $('#crnt').val();
//        var len = $('#len').val();
//        var type = $('#type').html();
//        $('#cntrlbtn').css('display','none');

//        getData('{{$lt}}/sp-filter',crnt,len,type,'','');
    };
    
//    function delSP(id){
//        var title='<b>Confirmation<b>';
//        var msg='Are you sure to Delete Service Provider ??';
//        var onyes ='delData';
//        var param='\'{{$lt}}/del-service-provider\','+id+' , \'{{$lt}}/sp-filter\'';
//        var mdlname='confirmDel';
//        modalOpen(mdlname,title,msg,onyes,param);
//    }

</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Book New Ticket</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Book New Ticket</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        <div class="col l1 m1"></div>
        
        <div class="col m10 l10">
            <div class="row">
                <form id="form_search" data-parsley-validate method="post" action="{{URL::to('services/search-bus')}}" >
                <div class="col s12 m3 l3">
                    <div class="input-field">
                        <input id="from_city" name="from_city" type="text" autocomplete="off" required>
                        <input type="hidden" name="from" id="from_id" class="set_id" >
                        <!--<input type="hidden" name="nop" id="nop" value="4" >-->
                        <input type="hidden" name="backend" value="1" >
                        <input type="hidden" name="id" id="id_set" value="" >
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}"  >
                        <input type="hidden" id="session_id" value='<?php echo session()->getId();?>' />
                        <input type="hidden" id="city_list" value="{{$body['city_list']}}" >
                        <!--<input type="hidden" id="session_id" value='<?php // echo csrf_token();?>' />-->
                        <label for="height">From City</label>
                    </div>
                </div>
                <div class="col s12 m3 l3">
                    <div class="input-field">
                        <input id="to_city" name="to_city" type="text" autocomplete="off" required>
                        <input type="hidden" name="to" id="to_id" class="set_id">
                        <label for="height">To City</label>
                    </div>
                </div>
                <div class="col s12 m2 l2">
                    <div class="input-field">
                        <input id="journey_date" name="date" type="text" class="pikaday" autocomplete="off" value="{{date('Y-m-d')}}" data-parsley-date-diff="{{date('Y-m-d')}}" data-parsley-pattern="[0-9]{4}\-[0-9]{2}\-[0-9]{2}" data-inputmask="'mask': 'y-m-d'" required>
                        <label for="height">Journey Date</label>
                    </div>
                </div>
                <div class="col s12 m2 l2">
                    <div class="input-field">
                        <input id="id_nop" name="nop" type="number" data-parsley-max="4" data-parsley-min="1" autocomplete="off" value="1" required>
                        <label for="height">Number of person</label>
                    </div>
                </div>
                <div class="col s12 m2 l2">
                    <button class="btn" id="search_bus" >Search</button>
                </div>
                </form>
            </div>
            
            <div class="row route-search-result hidden-995" id="search_result">
                <div class="route-search-result-items"  id="result_search">
                    
                </div>
            </div>
        </div>
        <div class="col l1 m1"></div>
        
    </div>


@endsection