<?php 
$from_id = $body['param']['from_id'];
$to_id = $body['param']['to_id'];
$from_name = $body['param']['from_name'];
$to_name = $body['param']['to_name'];
$ft = $body['from']->lists('name','id');
$tt = $body['to']->lists('name','id');
//print_r($tt);
//$from_ter = $ft;
//$to_ter = $tt;
?>
<ul class="collapsible" data-collapsible="accordion">
    <li class="">
        <div class="collapsible-header active"> <b> {{ $from_name .'-'.$to_name }} </b>
                    <a id="" onClick="removeRoute(this);" style="cursor: pointer" class="default_color"> <i class="mdi-content-clear right small" ></i></a>
        </div>
        <div class="collapsible-body">
            <table id="{{ $from_id.'-'.$to_id }}" class="table">
<!--                <thead>
                    <tr>
                        <th colspan="6">{{ $from_name .'-'.$to_name }}
                            <a id="" onClick="removeRoute(this);" style="cursor: pointer" class="default_color"> <i class="mdi-content-clear right small" ></i></a>
                        </th>
                    </tr>
                </thead>-->
                <tbody>
                    <tr>
                        <th class="br bl bb bt"></th>
                        <td class="br bl bb bt">From Terminal </td>
                        <td class="br bl bb bt">
                            {{  Form::select('ter_from_'.$from_id.'-'.$to_id,$ft,'',['id'=>'ter_from_'.$from_id.'-'.$to_id,'class'=>'initialized']) }}
                        </td>
                        <td class="br bl bb bt">To Terminal</td>
                        <td class="br bl bb bt">
                            {{  Form::select('ter_to_'.$from_id.'-'.$to_id,$tt,'',['id'=>'ter_to_'.$from_id.'-'.$to_id,'class'=>'initialized']) }}
                        </td>
                        <td class="br bl bt bb"><a onclick="addTerminal( {{ $from_id }} , {{ $to_id }} ,'{{$from_name}}','{{$to_name}}')" class="btn btn-small default_color" style="color:white">Add</a></td>
                    </tr>
                    <tr>
                        <td  colspan="6" style="padding: 0px 80px;">
                            <table id="term_detail_{{ $from_id.'-'.$to_id }}" class="center" >
                                <tbody>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </li>
    
</ul>