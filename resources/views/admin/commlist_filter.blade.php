
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
//dd($data);
$result = $data['result'];
//dd($result);
if(count($result) > 0 ){ ?>
    <table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="text-align: center;">Booking ID</th>
            <th style="text-align: center;">Bus Operator</th>
            <th style="text-align: center;">Tanggal Berangkat</th>
            <th style="text-align: center;">Tanggal Pesan</th>
            <th style="text-align: center;">Rute Pejalanan</th>
            <th style="text-align: center;">User Type</th>
            <th style="text-align: center;">Nama Penumpang</th>
            <th style="text-align: center;">Seller Name</th>
            <th style="text-align: center;">Tipe Komisi</th>
            <th style="text-align: center;">No. Ticket</th>
            <th style="text-align: center;">Harga Tiket</th>
            <th style="text-align: center;">Komisi dari Service Provider</th>
            <th style="text-align: center;">Harga Net</th>
            <th style="text-align: center;">Komisi ke Seat Seller</th>
            <th style="text-align: center;">Handling Fee</th>
            <th style="text-align: center;">Pendapatan Bersih</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $t_ttl_amount = 0;
            $t_from_sp = 0;
            $t_net_price = 0;
            $t_to_ss = 0;
            $t_hf = 0;
            $t_net_income = 0;
            $total_ticket = 0;
            foreach($result as $r) { 
//                echo '<pre>';
//                print_r($r);
//                echo '</pre>';
                $to_ss = 0;
                $netprice = $r['booking']['total_amount'] - $r['com_amount'];
                $hf = 0;
                $total_ticket = $total_ticket + $r['booking']['nos'];
        ?>
        <tr>
            <td style="text-align: center;"><a href="{{URL::to($lt.'/ticket-details')}}/{{$r['booking_id']}}">{{$r['booking_id']}}</a></td>
            <td style="text-align: center;">{{ $r['sp']['first_name'] .' '. $r['sp']['last_name'] }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['booking']['pickup_date'])) }}</td>
            <td style="text-align: center;">{{ date('d-m-Y',strtotime($r['booking']['dropping_date'])) }}</td>
            <td style="text-align: center;">{{ $r['booking']['from_city'].' - '. $r['booking']['to_city']}}</td>
            <td style="text-align: center;">
            <?php 
                switch ($r['booked_by']) {
                    case 0:
                        echo 'User';
                        break;
                    case 1:
                        echo 'Admin';
                        break;
                    case 2:
                        echo 'Service Provider';
                        break;
                    case 3:
                        echo 'Service Provider A';
                        break;
                    case 4:
                        echo 'Service Provider B';
                        break;
                    case 5:
                        echo 'Seat Seller';
                        break;
                    case 6:
                        echo 'Seat Seller A';
                        break;
                    case 7:
                        echo 'Seat Seller B';
                        break;
                }
            ?>
            </td>
            <td style="text-align: center;">{{ $r['booking']['booker_name'] }}</td>
            <td style="text-align: center;">
            <?php 
                switch ($r['booked_by']) {
                    case 0:
                        echo 'Admin';
                        break;
                    case 1:
                        echo 'Admin';
                        break;
                    case 2:
                        echo '<a href="'.URL::to($lt.'/commission-management/sp/'.$r['sp']['id']).'" title="get commission">'. $r['sp']['first_name'].' '.$r['sp']['last_name'] .'</a>';
                        break;
                    case 3:
                        echo '<a href="'.URL::to($lt.'/commission-management/spa/'.$r['sp_comm']['sp']['id']).'" title="get commission">'.$r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'].'</a>';
                        break;
                    case 4:
                        echo '<a href="'.URL::to($lt.'/commission-management/spb/'.$r['sp_comm']['sp']['id']).'" title="get commission">'.$r['sp_comm']['sp']['first_name'].' '.$r['sp_comm']['sp']['last_name'].'</a>';
                        break;
                    case 5:
                        echo '<a href="'.URL::to($lt.'/commission-management/ss/'.$r['ss_comm'][0]['ss']['id']).'" title="get commission">'.$r['ss_comm'][0]['ss']['name'].'</a>';
                        break;
                    case 6:
                        echo '<a href="'.URL::to($lt.'/commission-management/ssa/'.$r['ss_comm'][0]['ss']['id']).'" title="get commission">'.$r['ss_comm'][0]['ss']['name'].'</a>';
                        break;
                    case 7:
                        echo '<a href="'.URL::to($lt.'/commission-management/ssb/'.$r['ss_comm'][0]['ss']['id']).'" title="get commission">'.$r['ss_comm'][0]['ss']['name'].'</a>';
                        break;
                }
            ?>
            </td>
            <td style="text-align: center;"><?php echo $r['comm_type'] == 'P' ? 'Percentage' : 'Fixed' ?></td>
            <td style="text-align: center;">{{ $r['booking']['nos'] }}</td>
            <td style="text-align: right;">{{ round($r['booking']['total_amount']) }}</td>
            <td style="text-align: right;">{{ round($r['com_amount']) }}</td>
            <td style="text-align: right;">{{ round($r['booking']['total_amount'] - $r['com_amount']) }}</td>
            <td style="text-align: right;">
                <?php 
                    if($r['booked_by'] == 5){
                        echo round($r['ss_comm'][0]['com_amount']);
                        $to_ss = $r['ss_comm'][0]['com_amount'];
                    }
                    elseif($r['booked_by'] == 6 || $r['booked_by'] == 7){
//                        dd($r['ss_comm']);
//                        foreach($r['ss_comm'] as $x){
                            
                            if($r['ss_comm'][1]['ss']['parent_id'] == 0){
                                echo round($r['ss_comm'][1]['com_amount']);
                                $to_ss = $r['ss_comm'][1]['com_amount'];
                            }
//                        }
                    }
                    else{
                        echo '0';
                    }
                ?>
            </td>
            <td style="text-align: right;">
                <?php 
                    if($r['booked_by'] == 5 || $r['booked_by'] == 6 || $r['booked_by'] == 7){
                        echo '0';
                    }
                    else{
                        echo round($r['handle_fee']);
                        $hf = $r['handle_fee'];
                    }
                    
                    $netincome = $r['com_amount'] - $to_ss + $hf;
                ?>
            </td>
            <td style="text-align: right;">{{ round($r['com_amount'] - $to_ss + $hf) }}</td>
            
            <?php
                $t_ttl_amount += $r['booking']['total_amount'] ;
                $t_from_sp += $r['com_amount'];
                $t_net_price += $netprice;
                $t_to_ss += $to_ss;
                $t_hf += $hf;
                $t_net_income += $netincome;
            ?>
            
        </tr>
        <?php } ?>
        <tr>
            <td colspan="9" style="text-align: center;"><h4 style="font-weight: bold;">Total</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ $total_ticket }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_ttl_amount,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_from_sp,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_net_price,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_to_ss,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_hf,3) }}</h4></td>
            <td style="text-align: right;"><h4 style="font-weight: bold;">{{ \General::number_format($t_net_income,3) }}</h4></td>
            
        </tr>
    </tbody>
</table>

<?php }else{ ?>
    <h2 style='color:red;'>No Data Found</h2>
<?php } ?>


<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />
<style>
    .get_child_comm{
        cursor: pointer;
        color: blue;
    }
    
</style>