@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('admin/card-filter',crnt,len,type,'','');
        
    };
    
    function delPrw(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Commuter ??';
        var onyes ='delData';
        var param='\'admin/delete-card\','+id+' , \'admin/card-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addprw(){
        
        $('#name').val('');
        $('#price').val('');
        $('#charge').val('');
        $('#bank_name').val('');
        $('#couriers').val('');
        $('#couriers').tagsInput({reset:true});
        $('#couriers').tagsInput(amenitTag());
        $('.tagsinput').css({border:'none'});
        $('#detail').html('');
        $('#functionality').html('');
        $('#use').html('');
        $('#card_image').attr('required',true);
        $('#modal-card').find('#save-banner').attr('onclick','addNewCard()');
        $('#modal-card').find('#save-banner').html('Add');
        $('#modal-card').openModal();
        $('#card_details').parsley().reset();
        
        Courier = {};
        
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
            CKEDITOR.instances[instance].setData('');
        }
        var chk = $('#cstatus').is(':checked');
        if(!chk){
            $('#cstatus').trigger('click');
        }
    }
    
    function upPrw(id){
        $('#cid').val(id);
        $('#name').val($('#name-'+id).html().trim());
        $('#price').val($('#price-'+id).html().trim());
        $('#charge').val($('#charge-'+id).html().trim());
        $('#bank_name').val($('#bank-'+id).html().trim());
        $('#couriers').val($('#courier-'+id).html().trim());
        $('#couriers').tagsInput({reset:true});
        $('#couriers').tagsInput(amenitTag());
        $('.tagsinput').css({border:'none'});
        $('#card_image').removeAttr('required');
        $('#modal-card').find('#save-banner').attr('onclick','editCard('+id+')');
        $('#modal-card').find('#save-banner').html('Update');
        $('#modal-card').openModal();
        $('#card_details').parsley().reset();
        Courier = {};
        
        var allA = $('#all_courier-'+id).val();
        if(allA.length > 0){
            var alA = allA.split(',');
            for(var i in alA){
                Courier[alA[i]] = alA[i];
            }
        }
        CKEDITOR.instances['detail'].setData($('#detail-'+id).text().trim());
        CKEDITOR.instances['functionality'].setData($('#functionality-'+id).text().trim());
        CKEDITOR.instances['use'].setData($('#use-'+id).text().trim());
        
//        CKEDITOR.instances['fleet_detail'].insertHtml($('#fleet_detail-'+id).html().trim());
//        CKEDITOR.instances['note'].insertHtml($('#note-'+id).html().trim());
        
        var status = $('#status-'+id).attr('name');
        var chk = $('#cstatus').is(':checked');
        if(status == 1 && !chk){
            $('#cstatus').trigger('click');
        }else if(status == 0 && chk){
            $('#cstatus').trigger('click');
        }
    }
    
    function addNewCard(){
        
        var url="{{URL::to('admin/add-card')}}";
        $('#card_details').attr('action',url);
//        console.log(Courier);
        $('#courier_array').val(JSON.stringify(Courier));
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/card-filter');
            }
            location.reload();
        });
    }
    function editCard(id){
//        alert('called');
        $('#courier_array').val(JSON.stringify(Courier));
       
        var url="{{URL::to('admin/edit-card')}}";
        $('#card_details').attr('action',url);
        
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        
        $('#card_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-card').closeModal();
                filterData('admin/card-filter');
            }
            location.reload();
        });
    }
   
   var couriers = {};
             
                 var aurl ='{{URL::to("admin/get-couriers")}}';
                 var adata = {};
                 postAjax(aurl,adata,function(res){
                     if(typeof res.flag != 'undefined' && res.flag !=1){
                         Materialize.toast('No couriers found', 2000,'rounded');
                     }
                     couriers = res.data;
//                    
                    $('#couriers').tagsInput(amenitTag());
                     $('.tagsinput').css({border:'none'});
                });
    function amenitTag(){
                   return {
//                        'autocomplete_url':url,
                        'autocomplete_data':couriers,
//                        'autocomplete_element':'couriers',
                        'height':'auto',
                        'width':'auto',
                        'autocomplete': {selectFirst:false,width:'auto',autoFill:true},
                        'interactive':true,
                        'defaultText':'add courier..',
                        'onAddTag':function(res){
                            var res_id = res.replace(/[^A-Za-z0-9]/gi, '_');
                            var tag_id = $('#'+res_id+'_id').val();
                            Courier[tag_id] = tag_id;
                        },
                        'onTagExist':function(res){
//                            console.log('exist');
                            Materialize.toast('This Courier '+res+' is already exist', 2000,'rounded');
                        },
                        'onRemoveTag':function(res){
                            var res_id = res.replace(/[^A-Za-z0-9]/gi, '_');
                            var tag_id = $('#'+res_id+'_id').val();
                            removeElement(tag_id);
//                            Materialize.toast('The Courier '+res+' is removed', 2000,'rounded');
                        },
//                        'onChange' : callback_function,
//                        'delimiter': [',',';'],   // Or a string with a single delimiter. Ex: ';'
                        'removeWithBackspace' : true,
                        'minChars' : 0,
                        'maxChars' : 0, // if not provided there is no limit
                        'placeholderColor' : '#666666'
                     };
    }
    function removeElement(key){
        if(Courier.hasOwnProperty(key)){
            delete Courier[key];
        }
//        console.log(key);
//        console.log(routes);
    }
    $(document).on('click','#search',function(){
        var status = $('#status :selected').val();
        var bank = $('#bank').val();
        var url = 'admin/card-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        if(bank != ''){
            data['bank_name'] = bank;
        }
//        pagination(url,data,'vtable');
        
        
        filterData('admin/card-filter','',data);
    });
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Commuter Management </h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Commuter Management</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter By Card Name</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="bank" type="text" class="validate" >
                        <label for="input_text">Filter By Bank</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="status">
                            <option value="">All</option>
                            <option value="0">InActive</option>
                            <option value="1">Active</option>
                        </select>
                        <label for="input_text">Filter By Status</label>
                        
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Commuter Management</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/card-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/card-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/card-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/card-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/card-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/card-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="card_details" action="{{URL::to('admin/add-card')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-card" class="modal bust-modal" style='width: 70%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Commuter Detail</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="cid" id="cid" value="" />
                <input type="hidden" name="opr" id='opr' value="Add" />
                <input type="hidden" name="courier_array" id='courier_array' value="" />    
                 
                    <div class="content">
                        
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <div class="input-field">

                                    <input id="name" name="name" type="text" placeholder="" autocomplete="off" required>
                                    <label for="sp_id">Card Name</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="input-field">

                                    <input id="price" name="price" type="number" placeholder="" autocomplete="off" required>
                                    <label for="city_id">Card Price</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <input id="charge" name="charge" type="number" placeholder=""  required>
                                    <label for="group">Shipping Charge</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <input id="bank_name" name="bank_name" type="text" placeholder=""  required>
                                    <label for="group">Bank Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field">
                                    <div class="btn"  style="width:100px;">
                                        <span class="">Card Image</span>
                                        <input type="file" name="card_image[]" id="card_image" accept="image/*" >
                                        <!--<label for="bnr_img">Upload Banner Image</label>-->
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="col s12 m12 l12">

                                    <input id="couriers" type="text" name="couriers" value="" class="validate" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        
                        <br>
                        
                        <label for="textarea1">Card Detail</label>
                        <div class="input-field">

                            <textarea  id="detail" name="detail" class="materialize-textarea"></textarea>
                          <script>
                          CKEDITOR.replace( 'detail' );
                          </script>
                        </div>
                        <br>
                        <label for="textarea1">Functionality </label>
                        <div class="input-field">

                            <textarea  id="functionality" name="functionality" class="materialize-textarea"></textarea>
                          <script>
                          CKEDITOR.replace( 'functionality' );
                          </script>
                        </div>
                        <br>
                        <label for="textarea1">How To Use Card </label>
                        <div class="input-field">

                            <textarea  id="use" name="use" class="materialize-textarea"></textarea>
                          <script>
                          CKEDITOR.replace( 'use' );
                          </script>
                        </div>
                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                    <input type="checkbox" id='cstatus' name='status' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>

                                </label>
                            </p>
                        </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-banner" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="addprw();"data-position="top" data-delay="50" data-tooltip="Add Commuter">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
@endsection