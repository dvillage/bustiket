<input type="hidden" id="total_person" value="{{$nop}}" />
<?php // dd($buses); ?>
<?php foreach ($buses as $key => $bus) { 
    if($key !== 'roda' && $key !== 'lorena'){
    ?>

    <div class="row route-item" data-bus_id="{{$bus['bus']['id']}}">
        <div class="route-subitem clearfix">
            <div class="route-subitem-content">
                <div class="route-subitem-box">
                    <figure class="route-item-ekspedisi" style="width: 111px;height: 25px">
                        <img src="{{$bus['bus']['sp']['avatar']}}" alt="" height="25px">
                    </figure>
                </div>
                <div class="route-subitem-box ekspedisi">
                    <span class="route-item-title">{{$bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']}}</span>
                    <span class="route-item-subtitle">{{$bus['bus']['name']}}</span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box berangkat">
                    <span class="route-item-title"><?php echo explode(" ", $bus['routes'][0]['boarding_time'])[1]; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap($bus['routes'][0]['from_terminal']['name'], 25, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box arrow">
                    <span class="sprite icon-arrow-right"></span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box tiba">
                    <span class="route-item-title"><?php echo explode(" ", $bus['routes'][0]['droping_time'])[1]; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap($bus['routes'][0]['to_terminal']['name'], 25, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

               
                <div class="route-subitem-box seat">
                    <span class="sprite icon-seat"></span>
                    <span class="route-item-subtitle">{{$bus['bus']['total_seats']}} Kursi</span>
                </div><!-- / .route-subitem-box -->

            </div><!-- / .route-subitem-content -->

            <div class="route-subitem-content">
                <div class="route-subitem-box cta">
                    <!--<a href="#">fasilitas</a>-->
                </div> <!-- / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Foto</a>-->
                </div> <!-- / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Rute</a>-->
                </div> <!--  / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Info</a>-->
                </div><!-- / .route-subitem-box -->

                
            </div> <!-- / .route-subitem-content -->
        </div>

        <div class="route-items-eksekusi">
            <span id="pricing_{{$bus['bus']['id']}}" class="pricing">Rp <?php echo \General::number_format($bus['bus']['price'],3)?></span>
            <input type="hidden" id="sp_id_{{$bus['bus']['id']}}" value="{{$bus['bus']['sp']['id']}}" />
            <input type="hidden" id="sp_name_{{$bus['bus']['id']}}" value="{{$bus['bus']['sp']['first_name'].' '.$bus['bus']['sp']['last_name']}}" />
            <input type="hidden" id="bus_name_{{$bus['bus']['id']}}" value="{{$bus['bus']['name']}}" />
            <input type="hidden" id="board_points_{{$bus['bus']['id']}}" value="{{json_encode($board_points[$bus['bus']['id']])}}" />
            <input type="hidden" id="drop_points_{{$bus['bus']['id']}}" value="{{json_encode($drop_points[$bus['bus']['id']])}}" />
            <input type="hidden" id="fare_{{$bus['bus']['id']}}" value="{{$bus['bus']['price']}}" />
            <input type="hidden" id="fare_format_{{$bus['bus']['id']}}" value="{{\General::number_format($bus['bus']['price'],3,".",".")}}" />
            <span class="btn btn-radius btn-orange view-seats" data-bus_id="{{$bus['bus']['id']}}" data-map="0">Pilih Kursi</span>
            <div class="loder" id="loader_{{$bus['bus']['id']}}" style="display: none;"><img src="{{URL::to('assets/img/ajax-loader.gif')}}"></div>
        </div>
    </div><!-- / .route-item -->
    <div class="route-item-detail clearfix" style="width: 102%;display: inline-block;margin-top: 1px;">
        
    </div>
    
<?php }else{
//    echo $key;
//    $bus['sp']['avatar'] = $bus['sp']['avatar'] == "" ? url("assets/images/sp/".$bus['sp']['avatar']."default.png") : url("assets/images/sp/".$bus['sp']['avatar']);
    $bus['sp']['avatar'] = url("assets/images/sp/default.png");
        foreach($bus['bus'] as $ky=>$bu){
            
            ?>
    <div class="row route-item" data-bus_id="{{$ky}}">
        <div class="route-subitem clearfix">
            <div class="route-subitem-content">
                <div class="route-subitem-box">
                    <figure class="route-item-ekspedisi" style="width: 111px;height: 25px">
                        <img src="{{$bus['sp']['avatar']}}" alt="" height="25px">
                    </figure>
                </div>
                <div class="route-subitem-box ekspedisi">
                    <span class="route-item-title">{{$bus['sp']['first_name'].' '.$bus['sp']['last_name']}}</span>
                    <span class="route-item-subtitle">{{$bu['bus']}}</span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box berangkat">
                    <span class="route-item-title"><?php echo current($bu['terminals'])[0]['departure_time'] ?> </span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap(current($bu['terminals'])[0]['board_point'], 20, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box arrow">
                    <span class="sprite icon-arrow-right"></span>
                </div><!-- / .route-subitem-box -->

                <div class="route-subitem-box tiba">
                    <span class="route-item-title"><?php echo current($bu['terminals'])[0]['arrival_time']; ?></span>
                    <span class="route-item-subtitle"><?php echo str_replace("*","&nbsp;&nbsp;",str_pad(wordwrap(current($bu['terminals'])[0]['drop_point'], 20, "<br />\n",true),25,"*",STR_PAD_BOTH)); ?></span>    
                </div><!-- / .route-subitem-box -->

               
                <div class="route-subitem-box seat">
                    <span class="sprite icon-seat"></span>
                    <span class="route-item-subtitle">{{$bu['total_seat']}} Kursi</span>
                </div><!-- / .route-subitem-box -->

            </div><!-- / .route-subitem-content -->

            <div class="route-subitem-content">
                <div class="route-subitem-box cta">
                    <!--<a href="#">fasilitas</a>-->
                </div> <!-- / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Foto</a>-->
                </div> <!-- / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Rute</a>-->
                </div> <!--  / .route-subitem-box -->

                <div class="route-subitem-box cta">
                    <!--<a href="#">Info</a>-->
                </div><!-- / .route-subitem-box -->

                
            </div> <!-- / .route-subitem-content -->
        </div>

        <div class="route-items-eksekusi">
            <span id="pricing_{{$ky}}" class="pricing">Rp <?php echo \General::number_format($bu['fare'],3)?></span>
            <input type="hidden" id="sp_id_{{$ky}}" value="{{$bus['sp']['id']}}" />
            <input type="hidden" id="sp_name_{{$ky}}" value="{{$bus['sp']['first_name'].' '.$bus['sp']['last_name']}}" />
            <input type="hidden" id="bus_name_{{$ky}}" value="{{$bu['bus']}}" />
            <input type="hidden" id="board_points_{{$ky}}" value="{{json_encode($board_points[$ky])}}" />
            <input type="hidden" id="drop_points_{{$ky}}" value="{{json_encode($drop_points[$ky])}}" />
            <input type="hidden" id="fare_{{$ky}}" value="{{$bu['fare']}}" />
            <input type="hidden" id="fare_format_{{$ky}}" value="{{\General::number_format($bu['fare'],3)}}" />
            <span class="btn btn-radius btn-orange view-seats" data-bus_id="{{$ky}}" data-map="0">Pilih Kursi</span>
            <div class="loder" id="loader_{{$ky}}" style="display: none;"><img src="{{URL::to('assets/img/ajax-loader.gif')}}"></div>
        </div>
    </div><!-- / .route-item -->
    <div class="route-item-detail clearfix" style="width: 102%;display: inline-block;margin-top: 1px;">
        
    </div>
    <?php
            
        }

    }
}
?>
