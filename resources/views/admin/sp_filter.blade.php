
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';
if($type == 'Main') { ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Logo </th>
            <th>Service Provider</th>
            <th>Email</th>
            <th>No. of Buses</th>
            <th>Contact Person</th>
            <th>Contact No.</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($result as $r) { ?>
        <tr>
            <?php if($r['avatar'] != ''){ ?>
            <td style="text-align:center;"><img src="{{URL::to('assets/images/sp/'.$r['avatar'])}}" id='img-{{$r['id']}}' name='{{$r['avatar']}}'style="height: 75px;width: 100px;"/></td>
            <?php }
            else{ ?>
            <td style="text-align:center;"><img src="{{URL::to('assets/images/sp/default.png')}}" id='img-{{$r['id']}}' name='default.png'style="height: 75px;width: 100px;"/></td>
            <?php } ?>
            <td>{{ $r['first_name'] .' '. $r['last_name'] }}</td>
            <td>{{ $r['email'] }}</td>
            <td><a href="{{URL::to($lt."/service-provider/buses/".$r['id'])}}"> {{ count($r['sp_buses']) }} View Buses</a></td>
            <td>{{ $r['sp_more'] ? $r['sp_more']['cp_name'] : '' }}</td>
            <td>{{ $r['sp_more'] ? $r['sp_more']['cp_mobile'] : '' }}</td>
            <td><a href="{{URL::to($lt."/update-provider/".$r['type']."/".$r['id'])}}" class="btn-floating btn-small blue" id='{{ $r['id'] }}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                <a class="btn-floating btn-small btn red modal-trigger" id='{{ $r['id'] }}' onclick="delSP({{ $r['id'] }});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            <?php echo $r['status'] == 1 ? '<i class="mdi-image-brightness-1 " style="color: green;" title="Active"></i>' : ($r['status'] == 2 ? '<i class="mdi-image-brightness-1 " style="color: orange;" title="Suspended"></i>' : '<i class="mdi-image-brightness-1 " style="color: red;" title="InActive"></i>' )?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php  }else{ ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Logo</th>
            <th>Service Provider Name</th>
            <th>Email</th>
            <th>City</th>
            <th>Contact No.</th>
            <th>Deposit Balance</th>
            <th>Commission</th>
            <th>Total Seat</th>
            <th>Transfer</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($result as $r) { ?>
        <tr>
            <?php if($r['avatar'] != ''){ ?>
            <td style="text-align:center;"><img src="{{URL::to('assets/images/sp/'.$r['avatar'])}}" id='img-{{$r['id']}}' name='{{$r['avatar']}}'style="height: 75px;width: 100px;"/></td>
            <?php }
            else{ ?>
            <td style="text-align:center;"><img src="{{URL::to('assets/images/sp/default.png')}}" id='img-{{$r['id']}}' name='default.png'style="height: 75px;width: 100px;"/></td>
            <?php } ?>
            <td>{{ $r['first_name'] .' '. $r['last_name'] }}</td>
            <td>{{ $r['email'] }}</td>
            <td>{{ $r['sp_more'] ? $r['sp_more']['city'] : '' }}</td>
            <td>{{ $r['sp_more'] ? $r['sp_more']['cp_mobile'] : '' }}</td>
            <td><a>{{ $r['balance'] }}</a></td>
            <td><a>0</a></td>
            <td><a>0</a></td>
            <td><a>Not Found</a></td>
            <td>
                <a href="{{URL::to($lt."/update-provider/".$r['type']."/".$r['id'])}}" class="btn-floating btn-small blue" id="{{$r['id']}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                <!--<a class="btn-floating btn-small red btn modal-trigger" href="#confirmDel" id= "{{$r['id']}}" onclick="modalOpen('<b>Confirmation<b>','Are you sure to Delete Service Provider ??','delData',  {{ $r['id'] }}  );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                <a class="btn-floating btn-small red btn modal-trigger" id= "{{$r['id']}}" onclick="delSP({{ $r['id'] }});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                
                <?php if($r['status']==1){ ?>
                        <i class="mdi-image-brightness-1 " style='color: green;'></i>
                    <?php }else{ ?> 
                        <i class="mdi-image-brightness-1 " style='color: red;'></i>
                    <?php }?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<?php } ?>
<input type="hidden" id="current" value="{{ $crnt_page }}" />
<input type="hidden" id="total_page" value="{{ $total_page }}" />
<input type="hidden" id="len" value="{{ $len }}" />