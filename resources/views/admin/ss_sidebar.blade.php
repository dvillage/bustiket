 <!--
  Yay Sidebar
  Options [you can use all of theme classnames]:
    .yay-hide-to-small         - no hide menu, just set it small with big icons
    .yay-static                - stop using fixed sidebar (will scroll with content)
    .yay-gestures              - to show and hide menu using gesture swipes
    .yay-light                 - light color scheme
    .yay-hide-on-content-click - hide menu on content click

  Effects [you can use one of these classnames]:
    .yay-overlay  - overlay content
    .yay-push     - push content to right
    .yay-shrink   - shrink content width
-->

  <aside class="yaybar yay-shrink yay-hide-to-small yay-gestures">

    <div class="top">
      <div>
        <!-- Sidebar toggle -->
        <a href="#" class="yay-toggle">
          <div class="burg1"></div>
          <div class="burg2"></div>
          <div class="burg3"></div>
        </a>
        <!-- Sidebar toggle -->

        <!-- Logo -->
        <a href="#!" class="brand-logo">
          <img src="{{URL::to('assets/img/bus_image/logo2.png')}}" alt="Con">
        </a>
        <!-- /Logo -->
      </div>
    </div>


    <div class="nano">
        <div class="nano-content">

            <ul>

                <li class="label">Menu</li>

                <li id='Dashboard'>
                    <a href="{{URL::to('ss/dashboard')}}" class="waves-effect waves-blue"><i class="fa fa-dashboard"></i> Dashboards</a>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-ticket"></i> Ticket<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='tkt-book'>
                            <a href="{{URL::to('ss/ticket-book-new')}}" class="waves-effect waves-blue"> Book New</a>
                        </li>
                        <li id='tkt-list'>
                            <a href="{{URL::to('ss/ticket-list')}}" class="waves-effect waves-blue"> List</a>
                        </li>
                    </ul>
                </li>
<!--                <li class="active">
                  <a href="{{URL::to('admin/dashboard')}}" class="yay-sub-toggle waves-effect waves-blue" ><i class="fa fa-dashboard"></i> Dashboards</a>
                </li>-->
                
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-device-dvr"></i> Seat Seller<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='ss-A'>
                            <a href="{{URL::to('ss/seatseller/A')}}" class="waves-effect waves-blue"> Seat Seller - A</a>
                        </li>
                        <li id='ss-B'>
                            <a href="{{URL::to('ss/seatseller/B')}}" class="waves-effect waves-blue"> Seat Seller - B</a>
                        </li>
                        <li id='ss-D'>
                            <a href="{{URL::to('ss/seatseller/D')}}" class="waves-effect waves-blue"> Seat Seller Deposit Request</a>
                        </li>
                        <li id='pay_req'>
                            <a href="{{URL::to('ss/payment-request')}}" class="waves-effect waves-blue"> Payment Management </a>
                        </li>
                    </ul>
                </li>
                <li id='coupon'>
                    <a href="{{URL::to('ss/coupon-code')}}" class="waves-effect waves-blue"><i class="mdi-action-receipt"></i> Coupon Code</a>
                </li>
                <li>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="mdi-action-assignment-ind"></i> Commission Management <span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li id='admin-comm'>
                            <a href="{{URL::to('ss/commission-management')}}" class="waves-effect waves-blue"> Commission Management</a>
                        </li>
                    </ul>
                </li>
                
                <li id='reporting'>
                    <a href="{{URL::to('ss/reports')}}" class="waves-effect waves-blue"><i class="fa fa-bar-chart"></i> Reporting</a>
                </li>
                    
            </ul>

        </div>
    </div>
  </aside>
  <!-- /Yay Sidebar -->


  <!-- Main Content -->
  <section class="content-wrap">
