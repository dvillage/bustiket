@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SS'?'ss':'admin';

//dd($body['data']['status']==1?'checked':'0');
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
    };
   
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Profile</h1>
                
            </div>
            
        </div>

    </div>
 
    <!-- /Breadcrumb -->
    <div class="row" >
        <div class="col l3 m12"></div>
        <div class="col l6 m12">
            <div class="card">
                <div class="title">
                    <h4>Profile</h4>
                    
                </div>
            <div class="content">
                <!-- Name -->
                <form id="profile" method="POST" action='{{URL::to($lt.'/edit-seatseller')}}' enctype="multipart/form-data" data-parsley-validate >
                    
                        
                        <div class="input-field">
                            <input id="ssname" name="ssname" type="text" placeholder="Seat-Seller Name" value="{{$body['data']['name']}}" required>
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="sid" id="sid" value="{{$body['data']['id']}}" />
                            <input type="hidden" name="type" id='type' value="{{$body['type']}}" />
                            <label for="group">Seat-Seller Name</label>
                        </div>
                        <?php
                        if($body['type']!='Main'){ 
                            
                                ?>
                        <input type="hidden" name="ssparent" id="ssparent" placeholder="" value="<?php echo config("constant.SS_PARENT") ?>" />
                                <input type="hidden" id="sslogin" value="1" />
                                             <?php
                            } ?>
                        <div class="">
                            <label for="height">Email</label><br>
                            <span>{{$body['data']['email']}}</span><br>
                        </div>
                        <div class="input-field">
                            <input id="sspassword" name="sspassword" type="text" placeholder="" >
                            <label for="sspassword">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="sscon" name="sscon" type="number" data-parsley-type="digits" data-parsley-length="[10,11]" value="{{$body['data']['mobile']}}" placeholder="Contact No." required>
                            <label for="height">Contact No.</label>
                        </div>
                        
                        <div class="input-field">
                            <input id="sscity" name="sscity" type="text" placeholder="City" value="{{$body['data']['city']}}" required>
                            <label for="width">City</label>
                        </div>
                        <div class="input-field">
                            
                        </div>
                        <div class="">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <label for="width">Commission Value & Type</label><br>
                                    <span>{{$body['data']['comm']}}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                                <!--</div>-->
                                <!--<div class="col l6 m6 s6">-->
                                    <span>
                                        <?php 
                                    if($body['data']['comm_type']=='P'){
                                        echo 'Percentage(%)';
                                    }else{
                                        echo 'Fixed';
                                    }
                                    ?>
                                    </span>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="input-field">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <input id="sshf" name="sshf" type="text" placeholder="Handlefee" value="{{$body['data']['handling_fee']}}" required>
                            <label for="width">Handlefee Value & Type</label>
                                </div>
                                <div class="col l6 m6 s6">
                                    <select id="hf_type" name="hf_type" required>
                                        <option value='0' disabled selected>Select Commission Type</option>
                                        <option value='F' <?php echo isset($body['data']['handling_fee_type']) && $body['data']['handling_fee_type']=='F'?'Selected':''; ?>>Fixed</option>
                                        <option value='P' <?php echo isset($body['data']['handling_fee_type']) && $body['data']['handling_fee_type']=='P'?'Selected':''; ?>>Percentage (%)</option>
                                    </select>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <label for="width">Balance</label><br>
                            <span>{{$body['data']['balance']}}</span><br><br>
                        </div>
                    <div class="row">
                      <div class="col s12">
                        <button class="btn">Update</button>
                        <a class="btn"  href='{{URL::to($lt.'/dashboard' )}}' >Cancel </a>
                      </div>
                    </div>
                    
                    <input type="hidden" name="profile" value="1"/>
                </form> 

            </div>
        </div>



      </div>
        <div class="col l3 m12"></div>

    </div>

@endsection