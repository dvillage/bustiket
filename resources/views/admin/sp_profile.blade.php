@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':'admin';

//dd($body['data']['status']==1?'checked':'0');
?>
<script type="text/javascript">
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        if($('#paypal').prop("checked") == true){
            $('#paypalblock').css('display','block');
        }
        

    };
    $(document).ready(function(){
        $('#paypal').change(function(){
            if($(this).prop("checked") == true){
                $('#paypalblock').css('display','block');
                $('#paypal_id').attr('required',true);
            }
            else if($(this).prop("checked") == false){
                $('#paypalblock').css('display','none');
                $('#paypal_id').removeAttr('required');
                $('#paypal_id').val('');
            }
        });
    });
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Profile</h1>
                
            </div>
            
        </div>

    </div>
 
    <!-- /Breadcrumb -->
    <div class="row" >
        <div class="col l3 m12"></div>
        <div class="col l6 m12">
            <div class="card">
                <div class="title">
                    <h4>Profile</h4>
                    
                </div>
            <div class="content">
                <!-- Name -->
                <form id="profile" method="POST" action='{{URL::to($lt.'/edit-service-provider')}}' enctype="multipart/form-data" data-parsley-validate >
                    
                    <?php if(isset($body['msg']) && $body['msg']!=""){ ?>   
                        <div class="alert pb-10 mb-15">
                            <strong><?php echo $body['msg']; ?></strong>
                        </div>   
                    <?php } ?>
                    

                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input type='hidden' name='type' id='type' value='<?php echo $body['type']?>'>
                                <input type='hidden' name='spid' id='spid' value='<?php echo $body['data']['id']?>'>
                                <input id="fname" name="fname" type="text" value='<?php echo isset($body['data']['first_name'])?$body['data']['first_name']:''; ?>'required>
                                <label for="fname">First Name</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="lname" name="lname" type="text" value='<?php echo isset($body['data']['last_name'])?$body['data']['last_name']:''; ?>' required>
                                <label for="lname">Last Name</label>
                            </div>
                        </div>
                    </div>
                    <?php if($body['type'] != 'Main'){
                        ?>
                    <div class="input-field">
                        <!--<input type="text" value='<?php echo config('constant.SP_NAME') ; ?>' readonly="">-->
                        <input type='hidden' name='parentsp' id='parentsp' value='<?php echo config('constant.SP_PARENT')?>'>
                        <!--<label for="lname">Service Provider</label>-->
                    </div>
                    <?php 
                       } ?>
                    <div class="">
                        
                        <label for="email">Email</label><br>
                      <span><?php echo isset($body['data']['email'])?$body['data']['email']:''; ?></span>
                    </div>
                    
                    <div class="input-field">
                      <input id="password" name="password" type="password" >
                      <label for="passowrd">Password</label>
                    </div>
                    
                    <div class="input-field">
                        <input id="vat" name="vat" type="text" value='<?php echo isset($body['data']['sp_more']['vat'])?$body['data']['sp_more']['vat']:''; ?>' data-parsley-type="number">
                      <label for="vat">VAT</label>
                    </div>
                    

                    
                    <div class="input-field">
                        <input id="address" name="address" type="text" value='<?php echo isset($body['data']['sp_more']['address'])?$body['data']['sp_more']['address']:''; ?>' required>
                      <label for="address">Address</label>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="city" name="city" type="text" value='<?php echo isset($body['data']['sp_more']['city'])?$body['data']['sp_more']['city']:''; ?>' required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="state" name="state" type="text" value='<?php echo isset($body['data']['sp_more']['state'])?$body['data']['sp_more']['state']:''; ?>' required>
                                <label for="state">State</label>
                            </div>
                        </div>
                        <div class="col l4 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="pincode" name="pincode" type="text" value='<?php echo isset($body['data']['sp_more']['postal_code'])?$body['data']['sp_more']['postal_code']:''; ?>' required data-parsley-type="digits">
                                <label for="pincode">Postal Code</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="landline" name="landline" type="text" value='<?php echo isset($body['data']['sp_more']['land_no'])?$body['data']['sp_more']['land_no']:''; ?>' required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="landline">Landline</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="fax" name="fax" type="text" value='<?php echo isset($body['data']['sp_more']['fax'])?$body['data']['sp_more']['fax']:''; ?>' >
                                <label for="fax">FAX</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="mobile1" name="mobile1" type="text" value='<?php echo isset($body['data']['sp_more']['mobile1'])?$body['data']['sp_more']['mobile1']:''; ?>' required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="mobile1">Mobile 1</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="mobile2" name="mobile2" type="text" value='<?php echo isset($body['data']['sp_more']['mobile2'])?$body['data']['sp_more']['mobile2']:''; ?>' required data-parsley-type="digits" data-parsley-minlength="10">
                                <label for="mobile2">Mobile 2</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <?php if($body['type'] != 'Main'){ ?>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <input id="handlefee" name="handlefee" type="text" value='<?php echo isset($body['data']['handling_fee'])?$body['data']['handling_fee']:''; ?>' required data-parsley-type="number">
                                <label for="handlefee">Handling Fee</label>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <select id='handletype' name='handletype' required >
                                    <option value="P" <?php echo isset($body['data']['handling_fee_type']) && $body['data']['handling_fee_type']=='P'?'Selected':''; ?> >Percentage (%)</option>
                                    <option value="F" <?php echo isset($body['data']['handling_fee_type']) && $body['data']['handling_fee_type']=='F'?'Selected':''; ?> >Flat</option>
                                </select>
                                <label for="handletype">Handling Type</label>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="">
                                
                                <label for="handlefee">Handling Fee</label><br>
                                <span><?php echo isset($body['data']['handling_fee'])?$body['data']['handling_fee']:''; ?></span>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="">
                                <label for="handletype">Handling Type</label><br>
                                <span>
                                    <?php 
                                    if($body['data']['handling_fee_type']=='P'){
                                        echo 'Percentage(%)';
                                    }else{
                                        echo 'Flat';
                                    }
                                    ?>
                                </span>
                            </div>
                        </div>
                        <br>
                        <?php } ?>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="">
                                <label for="cfsp">Given Commission</label><br>
                                <span><?php echo isset($body['data']['comm_fm_sp'])?$body['data']['comm_fm_sp']:''; ?></span>
                            </div>
                        </div>
                        <div class="col l6 m12" style='padding:0 10px 0 0 ;'>
                            <div class="">
                                <label for="cfsptype">Given Commission Type</label><br>
                                <span>
                                    <?php 
                                    if($body['data']['comm_fm_sp_type']=='P'){
                                        echo 'Percentage(%)';
                                    }else{
                                        echo 'Flat';
                                    }
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div><br>
                    
                    <div class="input-field">
                      <input id="cpname" name="cpname" type="text" value='<?php echo isset($body['data']['sp_more']['cp_name'])?$body['data']['sp_more']['cp_name']:''; ?>' required>
                      <label for="cpname">Contact Person Name</label>
                    </div>
                    
                    <div class="input-field">
                      <input id="cpno" name="cpno" type="text" value='<?php echo isset($body['data']['sp_more']['cp_mobile'])?$body['data']['sp_more']['cp_mobile']:''; ?>' required data-parsley-type="digits" data-parsley-minlength="10">
                      <label for="cpno">Contact Person No.</label>
                      
                    </div>
                    
                    <div class="input-field">
                        <textarea id="comment" name="comment" class="materialize-textarea"><?php echo isset($body['data']['sp_more']['comment'])?$body['data']['sp_more']['comment']:''; ?></textarea>
                      <label for="comment">Comment</label>
                      <input type="hidden" id='_token' name='_token' value='{{ csrf_token()}}'>
                    </div>
                    
                    <div class="file-field input-field">
                        <div class="btn small">
                            <span class="medium">Logo</span>
                            <input type="file" name="sp_logo" id="sp_logo" accept="image/*">
                            <!--<label for="bnr_img">Upload Banner Image</label>-->
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    
                    <div class="row" style='margin:0;' >
                        <div class="col s9 l6 m6" style='padding:0 10px 0 0 ;'>
                            <div class="input-field">
                                <label for="cfsp">Have Paypal Account ?</label>
                            </div>
                        </div>
                        <div class="col s3 l6 m6" style='padding:0px 10px 0 0 ;'>
                            <div class="input-field">
                                <p class="switch">
                                <label>
                                  <input type="checkbox" id='paypal' <?php echo isset($body['data']['sp_more']['paypal_email']) && $body['data']['sp_more']['paypal_email']!=''?'checked':''; ?> />
                                  <span class="lever"></span>
                                </label>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-field" id='paypalblock' style='display: none;margin-top: 40px;'>
                      <input id="paypal_id" name="paypal_id" value='<?php echo isset($body['data']['sp_more']['paypal_email'])?$body['data']['sp_more']['paypal_email']:''; ?>' type="text">
                      <label for="paypal_id">Paypal ID </label>
                    </div>
                    
                    <?php if($body['type'] != 'Main'){ ?>
                    <div class="row" >
                        <label for="paypal_id">Balance</label><br>
                        <span>{{$body['data']['balance']}}</span><br><br>
                    </div>
                    <?php } ?>
                    

                    <!-- Validation Button -->
                    <div class="row">
                      <div class="col s12">
                        <button class="btn">Update</button>
                        <a class="btn"  href='{{URL::to($lt.'/dashboard' )}}' >Cancel </a>
                      </div>
                    </div>
                    <!-- /Validation Button -->
                    <input type="hidden" name="profile" value="1"/>
                </form> 

            </div>
        </div>



      </div>
        <div class="col l3 m12"></div>

    </div>

@endsection