<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="loading">
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($header['title']) ? $header['title'] : ''; ?></title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="{{URL::to('assets/img/bus_image/bussel.png')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/pikaday/pikaday.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/jquery-clockpicker/jquery-clockpicker.min.css')}}" />
  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/nanoscroller.css')}}" />
  

  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/font-awesome.min.css')}}" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/material-design-icons.min.css')}}" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/ionicons.min.css')}}" />

  <!-- nvd3 -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/nv.d3.min.css')}}" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/prettify.css')}}" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/main.min.css')}}" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/light-green.min.css')}}" />
  
  
  <link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/common.css')}}" />
       
  <!--[if lt IE 9]>
    <script src="assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
  <?php 
  
    if (isset($header['css'])){
        for ($i = 0; $i < count($header['css']); $i++){
            if (strpos($header['css'][$i], "https://") !== FALSE || strpos($header['css'][$i], "http://") !== FALSE)
                echo '<link rel="stylesheet" type="text/css" href="' . $header['css'][$i] . '"/>';
            else
                echo '<link rel="stylesheet" type="text/css" href="' . \URL::to('assets/css/' . $header['css'][$i]) . '"/>';
        }
    }
    ?>
  
  
    <!-- jQuery -->
    <script type="text/javascript" src="{{URL::to('assets/js/jquery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{URL::to('assets/js/jquery.form.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('assets/js/common.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('assets/js/moment.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('assets/pikaday/pikaday.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('assets/pikaday/pikaday.jquery.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('assets/jquery-clockpicker/jquery-clockpicker.min.js')}}"></script>
    <!--<script type="text/javascript" src="{{URL::to('assets/js/materialize.js')}}"></script>-->
    @yield('header')
        <?php
    if (isset($header['js'])){
        for ($i = 0; $i < count($header['js']); $i++) {
            if (strpos($header['js'][$i], "https://") !== FALSE || strpos($header['js'][$i], "http://") !== FALSE)
                echo '<script type="text/javascript" src="' . $header['js'][$i] . '"></script>';
            else
                echo '<script type="text/javascript" src="' . \URL::to('assets/js/' . $header['js'][$i]) . '"></script>';
        }
    }
    $type = config('constant.LOGGER') == 'SS' ? 'ss' : (config('constant.LOGGER') == 'SP' ? 'sp' : 'admin');
     
    ?>
    
  <script>
    setTimeout(function(){
        document.getElementsByTagName( "html" )[0].classList.remove( "loading" );

        // All browsers
        document.getElementsByTagName( "html" )[0].className.replace( /loading/, "" );

        // Or with jQuery
        $( "html" ).removeClass( "loading" );
    }, 100);

    function postAjax(url,data,cb){
        
        var token='<?php echo csrf_token(); ?>';
        var jdata={_token:token};
        for(var k in data){
            jdata[k]=data[k];
        }
        $.ajax({
            type:'POST',
            url:url,
            data:jdata,
            success: function(data){
                if(typeof(data)==='object'){
                    if(data.flag==8){
                        window.location.replace("{{URL::to($type.'/login/scottiger')}}");
                    }
                    cb(data);
                }
                else{
                    cb(data);
                }
//                document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
//
//                // All browsers
//                document.getElementsByTagName( "html" )[0].className.replace( /loading/, "" );
//
//                // Or with jQuery
//                $( "html" ).removeClass( "loading" );
            }, 
//            error:function(){
//                document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
//
//                // All browsers
//                document.getElementsByTagName( "html" )[0].className.replace( /loading/, "" );
//
//                // Or with jQuery
//                $( "html" ).removeClass( "loading" );
//            }
        });
    }
</script>
  
</head>

<body>

    <input type="hidden" id="base_url" value="{{url('/')}}/" />
  <!--
  Top Navbar
    Options:
      .navbar-dark - dark color scheme
      .navbar-static - static navbar
      .navbar-under - under sidebar
-->
  <nav class="navbar-top">
    <div class="nav-wrapper">

      <!-- Sidebar toggle -->
      <a href="#" class="yay-toggle">
        <div class="burg1"></div>
        <div class="burg2"></div>
        <div class="burg3"></div>
      </a>
      <!-- Sidebar toggle -->

      <!-- Logo -->
      <a href="{{URL::to($type.'/dashboard')}}" class="brand-logo">
        <img src="{{URL::to('assets/img/bus_image/logo2.png')}}" alt="Con">
      </a>
      <!-- /Logo -->

      <!-- Menu -->
      <ul>
        <li class="user">
          <a class="dropdown-button" href="#!" data-activates="user-dropdown">
            <!--<img src="{{URL::to('assets/img/bus_image/logo2.png')}}" alt="John Doe" class="circle">Admin<i class="mdi-navigation-expand-more right"></i>-->
              <span class="mdi-action-account-circle"  style="font-size: 45px;color:#9ccc65;display:inline-block;"></span>{{config('constant.LOGGER') =='SS' ? config('constant.SS_NAME') : (config('constant.LOGGER') =='SP' ? config('constant.SP_NAME') : 'Admin') }}<i class="mdi-navigation-expand-more right"></i>
          </a>

          <ul id="user-dropdown" class="dropdown-content">
            <li><a href="{{URL::to($type.'/profile')}}"><i class="fa fa-user"></i> Profile</a>
            </li>
            <li class="divider"></li>
            <li><a href="{{URL::to($type.'/logout')}}"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- /Menu -->

    </div>
  </nav>
  <!-- /Top Navbar -->
