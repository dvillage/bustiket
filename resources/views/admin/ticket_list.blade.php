@extends('layouts.admin')

@section('content')
<?php // dd($body['city']); 
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
$id = 0;
$book_by = -1;
if(isset($body['filter']) && count($body['filter'])>0){
    $id = $body['filter']['id'];
    $book_by = $body['filter']['book_by'];
}

?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    $(document).ready(function () {
        base_url = getBaseURL();
        var flag = 0;
//        $(document).on('keyup','#from_city,#to_city',function(){
//        $(document).on('keyup','.autocomplete',function(){
//            var obj = $(this);
//            var s = $(this).val();
//            if(s.length < 2){
//                flag = 1;
//                return false;
//            }
//            if(flag == 1){
//                flag = 0;
//               var url = base_url+"services/city-name";
//               var data = {name:s};
//               postAjax(url,data,function(res){
//                   if(res.flag == 1){
//                       $(obj).autocomplete({
//                        data: res.data,
//                        limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
//                        onAutocomplete: function(val,key) {
//                            if(key > 0){
//                                obj.next().val(val);
//                            }else if($('#sp_filter').val() == ''){
//                                obj.next().val('');
//                            }
//
//                        },
//                        minLength: 2, // The minimum length of the input for the autocomplete to start. Default: 1.
//                    });
//                   }
//               });
//            }
//        });

        $('.autocomplete').autocomplete({
            data: <?php echo $body['city']; ?>,
            limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
            onAutocomplete: function(val) {
              console.log(val);
            },
            minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
        });

    });
    
    
    
    
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');
        
        var id = '{{$id}}';
        var book_by = '{{$book_by}}';
        
        if(id > 0 && book_by > -1){
            var filter = {
                'user_id'  :id,
                'book_by'  :[book_by]
            };
            
            return filterData('{{$lt}}/tlist-filter',len,filter);
        }else{
            getData('{{$lt}}/tlist-filter',crnt,len,type,'','');
        }
        
        
    };
    
    function filterBy(c='',len='',elem=''){
        var filter = {
            'tid'  :$('#tid').val(),
            'date'  :$('#filter_date').val(),
            'from'  :$('#fromcity').val(),
            'to'    :$('#tocity').val(),
        };
//        console.log(c);
//        console.log(len);
//        console.log(elem);

        var id = '{{$id}}';
        var book_by = '{{$book_by}}';
        
        if(id > 0 && book_by > -1){
            filter['user_id']  = id;
            filter['book_by']  = [book_by];
               
        }
        if(c === 'w'){
            filterDataWith('{{$lt}}/tlist-filter',elem,filter)
        }
        else{
            return filterData('{{$lt}}/tlist-filter',len,filter);
        }
    }
    
    function tktApprove(id){
        console.log(id);
        base_url = getBaseURL();
        var flag = 0;
        var url = base_url+"{{$lt}}/approve-ticket";
        var data = {id:id};
        postAjax(url,data,function(res){
                console.log(res);
                if(typeof data.flag !== 'undefined' && data.flag != 1){
                    Materialize.toast(data.msg, 2000,'rounded');
                    return false;
                }
                Materialize.toast(data.msg, 2000,'rounded');
                filterData('{{$lt}}/tlist-filter');
        });
    }
    
    function delTicket(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Ticket ??';
        var onyes ='delData';
        var param='\'{{$lt}}/del-ticket\',"'+id+'" , \'{{$lt}}/tlist-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function refTicket(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Refund this Ticket ??';
        var onyes ='delData';
        var param='\'{{$lt}}/refund-ticket\',"'+id+'" , \'{{$lt}}/tlist-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function mdlApprove(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to mark Ticket as Booked (Travel) ??';
        var onyes ='tktApprove';
        var param="'"+id+"'";
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Ticket List</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Ticket List</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="tid" name="" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">By Ticket No.</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="filter_date" name="filter[date]" class="pikaday" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">By Travel Date</label>
                    </div>

                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="fromcity" class="autocomplete" name="filter[fromcity]" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">By From City Name</label>
                    </div>

                    <div class="input-field" style='margin: 15px 0 0 10px;width:22%;display:inline-block;'>
                        <input id="tocity" class="autocomplete" name="filter[tocity]" type="text" class="validate" onchange="return filterBy();">
                        <label for="input_text">By To City Name</label>
                    </div>
                    <br>
                    <div class="input-field" style='width:50%;display:inline-block;'>
                        <!--<a class="btn" style="float:right;width: 80px;" onclick="return filterData('admin/tlist-filter','',$('[name = \"filter\"]'));"><i class="fa fa-search"></i></a>-->
                        <a class="btn" style="float:right;width: 80px;" onclick="return filterBy();"><i class="fa fa-search"></i></a>
                    </div>
                    
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--<h3>No Data Found !!</h3>-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                        </table>
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
<!--                        <a class="btn" id="first" onclick="filterDataWith('admin/tlist-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/tlist-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/tlist-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/tlist-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/tlist-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/tlist-filter',this.value);">-->
                        <a class="btn" id="first" onclick="filterBy('w','',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterBy('w','',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterBy();">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterBy('w','',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterBy('w','',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterBy('',this.value);">
                            <option value="10">10</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal" style="width: 25%;">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
       
    </div>

@endsection