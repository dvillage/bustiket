
<?php  $user = $body['user'] ; ?>
<h4>User Detail of : {{$user['name']}}</h4>       
<div class="row">
    <table >
        <tr>
            <td>Status</td>
            <td id="status_d">
                <select id="e-status">
                    <option value="0" <?php echo $user['status'] == 0 ? 'Selected':'' ?> >InActive</option>
                    <option value="1" <?php echo $user['status'] == 1 ? 'Selected':'' ?> >Active</option>
                    <option value="2" <?php echo $user['status'] == 2 ? 'Selected':'' ?> >Suspend</option>
                </select>
            </td>
            <td>Name</td>
            <td>{{$user['name']}}</td>
        </tr>
        <tr>
            <td>Mobile</td>
            <td>{{$user['mobile']}}</td>
            <td>Email ID</td>
            <td>{{$user['email']}}</td>
        </tr>
        <tr>
            <td>Wallet Balance</td>
            <td>{{(int)$user['wb']}}</td>
            <td>Birth Date</td>
            <td>{{date('d-m-Y',strtotime($user['dob']))}}</td>
        </tr>
            <tr>
                <td>Gender</td>
                <td>{{$user['gender'] =='M' ? 'Male' : 'Female'}}</td>
                <td>Created At</td>
                <td>{{date('d-m-Y H:i:s',strtotime($user['created_at']))}}</td>
            </tr>
            <tr>
                <td>Last Login Ip</td>
                <td>{{$user['last_ip']}}</td>
                <td>Last Login</td>
                <td>{{date('d-m-Y H:i:s',strtotime($user['login_date']))}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><button class="btn left" id="edit" onclick="editUser({{$user['id']}});">Save</button></td>
            </tr>
    </table>
    
</div>
<script>
    $('ul.tabs').tabs();
    $('#e-status').material_select();
    $('#status_d .select-wrapper').css({width:'25%'});
//    $('.collapsible').collapsible();
</script>
