@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Amenity = {};
    
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/pariwisata-filter',crnt,len,type,'','');
        
    };
    
    function showBtnFooter(){
        $("#btnFooter").show();
    }
    
    function showBtnHeader(){
         $("#btnFooter").hide();
    }
    
    function delPrw(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Pariwisata ??';
        var onyes ='delData';
        var param='\'{{$lt}}/delete-pariwisata\','+id+' , \'{{$lt}}/pariwisata-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addprw(){
        var lt = '{{$lt}}';
        if(lt != 'sp'){
            $('#sp_id').val('');
        }
        $("#btnFooter").hide();
        
        
//        $('#sp_id').material_select();
        $('#city_id').val('');
//        $('#city_id').material_select();
        $('#price').val('');
        $('#engine').val('');
        $('#manufacturer').val('');
        $('#amenities').val('');
        $('#amenities').tagsInput({reset:true});
        $('#amenities').tagsInput(amenitTag());
        $('.tagsinput').css({border:'none'});
        $('#fleet_detail').html('');
        $('#note').html('');
        $('#bus_img').attr('required',true);
        $('#main_image_pre').html('');
        $('#other_image_pre').html('');
        $('#modal-pariwisata').find('.save-banner').attr('onclick','addNewPariwisata()');
        $('#modal-pariwisata').find('.save-banner').html('Add');
        
        $('#modal-pariwisata').openModal();
        $('#pariwisata_details').parsley().reset();
        
        Amenity = {};
        
        
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
            CKEDITOR.instances[instance].setData('');
        }
        var chk = $('#ac').is(':checked');
        if(!chk){
            $('#ac').trigger('click');
        }
        var j = 0;
        $('.p_pricing').each(function(){
            if(j > 0){
                $(this).remove();
            }
            $(this).find('.p_houre').val('');
            $(this).find('.p_city').val('');
            $(this).find('.p_seat_count').val('');
            $(this).find('.p_price').val('');
            $(this).find('.id').val('');            
            j++;
            
        });
    }
    var removedImg = {};
    function upPrw(id){
        $('#pid').val(id);
        $('#sp_id').val($('#sp-'+id).attr('name'));
        $('#sp_filter').val($('#sp-'+id).html());
        $('#city_id').val($('#city-'+id).attr('name'));
        $('#city_filter').val($('#city-'+id).html());
        $('#bus_type').val($('#btype-'+id).html().trim());
        $('#bus_type').material_select();
        $('#price').val($('#price-'+id).html().trim());
        $('#engine').val($('#engine-'+id).html().trim());
        $('#manufacturer').val($('#manufacurer-'+id).html().trim());
        $('#amenities').val($('#amenity-'+id).html().trim());
        $('#amenities').tagsInput({reset:true});
        $('#amenities').tagsInput(amenitTag());
        $('.tagsinput').css({border:'none'});
        $('#main_image_pre').html('<img src="'+$('#img-'+id).attr('src')+'" style="height:30px;width:30px;" />');
        $('#other_image_pre').html('');
        $('.oimg-'+id).each(function(){
            $('#other_image_pre').append('<span style="height:50px;width:50px;position:relative;"><img src="'+$(this).attr('src')+'" style="height:30px;width:30px;" /> <span style="position:relative;top:-25px;color:red;cursor:pointer;" name="'+$(this).attr('name')+'" onclick="removeImg(this);">x</span> </span>&nbsp;');
        });
        $('#bus_img').removeAttr('required');
        $('#modal-pariwisata').find('.save-banner').attr('onclick','editPariwisata('+id+')');
        $('#modal-pariwisata').find('.save-banner').html('Update');
        $('#modal-pariwisata').openModal();
        $('#pariwisata_details').parsley().reset();
        Amenity = {};
        removedImg = {};
        var allA = $('#all_amenity-'+id).val();
        if(allA.length > 0){
            var alA = allA.split(',');
            for(var i in alA){
                Amenity[alA[i]] = alA[i];
            }
        }
        CKEDITOR.instances['fleet_detail'].setData($('#fleet_detail-'+id).text().trim());
        CKEDITOR.instances['note'].setData($('#note-'+id).text().trim());
        
//        CKEDITOR.instances['fleet_detail'].insertHtml($('#fleet_detail-'+id).html().trim());
//        CKEDITOR.instances['note'].insertHtml($('#note-'+id).html().trim());
        
        var status = $('#ac-'+id).attr('name');
        var chk = $('#ac').is(':checked');
        if(status == 1 && !chk){
            $('#ac').trigger('click');
        }else if(status == 0 && chk){
            $('#ac').trigger('click');
        }
        var j = 0;
        $('.p_pricing').each(function(){
            if(j > 0){
                $(this).remove();
            }
            j++;
            
        });
        
        
        var pr = JSON.parse($('#all_prices-'+id).val());
//        console.log(pr.length);
        if(pr.length > 0){
            for(var i in pr){
//                console.log(pr[i]);
                var par = $('.add_more').parent().parent();
                par.find('.p_houre').val(pr[i]['hour']);
                 par.find('.p_city').val(pr[i]['direction']);
                 par.find('.p_bus_type').val(pr[i]['type']);
                 par.find('.p_seat_count').val(pr[i]['seat_count']);
                 par.find('.p_price').val(pr[i]['price']);
                 par.find('.id').val(pr[i]['id']);
                 $('.add_more').trigger('click');
                 
                 
            }
        var j = 0;
        $('.p_pricing').each(function(){
            if(j == 0){
                $(this).find('.p_houre').val('');
                $(this).find('.p_city').val('');
                $(this).find('.p_seat_count').val('');
                $(this).find('.p_price').val('');
                $(this).find('.id').val('');
            }
            j++;
        });
        }else{
            var par = $('.add_more').parent().parent();
            par.find('.p_houre').val('');
             par.find('.p_city').val('');
             par.find('.p_seat_count').val('');
             par.find('.p_price').val('');
             par.find('.id').val('');
        }
    }
    function removeImg(obj){
        var img = $(obj).attr('name');
        removedImg[img] = img;
        $(obj).parent().remove();
        
    }
    
    function addNewPariwisata(){
        
        var url="{{URL::to($lt.'/add-pariwisata')}}";
        $('#pariwisata_details').attr('action',url);
        $('#amenity_array').val(JSON.stringify(Amenity));
//        $('#all_price').val(JSON.stringify(pricing));
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        
        $('#pariwisata_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-pariwisata').closeModal();
                filterData('{{$lt}}/pariwisata-filter');
            }
            location.reload();
        });
    }
    function editPariwisata(id){
//        alert('called');
        $('#amenity_array').val(JSON.stringify(Amenity));
        $('#removed_images').val(JSON.stringify(removedImg));
//        $('#all_price').val(JSON.stringify(pricing));
        var url="{{URL::to($lt.'/edit-pariwisata')}}";
        $('#pariwisata_details').attr('action',url);
        
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        
        $('#pariwisata_details').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-pariwisata').closeModal();
                filterData('{{$lt}}/pariwisata-filter');
            }
            location.reload();
        });
    }
   
   var amenities = {};
             
                 var aurl ='{{URL::to($lt."/get-amenites")}}';
                 var adata = {};
                 postAjax(aurl,adata,function(res){
                     if(typeof res.flag != 'undefined' && res.flag !=1){
                         Materialize.toast('No amenities found', 2000,'rounded');
                     }
                     amenities = res.data;
//                    
                    $('#amenities').tagsInput(amenitTag());
                     $('.tagsinput').css({border:'none'});
                });
    function amenitTag(){
                   return {
//                        'autocomplete_url':url,
                        'autocomplete_data':amenities,
//                        'autocomplete_element':'amenities',
                        'height':'auto',
                        'width':'auto',
                        'autocomplete': {selectFirst:false,width:'auto',autoFill:true},
                        'interactive':true,
                        'defaultText':'type here..',
                        'onAddTag':function(res,key){
                            var res_id = res.replace(/[^A-Za-z0-9]/gi, '_');
                            var tag_id = $('#'+res_id+'_id').val();
                            Amenity[tag_id] = tag_id;
//                            console.log(Amenity);
                        },
                        'onTagExist':function(res){
//                            console.log('exist');
                            Materialize.toast('This amenity '+res+' is already exist', 2000,'rounded');
                        },
                        'onRemoveTag':function(res){
                            var res_id = res.replace(/[^A-Za-z0-9]/gi, '_');
                            var tag_id = $('#'+res_id+'_id').val();
                            removeElement(tag_id);
//                            Materialize.toast('The amenity '+res+' is removed', 2000,'rounded');
                        },
//                        'onChange' : callback_function,
//                        'delimiter': [',',';'],   // Or a string with a single delimiter. Ex: ';'
                        'removeWithBackspace' : true,
                        'minChars' : 0,
                        'maxChars' : 0, // if not provided there is no limit
                        'placeholderColor' : '#666666'
                     };
    }
    function removeElement(key){
        if(Amenity.hasOwnProperty(key)){
            delete Amenity[key];
        }
//        console.log(key);
//        console.log(routes);
    }
    $(document).on('click','#search',function(){
        var city = $('#city').val();
        var busT = $('#busT :selected').val();
        var ac = $('#AC :selected').val();
        var url = '{{$lt}}/pariwisata-filter';
        var data = {};
        
        if(city != ''){
            data['city'] = city;
        }
        if(busT != ''){
            data['bus_type'] = busT;
        }
        if(ac != ''){
            data['ac'] = ac;
        }
//        pagination(url,data,'vtable');
        
        
        filterData('{{$lt}}/pariwisata-filter','',data);
    });
    var flag = 0;
    $(document).on('keyup','#sp_filter',function(){
        var s = $(this).val();
        if(s.length < 2){
            flag = 1;
            return false;
        }
        if(flag == 1){
            flag = 0;
           var url = '{{URL::to($lt."/sp-name")}}';
           var data = {name:s};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#sp_filter').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#sp_id').val(key);
                        }else if($('#sp_filter').val() == ''){
                            $('#sp_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
    var flag1 = 0;
    $(document).on('keyup','#city_filter',function(){
        var s = $(this).val();
        if(s.length < 2){
            flag1 = 1;
            return false;
        }
        if(flag1 == 1){
            flag1 = 0;
           var url = '{{URL::to($lt."/city-name")}}';
           var data = {name:s};
           postAjax(url,data,function(res){
               if(res.flag == 1){
                   $('#city_filter').autocomplete({
                    data: res.data,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val,key) {
                        if(key > 0){
                            $('#city_id').val(key);
                        }else if($('#city_filter').val() == ''){
                            $('#city_id').val('');
                        }
                        
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
               }
           });
        }
    });
    
    $(document).on('click','.add_more',function(){
        var par = $(this).parent().parent();
        var par_apnd = par;
        var hour = par.find('.p_houre').val();
        var city = par.find('.p_city').val();
        var type = par.find('.p_bus_type').val();
        var seat = par.find('.p_seat_count').val();
        var price = par.find('.p_price').val();
        var id = par.find('.id').val();
        if(hour == ''){
            Materialize.toast('Please enter hours', 2000,'rounded');
            return false;
        }else if(city == ''){
            Materialize.toast('Please enter city', 2000,'rounded');
            return false;
        }else if(seat == ''){
            Materialize.toast('Please enter seats', 2000,'rounded');
            return false;
        }else if(price == ''){
            Materialize.toast('Please enter price', 2000,'rounded');
            return false;
        }
        
        var cnt = par.parent().find('.remove').length;
        var key = Math.random().toString(36).substr(2, 5);
//        console.log(cnt);
//        par.append(par.clone());
//        par_apnd.insertAfter(par);
        par.clone().insertBefore(par_apnd);
//        par.clone().insertAfter(par_apnd);
        
        var j = 0;
        $('.p_pricing').each(function(){
            if(j == 0){
                $(this).find('.p_houre').val('');
                $(this).find('.p_city').val('');
                $(this).find('.p_bus_type').val('');
                $(this).find('.p_seat_count').val('');
                $(this).find('.p_price').val('');
                $(this).find('.id').val('');
            }
            j++;
        });
        
        $(this).html('');
        $(this).removeClass('btn add_more').append('<i class="fa fa-trash-o btn red remove" onClick="removePrice(this,\''+key+'\');"></i>');
         $(this).parent().parent().find('.p_houre').attr('required','true');
         $(this).parent().parent().find('.p_city').attr('required','true');
         $(this).parent().parent().find('.p_bus_type').attr('required','true')
         $(this).parent().parent().find('.p_price').attr('required','true');
         $(this).parent().parent().find('.p_price').attr('required','true');
//        $('.p_bus_type').material_select();
    });

    function removePrice(obj,key){
        $(obj).parent().parent().parent().remove();
    }
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Pariwisata</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Pariwisata</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter By Service Provider Name</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="city" type="text" class="validate">
                        <label for="input_text">Filter By City Name</label>
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="busT">
                            <option value="">All</option>
                            <option value="Big Bus">Big Bus</option>
                            <option value="Mini Bus">Mini Bus</option>
                        </select>
                        <label for="input_text">Filter By Bus Type</label>
                        
                    </div>
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="AC">
                            <option value="">All</option>
                            <option value="0">Non AC</option>
                            <option value="1">AC</option>
                        </select>
                        <label for="input_text">Filter By AC</label>
                        
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Pariwisata</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/pariwisata-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/pariwisata-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/pariwisata-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/pariwisata-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/pariwisata-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('{{$lt}}/pariwisata-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="pariwisata_details" action="{{URL::to($lt.'/add-pariwisata')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div id="modal-pariwisata" class="modal bust-modal" style='width: 70%;font-size:100%;overflow-y: scroll;overflow-x: hidden;'>
          <div class="modal-content">
              <div class="row" >
              <div class="col s12">
                  <ul class="tabs" style="overflow-x: hidden;">
                    <li class="tab col s3"><a class="" onclick="showBtnHeader();" href="#pariwisata_detail">Pariwisata Detail</a></li>
                  <li class="tab col s3"><a class="" onclick="showBtnFooter();" href="#p_price">Pricing</a></li>
                </ul>
              </div>
              <div id="pariwisata_detail" class="col s12">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Pariwisata</h4>
                        <div class="modal-footer">
                            
                            <a id='mdlaborth'class="modal-action modal-close waves-effect waves-red btn-flat mdlabort"> Cancel</a>
               
                            <button id="save-bannerh" class="modal-action waves-effect waves-green btn-flat save-banner"> Save</button>
                            
                        </div>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="pid" id="pid" value="" />
                <input type="hidden" name="opr" id='opr' value="Add" />
                <input type="hidden" name="amenity_array" id='amenity_array' value="" />    
                <input type="hidden" name="removed_images" id='removed_images' value="" />    
                    <div class="content">
                        
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <?php
                                    if($lt == 'sp'){
                                        $sp_id = config('constant.CURRENT_LOGIN_ID');
                                        $sp_name = config('constant.SP_NAME');
                                        
                                        ?>
                                    <input id="" name="sp_filter" type="text" value="{{$sp_name}}" placeholder="" autocomplete="off" readonly="">
                                    <input id="sp_id" name="sp_id" value="<?php echo $sp_id ?>" type="hidden">
                                    <label for="sp_id">Service Provider</label>
                                    <?php
                                    
                                    }else{
                                    ?>
                                    <input id="sp_filter" name="sp_filter" type="text" placeholder="" autocomplete="off" required>
                                    <input id="sp_id" name="sp_id" type="hidden">
                                    <label for="sp_id">Service Provider</label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="input-field">

                                    <input id="city_filter" name="city_filter" type="text" placeholder="" autocomplete="off" required>
                                    <input id="city_id" name="city_id" type="hidden">
                                    <label for="city_id">City</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <select id="bus_type" name="bus_type" required="">
                                        <!--<option value="" disabled selected>Select Height Type</option>-->
                                        <option value="Mini Bus">Mini Bus</option>
                                        <option value="Big Bus">Big Bus</option>
                                    </select>
                                    <label for="sp_id">Bus Type</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 ">
                                <div class="input-field">
                                    <input id="price" name="price" type="number" placeholder=""  required>
                                    <label for="group">Price</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field">
                                    <div class="btn"  style="width:100px;">
                                        <span class="">Bus Image</span>
                                        <input type="file" name="bus_img[]" id="bus_img" accept="image/*" ="">
                                        <!--<label for="bnr_img">Upload Banner Image</label>-->
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="file-field input-field small">
                                    <div class="btn" style="width:100px;">
                                        <span class="">Other Images</span>
                                        <input type="file" name="other_img[]" id="other_img" multiple="" accept="image/*" ="">
                                        <!--<label for="bnr_img">Upload Banner Image</label>-->
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6">
                                <div id="main_image_pre"></div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div id="other_image_pre"></div>
                            </div>
                        </div>
                        <div class="row" style="display: inline">
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <input id="engine" name="engine" placeholder="" type="text" >
                                    <label for="height">Engine</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l6">
                                <div class="input-field">
                                    <input id="manufacturer" name="manufacturer" type="text" placeholder=""  >
                                    <label for="group">Body Manufacturer</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display:inline;">
                        <div class="col s12 m12 l12">
                            <div class="col s12 m1 l1" style="padding: 0px;margin-top: 10px">Amenities : </div>
                            <div class="col s12 m12 l12">
                                
                                <input id="amenities" type="text" name="amenities" value="" class="validate" autocomplete="off" >
                            </div>
                        </div>
                    </div>
                        <br>
                        
                        <label for="textarea1">Fleet Detail</label>
                        <div class="input-field">

                            <textarea  id="fleet_detail" name="fleet_detail" class="materialize-textarea"></textarea>
                          <script>
                          CKEDITOR.replace( 'fleet_detail' );
                          </script>
                        </div>
                        <br>
                        <label for="textarea1">Note </label>
                        <div class="input-field">

                            <textarea  id="note" name="note" class="materialize-textarea"></textarea>
                          <script>
                          CKEDITOR.replace( 'note' );
                          </script>
                        </div>
                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>AC : </span>
                                    <input type="checkbox" id='ac' name='ac' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>

                                </label>
                            </p>
                        </div>
                    </div>
              </div>
                  <div id="p_price" class="col s12">
                      <input type="hidden" id="all_price" name="all_price" />
                    <div class="row p_pricing">
                          <div class="col s12 m2 l2">
                                <div class="input-field">
                                    <input  class="p_houre" name="p_hour[]" type="number" placeholder="" autocomplete="off" >
                                    <label for="sp_id">Enter jam</label>
                                </div>
                          </div>
                          <div class="col s12 m2 l2">
                                <div class="input-field">
                                    <input class="p_city" name="p_city_name[]" placeholder="" type="text" >
                                    <label for="city_id">Enter Tujuans</label>
                                </div>
                          </div>
                          <div class="col s12 m2 l2">
                                <div class="input-field">
                                    <label for="sp_id">Bus Type</label>
                                    <input class="p_bus_type" name="p_bus_type[]" placeholder="" type="text" >
<!--                                    <select class="p_bus_type browser-default" name="p_bus_type[]">
                                        <option value="" disabled>Select Bus Type</option>
                                        <option value="Mini Bus"  selected>Mini Bus</option>
                                        <option value="Big Bus">Big Bus</option>
                                    </select>-->
                                    
                                </div>
                          </div>
                          <div class="col s12 m2 l2">
                                <div class="input-field">
                                    <input  class="p_seat_count" name="p_seat_count[]" placeholder="" type="number" >
                                    <label for="city_id">Jumlah Kursi</label>
                                </div>
                          </div>
                          <div class="col s12 m2 l2">
                                <div class="input-field">
                                    <input name="p_price[]" class="p_price" type="number" placeholder="" autocomplete="off" >
                                    <input name="id[]" class="id" type="hidden" autocomplete="off" >
                                    <label for="sp_id">Enter Harga</label>
                                </div>
                          </div>
                          <div class="col s12 m2 l2">
                                <!--<div class="input-field">-->
                                    <span class="btn add_more">Add</span>
                                <!--</div>-->
                          </div>
                      </div>
                  </div>
               
            <!--</div>-->
              </div>
          </div>
          <div class="modal-footer" id="btnFooter">
                <a id='mdlabortf'class="modal-action modal-close waves-effect waves-red btn-flat mdlabort">Cancel</a>
                <button id="save-bannerf" class="modal-action waves-effect waves-green btn-flat save-banner">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <a class="btn-floating btn-large red tooltipped" onclick="addprw();" data-position="top" data-delay="50" data-tooltip="Add Pariwisata">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
@endsection