
<?php  $bus = $body['bus'] ; ?>
<h4>Bus Detail of : {{$bus['name']}}</h4>       
<div class="row">
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s3"><a href="#general" class="active">General</a></li>
      <li class="tab col s3"><a href="#routes">Routes</a></li>
      <li class="tab col s3"><a href="#layouts">Layouts & Coupon Code</a></li>
    </ul>
  </div>
    <div id="general" class="col s12">
        <div class="col s12 m6 l6">
            <div class="col s12 m3 l3"> <h5>Bus Name :</h5> </div> 
            <div class="col s12 m6 l6"> {{$bus['name']}} </div>
        </div>
        <div class="col s12 m6 l6">
            <div class="col s12 m4 l4"> <h5>Bus Status :</h5> </div> 
            <div class="col s12 m6 l6"> <?php echo $bus['status'] ? '<span style="color:green;">Active</span>' : '<span style="color:red;">Inactive</span>' ?> </div>
        </div>
        <div class="col s12 m12 l12">
            <h5>Driver List : </h5> <br>
            <?php
                $dr = $bus['drivers'];
                                if(count($dr) > 0){
            ?>
            <table id="driver_list" class="default_padding table-bordered">
                            <thead>
                                <tr>
                                    <th style="padding: 5px 5px !important;">Name</th>
                                    <th style="padding: 5px 5px !important;">Mobile</th>
                                    <th style="padding: 5px 5px !important;">Email</th>
                                    <th style="padding: 5px 5px !important;">Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                    foreach($dr as $d){
                                        ?>
                                <tr>
                                    <td style="padding: 5px 5px !important;">{{$d['driver']['name']}}  </td>
                                    <td style="padding: 5px 5px !important;">{{$d['driver']['mobile']}}</td>
                                    <td style="padding: 5px 5px !important;">{{$d['driver']['email']}}</td>
                                    <td style="padding: 5px 5px !important;">{{$d['driver']['address']}}</td>
                                </tr>    
                                <?php
                                    }
                                
                                ?>
                            </tbody>
                        </table>
            <?php
            }else{
                echo 'No drivers assigned to this bus yet.';
            }
            ?>
        </div>
        <div class="col s12 m12 l12">
            <h5>Amenities : </h5> <br>
            <div class="col s12 m12 l12 tagsinput" style="border-bottom: none !important">
            <?php 
            $amenities = $bus['amenities'];
            $a = '';
            if(isset($amenities) && count($amenities)>0){
               
                foreach($amenities as $am){
                    $im = '';
                    echo '<span class="tag">';
                     $filepath = config('constant.UPLOAD_AMENITY_DIR_PATH').$am['aimg'];
                    $url = URL::to('assets/uploads/amenities').'/'.$am['aimg'];
                    if(file_exists($filepath)){
//                        $im = '<img src="'.$url.'" style="heifht:25px;width:25px;"/>';
                    }
                   echo $a = '<span style="">'.$am['aname'].'</span>'.$im; 
                   echo '</span >';
                }
            }else{
              echo  $a = '<span style="">No Amenites in this bus.</span>';
            }
            
            ?>
            </div>
        </div>
    </div>
    <div id="routes" class="col s12">
        <div id="all_routes">
                <?php
                    $routes = $body['routes'];
                    
                    $rcnt = count($routes);
                    $cnt = 0;
                    
                    if($rcnt > 0){
                        $prc = count($routes[0]['prices']);
                        ?>
            <ul class="collapsible"  id="coll-1" data-collapsible="accordion">
                <li  class="active">
                    <div class="collapsible-header active"> <b>Route Cities </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-1-ar" class="mdi-hardware-keyboard-arrow-up right small arrows" ></i></a>
                    </div>
                    <div class="collapsible-body">
            <table class="table-bordered default_padding">
                    <thead>
                    <tr>
                        <th>City</th>
                        <th>District</th>
                        <th>Terminal</th>
                        <th>Departure Time</th>
                    </tr>
                    </thead>
                    <tbody id="route_list">
                        <?php 
                        $added = array();
                        $prc = 1;
                        if(count($routes) > 0){
                        foreach($routes as $rt){ 
                            $prc = count($rt['prices']);
                            $id = $rt['from_district_id'].'-'.$rt['from_city_id'].'-'.$rt['from_terminal_id'];
                            if(!in_array($id, $added)){
                                array_push($added, $id);
                            ?>
                        <tr>
                            <td>{{$rt['fromCity']['name']}}</td> 
                            <td>{{$rt['fromDistrict']['name']}} </td> 
                            <td>{{$rt['fromTerminal']['name']}}</td>
                            <td><span>{{$rt['boarding_time']}} &nbsp;&nbsp;</span></td>  
                        </tr>
                        <?php }
                        $id = $rt['to_district_id'].'-'.$rt['to_city_id'].'-'.$rt['to_terminal_id'];
                        if(!in_array($id, $added)){
                                array_push($added, $id);
                            ?>
                        <tr>
                            <td>{{$rt['toCity']['name']}}</td> 
                            <td>{{$rt['toDistrict']['name']}}  </td> 
                            <td>{{$rt['toTerminal']['name']}}</td>
                            <td><span>{{$rt['droping_time']}} &nbsp;&nbsp;</span></td>  
                        </tr>
                        <?php }
                        }
                        }
                        ?>
                    </tbody>
                </table>
                    </div>
                </li>
            </ul>
            <ul class="collapsible"  id="coll-1" data-collapsible="accordion">
                <li  class="active">
                    <div class="collapsible-header active"> <b>All Routes with price</b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-1-ar" class="mdi-hardware-keyboard-arrow-up right small arrows" ></i></a>
                    </div>
                    <div class="collapsible-body">
            <table class="table-bordered">
                    <thead>
                        <tr>
                            <th  colspan="10"></th>
                            <th colspan="{{$prc}}" style="text-align: center;">Price </th>
                        </tr>
                        <tr>
                            <th colspan="4" style="text-align: center">From</th>
                            <th>&nbsp;</th>
                            <th colspan="4" style="text-align: center">To</th>
                            <th>&nbsp;</th>
                            <?php for($i = 0;$i<$prc;$i++){ 
                            ?>
                            <th style="text-align: center;">Price <?php echo App\Models\General::getAlphabate($i); ?>
                            </th>
                            <?php } ?>
                        </tr>
                        <tr id="menu_head">
                            <th>City</th>
                            <th>District</th>
                            <th>Terminal</th>
                            <th>Time</th>
                            <th></td>
                            <th>City</th>
                            <th>District</th>
                            <th>Terminal</th>
                            <th>Time</th>
                            <th></th>
                            <?php for($i = 0;$i<$prc;$i++){
                                $a = App\Models\General::getAlphabate($i);
                                ?>
                            <th style="width: 12%">
                                <div class="col sm12 m12 l12 default_padding">
                                    <span> Seat Count : <?php  echo count($routes) > 0 ?$routes[0]['prices'][$i]['seat_count']:'' ?></span>
                                </div>
                            </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody id="set_all_fare">
                        <?php
                        $RouteJs = array();
                        $collect = array();
                        if(count($routes) > 0){
                        foreach($routes as $rt){
                            ?>
                            <tr id="">
                                    <td>{{$rt['fromCity']['name']}}</td>
                                    <td>{{$rt['fromDistrict']['name']}}</td>
                                    <td>{{$rt['fromTerminal']['name']}}</td>
                                    <td>{{$rt['boarding_time']}}</td>
                                    <td></td>
                                    <td>{{$rt['toCity']['name']}}</td>
                                    <td>{{$rt['toDistrict']['name']}}</td>
                                    <td>{{$rt['toTerminal']['name']}}</td>
                                    <td>{{$rt['droping_time']}}</td>
                                    <td></td>
                                    <?php foreach($rt['prices'] as $k=>$r){
                                        $a = App\Models\General::getAlphabate($k);
                                        ?>
                                    <td>
                                        <div class="col sm12 m6 l6 default_padding">
                                            <span>{{$r['price']}}</span>
                                        </div>   
                                    </td>
                                    <?php } ?>
                            </tr>
                            <?php
                        }
                        }
                        ?>
                    </tbody>
                </table>
                    </div>
                </li>
            </ul>
            <?php
                    }else{
                        ?>
                        <ul class="collapsible" data-collapsible="accordion">
                            <li class="">
                                <div class="collapsible-header active"> <b> No Routes Found </b>
                                            <a style="cursor: pointer" class="default_color"> <i id="coll-1-ar" class="mdi-hardware-keyboard-arrow-up right small arrows" ></i></a>
                                </div>
                            </li>
                        </ul>
                        <?php
                    } 
                ?>
            </div>
    </div>
<div id="layouts" class="col s12">
        <div class="col s12 m6 l6">
            <div class="col s12 m4 l4"> <h5>Have bus layout : </h5> </div> 
            <div class="col s12 m6 l6"> <?php echo $bus['layout'] ? '<span style="color:green;">Yes</span>' : '<span style="color:red;">No</span>' ?> </div>
        </div>
        <div class="col s12 m6 l6">
            <div class="col s12 m4 l4"> <h5>Total Seats :</h5> </div> 
            <div class="col s12 m6 l6"> <?php echo $bus['layout'] ? count($bus['bus_layout'][0]['seats']) : $bus['seat_capacity'] ?> </div>
        </div>
    
    <div class="col s12 m12 l12">
    <ul class="collapsible"  id="coll-1" data-collapsible="accordion">
                <li  class="active">
                    <div class="collapsible-header active"> <b>Coupon Code </b>
                        <a style="cursor: pointer" class="default_color"> <i id="coll-1-ar" class="mdi-hardware-keyboard-arrow-up right small arrows" ></i></a>
                    </div>
                    <div class="collapsible-body">
                        <?php
                            $ccode = $bus['coupon_code'];
                            if(count($ccode) > 0){
                                ?>
                        <table class="table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($ccode as $cd){
                                    ?>
                            <tr>
                                <td><span id='ccode-{{ $cd['id'] }}'><?php echo $cd['status'] == 1 ? '<span style="color:green">'.$cd['code'].'<span>' : '<span style="color:red">'.$cd['code'].'</span>' ; ?></span></td>
                                <td><span id='csdate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['start_date']))}}</span></td>
                                <td><span id='cedate-{{ $cd['id'] }}'>{{date('Y-m-d',strtotime($cd['end_date']))}}</span></td>
                                <td><span id='ctype-{{ $cd['id'] }}' name="{{$cd['type']}}" >{{$cd['type'] == 'P'? (int)$cd['value'].' %':'Rs. '.(int)$cd['value']}}</span></td>
                            </tr>
                            <?php
                                }
                            
                            ?>
                        </tbody>
                    </table>
                        <?php
                        }else{
                                echo 'No coupon code found.';
                            }
                        ?>
                    </div>
                    
                </li>
            </ul>
    </div>
</div>
  
</div>
<script>
    $('ul.tabs').tabs();
    $('.collapsible').collapsible();
    
    $('.collapsible').click(function(){
        
        if($(this).children().hasClass('active')){
            $(this).find('.arrows').removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-up');
        }else{
            $(this).find('.arrows').removeClass('mdi-hardware-keyboard-arrow-up').addClass('mdi-hardware-keyboard-arrow-down');
        }
    });
</script>
