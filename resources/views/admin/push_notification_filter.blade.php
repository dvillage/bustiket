<table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>
                    <p class="form-field-inline clearfix nospace-top checkbox-check">
                            <input type="checkbox" name="" onClick="allCheckBox();" id='masterCheck'>
                            <label for='masterCheck'></label>
                    </p>
                </th>
              <th style="vertical-align: inherit;text-align: center;">Name</th>
              <th style="vertical-align: inherit;text-align: center;">Email</th>
            </tr>
        </thead>
        
        <tbody>
            @foreach($result as $r)
            <tr>
                <td width="5%">
                    <p class="form-field-inline clearfix nospace-top checkbox-check">
                        <input type="checkbox" name="" class="icheck" id='p-{{ $r['id'] }}'  data-email="{{$r['email']}}">
                            <label for='p-{{ $r['id'] }}'></label>
                    </p>
                </td>
                <?php
                    if(isset($r['first_name']) || isset($r['last_name'])){
                        $name=$r['first_name'].' '.$r['last_name'];
                    }else{
                        $name=$r['name'];
                    }
                ?>
                <td width="20%"><span id='pname-{{ $r['id'] }}' name="pname-{{ $r['id'] }}" >{{$name}}</span></td>
                <td ><span> {{$r['email']}}</span></td>
            </tr>
            @endforeach            
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $crnt_page }}" />
<input type="hidden" id="total_page" value="{{ $total_page }}" />
<input type="hidden" id="len" value="{{ $len }}" />

