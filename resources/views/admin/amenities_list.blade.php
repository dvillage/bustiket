@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('admin/amenities-filter',crnt,len,type,'','');
        
    };
    
    function delAmenity(id){
        var type = $('#type').html().toLowerCase();
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete '+type+' ??';
        var onyes ='delData';
        var param='\'admin/delete-amenities\','+id+' , \'admin/amenities-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function addAmenity(){
        
        $('#add-name').val('');
        $('#amenity_img').val('');
        $('#modal-amenity').find('#mdlyes').attr('onclick','addNewAmenity()');
        $('#modal-amenity').find('#mdlyes').html('Add');
        $('#modal-amenity').openModal();
        $('#add_amenity').parsley().reset();
    }
    function upAmenity(id){
        $('#add-name').val($('#aname-'+id).html());
        $('#amenity_img').val('');
        $('#modal-amenity').find('#mdlyes').attr('onclick','editAmenity('+id+')')
        $('#modal-amenity').find('#mdlyes').html('Update');
        $('#modal-amenity').openModal();
        $('#add_amenity').parsley().reset();
    }
    
    function addNewAmenity(){
        var url="{{URL::to('admin/add-amenities')}}";
        $('#add_amenity').attr('action',url);
        $('#add_amenity').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-amenity').closeModal();
                filterData('admin/amenities-filter');
            }
        });
    }
    function editAmenity(id){
//        alert('called');
        var url="{{URL::to('admin/edit-amenities')}}";
        $('#a_id').val($('#aid-'+id).attr('name'));
        $('#add_amenity').attr('action',url);
        $('#add_amenity').ajaxForm(function(res) {
            if(res.flag!=1){
                Materialize.toast(res.msg, 2000,'rounded');
                return false;
            }
            else{
                Materialize.toast('your data saved successfully !!', 2000,'rounded');
                $('#modal-amenity').closeModal();
                filterData('admin/amenities-filter');
            }
        });
    }
   
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Amenities</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Amenities</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l2 m12 s12"></div>
        <div class="col l8 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Amenities : <span id="type"></span></h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/amenities-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/amenities-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('admin/amenities-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/amenities-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/amenities-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/amenities-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l2 m12 s12"></div>
        
        <!-- Modal Structure -->
        <form id="add_amenity" method="post" action="" data-parsley-validate >
        <div id="modal-amenity" class="modal bust-modal" style='width:35%;font-size:100%;' >
          <div class="modal-content">
            <div class="card">
                <div class="title">
                    <h5 id='mdltitle'>Amenity Details</h5>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                <div class="content">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <input type="hidden" name="a_id" id="a_id" value="" />
                    <div class="input-field">
                        <input id="add-name" name="name" type="text" placeholder="Amenity Name" required="">
                        <label for="add-dname">Amenity Name Name</label>
                    </div>
                    
                    <div class="file-field input-field">
                        <div class="btn small">
                            <span class="">Amenity</span>
                            <input type="file" name="amenity_img" id="amenity_img" accept="image/*" ="">
                            <!--<label for="bnr_img">Upload Banner Image</label>-->
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    
                </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <button id='mdlyes' class="modal-action waves-effect waves-green btn-flat ">Add</button>
          </div>
        </div>
        </form>
        
        <!-- Modal Structure Over-->
       
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            
            <a class="btn-floating btn-large red tooltipped" onclick="addAmenity();"data-position="top" data-delay="50" data-tooltip="Add Service Provider">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>
    
    <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
    
@endsection