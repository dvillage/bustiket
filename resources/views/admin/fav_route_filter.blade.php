
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>From City</th>
              <th>To City</th>
              <th>Bus</th>
              <th>Price</th>
              <th>Image</th>
              <th>Display in footer</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='fcity-{{ $r['id'] }}' name="{{$r['from']}}">{{$r['from_city']['name']}}</span></td>
                <td><span id='tcity-{{ $r['id'] }}' name="{{$r['to']}}" > {{$r['to_city']['name']}} </span></td>
                <td><span id='bus-{{ $r['id'] }}' name="{{$r['bus_id']}}" > {{$r['bus']['name']}}</span></td>
                <td><span id='price-{{ $r['id'] }}' name="{{$r['price']}}" > {{\General::number_format($r['price'])}} </span></td>
                <td>
                    <span id='cimg-{{ $r['id'] }}'> 
                        <?php
                        if($r['image'] != ''){
                           $file = URL::to('assets/uploads/fav_routes').'/'.$r['image'];
                           $file_check = config('constant.UPLOAD_FAV_ROUTE_DIR_PATH').$r['image'];
                           if(file_exists($file_check)){
                               echo '<img id="img-'.$r['id'].'" src="'.$file.'" style="height:25px;width:25px"/>';
                           }
                        }
                        ?>
                    </span>
                </td>
                <td><span id='in_footer-{{ $r['id'] }}' name="{{$r['in_footer']}}" >{{ $r['in_footer'] ? 'Yes':'No'}}</span></td>
                
              <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upPrw( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delPrw( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />