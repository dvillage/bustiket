@extends('layouts.admin')

@section('content')
<?php // dd($body['data'])?>
<?php
$lt = config('constant.LOGGER') == 'SP'?'sp':(config('constant.LOGGER') == 'SS'?'ss':'admin');
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var base_url = getBaseURL();
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
    };
    
    function filterBy(c='',len='',elem=''){
        var filter = {
            'tid'  :$('#tid').val(),
            'date'  :$('#filter_date').val(),
            'from'  :$('#fromcity').val(),
            'to'    :$('#tocity').val(),
        };
        if(c === 'w'){
            filterDataWith('{{$lt}}/tlist-filter',elem)
        }
        else{
            return filterData('{{$lt}}/tlist-filter',len,filter);
        }
    }
    
    function delTicket(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Cancel Ticket ??';
        var onyes ='canceltkt';
        var param= "'"+id+"'";
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    function refTicket(id){
        var title='<b>Confirmation<b>';
        var msg='Are you sure to refund this Ticket ??';
        var onyes ='refundtkt';
        var param= "'"+id+"'";
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function sendMail(){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var d = CKEDITOR.instances['emaildata'].getData();
        console.log(d);
        
        flag = 0;
        var userEmail = '<?php echo $body['data']['booker_email']; ?>';
        var url = base_url+"{{$lt}}/send-email";
        var data = {useremail:userEmail,data:d};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    }
    
    
    function sendSMS(){
        var d = $('#smsdata').val();
        console.log(d);
        
        flag = 0;
        var userCon = '<?php echo $body['data']['booker_mo']; ?>';
        var url = base_url+"{{$lt}}/send-sms";
        var data = {usercon:userCon,data:d};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    }
    
    function canceltkt(id){
        console.log(id);
        
        var url = base_url+"{{$lt}}/cancel-ticket";
        var data = {id:id,};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
                location.reload();
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    }
    function refundtkt(id){
        console.log(id);
        
        var url = base_url+"{{$lt}}/refund-ticket";
        var data = {id:id,};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
                location.reload();
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
                
            }
        });
    }
    <?php if($flag == 0){ ?>
    function applyCode(){
        var code = $('#ccode').val();
        var amount = '{{$param["total_amount"]}}';
        if(code == ''){
             $('#coupon_discount').val(0);
            $('#total_amount').val(amount);
            $('#display_amount').html(amount);
            $('#msg').html('');
            Materialize.toast('please enter coupon code', 2000,'rounded');
            return false;
        }
        var b_id = '{{$param["bus_id"]}}';
        var url=getBaseURL()+"payment/coupon-discount";
        var data={ccode:code,busid:b_id,amount:amount};
        postAjax(url,data,function(res){
            if(res.flag ==1 ){
                var coupon_discount = res.discount;
                var total_amount = amount - res.discount;
                $('#coupon_discount').val(coupon_discount);
                $('#total_amount').val(total_amount);
                $('#display_amount').html(total_amount);
                $('#msg').html(res.msg);
                $('#msg').css('color','green');
                $('#apply').removeAttr('onclick');
            }else{
                $('#msg').html(res.msg);
                $('#msg').css('color','red');
                $('#display_amount').html(amount);
            }
        });
    }
    $(document).on('click','#book',function(){
//            $('#book_form').submit();
//        $('#book_form').ajaxForm(function(res){
//            console.log(res);
//        });
        $('#book_form').ajaxSubmit(function(res){
            console.log(res);
            if(res.flag == 1){
                Materialize.toast('ticket booked successfully', 2000,'rounded');
                window.location = '{{URL::to($lt."/ticket-details")}}/'+res.data.booking_id;
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
    });
<?php } ?>
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Ticket Details</h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Ticket Details</a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        <div class="col l2 m2"></div>
        <div class="col l8 m8 s12">
            <div class="row">
            <?php if($flag){ ?>
            <div class="col l12 m12 s12 pad-b-0">
                <button class="btn modal-trigger" id='sendmail'  href="#sendEmail" style="width:10%">
                    Send Email 
                </button>
                <button class="btn modal-trigger" id='sendsms' href="#sendSMS" style="width:10%">
                    Send Sms
                </button>
            </div>
            <?php } ?>
            <div class="col l12 m12 s12 ">
            <div class="card">
                <div class="title">
                    <div class="row">
                        <div class="col l6 m6 s6"> <h5 >Booking Details</h5> </div>
                        <div class="col l6 m6 s6">
                            <h5 style="float: right;">One-Way</h5>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Booking ID :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['booking_id']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Nama Pemesan :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['booker_name']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Email :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['booker_email']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    No. Tel :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['booker_mo']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Tanggal Pesan :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{date('Y-m-d',strtotime($body['data']['created_at']))}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Tanggal Berangkat :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{date('Y-m-d',strtotime($body['data']['journey_date']))}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Service Provider & Type :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>
                                    <?php if($flag){ ?>
                                    {{$body['data']['sp']['first_name'].' '.$body['data']['sp']['last_name'].' & '.$body['data']['sp']['type']}}
                                    <?php }else{ ?>
                                        {{$body['data']['sp_name']}}
                                    <?php } ?>
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Commission :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>
                                    <!--{ {$body['data']['handling_fee']}}-->
                                </span>
                            </div>
                        </div>
                        <div class="col l6 m6 s12">
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Kota Asal :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['from_terminal']['loc_citi']['name']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Kota Tujuan :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['to_terminal']['loc_citi']['name']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Lokasi & Waktu Keberangkatan :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['from_terminal']['name']}} ({{date('Y-m-d h:i A',strtotime($body['data']['pickup_date']))}})</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Lokasi & Waktu Kedatangan :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['to_terminal']['name']}} ({{date('Y-m-d h:i A',strtotime($body['data']['dropping_date']))}})</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Jumlah Penumpang :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['nos']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Nomor Kursi :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>
                                    (
                                    <?php 
                                    $lbl = '';
                                    foreach($body['data']['booking_seats'] as $seat){ 
                                         if(isset($seat['seat_lbl']) && $seat['seat_lbl'] != ''){
                                             if($lbl != ''){
                                                $lbl = $lbl.','; 
                                             }
                                             $lbl .= $seat['seat_lbl'];
                                         }else{
                                             $lbl = 'Saat Keberangkatan';
                                         }
                                    }
                                    echo $lbl;
                                    ?>
                                    )
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Harga Tiket :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span>{{$body['data']['base_amount']}}</span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span class="setting-title" style="font-weight: bolder;">
                                    Total Harga :
                                </span>
                            </div>
                            <div class="col l6 m6 s12 no-padding">
                                <span id="display_amount">{{$body['data']['total_amount']}}</span>
                            </div>
                        </div>
                        <div class="col l12 m12 s12" align='center'>
                            
                            <h5 style="font-weight: bolder;">Detail Penumpang<span style="float:right">
                                    <?php if($flag){
                                        
                                        if($body['data']['status'] == 1){
                                        ?>
                                    
                                    <a style="cursor: pointer;" onclick="delTicket('{{$body['data']['booking_id']}}')">Cancel</a>
                                    <?php
                                        }elseif($body['data']['status'] == 3){
                                            if($body['data']['booking_seats'][0]['status'] == 3){
                                                echo '<span style="color:green">Refunded</span>';
                                            }else{
                                            ?>
                                    <a style="cursor: pointer;" onclick="refTicket('{{$body['data']['booking_id']}}')">Refund</a>
                                    <?php
                                            }
                                        }
                                        } ?>
                                </span></h5>
                            
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">No</th>
                                        <th style="text-align: center;">No.Tiket</th>
                                        <th style="text-align: center;">Nama Penumpang</th>
                                        <th style="text-align: center;">No.Kursi</th>
                                        <th style="text-align: center;">Jenis Kelamin</th>
                                        <th style="text-align: center;">Usia</th>
                                        <!--<th style="text-align: center;">Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php $i = 0;
                                    foreach($body['data']['booking_seats'] as $seat){ 
                                        $i++;?>
                                    <tr>
                                        <td style="text-align: center;">{{$i}}</td>
                                        <td style="text-align: center;">{{$seat['ticket_id']}}</td>
                                        <td style="text-align: center;">{{$seat['passenger_name']}}</td>
                                        <td style="text-align: center;">{{ $seat['seat_lbl'] != '' ? $seat['seat_lbl'] : 'Saat Keberangkatan' }}</td>
                                        <td style="text-align: center;">
                                            <?php echo ($seat['passenger_gender'] == 'm' || $seat['passenger_gender'] == 'M') ? 'Pria' : 'Wanita'; ?>
                                        </td>
                                        <td style="text-align: center;">{{$seat['passenger_age']}}</td>
<!--                                        <td style="text-align: center;">{{ $seat['seat_lbl'] != '' ? $seat['seat_lbl'] : 'Saat Keberangkatan' }}</td>-->
<!--                                        <td style="text-align: center;">
                                            <a class="btn-floating btn-small btn red modal-trigger tooltipped" id='{{ $seat['id'] }}' onclick="delSP({{ $seat['id'] }});"  data-position="top" data-delay="50" data-tooltip="Cancel Ticket"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <a class="btn-floating btn-small btn black modal-trigger tooltipped" id='{{ $seat['id'] }}' onclick="delSP({{ $seat['id'] }});" data-position="top" data-delay="50" data-tooltip="Delete Ticket"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        </td>-->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <?php if($flag){ ?>
                        <div class="col l12 m12 s12">
                            <h5 style="font-weight: bolder;" class='center'>Syarat & Ketentuan</h5>
                            <ol>
                                <li>Harap print e-Tiket ini dibawa untuk ditukarkan dengan tiket bus resmi.</li>
                                <li>Waktu yang tercantum pada tiket adalah waktu setempat.</li>
                                <li>Harap tiba di terminal keberangkatan 60 menit sebelum waktu keberangkatan yang tercantum pada e-Tiket.</li>
                                <li>Nomor kursi dapat berubah sesuai kebijakan operator pada saat keberangkatan.</li>
                                <li>Harga dapat berubah sewaktu-waktu pada saat keberangkatan sesuai dengan kebijakan masing-masing operator dikarenakan high season, liburan panjang, lebaran dan lain-lain yang berpotensi terhadap kenaikan tiket. Tambahan biaya tiket tersebut akan dibebankan kepada penumpang.</li>
                                <li>BUSTIKET tidak bertanggung jawab atas keterlambatan atau penjadwalan ulang bus. Hal tersebut berada dibawah kendali masing-masing manajemen bus.</li>
                                <li>Jika Anda menghadapi memiliki pertanyaan silahkan hubungi kami di  0812 8000 3919.</li>
                            </ol> 
                        </div>
                        <?php }else{ ?>
                        <form id="book_form" method="post" action="{{URL::to('services/book-ticket')}}" >
                            <div class="col l12 m12 s12">
                                <div class="col s6 m6 l6 no-padding">
                                    <div class="input-field">
                                        <input id="ccode" type="text" name="coupon_code" >
                                        <label for="input_text" >Apply Coupon Code</label>
                                        <span id="msg"></span>
                                    </div>
                                </div>
                                <div class="col s6 m6 l6 ">
                                    <div class="input-field">
                                        <input id="apply" type="button" Value="Apply Code" onclick="applyCode();" class="btn" style="width:82px;" >
                                    </div>
                                </div>

                            </div>
                            <div class="col s6 m6 l6 ">
                                <input id="book" type="button" Value="Book" class="btn" style="width:82px;" >
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <input type="hidden" id="base_amount" name="base_amount" value="{{$body['data']['base_amount']}}" >
                                <input type="hidden" id="total_amount" name="total_amount" value="{{$body['data']['total_amount']}}" >
                                <input type="hidden" id="coupon_discount" name="coupon_discount" value="" >
                                <input type="hidden" id="param" name="param" value='{{json_encode($param)}}' >
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
                </div>
        </div>
        </div>
        <div class="col l2 m2"></div>
        <?php if($flag){ ?>
        <!-- Modal Structure -->
        <div id="sendEmail" class="modal" style="">
          <div class="modal-content">
            <h4 id='mdltitle'>Send Email</h4>
            <textarea  id="emaildata" name="emaildata" class="materialize-textarea"></textarea>
            <script>
                CKEDITOR.replace( 'emaildata' );
            </script>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat " onclick="sendMail();">Send Mail</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="sendSMS" class="modal" style="">
          <div class="modal-content">
            <h4 id='mdltitle'>Send SMS</h4>
            <textarea  id="smsdata" name="smsdata" class="materialize-textarea" placeholder="Write SMS msg Here."></textarea>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat " onclick="sendSMS();">Send SMS</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        <?php } ?>
        
        
    </div>
    <!-- Modal Structure -->
        <div id="confirmDel" class="modal" style="width: 25%;">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->

@endsection