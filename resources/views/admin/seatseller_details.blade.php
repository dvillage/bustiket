@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
    };
    
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Seat Seller</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a href='{{URL::to('admin/seatseller/'.$body['data']['type'])}}'>Seat Seller - <?php echo $body['data']['type']!=""?$body['data']['type']:''; ?></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a ><?php echo $body['data']['name']!=""?$body['data']['name']:''; ?></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->


    <div class="row" >
        
    </div>


@endsection