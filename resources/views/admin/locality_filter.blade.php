    <?php if($type=='Province') {
//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Name</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='pname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td style="text-align:center;">
                    <a  class="btn-floating btn-small blue" id="" onclick="upLoc( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <!--<a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delLoc( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                    
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }
    elseif($type=='District'){ 
//        dd($data);
        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr >
                <th style="text-align: center;">Name</th>
                <th style="text-align: center;">Province</th>
                <th style="text-align: center;">City</th>
                <th style="text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td style="text-align: center;"><span id='dname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td style="text-align: center;"><span id='dpro-{{ $r['id'] }}' name='{{ $r['province_id'] }}'>{{ $r['loc_province']['name'] }}</span></td>
                <td style="text-align: center;"><span id='dcity-{{ $r['id'] }}' name='{{ $r['city_id'] }}'>{{ $r['loc_citi']['name'] }}</span></td>
                <td style="text-align:center;">
                    <a class="btn-floating btn-small blue" id="" onclick="upLoc( {{ $r['id'] }} );" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <!--<a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delLoc( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                </td>
            </tr>
            <?php } ?>
      </tbody>
    </table>

    <?php }
    elseif($type=='City'){
//        dd($data);      

        ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center;">Name</th>
                <th style="text-align: center;">Province</th>
                <th style="text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='cname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td><span id='cpro-{{ $r['id'] }}' name='{{ $r['province_id'] }}'>{{ $r['loc_province']['name'] }}</span></td>
                <td style="text-align:center;">
                    <a class="btn-floating btn-small blue" id="" onclick="upLoc( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <!--<a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delLoc( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php } 
    elseif($type == 'Terminal'){
//        dd($data); ?>

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center;">Province</th>
                <th style="text-align: center;">City</th>
                <th style="text-align: center;">District</th>
                <th style="text-align: center;">Terminal</th>
                <th style="text-align: center;">Address</th>
                <th style="text-align: center;">Zip-Code</th>
                <th style="text-align: center;">Agent Name</th>
                <th style="text-align: center;">Agent Phone</th>
                <th style="text-align: center;">Agent Email</th>
                <th style="text-align: center;">Google Map Address</th>
                <th style="text-align: center;width:100px;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='tpro-{{ $r['id'] }}' name='{{ $r['province_id'] }}'>{{ $r['loc_province']['name'] }}</span></td>
                <td><span id='tcity-{{ $r['id'] }}' name='{{ $r['city_id'] }}'>{{ $r['loc_citi']['name'] }}</span></td>
                <td><span id='tdist-{{ $r['id'] }}' name='{{ $r['district_id'] }}'>{{ $r['loc_district']['name'] }}</span></td>
                <td><span id='tname-{{ $r['id'] }}'>{{ $r['name'] }}</span></td>
                <td><span id='tadd-{{ $r['id'] }}'>{{ $r['address'] }}</span></td>
                <td><span id='tzip-{{ $r['id'] }}'>{{ $r['zip_code'] }}</span></td>
                <td><span id='ta_name-{{ $r['id'] }}'>{{ $r['agent_name'] }}</span></td>
                <td><span id='ta_phone-{{ $r['id'] }}'>{{ $r['agent_phone'] }}</span></td>
                <td><span id='ta_email-{{ $r['id'] }}'>{{ $r['agent_email'] }}</span></td>
                <td><span id='googlemap-{{ $r['id'] }}'>{{ $r['google_map_add'] }}</span></td>
                
                <td style="text-align:center;">
                    <a class="btn-floating btn-small blue" id="" onclick="upLoc( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="delLoc( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>


    <?php    
        }
    ?>

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />