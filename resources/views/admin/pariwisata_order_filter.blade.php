
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Subscriber Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Company Name</th>
              <th>Pickup Address</th>
              <th>Date</th>
              <th>Service Type</th>
              <th>Comment</th>
              <th>Total Bus</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data['result'] as $r) { ?>
            <tr>
                <td><span id='sp-{{ $r['id'] }}'>{{ $r['subscriber_name'] }}</span></td>
                <td><span id='btype-{{ $r['id'] }}'> {{$r['email']}} </span></td>
                <td><span id='ac-{{ $r['id'] }}' > {{ $r['mobile'] }}</span></td>
                <td><span id='city-{{ $r['id'] }}'>{{ $r['company_name']}}</span></td>
                <td><span id='price-{{ $r['id'] }}'> {{$r['pickup_address']}} </span></td>
                <td><span id='engine-{{ $r['id'] }}'> {{$r['order_date']}} </span></td>
                <td><span id='manufacurer-{{ $r['id'] }}'> {{$r['order_service_type']}} </span></td>
                <td><span id='fleet_detail-{{ $r['id'] }}'> {{$r['comment']}} </span></td>
                <td><span id='fleet_detail-{{ $r['id'] }}'> {{$r['total_bus']}} </span></td>
                <td style="text-align:center;">
                    <?php if($r['order_status'] == 0){ ?>
                    <a href="#" title="Approve" onclick="approve( {{ $r['id'] }} ,'A');"><i class="mdi-action-done small default_color modal-trigger"></i></a>
                    <a href="#" title="Cancel" onclick="approve( {{ $r['id'] }} ,'R');"><i class="mdi-content-clear small modal-trigger"></i></a>
                    <a href="#" title="Delete" class="btn-floating btn-small red btn " onclick="approve( {{ $r['id'] }} ,'D');"><i class="fa fa-trash-o"></i></a>
                    <?php }else{
                        if($r['order_status'] == 1){
                            ?>
                    <i class="mdi-image-brightness-1" title="Approved" style="color:green"></i>
                    <a href="#" title="Cancel" onclick="approve( {{ $r['id'] }} ,'R');"><i class="mdi-content-clear small modal-trigger"></i></a>
                    <a href="#" title="Delete" class="btn-floating btn-small red btn " onclick="approve( {{ $r['id'] }} ,'D');"><i class="fa fa-trash-o"></i></a>
                    <?php
                        }else{
                            ?>
                    <a href="#" title="Approve" onclick="approve( {{ $r['id'] }} ,'A');"><i class="mdi-action-done small default_color modal-trigger"></i></a>
                    <i  class="mdi-image-brightness-1" title="Cancelled" style="color:red"></i>
                    <a href="#" title="Delete" class="btn-floating btn-small red btn " onclick="approve( {{ $r['id'] }} ,'D');"><i class="fa fa-trash-o"></i></a>
                    <?php 
                        }
                       
                    } ?>
                    
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />