<table class="table table-bordered table-striped">
        <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Message</th>
              <th>Image</th>
               <th>Video</th>
              <th>Action</th>
            </tr>
        </thead>
        
        <tbody>
    
             @foreach($data['result'] as $r)
            <tr>
                <td><span id='uname-{{ $r['id'] }}' name="name-{{ $r['id'] }}">{{$r['name']}}</span></td>
                <td><span id='email-{{ $r['id'] }}' name="email-{{ $r['id'] }}" >{{$r['email']}}</span></td>
                <td><span id='message-{{ $r['id']}}' name="message-{{ $r['id']}}" > {{$r['message']}}</span></td>
                
                <td>
                    <span id='cimg-{{ $r['id'] }}'> 
                        <?php
                        if($r['image'] != ''){
                           $file = URL::to('assets/uploads/testimonial').'/'.$r['image'];
                           $file_check = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH').$r['image'];
                           if(file_exists($file_check)){
                               echo '<img id="img-'.$r['id'].'" src="'.$file.'" style="height:25px;width:25px"/>';
                           }
                        }
                        ?>
                    </span>
               </td>
               <td>
                    <span id='video-{{ $r['id'] }}'> 
                        <?php
                        if($r['video'] != ''){
                           $file = URL::to('assets/uploads/testimonial').'/'.$r['video'];
                           $file_check = config('constant.UPLOAD_TESTIMONIAL_DIR_PATH').$r['video'];
                           if(file_exists($file_check)){
                               echo '<video width="100" height="100" controls>
                                    <source  id="vid-'.$r['id'].'"  src="'.$file.'" type="video/mp4">
                                    Your browser does not support the video tag.
                                  </video>';
                           }
                        }
                        ?>
                    </span>
               </td>
               
               
      
              
                
              <td style="text-align:center;">
                  <?php if($r['status']==1){ ?>
                    <i class="mdi-image-brightness-1 " id='statusg-{{ $r['id']}}' data-st="{{$r['status']}}" style='color: green;'></i>
                <?php }
                else{ ?> 
                    <i class="mdi-image-brightness-1 " id='statusg-{{ $r['id']}}' data-st="{{$r['status']}}" style='color: red;'></i>
                <?php }?>
                    <a  class="btn-floating btn-small blue" id="" onclick="updateTestimonial( {{ $r['id'] }} );"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                    <a class="btn-floating btn-small red btn modal-trigger" id= "" onclick="deleteTestimonial( {{ $r['id'] }} );"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    

<input type="hidden" id="current" value="{{ $data['crnt_page'] }}" />
<input type="hidden" id="total_page" value="{{ $data['total_page'] }}" />
<input type="hidden" id="len" value="{{ $data['len'] }}" />