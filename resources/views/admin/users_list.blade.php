@extends('layouts.admin')

@section('content')
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    var Courier = {};
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('admin/users-filter',crnt,len,type,'','');
        
    };
    
    function delUser(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete User ??';
        var onyes ='delData';
        var param='\'admin/delete-user\','+id+' , \'admin/users-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function getBus(id){
        $('#show_user').openModal();
        var url="{{URL::to('admin/get-user')}}";
        var data={id:id};
        postAjax(url,data,function(data){
            
                $('#show_user #user_detail').html('');
                $('#show_user #user_detail').html(data);
        });
    }
   function editUser(id){
       var status = $('#e-status :selected').val();
       var url = '{{URL::to("admin/edit-user")}}';
       var data={status:status,id:id};
        postAjax(url,data,function(res){
            if(res.flag == 1){
                Materialize.toast(res.msg, 2000,'rounded');
                $('#show_user').closeModal();
                filterData('admin/users-filter');
            }else{
                Materialize.toast(res.msg, 2000,'rounded');
            }
        });
   }
    $(document).on('click','#search',function(){
        var status = $('#status :selected').val();
        var url = 'admin/card-filter';
        var data = {};
        
        if(status != ''){
            data['status'] = status;
        }
        
//        pagination(url,data,'vtable');
        
        
        filterData('admin/users-filter','',data);
    });
    
</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Users List</h1>
                <ul>
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Users List</a>
                    </li>
                    <li><a ></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" >
                        <label for="input_text">Filter by user name or mobile</label>
                    </div>
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:20%;display:inline-block;'>
                        <select id="status">
                            <option value="">All</option>
                            <option value="0">InActive</option>
                            <option value="1">Active</option>
                            <option value="2">Suspend</option>
                        </select>
                        <label for="input_text">Filter By Status</label>
                        
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" id="search" ><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Users List</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('admin/users-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('admin/users-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('users/card-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('admin/users-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('admin/users-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/users-filter',this.value);">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div id="show_user" class="modal bust-modal" style="width: 60%;font-size: 100%;overflow: auto;overflow-x: auto">
            
            <div id="user_detail" class="modal-content" style="display: table-cell;width: 40%">
              
            </div>
            <div class="modal-footer">
              <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
              <!--<button id="save_bus" href="#!" class="waves-effect waves-green btn-flat ">Save</button>-->
            </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
    </div>
    
@endsection