@extends('layouts.admin')

@section('content')
<?php
$lt = config('constant.LOGGER') == 'SS' ? 'ss' : 'admin';
?>
<style>
    .txt{
        text-align: center!important;
        font-size: 15px!important;
        border: 1px solid #9e9e9e!important;
        width: 45px!important;
        height: 34px!important;
        margin:0px!important;
        border-radius: 2px!important;
        color:black!important;
    }
    .txt:hover{
        background-color: transparent;
        cursor: auto;
    }
    .btn{
        width: 45px;
        padding: 0px;
        margin: 0px;
        height: 36px;
    }
</style>
<script>
    window.onload=function(){
        $("#<?php echo $body['id']?>").addClass("active");
        $('li.active').parent().parent().addClass('open');
        
        var crnt = $('#crnt').val();
        var len = $('#len').val();
        var type = $('#type').html();
        $('#cntrlbtn').css('display','none');

        getData('{{$lt}}/seat-seller-filter',crnt,len,type,'','');
    };
    
    function delSeatSeller(id){
        
        var title='<b>Confirmation<b>';
        var msg='Are you sure to Delete Seat Seller ??';
        var onyes ='delData';
        var param='\'{{$lt}}/del-seatseller\','+id+' , \'{{$lt}}/seat-seller-filter\'';
        var mdlname='confirmDel';
        modalOpen(mdlname,title,msg,onyes,param);
    }
    
    function addss(){
        $('#ssname').val('');
        $('#ssemail').val('');
        $('#ssemail').removeAttr('readonly');
        $('#sscon').val('');
        $('#sscity').val('');
        $('#sscomm').val('');
        $('#comm_type').val(0);
        $('#sshf').val('');
        $('#hf_type').val(0);
        if($('#sslogin').val() == 0){
            $('#ssparent').val(0);
        }
        
        $('#opr').val('Add');
        
        $('#ss-details').attr('action',"{{URL::to($lt.'/add-seatseller')}}");
        
        $('#modal-seatseller').find('#save-ss').html('Add');
        $('#modal-seatseller').openModal();
        $('#status').trigger('click');
        $('#ss-details').parsley().reset();
    }
    
    function editss(id){
        
        $('#ssname').val($('#name-'+id).html());
        $('#ssemail').val($('#email-'+id).html());
        $('#ssemail').attr('readonly','true');
        $('#sscon').val($('#con-'+id).html());
        $('#sscity').val($('#city-'+id).html());
        $('#sscomm').val($('#comm-'+id).html());
        $('#comm_type').val($('#comm-'+id).attr('name'));
        $('#sshf').val($('#hf-'+id).val());
        $('#hf_type').val($('#hf-'+id).attr('name'));
        $('#ssparent').val($('#parent-'+id).attr('name'));
        $('#sid').val(id);
        $('#opr').val('Update');
        $('#comm_type').material_select();
        $('#hf_type').material_select();
        var parent=$('#ssparent').val();
        if(parent !='undefined' && parent != null && comm_type == '0'){
            $('#ssparent').material_select();
        }
        $('#ss-details').attr('action',"{{URL::to($lt.'/edit-seatseller')}}");
        
        $('#modal-seatseller').find('#save-ss').html('Update');
        $('#modal-seatseller').openModal();
        if($('#sslogin').val() == 0){
            $('#ssparent').material_select();
        }
        var status = $('#status-'+id).val();
        var chk = $('#status').is(':checked');
        if(status == 1 && !chk){
            $('#status').trigger('click');
        }else if(status == 0 && chk){
            $('#status').trigger('click');
        }

        $('#ss-details').parsley().reset();
    }
    function ssdetails(id){
        
        $('#d_ssname').val($('#name-'+id).html());
        $('#d_ssemail').val($('#email-'+id).html());
        $('#d_sscon').val($('#con-'+id).html());
        $('#d_sscity').val($('#city-'+id).html());
        $('#d_sscomm').val($('#comm-'+id).html());
        $('#d_comm_type').val($('#comm-'+id).attr('name'));
        $('#d_sshf').val($('#hf-'+id).val());
        $('#d_hf_type').val($('#hf-'+id).attr('name'));
        $('#d_ssparent').val($('#parent-'+id).attr('name'));
        $('#d_sid').val(id);
        $('#d_opr').val('Update');
        $('#d_comm_type').material_select();
        $('#d_hf_type').material_select();
        var parent=$('#d_ssparent').val();
        if(parent !='undefined' && parent != null && comm_type == '0'){
            $('#d_ssparent').material_select();
        }
        $('#modal-seatseller-details').openModal();
        if($('#d_sslogin').val() == 0){
            $('#d_ssparent').material_select();
        }
        $('#ss-details').parsley().reset();
    }
    
    $(document).on('click','#save-ss',function(){
        var name=$('#ssname').val();
        var email=$('#ssemail').val();
        var password=$('#sspassword').val();
        var con=$('#sscon').val();
        var city=$('#sscity').val();
        var comm=$('#sscomm').val();
        var comm_type=$('#comm_type').val();
        var parent=$('#ssparent').val();
//        var h_type = $('#height_type:selected').val();
//        var w_type = $('#width_type:selected').val();
        
//        if(name == ''){
//            Materialize.toast('Name should not be blank !!', 2000,'rounded');
//            return false;
//        }else if(email== ''){
//            Materialize.toast('Email should not be blank !!', 2000,'rounded');
//            return false;
//        }else if(con == ''){
//            Materialize.toast('Contact No. should not be blank !!', 2000,'rounded');
//            return false;
//        
//        }else if(city == ''){
//            Materialize.toast('City should not be blank !!', 2000,'rounded');
//            return false;
//        
//        }else if(comm == ''){
//            Materialize.toast('Commission should not be blank !!', 2000,'rounded');
//            return false;
//        
//        }else if(comm_type == '0'){
//            Materialize.toast('Select Appropriate Commission Type !!', 2000,'rounded');
//            return false;
//        }else if(parent !='undefined' && parent != null && comm_type == '0'){
//            Materialize.toast('Select Parent Seat-Seller !!', 2000,'rounded');
//            return false;
//        }
        
        
        $('#ss-details').ajaxForm(function(res) {
           console.log(res);
           if(res.flag == 1){
               if($('#opr').val()==='Update'){
                   Materialize.toast('Seat-Seller Details Updated successfully !!', 2000,'rounded');
               }
               else{
                    Materialize.toast('Seat-Seller Added successfully !!', 2000,'rounded');
                }
                $('#modal-seatseller').closeModal();
               filterData('{{$lt}}/seat-seller-filter');
               
           }else{
               Materialize.toast(res.msg, 2000,'rounded');
           }
       });
    });

</script>

<!-- Breadcrumb -->
    <div class="page-title">

        <div class="row">
            <div class="col s12 m9 l10">
                <h1>Seat Seller <?php echo $body['type']!=""?$body['type']:''; ?></h1>
                <ul>
                    <li>
                        <a href="{{URL::to($lt.'/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a >Seat Seller <?php echo $body['type']!=""?$body['type']:''; ?></a>
                    </li>
                </ul>
            </div>
            
        </div>

    </div>
    <!-- /Breadcrumb -->

    <div class="row">
        
        
    </div>
    <div class="row" >
        <div class="col l12 m12 pb-0">
            <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header" style='font-size:20px;'><i class="fa fa-filter"></i>Filter</div>
                <div class="collapsible-body">
                    
                    <div class="input-field" style='margin: 15px 0 0 10px;width:30%;display:inline-block;'>
                        <input id="searchname" type="text" class="validate" onchange="return filterData('{{$lt}}/seat-seller-filter');">
                        <label for="input_text">Filter By Seat Seller Name</label>
                    </div>
                    <div style='width:10%;display:inline-block;'>
                        <a class="btn" onclick="return filterData('{{$lt}}/seat-seller-filter');"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </li>
            
          </ul>
        </div>
        <div class="col l12 m12">
            <div class="card-panel">
                <div class="row" style="margin-top: 0px;">
                    <div class="col s12 m12 l12">
                        <h4>Seat Seller : <span id="type"><?php echo $body['type']!=""?$body['type']:''; ?></span></h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col l12 m12">
                        <div class="" id='alerterror'style='text-align:center;display:none;'>
                            <!--No Data Found !!-->
                        </div>
                        <table id='vtable' class="table table-bordered table-striped">
                            
                        </table>
                        
                        
                    </div> 
                    
                    <div class="col l12 m12" align="center" id='cntrlbtn' style='text-align: center;'>
                        
                        <a class="btn" id="first" onclick="filterDataWith('{{$lt}}/seat-seller-filter',this);"><i class="fa fa-angle-double-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn" id="prev" onclick="filterDataWith('{{$lt}}/seat-seller-filter',this);"><i class="fa fa-angle-left" aria-hidden="true" style="font-size:30px;"></i></a>
                        <input type="number"  class='btn txt' id="crnt" value='1' onchange="return filterData('{{$lt}}/seat-seller-filter');">
                        <span style='font-size: 20px;'> / </span>
                        <span class='txt btn' id="total" style='background-color:transparent;border:0px!important;'></span>
                        <a class="btn " id="next" onclick="filterDataWith('{{$lt}}/seat-seller-filter',this);"><i class="fa fa-angle-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <a class="btn " id="last" onclick="filterDataWith('{{$lt}}/seat-seller-filter',this);"><i class="fa fa-angle-double-right" aria-hidden="true" style="font-size:30px;"></i></a>
                        <select class="browser-default txt btn" id="len" style='display: inline-block;background-color:transparent;color:black!important;padding:0px!important; ' onchange="return filterData('admin/seat-seller-filter',this.value);">
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Modal Structure -->
        <form id="ss-details" action="" method="post" enctype="multipart/form-data" data-parsley-validate >
        <div id="modal-seatseller" class="modal bust-modal" style='width:40%;font-size:100%;overflow-y: auto;overflow-x:hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Seat-Seller</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                    
                    <div class="content">
                        
                        <div class="input-field">
                            <input id="ssname" name="ssname" type="text" placeholder="Seat-Seller Name" required>
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="sid" id="sid" value="" />
                            <input type="hidden" name="opr" id="opr" value="" />
                            <input type="hidden" name="type" id='type' value="{{$body['type']}}" />
                            <label for="group">Seat-Seller Name</label>
                        </div>
                        <?php
                        if($body['type']!='Main'){ 
                            if(config('constant.LOGGER') == 'SS'){
//                                echo config("constant.CURRENT_LOGIN_ID");
                                ?>
                        <input type="hidden" name="ssparent" id="ssparent" placeholder="" value="<?php echo config("constant.CURRENT_LOGIN_ID") ?>" />
                                <input type="hidden" id="sslogin" value="1" />
                                             <?php
                            }else{
                            ?>
                        <input type="hidden" id="sslogin" value="0" />
                        <div class="input-field">
                            <select id='ssparent' name='ssparent' required>
                                <option value='0' selected disabled>Select Parent Seat-Seller</option>
                                <?php foreach($body['plist'] as $x){?>

                                    <option value='{{$x['id']}}'>{{$x['name']}}</option>

                                <?php } ?>

                            </select>
                            <label for="ssparent">Parent Seat-Seller</label>
                        </div>
                            <?php }
                            
                                } ?>
                        <div class="input-field">
                            <input id="ssemail" name="ssemail" type="text" placeholder="Email" required>
                            <label for="height">Email</label>
                        </div>
                        <div class="input-field">
                            <input id="sspassword" name="sspassword" type="text" placeholder="" required>
                            <label for="sspassword">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="sscon" name="sscon" type="number" data-parsley-type="digits" data-parsley-length="[10,11]" placeholder="Contact No." required>
                            <label for="height">Contact No.</label>
                        </div>
                        
                        <div class="input-field">
                            <input id="sscity" name="sscity" type="text" placeholder="City" required>
                            <label for="width">City</label>
                        </div>
                        <div class="input-field">
                            
                        </div>
                        <div class="input-field">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <input id="sscomm" name="sscomm" type="text" placeholder="Commission" required>
                            <label for="width">Commission Value & Type</label>
                                </div>
                                <div class="col l6 m6 s6">
                                    <select id="comm_type" name="comm_type" required>
                                        <option value='0' disabled selected>Select Commission Type</option>
                                        <option value='F'>Fixed</option>
                                        <option value='P'>Percentage (%)</option>
                                    </select>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="input-field">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <input id="sshf" name="sshf" type="text" placeholder="Handlefee" required>
                            <label for="width">Handlefee Value & Type</label>
                                </div>
                                <div class="col l6 m6 s6">
                                    <select id="hf_type" name="hf_type" required>
                                        <option value='0' disabled selected>Select Commission Type</option>
                                        <option value='F'>Fixed</option>
                                        <option value='P'>Percentage (%)</option>
                                    </select>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                    <input type="checkbox" id='status' name='status' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>
                                    
                                </label>
                            </p>
                        </div>
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
            <!--<a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Add</a>-->
            <button id="save-ss" class="modal-action waves-effect waves-green btn-flat ">Save</button>
          </div>
        </div>
         </form>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="modal-seatseller-details" class="modal bust-modal" style='width:40%;font-size:100%;overflow: hidden;'>
          <div class="modal-content">
            <!--<div class="card">-->
                <div class="title" style="margin: 0 0 25px 0;">
                    <h4 id='mdltitle'>Seat-Seller Details</h4>
                    <span id='mdlmsg' style='display:none;'></span>
                </div>
                
                    
                    <div class="content">
                        
                        <div class="input-field">
                            <input id="d_ssname" name="ssname" type="text" placeholder="Seat-Seller Name" readonly>
                            <input type="hidden" name="sid" id="d_sid" value="" />
                            <input type="hidden" name="opr" id="d_opr" value="" />
                            <input type="hidden" name="type" id='d_type' value="{{$body['type']}}" />
                            <label for="group">Seat-Seller Name</label>
                        </div>
                        <?php
                        if($body['type']!='Main'){ 
                            if(config('constant.LOGGER') == 'SS'){
//                                echo config("constant.CURRENT_LOGIN_ID");
                                ?>
                        <input type="hidden" name="ssparent" id="d_ssparent" placeholder="" value="<?php echo config("constant.CURRENT_LOGIN_ID") ?>" />
                                <input type="hidden" id="d_sslogin" value="1" />
                                             <?php
                            }else{
                            ?>
                        <input type="hidden" id="d_sslogin" value="0" />
                        <div class="input-field">
                            <select id='d_ssparent' name='ssparent' required>
                                <option value='0' selected disabled>Select Parent Seat-Seller</option>
                                <?php foreach($body['plist'] as $x){?>

                                    <option value='{{$x['id']}}'>{{$x['name']}}</option>

                                <?php } ?>

                            </select>
                            <label for="ssparent">Parent Seat-Seller</label>
                        </div>
                            <?php }
                            
                                } ?>
                        <div class="input-field">
                            <input id="d_ssemail" name="ssemail" type="text" readonly placeholder="Email">
                            <label for="height">Email</label>
                        </div>
                        <div class="input-field">
                            <input id="d_sspassword" name="sspassword" type="text" placeholder="" readonly>
                            <label for="sspassword">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="d_sscon" name="sscon" type="number" data-parsley-type="digits" data-parsley-length="[10,11]" placeholder="Contact No." readonly>
                            <label for="height">Contact No.</label>
                        </div>
                        
                        <div class="input-field">
                            <input id="d_sscity" name="sscity" type="text" placeholder="City" readonly>
                            <label for="width">City</label>
                        </div>
                        <div class="input-field">
                            
                        </div>
                        <div class="input-field">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <input id="d_sscomm" name="sscomm" type="text" placeholder="Commission" readonly>
                            <label for="width">Commission Value & Type</label>
                                </div>
                                <div class="col l6 m6 s6">
                                    <select id="d_comm_type" name="comm_type" disabled>
                                        <option value='0' disabled selected>Select Commission Type</option>
                                        <option value='F'>Fixed</option>
                                        <option value='P'>Percentage (%)</option>
                                    </select>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="input-field">
                            <div class="row">
                                <div class="col l6 m6 s6">
                                    <input id="d_sshf" name="sshf" type="text" placeholder="Handlefee" readonly>
                            <label for="width">Handlefee Value & Type</label>
                                </div>
                                <div class="col l6 m6 s6">
                                    <select id="d_hf_type" name="hf_type" disabled>
                                        <option value='0' disabled selected>Select Commission Type</option>
                                        <option value='F'>Fixed</option>
                                        <option value='P'>Percentage (%)</option>
                                    </select>
                                    <!--<label for="width_type">Commission Type</label>-->
                                </div>
                            </div>
                            
                        </div>
<!--                        <div class="input-field">
                            <p class="switch" style='padding: 10px 0 20px 0;'>
                                <label>
                                    <span style='font-size: 20px;padding-bottom: 10px;'>Status : </span>
                                    <input type="checkbox" id='status' name='status' value="1" />
                                    <span class="lever" style="margin-bottom: 8px;"></span>
                                    
                                </label>
                            </p>
                        </div>-->
                    </div>
                    
               
            <!--</div>-->
            
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">Ok</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <!-- Modal Structure -->
        <div id="confirmDel" class="modal">
          <div class="modal-content">
            <h4 id='mdltitle'>Confirmation</h4>
            <p id='mdlmsg'>Are you sure to Delete Service Provider ??</p>
          </div>
          <div class="modal-footer">
            <a id='mdlabort'class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
            <a id='mdlyes' class="modal-action modal-close waves-effect waves-green btn-flat ">Yes</a>
          </div>
        </div>
        
        <!-- Modal Structure Over-->
        
        <div class="fixed-action-btn" style="bottom: 75px; right: 24px;">
            <?php $t=$body['type']!=''?$body['type']:''; ?>
            <a class="btn-floating btn-large red tooltipped" onclick="addss();"data-position="top" data-delay="50" data-tooltip="Add Service Provider">
                <i class="mdi-content-add"></i>
            </a>
        </div>
    </div>


@endsection