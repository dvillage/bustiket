<?php
return array(
	'user_updated' => 'User Updated Successfully.',
	'credential_saved' => 'Credential Saved Successfully.',
	'verify_email_mail_subject' => 'Verify your email address',
	'welcome_mail_subject' => 'Welcomes to :platform',
	'auth_disabled' => 'Auth Credential Disabled Successfully.',
	'signup' => 'Congratulation!! Your signup was successful',
	'user_profile_updated' => 'Profile updated successful',
	'forgot_password_mail_sent' => 'Password reset link sent to your email address. Also check in spam folder',
	'forgetpassword_mail_subject' => 'Reset Password request',
	'password_changed' => 'Password changed successfully',
	'login' => 'Login success',
	'logout' => 'Logout successfully',
);
