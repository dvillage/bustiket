$(document).ready(function() {
    topmenu();
    
    $("#myCarousel .carousel-indicators li").first().addClass('active');
    $("#myCarousel .carousel-inner .item").first().addClass('active');
    
    searchWidth();
    $(".journey_tab .ctab").click(function(){


        var current_index = $(this).index();

        $(this).parents(".journey_tab").find(".ctab").removeClass("active");
        $(this).addClass("active");

        if($(window).width() < 768){
            if(current_index==1)
            {   
                $('#bus1-img1').css("display","block");
                $('#bus1-img2').css("display","none");
                $('#bus2-img1').css("display","none");
                $('#bus2-img2').css("display","block");
            }
            else
            {
                $('#bus1-img1').css("display","none");
                $('#bus1-img2').css("display","block");
                $('#bus2-img1').css("display","block");
                $('#bus2-img2').css("display","none");
            }
        }
        else
        {
            if(current_index==0)
            {   
                $('#bus1-img1').css("display","block");
                $('#bus1-img2').css("display","none");
                $('#bus2-img1').css("display","none");
                $('#bus2-img2').css("display","block");
            }
            else
            {
                $('#bus1-img1').css("display","none");
                $('#bus1-img2').css("display","block");
                $('#bus2-img1').css("display","block");
                $('#bus2-img2').css("display","none");
            }
        }
        $("#myTabContent .tab-pane").hide();
        $("#myTabContent .tab-pane:eq("+(current_index)+")").show();
    });
              
    $(".main_yellow_div .mobile_div .right_arrow").click(function() {
        var from = $(".main_yellow_div .mobile_div .center_img.active").index();
        var to = from + 1;
        how_to_order_slider(from, to);
    });
    $(".main_yellow_div .detail_div a").click(function() {
        var id=$(this).attr("id");
        $(".main_yellow_div .detail_div .slize").removeClass("active");
        $(this).closest(".slize").addClass("active");
        $(".main_yellow_div .mobile_div .center_img").removeClass("active");
        $(".main_yellow_div .mobile_div #"+id).addClass("active");
    });

    $(".main_yellow_div .mobile_div .left_arrow").click(function() {
        var from = $(".main_yellow_div .mobile_div .center_img.active").index();
        var to = from - 1;
        how_to_order_slider(from, to);
    });
    
    $(".main_4nd_div .next").click(function() {
            
        var from = $(".main_4nd_div .review_box .review.active").index();
        var fromvid=$(".main_4nd_div .reviewvid.active").index();

        var to = from + 1;
        var tovid=fromvid+1;
        review_slider(this,from, to);
        review_video(this,fromvid, tovid);
    });
    $(".main_4nd_div .previous").click(function() {
        var from = $(".main_4nd_div .review_box .review .active").index();
        var fromvid = $(".main_4nd_div .reviewvid .active").index();
        var to = from - 1;
        var tovid = fromvid - 1;
        review_slider(this,from, to);
        review_video(this,fromvid, tovid);
    });
    
    
    $('#subscribe-email').click(function(){
    var name = $('#sname').val();
    var email = $('#semail').val();
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    var msg = '';
    if(name == ''){
        msg += "please enter name \n";
    }
    if(email == ''){
        msg += "please enter email \n";
    }
    if (email!= '' && !re.test(email))
    {
        msg += "Please enter a valid email address \n";

    }   
    if(msg != ''){
        alert(msg);
        return;
    }
    $.ajax({
        data:{name:name,email:email},
        url: "subscribe.php",
        success: function(result){
            if(result == 1){
                $('#sname').val('');
                $('#semail').val('');
                $('#success-msg').css('display','block');
                $('#success-msg').html('Thank you for your submission. We will notify you for any promotion we offer');
            }else{
                $('#success-msg').css('display','block');
                $('#success-msg').html('your subscription failed.please try again.');
            }
        }
    });

});

    $("#bus_type_block_ul .selected").click(function (event) {            
         event.stopPropagation();
    });
    
    $(window).resize(function(){				
        topmenu();
        searchWidth();
    });
    
});

var d = new Date();
    var g = new Date();
    var f = d.setDate(d.getDate() + 3);
    g.setDate(g.getDate() + 3);
    $(function() {

        $(".datepicker").datepicker({
            //dateFormat: 'dd/mm/yy',

            numberOfMonths: 2,
            minDate: d,
            dateFormat: "dd-mm-yy",
        });
        $(".datepicker1").datepicker({
            //dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            minDate: g,
            dateFormat: "dd-mm-yy",
        });



    });

$(function() {
    
        var domCalendar = Calendar();
        
        domCalendar.init("departure_date", {
            showYearAndMonthMenu: false,
            endsLessThanNow: 2,
            dateFormat : 'DD-MMM-YYYY',
            startDate : moment().add(3, 'day').locale('en-NZ'),
            selectedDate : moment().add(3, 'day').locale('en-NZ')
        }, function(date) {
            
            var day = moment(date).locale('en-NZ').format('dddd');
            var dat = moment(date).locale('en-NZ').date();
            var month = moment(date).locale('en-NZ').format('MMM');
            var year = moment(date).locale('en-NZ').format('YY');
//            console.log(date);
//            console.log(day);
//            console.log(dat);
            $(".date-holder #onward_cal #cal_date").html(dat);
            $(".date-holder #onward_cal .day-holder #cal_do_week").html(day);
            $(".date-holder #onward_cal .day-holder #cal_mnth").html(month);
            $(".date-holder #onward_cal .day-holder #cal_yr").html(year);
//            month = moment(date).format('MM');
            month = moment(date).format('MM');
            year = moment(date).format('YYYY');
            var formated_date = dat+"-"+month+"-"+year;
            $("#datepicker").val(formated_date);
        });
        
        var domCalendar11 = Calendar();
        domCalendar11.init("departure_date11", {
            showYearAndMonthMenu: false,
            endsLessThanNow: 2,
            dateFormat : 'DD-MMM-YYYY',
            startDate : moment().add(3, 'day').locale('en-NZ'),
            selectedDate : moment().add(3, 'day').locale('en-NZ')
        }, function(date) {
            var day = moment(date).locale('en-NZ').format('dddd');
            var dat = moment(date).locale('en-NZ').date();
            var month = moment(date).locale('en-NZ').format('MMM');
            var year = moment(date).locale('en-NZ').format('YY');
            $(".date-holder #onward_cal #cal_date").html(dat);
            $(".date-holder #onward_cal .day-holder #cal_do_week").html(day);
            $(".date-holder #onward_cal .day-holder #cal_mnth").html(month);
            $(".date-holder #onward_cal .day-holder #cal_yr").html(year);
            month = moment(date).format('MM');
            year = moment(date).format('YYYY');
            var formated_date = dat+"-"+month+"-"+year;
            $("#datepicker11").val(formated_date);
        });
    });

var $j = jQuery.noConflict();

$j(document).ready(function() {

    var owl = $j("#owl-demo");

    owl.owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 3], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 2], // betweem 900px and 601px
        itemsTablet: [600, 1], //2 items between 600 and 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option\
                //autoPlay: true 
    });

    // Custom Navigation Events
    $j(".next").click(function() {
        owl.trigger('owl.next');
    });
    $j(".prev").click(function() {
        owl.trigger('owl.prev');
    });
    $j(".play").click(function() {
        owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
    });
    $j(".stop").click(function() {
        owl.trigger('owl.stop');
    });
});


function topmenu(){

    var li_con = $('#nav_main_us li').length;

     var ul_wth = $('#nav_main_us').innerWidth();

     var ul_wth_s = $('#sign-toggle').outerWidth(true);

     var nav_wth = $('#navbar').innerWidth();

     var rel_wth = nav_wth - ul_wth_s;

     //alert ("li_con " + li_con + "ul_wth" + ul_wth + "ul_wth_s" + ul_wth_s + "nav_wth" + nav_wth + "rel_wth" + rel_wth); 

     $('#nav_main_us li').css('width', parseInt(rel_wth/li_con)-20+'px');
}

function hideleftsession(){
    $(".left-session").hide();
}

function searchWidth() {
                                    
    var current_tab_index = $(".journey_tab .ctab.active").index();
    var current_selector_id = $(".tab-pane:eq("+current_tab_index+")").attr("id");
    var li_x = $('#'+current_selector_id+' li').filter(function() {
        return $(this).css('display') === 'none';
    }).length;
    var li_t = $('#'+current_selector_id+' li').length;
    var widht_out = $('#'+current_selector_id).innerWidth();
    var atn_w = $('#'+current_selector_id+' .action-zone').innerWidth();
    var g_w = widht_out - atn_w;
    var li_c = li_t - li_x;
    var width_doc = $(window).width();
    //alert(width_doc);
    //alert(widht_out);
    if (width_doc >= 767)
    {
        $('#bus-malam li').css('width', (g_w / li_c) + 'px');
        $('#bus-malam2 li').css('width', (g_w / li_c) + 'px');
        $(".preload").css("display","none");
        $(".city-zone").css("display","block");
        $(".date-zone").css("display","block");
        $(".action-zone").css("display","block");
    }
    else
    {
        $('#bus-malam li').css('width', '100%');
        $('#bus-malam2 li').css('width', '100%');
        $(".preload").css("display","none");
        $(".city-zone").css("display","block");
        $(".date-zone").css("display","block");
        $(".action-zone").css("display","block");
    }
//                                    var li_x = $('#bus-malam li').filter(function() {
//                                        return $(this).css('display') === 'none';
//                                    }).length;
//                                    var li_t = $('#bus-malam li').length;
//                                    var widht_out = $('#bus-malam').innerWidth();
//                                    var atn_w = $('#bus-malam .action-zone').innerWidth();
//                                    var g_w = widht_out - atn_w;
//                                    var li_c = li_t - li_x;
//                                    var width_doc = $(window).width();
//                                    //alert(width_doc);
//                                    //alert(widht_out);
//                                    if (width_doc >= 767)
//                                    {
//                                        $('#bus-malam li').css('width', (g_w / li_c) + 'px');
//                                        $('#bus-malam2 li').css('width', (g_w / li_c) + 'px');
//                                    }
//                                    else
//                                    {
//                                        $('#bus-malam li').css('width', '100%');
//                                        $('#bus-malam2 li').css('width', '100%');
//                                    }
}

function switchTag(){
    var val1=$("#ter_from").val();
    var val2=$("#tag").val();
    $("#ter_from").val(val2);
    $("#tag").val(val1);
}

function how_to_order_slider(from, to){
    var current_index = $(".main_yellow_div .mobile_div .center_img.active").index();
    if (from > to && current_index < 2)
        return;
    var total_images = $(".main_yellow_div .mobile_div .center_img").size();
    if (total_images < to)
        return;
    $(".main_yellow_div .mobile_div .center_img").removeClass("active");
    $(".main_yellow_div .mobile_div .center_img:eq(" + (to - 1) + ")").addClass("active");
    $(".main_yellow_div .detail_div .slize").removeClass("active");
    $(".main_yellow_div .detail_div .slize:eq(" + (to - 1) + ")").addClass("active");
}

function review_slider(event,from, to){
    var current_index = $(".main_4nd_div .review_box .review.active").index();
    if (from > to && current_index < 1)
        return;
    var total_images = $(".main_4nd_div .review_box .review").size();
    if (total_images <= to)
        return;
    $(".main_4nd_div .review_box .review").removeClass("active");
    $(".main_4nd_div .review_box .review:eq(" + (to) + ")").addClass("active");
}
function review_video(event,from,to){
    var current_index =$(".main_4nd_div .reviewvid.active").index();
    if(from > to && current_index < 1)
        return;
    var total_video=$(".main_4nd_div .reviewvid").size();
    if(total_video <= to)
        return;
    $(".main_4nd_div .reviewvid").removeClass("active");
    $(".main_4nd_div .reviewvid:eq("+ (to) + ")").addClass("active");
}
function gotoTop()
{
    window.scrollTo(0,0);
}
function click_tab_change(type)
{ 
        $(".journey_tab").find(".ctab").removeClass("active");
        $("#"+type).addClass("active");
        if(type=='tiket')
        {
            $('#bus1-img1').css("display","block");
            $('#bus1-img2').css("display","none");
            $('#bus2-img1').css("display","none");
            $('#bus2-img2').css("display","block");
            $("#bus-malam").css("display","block");
            $("#bus-malam2").css("display","none");
        }
        else 
        {
            $('#bus1-img1').css("display","none");
            $('#bus1-img2').css("display","block");
            $('#bus2-img1').css("display","block");
            $('#bus2-img2').css("display","none");
            $("#bus-malam").css("display","none");
            $("#bus-malam2").css("display","block");
        }

}
function dat_val()
{
    if (document.getElementById('tag').value != "")
    {
        document.getElementById('dat').style.display = 'block';
        document.getElementById('datepicker').focus();
        return false;
    }
    searchWidth()
}
function pariwisataValidate()
{
    if (document.getElementById('ter_from_pa').value == "")
    {
        alert("Silakan masukan Kota Asal terlebih dahulu");
        document.getElementById('ter_from_pa').focus();
        return false;

    }

    if (document.getElementById('ter_bustype_pa').value == "")
    {
        alert("please enter the Destination value");
        document.getElementById('ter_bustype_pa').focus();
        return false;

    }

    /*if ((document.getElementById('ter_from_pa').value != "") && (document.getElementById('ter_bustype_pa').value != "") && (document.getElementById('datepicker').value == ""))
    {
        alert("Please choose Date of Journey");
        document.getElementById('datepicker').focus();
        return false;
    }*/

    var date = document.getElementById('datepicker').value;
    var date1 = document.getElementById('datepicker1').value;
    date = date.split('-');
    new_date = new Date(date[2], date[1], date[0]);
    date1 = date1.split('-');
    new_date1 = new Date(date1[2], date1[1], date1[0]);


    if (document.getElementById('datepicker1').value != "")
    {



        if (new_date1 < new_date)
        {
            alert('Please select Return date greater than Journey date');
            document.getElementById('datepicker1').value = "";
            document.getElementById('datepicker1').focus();
            return false;
        }
    }

}

function validate()
{
//alert("hello");
    if (document.getElementById('ter_from').value == "")
    {
        alert("Silakan masukan Kota Asal terlebih dahulu");
        document.getElementById('ter_from').focus();
        return false;

    }

    if (document.getElementById('tag').value == "")
    {
        alert("please enter the Destination value");
        document.getElementById('tag').focus();
        return false;

    }

    if ((document.getElementById('ter_from').value != "") && (document.getElementById('tag').value != "") && (document.getElementById('datepicker').value == ""))
    {
//alert("Please choose Date of Journey");
        document.getElementById('datepicker').focus();
        return false;
    }

    var date = document.getElementById('datepicker').value;
    var date1 = document.getElementById('datepicker1').value;
    date = date.split('-');
    new_date = new Date(date[2], date[1], date[0]);
    date1 = date1.split('-');
    new_date1 = new Date(date1[2], date1[1], date1[0]);


    if (document.getElementById('datepicker1').value != "")
    {



        if (new_date1 < new_date)
        {
            alert('Please select Return date greater than Journey date');
            document.getElementById('datepicker1').value = "";
            document.getElementById('datepicker1').focus();
            return false;
        }
        searchWidth()



    }
}
function showreturn(val)
{
    if (val == 2) {
        document.getElementById('datepicker1').disabled = false;
        document.getElementById('for-date1').classList.remove('dclass');
    }
    else
    {
        document.getElementById('datepicker1').disabled = true;
        document.getElementById('for-date1').classList.add('dclass');
    }
    searchWidth();
}

$jq = $.noConflict();
$jq(document).ready(function() {

//    $jq("#ter_from").autocomplete("fromcity.php", {
    
    $jq(this.target).find('#ter_from').autocomplete("fromcity.php", {
        width: 155,
        formatResult: function(data, value) {
            return value.split(",")[0];
        }
    });

//    $jq("#ter_from_pa").autocomplete("fromcity.php", {
    $jq(this.target).find('#ter_from_pa').autocomplete("fromcity.php", {
        width: 155,
        formatResult: function(data, value) {
            return value.split(",")[0];
        }
    });

   /* $jq("#ter_bustype_pa").autocomplete("fromBusType.php", {
        width: 155,
        formatResult: function(data, value) {
            return value.split(",")[0];
        }
    });*/
});
$jq("body").addClass('index');

function get_val_points()
{

    var fromval = document.getElementById('ter_from').value;
//alert(fromval);
    if (fromval != '')
    {
        $jq(document).ready(function() {
            $jq("#tag").autocomplete("tocity.php?from=" + fromval, {
                width: 155,
                formatResult: function(data, value) {
                    return value.split(",")[0];
                }
            });
        });
    }
    else
    {
        alert("Masukan Kota Asal...");
        document.getElementById('ter_from').focus();
        document.getElementById('tag').value = "";
        return false;
    }

} 

function selectBusType(data)
{
    var ter_bustype_pa = document.getElementById("ter_bustype_pa").value;

    if(data == "AC")
    {
        if(ter_bustype_pa != "")
        {
            if(ter_bustype_pa == "Non AC,")
            {
                document.getElementById("ter_bustype_pa").value = data+","+ter_bustype_pa;     
                document.getElementById("dropdown-button").innerHTML = data+","+ter_bustype_pa;     
                document.getElementById("bus_type_ac_check").style.display = "block";
            }
            else
            {
                var str = ter_bustype_pa.replace("AC,", "");
                document.getElementById("ter_bustype_pa").value = str;     
                document.getElementById("dropdown-button").innerHTML = str;     
                document.getElementById("bus_type_ac_check").style.display = "none";
                if(document.getElementById("dropdown-button").innerHTML == "")
                {
                    document.getElementById("dropdown-button").innerHTML = "Select Bus Type";
                }
            }
        }
        else
        {
            document.getElementById("ter_bustype_pa").value = data+","+ter_bustype_pa;     
            document.getElementById("dropdown-button").innerHTML = data+","+ter_bustype_pa;     
            document.getElementById("bus_type_ac_check").style.display = "block";
        }


    }
    if(data == "Non AC")
    {
        if(ter_bustype_pa != "")
        {
            if(ter_bustype_pa == "AC,")
            {
                document.getElementById("ter_bustype_pa").value = data+","+ter_bustype_pa;   
                document.getElementById("dropdown-button").innerHTML = data+","+ter_bustype_pa;     
                document.getElementById("bus_type_nonac_check").style.display = "block";
            }
            else
            {
                var str = ter_bustype_pa.replace("Non AC,", "");
                document.getElementById("ter_bustype_pa").value = str;     
                document.getElementById("dropdown-button").innerHTML = str;
                document.getElementById("bus_type_nonac_check").style.display = "none";
                if(document.getElementById("dropdown-button").innerHTML == "")
                {
                    document.getElementById("dropdown-button").innerHTML = "Select Bus Type";
                }
            }
        }
        else
        {
                document.getElementById("ter_bustype_pa").value = data+","+ter_bustype_pa;   
                document.getElementById("dropdown-button").innerHTML = data+","+ter_bustype_pa;     
                document.getElementById("bus_type_nonac_check").style.display = "block";
        }
    }
    if(data == "Select All")
    {
        document.getElementById("ter_bustype_pa").value = "AC,Non AC,";      
        document.getElementById("dropdown-button").innerHTML = "AC,Non AC,";
        document.getElementById("bus_type_nonac_check").style.display = "block";
        document.getElementById("bus_type_ac_check").style.display = "block";
    }
}