    var smd = JSON.parse($('#smaindata').val());
    var sad = JSON.parse($('#sadata').val());
    var sbd = JSON.parse($('#sbdata').val());
    var testdata2 = [
        {key: "One", y: 5},
        {key: "Two", y: 2},
        {key: "Three", y: 9},
        {key: "Four", y: 7},
        {key: "Five", y: 4},
        {key: "Six", y: 3},
        {key: "Seven", y: 0.5}
    ];
    var box_height = 250;
    var height = 350;
    var width = 350;
    nv.addGraph(function() {
        var chart = nv.models.pieChart()
            .x(function(d) { return d.key })
            .y(function(d) { return d.y })
            .width(width)
            .height(height)
//            .showTooltipPercent(true);
        d3.select("#ssmain")
            .datum(smd)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);
        d3.select("#ssa")
            .datum(sad)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);
        d3.select("#ssb")
            .datum(sbd)
            .transition().duration(1200)
            .attr('width', width)
            .attr('height', box_height)
            .call(chart);

        return chart;
    });
    
    
    var tsale = JSON.parse($('#tsale').val());
    var smcommm = JSON.parse($('#smcomm').val());
    var sabcomm = JSON.parse($('#sabcomm').val());
    var income = {};
    if($('#income')){
        var t,m,y;
//        console.log(smcommm);
        t = smcommm[0]['values'][0]['value'] - sabcomm[0]['values'][0]['value'];
        m = smcommm[0]['values'][1]['value'] - sabcomm[0]['values'][1]['value'];
        y = smcommm[0]['values'][2]['value'] - sabcomm[0]['values'][2]['value'];
       income = [{"key":"Commission in rupies","values":[{"label":"Today","value":t},{"label":"Month","value":m},{"label":"Year","value":y}]}];
    }
    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(true)
            .staggerLabels(tsale[0].values.length > 8)
            .showValues(true)
            .duration(250)
//            .showLegend(true)
            .showYAxis(false)
//            .showXAxis(false)
//            .height(box_height)
            .wrapLabels(true)
            ;
        d3.select('#totalsale svg')
            .datum(tsale)
            .attr('height', height)
            .call(chart);
        d3.select('#commss svg')
            .datum(smcommm)
            .attr('height', height)
            .call(chart);
        d3.select('#comab svg')
            .datum(sabcomm)
            .attr('height', height)
            .call(chart);
        
        if(income){
            d3.select('#income svg')
            .datum(income)
            .attr('height', height)
            .call(chart);
        }
    
        nv.utils.windowResize(chart.update);
        return chart;
    });
    
    
    var msale = JSON.parse($('#smonth').val());
    var sdaily = JSON.parse($('#sdaily').val());
//    console.log(msale);
    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(true)
            .staggerLabels(msale[0].values.length > 8)
            .showValues(true)
            .duration(250)
//            .showLegend(true)
//            .showYAxis(false)
//            .showXAxis(false)
//            .height(box_height)
            .wrapLabels(true)
            ;
            chart.yAxis
            .axisLabel('Number of seat sold');
        d3.select('#monthlysale svg')
            .datum(msale)
            .attr('height', height)
            .call(chart);
        
        nv.utils.windowResize(chart.update);
        return chart;
    });
    
    
    var sdaily = JSON.parse($('#sdaily').val());
//    console.log(msale);
    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(true)
            .staggerLabels(msale[0].values.length > 8)
            .showValues(true)
            .duration(250)
            .rotateLabels(-45)
//            .showLegend(true)
//            .showYAxis(false)
//            .showXAxis(false)
//            .height(box_height)
            .wrapLabels(true)
            ;
        chart.yAxis
            .axisLabel('Number of seat sold');
            
        d3.select('#dailysale svg')
            .datum(sdaily)
            .attr('height', height)
            .call(chart);
        
        nv.utils.windowResize(chart.update);
        return chart;
    });
    
    
    
    var ctx = document.getElementById("ssasalechart");
    var ssasale = JSON.parse($('#ssasale').val());
    var cnt = ssasale['labels'].length;
    
    if(cnt > 20){
        var h = cnt*10;
        $('#ssasalechart').attr('height',h);
        $('#ssbsalechart').attr('height',h);
    }
    
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: ssasale,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                            position: 'top',
                            ticks: {
                                beginAtZero:true
                            }
                    }],
            }
            
        }
    });
    
    var ctx = document.getElementById("ssbsalechart");
    var ssbsale = JSON.parse($('#ssbsale').val());
    
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: ssbsale,
//        data: {
//        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange","Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//        datasets: [{
//            label: '# of Votes',
//            data: [12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3],
//            backgroundColor: [
//                'rgba(255, 99, 132, 0.2)',
//                'rgba(54, 162, 235, 0.2)',
//                'rgba(255, 206, 86, 0.2)',
//                'rgba(75, 192, 192, 0.2)',
//                'rgba(153, 102, 255, 0.2)',
//                'rgba(255, 159, 64, 0.2)'
//            ],
//            borderColor: [
//                'rgba(255,99,132,1)',
//                'rgba(54, 162, 235, 1)',
//                'rgba(255, 206, 86, 1)',
//                'rgba(75, 192, 192, 1)',
//                'rgba(153, 102, 255, 1)',
//                'rgba(255, 159, 64, 1)'
//            ],
//            borderWidth: 1
//        }]
//    },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                            position: 'top',
                            ticks: {
                                beginAtZero:true
                            }
                    }],
            }
            
        }
    });